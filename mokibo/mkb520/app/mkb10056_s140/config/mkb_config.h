/*
 * ===============================================================================
 * Copyright (C) 2017-2019
 * INNOPRESSO INC. All Rights Reserved
 *
 * The content of this file is copyrighted by INNOPRESSO or its
 * affiliated, associated or related companies. All copyrights and other
 * associated intellectual property rights related thereto are expressly
 * reserved by INNOPRESSO INC.
 * ===============================================================================
 */

/**
 * Configuration Wizard Annotations Rules
 * https://www.keil.com/pack/doc/CMSIS/Pack/html/configWizard.html
 */

/** @file
 *
 * @defgroup mkb_config Innopresso Mokobo TouchKeyboard Product configuration
 * @{
 * @ingroup mkb_config
 *
 * @brief Mokibo TouchKeyboard configuration with target-specific default values.
 */

#ifndef MKB_CONFIG_H
#define MKB_CONFIG_H

// <<< Use Configuration Wizard in Context Menu >>>\n
#ifdef USE_APP_CONFIG
#include "app_config.h"
#endif

#include "sdk_config.h"

// <h> Error Check Support
//==========================================================

// <q> Error Check Enable - APP_ERROR_CHECK use or not
// <i> If build mode is debug, it use APP_ERROR_CHECK (RELEASE:print error log then reset, DEBUG:print error log then halt)
// <i> If build mode is release, it print error log.

#ifndef MKB_ERROR_CHECK_ENABLED
#define MKB_ERROR_CHECK_ENABLED 1
#endif

// </h> Error Check Support
//==========================================================

// <h> MOKIBO Device Information
//==========================================================

// <s> MKB_DEVICE_NAME - MOKIBO Device Name //BT Name
#define MKB_DEVICE_NAME "Mokibo Folio"

// <s> MKB_MODEL_NAME - MOKIBO Model Name
#define MKB_MODEL_NAME "MKB420"

// <s> MKB_HW_VERSION - MOKIBO Hardware version
#define MKB_HW_VERSION "1.00" // 20211001

// <s> MKB_FW_VERSION - MOKIBO Firmware version
#define MKB_FW_VERSION "1.26.3b5" // 20211014

// </h> MOKIBO Device Information
//==========================================================

// <h> Drivers Related Options
//==========================================================

// <e> TWI Support
//==========================================================
#ifndef MKB_TWI_ENABLED
#define MKB_TWI_ENABLED 1
#endif

// <e> TWI Init Delay Support
//----------------------------------------------------------
#ifndef MKB_TWI_INIT_DELAY_ENABLED
#define MKB_TWI_INIT_DELAY_ENABLED 0
#endif

// <o> TWI Init Delay Time [ms] (0~1000)
// <0-1000>
#ifndef MKB_TWI_INIT_DELAY
#define MKB_TWI_INIT_DELAY 10
#endif

// </e> TWI Init Delay Support
//----------------------------------------------------------

// <q> TWI Probe Support
#ifndef MKB_TWI_PROBE_ENABLED
#define MKB_TWI_PROBE_ENABLED 1
#endif

// <o> TWI Pin setup - SCL Pin number (0~47)
// <0-47>
#ifndef MKB_TWI_SCL_PIN
#define MKB_TWI_SCL_PIN 02 // 3
#endif

// <o> TWI Pin setup - SDA Pin number (0~47)
// <0-47>
#ifndef MKB_TWI_SDA_PIN
#define MKB_TWI_SDA_PIN 47 // 4
#endif

// </e> TWI Support
//==========================================================

// <q> AM2320 Sensor Support
#ifndef MKB_AM2320_ENABLED
#define MKB_AM2320_ENABLED 0
#endif

//==========================================================

// <e> CYPRESS Touchpad Sensor Support
//==========================================================
#ifndef MKB_CYPRESS_ENABLED
#define MKB_CYPRESS_ENABLED 0
#endif

// <q> Enable Firmware Update Support
#ifndef MKB_CYPRESS_FW_INCLUDED
#define MKB_CYPRESS_FW_INCLUDED 1
#endif

// <o> MKB_CYPRESS_MODE - Sensor Scanning Medthods
// <1=> TIMER
// <2=> GPIOTE

#ifndef MKB_CYPRESS_MODE
#define MKB_CYPRESS_MODE 2
#endif

// <e> Enable Cypress Power Pin
//-----------------------------------------------------
#ifndef MKB_CYPRESS_POWER_PIN_ENABLED
#define MKB_CYPRESS_POWER_PIN_ENABLED 0
#endif

// <o> MKB_CYPRESS_POWER_PIN - Power Control Pin (0~47)
// <i> U6_EN (cypress touch power) - use reset pin
// <0-47>
#ifndef MKB_CYPRESS_POWER_PIN
#define MKB_CYPRESS_POWER_PIN 30
#endif
//-----------------------------------------------------
// </e> Enable Cypress Power Pin

// <e> Enable Cypress Reset Pin
//-----------------------------------------------------
#ifndef MKB_CYPRESS_RESET_PIN_ENABLED
#define MKB_CYPRESS_RESET_PIN_ENABLED 1
#endif

// <o> MKB_CYPRESS_RESET_PIN - Reset Control Pin (0~47)
// <i> TT_RST
// <0-47>
#ifndef MKB_CYPRESS_RESET_PIN
#define MKB_CYPRESS_RESET_PIN 30
#endif
//-----------------------------------------------------
// </e> Enable Cypress Reset Pin

// <e> Enable Cypress INT Pin
//-----------------------------------------------------
#ifndef MKB_CYPRESS_INT_PIN_ENABLED
#define MKB_CYPRESS_INT_PIN_ENABLED 0
#endif

// <o> MKB_CYPRESS_INT_PIN - Interrupt Pin (0~47)
// <i> TT_INT
// <0-47>
#ifndef MKB_CYPRESS_INT_PIN
#define MKB_CYPRESS_INT_PIN 31
#endif
//-----------------------------------------------------
// </e> Enable Cypress INT Pin

// <o> MKB_CYPRESS_SCAN_INTERVAL [ms] - Sensor Scan timer interval
// <8=> 8ms
// <10=> 10ms

#ifndef MKB_CYPRESS_SCAN_INTERVAL
#define MKB_CYPRESS_SCAN_INTERVAL 8
#endif

// </e> CYPRESS Touchpad Sensor Support
//==========================================================

// <e> Key Matrix Support
//==========================================================
#ifndef MKB_KEYMATRIX_ENABLED
#define MKB_KEYMATRIX_ENABLED 0
#endif

// <o> MKB_KEYMATRIX_MODE - Scanning Medthods
// <1=> TIMER
// <2=> GPIOTE

#ifndef MKB_KEYMATRIX_MODE
#define MKB_KEYMATRIX_MODE 2
#endif

// <o> MKB_KEYMATRIX_SCAN_INTERVAL [ms] - Scan timer interval (10~200)
// <10-200>

#ifndef MKB_KEYMATRIX_SCAN_INTERVAL
#define MKB_KEYMATRIX_SCAN_INTERVAL 15
#endif

// <o> MKB_KEYMATRIX_TYPE
// <0=> MOKIBO
// <1=> IMAGES

#ifndef MKB_KEYMATRIX_TYPE
#define MKB_KEYMATRIX_TYPE 0
#endif

// <o> MKB_KEYMATRIX_PIN_COL_0  - Column 0 pin number (0~47)
// <0-47>
#ifndef MKB_KEYMATRIX_PIN_COL_0
#define MKB_KEYMATRIX_PIN_COL_0 47
#endif

// <o> MKB_KEYMATRIX_PIN_COL_1  - Column 1 pin number (0~47)
// <0-47>
#ifndef MKB_KEYMATRIX_PIN_COL_1
#define MKB_KEYMATRIX_PIN_COL_1 2
#endif

// <o> MKB_KEYMATRIX_PIN_COL_2  - Column 2 pin number (0~47)
// <0-47>
#ifndef MKB_KEYMATRIX_PIN_COL_2
#define MKB_KEYMATRIX_PIN_COL_2 26
#endif

// <o> MKB_KEYMATRIX_PIN_COL_3  - Column 3 pin number (0~47)
// <0-47>
#ifndef MKB_KEYMATRIX_PIN_COL_3
#define MKB_KEYMATRIX_PIN_COL_3 27
#endif

// <o> MKB_KEYMATRIX_PIN_COL_4  - Column 4 pin number (0~47)
// <0-47>
#ifndef MKB_KEYMATRIX_PIN_COL_4
#define MKB_KEYMATRIX_PIN_COL_4 45
#endif

// <o> MKB_KEYMATRIX_PIN_COL_5  - Column 5 pin number (0~47)
// <0-47>
#ifndef MKB_KEYMATRIX_PIN_COL_5
#define MKB_KEYMATRIX_PIN_COL_5 42
#endif

// <o> MKB_KEYMATRIX_PIN_COL_6  - Column 6 pin number (0~47)
// <0-47>
#ifndef MKB_KEYMATRIX_PIN_COL_6
#define MKB_KEYMATRIX_PIN_COL_6 39
#endif

// <o> MKB_KEYMATRIX_PIN_COL_7  - Column 7 pin number (0~47)
// <0-47>
#ifndef MKB_KEYMATRIX_PIN_COL_7
#define MKB_KEYMATRIX_PIN_COL_7 38
#endif

// <o> MKB_KEYMATRIX_PIN_COL_8  - Column 8 pin number (0~47)
// <0-47>
#ifndef MKB_KEYMATRIX_PIN_COL_8
#define MKB_KEYMATRIX_PIN_COL_8 37
#endif

// <o> MKB_KEYMATRIX_PIN_COL_9  - Column 9 pin number (0~47)
// <0-47>
#ifndef MKB_KEYMATRIX_PIN_COL_9
#define MKB_KEYMATRIX_PIN_COL_9 36
#endif

// <o> MKB_KEYMATRIX_PIN_COL_10  - Column 10 pin number (0~47)
// <0-47>
#ifndef MKB_KEYMATRIX_PIN_COL_10
#define MKB_KEYMATRIX_PIN_COL_10 35
#endif

// <o> MKB_KEYMATRIX_PIN_COL_11  - Column 11 pin number (0~47)
// <0-47>
#ifndef MKB_KEYMATRIX_PIN_COL_11
#define MKB_KEYMATRIX_PIN_COL_11 34
#endif

// <o> MKB_KEYMATRIX_PIN_COL_12  - Column 12 pin number (0~47)
// <0-47>
#ifndef MKB_KEYMATRIX_PIN_COL_12
#define MKB_KEYMATRIX_PIN_COL_12 4 // 30 //10
#endif

// <o> MKB_KEYMATRIX_PIN_COL_13  - Column 13 pin number (0~47)
// <0-47>
#ifndef MKB_KEYMATRIX_PIN_COL_13
#define MKB_KEYMATRIX_PIN_COL_13 3 // 31 //9
#endif

// <o> MKB_KEYMATRIX_PIN_COL_14  - Column 14 pin number (0~47)
// <0-47>
#ifndef MKB_KEYMATRIX_PIN_COL_14
#define MKB_KEYMATRIX_PIN_COL_14 8
#endif

// <o> MKB_KEYMATRIX_PIN_ROW_0  - Row 0 pin number (0~47)
// <0-47>
#ifndef MKB_KEYMATRIX_PIN_ROW_0
#define MKB_KEYMATRIX_PIN_ROW_0 5
#endif

// <o> MKB_KEYMATRIX_PIN_ROW_1  - Row 1 pin number (0~47)
// <0-47>
#ifndef MKB_KEYMATRIX_PIN_ROW_1
#define MKB_KEYMATRIX_PIN_ROW_1 6
#endif

// <o> MKB_KEYMATRIX_PIN_ROW_2  - Row 2 pin number (0~47)
// <0-47>
#ifndef MKB_KEYMATRIX_PIN_ROW_2
#define MKB_KEYMATRIX_PIN_ROW_2 7
#endif

// <o> MKB_KEYMATRIX_PIN_ROW_3  - Row 3 pin number (0~47)
// <0-47>
#ifndef MKB_KEYMATRIX_PIN_ROW_3
#define MKB_KEYMATRIX_PIN_ROW_3 33
#endif

// <o> MKB_KEYMATRIX_PIN_ROW_4  - Row 4 pin number (0~47)
// <0-47>
#ifndef MKB_KEYMATRIX_PIN_ROW_4
#define MKB_KEYMATRIX_PIN_ROW_4 40
#endif

// <o> MKB_KEYMATRIX_PIN_ROW_5  - Row 5 pin number (0~47)
// <0-47>
#ifndef MKB_KEYMATRIX_PIN_ROW_5
#define MKB_KEYMATRIX_PIN_ROW_5 43
#endif

// <o> MKB_KEYMATRIX_PIN_ROW_6  - Row 6 pin number (0~47)
// <0-47>
#ifndef MKB_KEYMATRIX_PIN_ROW_6
#define MKB_KEYMATRIX_PIN_ROW_6 44
#endif

// <o> MKB_KEYMATRIX_PIN_ROW_7  - Row 7 pin number (0~47)
// <0-47>
#ifndef MKB_KEYMATRIX_PIN_ROW_7
#define MKB_KEYMATRIX_PIN_ROW_7 46
#endif

// </e> Key Matrix Support
//==========================================================

// <e> PIXART Touchpad Sensor Support
//==========================================================
#ifndef MKB_PIXART_ENABLED
#define MKB_PIXART_ENABLED 1
#endif

// <q> Enable CLI PIXART Support
#ifndef MKB_CLI_PIXART_ENABLED
#define MKB_CLI_PIXART_ENABLED 0
#endif

// <o> MKB_PIXART_MODE - Sensor Scanning Medthods
// <1=> TIMER
// <2=> GPIOTE

#ifndef MKB_PIXART_MODE
#define MKB_PIXART_MODE 2
#endif

// <o> MKB_PIXART_SCAN_INTERVAL for TIMER Mose [ms] - (1~1000)
// <1-1000>

#ifndef MKB_PIXART_SCAN_INTERVAL
#define MKB_PIXART_SCAN_INTERVAL 4
#endif

// <o> MKB_PIXART_INT_SCAN_INTERVAL for GPIOTE Mode [ms] - (1~1000)
// <1-1000>

#ifndef MKB_PIXART_INT_SCAN_INTERVAL
#define MKB_PIXART_INT_SCAN_INTERVAL 1
#endif

// <o> MKB_PIXART_WAIT_INTERVAL, wait time before going to Touch Mode [ms] - (1~1000)
// <1-1000>

#ifndef MKB_PIXART_WAIT_INTERVAL
#define MKB_PIXART_WAIT_INTERVAL 100
#endif

// <e> Enable PIXART Power Pin
//-----------------------------------------------------
#ifndef MKB_PIXART_POWER_PIN_ENABLED
#define MKB_PIXART_POWER_PIN_ENABLED 0
#endif

// <o> MKB_PIXART_POWER_PIN - Power Control Pin (0~47)
// <i> U6_EN (pixart touch power) - use reset pin
// <0-47>
#ifndef MKB_PIXART_POWER_PIN
#define MKB_PIXART_POWER_PIN 30
#endif
//-----------------------------------------------------
// </e> Enable PIXART Power Pin

// <e> Enable PIXART Reset Pin
//-----------------------------------------------------
#ifndef MKB_PIXART_RESET_PIN_ENABLED
#define MKB_PIXART_RESET_PIN_ENABLED 0
#endif

// <o> MKB_PIXART_RESET_PIN - Reset Control Pin (0~47)
// <i> TT_RST
// <0-47>
#ifndef MKB_PIXART_RESET_PIN
#define MKB_PIXART_RESET_PIN 30
#endif
//-----------------------------------------------------
// </e> Enable PIXART Reset Pin

// <e> Enable PIXART INT Pin
//-----------------------------------------------------
#ifndef MKB_PIXART_INT_PIN_ENABLED
#define MKB_PIXART_INT_PIN_ENABLED 1
#endif

// <o> MKB_PIXART_INT_PIN - Interrupt Pin (0~47)
// <i> TT_INT
// <0-47>
#ifndef MKB_PIXART_INT_PIN
#define MKB_PIXART_INT_PIN 45
#endif
//-----------------------------------------------------
// </e> Enable PIXART INT Pin

// </e> PIXART Touchpad Sensor Support
//==========================================================

// </h> Drivers Related Options
//==========================================================

// <h> Modules Related Options
//==========================================================

// <e> Battery Module Support
//==========================================================
#ifndef MKB_BATTERY_ENABLED
#define MKB_BATTERY_ENABLED 1
#endif

// <o> Battery Sense Type
// <0=> Simulation
// <1=> ADC
// <2=> STM8

#ifndef MKB_BATTERY_SENSE_TYPE
#define MKB_BATTERY_SENSE_TYPE 1
#endif

// <o> Battery Sampling Time 1sec-1hour [ms] - (1000~360000)
// <1000-360000>
#ifndef MKB_BATTERY_SAMPLING_TIME
#define MKB_BATTERY_SAMPLING_TIME 1000
#endif

// <o> Battery Low Level [%] - (0~100)
// <0-100>
#ifndef MKB_BATTERY_LOW_LEVEL
#define MKB_BATTERY_LOW_LEVEL 20
#endif

// </e> Battery Module Support
//==========================================================

// <e> BLE Module Support
//==========================================================
#ifndef MKB_BLE_ENABLED
#define MKB_BLE_ENABLED 1
#endif

// <e> MKB_BLE_BT1_ENABLED
//----------------------------------------------------
#ifndef MKB_BLE_BT1_ENABLED
#define MKB_BLE_BT1_ENABLED 1
#endif

// <s> MKB_DEVICE_NAME_BT1 - MOKIBO BLE Device Name 1
#define MKB_DEVICE_NAME_BT1 "Mokibo CH1"
//----------------------------------------------------
// </e> MKB_BLE_BT1_ENABLED

// <e> MKB_BLE_BT2_ENABLED
//----------------------------------------------------
#ifndef MKB_BLE_BT2_ENABLED
#define MKB_BLE_BT2_ENABLED 1
#endif

// <s> MKB_DEVICE_NAME_BT2 - MOKIBO BLE Device Name 2
#define MKB_DEVICE_NAME_BT2 "Mokibo CH2"
//----------------------------------------------------
// </e> MKB_BLE_BT2_ENABLED

// <e> MKB_BLE_BT3_ENABLED
//----------------------------------------------------
#ifndef MKB_BLE_BT3_ENABLED
#define MKB_BLE_BT3_ENABLED 1
#endif

// <s> MKB_DEVICE_NAME_BT3 - MOKIBO BLE Device Name 3
#define MKB_DEVICE_NAME_BT3 "Mokibo CH3" // Mokibo Folio CH3
//----------------------------------------------------
// </e> MKB_BLE_BT3_ENABLED

// <o> MKB_BLE_ADDRESS_TYPE
// <0=> Static
// <1=> Random

#ifndef MKB_BLE_ADDRESS_TYPE
#define MKB_BLE_ADDRESS_TYPE 1
#endif

// <o> MKB_PM_BOND_TYPE
// <0=> Just Works
// <1=> Passkey

#ifndef MKB_PM_BOND_TYPE
#define MKB_PM_BOND_TYPE 0
#endif

// <f> BLE SWIFT Pair Support for Microsoft
#ifndef MKB_BLE_SWIFT_PAIR
#define MKB_BLE_SWIFT_PAIR 1
#endif

// <o> MKB_BLE_RADIO_TX_POWER
// <0=> -40dB
// <1=> -30dB
// <2=> -20dB
// <3=> -16dB
// <4=> -12dB
// <5=> -8dB
// <6=> -4dB
// <7=> 0dB
// <8=> 4dB
#ifndef MKB_BLE_RADIO_TX_POWER
#define MKB_BLE_RADIO_TX_POWER 8
#endif

// <e> MKB_BLE_TOUCH_DEVICE_ENABLED
//----------------------------------------------------
#ifndef MKB_BLE_TOUCH_DEVICE_ENABLED
#define MKB_BLE_TOUCH_DEVICE_ENABLED 0
#endif
// <o> MKB_BLE_TOUCH_DEVICE_TYPE
// <0=> TOUCHPAD
// <1=> TOUCHSCREEN
// <2=> PEN
// <3=> POINTER
#ifndef MKB_BLE_TOUCH_DEVICE_TYPE
#define MKB_BLE_TOUCH_DEVICE_TYPE 1
#endif
//----------------------------------------------------
// </e> MKB_BLE_TOUCH_DEVICE_ENABLED

// </e> BLE Module Support
//==========================================================

// <e> Button Module Support
//==========================================================
#ifndef MKB_BUTTON_ENABLED
#define MKB_BUTTON_ENABLED 1
#endif

// </e> Button Module Support
//==========================================================

// <e> COMS Module Support
//==========================================================
#ifndef MKB_COMS_ENABLED
#define MKB_COMS_ENABLED 1
#endif

// </e> COMS Module Support
//==========================================================

// <q> DB Support
#ifndef MKB_DB_ENABLED
#define MKB_DB_ENABLED 1
#endif

// <e> Event Module Support
//==========================================================
#ifndef MKB_EVENT_ENABLED
#define MKB_EVENT_ENABLED 1
#endif

// <e> MKB_EVENT_MONITOR_ENABLED
//----------------------------------------------------
#ifndef MKB_EVENT_MONITOR_ENABLED
#define MKB_EVENT_MONITOR_ENABLED 0
#endif

// <o> MKB_EVENT_MONITOR_TYPES

// <0=> None
// <1=> System
// <2=> Keyboard
// <3=> Touch
// <4=> HID
// <5=> BlueTooth

#ifndef MKB_EVENT_MONITOR_TYPES
#define MKB_EVENT_MONITOR_TYPES 1
#endif
//----------------------------------------------------
// </e> MKB_EVENT_MONITOR_ENABLED

// <o> Event Pool Size - (1~100)
// <1-100>
#ifndef MKB_EVENT_POOL_SIZE
#define MKB_EVENT_POOL_SIZE 30
#endif

// </e> Event Module Support
//==========================================================

// <q> FDS Support
#ifndef MKB_FDS_ENABLED
#define MKB_FDS_ENABLED 1
#endif

// <e> Keyboard Module Support
//==========================================================
#ifndef MKB_KEYBOARD_ENABLED
#define MKB_KEYBOARD_ENABLED 1
#endif

// <o> MKB_KEYBOARD_TYPE
// <0=> MOKIBO
// <1=> IMAGES

#ifndef MKB_KEYBOARD_TYPE
#define MKB_KEYBOARD_TYPE 0
#endif

// <e> enable Keyboard startup check for erase bond
//--------------------------------------
#ifndef MKB_KEYBOARD_STARTUP_ENABLED
#define MKB_KEYBOARD_STARTUP_ENABLED 0
#endif

// <o> Keyboard Startup Key 1
// <0=> None
// <255=> Func

#ifndef MKB_KEYBOARD_STARTUP_KEY_1
#define MKB_KEYBOARD_STARTUP_KEY_1 255
#endif

// <o> Keyboard Startup Key 2
// <0=> None
// <4=> A
// <5=> B
// <6=> C
// <7=> D

#ifndef MKB_KEYBOARD_STARTUP_KEY_2
#define MKB_KEYBOARD_STARTUP_KEY_2 6
#endif

// <o> Keyboard Startup Key 3
// <0=> None
// <4=> A
// <5=> B
// <6=> C
// <7=> D

#ifndef MKB_KEYBOARD_STARTUP_KEY_3
#define MKB_KEYBOARD_STARTUP_KEY_3 0
#endif

// <o> MKB_KEYBOARD_STARTUP_KEY_CNT - (0-3)
// <0-3>

#ifndef MKB_KEYBOARD_STARTUP_KEY_CNT
#define MKB_KEYBOARD_STARTUP_KEY_CNT 2
#endif
//--------------------------------------
// </e> enable Keyboard startup check for erase bond

// </e> Keyboard Module Support
//==========================================================

// <e> LED Module Support
//==========================================================
#ifndef MKB_LED_ENABLED
#define MKB_LED_ENABLED 1
#endif

// <o> MKB_LED_RGB_PIN_1_RED  - LED RGB RED pin number (0~47)
// <0-47>
#ifndef MKB_LED_RGB_PIN_1_RED
#define MKB_LED_RGB_PIN_1_RED 9
#endif

// <o> MKB_LED_RGB_PIN_1_GREEN  - LED RGB GREEN pin number (0~47)
// <0-47>
#ifndef MKB_LED_RGB_PIN_1_GREEN
#define MKB_LED_RGB_PIN_1_GREEN 10
#endif

// <o> MKB_LED_RGB_PIN_1_BLUE  - LED RGB BLUE pin number (0~47)
// <0-47>
#ifndef MKB_LED_RGB_PIN_1_BLUE
#define MKB_LED_RGB_PIN_1_BLUE 42
#endif

// <o> MKB_LED_RGB_PIN_2_RED  - LED RGB RED pin number (0~47)
// <0-47>
#ifndef MKB_LED_RGB_PIN_2_RED
#define MKB_LED_RGB_PIN_2_RED 26
#endif

// <o> MKB_LED_RGB_PIN_2_GREEN  - LED RGB GREEN pin number (0~47)
// <0-47>
#ifndef MKB_LED_RGB_PIN_2_GREEN
#define MKB_LED_RGB_PIN_2_GREEN 4
#endif

// <o> MKB_LED_RGB_PIN_2_BLUE  - LED RGB BLUE pin number (0~47)
// <0-47>
#ifndef MKB_LED_RGB_PIN_2_BLUE
#define MKB_LED_RGB_PIN_2_BLUE 6
#endif

// <o> MKB_LED_RGB_PIN_3_RED  - LED RGB RED pin number (0~47)
// <0-47>
#ifndef MKB_LED_RGB_PIN_3_RED
#define MKB_LED_RGB_PIN_3_RED 8
#endif

// <o> MKB_LED_RGB_PIN_3_GREEN  - LED RGB GREEN pin number (0~47)
// <0-47>
#ifndef MKB_LED_RGB_PIN_3_GREEN
#define MKB_LED_RGB_PIN_3_GREEN 41
#endif

// <o> MKB_LED_RGB_PIN_3_BLUE  - LED RGB BLUE pin number (0~47)
// <0-47>
#ifndef MKB_LED_RGB_PIN_3_BLUE
#define MKB_LED_RGB_PIN_3_BLUE 12
#endif

// <o> MKB_LED_RGB_PIN_4_RED  - LED RGB RED pin number (0~47)
// <0-47>
#ifndef MKB_LED_RGB_PIN_4_RED
#define MKB_LED_RGB_PIN_4_RED 34
#endif

// <o> MKB_LED_RGB_PIN_4_GREEN  - LED RGB GREEN pin number (0~47)
// <0-47>
#ifndef MKB_LED_RGB_PIN_4_GREEN
#define MKB_LED_RGB_PIN_4_GREEN 36
#endif

// <o> MKB_LED_RGB_PIN_4_BLUE  - LED RGB BLUE pin number (0~47)
// <0-47>
#ifndef MKB_LED_RGB_PIN_4_BLUE
#define MKB_LED_RGB_PIN_4_BLUE 38
#endif

// </e> LED Module Support
//==========================================================

// <q> State Manager Support
#ifndef MKB_SM_ENABLED
#define MKB_SM_ENABLED 1
#endif

// <q> SYSTEM Support
#ifndef MKB_SYSTEM_ENABLED
#define MKB_SYSTEM_ENABLED 1
#endif

// <e> Touchpad Module Support
//==========================================================
#ifndef MKB_TOUCHPAD_ENABLED
#define MKB_TOUCHPAD_ENABLED 1
#endif

// <q> Touchpad firmware update Support
#ifndef MKB_TOUCHPAD_UPDATE_ENABLED
#define MKB_TOUCHPAD_UPDATE_ENABLED 0
#endif

// <q> Touchpad Test Mode Support
#ifndef MKB_TOUCHPAD_TEST_ENABLED
#define MKB_TOUCHPAD_TEST_ENABLED 0
#endif

// <o> Minimum pixel to move [pixel] - (0~10)
// <0-10>
#ifndef MKB_TOUCHPAD_MOVE_MINIMUM
#define MKB_TOUCHPAD_MOVE_MINIMUM 1
#endif

// <o> TAP detect threshold minimum time [ms] - (1~100)
// <1-100>
#ifndef MKB_TOUCHPAD_TAP_THRESHOLD_TIME_MIN
#define MKB_TOUCHPAD_TAP_THRESHOLD_TIME_MIN 60
#endif

// <o> TAP detect threshold maximum time [ms] - (1~300)
// <1-300>
#ifndef MKB_TOUCHPAD_TAP_THRESHOLD_TIME_MAX
#define MKB_TOUCHPAD_TAP_THRESHOLD_TIME_MAX 180
#endif

// <o> TAP detect threshold move - (1~100)
// <1-100>
#ifndef MKB_TOUCHPAD_TAP_THRESHOLD_MOVE
#define MKB_TOUCHPAD_TAP_THRESHOLD_MOVE 10
#endif

// <o> TAP detect threshold pressure - (1~30)
// <1-30>
#ifndef MKB_TOUCHPAD_TAP_THRESHOLD_PRESSURE
#define MKB_TOUCHPAD_TAP_THRESHOLD_PRESSURE 18
#endif

// <o> TOUCHPAD Move Speed level - (0~100)%
// <0-100>
#ifndef MKB_TOUCHPAD_MOVE_SPEED
#define MKB_TOUCHPAD_MOVE_SPEED 70
#endif

// <o> TOUCHPAD Move Speed threshold [pixel] - (1~100)
// <i> If the mouse movement distance exceeds the MKB_TOUCHPAD_MOVE_SPEED_THRESHOLD,
// <i> the MKB_TOUCHPAD_MOVE_SPEED is applied.
// <1-100>
#ifndef MKB_TOUCHPAD_MOVE_SPEED_THRESHOLD
#define MKB_TOUCHPAD_MOVE_SPEED_THRESHOLD 20
#endif

// <o> TOUCHPAD Scroll Speed level - (0~100)%
// <1-100>
#ifndef MKB_TOUCHPAD_SCROLL_SPEED
#define MKB_TOUCHPAD_SCROLL_SPEED 50
#endif

// </e> Touchpad Module Support
//==========================================================

// <e> USB Module Support
//==========================================================
#ifndef MKB_USB_ENABLED
#define MKB_USB_ENABLED 1
#endif

// </e> USB Module Support
//==========================================================

// <e> Watch Dog Module Support
//==========================================================
#ifndef MKB_WDT_ENABLED
#define MKB_WDT_ENABLED 1
#endif

// </e> Watch Dog Module Support
//==========================================================

// </h> Modules Related Options
//==========================================================

// <h> CLI Related Options
//==========================================================

// <q> MKB_CLI_ENABLED: Enable CLI Support
#ifndef MKB_CLI_ENABLED
#define MKB_CLI_ENABLED 1
#endif

// <o> CLI Terminal ID

// <0=> Terminal 0
// <1=> Terminal 1

#ifndef MKB_CLI_TERMINAL_ID
#define MKB_CLI_TERMINAL_ID 1
#endif

// </h> CLI Related Options
//==========================================================

// <h> MOKIBO Log
// <i> This section configures module-specific logging options.
//==========================================================

// <e> MKB_LOG_ENABLED: Enable Logging Support
//==========================================================
#ifndef MKB_LOG_ENABLED
#define MKB_LOG_ENABLED 1
#endif

// <o> LOG Terminal ID
// <0=> Terminal 0
// <1=> Terminal 1

#ifndef MKB_LOG_TERMINAL_ID
#define MKB_LOG_TERMINAL_ID 0
#endif

// <o> Default Logging level for MOKIBO
// <0=> None
// <1=> Error
// <2=> Warning
// <3=> Info
// <4=> Debug

#ifndef MKB_LOG_DEFAULT_LEVEL
#define MKB_LOG_DEFAULT_LEVEL 1
#endif

// <e> Debug Trace module Configuration
// <o> Boot Logging level
// <0=> disable
// <1=> enable
#ifndef MKB_DEBUG_LOG_ENABLE
#define MKB_DEBUG_LOG_ENABLE 1
#endif

// <e> Log module Configuration
// <0=> None
// <1=> Error
// <2=> Warning
// <3=> Info
// <4=> Debug
// <5=> Default
#ifndef MKB_BOOTING_LOG_LEVEL
#define MKB_BOOTING_LOG_LEVEL 3
#endif

// <o> Battery Logging level
// <0=> None
// <1=> Error
// <2=> Warning
// <3=> Info
// <4=> Debug
// <5=> Default

#ifndef MKB_BATTERY_LOG_LEVEL
#define MKB_BATTERY_LOG_LEVEL 5
#endif

// <o> BLE Logging level
// <0=> None
// <1=> Error
// <2=> Warning
// <3=> Info
// <4=> Debug
// <5=> Default

#ifndef MKB_BLE_LOG_LEVEL
#define MKB_BLE_LOG_LEVEL 5
#endif

// <o> BLE ADV Logging level
// <0=> None
// <1=> Error
// <2=> Warning
// <3=> Info
// <4=> Debug
// <5=> Default

#ifndef MKB_BLE_ADV_LOG_LEVEL
#define MKB_BLE_ADV_LOG_LEVEL 5
#endif

// <o> Button Logging level
// <0=> None
// <1=> Error
// <2=> Warning
// <3=> Info
// <4=> Debug
// <5=> Default

#ifndef MKB_BUTTON_LOG_LEVEL
#define MKB_BUTTON_LOG_LEVEL 5
#endif

// <o> CLI Logging level
// <0=> None
// <1=> Error
// <2=> Warning
// <3=> Info
// <4=> Debug
// <5=> Default

#ifndef MKB_CLI_LOG_LEVEL
#define MKB_CLI_LOG_LEVEL 5
#endif

// <o> COMS Logging level
// <0=> None
// <1=> Error
// <2=> Warning
// <3=> Info
// <4=> Debug
// <5=> Default

#ifndef MKB_COMS_LOG_LEVEL
#define MKB_COMS_LOG_LEVEL 5
#endif

// <o> CYPRESS Logging level
// <0=> None
// <1=> Error
// <2=> Warning
// <3=> Info
// <4=> Debug
// <5=> Default

#ifndef MKB_CYPRESS_LOG_LEVEL
#define MKB_CYPRESS_LOG_LEVEL 5
#endif

// <o> DB Logging level
// <0=> None
// <1=> Error
// <2=> Warning
// <3=> Info
// <4=> Debug
// <5=> Default

#ifndef MKB_DB_LOG_LEVEL
#define MKB_DB_LOG_LEVEL 5
#endif

// <o> DFU Logging level
// <0=> None
// <1=> Error
// <2=> Warning
// <3=> Info
// <4=> Debug
// <5=> Default

#ifndef MKB_DFU_LOG_LEVEL
#define MKB_DFU_LOG_LEVEL 5
#endif

// <o> Event Logging level
// <0=> None
// <1=> Error
// <2=> Warning
// <3=> Info
// <4=> Debug
// <5=> Default

#ifndef MKB_EVENT_LOG_LEVEL
#define MKB_EVENT_LOG_LEVEL 5
#endif

// <o> FDS Logging level
// <0=> None
// <1=> Error
// <2=> Warning
// <3=> Info
// <4=> Debug
// <5=> Default

#ifndef MKB_FDS_LOG_LEVEL
#define MKB_FDS_LOG_LEVEL 5
#endif

// <o> Init Logging level
// <0=> None
// <1=> Error
// <2=> Warning
// <3=> Info
// <4=> Debug
// <5=> Default

#ifndef MKB_INIT_LOG_LEVEL
#define MKB_INIT_LOG_LEVEL 5
#endif

// <o> IST40 Touch Sensor Logging level
// <0=> None
// <1=> Error
// <2=> Warning
// <3=> Info
// <4=> Debug
// <5=> Default

#ifndef MKB_IST40_LOG_LEVEL
#define MKB_IST40_LOG_LEVEL 5
#endif

// <o> Keyboard Logging level
// <0=> None
// <1=> Error
// <2=> Warning
// <3=> Info
// <4=> Debug
// <5=> Default

#ifndef MKB_KEYBOARD_LOG_LEVEL
#define MKB_KEYBOARD_LOG_LEVEL 5
#endif

// <o> Keymatrix - Logging level
// <0=> None
// <1=> Error
// <2=> Warning
// <3=> Info
// <4=> Debug
// <5=> Default

#ifndef MKB_KEYMATRIX_LOG_LEVEL
#define MKB_KEYMATRIX_LOG_LEVEL 5
#endif

// <o> LED Logging level
// <0=> None
// <1=> Error
// <2=> Warning
// <3=> Info
// <4=> Debug
// <5=> Default

#ifndef MKB_LED_LOG_LEVEL
#define MKB_LED_LOG_LEVEL 5
#endif

// <o> PIXART Logging level
// <0=> None
// <1=> Error
// <2=> Warning
// <3=> Info
// <4=> Debug
// <5=> Default

#ifndef MKB_PIXART_LOG_LEVEL
#define MKB_PIXART_LOG_LEVEL 5
#endif

// <o> STATE_MANAGER Logging level
// <0=> None
// <1=> Error
// <2=> Warning
// <3=> Info
// <4=> Debug
// <5=> Default

#ifndef MKB_SM_LOG_LEVEL
#define MKB_SM_LOG_LEVEL 5
#endif

// <o> SYSTEM Logging level
// <0=> None
// <1=> Error
// <2=> Warning
// <3=> Info
// <4=> Debug
// <5=> Default

#ifndef MKB_SYSTEM_LOG_LEVEL
#define MKB_SYSTEM_LOG_LEVEL 5
#endif

// <o> TOUCHPAD Logging level
// <0=> None
// <1=> Error
// <2=> Warning
// <3=> Info
// <4=> Debug
// <5=> Default

#ifndef MKB_TOUCHPAD_LOG_LEVEL
#define MKB_TOUCHPAD_LOG_LEVEL 5
#endif

// <o> USB Logging level
// <0=> None
// <1=> Error
// <2=> Warning
// <3=> Info
// <4=> Debug
// <5=> Default

#ifndef MKB_USB_LOG_LEVEL
#define MKB_USB_LOG_LEVEL 5
#endif

// <o> UTIL Logging level
// <0=> None
// <1=> Error
// <2=> Warning
// <3=> Info
// <4=> Debug
// <5=> Default

#ifndef MKB_UTIL_LOG_LEVEL
#define MKB_UTIL_LOG_LEVEL 5
#endif

// <o> WDT Logging level
// <0=> None
// <1=> Error
// <2=> Warning
// <3=> Info
// <4=> Debug
// <5=> Default

#ifndef MKB_WDT_LOG_LEVEL
#define MKB_WDT_LOG_LEVEL 5
#endif

// </e> Log module Configuration
//==========================================================

// </e> MKB_LOG_ENABLED
//==========================================================

// </h> MOKIBO Log
//==========================================================

// <<< end of configuration section >>>
#endif // MKB_CONFIG_H
