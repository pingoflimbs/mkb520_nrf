/*
* ===============================================================================
* Copyright (C) 2017-2019
* INNOPRESSO INC. All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
* ===============================================================================
*/

#include "sdk_common.h"
#include "app_timer.h"

#include "nrf_atomic.h"
#include "nrf_assert.h"
#include "nrf_log.h"
#include "nrf_delay.h"

#include "mkb_config.h"
#include "drv_twi.h"
#include "am2320.h"

#if (defined(MKB_AM2320_ENABLED) && MKB_AM2320_ENABLED)

APP_TIMER_DEF(m_am2320_timer);

static bool m_am2320_enabled;

/**@brief Variable protecting shared data used in read operation */
static nrf_atomic_flag_t m_am2320_read_operation_active;

static uint8_t m_am2320_buffer[AM2320_BUFFER_SIZE];

static float m_am2320_temperature;
static float m_am2320_humidity;

static am2320_read_temp_handler_t m_am2320_read_temp_handler;
static am2320_read_humi_handler_t m_am2320_read_humi_handler;

static uint8_t NRF_TWI_MNGR_BUFFER_LOC_IND am2320_cmd_wakeup[] 	= { AM2320_CMD_WAKEUP,  };
static uint8_t NRF_TWI_MNGR_BUFFER_LOC_IND am2320_cmd_humi[] 	= { AM2320_CMD_READ, AM2320_REG_HUMI, 2 };
static uint8_t NRF_TWI_MNGR_BUFFER_LOC_IND am2320_cmd_temp[] 	= { AM2320_CMD_READ, AM2320_REG_TEMP, 2 };
static uint8_t NRF_TWI_MNGR_BUFFER_LOC_IND am2320_cmd_all[] 	= { AM2320_CMD_READ, AM2320_REG_HUMI, 4 };

static nrf_twi_mngr_transfer_t const am2320_wakeup_transfers[] =
{
	NRF_TWI_MNGR_WRITE(AM2320_ADDR, am2320_cmd_wakeup, sizeof(am2320_cmd_wakeup), 0),
};
static nrf_twi_mngr_transfer_t const am2320_humi_transfers[] =
{
	NRF_TWI_MNGR_WRITE(AM2320_ADDR, am2320_cmd_humi, sizeof(am2320_cmd_humi), 0),
	NRF_TWI_MNGR_READ (AM2320_ADDR, &m_am2320_buffer[0],   6, 0)
};
static nrf_twi_mngr_transfer_t const am2320_temp_transfers[] =
{
	NRF_TWI_MNGR_WRITE(AM2320_ADDR, am2320_cmd_temp, sizeof(am2320_cmd_temp), 0),
	NRF_TWI_MNGR_READ (AM2320_ADDR, &m_am2320_buffer[0],   6, 0)
};
static nrf_twi_mngr_transfer_t const am2320_all_transfers[] =
{
	NRF_TWI_MNGR_WRITE(AM2320_ADDR, am2320_cmd_all, sizeof(am2320_cmd_all), 0),
	NRF_TWI_MNGR_READ (AM2320_ADDR, &m_am2320_buffer[0],   8, 0)
};

/**@brief Transaction for gyroscope waking up  */
static const nrf_twi_mngr_transaction_t am2320_wakeup_transaction =
{
	.callback            = NULL,
    .p_user_data         = NULL,
    .p_transfers         = am2320_wakeup_transfers,
    .number_of_transfers = sizeof(am2320_wakeup_transfers) / sizeof(am2320_wakeup_transfers[0])
};

/**@brief TWI transaction structure used in read operation */
static nrf_twi_mngr_transaction_t am2320_temp_transaction;
static nrf_twi_mngr_transaction_t am2320_humi_transaction;
static nrf_twi_mngr_transaction_t am2320_all_transaction;

static uint16_t am2320_crc16(uint8_t *buffer, uint8_t nbytes)
{
	uint16_t crc = 0xffff;
	
	for (int i=0; i<nbytes; i++)
	{
		uint8_t b = buffer[i];
		crc ^= b;
		for (int x=0; x<8; x++)
		{
			if (crc & 0x0001)
			{
				crc >>= 1;
				crc ^= 0xA001;
			} else
			{
				crc >>= 1;
			}
		}
	}
	return crc;
}

static void am2320_wakeup(void)
{
    APP_ERROR_CHECK(twi_schedule(&am2320_wakeup_transaction));
}

static void am2320_read_temperature_cb(ret_code_t result, void * p_user_data)
{
	float *p_temp = p_user_data;
	uint16_t t_samples;
	
	if (result == NRF_SUCCESS)
	{
		uint16_t the_crc = AM2320_GET_CRC_VALUE(m_am2320_buffer[5], m_am2320_buffer[4]);
		uint16_t calc_crc = am2320_crc16(m_am2320_buffer, 4); // preamble + data
		if (the_crc != calc_crc)
		{
			result = NRF_ERROR_INTERNAL;
			return;
		}
		
		//NRF_LOG_DEBUG("AM2320 T: 0x%02x%02x", m_am2320_buffer[2], m_am2320_buffer[3]);
		
		t_samples = AM2320_GET_TEMPERATURE_VALUE(m_am2320_buffer[2], m_am2320_buffer[3]);
		
		if(t_samples & 0x8000)
			*p_temp = (t_samples / 10.0) * -1;
		else
			*p_temp = (t_samples / 10.0);
	
		if(m_am2320_read_temp_handler)
			m_am2320_read_temp_handler(result, p_user_data);
	}
	
	nrf_atomic_flag_clear(&m_am2320_read_operation_active);
}

ret_code_t am2320_read_temperature(void)
{
	ret_code_t status;
	
    if (nrf_atomic_flag_set_fetch(&m_am2320_read_operation_active))
    {
        return NRF_ERROR_BUSY;
    }
	
    am2320_temp_transaction.callback             = am2320_read_temperature_cb;
    am2320_temp_transaction.p_user_data          = &m_am2320_temperature;
    am2320_temp_transaction.p_transfers          = am2320_temp_transfers;
    am2320_temp_transaction.number_of_transfers  = ARRAY_SIZE(am2320_temp_transfers);
	
	am2320_wakeup();	
	nrf_delay_ms(2);
	
	status = twi_schedule(&am2320_temp_transaction);
	
	if(status != NRF_SUCCESS)
		nrf_atomic_flag_clear(&m_am2320_read_operation_active);
	
	return status;
}

static void am2320_read_humidity_cb(ret_code_t result, void * p_user_data)
{
	float *p_humi = p_user_data;
	uint16_t h_samples;
	
	if (result == NRF_SUCCESS)
	{
		uint16_t the_crc = AM2320_GET_CRC_VALUE(m_am2320_buffer[5], m_am2320_buffer[4]);
		uint16_t calc_crc = am2320_crc16(m_am2320_buffer, 4); // preamble + data
		if (the_crc != calc_crc)
		{
			result = NRF_ERROR_INTERNAL;
			return;
		}
		
		//NRF_LOG_DEBUG("AM2320 H: 0x%02x%02x", m_am2320_buffer[2], m_am2320_buffer[3]);
		
		h_samples = AM2320_GET_HUMIDITY_VALUE(m_am2320_buffer[2], m_am2320_buffer[3]);
		*p_humi = (h_samples / 10.0);
	
		if(m_am2320_read_humi_handler)
			m_am2320_read_humi_handler(result, p_user_data);
	}
	
	nrf_atomic_flag_clear(&m_am2320_read_operation_active);
}

ret_code_t am2320_read_humidity(void)
{
	ret_code_t status;
	
    if (nrf_atomic_flag_set_fetch(&m_am2320_read_operation_active))
    {
        return NRF_ERROR_BUSY;
    }

    am2320_humi_transaction.callback             = am2320_read_humidity_cb;
    am2320_humi_transaction.p_user_data          = &m_am2320_humidity;
    am2320_humi_transaction.p_transfers          = am2320_humi_transfers;
    am2320_humi_transaction.number_of_transfers  = ARRAY_SIZE(am2320_humi_transfers);
	
	am2320_wakeup();	
	nrf_delay_ms(2);
	
	status = twi_schedule(&am2320_humi_transaction);
	
	if(status != NRF_SUCCESS)
		nrf_atomic_flag_clear(&m_am2320_read_operation_active);
	
	return status;
}

static void am2320_read_all_cb(ret_code_t result, void * p_user_data)
{
	float f_temp, f_humi;
	uint16_t t_samples, h_samples;
	
	if (result == NRF_SUCCESS)
	{
		uint16_t the_crc = AM2320_GET_CRC_VALUE(m_am2320_buffer[7], m_am2320_buffer[6]);
		uint16_t calc_crc = am2320_crc16(m_am2320_buffer, 6); // preamble + data
		if (the_crc != calc_crc)
		{
			result = NRF_ERROR_INTERNAL;
			return;
		}
		
		//NRF_LOG_DEBUG("AM2320 H: 0x%02x%02x", m_am2320_buffer[2], m_am2320_buffer[3]);
		
		h_samples = AM2320_GET_HUMIDITY_VALUE(m_am2320_buffer[2], m_am2320_buffer[3]);
		f_humi = (h_samples / 10.0);
		
		t_samples = AM2320_GET_TEMPERATURE_VALUE(m_am2320_buffer[4], m_am2320_buffer[5]);
		if(t_samples & 0x8000)
			f_temp = (t_samples / 10.0) * -1;
		else
			f_temp = (t_samples / 10.0);
	
		if(m_am2320_read_temp_handler)
			m_am2320_read_temp_handler(result, &f_temp);
		
		if(m_am2320_read_humi_handler)
			m_am2320_read_humi_handler(result, &f_humi);
	}
	
	nrf_atomic_flag_clear(&m_am2320_read_operation_active);
}

ret_code_t am2320_read_all(void)
{
	ret_code_t status;
	
    if (nrf_atomic_flag_set_fetch(&m_am2320_read_operation_active))
    {
        return NRF_ERROR_BUSY;
    }
	
    am2320_all_transaction.callback             = am2320_read_all_cb;
    am2320_all_transaction.p_user_data          = NULL;
    am2320_all_transaction.p_transfers          = am2320_all_transfers;
    am2320_all_transaction.number_of_transfers  = ARRAY_SIZE(am2320_all_transfers);
	
	am2320_wakeup();	
	nrf_delay_ms(2);
	
	status = twi_schedule(&am2320_all_transaction);
	
	if(status != NRF_SUCCESS)
		nrf_atomic_flag_clear(&m_am2320_read_operation_active);
	
	return status;
}

static void am2320_timer_handler(void * p_context)
{
	am2320_read_all();
}

void am2320_enable(void)
{
    ret_code_t err_code;

	if(!m_am2320_enabled)
	{
		err_code = app_timer_start(m_am2320_timer, APP_TIMER_TICKS(AM2320_TIMER_INTERVAL), NULL);
		APP_ERROR_CHECK(err_code);
		
		m_am2320_enabled = true;
	}
}

void am2320_disable(void)
{
    ret_code_t err_code;

	if(m_am2320_enabled)
	{
		err_code = app_timer_stop(m_am2320_timer);
		APP_ERROR_CHECK(err_code);
		
		m_am2320_enabled = false;
	}
}

void am2320_init(am2320_read_temp_handler_t temp_handler, am2320_read_humi_handler_t humi_handler)
{
    ret_code_t err_code;
	
    ASSERT(temp_handler != NULL);
    ASSERT(humi_handler != NULL);

	for(uint8_t i = 0; i < AM2320_BUFFER_SIZE; i++)
		m_am2320_buffer[i] = 0;

    err_code = app_timer_create(&m_am2320_timer, APP_TIMER_MODE_REPEATED, am2320_timer_handler);
    APP_ERROR_CHECK(err_code);
	
    // Initialize driver state.
    nrf_atomic_flag_clear(&m_am2320_read_operation_active);
	
	m_am2320_read_temp_handler = temp_handler;
	m_am2320_read_humi_handler = humi_handler;

	m_am2320_enabled = false;
}

#endif // (defined(MKB_AM2320_ENABLED) && MKB_AM2320_ENABLED)
