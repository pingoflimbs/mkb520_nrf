/*
* ===============================================================================
* Copyright (C) 2017-2019
* INNOPRESSO INC. All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
* ===============================================================================
*/

#ifndef AM2320_H__
#define AM2320_H__


#include "nrf_twi_mngr.h"

#ifdef __cplusplus
extern "C" {
#endif


// 0xB8 is the AM2320's address, it contains
// R/W bit and "nrf_drv_twi" (and consequently "nrf_twi_mngr") requires slave
// address without this bit, hence shifting.
#define AM2320_ADDR         (0xB8U >> 1)	// 0x5C

#define AM2320_CMD_WAKEUP   0x00
#define AM2320_CMD_READ     0x03

#define AM2320_REG_HUMI		0x00
#define AM2320_REG_TEMP     0x02
	
#define AM2320_BUFFER_SIZE		8
#define AM2320_TIMER_INTERVAL	1000	// 1sec

#define AM2320_GET_CRC_VALUE(crc_hi, crc_lo) \
    ((((int16_t)crc_hi << 8) | crc_lo))
#define AM2320_GET_TEMPERATURE_VALUE(temp_hi, temp_lo) \
    ((((int16_t)temp_hi << 8) | temp_lo))
#define AM2320_GET_HUMIDITY_VALUE(hum_hi, hum_lo) \
    ((((int16_t)hum_hi << 8) | hum_lo))

/**@brief AM2320 enabled handler */
typedef void (*am2320_ready_handler_t)(void);

/**@brief AM2320 read data handler */
typedef void (*am2320_read_temp_handler_t)(ret_code_t status, void *p_samples);
typedef void (*am2320_read_humi_handler_t)(ret_code_t status, void *p_samples);

extern ret_code_t am2320_read_temperature(void);
extern ret_code_t am2320_read_humidity(void);
extern void am2320_enable(void);
extern void am2320_disable(void);
extern void am2320_init(am2320_read_temp_handler_t temp_handler, am2320_read_humi_handler_t humi_handler);

#ifdef __cplusplus
}
#endif

#endif // AM2320_H__
