/*
* ===============================================================================
* Copyright (C) 2017-2019
* INNOPRESSO INC. All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
* ===============================================================================
*/

#ifndef __DRV_TWIS_H__
#define __DRV_TWIS_H__

#include <stdbool.h>
#include <stdint.h>

#include "nrf.h"
#include "nrf_drv_twis.h"

/**
* @defgroup TWI_COMMON TWI common procedures
* @{
* @ingroup other
* @brief Common TWI functions.
*/
#define MKB_TWIS_INST 1
#define MKB_TWIS_ADDR 0x08
#define TWIS_BUF_MAX_BYTES 256

/**@brief Initialize TWI Slave.
 *
 * Function fills in bus configuration with correct data and initializes TWI
 *
 * @return NRF_SUCCESS on success, otherwise an error code.
 */
ret_code_t twis_init(void);

bool twis_error_check(void);
uint32_t twis_error_get_and_clear(void);

/**
 *@}
 **/

#endif /* __DRV_TWIS_H__ */
