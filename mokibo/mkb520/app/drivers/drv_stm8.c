/*
* ===============================================================================
* Copyright (C) 2017-2019
* INNOPRESSO INC. All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
* ===============================================================================
*/

/*\      $$\  $$$$$$\  $$$$$$$\  $$\   $$\ $$\       $$$$$$$$\  $$$$$$\  
$$$\    $$$ |$$  __$$\ $$  __$$\ $$ |  $$ |$$ |      $$  _____|$$  __$$\ 
$$$$\  $$$$ |$$ /  $$ |$$ |  $$ |$$ |  $$ |$$ |      $$ |      $$ /  \__|
$$\$$\$$ $$ |$$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |      $$$$$\    \$$$$$$\  
$$ \$$$  $$ |$$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |      $$  __|    \____$$\ 
$$ |\$  /$$ |$$ |  $$ |$$ |  $$ |$$ |  $$ |$$ |      $$ |      $$\   $$ |
$$ | \_/ $$ | $$$$$$  |$$$$$$$  |\$$$$$$  |$$$$$$$$\ $$$$$$$$\ \$$$$$$  |
\__|     \__| \______/ \_______/  \______/ \________|\________| \______/ 
//============================================================================//
//                              LOCAL MODULEL USED                            //
//============================================================================*/
#include "sdk_common.h"
#include "app_error.h"
#include "app_timer.h"
#include "app_scheduler.h"
#if defined(GPIOTE_ENABLED) && GPIOTE_ENABLED
#include "app_gpiote.h"
#endif

#include "nrf_atomic.h"
#include "nrf_assert.h"
#include "nrf_error.h"
#include "nrf_gpio.h"
#include "nrf_log.h"
#include "nrf_delay.h"
#include "nrf_pwr_mgmt.h"

#define MKB_LOG_LEVEL MKB_STM8_LOG_LEVEL
#include "mkb_common.h"
#include "drv_twi.h"
#include "drv_stm8.h"

#if (defined(MKB_STM8_ENABLED) && MKB_STM8_ENABLED)

APP_TIMER_DEF(m_stm8_timer_id);

#if (STM8_MODE == STM8_MODE_GPIOTE)
static app_gpiote_user_id_t m_stm8_gpiote; //!< GPIOTE Handle.
#endif

STM8_STATE_t m_stm8_state;

static STM8_TOUCH_t m_stm8_touch;
static STM8_BUTTON_t m_stm8_button;
static STM8_CENTER_t m_stm8_center;
static STM8_TOUCH_t m_stm8_state_touch;
static STM8_BUTTON_t m_stm8_state_button;
static STM8_CENTER_t m_stm8_state_center;

static bool m_stm8_enabled;
static uint8_t m_stm8_rdbuff;
static uint8_t m_stm8_wrbuff;

static nrf_atomic_flag_t m_stm8_operation_active;

static stm8_read_handler_t m_stm8_read_handler;

static uint8_t NRF_TWI_MNGR_BUFFER_LOC_IND stm8_cmd_read_register[] =
	{
		STM8_READ_CMD,
		STM8_READ_CMD};

static uint8_t NRF_TWI_MNGR_BUFFER_LOC_IND stm8_cmd_write_register[] =
	{
		STM8_WRITE_CMD,
		STM8_WRITE_CMD};

static uint8_t NRF_TWI_MNGR_BUFFER_LOC_IND stm8_cmd_touch_left_register[] =
	{
		STM8_READ_CMD,
		STM8_READ_TOUCH_LEFT << 3};
static uint8_t NRF_TWI_MNGR_BUFFER_LOC_IND stm8_cmd_touch_right_register[] =
	{
		STM8_READ_CMD,
		STM8_READ_TOUCH_RIGHT << 3};
static uint8_t NRF_TWI_MNGR_BUFFER_LOC_IND stm8_cmd_button_left_register[] =
	{
		STM8_READ_CMD,
		STM8_READ_BUTTON_LEFT << 3};
static uint8_t NRF_TWI_MNGR_BUFFER_LOC_IND stm8_cmd_button_right_register[] =
	{
		STM8_READ_CMD,
		STM8_READ_BUTTON_RIGHT << 3};
static uint8_t NRF_TWI_MNGR_BUFFER_LOC_IND stm8_cmd_center_left_register[] =
	{
		STM8_READ_CMD,
		STM8_READ_TOUCH_CENTER_LEFT << 3};
static uint8_t NRF_TWI_MNGR_BUFFER_LOC_IND stm8_cmd_center_right_register[] =
	{
		STM8_READ_CMD,
		STM8_READ_TOUCH_CENTER_RIGHT << 3};

static nrf_twi_mngr_transfer_t const stm8_cmd_read_transfers[] =
	{
		NRF_TWI_MNGR_WRITE(STM8_ADDR, stm8_cmd_read_register, sizeof(stm8_cmd_read_register), 0),
		NRF_TWI_MNGR_READ(STM8_ADDR, &m_stm8_rdbuff, 1, 0)};
static nrf_twi_mngr_transfer_t const stm8_cmd_write_transfers[] =
	{
		NRF_TWI_MNGR_WRITE(STM8_ADDR, stm8_cmd_write_register, sizeof(stm8_cmd_write_register), 0)};
static nrf_twi_mngr_transfer_t stm8_read_state_transfers[] =
	{
		NRF_TWI_MNGR_WRITE(STM8_ADDR, stm8_cmd_touch_left_register, sizeof(stm8_cmd_touch_left_register), 0),
		NRF_TWI_MNGR_READ(STM8_ADDR, &m_stm8_touch.left, 1, 0),
		NRF_TWI_MNGR_WRITE(STM8_ADDR, stm8_cmd_touch_right_register, sizeof(stm8_cmd_touch_right_register), 0),
		NRF_TWI_MNGR_READ(STM8_ADDR, &m_stm8_touch.right, 1, 0),
		//NRF_TWI_MNGR_WRITE(STM8_ADDR, stm8_cmd_center_left_register, sizeof(stm8_cmd_center_left_register), 0),
		//NRF_TWI_MNGR_READ (STM8_ADDR, &m_stm8_center.left,   1, 0),
		//NRF_TWI_MNGR_WRITE(STM8_ADDR, stm8_cmd_center_right_register, sizeof(stm8_cmd_center_right_register), 0),
		//NRF_TWI_MNGR_READ (STM8_ADDR, &m_stm8_center.right,   1, 0),
		NRF_TWI_MNGR_WRITE(STM8_ADDR, stm8_cmd_button_right_register, sizeof(stm8_cmd_button_right_register), 0),
		NRF_TWI_MNGR_READ(STM8_ADDR, &m_stm8_button.right, 1, 0),
		NRF_TWI_MNGR_WRITE(STM8_ADDR, stm8_cmd_button_left_register, sizeof(stm8_cmd_button_left_register), 0),
		NRF_TWI_MNGR_READ(STM8_ADDR, &m_stm8_button.left, 1, 0),
};

static void stm8_read_state_cb(ret_code_t result, void *p_user_data);

/**@brief TWI transaction structure used in read operation */
static nrf_twi_mngr_transaction_t stm8_read_state_transaction =
	{
		.callback = stm8_read_state_cb,
		.p_user_data = NULL,
		.p_transfers = stm8_read_state_transfers,
		.number_of_transfers = ARRAY_SIZE(stm8_read_state_transfers),
		//.p_required_twi_cfg  = &g_twi_bus_config[CONFIG_TOUCHPAD_TWI_BUS]
};
static nrf_twi_mngr_transaction_t stm8_cmd_write_transaction =
	{
		.callback = NULL,
		.p_user_data = NULL,
		.p_transfers = stm8_cmd_write_transfers,
		.number_of_transfers = ARRAY_SIZE(stm8_cmd_write_transfers),
		//.p_required_twi_cfg  = &g_twi_bus_config[CONFIG_TOUCHPAD_TWI_BUS]
};

#if (STM8_MODE == STM8_MODE_GPIOTE)
/**@brief Enable interrupt-based key press sensing. */
static ret_code_t stm8_sense_enable(void)
{
	// Enable GPIOTE sensing.
	return app_gpiote_user_enable(m_stm8_gpiote);
}

/**@brief Disable interrupt-based key press sensing. */
static ret_code_t stm8_sense_disable(void)
{
	// Disable GPIOTE sensing.
	return app_gpiote_user_disable(m_stm8_gpiote);
}
#endif /* (STM8_MODE == STM8_MODE_GPIOTE) */

//-----------------------------------------------------------------------------------------------------------------------------------
//			drv_stm8_write
//-----------------------------------------------------------------------------------------------------------------------------------
ret_code_t drv_stm8_write(STM8_WRITE_CMD_t cmd, uint8_t value)
{
	uint8_t data = STM8_WRITE_CMD;
	ret_code_t err_code = NRF_SUCCESS;

	switch (cmd)
	{
	case STM8_WRITE_NO_OPERATION:
		break;

	case STM8_WRITE_FW_CMD:
		if (value >= STM8_FW_STATUS_MAX)
		{
			MKB_LOG_ERROR("Error! value param[%d]", value);
			err_code = NRF_ERROR_INVALID_PARAM;
		}
		break;

	case STM8_WRITE_LED_CUSTOM_RED:
	case STM8_WRITE_LED_CUSTOM_GREEN:
	case STM8_WRITE_LED_CUSTOM_BLUE:
		if (value > 0x07) // 3bit value
		{
			MKB_LOG_ERROR("Error! value param[%d]", value);
			err_code = NRF_ERROR_INVALID_PARAM;
		}
		break;

	case STM8_WRITE_LED_CUSTOM_NIBBLE:
		if (value > 2)
		{
			MKB_LOG_ERROR("Error! value param[%d]", value);
			err_code = NRF_ERROR_INVALID_PARAM;
		}
		break;

	case STM8_WRITE_LED_COLOR:
		if (value >= STM8_LED_COLOR_MAX)
		{
			MKB_LOG_ERROR("Error! value param[%d]", value);
			err_code = NRF_ERROR_INVALID_PARAM;
		}
		break;

	case STM8_WRITE_LED_STATUS:
		if (value >= STM8_LED_STATUS_MAX)
		{
			MKB_LOG_ERROR("Error! value param[%d]", value);
			err_code = NRF_ERROR_INVALID_PARAM;
		}
		break;

	case STM8_WRITE_RESET_ME:
		if (value > 7)
		{
			MKB_LOG_ERROR("Error! value param[%d]", value);
			err_code = NRF_ERROR_INVALID_PARAM;
		}
		break;

	case STM8_WRITE_LED_BR:
		if (value > 7)
		{
			MKB_LOG_ERROR("Error! value param[%d]", value);
			err_code = NRF_ERROR_INVALID_PARAM;
		}
		break;

	case STM8_WRITE_BATTERY_LEVEL_UPDATE:
		// Updaet with any value
		break;

	default:
		MKB_LOG_ERROR("Error! invalid command[%d]", cmd);
		err_code = NRF_ERROR_NOT_FOUND;
		break;
	}

	if (NRF_SUCCESS == err_code)
	{
		char *string[] =
			{
				"NO_OPERATION",
				"FW_CMD",
				"LED_CUSTOM_RED",
				"LED_CUSTOM_GREEN",
				"LED_CUSTOM_BLUE",
				"LED_CUSTOM_NIBBLE",
				"LED_COLOR",
				"LED_STATUS",
				"RESET_ME",
				"LED_BRIGHTNESS",
				"BATTERY_LEVEL_UPDATE",
			};

		data |= ((uint8_t)cmd) << 3;
		data |= (value & 0x07);
		//stm8_cmd_write_register[0] = data;
		stm8_cmd_write_register[1] = data;

		MKB_LOG_DEBUG("STM8 Write[0x%02x] CMD[%s] VAl[%d] is sent", stm8_cmd_write_register[1], string[cmd], value);

		err_code = twi_schedule(&stm8_cmd_write_transaction);
		nrf_delay_ms(1);
	}

	return err_code;
}

//-----------------------------------------------------------------------------------------------------------------------------------
//			stm8_read
//-----------------------------------------------------------------------------------------------------------------------------------
ret_code_t stm8_read(STM8_READ_CMD_t cmd, uint8_t *val)
{
	uint8_t data;
	ret_code_t err_code;

	data = (cmd << 3);
	stm8_cmd_read_register[1] = data;

	err_code = twi_perform(NULL, stm8_cmd_read_transfers, sizeof(stm8_cmd_read_transfers) / sizeof(nrf_twi_mngr_transfer_t));
	if (err_code != NRF_SUCCESS)
	{
		MKB_LOG_ERROR("I2C read failed!!");
		return (NRF_ERROR_INTERNAL);
	}

	// Refer to "message.txt: comment 3, comment 5"
	if ((STM8_READ_VERSION == cmd) || (STM8_READ_BATTERY_LEVEL == cmd))
	{
		*val = m_stm8_rdbuff; // 8-bits all
	}
	else
	{
		*val = m_stm8_rdbuff & STM8_READ_DATA_MASK; // LSB 3-bits only
	}

	return err_code;
}

//-----------------------------------------------------------------------------------------------------------------------------------
//			drv_stm8_get_version
//-----------------------------------------------------------------------------------------------------------------------------------
uint8_t drv_stm8_get_version(void)
{
	ret_code_t err_code;
	uint8_t stm8_fw_ver = 0xFF;

	err_code = stm8_read(STM8_READ_VERSION, &stm8_fw_ver);
	if (NRF_SUCCESS != err_code)
	{
		MKB_LOG_ERROR("read verion Fail! RET=0x%x", err_code);
	}
	else
	{
		MKB_LOG_DEBUG("verion=0x%x", stm8_fw_ver);
	}

	return stm8_fw_ver;
}

//-----------------------------------------------------------------------------------------------------------------------------------
//			drv_stm8_get_led_color
//-----------------------------------------------------------------------------------------------------------------------------------
uint8_t drv_stm8_get_led_color(void)
{
	ret_code_t err_code;
	uint8_t stm8_led = 0xFF;

	err_code = stm8_read(STM8_READ_LED_COLOR, &stm8_led);
	if (NRF_SUCCESS != err_code)
	{
		MKB_LOG_ERROR("read verion Fail! RET=0x%x", err_code);
	}
	else
	{
		MKB_LOG_DEBUG("led color=0x%x", stm8_led);
	}

	return stm8_led;
}

//-----------------------------------------------------------------------------------------------------------------------------------
//			drv_stm8_get_led_status
//-----------------------------------------------------------------------------------------------------------------------------------
uint8_t drv_stm8_get_led_status(void)
{
	ret_code_t err_code;
	uint8_t stm8_led = 0xFF;

	err_code = stm8_read(STM8_READ_LED_STATUS, &stm8_led);
	if (NRF_SUCCESS != err_code)
	{
		MKB_LOG_ERROR("read verion Fail! RET=0x%x", err_code);
	}
	else
	{
		MKB_LOG_DEBUG("led status=0x%x", stm8_led);
	}

	return stm8_led;
}

//-----------------------------------------------------------------------------------------------------------------------------------
//			drv_stm8_get_touch
//-----------------------------------------------------------------------------------------------------------------------------------
ret_code_t drv_stm8_get_touch(STM8_TOUCH_t *pState)
{
	ret_code_t err_code;
	STM8_TOUCH_STATUS_t left, right;

	err_code = stm8_read(STM8_READ_TOUCH_LEFT, &left);
	if (NRF_SUCCESS != err_code)
	{
		MKB_LOG_ERROR("read left touch state Fail! RET=0x%x", err_code);
		return err_code;
	}
	else
	{
		pState->left = left;
		MKB_LOG_DEBUG("left touch state=%d", left);
	}

	err_code = stm8_read(STM8_READ_TOUCH_RIGHT, &right);
	if (NRF_SUCCESS != err_code)
	{
		MKB_LOG_ERROR("read right touch state Fail! RET=0x%x", err_code);
		return err_code;
	}
	else
	{
		pState->right = right;
		MKB_LOG_DEBUG("right touch state=%d", right);
	}

	return err_code;
}

//-----------------------------------------------------------------------------------------------------------------------------------
//			drv_stm8_get_center
//-----------------------------------------------------------------------------------------------------------------------------------
ret_code_t drv_stm8_get_center(STM8_CENTER_t *pState)
{
	ret_code_t err_code;
	STM8_TOUCH_STATUS_t left, right;

	err_code = stm8_read(STM8_READ_TOUCH_CENTER_LEFT, &left);
	if (NRF_SUCCESS != err_code)
	{
		MKB_LOG_ERROR("read left center state Fail! RET=0x%x", err_code);
		return err_code;
	}
	else
	{
		pState->left = left;
		MKB_LOG_DEBUG("left center state=%d", left);
	}

	err_code = stm8_read(STM8_READ_TOUCH_CENTER_RIGHT, &right);
	if (NRF_SUCCESS != err_code)
	{
		MKB_LOG_ERROR("read right center state Fail! RET=0x%x", err_code);
		return err_code;
	}
	else
	{
		pState->right = right;
		MKB_LOG_DEBUG("right center state=%d", right);
	}

	return err_code;
}

//-----------------------------------------------------------------------------------------------------------------------------------
//			drv_stm8_get_button
//-----------------------------------------------------------------------------------------------------------------------------------
ret_code_t drv_stm8_get_button(STM8_BUTTON_t *pState)
{
	ret_code_t err_code;
	STM8_BUTTON_STATUS_t left, right;

	err_code = stm8_read(STM8_READ_BUTTON_LEFT, &left);
	if (NRF_SUCCESS != err_code)
	{
		MKB_LOG_ERROR("read left button state Fail! RET=0x%x", err_code);
		return err_code;
	}
	else
	{
		pState->left = left;
		MKB_LOG_DEBUG("left button state=%d", left);
	}

	err_code = stm8_read(STM8_READ_BUTTON_RIGHT, &right);
	if (NRF_SUCCESS != err_code)
	{
		MKB_LOG_ERROR("read right button state Fail! RET=0x%x", err_code);
		return err_code;
	}
	else
	{
		pState->right = right;
		MKB_LOG_DEBUG("right button state=%d", right);
	}

	return err_code;
}

//-----------------------------------------------------------------------------------------------------------------------------------
//			drv_stm8_get_battery_level
//-----------------------------------------------------------------------------------------------------------------------------------
ret_code_t drv_stm8_get_battery_level(uint8_t *pLevel)
{
	ret_code_t err_code;
	uint8_t level;

	err_code = stm8_read(STM8_READ_BATTERY_LEVEL, &level);
	if (NRF_SUCCESS != err_code)
	{
		MKB_LOG_ERROR("read battery level Fail! RET=0x%x", err_code);
		return err_code;
	}
	else
	{
		*pLevel = level;
		MKB_LOG_ERROR("battery level=%d", level);
	}

	return err_code;
}

//-----------------------------------------------------------------------------------------------------------------------------------
//			drv_stm8_get_battery_state
//-----------------------------------------------------------------------------------------------------------------------------------
ret_code_t drv_stm8_get_battery_state(uint8_t *pState)
{
	ret_code_t err_code;
	uint8_t state;

	err_code = stm8_read(STM8_READ_BATTERY_STATUS, &state);
	if (NRF_SUCCESS != err_code)
	{
		MKB_LOG_ERROR("read battery state Fail! RET=0x%x", err_code);
		return err_code;
	}
	else
	{
		*pState = state;
		MKB_LOG_DEBUG("battery state=%d", state);
	}

	return err_code;
}

static void stm8_read_state_cb(ret_code_t result, void *p_user_data)
{
	STM8_STATE_t state;

	if (result == NRF_SUCCESS)
	{
		state.bits.touch_left = m_stm8_touch.left;
		state.bits.touch_right = m_stm8_touch.right;
		state.bits.button_left = m_stm8_button.left;
		state.bits.button_right = m_stm8_button.right;
		state.bits.center_left = m_stm8_center.left;
		state.bits.center_right = m_stm8_center.right;

		if (m_stm8_read_handler)
			m_stm8_read_handler(result, &state);
	}

#if (STM8_MODE == STM8_MODE_GPIOTE)
	ret_code_t err_code = stm8_sense_enable();
	if (err_code != NRF_SUCCESS)
	{
		MKB_ERROR_CHECK(err_code);
	}
#endif /* (STM8_MODE == STM8_MODE_GPIOTE) */
}

//-----------------------------------------------------------------------------------------------------------------------------------
//			stm8_read_state_schedule
//-----------------------------------------------------------------------------------------------------------------------------------
static ret_code_t stm8_read_state_schedule(void)
{
	ret_code_t rv;

	//if (nrf_atomic_flag_set_fetch(&m_stm8_operation_active))
	//{
	//    return NRF_ERROR_BUSY;
	//}

	MKB_LOG_DEBUG("stm8 read request");

	rv = twi_schedule(&stm8_read_state_transaction);

	return rv;
}

static void stm8_scan_handler(void *p_event_data, uint16_t event_size)
{
	UNUSED_PARAMETER(p_event_data);
	UNUSED_PARAMETER(event_size);

	if (m_stm8_enabled)
	{
#if (STM8_MODE == STM8_MODE_GPIOTE)
		stm8_read_state_schedule();
#else
#if (MKB_STM8_INT_PIN_USE)
		if (!nrf_gpio_pin_read(STM8_INT_PIN))
			stm8_read_state_schedule();
#endif
		{
			STM8_STATE_t state;

			drv_stm8_get_button(&m_stm8_button);
			drv_stm8_get_touch(&m_stm8_touch);
			state.bits.touch_left = m_stm8_touch.left;
			state.bits.touch_right = m_stm8_touch.right;
			state.bits.button_left = m_stm8_button.left;
			state.bits.button_right = m_stm8_button.right;

			if (m_stm8_read_handler)
				m_stm8_read_handler(NRF_SUCCESS, &state);
		}
#endif
	}

	nrf_atomic_flag_clear(&m_stm8_operation_active);
}

#if (STM8_MODE == STM8_MODE_GPIOTE)
/**@brief GPIOTE PORT interrupt handler.*/
static void stm8_interrupt_handler(uint32_t const *p_event_pins_low_to_high,
								   uint32_t const *p_event_pins_high_to_low)
{
	if (m_stm8_enabled)
	{
		MKB_LOG_DEBUG("Interrupt: 0x%x", *p_event_pins_high_to_low);

		if (*p_event_pins_high_to_low & STM8_GPIOTE_MASK)
		{
			// Disable sensing.
			stm8_sense_disable();

			// Schedule cypress_scan
			ret_code_t ret_code = app_sched_event_put(NULL, 0, stm8_scan_handler);
			MKB_ERROR_CHECK(ret_code);
		}
	}
}
#elif (STM8_MODE == STM8_MODE_TIMER)
static void stm8_timeout_handler(void *p_context)
{
	UNUSED_PARAMETER(p_context);

	if (m_stm8_enabled)
	{
		ret_code_t ret_code = app_sched_event_put(NULL, 0, stm8_scan_handler);
		MKB_ERROR_CHECK(ret_code);
	}
}
#endif /* (STM8_MODE == STM8_MODE_GPIOTE) */

//-----------------------------------------------------------------------------------------------------------------------------------
//			drv_stm8_init
//-----------------------------------------------------------------------------------------------------------------------------------
ret_code_t drv_stm8_init(stm8_read_handler_t read_handler)
{
	ret_code_t err_code;

	nrf_gpio_pin_dir_set(STM8_INT_PIN, NRF_GPIO_PIN_DIR_INPUT);

	m_stm8_state.data = 0;

	// Initialize driver state.
	nrf_atomic_flag_clear(&m_stm8_operation_active);

	m_stm8_read_handler = read_handler;

	m_stm8_enabled = false;

	//drv_stm8_write(STM8_WRITE_LED_BR, 3);
	//drv_stm8_write(STM8_WRITE_LED_COLOR, STM8_LED_COLOR_GREEN);
	//drv_stm8_write(STM8_WRITE_LED_STATUS, STM8_LED_ON);
	//drv_stm8_write(STM8_WRITE_LED_STATUS, STM8_LED_RESET);

#if (STM8_MODE == STM8_MODE_GPIOTE)
	uint32_t low_to_high_mask = 0;
	uint32_t high_to_low_mask = STM8_GPIOTE_MASK;

	err_code = app_gpiote_user_register(&m_stm8_gpiote,
										&low_to_high_mask,
										&high_to_low_mask,
										stm8_interrupt_handler);
	if (err_code != NRF_SUCCESS)
	{
		MKB_ERROR_CHECK(err_code);
		return err_code;
	}
#elif (STM8_MODE == STM8_MODE_TIMER)
	// Create stm8 timer.
	err_code = app_timer_create(&m_stm8_timer_id, APP_TIMER_MODE_REPEATED, stm8_timeout_handler);
	if (err_code != NRF_SUCCESS)
	{
		MKB_ERROR_CHECK(err_code);
		return err_code;
	}
#endif /* (STM8_MODE == STM8_MODE_GPIOTE) */

	MKB_LOG_INFO("Initialized");

	return NRF_SUCCESS;
}

//-----------------------------------------------------------------------------------------------------------------------------------
//			drv_stm8_enable
//-----------------------------------------------------------------------------------------------------------------------------------
ret_code_t drv_stm8_enable(void)
{
	ret_code_t err_code;

	if (!m_stm8_enabled)
	{
#if (STM8_MODE == STM8_MODE_GPIOTE)
		err_code = stm8_sense_enable();
#elif (STM8_MODE == STM8_MODE_TIMER)
		err_code = app_timer_start(m_stm8_timer_id, APP_TIMER_TICKS(STM8_TIMER_INTERVAL), NULL);
#endif /* (STM8_MODE == STM8_MODE_GPIOTE) */
		MKB_ERROR_CHECK(err_code);

		m_stm8_enabled = true;
	}

	return NRF_SUCCESS;
}

//-----------------------------------------------------------------------------------------------------------------------------------
//			drv_stm8_disable
//-----------------------------------------------------------------------------------------------------------------------------------
ret_code_t drv_stm8_disable(void)
{
	ret_code_t err_code;

	if (m_stm8_enabled)
	{
		m_stm8_enabled = false;

#if (STM8_MODE == STM8_MODE_GPIOTE)
		err_code = stm8_sense_disable();
#elif (STM8_MODE == STM8_MODE_TIMER)
		err_code = app_timer_stop(m_stm8_timer_id);
#endif /* (STM8_MODE == STM8_MODE_GPIOTE) */
		MKB_ERROR_CHECK(err_code);
	}

	return NRF_SUCCESS;
}

#if (defined(NRF_PWR_MGMT_ENABLED) && NRF_PWR_MGMT_ENABLED)
static bool mkb_stm8_shutdown(nrf_pwr_mgmt_evt_t event)
{
	if (event == NRF_PWR_MGMT_EVT_PREPARE_WAKEUP)
	{
		drv_stm8_disable();
		nrf_gpio_cfg_sense_set(MKB_STM8_INT_PIN, NRF_GPIO_PIN_SENSE_LOW);

		MKB_LOG_INFO("WAKEUP PIN: %d", MKB_STM8_INT_PIN);
	}

	return true;
}

NRF_PWR_MGMT_HANDLER_REGISTER(mkb_stm8_shutdown, SHUTDOWN_PRIORITY_DEFAULT);
#endif /* NRF_PWR_MGMT_ENABLED */

#endif // (defined(MKB_STM8_ENABLED) && MKB_STM8_ENABLED)