/*
* ===============================================================================
* Copyright (C) 2017-2019
* INNOPRESSO INC. All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
* ===============================================================================
*/

#include <stdbool.h>
#include <stdint.h>
#include "sdk_common.h"
#include "boards.h"
#include "app_timer.h"

#include "nrf_log.h"
#include "nrf_delay.h"
#include "nrf_pwr_mgmt.h"
#include "nrf_drv_twis.h"

#include "mkb_common.h"

#if (defined(MKB_TWIS_ENABLED) && MKB_TWIS_ENABLED)
#include "drv_twis.h"

#if (defined(MKB_WDT_ENABLED) && MKB_WDT_ENABLED)
#include "mkb_wdt.h"
#endif

#if TWI0_ENABLED
#if TWIS0_ENABLED
#error "No TWIS0 bus is available, You have to enable TWIS1_ENABLED"
#endif
#if !TWIS1_ENABLED
#error "You have to enable TWIS1_ENABLED"
#endif
#elif TWI1_ENABLED
#if TWIS1_ENABLED
#error "No TWIS1 bus is available, You have to enable TWIS0_ENABLED"
#endif
#if !TWIS0_ENABLED
#error "You have to enable TWIS0_ENABLED"
#endif
#else
#if !TWIS0_ENABLED && !TWIS1_ENABLED
#error "You have to enable TWIS0_ENABLED or TWIS1_ENABLED"
#endif
#endif

/**
 * @brief TWIS instance.
 *
 * TWIS driver instance used by this EEPROM simulator.
 */
static const nrf_drv_twis_t m_twis = NRF_DRV_TWIS_INSTANCE(MKB_TWIS_INST);

/**
 * @brief Receive buffer.
 *
 * Receiving buffer must contain the address and TWIS_BUF_MAX_BYTES bytes of data.
 */
static uint8_t m_rxbuff[TWIS_BUF_MAX_BYTES];
static uint8_t m_rxlen;

/**
 * @brief Transmit buffer.
 *
 * Receiving buffer must contain the address and TWIS_BUF_MAX_BYTES bytes of data.
 */
static uint8_t m_txbuff[TWIS_BUF_MAX_BYTES];
static uint8_t m_txlen;

/**
 * @brief Internal error flag.
 *
 * This flag is set if any communication error is detected.
 * It can be cleared by calling the @ref eeprom_simulator_error_get function.
 */
static bool m_error_flag;

/**
 * @brief Start after the WRITE command.
 *
 * Function sets pointers for TWIS to receive data.
 * WRITE command does not write directly to memory array.
 * Temporary receive buffer is used.
 * @sa m_rxbuff
 */
static void twis_writeBegin(void)
{
    MKB_LOG_INFO("RX Start");
    (void)nrf_drv_twis_rx_prepare(&m_twis, m_rxbuff, sizeof(m_rxbuff));
}

/**
 * @brief Finalize the WRITE command.
 *
 * Call this function when the write command is finished.
 * It sets a memory pointer and writes all received data to the memory array.
 */
static void twis_writeEnd(size_t cnt)
{
    MKB_LOG_INFO("RX End Len: %d", cnt);
    if (cnt > (0))
    {
        MKB_LOG_INFO("RX data 0: 0x%02x 1: 0x%02x", m_rxbuff[0], m_rxbuff[1]);
    }
}

/**
 * @brief Start after the READ command.
 *
 * Function sets pointers for TWIS to transmit data from the current address to the end of memory.
 */
static void twis_readBegin(void)
{
    MKB_LOG_INFO("TX Start");
    (void)nrf_drv_twis_tx_prepare(&m_twis, m_txbuff, sizeof(m_txbuff));
}

/**
 * @brief Finalize the READ command.
 *
 * Call this function when the read command is finished.
 * It adjusts the current m_addr pointer.
 * @param cnt Number of bytes read.
 */
static void twis_readEnd(size_t cnt)
{
    MKB_LOG_INFO("TX End Len: %d", cnt);
    m_txlen = cnt;
}

/**
 * @brief Event processing.
 *
 */
static void twis_event_handler(nrf_drv_twis_evt_t const *const p_event)
{
    switch (p_event->type)
    {
    case TWIS_EVT_READ_REQ:
        if (p_event->data.buf_req)
        {
            twis_readBegin();
        }
        break;
    case TWIS_EVT_READ_DONE:
        twis_readEnd(p_event->data.tx_amount);
        break;
    case TWIS_EVT_WRITE_REQ:
        if (p_event->data.buf_req)
        {
            twis_writeBegin();
        }
        break;
    case TWIS_EVT_WRITE_DONE:
        twis_writeEnd(p_event->data.rx_amount);
        break;

    case TWIS_EVT_READ_ERROR:
    case TWIS_EVT_WRITE_ERROR:
    case TWIS_EVT_GENERAL_ERROR:
        m_error_flag = true;
        break;
    default:
        break;
    }
}

ret_code_t twis_init(void)
{
    ret_code_t ret;

    const nrf_drv_twis_config_t config =
        {
            .addr = {MKB_TWIS_ADDR, 0},
            .scl = MKB_TWIS_SCL_PIN,
            .scl_pull = NRF_GPIO_PIN_PULLUP,
            .sda = MKB_TWIS_SDA_PIN,
            .sda_pull = NRF_GPIO_PIN_PULLUP,
            .interrupt_priority = APP_IRQ_PRIORITY_HIGH};

    /* Init TWIS */
    do
    {
        ret = nrf_drv_twis_init(&m_twis, &config, twis_event_handler);
        if (NRF_SUCCESS != ret)
        {
            break;
        }

        nrf_drv_twis_enable(&m_twis);
    } while (0);

    return ret;
}

bool twis_error_check(void)
{
    return m_error_flag;
}

uint32_t twis_error_get_and_clear(void)
{
    uint32_t ret = nrf_drv_twis_error_get_and_clear(&m_twis);
    m_error_flag = false;
    return ret;
}

#if (defined(NRF_PWR_MGMT_ENABLED) && NRF_PWR_MGMT_ENABLED)
/**@brief Flush TWI transactions and shutdown TWI modules before going to sleep.
 *
 * @param[in] wakeup If false, go to deepest sleep and do not prepare to wake up the system.
 *
 * @return True if the TWI subsystem is ready for shutdown, false otherwise.
 */
static bool twis_shutdown(nrf_pwr_mgmt_evt_t event)
{
    return true;
}
NRF_PWR_MGMT_HANDLER_REGISTER(twis_shutdown, SHUTDOWN_PRIORITY_DEFAULT);
#endif /* NRF_PWR_MGMT_ENABLED */

#endif //(defined(MKB_TWIS_ENABLED) && MKB_TWIS_ENABLED)
