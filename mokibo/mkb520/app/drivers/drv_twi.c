/*
* ===============================================================================
* Copyright (C) 2017-2019
* INNOPRESSO INC. All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
* ===============================================================================
*/

#include <stdbool.h>
#include <stdint.h>
#include "sdk_common.h"
#include "boards.h"
#include "app_timer.h"

#include "nrf_log.h"
#include "nrf_delay.h"
#include "nrf_twi_mngr.h"

#include "mkb_common.h"

#if (defined(MKB_TWI_ENABLED) && MKB_TWI_ENABLED)
#include "drv_twi.h"

#if (defined(MKB_WDT_ENABLED) && MKB_WDT_ENABLED)
#include "mkb_wdt.h"
#endif

#if (TWI0_ENABLED) || (TWI1_ENABLED)

#if TWI0_ENABLED
#define TWI_COMMON_DEFAULT_BUS 0
#elif TWI1_ENABLED
#define TWI_COMMON_DEFAULT_BUS 1
#else
#error "No TWI bus is available"
#endif

/**@brief TWI manager instance. */
NRF_TWI_MNGR_DEF(twi_mngr, CONFIG_TWI_QSIZE, 0);

/**@brief Configuration of TWI buses. */
const nrf_drv_twi_config_t g_twi_bus_config[] = {
#if TWI0_ENABLED
    /* TWI bus 0. */
    [0] = {
        .frequency = (nrf_twi_frequency_t)TWI_DEFAULT_CONFIG_FREQUENCY,
        .interrupt_priority = TWI_DEFAULT_CONFIG_IRQ_PRIORITY,
        .clear_bus_init = TWI_DEFAULT_CONFIG_CLR_BUS_INIT,
        .hold_bus_uninit = CONFIG_TWI0_UP_IN_SYSOFF_STATE,
        .scl = CONFIG_IO_TWI0_SCL,
        .sda = CONFIG_IO_TWI0_SDA,
    },
#endif
#if TWI1_ENABLED
    /* TWI bus 1. */
    [1] = {
        .frequency = (nrf_twi_frequency_t)TWI_DEFAULT_CONFIG_FREQUENCY,
        .interrupt_priority = TWI_DEFAULT_CONFIG_IRQ_PRIORITY,
        .clear_bus_init = TWI_DEFAULT_CONFIG_CLR_BUS_INIT,
        .hold_bus_uninit = CONFIG_TWI1_UP_IN_SYSOFF_STATE,
        .scl = CONFIG_IO_TWI1_SCL,
        .sda = CONFIG_IO_TWI1_SDA,
    }
#endif
};

/**@brief Pointer to TWI manager instance. */
nrf_twi_mngr_t const *const g_twi_mngr = &twi_mngr;

#if (defined(MKB_TWI_PROBE_ENABLED) && MKB_TWI_PROBE_ENABLED)
uint8_t m_twi_probe_buf[0x80];
uint8_t m_twi_probe_finds;

void twi_probe(void)
{
    nrf_twi_mngr_transfer_t transfers[1];
    uint8_t data, i;

    for (uint8_t i = 0; i < 0x80; i++)
        m_twi_probe_buf[i] = 0xff;
    m_twi_probe_finds = 0;

    MKB_LOG_INFO("TWI Probe Start!");
    for (uint8_t i = 0; i < 0x80; i++)
    {
        transfers[0].p_data = &data;
        transfers[0].length = 1;
        transfers[0].operation = NRF_TWI_MNGR_WRITE_OP(i);
        transfers[0].flags = 0;

        ret_code_t rv = twi_perform(NULL, transfers, ARRAY_SIZE(transfers));
        if (rv == NRF_SUCCESS)
        {
            transfers[0].p_data = &data;
            transfers[0].length = 1;
            transfers[0].operation = NRF_TWI_MNGR_READ_OP(i);
            transfers[0].flags = 0;

            rv = twi_perform(NULL, transfers, ARRAY_SIZE(transfers));
            if (rv == NRF_SUCCESS)
            {
                MKB_LOG_INFO("I2C 0x%02x find!", i);
                m_twi_probe_buf[m_twi_probe_finds++] = i;
            }
        }
    }
}

bool twi_probe_exist(uint8_t addr)
{
    for (uint8_t i = 0; i < m_twi_probe_finds; i++)
    {
        if (m_twi_probe_buf[i] == addr)
            return true;
    }

    return false;
}
#endif //(defined(MKB_TWI_PROBE_ENABLED) && MKB_TWI_PROBE_ENABLED)

ret_code_t twi_init(void)
{
    /* Initialize TWI manager. */
    ret_code_t status = nrf_twi_mngr_init(g_twi_mngr, &g_twi_bus_config[TWI_COMMON_DEFAULT_BUS]);

#if (defined(MKB_TWI_INIT_DELAY_ENABLED) && MKB_TWI_INIT_DELAY_ENABLED)
    if (status == NRF_SUCCESS)
    {
        /* Wait for a moment. Some TWI devices do not respond
         * if they are accessed right after bus initialization. */
        nrf_delay_ms(MKB_TWI_INIT_DELAY);
    }
#endif

#if (defined(MKB_TWI_PROBE_ENABLED) && MKB_TWI_PROBE_ENABLED)
    twi_probe();
#endif

    return status;
}

//uint32_t	timestamp;
void twi_idle(void)
{
//uint32_t	now, diff;

//now = app_timer_cnt_get();
//diff = app_timer_cnt_diff_compute(now, timestamp);
//if(diff > 100) SEGGER_RTT_printf(0, "app_timer_diff is: %u \r\n", diff);
#if (defined(MKB_WDT_ENABLED) && MKB_WDT_ENABLED)
    mkb_wdt_feed();
#endif

    nrf_pwr_mgmt_run();
}

ret_code_t twi_perform(nrf_drv_twi_config_t const *p_bus_config,
                       nrf_twi_mngr_transfer_t const *p_transfers,
                       uint8_t transfer_count)
{
#if NRF_PWR_MGMT_ENABLED
    //timestamp = app_timer_cnt_get();
    void (*idle_func)(void) = twi_idle;
#else
    void (*idle_func)(void) = NULL;
#endif
    return nrf_twi_mngr_perform(g_twi_mngr, p_bus_config, p_transfers, transfer_count, idle_func);
}

ret_code_t twi_schedule(nrf_twi_mngr_transaction_t const *p_transaction)
{
    return nrf_twi_mngr_schedule(g_twi_mngr, p_transaction);
}

ret_code_t twi_read_burst(nrf_drv_twi_config_t const *p_bus_config,
                          uint8_t device_addr,
                          uint8_t *p_value,
                          uint8_t len)
{
    nrf_twi_mngr_transfer_t transfers[1];

    transfers[0].p_data = p_value;
    transfers[0].length = len;
    transfers[0].operation = NRF_TWI_MNGR_READ_OP(device_addr);
    transfers[0].flags = 0;

    return twi_perform(p_bus_config, transfers, ARRAY_SIZE(transfers));
}

ret_code_t twi_write_burst(nrf_drv_twi_config_t const *p_bus_config,
                           uint8_t device_addr,
                           uint8_t *p_value,
                           uint8_t len)
{
    nrf_twi_mngr_transfer_t transfers[1];

    transfers[0].p_data = p_value;
    transfers[0].length = len;
    transfers[0].operation = NRF_TWI_MNGR_WRITE_OP(device_addr);
    transfers[0].flags = 0;

    return twi_perform(p_bus_config, transfers, ARRAY_SIZE(transfers));
}

ret_code_t twi_register_read(nrf_drv_twi_config_t const *p_bus_config,
                             uint8_t device_addr,
                             uint8_t register_addr,
                             uint8_t *p_value)
{
    nrf_twi_mngr_transfer_t transfers[2];

    transfers[0].p_data = &register_addr;
    transfers[0].length = sizeof(register_addr);
    transfers[0].operation = NRF_TWI_MNGR_WRITE_OP(device_addr);
    transfers[0].flags = NRF_TWI_MNGR_NO_STOP;

    transfers[1].p_data = p_value;
    transfers[1].length = sizeof(*p_value);
    transfers[1].operation = NRF_TWI_MNGR_READ_OP(device_addr);
    transfers[1].flags = 0;

    return twi_perform(p_bus_config, transfers, ARRAY_SIZE(transfers));
}

ret_code_t twi_register_write(nrf_drv_twi_config_t const *p_bus_config,
                              uint8_t device_addr,
                              uint8_t register_addr,
                              uint8_t value)
{
    nrf_twi_mngr_transfer_t transfers[1];
    uint8_t data_to_write[2];

    data_to_write[0] = register_addr;
    data_to_write[1] = value;

    transfers[0].p_data = data_to_write;
    transfers[0].length = sizeof(data_to_write);
    transfers[0].operation = NRF_TWI_MNGR_WRITE_OP(device_addr);
    transfers[0].flags = 0;

    return twi_perform(p_bus_config, transfers, ARRAY_SIZE(transfers));
}

ret_code_t twi_register_bulk_write(nrf_drv_twi_config_t const *p_bus_config,
                                   uint8_t device_addr,
                                   const uint8_t p_reg_val_array[][2],
                                   unsigned int reg_val_array_size,
                                   bool perform_verification)
{
    for (unsigned int i = 0; i < reg_val_array_size; i++)
    {
        ret_code_t status = twi_register_write(p_bus_config, device_addr, p_reg_val_array[i][0], p_reg_val_array[i][1]);
        if (status != NRF_SUCCESS)
        {
            return status;
        }

        if (perform_verification)
        {
            uint8_t read;

            status = twi_register_read(p_bus_config, device_addr, p_reg_val_array[i][0], &read);
            if (status != NRF_SUCCESS)
            {
                return status;
            }

            if (p_reg_val_array[i][1] != read)
            {
                return NRF_ERROR_INTERNAL;
            }
        }
    }

    return NRF_SUCCESS;
}

#if (defined(NRF_PWR_MGMT_ENABLED) && NRF_PWR_MGMT_ENABLED)
/**@brief Flush TWI transactions and shutdown TWI modules before going to sleep.
 *
 * @param[in] wakeup If false, go to deepest sleep and do not prepare to wake up the system.
 *
 * @return True if the TWI subsystem is ready for shutdown, false otherwise.
 */
static bool twi_shutdown(nrf_pwr_mgmt_evt_t event)
{
    while (!nrf_twi_mngr_is_idle(g_twi_mngr))
    {
        nrf_pwr_mgmt_run();
    }
    nrf_twi_mngr_uninit(g_twi_mngr);

    return true;
}
NRF_PWR_MGMT_HANDLER_REGISTER(twi_shutdown, SHUTDOWN_PRIORITY_DEFAULT);
#endif /* NRF_PWR_MGMT_ENABLED */

#endif /* TWI0_ENABLED || TWI1_ENABLED */

#endif //(defined(MKB_TWI_ENABLED) && MKB_TWI_ENABLED)
