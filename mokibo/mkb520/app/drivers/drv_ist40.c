/*
* ===============================================================================
* Copyright (C) 2017-2019
* INNOPRESSO INC. All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
* ===============================================================================
*/

/** @file
 *
 * @defgroup ble_app_hids_mkb316_driver drv_cypress.c
 * @{
 * @ingroup ble_app_hids_mkb316
 * @brief HID Touchpad Keyboard Application cypress driver file.
 *
 * The Cypress TrueTouch solution for a touchscreen.
 * TrueTouch is a register-programmable, general-purpose touchscreen system solution, 
 * not a low-level sensing development platform.
 *
 * The host physically connects to the device using a physical communication interface 
 * and uses a register access communication protocol over that physical interface 
 * to retrieve touch reports, perform device firmware updates in the field, 
 * manufacturing test, and retrieve touch/button reports.
 *
 * The physical communication interface is an I2C and SPI.
 *
 * Two hardware interfaces work together to communicate with the host. 
 * The primary communication interface is a packet based protocol operating over an I2C or SPI interface. 
 * The secondary communication interface is a unidirectional communication interrupt (COMM_INT) pin 
 * used for event notification to the host.
 *
 * An I2C slave address match can be configured to be a deep-sleep wakeup event.
 *
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "sdk_common.h"
#include "app_error.h"
#include "app_timer.h"
#include "app_scheduler.h"
#if defined(GPIOTE_ENABLED) && GPIOTE_ENABLED
#include "app_gpiote.h"
#endif

#include "nrf_atomic.h"
#include "nrf_assert.h"
#include "nrf_error.h"
#include "nrf_gpio.h"
#include "nrf_log.h"
#include "nrf_delay.h"
#include "nrf_pwr_mgmt.h"
#include "mem_manager.h"

#define MKB_LOG_LEVEL MKB_IST40_LOG_LEVEL
#include "mkb_common.h"
#include "drv_twi.h"
#include "drv_ist40.h"

#if (defined(MKB_IST40_ENABLED) && MKB_IST40_ENABLED)

APP_TIMER_DEF(m_ist40_timer);

#if (IST40_MODE == IST40_MODE_GPIOTE)
static app_gpiote_user_id_t m_ist40_gpiote; //!< GPIOTE Handle.
#endif

uint8_t m_dev_addr = IST40_ADDR;
static bool m_ist40_enabled;
static bool m_ist40_power_down;
static bool m_ist40_bootloader;

static uint8_t m_ist40_rdbuff[IST40_BUFFER_SIZE];
static uint8_t m_ist40_wrbuff[IST40_BUFFER_SIZE];

static ist40_info_t m_touch_info;

/**@brief Variable protecting shared data used in read operation */
static nrf_atomic_flag_t m_ist40_operation_active;

static ist40_read_handler_t m_ist40_read_handler;

static uint8_t NRF_TWI_MNGR_BUFFER_LOC_IND ist40_cmd_read[] =
	{
		0,
		0};
static nrf_twi_mngr_transfer_t const ist40_read_transfers[] =
	{
		NRF_TWI_MNGR_WRITE(IST40_ADDR, ist40_cmd_read, sizeof(ist40_cmd_read), 0),
};

static void ist40_read_cb(ret_code_t result, void *p_user_data);

/**@brief TWI transaction structure used in read operation */
static nrf_twi_mngr_transaction_t ist40_read_transaction =
	{
		.callback = ist40_read_cb,
		.p_user_data = &m_touch_info,
		.p_transfers = ist40_read_transfers,
		.number_of_transfers = ARRAY_SIZE(ist40_read_transfers),
};

// TSP Information
stIST40xxInfo_t gTSPInfo;

#if (IST40_MODE == IST40_MODE_GPIOTE)
/**@brief Enable interrupt-based key press sensing. */
static ret_code_t ist40_sense_enable(void)
{
	// Enable GPIOTE sensing.
	return app_gpiote_user_enable(m_ist40_gpiote);
}

/**@brief Disable interrupt-based key press sensing. */
static ret_code_t ist40_sense_disable(void)
{
	// Disable GPIOTE sensing.
	return app_gpiote_user_disable(m_ist40_gpiote);
}
#endif /* (IST40_MODE == IST40_MODE_GPIOTE) */

//-----------------------------------------------------------------------------------------------------------------------------------
//			ist40_gpio()
//-----------------------------------------------------------------------------------------------------------------------------------
#include "nrf_gpio.h"
static void ist40_gpio(void)
{
	nrf_gpio_pin_dir_set(IST40_INT_PIN, NRF_GPIO_PIN_DIR_INPUT);
	nrf_gpio_pin_dir_set(IST40_POWER_PIN, NRF_GPIO_PIN_DIR_OUTPUT);
}

//-----------------------------------------------------------------------------------------------------------------------------------
//			ist40_read_INT
//-----------------------------------------------------------------------------------------------------------------------------------
static uint8_t ist40_read_INT(void)
{
	uint8_t pinValue = nrf_gpio_pin_read(IST40_INT_PIN);
	return pinValue;
}

//-----------------------------------------------------------------------------------------------------------------------------------
//			ist40_waitGetLevel_INT
//-----------------------------------------------------------------------------------------------------------------------------------
static ret_code_t ist40_waitGetLevel_INT(uint8_t pinLevel, uint32_t ms)
{
	uint8_t pinValue = 0;				  //
	int32_t loop_count = ms * 1000 / 100; // signed... by Stanley
	//
	do
	{
		pinValue = ist40_read_INT();
		if (pinValue == pinLevel)
			break;
		//
		nrf_delay_us(100);
		loop_count--;
	} while (loop_count > 0);
	//
	if (loop_count <= 0)
	{
		return NRF_ERROR_INVALID_STATE;
	}
	return NRF_SUCCESS;
}

//-----------------------------------------------------------------------------------------------------------------------------------
//			IST40xx_Ctrl_CheckInterruptedReady_Low
//-----------------------------------------------------------------------------------------------------------------------------------
static ret_code_t IST40xx_Ctrl_CheckInterruptedReady_Low(uint32_t timeout_ms)
{
	return ist40_waitGetLevel_INT(IST40_PIN_LOW, timeout_ms); // 1ms wait
}
//-----------------------------------------------------------------------------------------------------------------------------------
//			IST40xx_Ctrl_CheckInterruptedReady_High
//-----------------------------------------------------------------------------------------------------------------------------------
static ret_code_t IST40xx_Ctrl_CheckInterruptedReady_High(uint32_t timeout_ms)
{
	return ist40_waitGetLevel_INT(IST40_PIN_HIGH, 1); // 1ms wait
}

//-------------------------------------------------------------------------------------------------------------------------
//			ist40_power_down
//-------------------------------------------------------------------------------------------------------------------------
static void ist40_power_down(void)
{
	// U6 power off - set LOW : @ P.25
	nrf_gpio_pin_write(IST40_POWER_PIN, IST40_PIN_LOW); // P.25 Off
	//==> power-off된 cypress touch는, i2c bus를 GND로 붙잡아서 않된다.
	m_ist40_power_down = true;
}

//-------------------------------------------------------------------------------------------------------------------------
//			ist40_power_up
//-------------------------------------------------------------------------------------------------------------------------
static void ist40_power_up(void)
{
	// U6 power on - set HIGH : @ P.25
	nrf_gpio_pin_write(IST40_POWER_PIN, IST40_PIN_HIGH); // P.25 ON
	m_ist40_power_down = false;
}

ret_code_t IST40xx_I2C_ReadWord(uint32_t regAddr,
								uint32_t *pData,
								uint32_t len)
{
	ret_code_t rv;
	nrf_twi_mngr_transfer_t transfers[2];
	uint32_t i, j;
	uint8_t sendData[sizeof(uint32_t)] = {
		0,
	};
	uint32_t byte_len = (len * sizeof(uint32_t));
	uint8_t buff[256];
	uint8_t *pBuf = buff;
	uint32_t tmpData;

	if (len == 0)
		return NRF_ERROR_INTERNAL;

	// ### Align the data for register address.
	for (i = 0; i < sizeof(regAddr); i++)
	{
		*(sendData + i) = (uint8_t)((regAddr >> (((sizeof(regAddr) - 1) - i) * 8)) & 0xFF);
	}

	transfers[0].p_data = sendData;
	transfers[0].length = sizeof(regAddr);
	transfers[0].operation = NRF_TWI_MNGR_WRITE_OP(m_dev_addr);
	transfers[0].flags = NRF_TWI_MNGR_NO_STOP;

	transfers[1].p_data = pBuf;
	transfers[1].length = byte_len;
	transfers[1].operation = NRF_TWI_MNGR_READ_OP(m_dev_addr);
	transfers[1].flags = 0;

	rv = twi_perform(NULL, transfers, ARRAY_SIZE(transfers));
	if (rv != NRF_SUCCESS)
	{
		MKB_LOG_ERROR("I2C failed!!");
	}
	else
	{
		// ### Align the data.
		for (i = 0; i < len; i++)
		{
			tmpData = 0;

			for (j = 0; j < sizeof(uint32_t); j++)
			{
				tmpData |= (uint32_t)(*(pBuf + ((i * sizeof(uint32_t)) + j)) << (((sizeof(uint32_t) - 1) - j) * 8));
			}

			*(pData + i) = tmpData;
		}
	}

	return rv;
}

ret_code_t IST40xx_I2C_WriteWord(uint32_t regAddr,
								 const uint32_t *pData,
								 uint32_t len)
{
	ret_code_t rv;
	nrf_twi_mngr_transfer_t transfers[1];
	uint32_t i, j;
	uint8_t buff[8];
	uint8_t *pBuf = buff;
	uint8_t byte_len;

	if (len == 0)
		return NRF_ERROR_INTERNAL;

	// ### regAddr + data length;
	byte_len = (sizeof(regAddr) + (len * sizeof(uint32_t)));

	// ### Align the data for register address.
	for (i = 0; i < sizeof(regAddr); i++)
	{
		*(pBuf + i) = (uint8_t)((regAddr >> (((sizeof(regAddr) - 1) - i) * 8)) & 0xFF);
	}

	// ### Align the data.
	for (i = 0; i < len; i++)
	{
		for (j = 0; j < sizeof(uint32_t); j++)
		{
			*(pBuf + (((i + 1) * sizeof(uint32_t)) + j)) = (uint8_t)((*(pData + i) >> (((sizeof(regAddr) - 1) - j) * 8)) & 0xFF);
		}
	}

	transfers[0].p_data = pBuf;
	transfers[0].length = byte_len;
	transfers[0].operation = NRF_TWI_MNGR_WRITE_OP(m_dev_addr);
	transfers[0].flags = 0;

	rv = twi_perform(NULL, transfers, ARRAY_SIZE(transfers));
	if (rv != NRF_SUCCESS)
	{
		MKB_LOG_ERROR("I2C failed!!");
	}

	return rv;
}

// H/W Reset IC and Hold Firmware
ret_code_t IST40xx_Ctrl_HW_Reset(uint32_t CheckIntr)
{
	ret_code_t rv;
	uint32_t Addr;
	uint32_t Data;

	MKB_LOG_INFO("INT state= %d", ist40_read_INT());

	ist40_power_down();
	nrf_delay_ms(IST40_POWEROFF_DELAY); // for sure reset
	ist40_power_up();

	MKB_LOG_INFO("INT state= %d", ist40_read_INT());

	if (CheckIntr)
	{
		if (IST40xx_Ctrl_CheckInterruptedReady_Low(IST40_INITIAL_INT_DELAY) != NRF_SUCCESS)
		{
			MKB_LOG_ERROR("H/W Reset Failed (No Intr after Reset)");
			return NRF_ERROR_INTERNAL;
		}
	}
	else
	{
		nrf_delay_ms(IST40_INITIAL_DELAY);
	}

	MKB_LOG_INFO("INT state= %d", ist40_read_INT());

	// Change to 32bit mode of I2C
	Addr = IST40_I2C_CTRL_REG;
	Data = IST40_I2C_32BIT_VALUE;
	rv = IST40xx_I2C_WriteWord(Addr, &Data, 1);
	if (rv != NRF_SUCCESS)
	{
		MKB_LOG_ERROR("Changeing to 32bit mode of I2C is failed!!");
		return (NRF_ERROR_INTERNAL);
	}

	rv = IST40xx_Ctrl_FwHold(1);
	if (rv != NRF_SUCCESS)
	{
		MKB_LOG_ERROR("F/W Hold failed after H/W Reset");
		return rv;
	}

	nrf_delay_ms(1);

	return NRF_SUCCESS;
}

// S/W Reset IC and hold Firmware
ret_code_t IST40xx_Ctrl_SW_Reset(ret_code_t CheckIntr)
{
	ret_code_t rv;
	uint32_t Addr;
	uint32_t Data;

	Addr = IST40_SW_RESET_CMD;
	Data = IST40_SW_RESET_VALUE;
	rv = IST40xx_I2C_WriteWord(Addr, &Data, 1);
	if (rv != NRF_SUCCESS)
	{
		MKB_LOG_ERROR("S/W Reset Command Failed!!");
		return (NRF_ERROR_INTERNAL);
	}

	if (CheckIntr)
	{
		if (IST40xx_Ctrl_CheckInterruptedReady_Low(IST40_INITIAL_INT_DELAY) != NRF_SUCCESS)
		{
			MKB_LOG_ERROR("S/W Reset Failed (No Intr after Reset)");
			return NRF_ERROR_INTERNAL;
		}
	}
	else
	{
		nrf_delay_ms(IST40_INITIAL_DELAY);
	}

	// Change to 32bit mode of I2C
	Addr = IST40_I2C_CTRL_REG;
	Data = IST40_I2C_32BIT_VALUE;
	rv = IST40xx_I2C_WriteWord(Addr, &Data, 1);
	if (rv != NRF_SUCCESS)
	{
		MKB_LOG_ERROR("I2C read failed!!");
		return (NRF_ERROR_INTERNAL);
	}

	rv = IST40xx_Ctrl_FwHold(1);
	if (rv != NRF_SUCCESS)
	{
		MKB_LOG_ERROR("F/W Hold Failed after S/W Reset");
		return NRF_ERROR_INTERNAL;
	}

	nrf_delay_ms(1);

	return NRF_SUCCESS;
}

ret_code_t IST40xx_Ctrl_FwStart()
{
	uint32_t Addr = IST40_HCOM_FW_START;
	uint32_t Data = true;

	return IST40xx_I2C_WriteWord(Addr, &Data, 1);
}

ret_code_t IST40xx_Ctrl_FwHold(uint32_t EnDis)
{
	uint32_t Addr = IST40_HCOM_FW_HOLD;
	uint32_t Data = EnDis;

	return IST40xx_I2C_WriteWord(Addr, &Data, 1);
}

ret_code_t IST40xx_Ctrl_AutoCalibration(void)
{
	uint32_t Addr = IST40_HCOM_RUN_CAL_AUTO;
	uint32_t Data = true;

	return IST40xx_I2C_WriteWord(Addr, &Data, 1);
}

ret_code_t IST40xx_Ctrl_SetSpecialMode(uint32_t Mode)
{
	uint32_t Addr = IST40_HCOM_SET_SPECIAL_MODE;
	uint32_t Data = Mode;

	return IST40xx_I2C_WriteWord(Addr, &Data, 1);
}

ret_code_t IST40xx_Ctrl_SetJigMode(uint32_t JigMode)
{
	uint32_t Addr = IST40_HCOM_SET_JIG_MODE;
	uint32_t Data = JigMode;

	return IST40xx_I2C_WriteWord(Addr, &Data, 1);
}

ret_code_t IST40xx_Ctrl_SetSleepMode(bool SleepMode)
{
	uint32_t Addr = IST40_HCOM_SLEEP_MODE_EN;
	uint32_t Data = SleepMode;

	return IST40xx_I2C_WriteWord(Addr, &Data, 1);
}

ret_code_t IST40xx_Ctrl_RateTime(uint32_t Us)
{
	ret_code_t rv;
	uint32_t Addr = IST40_HCOM_SET_TIME_IDLE;
	uint32_t Data = Us;

	rv = IST40xx_I2C_WriteWord(Addr, &Data, 1);
	if (rv != NRF_SUCCESS)
	{
		MKB_LOG_ERROR("I2C read failed!!");
		return (NRF_ERROR_INTERNAL);
	}

	nrf_delay_ms(40);

	Addr = IST40_HCOM_SET_TIME_ACTIVE;
	Data = Us;

	return IST40xx_I2C_WriteWord(Addr, &Data, 1);
}

ret_code_t IST40xx_Ctrl_SelfTest_Enable(uint32_t Mode)
{
	uint32_t Addr = IST40_HCOM_RUN_SELFTEST;
	uint32_t Data = Mode;

	return IST40xx_I2C_WriteWord(Addr, &Data, 1);
}

ret_code_t IST40xx_Ctrl_RunRamCode(uint32_t Execute)
{
	uint32_t Addr = IST40_HCOM_RUN_RAMCODE;
	uint32_t Data = Execute;

	return IST40xx_I2C_WriteWord(Addr, &Data, 1);
}

#define OSTEST_ADDR_MODE_ENABLE 0x30000100
ret_code_t IST40xx_Ctrl_CmCs_Enable(uint32_t Mode)
{
	uint32_t Addr = OSTEST_ADDR_MODE_ENABLE | IST40_DA_HIB_SINGLE;
	uint32_t Data = Mode;

	return IST40xx_I2C_WriteWord(Addr, &Data, 1);
}

uint8_t IST40xx_Ctrl_MakeChecksum8Byte(uint8_t *pStart, uint32_t Size)
{
	int32_t i;
	uint32_t Crc32;

	Crc32 = 0;

	for (i = Size; i > 0; i--)
		Crc32 += *pStart++;

	return (uint8_t)Crc32;
}

ret_code_t IST40xx_Ctrl_ReadTouchData(void)
{
	ret_code_t rv;
	uint32_t i;
	uint16_t X, Y;
	uint32_t Addr;
	uint32_t Touch1stData;
	uint32_t TouchCountScreen;
	stTouchCoord_t TouchDataValues[IST40_MAX_NUM_TOUCH_SCREEN] = {
		0,
	};

	TouchCountScreen = 0;

	// Read Touch Information 1st Word
	Addr = IST40_HCOM_GET_COORD_INFO;
	rv = IST40xx_I2C_ReadWord(Addr, &Touch1stData, 1);
	if (rv != NRF_SUCCESS)
	{
		MKB_LOG_ERROR("I2C read failed!!");
		return (NRF_ERROR_INTERNAL);
	}

	MKB_LOG_INFO("Touch1stData= 0x%08x", Touch1stData);

	// I/O Error
	if ((Touch1stData == 0x0) || (Touch1stData == 0xFFFFFFFF))
	{
		MKB_LOG_ERROR("I/O Error during Touch Data Read");
		return -1;
	}

	// IC Exception
	if ((Touch1stData & 0xFFFFFF00) == IST40_HOST_REPORT_EXCEPTION)
	{
		MKB_LOG_ERROR("Touch IC Exception Occurred");
		return -1;
	}

	if (Touch1stData == IST40_BOOT_OK_NOTIFY)
	{
		MKB_LOG_ERROR("Boot Notify Signature Read");
		return -1;
	}

	// CM/CS/Jitter Test End Notify
	if (((Touch1stData & IST40_END_NOTIFY_MASK) == IST40_END_NOTIFY_CM) || ((Touch1stData & IST40_END_NOTIFY_MASK) == IST40_END_NOTIFY_CS) || ((Touch1stData & IST40_END_NOTIFY_MASK) == IST40_END_NOTIFY_JITTER))
	{
		MKB_LOG_ERROR("Cm/Cs/Jitter End Notify Received during Touch Data Read");
		return -1;
	}

	// Calibration Result
	if ((Touch1stData & 0xF0000FFF) == 0x80000CAB)
	{
		MKB_LOG_ERROR("Calibration Result Received during Touch Data Read");
		return -1;
	}

	// Calibration Result
	if ((Touch1stData & 0xFFFF0000) == 0x3CAB0000)
	{
		MKB_LOG_ERROR("Calibration Result Received during Touch Data Read");
		return -1;
	}

	TouchCountScreen = IST40_PARSE_FINGER_CNT(Touch1stData);

	// Screen and Key cannot be pressed simultaneously
	if (TouchCountScreen == 0)
	{
		MKB_LOG_ERROR("Touch Count is 0.");
		return -1;
	}

	memset(TouchDataValues, 0, IST40_MAX_NUM_TOUCH_SCREEN * sizeof(stTouchCoord_t));

	// read Coordinates (Only Screen have Coordinate. Info)
	Addr = IST40_HCOM_GET_COORD_SCREEN;
	rv = IST40xx_I2C_ReadWord(Addr, (uint32_t *)TouchDataValues, ((TouchCountScreen > 5) ? 5 : TouchCountScreen) * 4);
	if (rv != NRF_SUCCESS)
	{
		MKB_LOG_ERROR("I2C read failed!!");
		return (NRF_ERROR_INTERNAL);
	}

	if (TouchCountScreen > 5)
	{
		Addr = IST40_HCOM_GET_EXT_COORD;
		IST40xx_I2C_ReadWord(Addr, (uint32_t *)(TouchDataValues + 5), (TouchCountScreen - 5) * 4);
	}

	MKB_LOG_INFO("Touch Test Result : ");
	MKB_LOG_INFO("\tNumber Of Touch Info : %d", TouchCountScreen);

	// Screen Touch Info
	for (i = 0; i < TouchCountScreen; i++)
	{
		MKB_LOG_RAW_INFO("\tID : Screen %d, ", TouchDataValues[i].TouchId);
		if ((TouchDataValues[i].TouchStatus == IST40_TOUCH_PRESS) || (TouchDataValues[i].TouchStatus == IST40_TOUCH_MOVE))
		{
			X = (TouchDataValues[i].X_11_4 << 4) | TouchDataValues[i].X_3_0;
			Y = (TouchDataValues[i].Y_11_4 << 4) | TouchDataValues[i].Y_3_0;
			MKB_LOG_INFO("Pressed, x : %d, y : %d, z : %d", X, Y, TouchDataValues[i].Z);
		}
		else
		{
			MKB_LOG_INFO("Released");
		}
	}

	return NRF_SUCCESS;
}

////////////////////////////////////////
// Read TSP Information
////////////////////////////////////////
ret_code_t IST40xx_Read_TSPInfo(void)
{
	uint32_t i;
	uint32_t Addr;
	uint32_t aComInfo[0x20] = {
		0,
	};
	uint32_t MakeComChecksum;

	Addr = IST40_HCOM_GET_TSP_INFO_ADDR;
	if (IST40xx_I2C_ReadWord(Addr, aComInfo, (sizeof(aComInfo) / 4)))
	{
		MKB_LOG_ERROR("Cannot Read TSP Information");
		return NRF_ERROR_INTERNAL;
	}

	MakeComChecksum = 0;
	for (i = 0; i < (IST40_HCOM_GET_MAX - 1); i++)
	{
		MakeComChecksum += aComInfo[i];
	}

	gTSPInfo.ChipId = aComInfo[IST40_HCOM_GET_CHIP_ID];

	gTSPInfo.CoreVer = aComInfo[IST40_HCOM_GET_VER_CORE];
	gTSPInfo.FwVer = aComInfo[IST40_HCOM_GET_VER_FW];
	gTSPInfo.ProjectVer = aComInfo[IST40_HCOM_GET_VER_PROJECT];
	gTSPInfo.TestVer = aComInfo[IST40_HCOM_GET_VER_TEST];

	gTSPInfo.LcdWidth = aComInfo[IST40_HCOM_GET_LCD_INFO] >> 16;
	gTSPInfo.LcdHeight = aComInfo[IST40_HCOM_GET_LCD_INFO] & 0xFFFF;

	gTSPInfo.NumOfRx = aComInfo[IST40_HCOM_GET_TSP_INFO] >> 16;
	gTSPInfo.NumOfTx = aComInfo[IST40_HCOM_GET_TSP_INFO] & 0xFFFF;

	gTSPInfo.NumOfScrRx = aComInfo[IST40_HCOM_GET_SCR_INFO] >> 16;
	gTSPInfo.NumOfScrTx = aComInfo[IST40_HCOM_GET_SCR_INFO] & 0xFFFF;

	gTSPInfo.Crc32 = aComInfo[IST40_HCOM_GET_CRC32];

	gTSPInfo.PCalResult = aComInfo[IST40_HCOM_GET_PCAL_RESULT];
	gTSPInfo.CalResult = aComInfo[IST40_HCOM_GET_CAL_RESULT];
	gTSPInfo.SCalResult = aComInfo[IST40_HCOM_GET_SCAL_RESULT];

	gTSPInfo.SwapInfo = aComInfo[IST40_HCOM_GET_SWAP_INFO];
	gTSPInfo.ComChecksum = aComInfo[IST40_HCOM_GET_COM_CHECKSUM];

	gTSPInfo.CsTRxAddr = aComInfo[IST40_HCOM_GET_CS_TRX_RES_BASE];
	gTSPInfo.SelfTestAddr = aComInfo[IST40_HCOM_GET_SELFTEST_BASE];
	gTSPInfo.ProxCDCAddr = aComInfo[IST40_HCOM_GET_PROX_CDC_BASE];
	gTSPInfo.SelfCDCAddr = IST40_MEMX_CDC_SLF;
	gTSPInfo.CDCAddr = IST40_MEMX_CDC_MTL;

	gTSPInfo.ScrBl = (aComInfo[IST40_HCOM_GET_BASELINE_TH] >> 16) & 0xFFFF;
	gTSPInfo.ScrTh = aComInfo[IST40_HCOM_GET_BASELINE_TH] & 0xFFFF;

	if (gTSPInfo.ComChecksum != MakeComChecksum)
	{
		MKB_LOG_ERROR("checksum fail - Get, Make: 0x%08X, 0x%08X", gTSPInfo.ComChecksum, MakeComChecksum);
		return NRF_ERROR_INTERNAL;
	}

	return NRF_SUCCESS;
}

void IST40xx_Display_TSPInfo(void)
{
	MKB_LOG_RAW_INFO("Chip ID         : 0x%08X\r\n", gTSPInfo.ChipId);

	MKB_LOG_RAW_INFO("Core Ver        : 0x%08X\r\n", gTSPInfo.CoreVer);
	MKB_LOG_RAW_INFO("FW Ver          : 0x%08X\r\n", gTSPInfo.FwVer);
	MKB_LOG_RAW_INFO("Project Ver     : 0x%08X\r\n", gTSPInfo.ProjectVer);
	MKB_LOG_RAW_INFO("Test Ver        : 0x%08X\r\n", gTSPInfo.TestVer);

	MKB_LOG_RAW_INFO("width, height   : %d, %d\r\n", gTSPInfo.LcdWidth, gTSPInfo.LcdHeight);
	MKB_LOG_RAW_INFO("Rx-Ch, Tx-Ch    : %d, %d\r\n", gTSPInfo.NumOfRx, gTSPInfo.NumOfTx);
	MKB_LOG_RAW_INFO("ScrRx, ScrTx    : %d, %d\r\n", gTSPInfo.NumOfScrRx, gTSPInfo.NumOfScrTx);

	MKB_LOG_RAW_INFO("Screen Baseline : %d\r\n", gTSPInfo.ScrBl);

	MKB_LOG_RAW_INFO("Crc32           : 0x%08X\r\n", gTSPInfo.Crc32);
	MKB_LOG_RAW_INFO("Calib Result    : 0x%08X\r\n", gTSPInfo.CalResult);
	MKB_LOG_RAW_INFO("Self Cal Result : 0x%08X\r\n", gTSPInfo.SCalResult);
	MKB_LOG_RAW_INFO("Prox Cal Result : 0x%08X\r\n", gTSPInfo.PCalResult);

	if (gTSPInfo.SwapInfo & IST40_SWAP_XY)
	{
		MKB_LOG_RAW_INFO("Swap Filp Info  : Swap");
	}
	else
	{
		MKB_LOG_RAW_INFO("Swap Filp Info  : No Swap");
	}

	MKB_LOG_RAW_INFO("\r\n");

	MKB_LOG_RAW_INFO("Com Checksum    : 0x%08X\r\n", gTSPInfo.ComChecksum);
}

ret_code_t ist40_init(void)
{
	MKB_LOG_INFO("INIT start");
	if (IST40xx_Ctrl_HW_Reset(true))
	{
		MKB_LOG_ERROR("IC Reset Failed");
		return NRF_ERROR_INTERNAL;
	}

	// Read TSP Module Info (IC Type, FW version, etc)
	if (IST40xx_Read_TSPInfo())
	{
		MKB_LOG_ERROR("Error Reading TSP Information");
		return NRF_ERROR_INTERNAL;
	}
	IST40xx_Display_TSPInfo();

	return NRF_SUCCESS;
}

//-------------------------------------------------------------------------------------------------------------------------
//			ist40_read_process
//-------------------------------------------------------------------------------------------------------------------------
ret_code_t ist40_read_process(ist40_info_t *pInfo)
{
#if 0
	ret_code_t rv;
	uint16_t correction;
	uint32_t i, valid_touch;

	cypress_header_t	*pHeader; // len, report_id, timestamp, touch_count
	cypress_packet_t	*pPacket; //len, pHeader, body

	cypress_finger_info_t	*pInfo_finger; //touchType touchID eventID tip x y pressure majorAxis minorAxis orient
	cypress_touch_info_t 	*pPacketInfo; //touchType touchID eventID tip x y majoraxis minorAxis orientation
		
	// get Header from Cypress Received Packets
	pHeader = (cypress_header_t *)m_cypress_rdbuff;
	
	if( ( pHeader->len < CYPRESS_RECORD_HEADER) ) 
	{
		// 터치를 하지 않아서 터치 정보가 없을때
		MKB_LOG_WARN("data length short len: %d, report ID: 0x%02x, command: 0x%02x", 
                    pHeader->len, pHeader->report_id, m_cypress_rdbuff[4]);
		return NRF_ERROR_INVALID_LENGTH;//9
	}

#if 0
	MKB_LOG_RAW_DEBUG("report_id: %d, timestamp: %5d, touch_count: %d, LO: %d, noise: %d, counter: %d\n",
					pHeader->report_id, pHeader->timestamp, pHeader->touch_count,
					pHeader->lo, pHeader->noise, pHeader->counter);
#endif

	if (pHeader->report_id != 1) 
	{
		//report id id 0x01
		MKB_LOG_WARN("mismatch report id!");
		return NRF_ERROR_INTERNAL;//3
	}
	
	if(pHeader->len > CYPRESS_DATA_REQ_LEN) 
	{ 
		//받아온 데이터 길이가 이상할때
		MKB_LOG_WARN("data length too much!");
		return NRF_ERROR_INVALID_LENGTH;//9
	}

	// less than 1 touches
	if (pHeader->touch_count <= 0) 
	{
		//터치 손가락갯수가 없음
		MKB_LOG_INFO("no finger!");
		
		pInfo->touchCnt = 0;
		pInfo->timestamp = pHeader->timestamp;
		return NRF_SUCCESS;//0
	}

	// more than 10 touches
	if (pHeader->touch_count > CYPRESS_INFO_MAX_COUNT) 
	{
		//터치 손가락갯수가 10개 넘음
		MKB_LOG_WARN("too much finger!");
		return NRF_ERROR_INTERNAL;//3
	}

	if(pHeader->len >= CYPRESS_DATA_ONE_LEN)
	{
		//받아온 패킷 저장
		pPacket = (cypress_packet_t *)m_cypress_rdbuff;
		
		//touch_count 8개까지.(break없음)	
		switch (pHeader->touch_count) 
		{
			case 1:
			case 2:
			case 3:
			case 4:		
			case 5:
			case 6:
			case 7:
			case 8:					
			case 9:
			case 10:					
				for(i = 0, valid_touch = 0; i < pHeader->touch_count; i++) 
				{			
					pPacketInfo  = &(pPacket->body.info[i]);////touchType touchID eventID tip x y majoraxis minorAxis orientation

                    if(pPacketInfo->eventID == 1)
					MKB_LOG_RAW_DEBUG("%d: type:%d id:%d event:%d {x:%04d,y:%04d} tip:%d pressure:%d\n",i,
								pPacketInfo->touchType,pPacketInfo->touchID,pPacketInfo->eventID,
								pPacketInfo->x,pPacketInfo->y,pPacketInfo->tip,pPacketInfo->pressure);

#if 0				
					//클리핑 영역 벗어났는지
					if(NRF_SUCCESS != cypress_check_valid_range(pPacket->body.info[i].x, pPacket->body.info[i].y)) 
					{					
						continue; // 멈추고 다음손가락 체크함
					}

					//qwer열 터치 휜거 보정
					correction = correct(pPacket->body.info[i].y);
					if(pPacket->body.info[i].x > correction)
					{
						pPacket->body.info[i].x -= correction;
					}
#endif			
					
					pInfo_finger = &(pInfo->info[valid_touch]);//현재 손가락의 터치 데이터 복사

					/*
					Event ID[1:0]: Indicates an event associated with this touch instance.
					0 = No event
					1 = Touchdown
					2 = Significant displacement (> active distance)
					3 = Liftoff (record reports last known coordinates)
					터치패드에서의 상태 전환: 손을 대면(1) -> 이동(2) -> 손을 떼면(3)
					*/
					if(pPacketInfo->eventID >= 1 && pPacketInfo->eventID <= 3)
					{
						pInfo_finger->touchType 	= pPacketInfo->touchType;
						pInfo_finger->touchID 		= valid_touch;	// <--> pPacketInfo->touchID;
						pInfo_finger->eventID 		= pPacketInfo->eventID;
						pInfo_finger->tip 			= pPacketInfo->tip;
						pInfo_finger->x 			= pPacketInfo->x; // = abs(4000 - pPacketInfo->x);
						pInfo_finger->y 			= pPacketInfo->y;
						pInfo_finger->presure 		= pPacketInfo->pressure;
						pInfo_finger->axis 			= pPacketInfo->axis;
						//pInfo_finger->major_axis 	= pPacketInfo->major_axis;
						//pInfo_finger->minor_axis 	= pPacketInfo->minor_axis;
						pInfo_finger->orient 		= pPacketInfo->orientation;
						//----------------------------------------------------------------------------------------------
						// Increse counter
						valid_touch++;
					}
				}
				
				break;

			default:
				// INFO: do not support more than 4 touch			
                MKB_LOG_WARN("can't support finger!");
                return NRF_ERROR_NOT_SUPPORTED;
		}

		if(valid_touch)
		{
			//CYPRESS_LOG_INFO("valid touch fingers: %d\n", valid_touch);
			pInfo->touchCnt = valid_touch;//손가락 번호
			pInfo->timestamp = pHeader->timestamp;
			return NRF_SUCCESS;//0
		}
		else
		{
			//validTouch 없음 14
			//클리핑된 영역안에 있을때 보통 리턴됨
			//return NRF_ERROR_NOT_FOUND; 
			MKB_LOG_WARN("invalid touch count!");
			return NRF_ERROR_NULL;
		}
	}
	else
	{
		// 헤더 사이즈보다 크지만 1개 데이터 사이즈 보다 작을때
		MKB_LOG_WARN("data length short len: %d!", pHeader->len);
		return NRF_ERROR_INVALID_LENGTH;//9
	}
#endif
}

static void ist40_read_cb(ret_code_t result, void *p_user_data)
{
	ret_code_t err_code;

	if (result == NRF_SUCCESS)
	{
		err_code = ist40_read_process(p_user_data);

		if (err_code == NRF_SUCCESS && m_ist40_read_handler)
			m_ist40_read_handler(result, p_user_data);
	}

	nrf_atomic_flag_clear(&m_ist40_operation_active);
}

static ret_code_t ist40_read_schedule(ist40_info_t *p_data)
{
	ret_code_t rv;

	if (nrf_atomic_flag_set_fetch(&m_ist40_operation_active))
	{
		return NRF_ERROR_BUSY;
	}

	//rv = twi_perform(NULL, ist40_read_length_transfers, sizeof(ist40_read_length_transfers)/sizeof(nrf_twi_mngr_transfer_t));
	if (NRF_SUCCESS != rv)
	{
		nrf_atomic_flag_clear(&m_ist40_operation_active);
		MKB_LOG_WARN("read Fail!");
		return NRF_ERROR_INTERNAL; //3
	}

	//ist40_read_transfers[1].length = m_ist40_rdbuff[0];

	ist40_read_transaction.p_user_data = p_data;

	rv = twi_schedule(&ist40_read_transaction);
	if (rv != NRF_SUCCESS)
		nrf_atomic_flag_clear(&m_ist40_operation_active);

	return rv;
}

static void ist40_scan_handler(void *p_event_data, uint16_t event_size)
{
	UNUSED_PARAMETER(p_event_data);
	UNUSED_PARAMETER(event_size);

	ret_code_t err_code;

	if (ist40_read_INT() == IST40_PIN_LOW)
	{
		//ist40_read_schedule(&m_touch_info);
		IST40xx_Ctrl_ReadTouchData();
	}

	if (m_ist40_enabled)
	{
#if (IST40_MODE == IST40_MODE_GPIOTE)
		err_code = ist40_sense_enable();
		if (err_code != NRF_SUCCESS)
		{
			MKB_ERROR_CHECK(err_code);
		}
#endif /* (IST40_MODE == IST40_MODE_GPIOTE) */
	}
}

#if (IST40_MODE == IST40_MODE_GPIOTE)
/**@brief GPIOTE PORT interrupt handler.*/
static void ist40_interrupt_handler(uint32_t const *p_event_pins_low_to_high,
									uint32_t const *p_event_pins_high_to_low)
{
	if (m_ist40_enabled)
	{
		MKB_LOG_DEBUG("Interrupt: 0x%x", *p_event_pins_high_to_low);

		if (*p_event_pins_high_to_low & IST40_GPIOTE_MASK)
		{
			// Disable sensing.
			ist40_sense_disable();

			// Schedule cypress_scan
			ret_code_t ret_code = app_sched_event_put(NULL, 0, ist40_scan_handler);
			MKB_ERROR_CHECK(ret_code);
		}
	}
}
#elif (IST40_MODE == IST40_MODE_TIMER)
static void ist40_timeout_handler(void *p_context)
{
	UNUSED_PARAMETER(p_context);

	if (m_ist40_enabled)
	{
		ret_code_t ret_code = app_sched_event_put(NULL, 0, ist40_scan_handler);
		MKB_ERROR_CHECK(ret_code);
	}
}
#endif /* (IST40_MODE == IST40_MODE_GPIOTE) */

ret_code_t drv_ist40_reset(void)
{
	ret_code_t err_code;

	if (!m_ist40_enabled)
	{
		if (IST40xx_Ctrl_HW_Reset(true))
		{
			MKB_LOG_ERROR("IC Reset Failed");
			return NRF_ERROR_INTERNAL;
		}
	}

	return NRF_SUCCESS;
}

ret_code_t drv_ist40_enable(void)
{
	ret_code_t err_code;

	if (!m_ist40_enabled)
	{
#if (IST40_MODE == IST40_MODE_GPIOTE)
		err_code = ist40_sense_enable();
#elif (IST40_MODE == IST40_MODE_TIMER)
		err_code = app_timer_start(m_ist40_timer, APP_TIMER_TICKS(IST40_TIMER_INTERVAL), NULL);
#endif /* (IST40_MODE == IST40_MODE_GPIOTE) */
		if (err_code != NRF_SUCCESS)
		{
			MKB_ERROR_CHECK(err_code);
			return err_code;
		}

		MKB_LOG_INFO("Enable Done!");

		m_ist40_enabled = true;
	}

	return NRF_SUCCESS;
}

ret_code_t drv_ist40_disable(void)
{
	ret_code_t err_code;

	if (m_ist40_enabled)
	{
		m_ist40_enabled = false;

#if (IST40_MODE == IST40_MODE_GPIOTE)
		err_code = ist40_sense_disable();
#elif (IST40_MODE == IST40_MODE_TIMER)
		err_code = app_timer_stop(m_ist40_timer);
#endif /* (IST40_MODE == IST40_MODE_GPIOTE) */
		if (err_code != NRF_SUCCESS)
		{
			MKB_ERROR_CHECK(err_code);
			return err_code;
		}
	}

	m_ist40_enabled = false;

	return NRF_SUCCESS;
}

ret_code_t drv_ist40_init(ist40_read_handler_t read_handler)
{
	ret_code_t err_code;

	// Initialize driver state.
	nrf_atomic_flag_clear(&m_ist40_operation_active);

	m_ist40_read_handler = read_handler;

	m_ist40_enabled = false;
	m_ist40_bootloader = false;

	ist40_gpio();

#if (IST40_MODE == IST40_MODE_GPIOTE)
	uint32_t low_to_high_mask = 0;
	uint32_t high_to_low_mask = IST40_GPIOTE_MASK;

	err_code = app_gpiote_user_register(&m_ist40_gpiote,
										&low_to_high_mask,
										&high_to_low_mask,
										ist40_interrupt_handler);
	if (err_code != NRF_SUCCESS)
	{
		MKB_ERROR_CHECK(err_code);
		return err_code;
	}

	err_code = ist40_sense_disable();
	if (err_code != NRF_SUCCESS)
	{
		MKB_ERROR_CHECK(err_code);
		return err_code;
	}

	uint32_t gpio_pins_state;
	err_code = app_gpiote_pins_state_get(m_ist40_gpiote, &gpio_pins_state);
	if (err_code != NRF_SUCCESS)
	{
		// Failed to read state information. Take corrective action.
		MKB_LOG_ERROR("GPIO PIN STATE");
	}
	else
	{
		if (gpio_pins_state & IST40_GPIOTE_MASK) // Checks if pin one and two are set
		{
			// Take necessary action
			MKB_LOG_INFO("GPIOTE setting pins (0x%x)", gpio_pins_state);
		}
	}
#elif (IST40_MODE == IST40_MODE_TIMER)
	err_code = app_timer_create(&m_ist40_timer, APP_TIMER_MODE_REPEATED, ist40_timeout_handler);
	if (err_code != NRF_SUCCESS)
	{
		MKB_ERROR_CHECK(err_code);
		return err_code;
	}
#endif /* (IST40_MODE == IST40_MODE_GPIOTE) */

	ist40_init();

	MKB_LOG_INFO("Initialize Done!");

	return NRF_SUCCESS;
}

#if (defined(NRF_PWR_MGMT_ENABLED) && NRF_PWR_MGMT_ENABLED)
static bool mkb_ist40_shutdown(nrf_pwr_mgmt_evt_t event)
{
	ret_code_t err_code;

	MKB_LOG_INFO("Shutdown prepare");

	if (m_ist40_enabled)
	{
		m_ist40_enabled = false;

#if (IST40_MODE == IST40_MODE_GPIOTE)
		err_code = ist40_sense_disable();
#elif (IST40_MODE == IST40_MODE_TIMER)
		err_code = app_timer_stop(m_ist40_timer);
#endif /* (IST40_MODE == IST40_MODE_GPIOTE) */
		if (err_code != NRF_SUCCESS)
		{
			MKB_ERROR_CHECK(err_code);
			return err_code;
		}
	}

	nrf_gpio_cfg_default(IST40_INT_PIN);
	nrf_gpio_cfg_default(IST40_POWER_PIN);

	return true;
}

NRF_PWR_MGMT_HANDLER_REGISTER(mkb_ist40_shutdown, SHUTDOWN_PRIORITY_DEFAULT);
#endif /* NRF_PWR_MGMT_ENABLED */

#endif // (defined(MKB_IST40_ENABLED) && MKB_IST40_ENABLED)
