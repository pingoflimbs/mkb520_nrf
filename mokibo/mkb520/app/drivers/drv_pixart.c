/*
 * ===============================================================================
 * Copyright (C) 2017-2019
 * INNOPRESSO INC. All Rights Reserved
 *
 * The content of this file is copyrighted by INNOPRESSO or its
 * affiliated, associated or related companies. All copyrights and other
 * associated intellectual property rights related thereto are expressly
 * reserved by INNOPRESSO INC.
 * ===============================================================================
 */

/** @file
 *
 * @defgroup ble_app_hids_mkb316_driver drv_pixart.c
 * @{
 * @ingroup ble_app_hids_mkb316
 * @brief HID Touchpad Keyboard Application pixart driver file.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "sdk_common.h"
#include "app_error.h"
#include "app_timer.h"
#include "app_scheduler.h"
#if defined(GPIOTE_ENABLED) && GPIOTE_ENABLED
#include "app_gpiote.h"
#endif

#include "nrf_atomic.h"
#include "nrf_assert.h"
#include "nrf_error.h"
#include "nrf_gpio.h"
#include "nrf_log.h"
#include "nrf_delay.h"
#include "nrf_pwr_mgmt.h"

#define MKB_LOG_LEVEL MKB_PIXART_LOG_LEVEL
#include "mkb_common.h"
#include "drv_twi.h"
#include "drv_pixart.h"
#include "math.h"

#if (defined(MKB_WDT_ENABLED) && MKB_WDT_ENABLED)
#include "mkb_wdt.h"
#endif

#include "mkb_led.h"
#include "mkb_state_manager.h"

#include "drv_pixart_dfu_mkb420_0811_v140A.h"

// TEST

#include "nrf_log.h"
#include "nrf_log_ctrl.h"

#if (defined(MKB_PIXART_ENABLED) && MKB_PIXART_ENABLED)

APP_TIMER_DEF(m_pixart_timer);
APP_TIMER_DEF(m_pixart_watchdog_timer);

#if (PIXART_MODE == PIXART_MODE_GPIOTE)
static app_gpiote_user_id_t m_pixart_gpiote; //!< GPIOTE Handle.
#endif

#ifdef ________DBG_20211117_check_for_i2c_stop_sprintf
char logbuf[512] = {
    0,
};
#endif

static bool m_pixart_init;
static bool m_pixart_enabled;
static volatile bool m_pixart_sense_enabled;
static bool m_pixart_power_down;
static int16_t m_cnt_interrupted;
static volatile uint8_t m_cnt_scanned;
static volatile uint8_t m_cnt_timered;

bool m_is_scanhandler_run = false;
static uint8_t m_pixart_idle_cnt;
static uint8_t m_pixart_key_cnt;
static uint8_t m_pixart_mod_key_cnt;
static uint8_t m_pixart_read;
static uint8_t m_pixart_rdbuff[PIXART_BUFFER_SIZE];
static uint8_t m_pixart_wrbuff[PIXART_BUFFER_SIZE];
static mokibo_touch_data_t m_touch_info;

/**@brief Variable protecting shared data used in read operation */
static nrf_atomic_flag_t m_pixart_operation_active;

static pixart_read_handler_t m_pixart_read_touch_handler = NULL;
static pixart_key_read_handler_t m_pixart_read_keys_handler = NULL;

static void pixart_interrupt_handler();
static void pixart_scan_handler();
static ret_code_t pixart_status();
static ret_code_t pixart_read_schedule(mokibo_touch_data_t *p_data); // p_data 안쓰고있음.
static ret_code_t pixart_read_report();
static void pixart_read_cb(ret_code_t result, void *p_user_data); // result 안쓰고있음.

static uint8_t NRF_TWI_MNGR_BUFFER_LOC_IND pixart_cmd_boot_ready[] =
    {
        PIXART_REG_BOOT_READY,
};
static uint8_t NRF_TWI_MNGR_BUFFER_LOC_IND pixart_cmd_status[] =
    {
        PIXART_REG_STATUS,
};
static uint8_t NRF_TWI_MNGR_BUFFER_LOC_IND pixart_cmd_ack_clear[] =
    {
        PIXART_REG_STATUS,
        0x00};
static uint8_t NRF_TWI_MNGR_BUFFER_LOC_IND pixart_cmd_disable_deepsleep[] =
    {
        PIXART_REG_DISABLE,
        0x01};
static uint8_t NRF_TWI_MNGR_BUFFER_LOC_IND pixart_cmd_sleep_status[] =
    {
        PIXART_REG_DISABLE,
};
static uint8_t NRF_TWI_MNGR_BUFFER_LOC_IND pixart_cmd_host_reset_enable[] =
    {
        PIXART_REG_ENABLE,
        0x00};
static uint8_t NRF_TWI_MNGR_BUFFER_LOC_IND pixart_cmd_software_reset1[] =
    {
        PIXART_REG_COMMAND,
        0xaa};
static uint8_t NRF_TWI_MNGR_BUFFER_LOC_IND pixart_cmd_software_reset2[] =
    {
        PIXART_REG_COMMAND,
        0xbb};
static uint8_t NRF_TWI_MNGR_BUFFER_LOC_IND pixart_cmd_suspend_enable[] =
    {
        PIXART_REG_ENABLE,
        0x01};
static uint8_t NRF_TWI_MNGR_BUFFER_LOC_IND pixart_cmd_suspend[] =
    {
        PIXART_REG_COMMAND,
        0x99};
static uint8_t NRF_TWI_MNGR_BUFFER_LOC_IND pixart_cmd_resume[] =
    {
        PIXART_REG_COMMAND,
        0xbb};
static uint8_t NRF_TWI_MNGR_BUFFER_LOC_IND pixart_cmd_read_report_c1[] =
    {
        0x09,
        0x08};
static uint8_t NRF_TWI_MNGR_BUFFER_LOC_IND pixart_cmd_read_report_c2[] =
    {
        0x0a,
        0x00};
static uint8_t NRF_TWI_MNGR_BUFFER_LOC_IND pixart_cmd_read_report_e[] =
    {
        0x0a,
        0x01};
static uint8_t NRF_TWI_MNGR_BUFFER_LOC_IND pixart_cmd_read_report_rs[] =
    {
        0x0b,
};
static uint8_t NRF_TWI_MNGR_BUFFER_LOC_IND pixart_cmd_read_report_ra[] =
    {
        0x8b,
};

static nrf_twi_mngr_transfer_t const pixart_boot_ready_transfers[] =
    {
        NRF_TWI_MNGR_WRITE(PIXART_ADDR, pixart_cmd_boot_ready, sizeof(pixart_cmd_boot_ready), NRF_TWI_MNGR_NO_STOP),
        NRF_TWI_MNGR_READ(PIXART_ADDR, m_pixart_rdbuff, 1, 0)};
static nrf_twi_mngr_transfer_t const pixart_status_transfers[] =
    {
        NRF_TWI_MNGR_WRITE(PIXART_ADDR, pixart_cmd_status, sizeof(pixart_cmd_status), NRF_TWI_MNGR_NO_STOP),
        NRF_TWI_MNGR_READ(PIXART_ADDR, m_pixart_rdbuff, 1, 0)};
static nrf_twi_mngr_transfer_t const pixart_ack_clear_transfers[] =
    {
        NRF_TWI_MNGR_WRITE(PIXART_ADDR, pixart_cmd_ack_clear, sizeof(pixart_cmd_ack_clear), 0),
};
static nrf_twi_mngr_transfer_t const pixart_disable_deepsleep_transfers[] =
    {
        NRF_TWI_MNGR_WRITE(PIXART_ADDR, pixart_cmd_disable_deepsleep, sizeof(pixart_cmd_disable_deepsleep), 0),
};
static nrf_twi_mngr_transfer_t const pixart_sleep_status_transfers[] =
    {
        NRF_TWI_MNGR_WRITE(PIXART_ADDR, pixart_cmd_sleep_status, sizeof(pixart_cmd_sleep_status), NRF_TWI_MNGR_NO_STOP),
        NRF_TWI_MNGR_READ(PIXART_ADDR, m_pixart_rdbuff, 1, 0)};
static nrf_twi_mngr_transfer_t const pixart_software_reset1_transfers[] =
    {
        NRF_TWI_MNGR_WRITE(PIXART_ADDR, pixart_cmd_host_reset_enable, sizeof(pixart_cmd_host_reset_enable), 0),
        NRF_TWI_MNGR_WRITE(PIXART_ADDR, pixart_cmd_software_reset1, sizeof(pixart_cmd_software_reset1), 0),
};
static nrf_twi_mngr_transfer_t const pixart_software_reset2_transfers[] =
    {
        NRF_TWI_MNGR_WRITE(PIXART_ADDR, pixart_cmd_software_reset2, sizeof(pixart_cmd_software_reset2), 0),
};
static nrf_twi_mngr_transfer_t const pixart_suspend_transfers[] =
    {
        NRF_TWI_MNGR_WRITE(PIXART_ADDR, pixart_cmd_suspend_enable, sizeof(pixart_cmd_suspend_enable), 0),
        NRF_TWI_MNGR_WRITE(PIXART_ADDR, pixart_cmd_suspend, sizeof(pixart_cmd_suspend), 0),
};
static nrf_twi_mngr_transfer_t const pixart_resume_transfers[] =
    {
        NRF_TWI_MNGR_WRITE(PIXART_ADDR, pixart_cmd_resume, sizeof(pixart_cmd_resume), 0),
};
static nrf_twi_mngr_transfer_t const pixart_read_report_start_transfers[] =
    {
        NRF_TWI_MNGR_WRITE(PIXART_ADDR, pixart_cmd_read_report_c1, sizeof(pixart_cmd_read_report_c1), 0),
        NRF_TWI_MNGR_WRITE(PIXART_ADDR, pixart_cmd_read_report_c2, sizeof(pixart_cmd_read_report_c2), 0),
};
static nrf_twi_mngr_transfer_t const pixart_read_report_single_transfers[] =
    {
        NRF_TWI_MNGR_WRITE(PIXART_ADDR, pixart_cmd_read_report_rs, sizeof(pixart_cmd_read_report_rs), NRF_TWI_MNGR_NO_STOP),
};
static nrf_twi_mngr_transfer_t const pixart_read_report_multi_transfers[] =
    {
        NRF_TWI_MNGR_WRITE(PIXART_ADDR, pixart_cmd_read_report_c1, sizeof(pixart_cmd_read_report_c1), 0),
        NRF_TWI_MNGR_WRITE(PIXART_ADDR, pixart_cmd_read_report_c2, sizeof(pixart_cmd_read_report_c2), 0),
        NRF_TWI_MNGR_WRITE(PIXART_ADDR, pixart_cmd_read_report_ra, sizeof(pixart_cmd_read_report_ra), NRF_TWI_MNGR_NO_STOP),
        NRF_TWI_MNGR_READ(PIXART_ADDR, m_pixart_rdbuff, PIXART_DATA_REQ_LEN, 0)};
static nrf_twi_mngr_transfer_t const pixart_read_report_loop_transfers[] =
    {
        NRF_TWI_MNGR_READ(PIXART_ADDR, &m_pixart_read, 1, 0)};
static nrf_twi_mngr_transfer_t const pixart_read_report_end_transfers[] =
    {
        NRF_TWI_MNGR_WRITE(PIXART_ADDR, pixart_cmd_read_report_e, sizeof(pixart_cmd_read_report_e), 0),
};

#if (PIXART_MODE == PIXART_MODE_GPIOTE)
/**@brief Enable interrupt-based key press sensing. */
static ret_code_t pixart_sense_enable(void)
{
    ________DBG_20211202_drv_pixart_sense_trace("pixart_sense_enabled");
    // Enable GPIOTE sensing.
    m_pixart_sense_enabled = true;
    return app_gpiote_user_enable(m_pixart_gpiote);
}

/**@brief Disable interrupt-based key press sensing. */
static ret_code_t pixart_sense_disable(void)
{
    ________DBG_20211202_drv_pixart_sense_trace("pixart_sense_disable");
    // Disable GPIOTE sensing.
    m_pixart_sense_enabled = false;
    return app_gpiote_user_disable(m_pixart_gpiote);
    return NRF_SUCCESS;
}
#endif /* (PIXART_MODE == PIXART_MODE_GPIOTE) */

static ret_code_t pixart_init(void);

//-------------------------------------------------------------------------------------------------------------------------
//			pixart_power_down
//-------------------------------------------------------------------------------------------------------------------------
static void pixart_power_down(void)
{
// U6 power off - set LOW : @ P.25
#if (defined(MKB_PIXART_POWER_PIN_ENABLED) && MKB_PIXART_POWER_PIN_ENABLED)
    nrf_gpio_pin_write(PIXART_POWER_PIN, PIXART_PIN_LOW); // P.25 Off
#endif                                                    //(defined(MKB_PIXART_POWER_PIN_ENABLED) && MKB_PIXART_POWER_PIN_ENABLED)
    //==> power-off된 pixart touch는, i2c bus를 GND로 붙잡아서 않된다.
    m_pixart_power_down = true;
}

//-------------------------------------------------------------------------------------------------------------------------
//			pixart_power_up
//-------------------------------------------------------------------------------------------------------------------------
static void pixart_power_up(void)
{
// U6 power on - set HIGH : @ P.25
#if (defined(MKB_PIXART_POWER_PIN_ENABLED) && MKB_PIXART_POWER_PIN_ENABLED)
    nrf_gpio_pin_write(PIXART_POWER_PIN, PIXART_PIN_HIGH); // P.25 ON
#endif                                                     //(defined(MKB_PIXART_POWER_PIN_ENABLED) && MKB_PIXART_POWER_PIN_ENABLED)
    m_pixart_power_down = false;
}

//-----------------------------------------------------------------------------------------------------------------------------------
//			pixart_reset_low
//-----------------------------------------------------------------------------------------------------------------------------------
static void pixart_reset_low(void)
{
// reset pin : @ P.29 --> make it 'low'
#if (defined(MKB_PIXART_RESET_PIN_ENABLED) && MKB_PIXART_RESET_PIN_ENABLED)
    nrf_gpio_pin_write(PIXART_RESET_PIN, PIXART_PIN_LOW); // P.29 Reset Pin --> to 'low'
#endif                                                    //(defined(MKB_PIXART_RESET_PIN_ENABLED) && MKB_PIXART_RESET_PIN_ENABLED)
}

//-----------------------------------------------------------------------------------------------------------------------------------
//			pixart_reset_high
//-----------------------------------------------------------------------------------------------------------------------------------
static void pixart_reset_high(void)
{
// reset pin : @ P.29 --> make it 'high'
#if (defined(MKB_PIXART_RESET_PIN_ENABLED) && MKB_PIXART_RESET_PIN_ENABLED)
    nrf_gpio_pin_write(PIXART_RESET_PIN, PIXART_PIN_HIGH); // P.29 Reset Pin --> to 'High'
#endif                                                     //(defined(MKB_PIXART_RESET_PIN_ENABLED) && MKB_PIXART_RESET_PIN_ENABLED)
}

//-----------------------------------------------------------------------------------------------------------------------------------
//			pixart_reset()
// Parade Touch Chip의 Reset(active low) 단자를 Low로 떨어뜨렸다가 HIgh로 올림.
//-----------------------------------------------------------------------------------------------------------------------------------
#if (defined(MKB_PIXART_RESET_PIN_ENABLED) && MKB_PIXART_RESET_PIN_ENABLED)
static void pixart_reset(void)
{
    pixart_reset_low();
    nrf_delay_ms(100); // for sure reset
    pixart_reset_high();
    // nrf_delay_ms (20);
}
#endif //(defined(MKB_PIXART_RESET_PIN_ENABLED) && MKB_PIXART_RESET_PIN_ENABLED)

//-----------------------------------------------------------------------------------------------------------------------------------
//			pixart_gpio()
//-----------------------------------------------------------------------------------------------------------------------------------
#include "nrf_gpio.h"
static void pixart_gpio(void)
{
#if (defined(MKB_PIXART_INT_PIN_ENABLED) && MKB_PIXART_INT_PIN_ENABLED)
    // nrf_gpio_cfg_default(PIXART_INT_PIN);
    // nrf_gpio_cfg_input(PIXART_INT_PIN, NRF_GPIO_PIN_PULLUP);
    nrf_gpio_pin_dir_set(PIXART_INT_PIN, NRF_GPIO_PIN_DIR_INPUT);
#endif //(defined(MKB_PIXART_INT_PIN_ENABLED) && MKB_PIXART_INT_PIN_ENABLED)

#if (defined(MKB_PIXART_POWER_PIN_ENABLED) && MKB_PIXART_POWER_PIN_ENABLED)
    nrf_gpio_pin_dir_set(PIXART_POWER_PIN, NRF_GPIO_PIN_DIR_OUTPUT);
#endif //(defined(MKB_PIXART_POWER_PIN_ENABLED) && MKB_PIXART_POWER_PIN_ENABLED)

#if (defined(MKB_PIXART_RESET_PIN_ENABLED) && MKB_PIXART_RESET_PIN_ENABLED)
    nrf_gpio_pin_dir_set(PIXART_RESET_PIN, NRF_GPIO_PIN_DIR_OUTPUT);
#endif //(defined(MKB_PIXART_RESET_PIN_ENABLED) && MKB_PIXART_RESET_PIN_ENABLED)
}

//-----------------------------------------------------------------------------------------------------------------------------------
//			pixart_read_TT_INT
//-----------------------------------------------------------------------------------------------------------------------------------
static uint8_t pixart_read_TT_INT(void)
{
#if (defined(MKB_PIXART_INT_PIN_ENABLED) && MKB_PIXART_INT_PIN_ENABLED)
    uint8_t pinValue = nrf_gpio_pin_read(PIXART_INT_PIN);
    return pinValue; // P.30
#else
    return 0;
#endif //(defined(MKB_PIXART_INT_PIN_ENABLED) && MKB_PIXART_INT_PIN_ENABLED)
}

//-----------------------------------------------------------------------------------------------------------------------------------
//			pixart_waitGetLevel_TT_INT
//-----------------------------------------------------------------------------------------------------------------------------------
static ret_code_t pixart_waitGetLevel_TT_INT(uint8_t pinLevel, uint32_t ms)
{
    uint8_t pinValue = 0;                 //
    int32_t loop_count = ms * 1000 / 100; // signed... by Stanley
    //
    do
    {
#if (defined(MKB_WDT_ENABLED) && MKB_WDT_ENABLED)
        mkb_wdt_feed();
#endif

        pinValue = pixart_read_TT_INT();
        if (pinValue == pinLevel)
        {
            break;
        }
        //
        loop_count--;
        nrf_delay_us(50);
    } while (loop_count > 0);
    //

    if (loop_count <= 0)
    {
        pinValue = pixart_read_TT_INT(); // 50us 의 시간이 비었으므로 그동안 한번더 체크한다.
        if (pinValue == pinLevel)
        {
            return NRF_SUCCESS;
        }
        else
        {
            return NRF_ERROR_INVALID_STATE;
        }
    }
    return NRF_SUCCESS;
}
//-----------------------------------------------------------------------------------------------------------------------------------
//			pixart_is_TT_INT_low
//-----------------------------------------------------------------------------------------------------------------------------------
static ret_code_t pixart_is_TT_INT_low(uint32_t timeout_ms)
{
    return pixart_waitGetLevel_TT_INT(PIXART_PIN_LOW, timeout_ms); // 1ms wait
}
//-----------------------------------------------------------------------------------------------------------------------------------
//			pixart_is_TT_INT_high
//-----------------------------------------------------------------------------------------------------------------------------------
static ret_code_t pixart_is_TT_INT_high(uint32_t timeout_ms)
{
    return pixart_waitGetLevel_TT_INT(PIXART_PIN_HIGH, timeout_ms); // 1ms wait
}

/*
Bit[0]: When this bit is set, the firmware has finished its booting phase and the host can access user registers.
Bit[1]: When this bit is set, CPU can't get flash ID.
Bit[2]: When this bit is set, there is something wrong in flash IFB sector.
Bit[3]: When this bit is set, FW code in both flash banks are incorrect.
Bit[4]: When this bit is set, Boot to SRAM fail due to incorrect SRAM content.
Bit[5]: When this bit is set, user register sector is incorrect.
Bit[6]: Boot up FW selection: bank 0 or 1
Bit[7]: When this bit is set, the navigation is ready
*/
static ret_code_t pixart_boot_ready(void)
{

    ret_code_t rv;

    // buffer for the next reading
    memset(m_pixart_rdbuff, 0, 1);

    for (uint8_t i = 0; i < 50; i++)
    {
#if (defined(MKB_WDT_ENABLED) && MKB_WDT_ENABLED)
        mkb_wdt_feed();
#endif

        nrf_delay_ms(10);

        rv = twi_perform(NULL, pixart_boot_ready_transfers, sizeof(pixart_boot_ready_transfers) / sizeof(nrf_twi_mngr_transfer_t));
        if (rv != NRF_SUCCESS)
        {
            MKB_LOG_ERROR("I2C read failed");
            return NRF_ERROR_INTERNAL;
        }

        if (m_pixart_rdbuff[0] == 0x00)
        {
            ________DBG_20210810_pixart_boot_ready_message("booting message is 0x00");
        }
        if (m_pixart_rdbuff[0] & (1 << 0))
        {
            ________DBG_20210810_pixart_boot_ready_message("fisished booting, host can access user register");
        }
        if (m_pixart_rdbuff[0] & (1 << 1))
        {
            ________DBG_20210810_pixart_boot_ready_message("CPU can't get flash ID");
        }
        if (m_pixart_rdbuff[0] & (1 << 2))
        {
            ________DBG_20210810_pixart_boot_ready_message("there is something wrong in flash IFB sector.");
        }
        if (m_pixart_rdbuff[0] & (1 << 3))
        {
            ________DBG_20210810_pixart_boot_ready_message("FW code in both flash banks are incorrect.");
        }
        if (m_pixart_rdbuff[0] & (1 << 4))
        {
            ________DBG_20210810_pixart_boot_ready_message("Boot to SRAM fail due to incorrect SRAM content.");
        }
        if (m_pixart_rdbuff[0] & (1 << 5))
        {
            ________DBG_20210810_pixart_boot_ready_message("user register sector is incorrect.");
        }
        if ((m_pixart_rdbuff[0] & (1 << 6)) == true)
        {
            ________DBG_20210810_pixart_boot_ready_message("Boot up FW selection: bank 0");
        }
        if ((m_pixart_rdbuff[0] & (1 << 6)) == false)
        {
            ________DBG_20210810_pixart_boot_ready_message("Boot up FW selection: bank 1");
        }
        if (m_pixart_rdbuff[0] & (1 << 7))
        {
            ________DBG_20210810_pixart_boot_ready_message("the navigation is ready");
        }

        // the sentinel data should be 0x0000 ...
        if (m_pixart_rdbuff[0] & (1 << 0)) // if ((m_pixart_rdbuff[0] != 0x00))
        {

            return NRF_SUCCESS;
        }
    }

    MKB_LOG_ERROR("boot ready check failed");
    return NRF_ERROR_INTERNAL;
}

ret_code_t pixart_tx_usr(uint8_t bank, uint8_t reg, uint8_t data)
{
    ret_code_t rv;
    uint8_t tx_buff_usrbnk[] = {0x7F, 0x06};
    uint8_t tx_buff_indbnk[] = {0x73, bank};
    uint8_t tx_buff_indreg[] = {0x74, reg};
    uint8_t tx_buff_indval[] = {0x75, data};
    nrf_twi_mngr_transfer_t const test[] = {
        NRF_TWI_MNGR_WRITE(PIXART_ADDR, tx_buff_usrbnk, 2, 1),
        NRF_TWI_MNGR_WRITE(PIXART_ADDR, tx_buff_indbnk, 2, 1),
        NRF_TWI_MNGR_WRITE(PIXART_ADDR, tx_buff_indreg, 2, 1),
        NRF_TWI_MNGR_WRITE(PIXART_ADDR, tx_buff_indval, 2, 0),
    };

    rv = twi_perform(NULL, test, sizeof(test) / sizeof(nrf_twi_mngr_transfer_t));
    if (NRF_SUCCESS != rv)
    {
        MKB_LOG_ERROR("###Error");
        return (NRF_ERROR_INTERNAL);
    }
}

uint8_t pixart_rx_usr(uint8_t bank, uint8_t reg)
{
    ret_code_t rv;
    uint8_t result = 0;
    uint8_t tx_buff_usrbnk[] = {0x7F, 0x06};
    uint8_t tx_buff_indbnk[] = {0x73, bank};
    uint8_t tx_buff_indreg[] = {0x74, reg};
    uint8_t tx_buff_indval[] = {0x75};
    nrf_twi_mngr_transfer_t const transfer[] = {
        NRF_TWI_MNGR_WRITE(PIXART_ADDR, tx_buff_usrbnk, 2, 1),
        NRF_TWI_MNGR_WRITE(PIXART_ADDR, tx_buff_indbnk, 2, 1),
        NRF_TWI_MNGR_WRITE(PIXART_ADDR, tx_buff_indreg, 2, 1),
        NRF_TWI_MNGR_WRITE(PIXART_ADDR, tx_buff_indval, 1, 0),
        NRF_TWI_MNGR_READ(PIXART_ADDR, &result, 1, 0),
    };

    rv = twi_perform(NULL, transfer, sizeof(transfer) / sizeof(nrf_twi_mngr_transfer_t));
    if (NRF_SUCCESS != rv)
    {
        MKB_LOG_ERROR("###Error");
        return 0;
    }
    return result;
}

// The program starts from the function of Update_Flash()
static void I2cWriteRegister(uint8_t addr, uint8_t data)
{
    // The default slave ID for I2C int16_terface is 0x33
    // Example: write a value of 0x02 to addr 0x07
    //          START -> 0x66 (i.e. 0x33 + WRITE) -> 0x07 -> 0x02 -> STOP
    ret_code_t rv;
    uint8_t tx_buff_addr_data[] = {addr, data};
    nrf_twi_mngr_transfer_t const transfer[] = {
        NRF_TWI_MNGR_WRITE(PIXART_ADDR, tx_buff_addr_data, 2, 1),
    };

    rv = twi_perform(NULL, transfer, sizeof(transfer) / sizeof(nrf_twi_mngr_transfer_t));
    if (NRF_SUCCESS != rv)
    {
        MKB_LOG_ERROR("###Error");
    }
}

static uint8_t I2cReadRegister(uint8_t addr)
{
    // The default slave ID for I2C int16_terface is 0x33
    // Example: read data from addr 0x07
    //          START -> 0x66 (i.e. 0x33 + WRITE) -> 0x07 -> RESTART -> 0x67 (i.e. 0x33 + READ) -> DATA -> STOP

    ret_code_t rv;
    uint8_t data = 0;

    uint8_t tx_buff_addr[] = {addr};
    nrf_twi_mngr_transfer_t const transfer[] = {
        NRF_TWI_MNGR_WRITE(PIXART_ADDR, tx_buff_addr, 1, 1),
        NRF_TWI_MNGR_READ(PIXART_ADDR, &data, 1, 0),
    };

    rv = twi_perform(NULL, transfer, sizeof(transfer) / sizeof(nrf_twi_mngr_transfer_t));
    if (NRF_SUCCESS != rv)
    {
        MKB_LOG_ERROR("###Error");
        return (NRF_ERROR_INTERNAL);
    }

    return data;
}

#if 1

static void Enter_Engineering_Mode(uint8_t key1, uint8_t key2)
{
    I2cWriteRegister(0x7f, 0x01);
    I2cWriteRegister(0x2c, key1);
    I2cWriteRegister(0x2d, key2);
    I2cWriteRegister(0x7f, 0x06);

    nrf_delay_ms(10); // Delay 10ms
}

static void Exit_Engineering_Mode(uint8_t key1, uint8_t key2)
{
    uint16_t MAX_CHECK_COUNT = 50;
    uint16_t check_count = 0;
    uint8_t boot_ready_status = 0;

    I2cWriteRegister(0x7f, 0x01);
    I2cWriteRegister(0x2c, key1);
    I2cWriteRegister(0x2d, key2);
    I2cWriteRegister(0x7f, 0x06);

    for (check_count = 1; check_count <= MAX_CHECK_COUNT; check_count++) // max check time ~= 500ms
    {
        nrf_delay_ms(10); // Delay 10ms

        boot_ready_status = I2cReadRegister(0x70); // read boot ready status

        if ((boot_ready_status & 0x01) == 0x01)
        {
            break;
        }
        else if (check_count == MAX_CHECK_COUNT)
        {
            ;
            ________DBG_20210811_pct_i2c_dfu_test("report error");
        }
    }
}

void Flash_Execute(uint8_t inst_cmd, uint32_t CCR_Cmd, uint16_t data_cnt)
{
    uint8_t frame_start;
    uint16_t check_cnt;
    uint16_t MAX_CHECK_CNT = 10;

    // write uint16_tersram access
    I2cWriteRegister(0x7f, 0x04);
    I2cWriteRegister(0x2C, inst_cmd);

    // Write CCR command
    I2cWriteRegister(0x40, (uint8_t)((CCR_Cmd >> 0) & 0xFF));
    I2cWriteRegister(0x41, (uint8_t)((CCR_Cmd >> 8) & 0xFF));
    I2cWriteRegister(0x42, (uint8_t)((CCR_Cmd >> 16) & 0xFF));
    I2cWriteRegister(0x43, (uint8_t)((CCR_Cmd >> 24) & 0xFF));

    // Write data count
    I2cWriteRegister(0x44, (uint8_t)((data_cnt >> 0) & 0xFF));
    I2cWriteRegister(0x45, (uint8_t)((data_cnt >> 8) & 0xFF));

    // Frame start
    I2cWriteRegister(0x56, 0x01);
    I2cWriteRegister(0x7f, 0x06);

    for (check_cnt = 0; check_cnt < MAX_CHECK_CNT; check_cnt++)
    {
        nrf_delay_ms(1);

        I2cWriteRegister(0x73, 0x84);
        I2cWriteRegister(0x74, 0x56);
        nrf_delay_ms(1); // it is better to delay 1ms for the IC to prepare the correct data
        frame_start = I2cReadRegister(0x75);

        if (frame_start == 0)
            break;
        else if (check_cnt == MAX_CHECK_CNT - 1)
        {
            ________DBG_20210811_pct_i2c_dfu_test("report error");
        }
    }
}

static void Flash_Write_Enable()
{
    uint8_t flash_status;
    uint16_t check_cnt;
    uint16_t MAX_CHECK_CNT = 10;

    Flash_Execute(0x00, 0x00000106, 0); // WEN

    for (check_cnt = 0; check_cnt < MAX_CHECK_CNT; check_cnt++)
    {
        Flash_Execute(0x01, 0x01000105, 1); // RDSR
        nrf_delay_ms(1);                    // delay 1 ms

        I2cWriteRegister(0x73, 0x84);
        I2cWriteRegister(0x74, 0x1C);
        nrf_delay_ms(1); // it is better to delay 1ms for the IC to prepare the correct data
        flash_status = I2cReadRegister(0x75);

        if ((flash_status & 0x02) == 0x02)
        {
            break;
        }
        else if (check_cnt == MAX_CHECK_CNT)
        {
            ________DBG_20210811_pct_i2c_dfu_test("report error");
        }
    }
}

static void Wait_Flash_Busy()
{
    uint8_t flash_status;
    uint16_t check_cnt;
    uint16_t MAX_CHECK_CNT = 1000;

    for (check_cnt = 0; check_cnt < MAX_CHECK_CNT; check_cnt++)
    {
        Flash_Execute(0x01, 0x01000105, 1); // RDSR
        nrf_delay_ms(1);                    // delay 1 ms

        I2cWriteRegister(0x73, 0x84);
        I2cWriteRegister(0x74, 0x1C);
        nrf_delay_ms(1); // it is better to delay 1ms for the IC to prepare the correct data
        flash_status = I2cReadRegister(0x75);

        if ((flash_status & 0x01) == 0x00) // check if bit 0 is equal to 0
            break;
        else if (check_cnt == MAX_CHECK_CNT - 1)
        {
            ________DBG_20210811_pct_i2c_dfu_test("report error");
        }
    }
}

static void Erase_Flash_Sector(uint16_t sector)
{
    uint32_t flash_addr = (uint32_t)(sector)*4096;
    Wait_Flash_Busy();
    Flash_Write_Enable();

    I2cWriteRegister(0x7f, 0x04);
    I2cWriteRegister(0x48, (uint8_t)((flash_addr >> 0) & 0xff));
    I2cWriteRegister(0x49, (uint8_t)((flash_addr >> 8) & 0xff));
    I2cWriteRegister(0x4a, (uint8_t)((flash_addr >> 16) & 0xff));
    I2cWriteRegister(0x4b, (uint8_t)((flash_addr >> 24) & 0xff));
    I2cWriteRegister(0x7f, 0x06);
    Flash_Execute(0x00, 0x00002520, 0);
}

static void Write_256B_Data_to_SRAM(uint8_t data[], uint16_t data_size, uint16_t offset)
{
    uint16_t data_count;
    uint8_t write_data;

    // enable to write SRAM
    I2cWriteRegister(0x09, 0x08);
    I2cWriteRegister(0x0a, 0x00);

    for (data_count = 0; data_count < 256; data_count++)
    {
        write_data = (offset + data_count < data_size) ? data[offset + data_count] : (uint8_t)0xff;
        I2cWriteRegister(0x0b, write_data);
    }

    // disable to write SRAM
    I2cWriteRegister(0x0a, 0x01);
}

static void Program_256B_SRAM_Data_to_Flash(uint16_t sector, uint16_t page)
{
    uint32_t flash_addr;

    Wait_Flash_Busy();
    Flash_Write_Enable();

    // Set flash address
    flash_addr = (uint32_t)sector * 4096 + (uint32_t)page * 256;
    I2cWriteRegister(0x7f, 0x04);
    I2cWriteRegister(0x48, (uint8_t)((flash_addr >> 0) & 0xff));
    I2cWriteRegister(0x49, (uint8_t)((flash_addr >> 8) & 0xff));
    I2cWriteRegister(0x4a, (uint8_t)((flash_addr >> 16) & 0xff));
    I2cWriteRegister(0x4b, (uint8_t)((flash_addr >> 24) & 0xff));

    // Set SRAM access offset
    I2cWriteRegister(0x2e, 0x00);
    I2cWriteRegister(0x2f, 0x00);
    I2cWriteRegister(0x7f, 0x06);

    Flash_Execute(0x84, 0x01002502, 256); // Flash Execute with Page Program
}

static void Flash_Update_Process(uint8_t data[], uint16_t data_size, uint16_t sector)
{
    uint16_t sector_count = 0;
    uint16_t page_count = 0;
    uint16_t N = (data_size >> 12) + (((data_size & 0x00000fff) == 0) ? 0 : 1);

    ________DBG_20210811_pct_i2c_dfu_test("Flash_Update_Process(data[0]:%02x,data[1]:%02x], data_size:%d, sector:%d, N:%d)", data[42404], data[42405], data_size, sector, N);

    mkb_led_indication_debug(0, 100, 0, 0);
    mkb_led_indication_debug(1, 100, 0, 0);
    mkb_led_indication_debug(2, 100, 0, 0);
    mkb_led_indication_debug(3, 100, 0, 0);

    uint8_t led_cnt = 0;
    for (sector_count = 0; sector_count < N; sector_count++)
    {
        // 0123인데 3012로 해야됨.

        mkb_wdt_feed();
        Erase_Flash_Sector(sector + sector_count);
        ________DBG_20210811_pct_i2c_dfu_test("Erase_Flash_Sector(sector:%d + sector_count:%d)", sector, sector_count);

        for (page_count = 0; page_count < 16; page_count++)
        {

            mkb_led_indication_debug(((led_cnt / (16 * 4)) + 3) % 4, 64 - (led_cnt % (16 * 4)), (led_cnt % (16 * 4)), 0);
            led_cnt++;

            Write_256B_Data_to_SRAM(data, data_size, (sector_count << 12) + (page_count << 8));
            Program_256B_SRAM_Data_to_Flash(sector + sector_count, page_count);
            ________DBG_20210811_pct_i2c_dfu_test("Program_256B_SRAM_Data_to_Flash(sector:%d + sector_count:%d, page_count:%d)", sector, sector_count, page_count);
        }
    }
    mkb_led_indication_debug(0, 0, 10, 10);
    mkb_led_indication_debug(1, 0, 10, 10);
    mkb_led_indication_debug(2, 0, 10, 10);
    mkb_led_indication_debug(3, 0, 10, 10);
}

static void Update_Flash(uint8_t flash_data[], uint16_t data_size, uint16_t start_flash_sector)
{
    // start_flash_sector should be 0x00 for FW update and 0x0E for user parameter update
    Enter_Engineering_Mode(0xaa, 0xcc);

    // Power up flash controller
    I2cWriteRegister(0x7f, 0x01);
    I2cWriteRegister(0x0d, 0x02);
    I2cWriteRegister(0x7f, 0x06);

    Flash_Update_Process(flash_data, data_size, start_flash_sector);
    Exit_Engineering_Mode(0xaa, 0xbb);
}

uint32_t Read_CRC(uint8_t CRC_control_data)
{
    uint8_t read_data;
    uint16_t check_cnt;
    uint16_t MAX_CHECK_CNT = 50;
    uint32_t CRC_value;

    // Set CRC control
    I2cWriteRegister(0x73, 0x00);
    I2cWriteRegister(0x74, 0x82);
    I2cWriteRegister(0x75, CRC_control_data);

    for (check_cnt = 0; check_cnt < MAX_CHECK_CNT; check_cnt++)
    {
        nrf_delay_ms(10); // delay 10ms

        // read CRC_CALC_CTL
        I2cWriteRegister(0x74, 0x82);
        read_data = I2cReadRegister(0x75);

        if ((read_data & 0x01) == 0x00)
        {
            break;
        }
        else if (check_cnt == MAX_CHECK_CNT - 1)
        {
            ________DBG_20210811_pct_i2c_dfu_test("report error");
        }
    }

    CRC_value = 0;
    I2cWriteRegister(0x74, 0x84);
    read_data = I2cReadRegister(0x75);
    CRC_value += (uint32_t)read_data;

    I2cWriteRegister(0x74, 0x85);
    read_data = I2cReadRegister(0x75);
    CRC_value += ((uint32_t)read_data << 8);

    I2cWriteRegister(0x74, 0x86);
    read_data = I2cReadRegister(0x75);
    CRC_value += ((uint32_t)read_data << 16);

    I2cWriteRegister(0x74, 0x87);
    read_data = I2cReadRegister(0x75);
    CRC_value += ((uint32_t)read_data << 24);

    return CRC_value;
}

#ifdef ________DBG_20210811_print_prj_ver
uint16_t pixart_get_prj_ver()
{
    uint16_t prj_ver = 0;
    prj_ver |= pixart_rx_usr(0x00, 0xB2);      // ver
    prj_ver |= pixart_rx_usr(0x00, 0xB3) << 8; // prj
    ________DBG_20210811_print_prj_ver("prj_ver:%04X", prj_ver);
    return prj_ver;
}
#endif

// 0 : all area
// 1 : right hand mode
// 2 : left hand mode
static void pixart_set_left()
{
    // m_pixart_read = 0;
    uint8_t buff = 0;
    pixart_tx_usr(0x02, 0x3B, 2);

    // pixart_rx_usr(0x02, 0x3B);
    // buff = m_pixart_read;
    //________DBG_20210809_rx_usr_test("aamode:%02x", buff);
}

bool drv_pixart_get_otherhand_flag()
{
    /*
        Change list:
                (1) This version for V1.5_C Mokibo module.
                (2) Add touch flag (UB2A61) when right/left hand mode.
                        1: has touch in Left/Right hand side for Right/Left hand mode
                        0: no touch in Left/Right hand side for Right/Left hand mode
                        Others: reserved
    */
    bool buff = 0;
    buff = pixart_rx_usr(0x02, 0x61);
    ________DBG_20211130_otherhand_flag("otherhand_flag: %d", buff);
    return buff;
}

void drv_pixart_set_aamode(pixart_aamode_type_t touch_area)
{
    // m_pixart_read = 0;
    uint8_t buff = 0;

    if (touch_area == PIXART_AAMODE_TYPE_ALL)
    {
        pixart_tx_usr(0x02, 0x3B, 0); // all
    }
    else if (touch_area == PIXART_AAMODE_TYPE_LEFT)
    {
        pixart_tx_usr(0x02, 0x3B, 2); // left
    }
    else if (touch_area == PIXART_AAMODE_TYPE_RIGHT)
    {
        pixart_tx_usr(0x02, 0x3B, 1); // right
    }
    else // PIXART_AAMODE_TYPE_NONE
    {
        pixart_tx_usr(0x02, 0x3B, 1); // right
    }
}

void drv_pixart_set_aamode_all(bool is_all)
{
    if (is_all)
    {
        drv_pixart_set_aamode(PIXART_AAMODE_TYPE_ALL);
    }
    else
    {
        drv_pixart_set_aamode(mkb_db_aamode_get());
    }
}

static void pixart_set_kttt()
{
    /*
                ) Add KT (Keyboard de-jitter threshold) (UB2A4E~4F)
                        V1410 setting 0x5D0, Strong de-jitter if increase.
                ) TT (Touch de-jitter threshold) (UB2A06~07)
                        V1410 setting 0xBA, Strong de-jitter if increase.*/

    // Hi @Flimbs, KT can fine tune by Host(BLE).V1410, default is 0x5D0 by Mail.If want to reduce to

    //    half the current level, you can set it from 0x5d0 to 0x2e8.
    // if still fell the value is too large.reduice again 0x2e8 to 0x174.
    // distant base block
    // pixart_tx_usr(0x02, 0x4E, 0xD0);//default D0 작은수
    // pixart_tx_usr(0x02, 0x4F, 0x05);//default 05 큰수 픽스아트에서알려준값:curr(0x05,0xD0) mid(0x02,0xE8) small(0x74,0x01)

    // KT Threshold : works well only large value
    // USERBANK=2, ADDR=0x4F, VAL=0x05
    // USERBANK=2, ADDR=0x4E, VAL=0xD0
    // preset 큰값:(0x05,0xD0), 중간값: (0x01,0x74)
    pixart_tx_usr(0x02, 0x4F, 0x00); // did not work in small value
    pixart_tx_usr(0x02, 0x4E, 0x00);

    // TT Threshold : didn't work
    // USERBANK=2, ADDR=0x0D, VAL=0x00
    // USERBANK=2, ADDR=0x0C, VAL=0x2E
    pixart_tx_usr(0x02, 0x0D, 0x00); // did not work
    pixart_tx_usr(0x02, 0x0C, 0x00);

    // Time Threshold : works well
    // USERBANK=03, ADDR=0x49, VAL=0x00 (set enable)
    // USERBANK=03, ADDR=0x37, VAL=0x00
    pixart_tx_usr(0x03, 0x49, 0x00);
    pixart_tx_usr(0x03, 0x37, 0x00);
}

static void pixart_set_calibration()
{
    /*Hi @Flimbs , I try to typing over 1 hour already.
    It still not occur this issue. From this issue, we have analysis the possible reason:
    This issue will occur when FW internal calibration error.
    For this assume, could you help me set UB0A72 from 1 to 0 by host?
    It's may effectlly(Use for verify).

    Hi @Flimbs
    I corrected my description. We need your help to write 2 user register:
    (1) Set UB0A72 from 1 to 0. (As previous description)
    (2) Set UB1A71 from 15 to 7. (Need to change both)
    Sorry for my mistake. Thanks
*/
    // pixart_tx_usr(0x00, 0x72, 0);
    // pixart_tx_usr(0x01, 0x71, 7);

    /*
    20220217
    @Flimbs please remove (UB0A72 set 0) and
    (UB1A71 set 7)
    Hi @Flimbs, Thanks for your help. We send new FW to solved this issue. (please check by mail).
    The root cause is our thermal calibration trigger when finger put on "ASDF". We fixed it already.
    If still occur same issue. please write 0x46 to UB0A83 when power on (just once).
    it will enable debug like as V14D4 method(host read user register (UB0A83~UB0A88) 6 bytes.
    */

    pixart_tx_usr(0x00, 0x83, 0x46);
}
static void pixart_set_gesture_kttt()
{
    /*
    BANK:0x03, ADDR:0x4C, VAL:16    4f left
    BANK:0x03, ADDR:0x4D, VAL:16    4f right
    BANK:0x03, ADDR:0x60, VAL:16    4f up
    BANK:0x03, ADDR:0x61, VAL:16    4f down

    BANK:0x03, ADDR:0x50, VAL:16    3f left
    BANK:0x03, ADDR:0x51, VAL:16    3f right
    BANK:0x03, ADDR:0x52, VAL:16    3f up
    BANK:0x03, ADDR:0x53, VAL:16    3f down
    */

    // 3점 상하좌우
    pixart_tx_usr(0x03, 0x52, 2);
    pixart_tx_usr(0x03, 0x53, 2);
    pixart_tx_usr(0x03, 0x50, 2);
    pixart_tx_usr(0x03, 0x51, 2);

    // 4점 상하좌우
    pixart_tx_usr(0x03, 0x60, 2);
    pixart_tx_usr(0x03, 0x61, 2);
    pixart_tx_usr(0x03, 0x4C, 2);
    pixart_tx_usr(0x03, 0x4D, 2);

    pixart_tx_usr(0x02, 0x62, 0);
}

static void pixart_cursor_threshold()
{
/*
You may try to reduce UB3A18 value (default 64), like as 48, 32,16..., it maybe fits your requirement.
We know you wanted cursor immediately, but "Tap" and "Cursor" have some overlap time.
We provide timer & distance (2 viewpoint) to help judge Cursor or Tap.
We recommend UB3A18 (Distance) from 0 up to 18. (Unit: 1 sensor cell == 32)
UB3A19 (Timer) from 0 up to 30 (Unit: 0.01 s)
Cursor condition trigger when "the point move > Distance" || "the point alive > Timer"
*/
// distance threhosld: bank 0x03 addr 0x18 value (j)
#ifdef ________DBG_20220314_idle_module_refactory
    pixart_tx_usr(0x03, 0x18, 0); // pixart_tx_usr(0x03, 0x18, 0x00); //1만 올려도 KT슬라이딩 현상이 생김

    // timer threhosld: bank 0x03 addr 0x19 value (value = 10ms)
    pixart_tx_usr(0x03, 0x19, 0);
#else

    // distance. 0~18 추천함
    // pixart_tx_usr(0x03, 0x0C, 18);
    pixart_tx_usr(0x03, 0x18, 0); // pixart_tx_usr(0x03, 0x18, 0x00); //1만 올려도 KT슬라이딩 현상이 생김

    // timer threhosld: bank 0x03 addr 0x19 value (value = 10ms)
    // timer 0~30 추천함
    // pixart_tx_usr(0x03, 0x0D, 16);
    pixart_tx_usr(0x03, 0x19, 0);
#endif
}

static void pixart_tap_sensitive()
{
    /*
    Let's SOP fine tune flow.

    To decision "Distance"
    Step 1: Disabled timer condition
    UB3A0D = UB3A19 = 0xff (write 0xff to both register)
    Step 2: Fine tune finger move distance
    UB3A0C = UB3A18 = 0 -> 1 -> 2 -> ... (0 is cursor most sensitive, but tap hit rate is lower)
    You must try which value can balance between the "tap hit rate" & "cursor sensitivity".

    To decision "Timer"
    Step 1: Disabled(difficult trigger) distance condition
    UB3A0C = UB3A18 = 0xff (write 0xff to both register)
    Step 2: Fine tune finger exist timer
    UB3A0D = UB3A19 = 10 -> 20 -> ... (10 means if finger touch > 100 ms, then gesture will don't care distance and direct judge to cursor.)
    You must try which value can balance between the "tap hit rate" & "cursor sensitivity".
    */

    // pixart_tx_usr(0x03,0x18)
}

#ifdef ________DBG_20220215_asdf_problem
static void pixart_asdf_problem_log()
{
    /*
    (1) Base on 0x1418 version.
    (2) This version for V1.5_C Mokibo module.
    (3) FW put debug information in user register
            When "ASDF" ghost key occur, please host read user register
            (UB0A83~UB0A88) 6 bytes.

    */
    uint8_t rx_buf[6] = {
        0,
    };

    rx_buf[0] = pixart_rx_usr(0x00, 0x83);
    rx_buf[1] = pixart_rx_usr(0x00, 0x84);
    rx_buf[2] = pixart_rx_usr(0x00, 0x85);
    rx_buf[3] = pixart_rx_usr(0x00, 0x86);
    rx_buf[4] = pixart_rx_usr(0x00, 0x87);
    rx_buf[5] = pixart_rx_usr(0x00, 0x88);

    ________DBG_20220215_asdf_problem("rx_buf={%02x, %02x, %02x, %02x, %02x, %02x}", rx_buf[0], rx_buf[1], rx_buf[2], rx_buf[3], rx_buf[4], rx_buf[5]);
}
#endif
static void pixart_init_settings()
{

    pixart_get_prj_ver();
    pixart_set_gesture_kttt();
    drv_pixart_set_aamode(mkb_db_aamode_get());
    pixart_set_calibration();
    pixart_cursor_threshold();
}
#endif

// time base block
// pixart_tx_usr(0x03, 0x49, 0x01); // default = 0x01? -> 0x00 . 세팅플래그. 0으로바꿔야 적용된다고함.
//  pixart_tx_usr(0x03, 0x37, 0x05);   // default = 0x05 //키입력후 val * 10ms 만큼 block한다
// pixart_tx_usr(0x03, 0x49, 0x00);
// pixart_tx_usr(0x03, 0x37, 0x00);
//-----------------------------------------------------------------------------------------------------------------------------------
//			pixart_status
//-----------------------------------------------------------------------------------------------------------------------------------
/*
The access flow after power on is described in Figure 17. After power on, host MCU has to check boot ready status
register (Reg0x70) to confirm the system is ready. Then, MCU starts to read STATUS register (Reg0x71) to check
whether touch report is ready. Host can select polling mode or interrupt mode to access data. Normally, it is
suggested to select interrupt mode for lower power and short touch reponse time. Once STATUS bit is not “0”,
MCU starts to read touch report. After touch report accessing is completed, write 0 to Reg0x71 to clear the status
*/

/*
    Bit[0]: STATUS_ERROR, When this bit is set, the certain error occurred. The host has to reset and reinitialize the device.
    Bit[1]: STATUS_TOUCH. When this bit is set, the touch data of this frame is valid. The host can read the data from 0x01~0x1f.
    Bit[3]: STATUS_GESTURE. When this bit is set, the gesture data of this frame is valid. The host can read the data from 0x60~0x6f.
    Bit[4]: NONE
    Bit[5]: STATUS_PALM. When this bit is set, the palm is detected.
    Bit[6]: STATUS_BUTTON. When this bit is set, the button state of this frame is valid. The host can read the data from BUTTON_STAT.
    Bit[7]: STATUS_WDOG. When this bit is set, the device was reset by the watchdog to the debug mode. The host has to reset and reinitialize the device.
    0000 0000
*/
static uint8_t aamode_cnt = 0;
static ret_code_t pixart_status(void)
{
    ret_code_t rv;

    if (aamode_cnt == 2)
    {
        pixart_init_settings();
        aamode_cnt++;
    }
    else if (aamode_cnt < 2)
    {
        aamode_cnt++;
    }

    // buffer for the next reading
    memset(m_pixart_rdbuff, 0, 1);

    rv = twi_perform(NULL, pixart_status_transfers, sizeof(pixart_status_transfers) / sizeof(nrf_twi_mngr_transfer_t));

    if (NRF_SUCCESS != rv)
    {
        MKB_LOG_ERROR("###Error");
        return (NRF_ERROR_INTERNAL);
    }

    if (m_pixart_rdbuff[0] == 0)
    {
        ________DBG_20210810_pixart_status_message("status message is 0");
    }
    if (m_pixart_rdbuff[0] & (1 << 0))
    {
        //, When this bit is set, the certain error occurred. The host has to reset and reinitialize the device.
        ________DBG_20210810_pixart_status_message("STATUS_ERROR(0x%X)", m_pixart_rdbuff[0]);
        MKB_LOG_ERROR("STATUS_ERROR, When this bit is set, the certain error occurred. The host has to reset and reinitialize the device.");
    }
    if (m_pixart_rdbuff[0] & (1 << 1))
    {
        // When this bit is set, the touch data of this frame is valid. The host can read the data from 0x01~0x1f.
        ________DBG_20210810_pixart_status_message("STATrUS_TOUCH(0x%X).", m_pixart_rdbuff[0]);
    }
    if (m_pixart_rdbuff[0] & (1 << 3))
    {
        // When this bit is set, the gesture data of this frame is valid. The host can read the data from 0x60~0x6f.
        ________DBG_20210810_pixart_status_message("STATUS_GESTURE(0x%X).", m_pixart_rdbuff[0]);
    }
    if (m_pixart_rdbuff[0] & (1 << 5))
    {
        // When this bit is set, the palm is detected.
        ________DBG_20210810_pixart_status_message("STATUS_PALM(0x%X).", m_pixart_rdbuff[0]);
    }
    if (m_pixart_rdbuff[0] & (1 << 6))
    {
        // When this bit is set, the button state of this frame is valid. The host can read the data from BUTTON_STAT.
        ________DBG_20210810_pixart_status_message("STATUS_BUTTON(0x%X).", m_pixart_rdbuff[0]);
    }
    if (m_pixart_rdbuff[0] & (1 << 7))
    {
        // When this bit is set, the device was reset by the watchdog to the debug mode. The host has to reset and reinitialize the device.
        ________DBG_20210810_pixart_status_message("STATUS_WDOG(0x%X).", m_pixart_rdbuff[0]);
        MKB_LOG_ERROR("STATUS_WDOG. When this bit is set, the device was reset by the watchdog to the debug mode. The host has to reset and reinitialize the device.");
    }

#ifdef ________DBG_20211117_check_for_i2c_stop_sprintf
    //
    sprintf(logbuf, "%s status:%X,", logbuf, m_pixart_rdbuff[0]);
#endif

    if ((m_pixart_rdbuff[0] == 0x00))
    {
        // MKB_LOG_ERROR("status check success");
        return NRF_ERROR_INTERNAL;
    }
    else if ((m_pixart_rdbuff[0] & 0b00000001))
    {
        MKB_LOG_ERROR("status check Error");
        return 0x8000;
    }
    else if ((m_pixart_rdbuff[0] & 0b10000000))
    {
        MKB_LOG_ERROR("watchdog");
        return 0x8001;
    }
    // the sentinel data should be 0x0000 ...
    else if ((m_pixart_rdbuff[0] != 0x00))
    {
        return NRF_SUCCESS;
    }

    MKB_LOG_DEBUG("Read status: 0x%x", m_pixart_rdbuff[0]); //보통 0x20 이 나옴

    return (NRF_SUCCESS);
}

//-----------------------------------------------------------------------------------------------------------------------------------
//			pixart_suspend
//-----------------------------------------------------------------------------------------------------------------------------------
static ret_code_t pixart_suspend(void)
{
    ret_code_t rv;
    uint8_t i;

    rv = twi_perform(NULL, pixart_disable_deepsleep_transfers, sizeof(pixart_disable_deepsleep_transfers) / sizeof(nrf_twi_mngr_transfer_t));
    if (NRF_SUCCESS != rv)
    {
        MKB_LOG_ERROR("###Error");
        return (NRF_ERROR_INTERNAL);
    }

    for (i = 0; i < 100; i++)
    {
#if (defined(MKB_WDT_ENABLED) && MKB_WDT_ENABLED)
        mkb_wdt_feed();
#endif

        nrf_delay_ms(10);

        // buffer for the next reading
        memset(m_pixart_rdbuff, 0, 1);

        rv = twi_perform(NULL, pixart_sleep_status_transfers, sizeof(pixart_sleep_status_transfers) / sizeof(nrf_twi_mngr_transfer_t));
        if (NRF_SUCCESS != rv)
        {
            MKB_LOG_ERROR("###Error");
            return (NRF_ERROR_INTERNAL);
        }

        MKB_LOG_DEBUG("Read status: 0x%x", m_pixart_rdbuff[0]);

        if ((m_pixart_rdbuff[0] != 0x08))
        {
            break;
        }
    }

    if (i >= 100)
    {
        MKB_LOG_ERROR("suspend check failed");
        return NRF_ERROR_INTERNAL;
    }

    rv = twi_perform(NULL, pixart_suspend_transfers, sizeof(pixart_suspend_transfers) / sizeof(nrf_twi_mngr_transfer_t));
    if (NRF_SUCCESS != rv)
    {
        MKB_LOG_ERROR("###Error");
        return (NRF_ERROR_INTERNAL);
    }

    nrf_delay_ms(10);

    return (NRF_SUCCESS);
}

static ret_code_t pixart_resume(void)
{
    ret_code_t rv;

    rv = twi_perform(NULL, pixart_resume_transfers, sizeof(pixart_resume_transfers) / sizeof(nrf_twi_mngr_transfer_t));
    if (NRF_SUCCESS != rv)
    {
        MKB_LOG_ERROR("###Error");
        return (NRF_ERROR_INTERNAL);
    }

    for (uint8_t i = 0; i < 50; i++)
    {
#if (defined(MKB_WDT_ENABLED) && MKB_WDT_ENABLED)
        mkb_wdt_feed();
#endif

        nrf_delay_ms(10);

        // buffer for the next reading
        memset(m_pixart_rdbuff, 0, 1);

        rv = twi_perform(NULL, pixart_boot_ready_transfers, sizeof(pixart_boot_ready_transfers) / sizeof(nrf_twi_mngr_transfer_t));
        if (rv != NRF_SUCCESS)
        {
            MKB_LOG_ERROR("I2C read failed");
            return NRF_ERROR_INTERNAL;
        }

        MKB_LOG_DEBUG("Read status: 0x%x", m_pixart_rdbuff[0]);

        // the sentinel data should be 0x0000 ...
        if ((m_pixart_rdbuff[0] != 0x00))
        {
            return NRF_SUCCESS;
        }
    }

    MKB_LOG_ERROR("boot ready check failed");
    return NRF_ERROR_INTERNAL;
}

//-----------------------------------------------------------------------------------------------------------------------------------
//			pixart_software_reset
//-----------------------------------------------------------------------------------------------------------------------------------
ret_code_t pixart_software_reset(void)
{
    ret_code_t rv;
    uint8_t i;

    rv = twi_perform(NULL, pixart_disable_deepsleep_transfers, sizeof(pixart_disable_deepsleep_transfers) / sizeof(nrf_twi_mngr_transfer_t));
    if (NRF_SUCCESS != rv)
    {
        MKB_LOG_ERROR("###Error");
        return (NRF_ERROR_INTERNAL);
    }

    for (i = 0; i < 100; i++)
    {
#if (defined(MKB_WDT_ENABLED) && MKB_WDT_ENABLED)
        mkb_wdt_feed();
#endif

        nrf_delay_ms(10);

        // buffer for the next reading
        memset(m_pixart_rdbuff, 0, 1);

        rv = twi_perform(NULL, pixart_sleep_status_transfers, sizeof(pixart_sleep_status_transfers) / sizeof(nrf_twi_mngr_transfer_t));
        if (NRF_SUCCESS != rv)
        {
            MKB_LOG_ERROR("###Error");
            return (NRF_ERROR_INTERNAL);
        }

        MKB_LOG_DEBUG("Read status: 0x%x", m_pixart_rdbuff[0]);

        if ((m_pixart_rdbuff[0] != 0x08))
        {
            break;
        }
    }

    if (i >= 100)
    {
        MKB_LOG_ERROR("status check failed");
        return NRF_ERROR_INTERNAL;
    }

    rv = twi_perform(NULL, pixart_software_reset1_transfers, sizeof(pixart_software_reset1_transfers) / sizeof(nrf_twi_mngr_transfer_t));
    if (NRF_SUCCESS != rv)
    {
        MKB_LOG_ERROR("###Error");
        return (NRF_ERROR_INTERNAL);
    }

    nrf_delay_ms(10);

    rv = twi_perform(NULL, pixart_software_reset2_transfers, sizeof(pixart_software_reset2_transfers) / sizeof(nrf_twi_mngr_transfer_t));
    if (NRF_SUCCESS != rv)
    {
        MKB_LOG_ERROR("###Error");
        return (NRF_ERROR_INTERNAL);
    }

    return (NRF_SUCCESS);
}

//-----------------------------------------------------------------------------------------------------------------------------------
//			pixart_read_report
//          report state 받기 > 데이터 받기 > ack_clear 로 이루어진다.
//-----------------------------------------------------------------------------------------------------------------------------------
ret_code_t pixart_read_report(void)
{
    ret_code_t rv;
    uint8_t packet_cursor;

    // buffer for the next reading
    memset(m_pixart_rdbuff, 0, PIXART_DATA_REQ_LEN);

    // report state 받아오기
    {
        rv = twi_perform(NULL, pixart_read_report_start_transfers, sizeof(pixart_read_report_start_transfers) / sizeof(nrf_twi_mngr_transfer_t));
        if (NRF_SUCCESS != rv)
        {
            MKB_LOG_ERROR("###Error");
            return (NRF_ERROR_INTERNAL);
        }

        rv = twi_perform(NULL, pixart_read_report_single_transfers, sizeof(pixart_read_report_single_transfers) / sizeof(nrf_twi_mngr_transfer_t));
        if (NRF_SUCCESS != rv)
        {
            MKB_LOG_ERROR("###Error");
            return (NRF_ERROR_INTERNAL);
        }
    }

    //실제 데이터 패킷 받아오기
    //키보드면 바로받아오고 터치면이면 각 손가락 갯수만큼 데이터 m_pixart_rdbuff로 받아온다.
    //입력이 키보드면 14번까지
    //아니면 터치니까 8번까지  돌고 갯수 받아온다음에 터치갯수 * 11개 더 받아오기
    // gstr1 x2y2z2 btn1N1 해서 9개 + (obj1 x2y2z2 area2 해서 9개)*N
    // 9 + 9*N
    {
        for (packet_cursor = 0; packet_cursor <= PIXART_DATA_IS_KEYBOARD_FLAG_LOC; packet_cursor++)
        { //키보드터치 플래그 패킷 수집
            rv = twi_perform(NULL, pixart_read_report_loop_transfers, sizeof(pixart_read_report_loop_transfers) / sizeof(nrf_twi_mngr_transfer_t));
            if (NRF_SUCCESS != rv)
            {
                MKB_LOG_ERROR("###Error");
                return (NRF_ERROR_INTERNAL);
            }
            m_pixart_rdbuff[packet_cursor] = m_pixart_read;
        }

        if (m_pixart_rdbuff[PIXART_DATA_IS_KEYBOARD_FLAG_LOC] == PIXART_DATA_IS_KEYBOARD_FLAG)
        { //키보드 패킷 일때

            for (packet_cursor; packet_cursor <= PIXART_DATA_KEYBOARD_PACKET_LEN; packet_cursor++)
            {
                rv = twi_perform(NULL, pixart_read_report_loop_transfers, sizeof(pixart_read_report_loop_transfers) / sizeof(nrf_twi_mngr_transfer_t));
                if (NRF_SUCCESS != rv)
                {
                    MKB_LOG_ERROR("###Error");
                    return (NRF_ERROR_INTERNAL);
                }
                m_pixart_rdbuff[packet_cursor] = m_pixart_read;
            }
        }
        else
        { //터치 패킷일때
            for (packet_cursor; packet_cursor <= PIXART_DATA_TOUCH_CNT_LOC; packet_cursor++)
            { //최초 9바이트
                rv = twi_perform(NULL, pixart_read_report_loop_transfers, sizeof(pixart_read_report_loop_transfers) / sizeof(nrf_twi_mngr_transfer_t));
                if (NRF_SUCCESS != rv)
                {
                    MKB_LOG_ERROR("###Error");
                    return (NRF_ERROR_INTERNAL);
                }

                m_pixart_rdbuff[packet_cursor] = m_pixart_read;
            }

            for (packet_cursor; packet_cursor <= PIXART_DATA_TOUCH_CNT_LOC + (m_pixart_rdbuff[PIXART_DATA_TOUCH_CNT_LOC] * PIXART_DATA_TOUCH_FINGER_PACKET_LEN); packet_cursor++)
            { //핑거 9바이트 * N
                rv = twi_perform(NULL, pixart_read_report_loop_transfers, sizeof(pixart_read_report_loop_transfers) / sizeof(nrf_twi_mngr_transfer_t));
                if (NRF_SUCCESS != rv)
                {
                    MKB_LOG_ERROR("###Error");
                    return (NRF_ERROR_INTERNAL);
                }

                m_pixart_rdbuff[packet_cursor] = m_pixart_read;
            }
        }

#ifdef ________DBG_20220215_asdf_problem
        pixart_asdf_problem_log();
#endif

        ________DBG_20210801_pct_packet_raw_gstr_packet("gstr:%d", m_pixart_rdbuff[0]);
#ifdef ________DBG_20210801_pct_packet_raw_sprint
        char logbuf[250];
        sprintf(logbuf, "[%02d] ", packet_cursor);
        for (int j = 0; j <= packet_cursor - 1; j++)
        {
            if ((j % 9) == 0 && packet_cursor != 0)
            {
                sprintf(logbuf, "%s ", logbuf);
            }
            sprintf(logbuf, "%s|%02X", logbuf, m_pixart_rdbuff[j]);
        }
        ________DBG_20210801_pct_packet_raw_sprint("%s", logbuf);
#endif

#ifdef ________DBG_20210801_pct_touch_packet_value_sprint

        static int16_t old_xyza[15] = {
            0,
        };
        if (m_pixart_rdbuff[0] != 0x43)
        {
            ________DBG_20210801_pct_touch_packet_value_sprint("time:%-4d len:%-2d ", mkb_util_get_tick_delta(), packet_cursor);
            //[32] 00|01|02|03|04|05|06|07|08|09|10|11|12|13|14|15|16|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|
            //    gst x---- y---- z----btncnt objx---- y---- z---- a---- objx---- y---- z---- a----
            if (packet_cursor == 9)
            {
                memset(old_xyza, 0, sizeof(int16_t) * 15);
            }

            for (int j = 0; j < packet_cursor; j++)
            {
                if (j == 0)
                {
                    ________DBG_20210801_pct_touch_packet_value_sprint(" || [0]:(GST:%-2d|", (int8_t)m_pixart_rdbuff[j]);
                }
                if (j == 2)
                {
                    ________DBG_20210801_pct_touch_packet_value_sprint("X:");
                    int16_t delta = (int16_t)((int16_t)m_pixart_rdbuff[j - 1] + (int16_t)m_pixart_rdbuff[j] * 256);
                    ________DBG_20210801_pct_touch_packet_value_sprint("%4d|", delta);
                    for (uint8_t i = 0; i < 8; i++)
                    {
                        if (i >= abs(delta))
                        {
                            ________DBG_20210801_pct_touch_packet_value_sprint("-");
                        }
                        else
                        {
                            ________DBG_20210801_pct_touch_packet_value_sprint("x");
                        }
                    }
                    ________DBG_20210801_pct_touch_packet_value_sprint("|");
                }
                else if (j == 4)
                {
                    ________DBG_20210801_pct_touch_packet_value_sprint("Y:");
                    int16_t delta = (int16_t)((int16_t)m_pixart_rdbuff[j - 1] + (int16_t)m_pixart_rdbuff[j] * 256);
                    ________DBG_20210801_pct_touch_packet_value_sprint("%4d|", delta);
                    for (uint8_t i = 0; i < 8; i++)
                    {
                        if (i >= abs(delta))
                        {
                            ________DBG_20210801_pct_touch_packet_value_sprint("-");
                        }
                        else
                        {
                            ________DBG_20210801_pct_touch_packet_value_sprint("y");
                        }
                    }
                    ________DBG_20210801_pct_touch_packet_value_sprint("|");
                }
                else if (j == 6)
                {
                    ________DBG_20210801_pct_touch_packet_value_sprint("Z:%-4d|", (int16_t)((int16_t)m_pixart_rdbuff[j - 1] + (int16_t)m_pixart_rdbuff[j] * 256));
                }
                else if (j == 7)
                {
                    ________DBG_20210801_pct_touch_packet_value_sprint("BTN:%-1d|", (uint8_t)m_pixart_rdbuff[j]);
                }
                else if (j == 8)
                {
                    ________DBG_20210801_pct_touch_packet_value_sprint("FGR:%-1d)", (uint8_t)m_pixart_rdbuff[j]);
                }
                else if (j >= 9)
                {
                    //[32] 00|01|02|03|04|05|06|07|08|09|10|11|12|13|14|15|16|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|
                    //    gst x---- y---- z----btncnt objx---- y---- z---- a---- objx---- y---- z---- a---- objx---- y---- z---- a----
                    if ((j - 0) % 9 == 0) // 9 18 27
                    {
                        ________DBG_20210801_pct_touch_packet_value_sprint(" || [%d]:(OBJ:%-4d|", (j / 9), (uint8_t)m_pixart_rdbuff[j]);
                    }
                    else if ((j - 11) % 9 == 0) // 11 20 29 X
                    {
                        ________DBG_20210801_pct_touch_packet_value_sprint("X:");

                        int8_t old_pos = (((j + -11) / 9) * 3) + 0;
                        int16_t new_xyza = (int16_t)((int16_t)m_pixart_rdbuff[j - 1] + (int16_t)m_pixart_rdbuff[j] * 256);
                        int8_t delta = (int16_t)(old_xyza[old_pos] - new_xyza);

                        ________DBG_20210801_pct_touch_packet_value_sprint("%4d", (int16_t)(old_xyza[old_pos] - new_xyza));
                        for (uint8_t i = 0; i < 8; i++)
                        {
                            if (i >= abs(delta))
                            {
                                ________DBG_20210801_pct_touch_packet_value_sprint("-");
                            }
                            else
                            {
                                ________DBG_20210801_pct_touch_packet_value_sprint("x");
                            }
                        }
                        ________DBG_20210801_pct_touch_packet_value_sprint("|");
                        old_xyza[old_pos] = new_xyza;
                    }
                    else if ((j - 13) % 9 == 0) // 13 22 31 Y 22
                    {
                        ________DBG_20210801_pct_touch_packet_value_sprint("Y:");
                        int8_t old_pos = (((j + -13) / 9) * 3) + 1;
                        int16_t new_xyza = (int16_t)((int16_t)m_pixart_rdbuff[j - 1] + (int16_t)m_pixart_rdbuff[j] * 256);
                        int8_t delta = (int16_t)(old_xyza[old_pos] - new_xyza);
                        ________DBG_20210801_pct_touch_packet_value_sprint("%4d", (int16_t)(old_xyza[old_pos] - new_xyza));
                        for (uint8_t i = 0; i < 8; i++)
                        {
                            if (i >= abs(delta))
                            {
                                ________DBG_20210801_pct_touch_packet_value_sprint("-");
                            }
                            else
                            {
                                ________DBG_20210801_pct_touch_packet_value_sprint("y");
                            }
                        }
                        ________DBG_20210801_pct_touch_packet_value_sprint("|");
                        old_xyza[old_pos] = new_xyza;
                    }
                    else if ((j - 15) % 9 == 0) // 15 24 33
                    {
                        int8_t old_pos = (((j + -15) / 9) * 3) + 2;
                        int16_t new_xyza = (int16_t)((int16_t)m_pixart_rdbuff[j - 1] + (int16_t)m_pixart_rdbuff[j] * 256);
                        ________DBG_20210801_pct_touch_packet_value_sprint("Z:%-4d|", new_xyza);
                        // old_xyza[old_pos] = new_xyza;
                    }
                    else if ((j + 1) % 9 == 0)
                    {
                        ________DBG_20210801_pct_touch_packet_value_sprint("A:%-2d)", (int16_t)((int16_t)m_pixart_rdbuff[j - 1] + (int16_t)m_pixart_rdbuff[j] * 256));
                    }
                }
            }
            ________DBG_20210801_pct_touch_packet_value_sprint("\n");
        }
#endif

#if 0

        int16_t old_xyza[15] = {
            0,
        };
        if (m_pixart_rdbuff[0] != 0x43)
        {
            char logbuf[300];
            sprintf(logbuf, "[%2d] ", packet_cursor);
            //[32] 00|01|02|03|04|05|06|07|08|09|10|11|12|13|14|15|16|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|
            //    gst x---- y---- z----btncnt objx---- y---- z---- a---- objx---- y---- z---- a----
            for (int j = 0; j <= packet_cursor; j++)
            {
                if (j == 0)
                {
                    sprintf(logbuf, "%s<GST:%2d|", logbuf, (int8_t)m_pixart_rdbuff[j]);
                }
                if (j == 2)
                {
                    sprintf(logbuf, "%sX:", logbuf);
                    sprintf(logbuf, "%s%4d|", logbuf, (int16_t)((int16_t)m_pixart_rdbuff[j - 1] + (int16_t)m_pixart_rdbuff[j] * 256));
                }
                else if (j == 4)
                {
                    sprintf(logbuf, "%sY:", logbuf);
                    sprintf(logbuf, "%s%4d|", logbuf, (int16_t)((int16_t)m_pixart_rdbuff[j - 1] + (int16_t)m_pixart_rdbuff[j] * 256));
                }
                else if (j == 6)
                {
                    sprintf(logbuf, "%sZ:", logbuf);
                    sprintf(logbuf, "%s%4d|", logbuf, (int16_t)((int16_t)m_pixart_rdbuff[j - 1] + (int16_t)m_pixart_rdbuff[j] * 256));
                }
                else if (j == 7)
                {
                    sprintf(logbuf, "%sBTN:%1d|", logbuf, (uint8_t)m_pixart_rdbuff[j]);
                }
                else if (j == 8)
                {
                    sprintf(logbuf, "%sFGR:%0d>", logbuf, (int8_t)m_pixart_rdbuff[j]);
                }
                else if (j >= 9)
                {
                    //[32] 00|01|02|03|04|05|06|07|08|09|10|11|12|13|14|15|16|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|
                    //    gst x---- y---- z----btncnt objx---- y---- z---- a---- objx---- y---- z---- a---- objx---- y---- z---- a----
                    if ((j - 0) % 9 == 0) // 9 18 27
                    {
                        sprintf(logbuf, "%s || (NO:%d OBJ:%4d|", logbuf, (j / 9), (uint8_t)m_pixart_rdbuff[j]);
                    }
                    else if ((j - 2) % 9 == 0) // 11 20 29 X
                    {
                        int16_t new_xyza = (int16_t)((int16_t)m_pixart_rdbuff[j - 1] + (int16_t)m_pixart_rdbuff[j] * 256);
                        sprintf(logbuf, "%sX:", logbuf);
                        sprintf(logbuf, "%s%4d|", logbuf, old_xyza[((j + 7) / 9 * 4) + 0] - new_xyza);
                        old_xyza[(j + 7 / 9 * 4) + 0] = new_xyza;
                    }
                    else if ((j - 4) % 9 == 0) // 13 22 31 Y 22
                    {
                        int16_t new_xyza = (int16_t)((int16_t)m_pixart_rdbuff[j - 1] + (int16_t)m_pixart_rdbuff[j] * 256);
                        sprintf(logbuf, "%sY:", logbuf);
                        sprintf(logbuf, "%s%4d|", logbuf, old_xyza[(j + 7 / 9 * 4) + 1] - new_xyza);
                        old_xyza[(j + 7 / 9 * 4) + 1] = new_xyza;
                    }
                    else if ((j - 6) % 9 == 0) // 15 24 33
                    {
                        int16_t new_xyza = (int16_t)((int16_t)m_pixart_rdbuff[j - 1] + (int16_t)m_pixart_rdbuff[j] * 256);
                        sprintf(logbuf, "%sZ:", logbuf);
                        sprintf(logbuf, "%s%4d|", logbuf, old_xyza[(j + 7 / 9 * 4) + 2] - new_xyza);
                        old_xyza[(j + 7 / 9 * 4) + 2] = new_xyza;
                    }
                    else if ((j + 1) % 9 == 0)
                    {
                        sprintf(logbuf, "%sA:", logbuf);
                        sprintf(logbuf, "%s%2d)", logbuf, (int16_t)((int16_t)m_pixart_rdbuff[j - 1] + (int16_t)m_pixart_rdbuff[j] * 256));
                    }
                }
            }
            ________DBG_20210801_pct_touch_packet_value_sprint("%s", logbuf);
        }
#endif

#ifdef ________DBG_20210801_pct_touch_packet_raw_sprint
        if (m_pixart_rdbuff[0] != 0x43)
        {
            char logbuf[250];
            sprintf(logbuf, "[%d] ", packet_cursor);
            for (int j = 0; j <= packet_cursor; j++)
            {
                sprintf(logbuf, "%s%02x|", logbuf, m_pixart_rdbuff[j]);
            }
            ________DBG_20210801_pct_touch_packet_raw_sprint("%s", logbuf);
        }
#endif

        rv = twi_perform(NULL, pixart_read_report_end_transfers, sizeof(pixart_read_report_end_transfers) / sizeof(nrf_twi_mngr_transfer_t));
        if (NRF_SUCCESS != rv)
        {
            MKB_LOG_ERROR("###Error");
            return (NRF_ERROR_INTERNAL);
        }
    }

#if 0
    { // ack_clear 동작.
        rv = twi_perform(NULL, pixart_ack_clear_transfers, sizeof(pixart_ack_clear_transfers) / sizeof(nrf_twi_mngr_transfer_t));
        if (NRF_SUCCESS != rv)
        {
            MKB_LOG_ERROR("###Error");
            return (NRF_ERROR_INTERNAL);
        }
    }
#endif

    return (NRF_SUCCESS);
}

static void pixart_wait_timeout_handler(void *p_context)
{
    UNUSED_PARAMETER(p_context);
}

//-------------------------------------------------------------------------------------------------------------------------
//			pixart_read_keys_process
//-------------------------------------------------------------------------------------------------------------------------
ret_code_t pixart_read_keys_process(mokibo_touch_data_t *pInfo)
{

    pixart_packet_t *p_pct_packet = (pixart_packet_t *)m_pixart_rdbuff;

    uint8_t key_len; //눌린 일반 키 갯수
    uint8_t mod_len; //눌린 모드 키 갯수

    uint8_t key_packets[6];
    memcpy(key_packets, p_pct_packet->keyboard.key, PIXART_MAX_KEYS);
    uint8_t mod_packet = p_pct_packet->keyboard.modkey;

    static uint8_t old_key_packets[6];
    static uint8_t old_mod_packet;

    bool is_key_packet_changed = false;
    bool is_mod_packet_changed = false;

    if (p_pct_packet->keyboard.type == 0x43)
    {

        ________DBG_20210801_pct_keyboard_raw("size : %d or %d ,gesture type: 0x%02x, key: [0x%02x][0x%02x][0x%02x][0x%02x][0x%02x][0x%02x]",
                                              sizeof(p_pct_packet), sizeof(p_pct_packet),
                                              p_pct_packet->keyboard.type,
                                              p_pct_packet->keyboard.key[0], p_pct_packet->keyboard.key[1], p_pct_packet->keyboard.key[2],
                                              p_pct_packet->keyboard.key[3], p_pct_packet->keyboard.key[4], p_pct_packet->keyboard.key[5]);

        ________DBG_20210802_kbd_func_check("step1");

        for (uint8_t i = 0; i < PIXART_MAX_KEYS; i++)
        {
            if (key_packets[i] != old_key_packets[i])
            {
                is_key_packet_changed = true;
                memcpy(old_key_packets, key_packets, PIXART_MAX_KEYS);
                break;
            }
        }

        ________DBG_20210810_keyboard_modkey_debug("old_modi: %02x -> new_modi: %02x", old_mod_packet, mod_packet);

        if (mod_packet != old_mod_packet)
        {
            is_mod_packet_changed = true;
            old_mod_packet = mod_packet;
        }

        for (uint8_t i = 0; i < PIXART_MAX_KEYS; i++)
        {
            if (p_pct_packet->keyboard.key[i] > 0)
                key_len++;
        }

        while (mod_packet)
        {
            mod_len += (mod_packet & 1);
            mod_packet >>= 1;
        }

        ________DBG_20210802_kbd_func_check("step2 keycnt :");

        if (is_key_packet_changed || is_mod_packet_changed)
        {
            ________DBG_20210802_kbd_func_check("step3");
            if (key_len || mod_len) // KEY_DOWN
            {

                if (m_pixart_read_keys_handler)
                {
                    ________DBG_20210802_kbd_func_check("step4-1");
                    m_pixart_read_keys_handler(p_pct_packet->keyboard.key, key_len, false);
                    ________DBG_20210830_key_event_dump_phenomenon_debug("read_keys_handler");
                }
            }

            if (((key_len == 0) && (mod_len == 0)) && ((m_pixart_key_cnt) || (m_pixart_mod_key_cnt))) // ALL KEY_UP EDGE
            {

                if (m_pixart_read_keys_handler)
                {
                    ________DBG_20210802_kbd_func_check("step4-2");
                    m_pixart_read_keys_handler(p_pct_packet->keyboard.key, key_len, false);
                    ________DBG_20210830_key_event_dump_phenomenon_debug("read_keys_handler clear");
                }
            }
        }

        m_pixart_key_cnt = key_len;
        m_pixart_mod_key_cnt = mod_len;

        return NRF_SUCCESS; // 0
    }
    else
        return NRF_ERROR_INTERNAL;
}

//-------------------------------------------------------------------------------------------------------------------------
//			pixart_read_process
//-------------------------------------------------------------------------------------------------------------------------
//받아온 pixart 데이터를 p_mkb_touch 에 매핑한다.
ret_code_t pixart_read_touch_process(mokibo_touch_data_t *p_mkb_touch)
{
    ret_code_t rv;

    pixart_packet_t *p_pct_packet;                     // len, pHeader, touch
    p_pct_packet = (pixart_packet_t *)m_pixart_rdbuff; // PCT로부터 읽어온 버퍼

    pixart_finger_packet_t *p_pct_finger; // touchType touchID eventID tip x y majoraxis minorAxis orientation //pct_packet에서 꺼낸 각 핑거패킷
    mokibo_finger_data_t *p_mkb_fingers;  // cpt_finger_packet을 복사해서mkb_touchpad_read_prcess 로 넘김. 모아서 p_mkb_touch에 넣을거임
    // p_mkb_touch.p_mkb_fingers <= p_pct_packet.p_pct_finger <= m_pixart_rdbuff 로 전달하는 과정

    // more than 10 touches
    if (p_pct_packet->touch.touch_count > PIXART_INFO_MAX_COUNT)
    {
        //터치 손가락갯수가 10개 넘음
        MKB_LOG_WARN("too much finger!");
        return NRF_ERROR_INTERNAL; // 3
    }

    ________DBG_20210801_pct_gesture_raw("%02x", p_pct_packet->touch.gesture.type);

    uint8_t valid_touch = 0; //손가락 번호
    // mkb_finger에  마다 pct_finger 데이터 복사
    {
        for (uint8_t i = 0; i < p_pct_packet->touch.touch_count; i++)
        {
            p_pct_finger = &(p_pct_packet->touch.fingers[i]); ////touchType touchID eventID tip x y majoraxis minorAxis orientation

            p_mkb_fingers = &(p_mkb_touch->fingers[valid_touch]); //현재 손가락의 터치 데이터 복사

            // raw데이터들 똑같이 옮긴다.
            p_mkb_fingers->touchID = p_pct_finger->touchID;
            p_mkb_fingers->tip = p_pct_finger->tip;
            p_mkb_fingers->confidence = p_pct_finger->confidence;
            p_mkb_fingers->tip = p_pct_finger->tip;
            p_mkb_fingers->x = p_pct_finger->x;
            p_mkb_fingers->y = p_pct_finger->y;
            p_mkb_fingers->z = p_pct_finger->z;
            p_mkb_fingers->area = p_pct_finger->area; //닿는 지점의 크기인듯?
            valid_touch++;
        }
    }

    // mkb_touch에 pct_touch 데이터 복사
    {
        p_mkb_touch->touchCnt = valid_touch;          //손가락 번호
        p_mkb_touch->timestamp = app_timer_cnt_get(); // NRF의 타임스탬프
        p_mkb_touch->button = p_pct_packet->touch.button;
        p_mkb_touch->gesture = p_pct_packet->touch.gesture;
    }

    if (m_pixart_read_touch_handler)
    {
        m_pixart_read_touch_handler(NRF_SUCCESS, p_mkb_touch);
    }

    return NRF_SUCCESS;
}

static void pixart_read_cb(ret_code_t result, void *p_user_data)
{
    ret_code_t err_code;
    //________DBG_20210830_key_event_dump_phenomenon_debug("pixart_read_cb");
    if (result == NRF_SUCCESS)
    {
        err_code = pixart_read_keys_process(p_user_data);
        if (err_code != NRF_SUCCESS)
        {
            err_code = pixart_read_touch_process(p_user_data);
        }
    }
}

static ret_code_t pixart_read_schedule(mokibo_touch_data_t *p_data)
{
    ret_code_t rv;

    rv = pixart_read_report();
    if (rv == NRF_SUCCESS)
    {
        //받아온 m_pixart_rdbuff 를 읽는다
        pixart_read_cb(NRF_SUCCESS, &m_touch_info);
    }
    return rv;
}

/*
인터럽트가 들어오면
1. 스테이터스 != 0 인지 확인
2. 스테이터스 != 1 와치독 != 1 확인
3. read report
    1. 쓰기: 0x09 에 0x08
    2. 쓰기: 0x0A 에 0x00
    3. 읽기: 0x0B * REPORT_SIZE
    4. 쓰기: 0x0A 에 0x01
4. ack and clear interrupt

*/

/*
static app_gpiote_user_id_t m_pixart_gpiote; //!< GPIOTE Handle.
return app_gpiote_user_enable(m_pixart_gpiote);
return app_gpiote_user_disable(m_pixart_gpiote);
app_gpiote_user_register_ex(&m_pixart_gpiote, &m_interrupt_pin, 1, pixart_interrupt_handler);

drv_pixart_init() {app_gpiote_user_register_ex()-> pixart_interrupt_handler()}
pixart_interrupt_handler() {app_sched_event_put(NULL, 0, pixart_scan_handler);}
pixart_scan_handler()
{
    pixart_status()
    {
        [transfer_read_status]
    }
    pixart_read_schedule()
    {
        read_report()
        {
            [transfer_read_report_start]
            [transfer_read_report_single]
            [transfer_read_report_loop]
            [transfer_read_report_end]
            [transfer_ack_clear]
        }
        read_cb()
        {
            read_key_process(){read_keys_handler()}
            read_touch_process(){read_touch_handler()}
        }
    }
}
*/

#define ENABLE_ATOMIC_FLAG 1

#if 0
static void pixart_scan_handler()
{
    ret_code_t err_code;
    m_is_scanhandler_run = true;
    if (m_pixart_enabled)
    {
        while (m_cnt_interrupted > 0)
        {
            ________DBG_20211130_check_for_i2c_new_scanner("interrupted left..%d", m_cnt_interrupted);
            if (pixart_is_TT_INT_low(4) == NRF_SUCCESS) // if (pixart_read_TT_INT() == PIXART_PIN_LOW)
            {
                // app_timer_stop(m_pixart_watchdog_timer);
                if (nrf_atomic_flag_set_fetch(&m_pixart_operation_active))
                {
                    return;
                }

                { //통신
                    err_code = pixart_status();

                    if (NRF_SUCCESS == err_code)
                    {
                        ________DBG_20211130_check_for_i2c_new_scanner("status_success");
                        pixart_read_schedule(&m_touch_info);
                    }
                    else
                    {
                        ________DBG_20211130_check_for_i2c_new_scanner("status fail:%x", err_code);
                        return;
                    }

                    if (err_code >= 0x8000) // 0x8000 == status check_error, lx
                    {
                        ________DBG_20211130_check_for_i2c_new_scanner("status fail:%x", err_code);
                        pixart_software_reset();
                        pixart_init();
                        pixart_sense_disable();
                    }
                }

                { //통신 완료
                    err_code = twi_perform(NULL, pixart_ack_clear_transfers, sizeof(pixart_ack_clear_transfers) / sizeof(nrf_twi_mngr_transfer_t));
                    if (err_code != NRF_SUCCESS)
                    {
                        ________DBG_20211130_check_for_i2c_new_scanner("ack error:%d", err_code);
                    }
                    else
                    {
                    }
                }

                if (pixart_is_TT_INT_high(8) == NRF_SUCCESS)
                {
                    ________DBG_20211130_check_for_i2c_new_scanner("ack ok");
                    // OK
                }
                else
                {
                    ________DBG_20211130_check_for_i2c_new_scanner("ack wait");
                }

#if ENABLE_ATOMIC_FLAG == 1
                nrf_atomic_flag_clear(&m_pixart_operation_active);
#endif
            }
            if (m_cnt_interrupted > 0)
            {
                m_cnt_interrupted--;
                ________DBG_20211130_check_for_i2c_new_scanner("m_cnt_interrupted--:%d", m_cnt_interrupted);
            }
            // app_timer_start(m_pixart_watchdog_timer, APP_TIMER_TICKS(1000), NULL);
        }
    }
    m_is_scanhandler_run = false;
}
#else

static void pixart_scan_handler()
{
    m_is_scanhandler_run = true;
    m_cnt_scanned++;
    ________DBG_20211117_check_for_i2c_stop_race_check("  [Scan:%d]START", m_cnt_scanned);
#ifdef ________DBG_20211117_check_for_i2c_stop_sprintf
    ________DBG_20211117_check_for_i2c_stop_sprintf("  [Scn] START");
#endif
    // UNUSED_PARAMETER(p_event_data);
    // UNUSED_PARAMETER(event_size);

    ret_code_t err_code;

#ifdef ________DBG_20211117_check_for_i2c_stop_sprintf
    sprintf(logbuf, "%s [Scn]ena:%d,", logbuf, m_pixart_enabled);
#endif

    if (m_pixart_enabled)
    {

        {
            //인터럽트 핀 확인
            if (pixart_is_TT_INT_low(4) == NRF_SUCCESS) // if (pixart_read_TT_INT() == PIXART_PIN_LOW)
            {

                if (m_pixart_sense_enabled == true)
                {
                    pixart_sense_disable();
                }
#ifdef ________DBG_20211117_check_for_i2c_stop_sprintf
                sprintf(logbuf, "%s [IO]int:LOW,", logbuf);
#endif

#if ENABLE_ATOMIC_FLAG == 1
                if (nrf_atomic_flag_set_fetch(&m_pixart_operation_active))
                {
                    MKB_LOG_DEBUG("flag set fetch Busy");
#ifdef ________DBG_20211117_check_for_i2c_stop_sprintf
                    sprintf(logbuf, "%s [!]fetch:fail! -> return;", logbuf);
                    ________DBG_20211117_check_for_i2c_stop_sprintf("%s", logbuf);
#endif
                    return;
                }
#endif
                err_code = pixart_status();

                if (NRF_SUCCESS == err_code)
                {
                    m_pixart_idle_cnt = 0;
                    ________DBG_20211117_check_for_i2c_stop("pixart_status:SUCCESS, pixart_read_schedule..");
#ifdef ________DBG_20211117_check_for_i2c_stop_sprintf
                    sprintf(logbuf, "%s [Stt]status:OK,", logbuf);
#endif
                    pixart_read_schedule(&m_touch_info);
                }
                else
                {
                    m_pixart_idle_cnt++;
                    ________DBG_20211117_check_for_i2c_stop("pixart_status:FAIL(try:%d)", m_pixart_idle_cnt);
#ifdef ________DBG_20211117_check_for_i2c_stop_sprintf
                    sprintf(logbuf, "%s [Stts]status:NG(try:%d),", logbuf, m_pixart_idle_cnt);
#endif
                }

                if (err_code >= 0x8000) // 0x8000 == status check_error, lx
                {
                    ________DBG_20211117_check_for_i2c_stop("pixart_status:ERROR, will pct reset(reason:%x)", err_code);
#ifdef ________DBG_20211117_check_for_i2c_stop_sprintf
                    sprintf(logbuf, "%s [Stts]status:ERR!,", logbuf);
#endif
                    m_pixart_idle_cnt = 0;
                    pixart_software_reset();
                    pixart_init();
                }
                else // pixart_is_TT_INT_low(4) == FAIL)
                {
                    // MKB_LOG_DEBUG("pixart_intterrupt : high");
                    m_pixart_idle_cnt++;
                    ________DBG_20211117_check_for_i2c_stop("wait_TTINT_low(4) = FAIL(try:%d)", m_pixart_idle_cnt);
#ifdef ________DBG_20211117_check_for_i2c_stop_sprintf
                    sprintf(logbuf, "%s [IOX]int:HIGH(try:%d),", logbuf, m_pixart_idle_cnt);
#endif
                }

                {
                    ________DBG_20211130_check_for_i2c_int_high("");
                    if (pixart_is_TT_INT_high(4) == NRF_SUCCESS)
                    {
                        ________DBG_20211130_check_for_i2c_int_high("[BeforeClr]int:HIGH!!!!!!!!");
                    }
                    else
                    {
                        ________DBG_20211130_check_for_i2c_int_high("[BeforeClr]int:LOW");
                    }

                    {
                        err_code = twi_perform(NULL, pixart_ack_clear_transfers, sizeof(pixart_ack_clear_transfers) / sizeof(nrf_twi_mngr_transfer_t));
                        if (NRF_SUCCESS != err_code)
                        {
#ifdef ________DBG_20211117_check_for_i2c_stop_sprintf
                            sprintf(logbuf, "%s [Clr]ack:NG,", logbuf);
#endif

                            err_code = twi_perform(NULL, pixart_ack_clear_transfers, sizeof(pixart_ack_clear_transfers) / sizeof(nrf_twi_mngr_transfer_t));
                            if (NRF_SUCCESS != err_code)
                            {
                                MKB_LOG_ERROR("###Error");
                            }
                            else
                            {
#ifdef ________DBG_20211117_check_for_i2c_stop_sprintf
                                sprintf(logbuf, "%s [Clr]ack:OK,", logbuf);
#endif
                                nrf_atomic_flag_clear(&m_pixart_operation_active);
                                if (m_pixart_sense_enabled == false)
                                {
                                    pixart_sense_enable();
                                }
                            }
                        }
                        else
                        {
#ifdef ________DBG_20211117_check_for_i2c_stop_sprintf
                            sprintf(logbuf, "%s [Clr]ack:OK,", logbuf);
#endif
                            nrf_atomic_flag_clear(&m_pixart_operation_active);
                            if (m_pixart_sense_enabled == false)
                            {
                                pixart_sense_enable();
                            }
                        }
                    }
                    /*
                    for (uint8_t ack_cnt = 0;; ack_cnt++)
                    {
                        //________DBG_20211130_check_for_i2c_int_high("[clrtry:%d]", ack_cnt);
                        err_code = twi_perform(NULL, pixart_ack_clear_transfers, sizeof(pixart_ack_clear_transfers) / sizeof(nrf_twi_mngr_transfer_t));

                        if (NRF_SUCCESS == err_code)
                        {
                            if (pixart_is_TT_INT_low(4) == NRF_SUCCESS)
                            {
                                if (ack_cnt < 100)
                                {
                                    ________DBG_20211130_check_for_i2c_int_high("[Clr]ack:OKLOW(try:%d)!!!!!!!!!!!!!!", ack_cnt);
                                }
                                else
                                {

                                    ________DBG_20211130_check_for_i2c_int_high("[Clr]ack:FAIL(try:%d)!!!!!!!!!!!!!!!", ack_cnt);

                                    break;
                                }
                            }
                            else
                            {
                                ________DBG_20211130_check_for_i2c_int_high("[Clr]ack:OKHIGH,", logbuf);
                                break;
                            }
                        }
                        else
                        {

                            ________DBG_20211130_check_for_i2c_int_high("[Clr]ack:NG!!!!!!!x!x!!!!!,");
                            // MKB_LOG_ERROR("###Error");
                            break;
                        }
                    }
                    */

#if ENABLE_ATOMIC_FLAG == 1
                    nrf_atomic_flag_clear(&m_pixart_operation_active);
#endif
                }
            }
        }

        //타이머 트리거됐을때 일정횟수 이상 초과하면 종료
        {
            if (pixart_is_TT_INT_high(4) != NRF_SUCCESS)
            {
#ifdef ________DBG_20211117_check_for_i2c_stop_sprintf
                sprintf(logbuf, "%s [IOX]TIMER_START:(try:%d),", logbuf, m_pixart_idle_cnt);
#endif
                err_code = app_timer_start(m_pixart_timer, 0, NULL); //
                if (err_code != NRF_SUCCESS)
                {
                    MKB_ERROR_CHECK(err_code);
                }
            }
            else if (m_pixart_idle_cnt == 0) //(m_pixart_idle_cnt < PIXART_INT_IDLE_CNT) // 5
            {
#ifdef ________DBG_20211117_check_for_i2c_stop_sprintf
                sprintf(logbuf, "%s [IOX]TIMER_START:(try:%d),", logbuf, m_pixart_idle_cnt);
#endif
                err_code = app_timer_start(m_pixart_timer, APP_TIMER_TICKS(PIXART_INT_TIMER_INTERVAL), NULL); // 1ms 뒤 스스로 다시 부름
                if (err_code != NRF_SUCCESS)
                {
                    MKB_ERROR_CHECK(err_code);
                }
            }
            else
            {
#ifdef ________DBG_20211117_check_for_i2c_stop_sprintf
                sprintf(logbuf, "%s [IOX]TIMER_END:(try:%d),", logbuf, m_pixart_idle_cnt);
#endif
                m_pixart_idle_cnt = 0;

                // Eable sensing.

                if (m_pixart_sense_enabled == false)
                {
                    pixart_sense_enable();
                }

#ifdef ________DBG_20211117_check_for_i2c_stop_sprintf
                sprintf(logbuf, "%s [IOX],", logbuf);
#endif
            }
        }

#ifdef ________DBG_20211117_check_for_i2c_stop_sprintf
        ________DBG_20211117_check_for_i2c_stop_sprintf("       %s", logbuf);
        ________DBG_20211117_check_for_i2c_stop_sprintf("   pixart_scan_handler() END");
        memset(logbuf, 0, sizeof(char) * 512);
#endif
    }
    else // pixart_enabled == false
    {
    }
    ________DBG_20211117_check_for_i2c_stop_race_check("  [Scan]END");
    m_is_scanhandler_run = false;
    m_cnt_interrupted--;
}
#endif

#if (PIXART_MODE == PIXART_MODE_GPIOTE)
/**@brief GPIOTE PORT interrupt handler.*/

uint32_t last_int_time = 0;
static void pixart_interrupt_handler()
{
    // timer 와 gpio 사이에 race condition 이 발생한다.
    // scan_handler() 가 동작이 끝나면 INT:HIGH 로 변함

    // timer 도 gpio 도 main() 도 다 다른스레드라고 생각해야함

    // INT:LOW 상태에서 sense_enable() 을 하면 인식을 못한다.

    // INT:HIGH 1ms마다 3번 검사 * 4번체크

    if (m_pixart_enabled)
    {
        if (pixart_is_TT_INT_low(4) == NRF_SUCCESS) // if (pixart_read_TT_INT() == PIXART_PIN_LOW)
        {

            //________DBG_20211130_check_for_i2c_app_sched("%d/%d", app_sched_queue_space_get(), app_sched_queue_utilization_get());

#ifdef ________DBG_20211117_check_for_i2c_stop_sprintf
            ________DBG_20211117_check_for_i2c_stop_sprintf("[Int]");
#endif

#if 1
            {
                if (app_timer_cnt_diff_compute(app_timer_cnt_get(), last_int_time) > APP_TIMER_TICKS(2))
                {
                    m_cnt_interrupted++;
                    ________DBG_20211130_check_for_i2c_new_scanner("intterupted++: %d", m_cnt_interrupted);
                    last_int_time = app_timer_cnt_get();

                    ________DBG_20211117_check_for_i2c_stop_race_check("[Int:%d]sched:%d", m_cnt_interrupted, app_sched_queue_space_get());

                    {
                        if (app_sched_queue_space_get() > 10)
                        {
                            ret_code_t ret_code = app_sched_event_put(NULL, 0, pixart_scan_handler);
                            MKB_ERROR_CHECK(ret_code);
                        }
                    }
                }
            }

#else
            {
                ________DBG_20211117_check_for_i2c_stop_race_check("[Int:%d]", m_cnt_interrupted);
                ret_code_t err_code = app_timer_start(m_pixart_timer, 0, NULL);
                if (err_code != NRF_SUCCESS)
                {
                    MKB_ERROR_CHECK(err_code);
                }
            }

#endif
        }
        else
        {
            MKB_LOG_DEBUG("INT_HIGH");
        }
    }
}

#elif (PIXART_MODE == PIXART_MODE_TIMER)
#endif /* (PIXART_MODE == PIXART_MODE_GPIOTE) */
static void pixart_timeout_handler(void *p_context)
{
    // MKB_LOG_INFO("<timeout_handler>");
    UNUSED_PARAMETER(p_context);

#ifdef ________DBG_20211117_check_for_i2c_stop_sprintf
    ________DBG_20211117_check_for_i2c_stop_sprintf("[Tmr]");
#endif

    if (m_is_scanhandler_run == false)
    {
        if (pixart_is_TT_INT_low(4) == NRF_SUCCESS)
        {
            if (m_pixart_enabled)
            {
                ________DBG_20211130_check_for_i2c_timer_triggerd("ack and low. timer triggerd");
                {
                    m_cnt_timered++;
                    ________DBG_20211117_check_for_i2c_stop_race_check("[Timer:%d]", m_cnt_timered);
                    ret_code_t ret_code = app_sched_event_put(NULL, 0, pixart_scan_handler);
                    MKB_ERROR_CHECK(ret_code);
                }
            }
        }
    }
}

static void pixart_watchdog_timer_handler(void *p_context)
{
    if (pixart_read_TT_INT() == PIXART_PIN_LOW && m_is_scanhandler_run == false)
    {
        ________DBG_20211130_check_for_i2c_new_scanner("timer_triggered");
        ________DBG_20211130_check_for_i2c_watchdog("watchdog triggered!!!!!!!!");
        pixart_interrupt_handler();
    }
}

static ret_code_t pixart_init(void)
{
    ret_code_t err_code;
    aamode_cnt = 0;
    err_code = pixart_boot_ready();
    if (err_code != NRF_SUCCESS)
    {
        err_code = pixart_boot_ready();
        if (err_code != NRF_SUCCESS)
        {
            MKB_ERROR_CHECK(err_code);
            return NRF_ERROR_INTERNAL;
        }
    }

    MKB_LOG_INFO("Application Start OK !!");

    nrf_delay_ms(50);

    return NRF_SUCCESS;
}

ret_code_t drv_pixart_reset(void)
{
    ret_code_t err_code;
    aamode_cnt = 0;
    if (!m_pixart_enabled)
    {
        err_code = pixart_init();
        if (NRF_SUCCESS != err_code)
        {
            MKB_LOG_ERROR("Error! Initial[%d]", err_code);
            return NRF_ERROR_INTERNAL;
        }
    }

    return NRF_SUCCESS;
}

ret_code_t drv_pixart_sleep(void)
{
    ret_code_t err_code;

    if (!m_pixart_enabled)
    {
        // err_code = pixart_deep_sleep();
        MKB_ERROR_CHECK(err_code);
    }

    return NRF_SUCCESS;
}

ret_code_t drv_pixart_wakeup(void)
{
    ret_code_t err_code;

    if (!m_pixart_enabled)
    {
        // err_code = pixart_wake_up();
        MKB_ERROR_CHECK(err_code);
    }

    return NRF_SUCCESS;
}

ret_code_t drv_pixart_enable(void)
{
    ret_code_t err_code;

    if (!m_pixart_enabled && m_pixart_init)
    {
        pixart_resume();

#if (PIXART_MODE == PIXART_MODE_GPIOTE)
        err_code = pixart_sense_enable();
#elif (PIXART_MODE == PIXART_MODE_TIMER)
        err_code = app_timer_start(m_pixart_timer, APP_TIMER_TICKS(PIXART_TIMER_INTERVAL), NULL);
#endif /* (PIXART_MODE == PIXART_MODE_GPIOTE) */
        if (err_code != NRF_SUCCESS)
        {
            MKB_ERROR_CHECK(err_code);
            return err_code;
        }

        MKB_LOG_INFO("Enable Done!");

        m_pixart_enabled = true;
    }

    return NRF_SUCCESS;
}

ret_code_t drv_pixart_disable(void)
{
    ret_code_t err_code;

    if (m_pixart_enabled && m_pixart_init)
    {
        m_pixart_enabled = false;

        // err_code = app_timer_stop(m_pixart_wait_timer);
        if (err_code != NRF_SUCCESS)
        {
            MKB_ERROR_CHECK(err_code);
        }

#if (PIXART_MODE == PIXART_MODE_GPIOTE)
        err_code = pixart_sense_disable();
#elif (PIXART_MODE == PIXART_MODE_TIMER)
        err_code = app_timer_stop(m_pixart_timer);
#endif /* (PIXART_MODE == PIXART_MODE_GPIOTE) */
        if (err_code != NRF_SUCCESS)
        {
            MKB_ERROR_CHECK(err_code);
            return err_code;
        }
    }

    pixart_suspend();

    m_pixart_enabled = false;

    return NRF_SUCCESS;
}

ret_code_t drv_pixart_init(pixart_read_handler_t read_handler)
{
    ret_code_t err_code;

    // Initialize driver state.
    nrf_atomic_flag_clear(&m_pixart_operation_active);

    m_pixart_read_touch_handler = read_handler; // mkb_touchpad_read_process(void *p_data);

    m_pixart_init = false;
    m_pixart_enabled = false;
    m_pixart_idle_cnt = 0;
    m_pixart_key_cnt = 0;

    pixart_gpio();

#if (PIXART_MODE == PIXART_MODE_GPIOTE)
    // uint32_t low_to_high_mask = 0;
    // uint32_t high_to_low_mask = PIXART_GPIOTE_MASK;

    const app_gpiote_user_pin_config_t m_interrupt_pin = {.pin_number = MKB_PIXART_INT_PIN, .sense = GPIO_PIN_CNF_SENSE_Low};

    err_code = app_gpiote_user_register_ex(&m_pixart_gpiote, &m_interrupt_pin, 1, pixart_interrupt_handler);
    // err_code = app_gpiote_user_register(&m_pixart_gpiote, &low_to_high_mask,&high_to_low_mask, pixart_interrupt_handler);

    if (err_code != NRF_SUCCESS)
    {
        MKB_ERROR_CHECK(err_code);
        return err_code;
    }

    err_code = pixart_sense_disable();

    if (err_code != NRF_SUCCESS)
    {
        MKB_ERROR_CHECK(err_code);
        return err_code;
    }

    err_code = app_timer_create(&m_pixart_timer, APP_TIMER_MODE_SINGLE_SHOT, pixart_timeout_handler);
    err_code = app_timer_create(&m_pixart_watchdog_timer, APP_TIMER_MODE_REPEATED, pixart_watchdog_timer_handler);
    if (err_code != NRF_SUCCESS)
    {
        MKB_ERROR_CHECK(err_code);
        return err_code;
    }

#endif /* (PIXART_MODE == PIXART_MODE_GPIOTE) */

    // err_code = app_timer_create(&m_pixart_wait_timer, APP_TIMER_MODE_REPEATED, pixart_wait_timeout_handler);
    if (err_code != NRF_SUCCESS)
    {
        MKB_ERROR_CHECK(err_code);
        return err_code;
    }

#if (defined(MKB_TWI_PROBE_ENABLED) && MKB_TWI_PROBE_ENABLED)
    if (twi_probe_exist(PIXART_ADDR))
#endif //(defined(MKB_TWI_PROBE_ENABLED) && MKB_TWI_PROBE_ENABLED)
    {
        nrf_delay_ms(10);

        do
        {
            err_code = pixart_software_reset();
            if (NRF_SUCCESS != err_code)
            {
                MKB_LOG_ERROR("Error! software Reset [%d]", err_code);
                MKB_ERROR_CHECK(err_code);
                break;
            }

            err_code = pixart_init();
            if (NRF_SUCCESS != err_code)
            {
                MKB_LOG_ERROR("Error! Initial[%d]", err_code);
                MKB_ERROR_CHECK(err_code);
                break;
            }

            m_pixart_init = true;
        } while (0);
    }

    nrf_delay_ms(10);

    uint32_t CRC = Read_CRC(0x02);
    uint16_t prj_ver = pixart_get_prj_ver();
    ________DBG_20210811_print_prj_ver("prj:%02x, ver:%02x", (prj_ver & 0xFF00) >> 8, (prj_ver & 0x00FF));
    ________DBG_20210811_print_prj_ver("CRC: %lu, %x", CRC, CRC);

    if (1)
    {
        if (prj_ver != pixart_dfu_hex_prjver)
        {
            ________DBG_20210811_pct_i2c_dfu_test("Start Flash_Update_Process()");
            Update_Flash(pixart_dfu_hex, pixart_dfu_hex_size, 0x00);
        }
    }

    nrf_delay_ms(10);

    pixart_get_prj_ver();

    if (m_pixart_init)
    {
        drv_pixart_disable();
        err_code = NRF_SUCCESS;
        MKB_LOG_INFO("Initialize Done!");
    }
    else
    {
        err_code = NRF_ERROR_INTERNAL;
        MKB_LOG_ERROR("can't find senser, Initialize Fail!");
    }

    // err_code = app_timer_start(m_pixart_wait_timer, APP_TIMER_TICKS(1000), NULL);
    err_code = app_timer_start(m_pixart_watchdog_timer, APP_TIMER_TICKS(36), NULL);

    return err_code;
}

ret_code_t drv_pixart_register_key_handler(pixart_key_read_handler_t read_handler)
{
    ret_code_t err_code;

    m_pixart_read_keys_handler = read_handler; // mkb_keyboard_read_handler

    return NRF_SUCCESS;
}

ret_code_t mkb_pixart_cli(size_t size, char **params)
{
    ret_code_t err_code = NRF_SUCCESS;
    if (!strcmp(params[1], "help"))
    {
        ________DBG_20210831_mkb_cli_dbg("1:drv_pixart_enable");
        ________DBG_20210831_mkb_cli_dbg("2:drv_pixart_disable");
        ________DBG_20210831_mkb_cli_dbg("3:drv_pixart_reset");
        ________DBG_20210831_mkb_cli_dbg("4:pixart_sense_disable");
        ________DBG_20210831_mkb_cli_dbg("5:pixart_sense_enable");
        ________DBG_20210831_mkb_cli_dbg("6:pixart_read_TT_INT");
        ________DBG_20210831_mkb_cli_dbg("7:pixart_is_TT_INT_low");
        ________DBG_20210831_mkb_cli_dbg("8:int_restart");
    }
    else if (!strcmp(params[1], "drv_pixart_enable"))
    {
        drv_pixart_enable();
    }
    else if (!strcmp(params[1], "drv_pixart_disable"))
    {
        drv_pixart_disable();
    }
    else if (!strcmp(params[1], "drv_pixart_reset"))
    {
        drv_pixart_reset();
    }
    else if (!strcmp(params[1], "pixart_sense_disable"))
    {
        pixart_sense_disable();
        ________DBG_20210831_mkb_cli_dbg("err_code", err_code);
    }
    else if (!strcmp(params[1], "pixart_sense_enable"))
    {
        pixart_sense_enable();
        ________DBG_20210831_mkb_cli_dbg("err_code", err_code);
    }
    else if (!strcmp(params[1], "pixart_read_TT_INT"))
    {
        uint8_t pinValue = pixart_read_TT_INT();
        ________DBG_20210831_mkb_cli_dbg("pixart_read_TT_INT():%d", pinValue);
    }
    else if (!strcmp(params[1], "pixart_is_TT_INT_low"))
    {
        uint8_t pinValue = pixart_is_TT_INT_low(4);
        ________DBG_20210831_mkb_cli_dbg("pixart_is_TT_INT_low():%d", pinValue);
    }
    else if (!strcmp(params[1], "int_restart"))
    {
        if (app_sched_queue_space_get() > 10)
        {
            ret_code_t ret_code = app_sched_event_put(NULL, 0, pixart_scan_handler);
            MKB_ERROR_CHECK(ret_code);
        }
        ________DBG_20210831_mkb_cli_dbg("app_sched_event_put(NULL, 0, pixart_scan_handler);");
    }
    else
    {
        err_code = NRF_ERROR_INTERNAL;
    }
    return err_code;
    /*
    drv_pixart_enable();
    drv_pixart_disable();
    drv_pixart_reset();
    drv_pixart_wakeup();
    mkb_event_send(EVT_TOUCH_TAB, 0);
    mkb_pixart_cli("ack_clear");
    mkb_pixart_cli(2);b
    */
}

#if (defined(NRF_PWR_MGMT_ENABLED) && NRF_PWR_MGMT_ENABLED)
static bool mkb_pixart_shutdown(nrf_pwr_mgmt_evt_t event)
{
    ret_code_t err_code;

    MKB_LOG_INFO("Shutdown prepare");

    if (m_pixart_enabled)
    {
        m_pixart_enabled = false;

#if (PIXART_MODE == PIXART_MODE_GPIOTE)
        err_code = pixart_sense_disable();
#elif (PIXART_MODE == PIXART_MODE_TIMER)
        err_code = app_timer_stop(m_pixart_timer);
#endif /* (PIXART_MODE == PIXART_MODE_GPIOTE) */
        if (err_code != NRF_SUCCESS)
        {
            MKB_ERROR_CHECK(err_code);
            return err_code;
        }
    }

#if (defined(MKB_PIXART_POWER_PIN_ENABLED) && MKB_PIXART_POWER_PIN_ENABLED)
    nrf_gpio_cfg_default(PIXART_POWER_PIN);
#endif //(defined(MKB_PIXART_POWER_PIN_ENABLED) && MKB_PIXART_POWER_PIN_ENABLED)

#if (defined(MKB_PIXART_RESET_PIN_ENABLED) && MKB_PIXART_RESET_PIN_ENABLED)
    nrf_gpio_cfg_default(PIXART_RESET_PIN);
#endif //(defined(MKB_PIXART_RESET_PIN_ENABLED) && MKB_PIXART_RESET_PIN_ENABLED)

#if (defined(MKB_PIXART_INT_PIN_ENABLED) && MKB_PIXART_INT_PIN_ENABLED)
    nrf_gpio_pin_dir_set(PIXART_INT_PIN, NRF_GPIO_PIN_DIR_INPUT);
    nrf_gpio_cfg_sense_set(PIXART_INT_PIN, NRF_GPIO_PIN_SENSE_LOW);
#endif //(defined(MKB_PIXART_INT_PIN_ENABLED) && MKB_PIXART_INT_PIN_ENABLED)

    return true;
}

NRF_PWR_MGMT_HANDLER_REGISTER(mkb_pixart_shutdown, SHUTDOWN_PRIORITY_DEFAULT);
#endif /* NRF_PWR_MGMT_ENABLED */

#endif // (defined(MKB_PIXART_ENABLED) && MKB_PIXART_ENABLED)
