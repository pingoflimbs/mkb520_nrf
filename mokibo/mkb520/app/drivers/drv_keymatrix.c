/*
* ===============================================================================
* Copyright (C) 2017-2019
* INNOPRESSO INC. All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
* ===============================================================================
*/

#include <stdbool.h>
#include <stdint.h>

#include "sdk_common.h"
#include "app_error.h"
#include "app_timer.h"
#include "app_scheduler.h"
#if defined(GPIOTE_ENABLED) && GPIOTE_ENABLED
#include "app_gpiote.h"
#endif

#include "nrf_assert.h"
#include "nrf_error.h"
#include "nrf_gpio.h"
#include "nrf_log.h"
#include "nrf_delay.h"
#include "nrf_cli.h"
#include "nrf_pwr_mgmt.h"

#define MKB_LOG_LEVEL MKB_KEYMATRIX_LOG_LEVEL
#include "mkb_common.h"
#include "drv_keymatrix.h"

#if (defined(MKB_KEYMATRIX_ENABLED) && MKB_KEYMATRIX_ENABLED)

APP_TIMER_DEF(m_keymatrix_timer);

static bool m_keymatrix_enabled;

static uint8_t m_keys_pressed[KEYMATRIX_MAX_KEYS]; //!< vector holding currently pressed keys. Filled up from index 0.
static uint8_t m_keys_pressed_size;                //!< Size of the currently pressed keys vector.
static bool m_keys_blocked;                        //!< True if some of keys are blocked and cannot be read.

/**@brief Key Matrix Column pins buffer */
static const uint8_t m_column_to_pin_map[KEYMATRIX_NUM_OF_COLUMNS] =
    {
        (uint8_t)(KEYMATRIX_PIN_COL_0),
        (uint8_t)(KEYMATRIX_PIN_COL_1),
        (uint8_t)(KEYMATRIX_PIN_COL_2),
        (uint8_t)(KEYMATRIX_PIN_COL_3),
        (uint8_t)(KEYMATRIX_PIN_COL_4),
        (uint8_t)(KEYMATRIX_PIN_COL_5),
        (uint8_t)(KEYMATRIX_PIN_COL_6),
        (uint8_t)(KEYMATRIX_PIN_COL_7),
        (uint8_t)(KEYMATRIX_PIN_COL_8),
        (uint8_t)(KEYMATRIX_PIN_COL_9),
        (uint8_t)(KEYMATRIX_PIN_COL_10),
        (uint8_t)(KEYMATRIX_PIN_COL_11),
        (uint8_t)(KEYMATRIX_PIN_COL_12),
        (uint8_t)(KEYMATRIX_PIN_COL_13),
        (uint8_t)(KEYMATRIX_PIN_COL_14),
};

/**@brief Key Matrix Row pins buffer */
static const uint8_t m_row_to_pin_map[KEYMATRIX_NUM_OF_ROWS] =
    {
        (uint8_t)(KEYMATRIX_PIN_ROW_0),
        (uint8_t)(KEYMATRIX_PIN_ROW_1),
        (uint8_t)(KEYMATRIX_PIN_ROW_2),
        (uint8_t)(KEYMATRIX_PIN_ROW_3),
        (uint8_t)(KEYMATRIX_PIN_ROW_4),
        (uint8_t)(KEYMATRIX_PIN_ROW_5),
        (uint8_t)(KEYMATRIX_PIN_ROW_6),
        (uint8_t)(KEYMATRIX_PIN_ROW_7),
};

/**@brief Key Matrix Lookup Table */
#if (KEYMATRIX_TYPE == KEYMATRIX_TYPE_MOKIBO)
static const uint8_t m_keymatrix_lookup[KEYMATRIX_NUM_OF_COLUMNS][KEYMATRIX_NUM_OF_ROWS] =
    {
        /*         R0          R1          R2          R3          R4          R5          R6          R7 */
        /* C00 */ {KEY_NONE, KEY_NONE, KEY_UP, KEY_2, KEY_3, KEY_BS, KEY_F9, KEY_F10},
        /* C01 */ {KEY_LEFT, KEY_DOWN, KEY_RIGHT, KEY_NONE, KEY_NONE, KEY_NONE, KEY_DELETE, KEY_NONE},
        /* C02 */ {KEY_F7, KEY_1, KEY_F3, KEY_F4, KEY_Q, KEY_F8, KEY_F12, KEY_F11},
        /* C03 */ {KEY_F6, KEY_CAPS, KEY_F2, KEY_F1, KEY_TAB, KEY_F5, KEY_TILDE, KEY_ESC},
        /* C04 */ {KEY_NONE, KEY_NONE, KEY_DOT, KEY_SLASH, KEY_COLON, KEY_QUOTE, KEY_P, KEY_O_BRCKT},
        /* C05 */ {KEY_L_CTRL, KEY_NONE, KEY_R_FUNC, KEY_NONE, KEY_NONE, KEY_NONE, KEY_NONE, KEY_NONE},
        /* C06 */ {KEY_NONE, KEY_NONE, KEY_L_SHFT, KEY_R_SHFT, KEY_NONE, KEY_NONE, KEY_NONE, KEY_NONE},
        /* C07 */ {KEY_R_ALT, KEY_NONE, KEY_NONE, KEY_NONE, KEY_ENTER, KEY_C_BRCKT, KEY_PLUS, KEY_NONE},
        /* C08 */ {KEY_M, KEY_COMMA, KEY_K, KEY_L, KEY_I, KEY_O, KEY_0, KEY_U_SCORE},
        /* C09 */ {KEY_BSLASH, KEY_B, KEY_N, KEY_J, KEY_Y, KEY_U, KEY_8, KEY_9},
        /* C10 */ {KEY_SB, KEY_C, KEY_V, KEY_G, KEY_H, KEY_T, KEY_6, KEY_7},
        /* C11 */ {KEY_NONE, KEY_X, KEY_D, KEY_F, KEY_E, KEY_R, KEY_4, KEY_5},
        /* C12 */ {KEY_L_UI, KEY_R_CTRL, KEY_NONE, KEY_NONE, KEY_NONE, KEY_NONE, KEY_NONE, KEY_NONE},
        /* C13 */ {KEY_L_ALT, KEY_NONE, KEY_NONE, KEY_NONE, KEY_NONE, KEY_NONE, KEY_NONE, KEY_NONE},
        /* C14 */ {KEY_L_FUNC, KEY_NONE, KEY_Z, KEY_A, KEY_S, KEY_W, KEY_NONE, KEY_NONE},
};
#elif (KEYMATRIX_TYPE == KEYMATRIX_TYPE_MOKIBO)
static const uint8_t m_keymatrix_lookup[KEYMATRIX_NUM_OF_COLUMNS][KEYMATRIX_NUM_OF_ROWS] =
    {
        /*         R0          R1          R2          R3          R4          R5          R6          R7 */
        /* C00 */ {KEY_NONE, KEY_NONE, KEY_L_CTRL, KEY_NONE, KEY_R_CTRL, KEY_NONE, KEY_NONE, KEY_NONE},
        /* C01 */ {KEY_CAPS, KEY_ESC, KEY_TAB, KEY_TILDE, KEY_A, KEY_Z, KEY_1, KEY_Q},
        /* C02 */ {KEY_F1, KEY_F4, KEY_F3, KEY_F2, KEY_D, KEY_C, KEY_3, KEY_E},
        /* C03 */ {KEY_NONE, KEY_L_UI, KEY_NONE, KEY_NONE, KEY_NONE, KEY_NONE, KEY_NONE, KEY_NONE},
        /* C04 */ {KEY_B, KEY_G, KEY_T, KEY_5, KEY_F, KEY_V, KEY_4, KEY_R},
        /* C05 */ {KEY_F8, KEY_F7, KEY_F6, KEY_F5, KEY_S, KEY_X, KEY_2, KEY_W},
        /* C06 */ {KEY_NONE, KEY_NONE, KEY_C_BRCKT, KEY_NONE, KEY_K, KEY_COMMA, KEY_8, KEY_I},
        /* C07 */ {KEY_NONE, KEY_NONE, KEY_NONE, KEY_L_FUNC, KEY_NONE, KEY_NONE, KEY_NONE, KEY_NONE},
        /* C08 */ {KEY_N, KEY_H, KEY_Y, KEY_6, KEY_J, KEY_M, KEY_7, KEY_U},
        /* C09 */ {KEY_NONE, KEY_NONE, KEY_NONE, KEY_NONE, KEY_NONE, KEY_L_SHFT, KEY_NONE, KEY_R_SHFT},
        /* C10 */ {KEY_PLUS, KEY_QUOTE, KEY_O_BRCKT, KEY_U_SCORE, KEY_COLON, KEY_SLASH, KEY_0, KEY_P},
        /* C11 */ {KEY_F12, KEY_F11, KEY_F10, KEY_F9, KEY_L, KEY_DOT, KEY_9, KEY_0},
        /* C12 */ {KEY_R_ALT, KEY_R_CTRL, KEY_NONE, KEY_NONE, KEY_NONE, KEY_NONE, KEY_L_ALT, KEY_NONE},
        /* C13 */ {KEY_PRTSC, KEY_BS, KEY_NUM, KEY_BSLASH, KEY_ENTER, KEY_SB, KEY_DOWN, KEY_UP},
        /* C14 */ {KEY_DELETE, KEY_NONE, KEY_HOME, KEY_PGUP, KEY_PGDN, KEY_END, KEY_RIGHT, KEY_LEFT},
};
#else
#error "You have to define MKB_KEYMATRIX_TYPE in mkb_config.h"
#endif

#if (KEYMATRIX_MODE == KEYMATRIX_MODE_GPIOTE)
static app_gpiote_user_id_t m_keymatrix_gpiote; //!< GPIOTE Handle.
static uint8_t m_idle_scan_counter;             //!< Idle Scan Counter.

/**@brief Enable interrupt-based key press sensing. */
#define BUTTONS_ACTIVE_STATE 1
static void keymatrix_wakeup(bool enable)
{
    uint8_t row;

    for (row = 0; row < KEYMATRIX_NUM_OF_ROWS; row++)
    {
        nrf_gpio_pin_sense_t sense = enable ? (BUTTONS_ACTIVE_STATE ? NRF_GPIO_PIN_SENSE_HIGH : NRF_GPIO_PIN_SENSE_LOW) : NRF_GPIO_PIN_NOSENSE;
        nrf_gpio_cfg_sense_set(m_row_to_pin_map[row], NRF_GPIO_PIN_SENSE_HIGH);

        MKB_LOG_INFO("WAKEUP PIN: %d", m_row_to_pin_map[row]);
    }
}

/**@brief Enable interrupt-based key press sensing. */
static ret_code_t keymatrix_sense_enable(void)
{
    unsigned int column;

    for (column = 0; column < KEYMATRIX_NUM_OF_COLUMNS; column++)
    {
        uint8_t pin = m_column_to_pin_map[column];

        // Change to output
        nrf_gpio_cfg_output(pin);

        // Drive "1" on the selected column.
        nrf_gpio_pin_set(pin);
    }

    // Enable GPIOTE sensing.
    return app_gpiote_user_enable(m_keymatrix_gpiote);
}

/**@brief Disable interrupt-based key press sensing. */
static ret_code_t keymatrix_sense_disable(void)
{
    unsigned int column;
    ret_code_t status;

    // Disable GPIOTE sensing.
    status = app_gpiote_user_disable(m_keymatrix_gpiote);
    if (status != NRF_SUCCESS)
    {
        return status;
    }

    for (column = 0; column < KEYMATRIX_NUM_OF_COLUMNS; column++)
    {
        uint8_t pin = m_column_to_pin_map[column];

        // Drive "0" on the selected column.
        nrf_gpio_pin_clear(pin);

        // Change to input and disconnect
        nrf_gpio_cfg_default(pin);
    }

    return NRF_SUCCESS;
}
#endif /* (KEYMATRIX_MODE == KEYMATRIX_MODE_GPIOTE) */

static keymatrix_read_handler_t m_keymatrix_read_handler;

static void keymatrix_init_gpio(void)
{
    // Configure Columns.
    for (uint8_t i = 0; i < KEYMATRIX_NUM_OF_COLUMNS; i++)
    {
        nrf_gpio_cfg_output(m_column_to_pin_map[i]);

        // Set pin to low
        nrf_gpio_pin_clear(m_column_to_pin_map[i]);
    }

    // Configure Rows.
    for (uint8_t i = 0; i < KEYMATRIX_NUM_OF_ROWS; i++)
    {
        nrf_gpio_cfg_input(m_row_to_pin_map[i], NRF_GPIO_PIN_PULLDOWN);
    }
}

/**@brief Extract rows state form gpio lines state. */
static uint8_t keymatrix_get_rows_state(uint32_t *p_gpio_state)
{
    uint8_t rows_state = 0, gpio_state;
    unsigned int i, j;

    for (i = 0; i < KEYMATRIX_NUM_OF_ROWS; i++)
    {
        uint8_t port = (m_row_to_pin_map[i] < P0_PIN_NUM) ? 0 : 1;
        uint8_t pin = (m_row_to_pin_map[i] % P0_PIN_NUM);

        gpio_state = p_gpio_state[port];

        rows_state |= ((gpio_state >> pin) & 1) << i;
    }

    return rows_state;
}

static void keymatrix_scan(bool cb_flag)
{
    ret_code_t err_code;

    uint8_t blocking_mask, rows_state[KEYMATRIX_NUM_OF_COLUMNS], detected_keypresses_on_column;
    unsigned int column, row;

    // Perform scan.
    for (column = 0; column < KEYMATRIX_NUM_OF_COLUMNS; column++)
    {
        uint8_t pin = m_column_to_pin_map[column];
        uint32_t gpio_state = 0;

        // Change to output
        nrf_gpio_cfg_output(pin);

        // Drive "1" on the selected column.
        nrf_gpio_pin_set(pin);

        // Wait for column signal propagation.
        nrf_delay_us(KEYMATRIX_SELECT_TIME);

        // Read GPIOs state.
        for (row = 0; row < KEYMATRIX_NUM_OF_ROWS; row++)
        {
            if (nrf_gpio_pin_read(m_row_to_pin_map[row]))
            {
                gpio_state |= (1ul << row);
            }
        }

        // Drive "0" on the selected column.
        nrf_gpio_pin_clear(pin);

        // Change to input and disconnect
        nrf_gpio_cfg_default(pin);

        // Save rows state for given column.
        rows_state[column] = gpio_state;
    }

    // Process results.
    memset(m_keys_pressed, 0, KEYMATRIX_MAX_KEYS);

    blocking_mask = 0;
    m_keys_pressed_size = 0;
    m_keys_blocked = false;

    for (column = 0; column < KEYMATRIX_NUM_OF_COLUMNS; column++)
    {
        if (rows_state[column] == 0)
        {
            continue;
        }

        detected_keypresses_on_column = 0;

        for (row = 0; row < KEYMATRIX_NUM_OF_ROWS; row++)
        {
            if ((rows_state[column] & (1ul << row)) != 0)
            {
                detected_keypresses_on_column += 1;

                if (m_keys_pressed_size < KEYMATRIX_MAX_KEYS)
                {
                    m_keys_pressed[m_keys_pressed_size++] = KEYMATRIX_KEY_ID(row, column);

                    MKB_LOG_DEBUG("Key Row: %d, Column: %d", row, column);
                }
            }
        }

        if (((blocking_mask & rows_state[column]) != 0) && (detected_keypresses_on_column > 1))
        {
            // Blocking/ghosting occurs when three or more keys on two rows and columns are pressed at the same time
            m_keys_blocked = true;
            break;
        }

        blocking_mask |= rows_state[column];
    }

    // Callback
    if (cb_flag && m_keymatrix_read_handler)
        m_keymatrix_read_handler(m_keys_pressed, m_keys_pressed_size, m_keys_blocked);
}

static void keymatrix_scan_handler(void *p_event_data, uint16_t event_size)
{
    ret_code_t err_code;

    UNUSED_PARAMETER(p_event_data);
    UNUSED_PARAMETER(event_size);

    keymatrix_scan(true);

#if (KEYMATRIX_MODE == KEYMATRIX_MODE_GPIOTE)
    uint8_t num_of_pressed_keys;

    num_of_pressed_keys = m_keys_pressed_size;

    if (num_of_pressed_keys)
    {
        // Reset idle scan counter if at least one key is pressed.
        m_idle_scan_counter = 0;
    }
    else
    {
        // Count number of idle scans
        m_idle_scan_counter += 1;
    }

    if (m_keymatrix_enabled)
    {
        if (m_idle_scan_counter < KEYMATRIX_IDLE_SCAN_CNT_MAX)
        {
            // Keep actively polling keyboard if it is in use.
            MKB_LOG_DEBUG("Timer Enabled");
            err_code = app_timer_start(m_keymatrix_timer, APP_TIMER_TICKS(KEYMATRIX_TIMER_INTERVAL), NULL);
            if (err_code != NRF_SUCCESS)
            {
                MKB_ERROR_CHECK(err_code);
            }
        }
        else
        {
            // Fall back to sensing if keyboard is idle.
            MKB_LOG_DEBUG("Sense Enabled");
            err_code = keymatrix_sense_enable();
            if (err_code != NRF_SUCCESS)
            {
                MKB_ERROR_CHECK(err_code);
            }
        }
    }
#elif (KEYMATRIX_MODE == KEYMATRIX_MODE_TIMER)
    //MKB_LOG_DEBUG("Timer Enabled");
    err_code = app_timer_start(m_keymatrix_timer, APP_TIMER_TICKS(KEYMATRIX_TIMER_INTERVAL), NULL);
    if (err_code != NRF_SUCCESS)
    {
        MKB_ERROR_CHECK(err_code);
    }
#endif /* (KEYMATRIX_MODE == CYPRESS_MODE_GPIOTE) */
}

#if (KEYMATRIX_MODE == KEYMATRIX_MODE_GPIOTE)
/**@brief GPIOTE PORT interrupt handler.*/
static void keymatrix_interrupt_handler(uint32_t const *p_event_pins_low_to_high,
                                        uint32_t const *p_event_pins_high_to_low)
{
    if (m_keymatrix_enabled)
    {
#if defined(BOARD_PCA10056) || defined(BOARD_MKB10056)
        MKB_LOG_DEBUG("PINS[0: 0x%04x, 1: 0x%04x]", p_event_pins_low_to_high[0], p_event_pins_low_to_high[1]);
        MKB_LOG_DEBUG("MASK[0: 0x%04x, 1: 0x%04x]", KEYMATRIX_MASK_ROW_A, KEYMATRIX_MASK_ROW_B);
        if ((p_event_pins_low_to_high[0] & KEYMATRIX_MASK_ROW_A) || (p_event_pins_low_to_high[1] & KEYMATRIX_MASK_ROW_B))
#else
        if (*p_event_pins_low_to_high & KEYMATRIX_MASK_ROW_ALL)
#endif
        {
            // Disable sensing.
            keymatrix_sense_disable();

            // Reset idle scan counter.
            m_idle_scan_counter = 0;

            // Schedule cypress_scan
            ret_code_t ret_code = app_sched_event_put(NULL, 0, keymatrix_scan_handler);
            MKB_ERROR_CHECK(ret_code);
        }
    }
}
#endif /* (KEYMATRIX_MODE == KEYMATRIX_MODE_GPIOTE) */

static void keymatrix_timeout_handler(void *p_context)
{
    UNUSED_PARAMETER(p_context);

    if (m_keymatrix_enabled)
    {
        ret_code_t ret_code = app_sched_event_put(NULL, 0, keymatrix_scan_handler);
        MKB_ERROR_CHECK(ret_code);
    }
}

ret_code_t drv_keymatrix_init(keymatrix_read_handler_t read_handler)
{
    ret_code_t err_code;

    if (!read_handler)
    {
        return NRF_ERROR_INVALID_PARAM;
    }

    m_keymatrix_read_handler = read_handler;

    m_keymatrix_enabled = false;

#if (KEYMATRIX_MODE == KEYMATRIX_MODE_GPIOTE)

#if defined(BOARD_PCA10056) || defined(BOARD_MKB10056)
    uint32_t low_to_high_mask[GPIO_COUNT] = {KEYMATRIX_MASK_ROW_A, KEYMATRIX_MASK_ROW_B};
    uint32_t high_to_low_mask[GPIO_COUNT] = {0, 0};

    MKB_LOG_DEBUG("BIT Mask: 0: 0x%04x, 1: 0x%04x", KEYMATRIX_MASK_ROW_A, KEYMATRIX_MASK_ROW_B);
#else
    uint32_t low_to_high_mask[GPIO_COUNT] = {KEYMATRIX_MASK_ROW_ALL};
    uint32_t high_to_low_mask[GPIO_COUNT] = {0};

    MKB_LOG_DEBUG("BIT Mask: 0x%04x", KEYMATRIX_MASK_ROW_ALL);
#endif

    err_code = app_gpiote_user_register(&m_keymatrix_gpiote,
                                        low_to_high_mask,
                                        high_to_low_mask,
                                        keymatrix_interrupt_handler);
    if (err_code != NRF_SUCCESS)
    {
        MKB_ERROR_CHECK(err_code);
        return err_code;
    }

    err_code = keymatrix_sense_disable();
    if (err_code != NRF_SUCCESS)
    {
        MKB_ERROR_CHECK(err_code);
        return err_code;
    }
#endif /* (KEYMATRIX_MODE == KEYMATRIX_MODE_GPIOTE) */

    err_code = app_timer_create(&m_keymatrix_timer, APP_TIMER_MODE_SINGLE_SHOT, keymatrix_timeout_handler);
    //err_code = app_timer_create(&m_keymatrix_timer, APP_TIMER_MODE_REPEATED, keymatrix_timeout_handler);
    if (err_code != NRF_SUCCESS)
    {
        MKB_ERROR_CHECK(err_code);
        return err_code;
    }

    keymatrix_init_gpio();

    return NRF_SUCCESS;
}

ret_code_t drv_keymatrix_keys_get(uint8_t *p_pressed_keys,
                                  uint8_t *p_number_of_pressed_keys,
                                  bool *p_keys_blocked)
{
    if (m_keymatrix_enabled)
    {
        return NRF_ERROR_INVALID_STATE;
    }

    keymatrix_scan(false);

    memcpy(p_pressed_keys, m_keys_pressed, sizeof(m_keys_pressed));
    *p_number_of_pressed_keys = m_keys_pressed_size;
    *p_keys_blocked = m_keys_blocked;

    return NRF_SUCCESS;
}

ret_code_t drv_keymatrix_enable(void)
{
    ret_code_t err_code;

    if (!m_keymatrix_enabled)
    {
        m_keymatrix_enabled = true;

#if (KEYMATRIX_MODE == KEYMATRIX_MODE_GPIOTE)
        err_code = keymatrix_sense_enable();
#elif (KEYMATRIX_MODE == KEYMATRIX_MODE_TIMER)
        err_code = app_timer_start(m_keymatrix_timer, APP_TIMER_TICKS(KEYMATRIX_TIMER_INTERVAL), NULL);
#endif /* (KEYMATRIX_MODE == KEYMATRIX_MODE_GPIOTE) */
        if (err_code != NRF_SUCCESS)
        {
            MKB_ERROR_CHECK(err_code);
            return err_code;
        }
    }

    return NRF_SUCCESS;
}

ret_code_t drv_keymatrix_disable(void)
{
    ret_code_t err_code;

    if (m_keymatrix_enabled)
    {
        m_keymatrix_enabled = false;

#if (KEYMATRIX_MODE == KEYMATRIX_MODE_GPIOTE)
        err_code = keymatrix_sense_disable();
#elif (KEYMATRIX_MODE == KEYMATRIX_MODE_TIMER)
        err_code = app_timer_stop(m_keymatrix_timer);
#endif /* (KEYMATRIX_MODE == KEYMATRIX_MODE_GPIOTE) */
        if (err_code != NRF_SUCCESS)
        {
            MKB_ERROR_CHECK(err_code);
            return err_code;
        }
    }

    return NRF_SUCCESS;
}

#if (defined(NRF_PWR_MGMT_ENABLED) && NRF_PWR_MGMT_ENABLED)
static bool drv_keymatrix_shutdown(nrf_pwr_mgmt_evt_t event)
{
    bool wakeup;

    wakeup = (event == NRF_PWR_MGMT_EVT_PREPARE_WAKEUP) ? true : false;

    if (wakeup)
    {
        MKB_LOG_INFO("Sense Enabled");

        drv_keymatrix_disable();
        //keymatrix_wakeup(true);
    }
    else
    {
        MKB_LOG_INFO("Sense Disabled");

        drv_keymatrix_disable();
        //keymatrix_wakeup(false);
    }

    return true;
}

NRF_PWR_MGMT_HANDLER_REGISTER(drv_keymatrix_shutdown, SHUTDOWN_PRIORITY_DEFAULT);
#endif /* NRF_PWR_MGMT_ENABLED */

///////////////////////////////////////////////////////////////////////////////

#endif // (defined(MKB_KEYMATRIX_ENABLED) && MKB_KEYMATRIX_ENABLED)
