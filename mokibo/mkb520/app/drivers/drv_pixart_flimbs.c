/*
 * ===============================================================================
 * Copyright (C) 2017-2019
 * INNOPRESSO INC. All Rights Reserved
 *
 * The content of this file is copyrighted by INNOPRESSO or its
 * affiliated, associated or related companies. All copyrights and other
 * associated intellectual property rights related thereto are expressly
 * reserved by INNOPRESSO INC.
 * ===============================================================================
 */

/** @file
 *
 * @defgroup ble_app_hids_mkb316_driver drv_pixart.c
 * @{
 * @ingroup ble_app_hids_mkb316
 * @brief HID Touchpad Keyboard Application pixart driver file.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "sdk_common.h"
#include "app_error.h"
#include "app_timer.h"
#include "app_scheduler.h"
#if defined(GPIOTE_ENABLED) && GPIOTE_ENABLED
#include "app_gpiote.h"
#endif

#include "nrf_atomic.h"
#include "nrf_assert.h"
#include "nrf_error.h"
#include "nrf_gpio.h"
#include "nrf_log.h"
#include "nrf_delay.h"
#include "nrf_pwr_mgmt.h"

#define MKB_LOG_LEVEL MKB_PIXART_LOG_LEVEL
#include "mkb_common.h"
#include "drv_twi.h"
#include "drv_pixart.h"
#include "math.h"

#if (defined(MKB_WDT_ENABLED) && MKB_WDT_ENABLED)
#include "mkb_wdt.h"
#endif

#include "mkb_led.h"
#include "mkb_state_manager.h"

#include "drv_pixart_dfu_mkb420_0811_v140A.h"

// TEST

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
/*
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "sdk_common.h"
#include "sdk_errors.h"

#include "nrf_atomic.h"
#include "nrf_assert.h"
#include "nrf_error.h"
#include "nrf_gpio.h"
#include "nrf_log.h"
#include "nrf_delay.h"
#include "nrf_pwr_mgmt.h"
#include "nrf_twi_mngr.h"

#include "app_error.h"
#include "app_timer.h"
#include "app_scheduler.h"
#include "app_gpiote.h"

#include "mkb_common.h"
#include "mkb_wdt.h"
#include "math.h"
#include "drv_pixart.h"

#define MKB_LOG_LEVEL MKB_PIXART_LOG_LEVEL
*/

APP_TIMER_DEF(m_pixart_intrpt_timer);
static app_gpiote_user_id_t m_pixart_gpiote;
static nrf_atomic_flag_t m_pixart_atomic_flag;

typedef struct
{
    bool is_pixart_init;
    bool is_pixart_enabled;
    bool is_intrpt_sense_enabled;
    uint8_t pct_rdbuff[256];
    uint8_t pct_rwbuff[256];
} drv_pixart_this_t;

static drv_pixart_this_t m_this;

// pixart basic twi

// trnsmit
static ret_code_t twi_com_get_bootready();
static ret_code_t twi_com_get_status();
static ret_code_t twi_com_get_schedule(); // p_data 안쓰고있음.
static ret_code_t twi_com_get_report();
static ret_code_t twi_com_get_prjver();
static ret_code_t twi_com_set_suspend();
static ret_code_t twi_com_set_resume();
static ret_code_t twi_com_set_softreset();
static ret_code_t twi_com_set_aamode();
static ret_code_t twi_com_set_pointer_kttt();
static ret_code_t twi_com_set_gesture_kttt();
static ret_code_t twi_com_set_ack();

// pxiart_dfu
static void twi_dfu_exit_engineering_mode();
static void twi_dfu_enter_engineering_mode();
static void twi_dfu_flash_execute();
static void twi_dfu_flash_write_enable();
static void twi_dfu_erase_flash_sector();
static void twi_dfu_wait_flash_busy();
static void twi_dfu_write_256byte_data_to_sram();
static void twi_dfu_program_256byte_sram_data_to_flash();
static void twi_dfu_flash_update_process();
static void twi_dfu_update_flash();
static void twi_dfu_read_CRC();

// handler
static void intrpt_handler();                                  // GPIOTE 가 들어오면 호출됨 intrpt_process  호출
static void intrpt_timer_handler();                            //타이머가 되면 호출됨 intrpt_process 호출
static void intrpt_process();                                  //들어온 신호를 읽어와 버퍼에 저장
static void read_packet(ret_code_t result, void *p_user_data); // read_keys_handler 와 read_touch_handler를 호출한다
static void read_keys_handler();
static void read_touch_handler();

// system funcs
void drv_pixart_init();
ret_code_t drv_pixart_key_handler_register();
ret_code_t mkb_pixart_cli();
static bool mkb_pixart_shutdown();

//
static uint8_t pixart_twi_rx(uint8_t addr)
{
    ret_code_t rv;
    uint8_t data = 0;

    uint8_t tx_buff_addr[] = {addr};
    nrf_twi_mngr_transfer_t const transfer[] = {
        NRF_TWI_MNGR_WRITE(PIXART_ADDR, tx_buff_addr, 1, 1),
        NRF_TWI_MNGR_READ(PIXART_ADDR, &data, 1, 0),
    };

    rv = twi_perform(NULL, transfer, sizeof(transfer) / sizeof(nrf_twi_mngr_transfer_t));
    if (NRF_SUCCESS != rv)
    {
        MKB_LOG_ERROR("###Error");
        return (NRF_ERROR_INTERNAL);
    }

    return data;
}

static void pixart_twi_tx(uint8_t addr, uint8_t data)
{
    ret_code_t rv;
    uint8_t tx_buff_addr_data[] = {addr, data};
    nrf_twi_mngr_transfer_t const transfer[] = {
        NRF_TWI_MNGR_WRITE(PIXART_ADDR, tx_buff_addr_data, 2, 1),
    };

    rv = twi_perform(NULL, transfer, sizeof(transfer) / sizeof(nrf_twi_mngr_transfer_t));
    if (NRF_SUCCESS != rv)
    {
        MKB_LOG_ERROR("###Error");
    }
}

static uint8_t pixart_twi_rx_usr(uint8_t bank, uint8_t reg)
{
    ret_code_t rv;
    uint8_t result = 0;
    uint8_t tx_buff_usrbnk[] = {0x7F, 0x06};
    uint8_t tx_buff_indbnk[] = {0x73, bank};
    uint8_t tx_buff_indreg[] = {0x74, reg};
    uint8_t tx_buff_indval[] = {0x75};
    nrf_twi_mngr_transfer_t const transfer[] = {
        NRF_TWI_MNGR_WRITE(PIXART_ADDR, tx_buff_usrbnk, 2, 1),
        NRF_TWI_MNGR_WRITE(PIXART_ADDR, tx_buff_indbnk, 2, 1),
        NRF_TWI_MNGR_WRITE(PIXART_ADDR, tx_buff_indreg, 2, 1),
        NRF_TWI_MNGR_WRITE(PIXART_ADDR, tx_buff_indval, 1, 0),
        NRF_TWI_MNGR_READ(PIXART_ADDR, &result, 1, 0),
    };

    rv = twi_perform(NULL, transfer, sizeof(transfer) / sizeof(nrf_twi_mngr_transfer_t));
    if (NRF_SUCCESS != rv)
    {
        MKB_LOG_ERROR("###Error");
        return 0;
    }
    return result;
}

ret_code_t uint8_t pixart_twi_tx_usr(uint8_t bank, uint8_t reg, uint8_t data)
{
    ret_code_t rv;
    uint8_t tx_buff_usrbnk[] = {0x7F, 0x06};
    uint8_t tx_buff_indbnk[] = {0x73, bank};
    uint8_t tx_buff_indreg[] = {0x74, reg};
    uint8_t tx_buff_indval[] = {0x75, data};
    nrf_twi_mngr_transfer_t const test[] = {
        NRF_TWI_MNGR_WRITE(PIXART_ADDR, tx_buff_usrbnk, 2, 1),
        NRF_TWI_MNGR_WRITE(PIXART_ADDR, tx_buff_indbnk, 2, 1),
        NRF_TWI_MNGR_WRITE(PIXART_ADDR, tx_buff_indreg, 2, 1),
        NRF_TWI_MNGR_WRITE(PIXART_ADDR, tx_buff_indval, 2, 0),
    };

    rv = twi_perform(NULL, test, sizeof(test) / sizeof(nrf_twi_mngr_transfer_t));
    if (NRF_SUCCESS != rv)
    {
        MKB_LOG_ERROR("###Error");
        return (NRF_ERROR_INTERNAL);
    }
}
