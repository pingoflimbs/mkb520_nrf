/*
 * ===============================================================================
 * Copyright (C) 2017-2019
 * INNOPRESSO INC. All Rights Reserved
 *
 * The content of this file is copyrighted by INNOPRESSO or its
 * affiliated, associated or related companies. All copyrights and other
 * associated intellectual property rights related thereto are expressly
 * reserved by INNOPRESSO INC.
 * ===============================================================================
 */

#ifndef __DRV_PIXART_H__
#define __DRV_PIXART_H__

#include "nrf_twi_mngr.h"

#ifdef __cplusplus
extern "C"
{
#endif

#pragma pack(push, 1) // 1바이트 크기로 정렬

// 0x66 is the DRV_PIXART's address, it contains
// R/W bit and "nrf_drv_twi" (and consequently "nrf_twi_mngr") requires slave
// address without this bit, hence shifting.
#define PIXART_ADDR 0x33
#define PIXART_POWER_PIN MKB_PIXART_POWER_PIN
#define PIXART_RESET_PIN MKB_PIXART_RESET_PIN
#define PIXART_INT_PIN MKB_PIXART_INT_PIN
#define PIXART_TIMER_INTERVAL MKB_PIXART_SCAN_INTERVAL
#define PIXART_INT_TIMER_INTERVAL MKB_PIXART_INT_SCAN_INTERVAL
#define PIXART_WAIT_TIMER_INTERVAL MKB_PIXART_WAIT_INTERVAL

#define PIXART_INT_IDLE_CNT 0 // 50 // idle wait time(ms) = PIXART_INT_IDLE_CNT * (PIXART_INT_TIMER_INTERVAL)

#define PIXART_GPIOTE_MASK (1 << PIXART_INT_PIN)

#define PIXART_MODE_TIMER 1
#define PIXART_MODE_GPIOTE 2

#define PIXART_MODE MKB_PIXART_MODE

#define PIXART_INFO_MAX_COUNT 10
#define PIXART_RECORD_HEADER 3
#define PIXART_RECORD_LENGTH 6
#define PIXART_RECORD_TAIL 4

#define PIXART_DATA_TOUCH_CNT_LOC 8
#define PIXART_DATA_TOUCH_FINGER_PACKET_LEN 9

#define PIXART_DATA_KEYBOARD_PACKET_LEN 14
#define PIXART_DATA_KEYBOARD_MODIFIER_LOC 13
#define PIXART_DATA_IS_KEYBOARD_FLAG_LOC 0
#define PIXART_DATA_IS_KEYBOARD_FLAG 0x43

#define PIXART_DATA_REQ_LEN (PIXART_RECORD_HEADER + (PIXART_RECORD_LENGTH * PIXART_INFO_MAX_COUNT) + PIXART_RECORD_TAIL)
#define PIXART_DATA_ONE_LEN (PIXART_RECORD_HEADER + PIXART_RECORD_LENGTH + PIXART_RECORD_TAIL)

#define PIXART_BUFFER_SIZE 256

#define PIXART_PIN_LOW 0
#define PIXART_PIN_HIGH 1

//	--- PIP(Packet Interface Protocol) Registers ---
#define PIXART_REG_BOOT_READY 0x70 //
#define PIXART_REG_STATUS 0x71	   //
#define PIXART_REG_COMMAND 0x7A	   //
#define PIXART_REG_ENABLE 0x7B	   //
#define PIXART_REG_DISABLE 0x7C	   //

	//----------------------------------------------------//pixart_packet_t
	typedef struct pixart_len
	{
		uint16_t len;
	} pixart_len_t;

	typedef enum
	{
		PIXART_AAMODE_TYPE_NONE = 0x00,
		PIXART_AAMODE_TYPE_RIGHT = 0x01,
		PIXART_AAMODE_TYPE_LEFT = 0x02,
		PIXART_AAMODE_TYPE_ALL = 0x03,
	} pixart_aamode_type_t;

	typedef enum
	{
		PIXART_GSTR_TYPE_NONE = 0x00,
		PIXART_GSTR_TYPE_ONE_MOVE = 0x01, // xy move

		PIXART_GSTR_TYPE_ONE_TAP_AND_DRAG = 0x0B, // xy move
		PIXART_GSTR_TYPE_ONE_TAP_AND_DRAG_RELEASE = 0x0C,

		PIXART_GSTR_TYPE_TWO_TAP = 0x03, //떨어질때 호출됨
		PIXART_GSTR_TYPE_TWO_TAP_RELEASE = 0x03C,

		PIXART_GSTR_TYPE_TWO_VSCRL = 0x07, // x:highres scrolll y:traditional scroll z:first is 1
		PIXART_GSTR_TYPE_TWO_HSCRL = 0x08, // x:highres scrolll y:traditional scroll z:first is 1
		PIXART_GSTR_TYPE_TWO_PINCH = 0x09, // x:highres scrolll y:traditional scroll z:first is 1
		PIXART_GSTR_TYPE_TWO_PINCH_RELEASE = 0x0A,

		PIXART_GSTR_TYPE_THREE_UP = 0x11,
		PIXART_GSTR_TYPE_THREE_DOWN = 0x12,
		PIXART_GSTR_TYPE_THREE_LEFT = 0x13,
		PIXART_GSTR_TYPE_THREE_RIGHT = 0x14,
		PIXART_GSTR_TYPE_THREE_MOVE = 0x41, // xy move, z:first is 1

		PIXART_GSTR_TYPE_THREE_RELEASE = 0x42,
		PIXART_GSTR_TYPE_THREE_TAP = 0x1E,
		PIXART_GSTR_TYPE_THREE_TAP_RELEASE = 0x3D,

		PIXART_GSTR_TYPE_FOUR_UP = 0x15,
		PIXART_GSTR_TYPE_FOUR_DOWN = 0x16,
		PIXART_GSTR_TYPE_FOUR_LEFT = 0x17,
		PIXART_GSTR_TYPE_FOUR_RIGHT = 0x18,

		PIXART_GSTR_TYPE_FOUR_TAP = 0x3F,
		PIXART_GSTR_TYPE_FOUR_TAP_RELEASE = 0x40,
	} pixart_gstr_type_t;

#define PIXART_MAX_KEYS 6

	/*
	   0 1  2  3  4  5  6  7  8  9 10 11 12 13 14 15
	typ k1 k2 k3 k4 k5 k6 r1 r2 r3 r4 r5 r6 r7 mod ?
[15] 43|00|00|00|00|00|00|00|01|f6|6b|00|11|04|00|00|
[15] 43|17|1c|00|00|00|00|00|01|e5|2d|04|13|01|00|00|
[15] 43|00|00|00|00|00|00|00|00|00|00|00|00|00|20|00|
[15] 43|00|00|00|00|00|00|00|00|00|00|00|00|00|04|00|

//RED : 247(0xF7)
//MOD : 248(0xF8)
//Fn  : 249(0xF9)
*/

	typedef struct
	{
		uint8_t type;				  // 0x43
		uint8_t key[PIXART_MAX_KEYS]; // key buffer
		uint8_t reserved[7];		  // reserved
		uint8_t modkey;				  // [1<<0]:LCTRL [1<<1]LSHIFT [1<<2]LALT [1<<3]LWIN [1<<4]RCTRL [1<<5]RSHIFT [1<<6]RALT [1<<7]RWIN
	} pixart_keyboard_packet_t;		  /* 7Bytes */

	/*
<PIXART TOUCH PROTOCOL>
	ID------------- cfd tip reservd
obj:[0] [0] [0] [0] [0] [0] [0] [0]

[32] 00|01|02|03|04|05|06|07|08|09|10|11|12|13|14|15|16|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|
	gst x---- y---- z----btncnt
[10]|0c|00|00|00|00|00|00|00|00|00|
								objx---- y---- z---- area-
[21]|00|00|00|00|00|00|00|00|01|f2|3c|04|55|01|00|11|16|00|00|00|00|
	gst x---- y---- z----btncnt objx---- y---- z---- a---- objx---- y----z----a----
[32]|08|9f|00|02|00|00|00|00|02|53|9c|05|74|01|00|09|0d|00|f4|dc|04|46|02|28|13|1b|00|00|00|00|00|00|
*/

	typedef struct
	{
		pixart_gstr_type_t type; /* 1byte */
		uint16_t x;				 /* 2bytes */
		uint16_t y;				 /* 2bytes */
		uint16_t z;				 /* 2bytes */
	} pixart_gesture_packet_t;	 /* 7Bytes */

	/*
<PIXART KEYBOARD PROTOCOL>
	ID------------- cfd tip reservd
obj:[0] [0] [0] [0] [0] [0] [0] [0]
*/
	typedef struct
	{
		uint8_t touchID : 4;	// Object ID
		uint8_t confidence : 1; // normal touch(1), abnormal like palm(0)
		uint8_t tip : 1;		// touch on surface(1), touch released(0)
		uint8_t res : 2;		// reserved
		uint16_t x;				// 16bit x-coordinates in pixels
		uint16_t y;				// 16bit y-coordinates in pixels
		uint16_t z;				// 16bit z-coordinates in pixels
		uint16_t area;			// 16bit y-coordinates in pixels
	} pixart_finger_packet_t;

	typedef struct
	{
		pixart_gesture_packet_t gesture;					   // Gst:1byte + X:2byte + Y:2byte + Z:2byte (7byte)
		uint8_t button;										   // Btn:1byte
		uint8_t touch_count;								   // Cnt:1byte
		pixart_finger_packet_t fingers[PIXART_INFO_MAX_COUNT]; //(ID:4bit + Confidce:1bit + Tip:1bit + Res:2bit)1byte + X:2byte + Y:2byte + Z:2byte + Area:2byte (9byte)
	} pixart_touch_packet_t;

	typedef union
	{
		uint8_t buf[PIXART_BUFFER_SIZE];
		pixart_keyboard_packet_t keyboard; // Type:1byte + Key:6byte (7byte)
		pixart_touch_packet_t touch;	   // 9byte + N*9byte (9+9*Nbyte)
	} pixart_packet_t;

	//----------------------------------------------------//mokibo_touch_data_t
	typedef struct
	{
		pixart_gstr_type_t type; /* 1byte */
		int16_t x;				 /* 2bytes */
		int16_t y;				 /* 2bytes */
		int16_t z;				 /* 2bytes */
	} mokibo_gesture_data_t;	 /* 7Bytes */

	typedef struct
	{
		uint8_t touchID;	// adjusted touch id //들어온 순서로 ++ID가 매겨진다. 9가 최대
		uint8_t confidence; // no_event(0), touch_down(1), significant_displacement(2), lift_off(3) //못쓸거같다
		uint8_t tip;		// touch is currently on the panel //탭에 쓸수있을듯 스크래치하면 패턴이 또 다르다.
		uint16_t x;			// 16bit x-coordinates in pixels
		uint16_t y;			// 16bit y-coordinates in pixels
		uint16_t z;			// 16bit z-coordinates in pixels
		uint16_t area;		//손끝이 얼마나 넓게 닿고있는지 크기나타냄
	} mokibo_finger_data_t;

	typedef struct
	{
		uint8_t touchCnt;	// total number of current touches
		uint32_t timestamp; // unit: 0.1ms //받아와서 read_touch_process 당시의 app_timer_cnt_get()
		uint8_t button;		// ?? click?

		pixart_gesture_packet_t gesture;

		mokibo_finger_data_t fingers[PIXART_INFO_MAX_COUNT]; // [10]
	} mokibo_touch_data_t;

	/**@brief DRV_PIXART read data handler */
	typedef void (*pixart_read_handler_t)(ret_code_t status, void *p_data);
	typedef void (*pixart_key_read_handler_t)(uint8_t *p_pressed_keys, uint8_t num_of_pressed_keys, bool keys_blocked);

	extern ret_code_t drv_pixart_init(pixart_read_handler_t read_handler);
	extern ret_code_t drv_pixart_register_key_handler(pixart_key_read_handler_t read_handler);
	extern ret_code_t drv_pixart_enable(void);
	extern ret_code_t drv_pixart_disable(void);
	extern ret_code_t drv_pixart_reset(void);
	extern ret_code_t drv_pixart_sleep(void);
	extern ret_code_t drv_pixart_wakeup(void);
	extern ret_code_t mkb_pixart_cli(size_t size, char **params);
	extern void drv_pixart_set_aamode(uint8_t touch_area_all_right_left);
	extern void drv_pixart_set_aamode_all(bool is_all);
	extern bool drv_pixart_get_otherhand_flag();

#pragma pack(pop)

#ifdef __cplusplus
}
#endif

#endif // __DRV_PIXART_H__
