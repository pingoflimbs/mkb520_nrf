/*
* ===============================================================================
* Copyright (C) 2017-2019
* INNOPRESSO INC. All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
* ===============================================================================
*/

#ifndef __DRV_IST40_H__
#define __DRV_IST40_H__

#include "nrf_twi_mngr.h"

#ifdef __cplusplus
extern "C"
{
#endif

#define IST40_ADDR 0x50 //0xA0
#define IST40_POWER_PIN MKB_IST40_POWER_PIN
#define IST40_INT_PIN MKB_IST40_INT_PIN
#define IST40_TIMER_INTERVAL MKB_IST40_SCAN_INTERVAL

#define IST40_GPIOTE_MASK (1 << IST40_INT_PIN)

#define IST40_MODE_TIMER 1
#define IST40_MODE_GPIOTE 2

#define IST40_MODE MKB_IST40_MODE

#define IST40_PIN_LOW 0
#define IST40_PIN_HIGH 1

#define IST40_BUFFER_SIZE 256

///////////////////////////////////////////
// IC Power Control
///////////////////////////////////////////

// time to hold IC off during H/W reset (in ms)
#define IST40_POWEROFF_DELAY 350
// time to wait until IC send Interrupt after IC reset (in ms)
#define IST40_INITIAL_INT_DELAY 500
// time to wait when not using INT check after IC reset (in ms)
#define IST40_INITIAL_DELAY 30

//////////////////////////////////////////////////////////////
// Elementary Definitions & Variable
//////////////////////////////////////////////////////////////
#define IST40_MAX_MTL_NODE 720
#define IST40_MAX_SLF_NODE 54

#define IST40_SWAP_XY (1 << 0)
#define IST40_FLIP_X (1 << 1)
#define IST40_FLIP_Y (1 << 2)

#define IST40_MAX_NUM_TOUCH_SCREEN 10

////////////////////////////////////////
// Touch Status
////////////////////////////////////////
#define IST40_TOUCH_NONE 0
#define IST40_TOUCH_PRESS 1
#define IST40_TOUCH_MOVE 2
#define IST40_TOUCH_RELEASE 3

#define IST40_DA_NORMAL_SINGLE (0x80000000)
#define IST40_DA_NORMAL_BURST (0x88000000)
#define IST40_DA_HIB_SINGLE (0x800B0000)
#define IST40_DA_HIB_BURST (0x880B0000)
#define IST40_DA_COMMAND (0x800A0000)

#define IST40_HCOM_GET_INTR_MSG (0x30000104 | IST40_DA_HIB_BURST)
#define IST40_HCOM_GET_SELFTEST_INFO IST40_HCOM_GET_INTR_MSG
#define IST40_HCOM_GET_CAL_INFO IST40_HCOM_GET_INTR_MSG
#define IST40_HCOM_GET_COORD_INFO IST40_HCOM_GET_INTR_MSG
#define IST40_HCOM_GET_COORD_SCREEN (0x30000108 | IST40_DA_HIB_BURST)
#define IST40_HCOM_GET_CRV (0x30000130 | IST40_DA_HIB_BURST)
#define IST40_HCOM_GET_EXT_COORD (0x00004290 | IST40_DA_NORMAL_BURST)

#define IST40_HCOM_GET_TSP_INFO_ADDR (0x00000000 | IST40_DA_NORMAL_BURST)

#define IST40_I2C_CTRL_REG (0x30000000 | IST40_DA_NORMAL_SINGLE)
#define IST40_I2C_32BIT_VALUE 0x003912D8

#define IST40_SW_RESET_CMD (0x40000000 | IST40_DA_COMMAND)
#define IST40_SW_RESET_VALUE 0x00000001

// MEMX Base
#define IST40_DFE_MEMX_BASE 0x30004000
#define IST40_MEMX_CDC_MTL (IST40_DFE_MEMX_BASE + 0x0000)
#define IST40_MEMX_CDC_SLF (IST40_DFE_MEMX_BASE + 0x2068)

#define IST40_BOOT_OK_NOTIFY 0x8EAD8EAD
#define IST40_HOST_REPORT_EXCEPTION 0xE11CE900 // LSB 1Byte는 Exception Information

#define IST40_JIG_MODE_TOUCH 0x80
#define IST40_TA_MODE_ON (1 << 0)
#define IST40_PROXIMITY_MODE_ON (1 << 1)
#define IST40_MODE_OFF 0

#define IST40_PARSE_FINGER_CNT(n) ((n >> 12) & 0xF)

////////////////////////////////////////
// OS (CM/CS/Jitter) Test
////////////////////////////////////////
#define IST40_HCOM_CM_ENABLE (1 << 0)
#define IST40_HCOM_CS_ENABLE (1 << 1)
#define IST40_HCOM_JITTER_ENABLE (1 << 2)
#define IST40_HCOM_PROXGAP_ENABLE (1 << 4)
#define IST40_HCOM_CRV_ENABLE (1 << 4)

#define IST40_END_NOTIFY_MASK 0xFFFFFF00
#define IST40_END_NOTIFY_CM 0x5E7FC300
#define IST40_END_NOTIFY_CS 0x5E7FC500
#define IST40_END_NOTIFY_JITTER 0x5E7F7100
#define IST40_END_NOTIFY_PROXGAP 0x5E7F9800
#define IST40_END_NOTIFY_CRV 0x5E7FC900

////////////////////////////////////////
// Read CMD
////////////////////////////////////////
#define IST40_HCOM_GET_CHIP_ID 0x00     //
#define IST40_HCOM_GET_VER_CORE 0x01    //
#define IST40_HCOM_GET_VER_FW 0x02      //
#define IST40_HCOM_GET_VER_PROJECT 0x03 //
#define IST40_HCOM_GET_VER_TEST 0x04    //
#define IST40_HCOM_GET_CRC32 0x05       //
#define IST40_HCOM_GET_PCAL_RESULT 0x06 //
#define IST40_HCOM_GET_CAL_RESULT 0x07  // calibration result, eAUTO_CAL_t 참조
#define IST40_HCOM_GET_SCAL_RESULT 0x08 //

#define IST40_HCOM_GET_BASELINE_TH 0x0E // [31:16]:BL, [15:0]:TH

#define IST40_HCOM_GET_LCD_INFO 0x10 // [31:16] width, [15:0] height
#define IST40_HCOM_GET_TSP_INFO 0x11 // [31:16] Number of Global Rx-ch, [15:0] Number of Global Tx-ch
#define IST40_HCOM_GET_SCR_INFO 0x12 // [31:16] Number of Screen Rx-ch, [15:0] Number of Screen Tx-ch

#define IST40_HCOM_GET_SWAP_INFO 0x17 // gstGlobalCfg.SwapXY

#define IST40_HCOM_GET_CS_TRX_RES_BASE 0x19
#define IST40_HCOM_GET_SELFTEST_BASE 0x1A
#define IST40_HCOM_GET_PROX_CDC_BASE 0x1C

#define IST40_HCOM_GET_COM_CHECKSUM 0x1F // Rx Com Buf(gaComInfo[]) Checksum

#define IST40_HCOM_GET_MAX 0x20

////////////////////////////////////////
// Write CMD
////////////////////////////////////////
#define IST40_HCOM_FW_START 0x01 //
#define IST40_HCOM_FW_HOLD 0x02  // '0':Disable, '1':Enable

#define IST40_HCOM_SLEEP_MODE_EN 0x15

#define IST40_HCOM_SET_TIME_ACTIVE 0x20  // Touch Active Time (단위 us)
#define IST40_HCOM_SET_TIME_IDLE 0x21    // Touch Idle Time (단위 us)
#define IST40_HCOM_SET_SPECIAL_MODE 0x22 // [0] TA, [1] Call, [2] FlipCover

#define IST40_HCOM_RUN_RAMCODE 0x30  // SRAM에 Code Loading 후 수행 => 완료 후 reset 필요
#define IST40_HCOM_RUN_CAL_AUTO 0x31 // 환경 변화에 의해 calibration이 필요한 경우
#define IST40_HCOM_RUN_CAL_TEST 0x32 // CR 테스트를 위한 calibration 결과 값 저장하지 않음
#define IST40_HCOM_RUN_SELFTEST 0x34 // SelfTest

#define IST40_HCOM_SET_JIG_MODE 0x80 // 0x40 : CR Test, 0x80 : Touch Test

#define IST40_HCOM_GENERATE_INT 0x90 // Set Auto Interrupt

////////////////////////////////////////
// Coordinate Struct
////////////////////////////////////////
#pragma pack(push, 1) // 1바이트 크기로 정렬
    typedef struct
    {
        uint8_t EventId : 2;
        uint8_t TouchId : 4;
        uint8_t TouchStatus : 2; // Press, Move, Release
        uint8_t X_11_4;
        uint8_t Y_11_4;
        uint8_t Y_3_0 : 4;
        uint8_t X_3_0 : 4;
        uint8_t Major;
        uint8_t Minor;
        uint8_t Z : 6; // 0 ~ 63
        uint8_t TouchType_3_2 : 2;
        uint8_t LeftNum : 5; // Num of Left Event
        uint8_t MaxEnergy : 1;
        uint8_t TouchType_1_0 : 2;
        uint8_t NoiseLevel;
        uint8_t MaxStrength;
        uint8_t HoverIdNum : 4;
        uint8_t LocationArea : 4;
        uint8_t Reserved0 : 8;
        uint8_t Reserved1 : 8;
        uint8_t Reserved2 : 8;
        uint8_t Reserved3 : 8;
        uint8_t Reserved4 : 8;
    } stTouchCoord_t;
#pragma pack(pop)

    ////////////////////////////////////////
    // TSP Info Struct
    ////////////////////////////////////////
    typedef struct
    {
        uint32_t ChipId;

        uint32_t CoreVer;
        uint32_t FwVer;
        uint32_t ProjectVer;
        uint32_t TestVer;

        uint32_t LcdWidth;
        uint32_t LcdHeight;

        uint32_t NumOfRx;
        uint32_t NumOfTx;
        uint32_t NumOfScrRx;
        uint32_t NumOfScrTx;

        uint32_t Crc32;
        uint32_t Crc32All;
        uint32_t CalResult;
        uint32_t SCalResult;
        uint32_t PCalResult;

        uint32_t SwapInfo;

        uint32_t ScrBl;
        uint32_t ScrTh;

        uint32_t SelfTestAddr;
        uint32_t CsTRxAddr;

        uint32_t CDCAddr;
        uint32_t SelfCDCAddr;
        uint32_t ProxCDCAddr;

        uint32_t ComChecksum;
    } stIST40xxInfo_t;

#define IST40_INFO_MAX_COUNT IST40_MAX_NUM_TOUCH_SCREEN
    typedef struct
    {
        uint8_t touchType; // finger(0), proximity(1), reserved(4~15)
        uint8_t touchID;   // adjusted touch id
        uint8_t eventID;   // no_event(0), touch_down(1), significant_displacement(2), lift_off(3)
        uint8_t tip;       // touch is currently on the panel
        uint16_t x;        // 16bit x-coordinates in pixels
        uint16_t y;        // 16bit y-coordinates in pixels
        uint8_t presure;   // touch intensity in counts
        uint16_t axis;
        uint8_t orient; // orientation
    } ist40_finger_info_t;

    typedef struct
    {
        uint8_t touchCnt;                               // total number of current touches
        uint16_t timestamp;                             // unit: 0.1ms
        ist40_finger_info_t info[IST40_INFO_MAX_COUNT]; // [10]
    } ist40_info_t;

    typedef void (*ist40_read_handler_t)(ret_code_t status, void *p_data);

    ////////////////////////////////////////
    // Elementary Functions
    ////////////////////////////////////////
    extern ret_code_t IST40xx_Ctrl_HW_Reset(uint32_t CheckIntr);
    extern ret_code_t IST40xx_Ctrl_SW_Reset(uint32_t CheckIntr);

    extern ret_code_t IST40xx_Ctrl_FwStart();
    extern ret_code_t IST40xx_Ctrl_FwHold(uint32_t EnDis);
    extern ret_code_t IST40xx_Ctrl_AutoCalibration(void);
    extern ret_code_t IST40xx_Ctrl_SetSpecialMode(uint32_t Mode);
    extern ret_code_t IST40xx_Ctrl_SetJigMode(uint32_t JigMode);
    extern ret_code_t IST40xx_Ctrl_SetSleepMode(bool SleepMode);
    extern ret_code_t IST40xx_Ctrl_RateTime(uint32_t Us);

    ////////////////////////////////////////
    // Test Function
    ////////////////////////////////////////
    extern ret_code_t IST40xx_Read_TSPInfo(void);
    extern ret_code_t IST40xx_PrepareOsTest(void);
    extern ret_code_t IST40xx_Test_CRV(uint32_t *pCrvData);
    extern ret_code_t IST40xx_Test_CM(uint16_t *pCmData);
    extern ret_code_t IST40xx_Test_Jitter(uint16_t *pJitterData);
    extern ret_code_t IST40xx_Test_CS(uint16_t *pCsData, uint32_t *pCsTx, uint32_t *pCsRx);
    extern ret_code_t IST40xx_Calibration(uint32_t *Result, uint32_t Timeout);
    extern ret_code_t IST40xx_Test_CR(uint32_t *pCrResult, uint32_t *pSelfCrResult, uint32_t *pProxCrResult);
    extern void IST40xx_Ctrl_InitTouchTest(void);
    extern ret_code_t IST40xx_Ctrl_ReadTouchData(void);

    extern ret_code_t drv_ist40_init(ist40_read_handler_t read_handler);
    extern ret_code_t drv_ist40_reset(void);
    extern ret_code_t drv_ist40_enable(void);
    extern ret_code_t drv_ist40_disable(void);
    extern ret_code_t drv_ist40_read(ist40_info_t *p_data);

#ifdef __cplusplus
}
#endif

#endif // __DRV_IST40_H__
