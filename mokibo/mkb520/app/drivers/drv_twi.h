/*
* ===============================================================================
* Copyright (C) 2017-2019
* INNOPRESSO INC. All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
* ===============================================================================
*/

#ifndef __DRV_TWI_H__
#define __DRV_TWI_H__

#include <stdbool.h>
#include <stdint.h>

#include "nrf.h"
#include "nrf_pwr_mgmt.h"
#include "nrf_twi_mngr.h"

// SoC configuration:
#define IS_IO_VALID(io) (((io) & ~0x1F) == 0)
#define ARRAY_SIZE(arr) (sizeof(arr) / sizeof((arr)[0]))

#define CONFIG_TWI_INIT_DELAY 500
#define CONFIG_TWI_QSIZE 10

// <q> Keep the interface powered up during System Off state.
/**@brief Primary TWI: Keep the interface powered up during System Off state. */
#if TWI0_ENABLED
#define CONFIG_TWI0_UP_IN_SYSOFF_STATE 0
#define CONFIG_IO_TWI0_SCL MKB_TWI_SCL_PIN
#define CONFIG_IO_TWI0_SDA MKB_TWI_SDA_PIN
#endif

// <q> Keep the interface powered up during System Off state.
/**@brief Primary TWI: Keep the interface powered up during System Off state. */
#if TWI1_ENABLED
#define CONFIG_TWI1_UP_IN_SYSOFF_STATE 0
#define CONFIG_IO_TWI1_SCL MKB_TWI_SCL_PIN
#define CONFIG_IO_TWI1_SDA MKB_TWI_SDA_PIN
#endif

/**
* @defgroup TWI_COMMON TWI common procedures
* @{
* @ingroup other
* @brief Common TWI functions.
*/

/**@brief Initialize TWI.
 *
 * Function fills in bus configuration with correct data and initializes TWI
 * manager.
 *
 * @return NRF_SUCCESS on success, otherwise an error code.
 */
ret_code_t twi_init(void);

/**@brief Perform a single synchronous TWI transaction.
 *
 * @param[in] p_bus_config    Required TWI bus configuration.
 * @param[in] p_transfers     Pointer to an array of transfers to be performed.
 * @param     transfer_count  Number of transfers to be performed.
 *
 * @return NRF_SUCCESS on success, otherwise an error code.
 */
ret_code_t twi_perform(nrf_drv_twi_config_t const *p_bus_config,
                       nrf_twi_mngr_transfer_t const *p_transfers,
                       uint8_t transfer_count);

/**@brief Perform a single synchronous TWI transaction.
 *
 * @param[in] p_transaction  Transaction to be scheduled by TWI master.
 *
 * @return NRF_SUCCESS on success, otherwise an error code.
 */
ret_code_t twi_schedule(nrf_twi_mngr_transaction_t const *p_transaction);

ret_code_t twi_read_burst(nrf_drv_twi_config_t const *p_bus_config,
                          uint8_t device_addr,
                          uint8_t *p_value,
                          uint8_t len);

ret_code_t twi_write_burst(nrf_drv_twi_config_t const *p_bus_config,
                           uint8_t device_addr,
                           uint8_t *p_value,
                           uint8_t len);

/**@brief Reads from the register of the TWI device.
 *
 * @param[in]  p_bus_config  Required TWI bus configuration.
 * @param[in]  device_addr   Device address.
 * @param[in]  register_addr Register address.
 * @param[out] p_value       Pointer to the variable to which the register value will be written.
 *
 * @return NRF_SUCCESS on success, otherwise an error code.
 */
ret_code_t twi_register_read(nrf_drv_twi_config_t const *p_bus_config,
                             uint8_t device_addr,
                             uint8_t register_addr,
                             uint8_t *p_value);

/**@brief Writes to the register of the TWI device.
 *
 * @param[in] p_bus_config   Required TWI bus configuration.
 * @param[in] device_addr    Device address.
 * @param[in] register_addr  Register address.
 * @param[in] value          Data to write.
 *
 * @return NRF_SUCCESS on success, otherwise an error code.
 */
ret_code_t twi_register_write(nrf_drv_twi_config_t const *p_bus_config,
                              uint8_t device_addr,
                              uint8_t register_addr,
                              uint8_t value);

/**@brief Writes to multiple registers of the TWI device.
 *
 * @param[in] p_bus_config          Required TWI bus configuration.
 * @param[in] device_addr           Device address.
 * @param[in] p_reg_val_array       Array of the { register address, value } pairs.
 * @param[in] reg_val_array_size    Number of the { register address, value } pairs to write.
 * @param[in] perform_verification  If true, then the register content will be verified after write.
 *
 * @return NRF_SUCCESS on success, otherwise an error code.
 */
ret_code_t twi_register_bulk_write(nrf_drv_twi_config_t const *p_bus_config,
                                   uint8_t device_addr,
                                   const uint8_t p_reg_val_array[][2],
                                   unsigned int reg_val_array_size,
                                   bool perform_verification);

/**
 *@}
 **/
extern void twi_probe(void);
extern bool twi_probe_exist(uint8_t addr);

#endif /* __DRV_TWI_H__ */
