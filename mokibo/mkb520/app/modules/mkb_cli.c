/*
* ===============================================================================
* Copyright (C) 2017-2019
* INNOPRESSO INC. All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
* ===============================================================================
*/

#include <stdbool.h>
#include <stdint.h>

#include "sdk_common.h"

#include "nrf_assert.h"
#include "nrf_error.h"
#include "nrf_log.h"
#include "nrf_delay.h"
#include "nrf_cli.h"
#include "nrf_cli_rtt.h"
#include "nrf_cli_types.h"

#include "app_error.h"
#include "app_timer.h"
#include "app_scheduler.h"

#define MKB_LOG_LEVEL MKB_CLI_LOG_LEVEL
#include "mkb_common.h"
#include "mkb_cli.h"

#if (defined(MKB_CLI_ENABLED) && MKB_CLI_ENABLED)

/**
 * @brief Command line interface instance
 * */
#define CLI_LOG_QUEUE_SIZE (4)

NRF_CLI_RTT_DEF(m_cli_rtt_transport);
NRF_CLI_DEF(m_cli_rtt,
            "rtt_cli:~$ ",
            &m_cli_rtt_transport.transport,
            '\n',
            CLI_LOG_QUEUE_SIZE);

void mkb_cli_init(void)
{
    ret_code_t ret;

    ret = nrf_cli_init(&m_cli_rtt, NULL, true, true, NRF_LOG_SEVERITY_INFO);
    APP_ERROR_CHECK(ret);
}

void mkb_cli_start(void)
{
    ret_code_t ret;

    ret = nrf_cli_start(&m_cli_rtt);
    APP_ERROR_CHECK(ret);
}

void mkb_cli_process(void)
{
    SEGGER_RTT_SetTerminal(MKB_CLI_TERMINAL_ID);
    nrf_cli_process(&m_cli_rtt);
    SEGGER_RTT_SetTerminal(MKB_LOG_TERMINAL_ID);
}

void mkb_cli_unknown_param_help(nrf_cli_t const *p_cli,
                                char const *p_param,
                                char const *p_cmd)
{
    nrf_cli_fprintf(p_cli,
                    NRF_CLI_ERROR,
                    "%s: unknown parameter '%s'\n"
                    "Try '%s -h' for help.\n",
                    p_cmd,
                    p_param,
                    p_cmd);
}

void mkb_cli_wrong_param_count_help(nrf_cli_t const *p_cli, char const *p_cmd)
{
    nrf_cli_fprintf(p_cli,
                    NRF_CLI_ERROR,
                    "%s: wrong parameter count.\n"
                    "Try '%s -h' for help.\n",
                    p_cmd,
                    p_cmd);
}

#endif // (defined(MKB_CLI_ENABLED) && MKB_CLI_ENABLED)
