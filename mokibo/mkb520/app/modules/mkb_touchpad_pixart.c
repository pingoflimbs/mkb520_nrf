/*
 * ===============================================================================
 * Copyright (C) 2017-2019
 * INNOPRESSO INC. All Rights Reserved
 *
 * The content of gstr_step file is copyrighted by INNOPRESSO or its
 * affiliated, associated or related companies. All copyrights and other
 * associated intellectual property rights related thereto are expressly
 * reserved by INNOPRESSO INC.
 * ===============================================================================
 */

#include <stdbool.h>
#include <stdint.h>

#include "sdk_common.h"

#include "nrf_assert.h"
#include "nrf_error.h"
#include "nrf_log.h"
#include "nrf_delay.h"
#include "nrf_cli.h"

#include "app_error.h"
#include "app_timer.h"
#include "app_scheduler.h"

#include "mkb_common.h"
#include "mkb_db.h"
#include "mkb_ble.h"
#include "mkb_util.h"
#include "mkb_coms.h"
#include "mkb_keyboard.h"
#include "drv_pixart.h"

#include "mkb_touchpad.h"
#include "mkb_touchpad_pixart.h"
#include "math.h"

#if (defined(MKB_PIXART_ENABLED) && MKB_PIXART_ENABLED)

#define MKB_LOG_LEVEL MKB_TOUCHPAD_LOG_LEVELl
#define M_COMM_CONSUMER_BUF_LEN 3

#define INCREASE(NUM, LIMIT) \
    if (NUM <= LIMIT)        \
    {                        \
        NUM = NUM + 1;       \
    }
#define DECREASE(NUM, LIMIT) \
    if (LIMIT <= NUM)        \
    {                        \
        NUM = NUM - 1;       \
    }
#define ADD_WITH_LIMIT(DEST, ADDER, UPLIMIT, DOWNLIMIT)  \
    if (ADDER > 0)                                       \
    {                                                    \
        if (ADDER > 0 && DEST + ADDER >= UPLIMIT)        \
        {                                                \
            DEST = UPLIMIT;                              \
        }                                                \
        else if (ADDER < 0 && DEST + ADDER <= DOWNLIMIT) \
        {                                                \
            DEST = DOWNLIMIT;                            \
        }                                                \
        else                                             \
        {                                                \
            DEST = DEST + ADDER;                         \
        }                                                \
    }

// X는 900부터 시작 키 하나당 약 230의 값을 가짐
#define XPOS_KEY_FG 820
#define XPOS_KEY_GH 1050
#define XPOS_KEY_HJ 1280
#define TOUCH_DB_MAX 48
#define TOUCH_FINGER_MAX 8

APP_TIMER_DEF(detect_idle_timer);

static const uint8_t m_vol_mute_cmd[M_COMM_CONSUMER_BUF_LEN] = {0xE2, 0x00, 0x00};
static const uint8_t m_vol_down_cmd[M_COMM_CONSUMER_BUF_LEN] = {0xEA, 0x00, 0x00};
static const uint8_t m_vol_up_cmd[M_COMM_CONSUMER_BUF_LEN] = {0xE9, 0x00, 0x00};

static const uint8_t m_android_home[M_COMM_CONSUMER_BUF_LEN] = {35, 2, 0};
static const uint8_t m_android_srch[M_COMM_CONSUMER_BUF_LEN] = {207, 0, 0};
static const uint8_t m_android_menu[M_COMM_CONSUMER_BUF_LEN] = {162, 1, 0};
static const uint8_t m_android_lock[M_COMM_CONSUMER_BUF_LEN] = {48, 0, 0};
static const uint8_t m_android_back[M_COMM_CONSUMER_BUF_LEN] = {70, 0, 0};

static const uint8_t m_iphone_home[M_COMM_CONSUMER_BUF_LEN] = {64, 0x00, 0x00};
static const uint8_t m_iphone_lock[M_COMM_CONSUMER_BUF_LEN] = {48, 0x00, 0x00};
static const uint8_t m_iphone_vkbd[M_COMM_CONSUMER_BUF_LEN] = {184, 0x00, 0x00};

static const uint8_t m_consumer_no_cmd[M_COMM_CONSUMER_BUF_LEN] = {0x00, 0x00, 0x00};
static const uint8_t m_virtual_keyboard_cmd[M_COMM_CONSUMER_BUF_LEN] = {0xAE, 0x01, 0x00};

typedef enum
{
    DIR_RIGHT,
    DIR_RIGHT_UP,
    DIR_UP,
    DIR_LEFT_UP,
    DIR_LEFT,
    DIR_LEFT_DOWN,
    DIR_DOWN,
    DIR_RIGHT_DOWN,
    DIR_NONE,
    DIR_MAX
} direction_t;

typedef enum
{
    PINCH_NONE = 0,
    PINCH_WIDE,
    PINCH_NARR,
    PINCH_MAX
} pinch_t;

typedef enum
{
    FGRX_UNKNOWN = 0,
    IDLE_0_FLOATING,
    IDLE_0_CLEARED,
    IDLE_0_X_SCROLL,
    IDLE_0_Y_SCROLL,

    IDLE_1_TAP,
    IDLE_1_DOUBLE_TAP,
    FNGR_1_XY_DRAG,
    FNGR_1_XY_POINTING,

    IDLE_2_TAP,
    IDLE_2_X_SWIPE,
    IDLE_2_PINCH,
    FNGR_2_Y_SCROLL,
    FNGR_2_X_SCROLL,
    FNGR_2_PINCH,

    IDLE_3_TAP,
    IDLE_3_UP_SWIPE,
    IDLE_3_DOWN_SWIPE,
    IDLE_3_X_SWIPE,
    FNGR_3_XY_SCROLL,

    IDLE_4_TAP,
    IDLE_4_UP_SWIPE,
    IDLE_4_DOWN_SWIPE,
    IDLE_4_X_SWIPE
} fgr_job_t;

typedef struct
{
    // need no clear
    fgr_job_t active_fngr_job[2]; //마지막 사용된 작업. clear 시에 fngr0_float 이 들어간다. acive_fngr_job으로 이름바꿔야함
    fgr_job_t detect_fngr_job[2]; //마지막 관찰된 작업. clear 시에 fngr0_float 이 들어간다.

    uint32_t active_time_stamp; //마지막 사용된 작업의 시간.
    uint32_t detect_time_stamp; //마지막 관찰된 작업의 시간.

    uint8_t detect_cnt_static; // 횟수. 작업바뀌면 초기화
    uint8_t active_cnt_static; //횟수. 작업바뀌면 초기화

    // need clear
    uint8_t detect_cnt_volatile; // 횟수.손 떼거나 작업바뀌면 초기화
    uint8_t active_cnt_volatile; // 횟수.손 떼거나 작업바뀌면 초기화

    int16_t dist_threshold_x; //공통적으로 사용되는 쓰레숄드.
    int16_t dist_threshold_y; //공통적으로 사용되는 쓰레숄드.
} last_job_t;

static last_job_t last_job; //마지막 작업을 기억함. 선택적으로 clear됨..?

typedef struct
{
    mokibo_gesture_data_t gesture;
    mokibo_finger_data_t fingers[PIXART_INFO_MAX_COUNT]; // 가장 왼쪽좌표가 0 가장오른쪽좌표가 finger_cnt-1

    // NEED 1 QUEUE
    uint8_t finger_cnt; //붙은 손가락 갯수
    int16_t continus_x; //손가락갯수 변화에도 연속성을 갖는 안정된 center 값
    int16_t continus_y; //손가락갯수 변화에도 연속성을 갖는 안정된 center 값
    int16_t area_size;  //맨왼손과 맨오른손가락의 거리. double로 해야되는데 숫자가 커서 그냥일케함
    uint32_t time;      // pixart_read_touch 시의 app_timer_cnt_get()

    // NEED 2 QUEUE
    int16_t delta_area;         // 여러 손가락의 최좌우측 간의 거리변화값
    int16_t delta_x;            //원본 x좌표 delta
    int16_t delta_y;            //원본 y좌표delta
    int16_t compensate_delta_x; //보정된 x좌표delta
    int16_t compensate_delta_y; //보정된 y좌표delta
    int16_t delta_time;         // timer delta

    // NEED 4 QUEUE //평균내야 하는거
    double delta_dist;     //최근 4개의 거리평균. double이라 디버그할땐 1000을곱해서 확인해야함
    direction_t delta_dir; //최근 4개의 방향평균
} touch_db_queue_t;        //터치데이터 순간순간의 delta 값들

typedef struct
{
    // need no clear
    uint8_t data_cnt;                     // queue의 갯수
    touch_db_queue_t queue[TOUCH_DB_MAX]; // delta 와 pos 통합큐 8*48 380ms

    int16_t start_x;     //시작점
    int16_t start_y;     //시작점
    uint32_t start_time; //시작시간

    pixart_gstr_type_t founded_gesture; //현재 큐 내에서 사용되었다고 보여지는 제스처

    int16_t continus_x_adj; // continus_x 을 위한 temp값. 실제로 사용되진않음
    int16_t continus_y_adj; // continus_x 를 위한 temp값. 실제로 사용되진않음

    uint8_t max_touch_cnt; //입력되었던 손가락중 가장 큰값
    uint8_t fingers_cnts[TOUCH_FINGER_MAX];
    uint8_t directions_cnts[DIR_MAX];           // DB에서 각 dir의 갯수
    uint8_t directions_cnts_6_samples[DIR_MAX]; // DB에서 각 dir의 갯수
    uint16_t pinchs_cnts[PINCH_MAX];

    // need no clear
    int16_t end_x;     //종료점
    int16_t end_y;     //종료점
    uint32_t end_time; //종료시간

} touch_db_t; //터치데이터 종합한 값, 손가락 떨어지면 리셋됨

static touch_db_t m_touch_db; //터치 큐. 젤중요. 파라미터가 아닌 전역변수로써 접근하도록한다. db_clear시 end_xytime 제외 전부 삭제됨.

uint8_t drag_step_temp = 0; //드래그 일단 우겨넣음. 나중에 리팩토링 해야댐. 코드 안망가지게.
uint32_t drag_time_temp = 0;
bool fgr0_is_aamode_all = 0;

typedef struct
{
    bool fgr0_is_tapped;           //    detect_idle_handler() 에서 체크하는, tap 이 들어갔는지 확인하는값.
    bool fgr0_is_need_mod_clear;   // 3점좌우, 4점좌우 등 모드키 해제해야하는지
    bool fgr0_is_need_click_clear; //드래그 해제해야하는지

    uint8_t fgr1_moving_step;
    uint8_t fgr1_dragging_step;

    int8_t fgr2_scroll_step;
    int8_t fgr2_hv_scroll_step;
    int16_t fgr2_x_scroll_move_threshold; // 값을 가지고있다가 delta_xy에 의해 쓰레숄드가 다 소모되면 트리거된다.
    int16_t fgr2_y_scroll_move_threshold;
    double fgr2_y_scroll_move_threshold_mac;

    int16_t fgr2_pinch_inout_step;

    int16_t fgr3_y_scroll_move_threshold;
    int16_t fgr3_x_scroll_move_threshold;
    int8_t fgr3_hv_scroll_step;
    int8_t fgr3_lr_scroll_setp;
    int8_t fgr3_ud_scroll_setp;

    int8_t fgr4_hv_scroll_step;
    int8_t fgr4_lr_scroll_step;
    int8_t fgr4_ud_scroll_step;
} mkb_touchpad_pixart_t;
mkb_touchpad_pixart_t gstr_step; //모듈에서 사용하는 변수들 //deprecated 예정.

// queueing job
ret_code_t mkb_touchpad_pixart_init();
void mkb_touchpad_read_process(void *p_data);
static bool touch_db_enqueue(touch_db_t *touch_db, mokibo_touch_data_t *mkb_touch_data);

// process job
static void fngr_process();
static void idle_process_timer_handler();
static void idle_process();

static void touch_db_clean();

static void last_job_active(fgr_job_t fgr_job);
static void last_job_detect(fgr_job_t fgr_job);
static void last_job_fngr0_clean(); // need erase

// search: 조건에 부합하면 true리턴.
//예외적 false (키입력후 쿨타임등)
//예외적 true (연속작업중 등)
//일반적 false로 이루어짐 (갯수 모자람 등)
// return true

// idle-0
static bool search_idle_0_y_scroll();
static void detect_idle_0_y_scroll();
static void active_idle_0_y_scroll(); // deprectated
static void active_idle_0_x_scroll(); // deprectated

// idle-1
static bool search_idle_1_tap();
static void detect_idle_1_tap();
static void active_idle_1_tap();

static bool search_idle_1_double_tap(); // need implement
static void detect_idle_1_double_tap(); // need implement
static void active_idle_1_double_tap(); // need implement

static bool search_idle_1_drag(); // need implement
static void detect_idle_1_drag(); // need implement
static void active_idle_1_drag(); // need implement

// fngr-1
static bool search_fngr_1_xy_pointing();
static void detect_fngr_1_xy_pointing();
static void active_fngr_1_xy_pointing();

// idle-2
static bool search_idle_2_tap();
static void detect_idle_2_tap();
static void active_idle_2_tap();

static bool search_idle_2_x_swipe();
static void detect_idle_2_x_swipe();
static void active_idle_2_x_swipe(direction_t direction);

static bool search_idle_2_pinch();              // deprectated
static void detect_idle_2_pinch();              // deprectated
static void active_idle_2_pinch(pinch_t pinch); // deprectated

// fngr-2
static bool search_fngr_2_y_scroll();
static void detect_fngr_2_y_scroll();
static void active_fngr_2_y_scroll();

static bool search_fngr_2_x_scroll();
static void detect_fngr_2_x_scroll();
static void active_fngr_2_x_scroll();

static bool search_fngr_2_pinch();
static void detect_fngr_2_pinch();
static void active_fngr_2_pinch(pinch_t pinch);

// idle-3
static bool search_idle_3_tap();
static void detect_idle_3_tap();
static void active_idle_3_tap();

static bool search_idle_3_up_swipe();
static void detect_idle_3_up_swipe();
static void active_idle_3_up_swipe();

static bool search_idle_3_down_swipe();
static void detect_idle_3_down_swipe();
static void active_idle_3_down_swipe();

static bool search_idle_3_x_swipe();
static void detect_idle_3_x_swipe();
static void active_idle_3_x_swipe();

// fngr-3
static bool search_fngr_3_xy_scroll();
static void detect_fngr_3_xy_scroll();
static void active_fngr_3_xy_scroll(direction_t direction);

// idle-4
static bool search_idle_4_tap();
static void detect_idle_4_tap();
static void active_idle_4_tap();

static bool search_idle_4_up_swipe();
static void detect_idle_4_up_swipe();
static void active_idle_4_up_swipe();

static bool search_idle_4_down_swipe();
static void detect_idle_4_down_swipe();
static void active_idle_4_down_swipe();

static bool search_idle_4_x_swipe();
static void detect_idle_4_x_swipe();
static void active_idle_4_x_swipe(bool isLeftorRight);

// fngr-n
static bool search_fngr_n_typing();
static void detect_fngr_n_typing();
static void active_fngr_n_typing();

// get sums
static int16_t get_is_pinch_sum(uint8_t sample_cnt);
static int16_t get_deltaarea_sum(uint8_t sample_cnt);

static bool is_safe_from_typing_ready_end_ms(uint16_t ms);

//외부 함수
extern uint32_t mkb_db_last_key_up_timer_get();
extern bool mkb_db_is_func_pressed_get();
extern uint32_t mkb_db_last_tap_time_get(void);
extern void mkb_db_last_tap_time_set(uint32_t last_tap_up_time);
extern uint32_t mkb_db_last_gstr_time_get(void);
extern void mkb_db_last_gstr_time_set(uint32_t last_gstr_time);
extern void mkb_db_is_drag_click_down_set(bool is_clicked);
extern void mkb_db_is_drag_click_down_get(void);
extern void drv_pixart_set_aamode(uint8_t touch_area);

//초기화
ret_code_t mkb_touchpad_pixart_init()
{
    app_timer_create(&detect_idle_timer, APP_TIMER_MODE_SINGLE_SHOT, idle_process_timer_handler);

    return NRF_SUCCESS;
}

// drv_pixart 에서 pixart_read_touch_process() 에서 계속 호출당하는 함수. 터치데이터를 받아와 관리한다.
void mkb_touchpad_read_process(void *p_data)
{
    mokibo_touch_data_t *p_mkb_touch_data = (mokibo_touch_data_t *)p_data; // drvpixart의 touch process 에서 받아온 raw값을 넣는다.

#ifdef ________DBG_20220303_touch_gesture_and_xy_data_sprintf
    char log_buf[20] = {
        NULL,
    };

    switch (p_mkb_touch_data->gesture.type)
    {
    case PIXART_GSTR_TYPE_ONE_MOVE:
    {
        sprintf(log_buf, "o Move");
        break;
    }
    case PIXART_GSTR_TYPE_ONE_TAP_AND_DRAG:
    {
        sprintf(log_buf, "o Drag");
        break;
    }
    case PIXART_GSTR_TYPE_ONE_TAP_AND_DRAG_RELEASE:
    {
        sprintf(log_buf, "o Drag Off");
        break;
    }
    case PIXART_GSTR_TYPE_TWO_HSCRL:
    {
        // continus 한 delta 데이터가 나온다.
        sprintf(log_buf, "oo Left Right");
        break;
    }
    case PIXART_GSTR_TYPE_TWO_VSCRL:
    {
        sprintf(log_buf, "oo Up Down");
        break;
    }
    case PIXART_GSTR_TYPE_TWO_PINCH:
    {
        sprintf(log_buf, "oo Zoom");
        break;
    }
    case PIXART_GSTR_TYPE_TWO_PINCH_RELEASE:
    {
        sprintf(log_buf, "oo Zoom Off");
        break;
    }
    case PIXART_GSTR_TYPE_TWO_TAP:
    {
        sprintf(log_buf, "oo Tap ");
        break;
    }
    case PIXART_GSTR_TYPE_TWO_TAP_RELEASE:
    {
        sprintf(log_buf, "oo Tap Off");
        break;
    }
    case PIXART_GSTR_TYPE_THREE_UP:
    {
        sprintf(log_buf, "ooo Up");
        break;
    }
    case PIXART_GSTR_TYPE_THREE_DOWN:
    {
        sprintf(log_buf, "ooo Down");
        break;
    }

    case PIXART_GSTR_TYPE_THREE_LEFT:
    {
        sprintf(log_buf, "ooo Left");
        break;
    }
    case PIXART_GSTR_TYPE_THREE_RIGHT:
    {
        sprintf(log_buf, "ooo Right");
        break;
    }

    case PIXART_GSTR_TYPE_THREE_MOVE:
    {
        sprintf(log_buf, "ooo Move");
        break;
    }

    case PIXART_GSTR_TYPE_THREE_RELEASE:
    {
        sprintf(log_buf, "ooo Off");
        break;
    }
    case PIXART_GSTR_TYPE_THREE_TAP:
    {
        sprintf(log_buf, "ooo Tap");
        break;
    }
    case PIXART_GSTR_TYPE_THREE_TAP_RELEASE:
    {
        sprintf(log_buf, "ooo Tap Off");
        break;
    }
    default:
    {
        break;
    }
    }
    ________DBG_20220303_touch_gesture_and_xy_data_sprintf("fngr[%d], gstr:[%-16s], gxyz:[%2d, %2d, %2d] f1xy[%4d, %4d], f2xy[%4d, %4d] f3xy[%4d, %4d]",
                                                           p_mkb_touch_data->touchCnt, log_buf, (int16_t)p_mkb_touch_data->gesture.x, (int16_t)p_mkb_touch_data->gesture.y, (int16_t)p_mkb_touch_data->gesture.z,
                                                           p_mkb_touch_data->fingers[0].x, p_mkb_touch_data->fingers[0].y,
                                                           p_mkb_touch_data->fingers[1].x, p_mkb_touch_data->fingers[1].y,
                                                           p_mkb_touch_data->fingers[2].x, p_mkb_touch_data->fingers[2].y);
#endif

#ifdef ________DBG_20220314_idle_module_refactory

    if (p_mkb_touch_data->touchCnt > 0) //손가락이 닿아있을때
    {
        touch_db_enqueue(&m_touch_db, p_mkb_touch_data);
        ________DBG_20220314_idle_module_refactory("fngr detected, timer stop");
        app_timer_stop(detect_idle_timer);
        fngr_process();
    }
    else if (p_mkb_touch_data->touchCnt == 0 && m_touch_db.queue[0].finger_cnt > 0) ///손가락이 떨어진 직후
    {
        ________DBG_20220314_idle_module_refactory("float detected, timer start");
        touch_db_enqueue(&m_touch_db, p_mkb_touch_data);

        idle_process();

        if (last_job.active_fngr_job[0] == IDLE_1_TAP)
        {
        }
        else if (last_job.active_fngr_job[0] == IDLE_0_Y_SCROLL)
        {
        }

        touch_db_clean();

        // app_timer_start(detect_idle_timer, APP_TIMER_TICKS(8), NULL); // detect_idle_0_float()
    }
    else //// 1. tap gesture//무시// 2. inertia_scroll//무시
    {
    }

#else

    ret_code_t err_code = touch_db_enqueue(&m_touch_db, p_mkb_touch_data); // touch_db 에 touch_data touchpad_db_pos_info로 가공하여 넣는다.

    fngr_process();
    if (err_code == NRF_SUCCESS)
    {
        app_timer_stop(detect_idle_timer);
        // app_timer_start(detect_idle_timer, APP_TIMER_TICKS(48), NULL); //키 전송 하는데 걸리는 시간때문에 48 이상으로 해야한다. 나중에 지연된 이벤트 사용하여 고칠것
        app_timer_start(detect_idle_timer, APP_TIMER_TICKS(58), NULL);
        // app_timer_start(detect_idle_timer, APP_TIMER_TICKS(8), NULL);
    }
#endif
}

// touch_db 큐잉
static bool touch_db_enqueue(touch_db_t *touch_db, mokibo_touch_data_t *mkb_touch_data)
{
#ifdef ________DBG_20220314_idle_module_refactory
#else
    { // inertia scroll 동작하면 손가락 떨어진거로 인식한다.
        if (mkb_touch_data->touchCnt == 0)
        {
            if (last_job.active_fngr_job[0] == FNGR_2_Y_SCROLL)
            {

                return NRF_ERROR_NOT_FOUND;
            }
        }
    }

    { // inertia scroll 동작하면 손가락 떨어진거로 인식한다.
        if ((mkb_touch_data->gesture.type == PIXART_GSTR_TYPE_TWO_VSCRL || mkb_touch_data->gesture.type == PIXART_GSTR_TYPE_TWO_HSCRL) && mkb_touch_data->touchCnt == 0)
        {
            return NRF_ERROR_NOT_FOUND;
        }
    }

    /*
        { //실험적: 터치없으면 아예 안받음
            if ((mkb_touch_data->gesture.type == PIXART_GSTR_TYPE_TWO_VSCRL || mkb_touch_data->gesture.type == PIXART_GSTR_TYPE_TWO_HSCRL) && mkb_touch_data->touchCnt == 0)
            {
                return NRF_ERROR_NOT_FOUND;
            }
        }*/
#endif

    { //터치락 켜져있으면 터치 인식 안하도록.
        if (mkb_db_system_state_get() == SYSTEM_STATE_TOUCHLOCK)
        {
            return NRF_ERROR_NOT_FOUND;
        }
    }

    { // data_cnt ++
        if (touch_db->data_cnt < TOUCH_DB_MAX)
        {
            touch_db->data_cnt++;
        }
    }

    { // touch_db enqueing. 터치 DB에서 queue 밀어넣고 memmove로 한칸씩 뒤로 민긴다. 새 노드는 초기화한다
        memmove(&(touch_db->queue[1]), &(touch_db->queue[0]), sizeof(touch_db->queue[0]) * (TOUCH_DB_MAX - 1));
        memset((&touch_db->queue[0]), 0, sizeof(touch_db_queue_t));
    }

    { // 새노드에 finger_cnt, time 복사
        touch_db->queue[0].finger_cnt = mkb_touch_data->touchCnt;
        touch_db->queue[0].time = mkb_touch_data->timestamp; // nrf_timer_cnt_get으로 받음
    }

    { // gesture 복사
        memcpy(&(touch_db->queue[0].gesture), &(mkb_touch_data->gesture), sizeof(mokibo_gesture_data_t));
    }

    { //새 노드에 손가락 복사하고 fingers[]를 x좌표에따라 fingers[]에 오름차순 정렬
        memcpy(&(touch_db->queue[0].fingers[0]), &(mkb_touch_data->fingers[0]), sizeof(mokibo_finger_data_t) * (mkb_touch_data->touchCnt));

        for (uint8_t i = 0; i < mkb_touch_data->touchCnt - 1; i++)
        {
            mokibo_finger_data_t finger_swap_temp;
            uint16_t leftest_finger_x = 10000;
            uint8_t leftest_finger_num = 0;

            for (uint8_t k = i; k < mkb_touch_data->touchCnt; k++)
            {
                if (touch_db->queue[0].fingers[k].x < leftest_finger_x)
                {
                    leftest_finger_x = touch_db->queue[0].fingers[k].x;
                    leftest_finger_num = k;
                }
            }
            //스왑
            memmove(&(finger_swap_temp), &(touch_db->queue[0].fingers[i]), sizeof(mokibo_finger_data_t));
            memmove(&(touch_db->queue[0].fingers[i]), &(touch_db->queue[0].fingers[leftest_finger_num]), sizeof(mokibo_finger_data_t));
            memmove(&(touch_db->queue[0].fingers[leftest_finger_num]), &(finger_swap_temp), sizeof(mokibo_finger_data_t));
        }

#ifdef ________DBG_20210729_pct_gesture_enqueued_sprint
        { //제스처 프린트
            char logbuf[250];
            sprintf(logbuf, "fng:%d cnt:%02d pos:%03d,%03d: ", touch_db->queue[0].finger_cnt, touch_db->data_cnt, touch_db->queue[0].fingers[0].x, touch_db->queue[0].fingers[0].y);
            for (int i = 0; i < TOUCH_DB_MAX; i++)
            {
                if (i < touch_db->data_cnt)
                {
                    sprintf(logbuf, "%s|% 2x", logbuf, touch_db->queue[i].gesture.type);
                }
                else
                {
                    sprintf(logbuf, "%s|  ", logbuf);
                }
            }
            ________DBG_20210729_pct_gesture_enqueued_sprint("%s", logbuf);
        }
#endif
    }

    { // founded gesture 가장 최근 입력받은 제스처 패킷
        if (touch_db->queue[0].gesture.type != PIXART_GSTR_TYPE_NONE)
        {
            touch_db->founded_gesture = touch_db->queue[0].gesture.type;
        }
    }

    { // continus 값 만들기
        //최초 좌표들어왔을때 맨 왼쪽의 좌표를 받음.
        //손가락 갯수가 바뀌면 이전 좌표 유지한 상태로 새 맨 왼쪽의 좌표와 차이를 adjust_val 저장함
        //이 값과 연산한 xy좌표를 continus xy 에 저장함. 이값은 -가 될수도 있음
        if (touch_db->data_cnt == 1) //최초입력이면
        {
            uint16_t x_sum = 0;
            uint16_t y_sum = 0;
            for (uint8_t i = 0; i < touch_db->queue[0].finger_cnt; i++)
            {
                x_sum += touch_db->queue[0].fingers[i].x;
                y_sum += touch_db->queue[0].fingers[i].y;
            }

            touch_db->queue[0].continus_x = (x_sum / touch_db->queue[0].finger_cnt);
            touch_db->queue[0].continus_y = (y_sum / touch_db->queue[0].finger_cnt);
            // touch_db->queue[0].continus_x = touch_db->queue[0].fingers[0].x;
            // touch_db->queue[0].continus_y = touch_db->queue[0].fingers[0].y;

            touch_db->continus_x_adj = 0;
            touch_db->continus_y_adj = 0;
        }
        else if (touch_db->data_cnt > 1) //맨왼손의 손가락에 변화가 있을때
        {
            uint16_t x_sum = 0;
            uint16_t y_sum = 0;
            for (uint8_t i = 0; i < touch_db->queue[0].finger_cnt; i++)
            {
                x_sum += touch_db->queue[0].fingers[i].x;
                y_sum += touch_db->queue[0].fingers[i].y;
            }

            //델타가 너무 크지 않다면(오입력)
            // if(abs(touch_db->queue[0].fingers[0].x - touch_db->queue[1].fingers[0].x) > 70)
            //손가락 갯수가 다르다면
            if (touch_db->queue[0].finger_cnt != touch_db->queue[1].finger_cnt)
            {
                // adj 값 보정
                ________DBG_20211119_mkb_finger_continus_test("adjusted!!!!!!!!!!!!!");
                touch_db->continus_x_adj = (x_sum / touch_db->queue[0].finger_cnt) - touch_db->queue[1].continus_x;
                touch_db->continus_y_adj = (y_sum / touch_db->queue[0].finger_cnt) - touch_db->queue[1].continus_y;
                // touch_db->continus_x_adj = touch_db->queue[0].fingers[0].x - touch_db->queue[1].continus_x;
                // touch_db->continus_y_adj = touch_db->queue[0].fingers[0].y - touch_db->queue[1].continus_y;
            }

            //보정된값 적용
            touch_db->queue[0].continus_x = (x_sum / touch_db->queue[0].finger_cnt) - touch_db->continus_x_adj;
            touch_db->queue[0].continus_y = (y_sum / touch_db->queue[0].finger_cnt) - touch_db->continus_y_adj;
            // touch_db->queue[0].continus_x = touch_db->queue[0].fingers[0].x - touch_db->continus_x_adj;
            // touch_db->queue[0].continus_y = touch_db->queue[0].fingers[0].y - touch_db->continus_y_adj;
        }

        ________DBG_20211119_mkb_finger_continus_test("leftest:%04d, adj:%04d conti:%04d q[0][%04d][%04d][%04d][%04d]", touch_db->queue[0].fingers[0].x, touch_db->continus_x_adj, touch_db->queue[0].continus_x,
                                                      touch_db->queue[0].fingers[0].x, touch_db->queue[0].fingers[1].x, touch_db->queue[0].fingers[2].x, touch_db->queue[0].fingers[3].x);
    }

    { // area size 값 연산하기
        if (touch_db->queue[0].finger_cnt >= 2)
        {
            touch_db->queue[0].area_size = sqrt(
                pow(touch_db->queue[0].fingers[0].x - touch_db->queue[0].fingers[touch_db->queue[0].finger_cnt - 1].x, 2) +
                pow(touch_db->queue[0].fingers[0].y - touch_db->queue[0].fingers[touch_db->queue[0].finger_cnt - 1].y, 2));
        }
        else
        {
            touch_db->queue[0].area_size = 0;
        }
        ________DBG_20211119_mkb_finger_areasize_test(" area-sise:%04d", touch_db->queue[0].area_size);
    }

    { // delta time, deltaxy, delta area값들 밀어넣기
        if (touch_db->data_cnt <= 1)
        {
            touch_db->queue[0].delta_time = 0;
            touch_db->queue[0].delta_x = 0;
            touch_db->queue[0].delta_y = 0;
        }
        else if (touch_db->data_cnt > 1)
        {
            touch_db->queue[0].delta_time = touch_db->queue[0].time - touch_db->queue[1].time;
            touch_db->queue[0].delta_x = touch_db->queue[0].continus_x - touch_db->queue[1].continus_x;
            touch_db->queue[0].delta_y = touch_db->queue[0].continus_y - touch_db->queue[1].continus_y;

            ________DBG_20211119_mkb_finger_deltas_test("dTime:%06d, dX:%04d, dY:%04d, dArea:%04d, dD:%04d, dir:%04d",
                                                        touch_db->queue[0].delta_time,
                                                        touch_db->queue[0].delta_x,
                                                        touch_db->queue[0].delta_y,
                                                        touch_db->queue[0].delta_area,
                                                        touch_db->queue[0].delta_dist,
                                                        touch_db->queue[0].delta_dir);
        }
    }

    { // delta size
        if (touch_db->data_cnt <= 1 || touch_db->queue[0].finger_cnt != touch_db->queue[1].finger_cnt)
        {
            touch_db->queue[0].delta_area = 0;
        }
        else
        {

            touch_db->queue[0].delta_area = touch_db->queue[0].area_size - touch_db->queue[1].area_size;
        }
    }
    ________DBG_20220314_deltaarea_finger_sumy("deltasize:%3d,delta_area:%3d ", m_touch_db.queue[0].area_size, m_touch_db.queue[0].delta_area);

    { // delta_dist 와 delta_dir 값구한다.  4개부터 안정적인 값이 들어오므로 가능하면 4개의 노드로  평균을 낸다
        if (touch_db->data_cnt <= 1)
        {
            touch_db->queue[0].delta_dist = 0;
            touch_db->queue[0].delta_dir = DIR_NONE;
        }
        else if (touch_db->data_cnt > 1)
        {
            //최근 4개값의 평균으로 쓰기
            int16_t sumof_x = 0;
            int16_t sumof_y = 0;
            uint8_t avg_data_cnt = 0;
            double sumof_delta = 0;
            uint16_t sumof_tick = 0;

            //큐의 0부터 3까지 deltax와 deltay를 합침
            for (avg_data_cnt = 0; (avg_data_cnt < touch_db->data_cnt) && (avg_data_cnt < 4); avg_data_cnt++)
            {
                sumof_x += touch_db->queue[avg_data_cnt].delta_x;
                sumof_y += touch_db->queue[avg_data_cnt].delta_y;
                sumof_tick += touch_db->queue[avg_data_cnt].delta_time;
            }
            sumof_delta = sqrt(pow(sumof_x, 2) + pow(sumof_y, 2));

            // delta_dist 쓰기
            touch_db->queue[0].delta_dist = sumof_delta / (double)avg_data_cnt;

            // delta_dir 쓰기
            if (abs(sumof_x) >= 3 || abs(sumof_y) >= 3 || abs(sumof_x) + abs(sumof_y) >= 5 || touch_db->queue[0].delta_dist >= 2)
            {
                int16_t angle = (atan2(sumof_y, sumof_x) * (-57.2957));
                if (angle < 0)
                {
                    angle = 360 + angle;
                }

                int8_t dir = ((angle - 23 + 45) % (360)) / 45;
                touch_db->queue[0].delta_dir = dir;
            }
            else
            {
                touch_db->queue[0].delta_dir = DIR_NONE;
            }

            if (touch_db->queue[0].delta_dir == DIR_RIGHT)
            {
                ________DBG_20211119_mkb_finger_angle_test(" ->");
            }
            else if (touch_db->queue[0].delta_dir == DIR_RIGHT_UP)
            {
                ________DBG_20211119_mkb_finger_angle_test(" />");
            }
            else if (touch_db->queue[0].delta_dir == DIR_UP)
            {
                ________DBG_20211119_mkb_finger_angle_test(" A  ");
            }
            else if (touch_db->queue[0].delta_dir == DIR_LEFT_UP)
            {
                ________DBG_20211119_mkb_finger_angle_test("<\\  ");
            }
            else if (touch_db->queue[0].delta_dir == DIR_LEFT)
            {
                ________DBG_20211119_mkb_finger_angle_test("<-  ");
            }
            else if (touch_db->queue[0].delta_dir == DIR_LEFT_DOWN)
            {
                ________DBG_20211119_mkb_finger_angle_test("</  ");
            }
            else if (touch_db->queue[0].delta_dir == DIR_DOWN)
            {
                ________DBG_20211119_mkb_finger_angle_test(" V  ");
            }
            else if (touch_db->queue[0].delta_dir == DIR_RIGHT_DOWN)
            {
                ________DBG_20211119_mkb_finger_angle_test(" \\> ");
            }
            else if (touch_db->queue[0].delta_dir == DIR_NONE)
            {
                ________DBG_20211119_mkb_finger_angle_test(" X ");
            }
        }
    }

    { // start point, end point 만들기
        if (touch_db->data_cnt == 1)
        {
            touch_db->start_x = touch_db->queue[0].continus_x;
            touch_db->start_y = touch_db->queue[0].continus_y;
            touch_db->start_time = touch_db->queue[0].time;
        }

        if (touch_db->data_cnt > 1)
        {
            touch_db->end_x = touch_db->queue[0].continus_x;
            touch_db->end_y = touch_db->queue[0].continus_y;
            touch_db->end_time = touch_db->queue[0].time;
        }
    }

    {
        // 보정된 compensate delta
        const uint8_t target_cnt = 12;
        uint8_t cnt = 0;

        int16_t fast_sum = 0;
        bool is_fast = false;

        uint8_t gstr_sum = 0;
        bool is_gstr = false;

        for (cnt = 0; (cnt < touch_db->data_cnt) && (cnt < target_cnt); cnt++)
        {
            if (abs(touch_db->queue[cnt].delta_dist) > 0)
            {
                fast_sum += abs(touch_db->queue[cnt].delta_dist);
            }
            if ((touch_db->queue[cnt].gesture.type == PIXART_GSTR_TYPE_ONE_MOVE || touch_db->queue[cnt].gesture.type == PIXART_GSTR_TYPE_ONE_TAP_AND_DRAG))
            {
                gstr_sum++;
            }
        }
        cnt--;

        is_fast = (cnt <= fast_sum) ? true : false;     //속도 평균이 1 이상 (소숫점필요없는)
        is_gstr = (cnt / 3 <= gstr_sum) ? true : false; //큐중에 상당수가 1점제스처

        if (is_fast == false)
        {
            const uint8_t slow_target_cnt = 8;
            uint8_t slow_cnt_x = 0;
            uint8_t slow_cnt_y = 0;

            int16_t slow_move_sum_x = 0;
            int16_t slow_move_sum_y = 0;

            uint8_t random_x = 0;
            uint8_t random_y = 0;

            //샘플갯수만큼 돌면서 더한다. delta_xy는 continus_xy 값을 사용했음
            for (uint8_t i = 0; (i < slow_target_cnt) && (i < touch_db->data_cnt); i++)
            {
                if ((touch_db->queue[i].gesture.type == PIXART_GSTR_TYPE_ONE_MOVE || touch_db->queue[i].gesture.type == PIXART_GSTR_TYPE_ONE_TAP_AND_DRAG))
                {
                    // slow_move_sum_x += (touch_db->queue[i].gesture.x) / abs(touch_db->queue[i].gesture.x);
                }
                else
                {
                    // slow_move_sum_x += (touch_db->queue[i].delta_x) / abs(touch_db->queue[i].delta_x);
                }
                slow_move_sum_x += (touch_db->queue[i].delta_x) / abs(touch_db->queue[i].delta_x);

                slow_cnt_x++;
            }

            for (uint8_t i = 0; (i < slow_target_cnt) && (i < touch_db->data_cnt); i++)
            {
                if ((touch_db->queue[i].gesture.type == PIXART_GSTR_TYPE_ONE_MOVE || touch_db->queue[i].gesture.type == PIXART_GSTR_TYPE_ONE_TAP_AND_DRAG))
                {
                    // slow_move_sum_y += (touch_db->queue[i].gesture.y) / abs(touch_db->queue[i].gesture.y);
                }
                else
                {
                    // slow_move_sum_y += (touch_db->queue[i].delta_y) / abs(touch_db->queue[i].delta_y);
                }
                slow_move_sum_y += (touch_db->queue[i].delta_y) / abs(touch_db->queue[i].delta_y);

                slow_cnt_y++;
            }

            //________LOG_20220225_pointing_speed_xy("[sum:%d] [rawdelta:%d] [delta:%d] [slowcnt:%d]", slow_move_sum_y, touch_db->queue[1].fingers[0].y - touch_db->queue[0].fingers[0].y, touch_db->queue[0].delta_y, slow_cnt_y);

            srand((int)(app_timer_cnt_get()));
            random_x = (rand() % slow_cnt_x);

            srand((int)(app_timer_cnt_get()));
            random_y = (rand() % slow_cnt_y);

            //최대 8개 값에서 확률적으로 좌표보냄
            if (random_x * 1.4 + 2 <= abs(slow_move_sum_x))
            {
                touch_db->queue[0].compensate_delta_x = slow_move_sum_x / abs(slow_move_sum_x); // touch_db->queue[0].delta_x / abs(touch_db->queue[0].delta_x);
                ________LOG_20220225_pointing_speed_xy("[%02d][%02d], sum:%d<cnt:%02d, SLOW[O] GSTR[ ] CM_X[O]         rand:%d<sum:%d:",
                                                       touch_db->queue[0].delta_x, touch_db->queue[0].delta_y, fast_sum, cnt, random_x, abs(slow_move_sum_x));
            }
            else
            {
                touch_db->queue[0].compensate_delta_x = 0;
                ________LOG_20220225_pointing_speed_xy("[%02d][%02d], sum:%d<cnt:%02d, SLOW[O] GSTR[ ] CM_X[-]         rand:%d<sum:%d:",
                                                       touch_db->queue[0].delta_x, touch_db->queue[0].delta_y, fast_sum, cnt, random_x, abs(slow_move_sum_x));
            }

            if (random_y * 1.4 + 2 <= abs(slow_move_sum_y))
            {
                touch_db->queue[0].compensate_delta_y = slow_move_sum_y / abs(slow_move_sum_y); // touch_db->queue[0].delta_y / abs(touch_db->queue[0].delta_y);
                ________LOG_20220225_pointing_speed_xy("[%02d][%02d], sum:%d<cnt:%02d, SLOW[O] GSTR[ ]         CM_Y[O] rand:%d<sum:%d:",
                                                       touch_db->queue[0].delta_x, touch_db->queue[0].delta_y, fast_sum, cnt, random_x, abs(slow_move_sum_x));
            }
            else
            {
                touch_db->queue[0].compensate_delta_y = 0;
                ________LOG_20220225_pointing_speed_xy("[%02d][%02d], sum:%d<cnt:%02d, SLOW[O] GSTR[ ]         CM_Y[-] rand:%d<sum:%d:",
                                                       touch_db->queue[0].delta_x, touch_db->queue[0].delta_y, fast_sum, cnt, random_x, abs(slow_move_sum_x));
            }
        }
        else if (is_fast == true) //속도가 빠를땐 제스처데이터, 혹은 raw값의 순차적 평균을 사용함
        {
            if (is_gstr)
            {
                ________LOG_20220225_pointing_speed_xy("[%02d][%02d], sum:%02d<cnt:%d, SLOW[-] GSTR[O] COMP[ ]",
                                                       touch_db->queue[0].delta_x, touch_db->queue[0].delta_y, fast_sum, cnt);
                touch_db->queue[0].compensate_delta_x = touch_db->queue[0].gesture.x;
                touch_db->queue[0].compensate_delta_y = touch_db->queue[0].gesture.y;
            }
            else
            {
                ________LOG_20220225_pointing_speed_xy("[%02d][%02d], sum:%02d<cnt:%d, SLOW[-] GSTR[-] COMP[ ]",
                                                       touch_db->queue[0].delta_x, touch_db->queue[0].delta_y, fast_sum, cnt);

                uint8_t stepper_arr[6] = {8, 2, 1, 1, 0, 0};
                uint16_t raw_avg_sample_cnt = 0;
                int16_t raw_average_x = 0;
                int16_t raw_average_y = 0;

                for (uint8_t i = 0; i < touch_db->data_cnt && i < 6; i++)
                {
                    if ((touch_db->queue[i].gesture.type == PIXART_GSTR_TYPE_ONE_MOVE || touch_db->queue[0].gesture.type == PIXART_GSTR_TYPE_ONE_TAP_AND_DRAG))
                    {
                        raw_average_x += touch_db->queue[i].gesture.x * stepper_arr[i];
                        raw_average_y += touch_db->queue[i].gesture.y * stepper_arr[i];
                    }
                    else
                    {
                        raw_average_x += touch_db->queue[i].delta_x * stepper_arr[i];
                        raw_average_y += touch_db->queue[i].delta_y * stepper_arr[i];
                    }

                    raw_avg_sample_cnt += stepper_arr[i];
                }
                touch_db->queue[0].compensate_delta_x = (raw_average_x * 1) / (raw_avg_sample_cnt);
                touch_db->queue[0].compensate_delta_y = (raw_average_y * 1) / (raw_avg_sample_cnt);
            }
        }

        ________DBG_20220302_raw_vs_gstr_vs_comp("[move:%d]raw:%02d, gst(%d):%02d, comp:%02d", abs(touch_db->start_x - touch_db->queue[0].fingers[0].x),
                                                 abs(touch_db->queue[0].delta_x), abs(touch_db->queue[0].delta_y),
                                                 touch_db->queue[0].gesture.type,
                                                 abs(touch_db->queue[0].gesture.x) + abs(touch_db->queue[0].gesture.y),
                                                 abs(touch_db->queue[0].compensate_delta_x) + abs(touch_db->queue[0].compensate_delta_y)

        );
    }

    { // max touch cnt 제일 손가락이 많을때를 저장함
        if (touch_db->queue[0].finger_cnt > touch_db->queue[1].finger_cnt)
        {
            touch_db->max_touch_cnt = touch_db->queue[0].finger_cnt;
        }
    }

    { // finger cnt summary        //각 손가락의 존재횟수를 더한다.
        INCREASE(touch_db->fingers_cnts[touch_db->queue[0].finger_cnt], 255);
        if (touch_db->data_cnt >= TOUCH_DB_MAX)
        {
            DECREASE(touch_db->fingers_cnts[touch_db->queue[touch_db->data_cnt - 1].finger_cnt], 0);
        }
#ifdef ________DBG_20211119_finger_cnt_sprint
        {
            char logbuf[250];
            sprintf(logbuf, "cnt:%02d 0:%02d 1:%02d 2:%02d 3:%02d 4:%02d:  ",
                    touch_db->data_cnt,
                    touch_db->fingers_cnts[0], touch_db->fingers_cnts[1], touch_db->fingers_cnts[2], touch_db->fingers_cnts[3], touch_db->fingers_cnts[4]);

            for (uint8_t i = 0; i < touch_db->data_cnt; i++)
            {
                sprintf(logbuf, "%s|%d", logbuf, touch_db->queue[i].finger_cnt);
            }
            ________DBG_20211119_finger_cnt_sprint("%s", logbuf);
        }
#endif
    }

    { // direction cnts 방향의 횟수를 가지는 배열
        INCREASE(touch_db->directions_cnts[touch_db->queue[0].delta_dir], 255);
        if (touch_db->data_cnt >= TOUCH_DB_MAX)
        {
            DECREASE(touch_db->directions_cnts[touch_db->queue[touch_db->data_cnt - 1].delta_dir], 0);
        }

#ifdef ________DBG_20211119_directions_cnt_sprint
        {
            ________DBG_20211119_directions_cnt_sprint("cnt:[%02d] X[%02d] ->[%02d] />[%02d] A[%02d] <\\[%02d] <-[%02d] </[%02d] V[%02d] \\>[%02d]",
                                                       touch_db->data_cnt,
                                                       touch_db->directions_cnts[DIR_NONE],
                                                       touch_db->directions_cnts[DIR_RIGHT],
                                                       touch_db->directions_cnts[DIR_RIGHT_UP],
                                                       touch_db->directions_cnts[DIR_UP],
                                                       touch_db->directions_cnts[DIR_LEFT_UP],
                                                       touch_db->directions_cnts[DIR_LEFT],
                                                       touch_db->directions_cnts[DIR_LEFT_DOWN],
                                                       touch_db->directions_cnts[DIR_DOWN],
                                                       touch_db->directions_cnts[DIR_RIGHT_DOWN]);
        }
#endif
    }

    { // direction cnts_200ms 방향의 횟수를 가지는 배열을 최근 6개로 구한다.
        INCREASE(touch_db->directions_cnts_6_samples[touch_db->queue[0].delta_dir], 255);
        if (touch_db->data_cnt >= 6)
        {
            DECREASE(touch_db->directions_cnts_6_samples[touch_db->queue[6 - 1].delta_dir], 0);
        }
    }

    { // pinchs cnts 핀치의 방향의 갯수 저장하는 배열을 만든다.
        if (touch_db->queue[0].delta_area > 4)
        {
            INCREASE(touch_db->pinchs_cnts[PINCH_WIDE], 255);
        }
        else if (touch_db->queue[0].delta_area < -4)
        {
            INCREASE(touch_db->pinchs_cnts[PINCH_NARR], 255);
        }
        else
        {
            INCREASE(touch_db->pinchs_cnts[PINCH_NONE], 255);
        }

        if (touch_db->data_cnt >= TOUCH_DB_MAX)
        {
            if (touch_db->queue[touch_db->data_cnt - 1].delta_area > 4)
            {
                DECREASE(touch_db->pinchs_cnts[PINCH_WIDE], 0);
            }
            else if (touch_db->queue[touch_db->data_cnt - 1].delta_area < -4)
            {
                DECREASE(touch_db->pinchs_cnts[PINCH_NARR], 0);
            }
            else
            {
                DECREASE(touch_db->pinchs_cnts[PINCH_NONE], 0);
            }
        }

#ifdef ________DBG_20211119_pinch_cnt_sprint
        {
            ________DBG_20211119_pinch_cnt_sprint("cnt:%02d, NONE[%02d] WIDER[%02d] NARRO[%02d]",
                                                  touch_db->data_cnt,
                                                  touch_db->pinchs_cnts[PINCH_NONE],
                                                  touch_db->pinchs_cnts[PINCH_WIDE],
                                                  touch_db->pinchs_cnts[PINCH_NARR]);
        }
#endif
    }

    { // aamode 켜기 끄기 DTA기능 활성화 비활성화
        if (touch_db->data_cnt >= 1 && (touch_db->fingers_cnts[1] >= 5 || touch_db->fingers_cnts[2] >= 10 || touch_db->fingers_cnts[3] >= 10))
        {
            if ((touch_db->fingers_cnts[1] >= 10) || (touch_db->fingers_cnts[2] >= 10 && touch_db->queue[0].area_size < 550) || touch_db->fingers_cnts[3] >= 10 && touch_db->queue[0].area_size < 900)
            {
                ________DBG_20211119_mkb_touch_aamode_test("posx:%d aamode:%d ", touch_db->queue[0].fingers[0].x, mkb_db_aamode_get());
                if (mkb_db_aamode_get() == PIXART_AAMODE_TYPE_RIGHT)
                {
                    if (1280 < touch_db->start_x && 800 < touch_db->queue[0].fingers[0].x && touch_db->queue[0].fingers[0].x <= 1050)
                    {
                        if (drv_pixart_get_otherhand_flag() == false)
                        {
                            if (fgr0_is_aamode_all == false)
                            {
                                fgr0_is_aamode_all = true;
                                drv_pixart_set_aamode(PIXART_AAMODE_TYPE_ALL);
                                ________DBG_20211119_mkb_touch_aamode_test("set all");
                            }
                        }
                    }
                }

                else if (mkb_db_aamode_get() == PIXART_AAMODE_TYPE_LEFT)
                {
                    ________DBG_20211119_mkb_touch_aamode_test("step 0");

                    ________DBG_20211119_mkb_touch_aamode_test("step 1");

                    if ((touch_db->start_x < 1000) && (touch_db->queue[0].fingers[touch_db->queue[0].finger_cnt - 1].x >= 800))
                    {
                        if (drv_pixart_get_otherhand_flag() == false)
                        {
                            if (fgr0_is_aamode_all == false)
                            {
                                fgr0_is_aamode_all = true;
                                drv_pixart_set_aamode(PIXART_AAMODE_TYPE_ALL);
                                ________DBG_20211119_mkb_touch_aamode_test("set all");
                            }
                        }
                    }
                }
            }
        }
        if (fgr0_is_aamode_all == true)
        {
            if (touch_db->fingers_cnts[5] > 5 ||
                touch_db->queue[0].area_size > 1200 ||
                (touch_db->fingers_cnts[4] >= 15 && touch_db->queue[0].area_size > 900) ||
                (touch_db->queue[0].finger_cnt > 0 && touch_db->fingers_cnts[4] >= 15 && mkb_db_aamode_get() == PIXART_AAMODE_TYPE_RIGHT && touch_db->queue[0].fingers[0].x < 450) ||
                (touch_db->queue[0].finger_cnt > 0 && touch_db->fingers_cnts[4] >= 15 && mkb_db_aamode_get() == PIXART_AAMODE_TYPE_LEFT && touch_db->queue[0].fingers[touch_db->queue[0].finger_cnt - 1].x > 1450))
            {
                aamode_clear();
            }
        }

        ________LOG_20220216_fngr_2_xy_pinch("lastjob[%d][%d] pinch_sum:[%4d] deltaarea_sum:[%4d], step:[%4d]", last_job.active_fngr_job[0], last_job.active_fngr_job[1], get_is_pinch_sum(40), get_deltaarea_sum(40), gstr_step.fgr2_pinch_inout_step);
    }

    return NRF_SUCCESS;
}

#if 1 //타임 베이스드 블로킹 코드들
static bool is_safe_from_multttouch_up_ms(uint16_t ms)
{
    if (APP_TIMER_TICKS(ms) > app_timer_cnt_diff_compute(app_timer_cnt_get(), mkb_db_last_multitouch_time_get()))
    {
        return false;
    }
    else
    {
        return true;
    }
}

static bool is_safe_from_red_key_up_ms(uint16_t ms)
{
    if (APP_TIMER_TICKS(ms) > app_timer_cnt_diff_compute(app_timer_cnt_get(), mkb_db_red_click_up_time_get()))
    {
        return false;
    }
    else
    {
        return true;
    }
}

static bool is_safe_from_typing_ready_end_ms(uint16_t ms) //마지막 키입력 시간에 따라 탭 입력을 막는다.
{
    //차이가 600보다 작으면 : 최근에 실행된거니까 false

    if (APP_TIMER_TICKS(ms) > app_timer_cnt_diff_compute(app_timer_cnt_get(), mkb_db_last_typing_ready_time_get()))
    {
        return false;
    }
    else
    {

        return true;
    }
}

static bool is_safe_from_key_up_ms(uint16_t ms) //마지막 키입력 시간에 따라 탭 입력을 막는다.
{
    //차이가 600보다 작으면 : 최근에 실행된거니까 false

    if (APP_TIMER_TICKS(ms) > app_timer_cnt_diff_compute(app_timer_cnt_get(), mkb_db_last_key_up_time_get()))
    {
        ________DBG_20211028_block_time_gstr("block");
        return false;
    }
    else
    {
        ________DBG_20211028_block_time_gstr("noblock");

        return true;
    }
}

static bool is_safe_from_gstr_up_ms(uint16_t ms) //마지막 제스처 입력 시간에 따라 멀티 탭 입력을 막는다.
{
    if (APP_TIMER_TICKS(ms) > app_timer_cnt_diff_compute(app_timer_cnt_get(), mkb_db_last_gstr_time_get()))
    {
        return false;
    }
    else
    {
        return true;
    }
}

static bool is_safe_from_tap_up_ms(uint16_t ms) //마지막 제스처 입력 시간에 따라 멀티 탭 입력을 막는다.
{
    if (APP_TIMER_TICKS(ms) > app_timer_cnt_diff_compute(app_timer_cnt_get(), mkb_db_last_tap_time_get()))
    {
        return false;
    }
    else
    {
        return true;
    }
}
void aamode_clear()
{
    if (fgr0_is_aamode_all == true)
    {
        ________DBG_20211028_dynamic_aamode("aamode off");
        drv_pixart_set_aamode_all(false);
        fgr0_is_aamode_all = false;
    }
}
#endif

// 1000배율 값을 리턴함. 샘플 갯수값에 8ms곱하기.
static uint16_t get_speed_sum_kilo(uint8_t sample_cnt)
{
    int16_t sumof_x = 0;
    int16_t sumof_y = 0;
    uint8_t avg_data_cnt = 0;
    uint16_t sumof_tick = 0;
    double sumof_delta = 0;

    for (avg_data_cnt = 0; (avg_data_cnt < m_touch_db.data_cnt) && (avg_data_cnt < sample_cnt); avg_data_cnt++)
    {
        sumof_x += m_touch_db.queue[avg_data_cnt].delta_x;
        sumof_y += m_touch_db.queue[avg_data_cnt].delta_y;
        sumof_tick += m_touch_db.queue[avg_data_cnt].delta_time;
    }
    sumof_delta = sqrt(pow(sumof_x, 2) + pow(sumof_y, 2));

    // delta_dist 쓰기
    return (uint16_t)(sumof_delta / (double)avg_data_cnt * 1000);
}

static void get_dir_sum(uint8_t cnqueue_cntt, direction_t dir)
{
}

static void get_fingers_sum(uint8_t queue_cnt, uint8_t finger_cnt)
{
}

// narrow갯수 vs wider 갯수
static int16_t get_is_pinch_sum(uint8_t sample_cnt)
{
    int16_t sumof_pinch = 0;

    uint8_t avg_data_cnt = 0;

    for (avg_data_cnt = 0; (avg_data_cnt < m_touch_db.data_cnt) && (avg_data_cnt < sample_cnt); avg_data_cnt++)
    {
        if (m_touch_db.queue[avg_data_cnt].finger_cnt >= 2)
        {
            if (m_touch_db.queue[avg_data_cnt].delta_area >= 1)
            {
                sumof_pinch++;
            }
            else if (m_touch_db.queue[avg_data_cnt].delta_area <= -1)
            {
                sumof_pinch--;
            }
        }
    }
    // delta_dist 쓰기
    return sumof_pinch;
}

static uint8_t get_qcnt_per(uint8_t value)
{
    double result = (double)value * 100.0 / (double)m_touch_db.data_cnt;
    return (uint8_t)result;
}

static uint8_t get_per_qcnt(uint8_t percent)
{
    double result = (percent / 100.0) * (double)m_touch_db.data_cnt;
    return (uint8_t)result;
}

// delta_area 의 합
static int16_t get_deltaarea_sum(uint8_t sample_cnt)
{
    int16_t sumof_area = 0;
    uint8_t avg_data_cnt = 0;

    for (avg_data_cnt = 0; (avg_data_cnt < m_touch_db.data_cnt) && (avg_data_cnt < sample_cnt); avg_data_cnt++)
    {
        if (m_touch_db.queue[avg_data_cnt].finger_cnt >= 2)
        {
            sumof_area += m_touch_db.queue[avg_data_cnt].delta_area;
        }
    }
    // delta_dist 쓰기
    return sumof_area;
}

static int16_t get_deltaarea_finger_sum(uint8_t finger_cnt, uint8_t sample_cnt)
{
    int16_t sumof_area = 0;
    uint8_t avg_data_cnt = 0;

    for (avg_data_cnt = 0; (avg_data_cnt < m_touch_db.data_cnt) && (avg_data_cnt < sample_cnt); avg_data_cnt++)
    {
        if (m_touch_db.queue[avg_data_cnt].finger_cnt == finger_cnt)
        {
            sumof_area += m_touch_db.queue[avg_data_cnt].delta_area;
            ________DBG_20220314_deltaarea_finger_summ_raw("(%3d), ", m_touch_db.queue[avg_data_cnt].delta_area);
        }
    }
    ________DBG_20220314_deltaarea_finger_summ_raw("\n");
    // delta_dist 쓰기
    return sumof_area;
}

static void last_job_active(fgr_job_t fgr_job)
{
    // fngr job, timestamp, cnt_static, cnt_volatile, threshold
    last_job.active_time_stamp = app_timer_cnt_get();

    if (fgr_job >= IDLE_2_TAP)
    {
        mkb_db_last_multitouch_time_set(app_timer_cnt_get());
    }
    if (fgr_job == IDLE_1_TAP || fgr_job == IDLE_2_TAP || fgr_job == IDLE_3_TAP || fgr_job == IDLE_4_TAP)
    {
        mkb_db_last_tap_time_set(app_timer_cnt_get());
    }
    if (fgr_job == IDLE_2_PINCH || fgr_job == IDLE_2_X_SWIPE || fgr_job == IDLE_2_X_SWIPE)
    {
    }

    // 새로운 job일때
    if (last_job.active_fngr_job[0] != fgr_job) // detect를 먼저하니 이건 쓸일이 없긴하다.
    {
        last_job.active_fngr_job[1] = last_job.active_fngr_job[0];
        last_job.active_fngr_job[0] = fgr_job;
        if (fgr_job != IDLE_0_FLOATING)
        {
            last_job.active_cnt_static = 1; // float 으로 리셋되지않는다.
        }
        last_job.active_cnt_volatile = 1; // float 으로 리셋된다.
        last_job.dist_threshold_x = 0;
        last_job.dist_threshold_y = 0;
    }
    else //연속된 job일때
    {
        INCREASE(last_job.active_cnt_static, UINT8_MAX - 1);
        INCREASE(last_job.active_cnt_volatile, UINT8_MAX - 1);
    }
}

static void last_job_detect(fgr_job_t fgr_job)
{
    // fngr job, timestamp, cnt_static, cnt_volatile, threshold
    last_job.detect_time_stamp = app_timer_cnt_get();
    // 새로운 job일때
    if (last_job.detect_fngr_job[0] != fgr_job) // detect를 먼저하니 이건 쓸일이 없긴하다.
    {
        last_job.detect_fngr_job[1] = last_job.detect_fngr_job[0];
        last_job.detect_fngr_job[0] = fgr_job;
        if (fgr_job != IDLE_0_FLOATING)
        {
            last_job.detect_cnt_static = 1; // float 으로 리셋되지않는다.
        }
        last_job.detect_cnt_volatile = 1; // float 으로 리셋된다.
        last_job.dist_threshold_x = 0;
        last_job.dist_threshold_y = 0;
    }
    else //연속된 job일때
    {
        INCREASE(last_job.detect_cnt_static, UINT8_MAX - 1);
        INCREASE(last_job.detect_cnt_volatile, UINT8_MAX - 1);
    }
}

static void last_job_fngr0_clear()
{
    last_job_active(IDLE_0_FLOATING);
    last_job_detect(IDLE_0_FLOATING);
}

// idle, swipe, tap 등 손가락이 떨어지고 나서 동작을 판단
static void idle_process_timer_handler()
{
    //
    /*
    8ms 후에 동작한다
    120ms후에도 입력이 없으면?
    탭동작
    마지막 터치데이터를
    */
    // last_job_active(IDLE_0_FLOATING);

#ifdef ________DBG_20220314_idle_module_refactory

    ________DBG_20220314_idle_module_refactory("idle_handler");
    // active

    //바로 일어날일
    // 80ms가 지나고 일어날일
    // 100ms가 지나고 일어날일
    // 무언가 동작하면 db를 clean한다.
    if (last_job.active_fngr_job[0] == IDLE_0_FLOATING)
    {
    }
    else
    {
        mokibo_touch_data_t fake_touch_data;
        memset(&fake_touch_data, 0, sizeof(mokibo_touch_data_t));
        fake_touch_data.timestamp = app_timer_cnt_get();
        mkb_touchpad_read_process(&fake_touch_data);
    }

#else
    idle_process();   //탭 등의 idle제스처 이후에
    touch_db_clean(); ////삭제
#endif
}

// db 지움. aamode, tapped, needmodclear, needclickclear, dragging 손가락뗀후 58ms이후 실행됨.
static void touch_db_clean()
{
    touch_db_t *touch_db = &m_touch_db;

    // istaped 클리어함.
    if (touch_db->data_cnt >= 6 && touch_db->data_cnt <= 18)
    {
        uint8_t gstr_sum;
        ________DBG_20210810_redbtn_click_down("datacnt:%d, gstr_sum:%d", touch_db->data_cnt, gstr_sum);
        if (gstr_sum == 0)
        {
            if (mkb_db_system_state_is_touch())
            {
                gstr_step.fgr0_is_tapped = true;
            }
        }
        else
        {
            gstr_step.fgr0_is_tapped = false;
        }
    }
    else
    {
        gstr_step.fgr0_is_tapped = false;
    }

    { //드래그 했다면
        if (drag_step_temp == 2)
        {
            ________DBG_20211201_idle_1_tap_flag("drag click up");

            mkb_db_is_tap_click_down_set(false);
            if (mkb_db_is_red_click_down_get() == false && mkb_db_is_tap_click_down_get() == false)
            {
                mkb_event_send(EVT_BUTTON_CLICK_LEFT, false);
            }
            drag_time_temp = 0;
            drag_step_temp = 0;
            ________DBG_20211201_idle_1_tap_flag("drag_step: 2->0: (%d)", drag_step_temp);
        }
        else if (drag_step_temp == 1)
        {
            // drag_time_temp = 0;
        }
    }

    { // alt tap 등의 mod키 사용하는 제스처
        if (gstr_step.fgr0_is_need_mod_clear == true)
        {

            uint8_t kbd_buff[8] = {
                0,
            };
            memset(kbd_buff, 0, 8 * sizeof(uint8_t));
            mkb_event_send_ms(30, EVT_HID_INPUT_KEYBOARD, kbd_buff, 8);
            mkb_event_send_ms(25, EVT_KEY_UP, HID_KEY_L_ALT);
            mkb_event_send_ms(26, EVT_KEY_UP, HID_KEY_L_CTRL);
            mkb_event_send_ms(27, EVT_KEY_UP, HID_KEY_L_GUI);
            mkb_event_send_ms(27, EVT_KEY_UP, HID_KEY_L_SHFT);
            ________DBG_20211201_idle_1_tap_flag("mgstr click up");
            mkb_event_send(EVT_BUTTON_CLICK_LEFT, false);
            mkb_event_send(EVT_BUTTON_CLICK_RIGHT, false);

            gstr_step.fgr0_is_need_mod_clear = false;

            ________DBG_tmp_3scroll_test("mod clear false");
        }
    }

    { // 드래그 등 클릭키 사용하는 제스처 초기화
        if (gstr_step.fgr0_is_need_click_clear == true)
        {

            ________DBG_20211201_idle_1_tap_flag("gstr click up");
            mkb_db_is_tap_click_down_set(false);
            if (mkb_db_is_red_click_down_get() == false && mkb_db_is_tap_click_down_get() == false)
            {
                mkb_event_send(EVT_BUTTON_CLICK_LEFT, false);
            }
            mkb_event_send(EVT_BUTTON_CLICK_RIGHT, false);
            gstr_step.fgr0_is_need_click_clear = false;
        }
    }

    // gstr_step clear
    {
        memset(&gstr_step, 0, sizeof(mkb_touchpad_pixart_t));
    }

    // touch db clear. detect idle 이 실행된 이후이므로 깔끔하게 날린다. end 는 다음 첫 enqueue 에서 사용하니 넣는다.
    {
        // memset(&(touch_db->queue[0]), 0, sizeof(touch_db->queue[0])*TOUCH_DB_MAX);
        int16_t end_x_temp = touch_db->end_x;
        int16_t end_y_temp = touch_db->end_y;
        int16_t end_time_temp = touch_db->end_time;
        memset(touch_db, 0, sizeof(touch_db_t));
        touch_db->end_x = end_x_temp;
        touch_db->end_y = end_y_temp;
        touch_db->end_time = end_time_temp;
    }

    {
        last_job_fngr0_clear();
    }
    ________DBG_20211119_mkb_touchdb_clean("touch_db_clean(): fgrcnt:[%d], gstr:[%d], lastjob:[%d<-%d]", m_touch_db.queue[0].finger_cnt, m_touch_db.queue[0].gesture.type, last_job.active_fngr_job[0], last_job.active_fngr_job[1]);
}

//손가락이 떨어지면 touch_db 큐를 해석하여 제스처를 보낸다.

#ifdef ________DBG_20220314_idle_module_refactory
static void idle_process()
{

    if (m_touch_db.max_touch_cnt == 2)
    {
        ________DBG_20211201_idle_process_datas_csv("%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d\n",
                                                    m_touch_db.data_cnt,
                                                    mkb_util_timer_ms(app_timer_cnt_diff_compute(m_touch_db.end_time, m_touch_db.start_time)),
                                                    m_touch_db.end_x - m_touch_db.start_x, m_touch_db.end_y - m_touch_db.start_y,
                                                    get_deltaarea_finger_sum(2, 40),
                                                    get_deltaarea_finger_sum(3, 40),
                                                    get_deltaarea_finger_sum(4, 40),
                                                    (m_touch_db.fingers_cnts[0]), get_qcnt_per(m_touch_db.fingers_cnts[1]), get_qcnt_per(m_touch_db.fingers_cnts[2]), get_qcnt_per(m_touch_db.fingers_cnts[3]), get_qcnt_per(m_touch_db.fingers_cnts[4]), get_qcnt_per(m_touch_db.fingers_cnts[5]),
                                                    get_qcnt_per(m_touch_db.pinchs_cnts[PINCH_NONE]), get_qcnt_per(m_touch_db.pinchs_cnts[PINCH_NARR]), get_qcnt_per(m_touch_db.pinchs_cnts[PINCH_WIDE]),
                                                    get_qcnt_per(m_touch_db.directions_cnts[DIR_UP]), get_qcnt_per(m_touch_db.directions_cnts[DIR_RIGHT_UP]), get_qcnt_per(m_touch_db.directions_cnts[DIR_RIGHT]), get_qcnt_per(m_touch_db.directions_cnts[DIR_RIGHT_DOWN]),
                                                    get_qcnt_per(m_touch_db.directions_cnts[DIR_DOWN]), get_qcnt_per(m_touch_db.directions_cnts[DIR_LEFT_DOWN]), get_qcnt_per(m_touch_db.directions_cnts[DIR_LEFT]), get_qcnt_per(m_touch_db.directions_cnts[DIR_LEFT_UP]), get_qcnt_per(m_touch_db.directions_cnts[DIR_NONE]));
    }

    ________DBG_20211201_idle_process_datas("******************************************");
    ________DBG_20211201_idle_process_datas("cnts:[%d], gstr:[%2d], time:[%4dms], dxy:[%3d,%3d], darea2[%4d],darea3[%4d],darea4[%4d]",
                                            m_touch_db.data_cnt,
                                            m_touch_db.founded_gesture,
                                            mkb_util_timer_ms(app_timer_cnt_diff_compute(m_touch_db.end_time, m_touch_db.start_time)),
                                            m_touch_db.end_x - m_touch_db.start_x, m_touch_db.end_y - m_touch_db.start_y,
                                            get_deltaarea_finger_sum(2, 40),
                                            get_deltaarea_finger_sum(3, 40),
                                            get_deltaarea_finger_sum(4, 40));

    ________DBG_20211201_idle_process_datas("keyup:[%d], gstrup:[%d], multup:[%d] tapup:[%d]",
                                            is_safe_from_key_up_ms(400), is_safe_from_gstr_up_ms(400), is_safe_from_multttouch_up_ms(400), is_safe_from_tap_up_ms(400));

    ________DBG_20211201_idle_process_datas("fngr[0:%d, 1:%d, 2:%d, 3:%d, 4:%d, 5:%d]",
                                            m_touch_db.fingers_cnts[0], m_touch_db.fingers_cnts[1], m_touch_db.fingers_cnts[2], m_touch_db.fingers_cnts[3], m_touch_db.fingers_cnts[4], m_touch_db.fingers_cnts[5]);
    ________DBG_20211201_idle_process_datas("pinch[X:%d, N:%d, W:%d]",
                                            m_touch_db.pinchs_cnts[PINCH_NONE], m_touch_db.pinchs_cnts[PINCH_NARR], m_touch_db.pinchs_cnts[PINCH_WIDE]);
    ________DBG_20211201_idle_process_datas("dir[X:%d, U:%d, RU:%d, R:%d, RD:%d, D:%d, LD:%d, L:%d, LU:%d]", m_touch_db.directions_cnts[DIR_NONE],
                                            m_touch_db.directions_cnts[DIR_UP], m_touch_db.directions_cnts[DIR_RIGHT_UP], m_touch_db.directions_cnts[DIR_RIGHT], m_touch_db.directions_cnts[DIR_RIGHT_DOWN],
                                            m_touch_db.directions_cnts[DIR_DOWN], m_touch_db.directions_cnts[DIR_LEFT_DOWN], m_touch_db.directions_cnts[DIR_LEFT], m_touch_db.directions_cnts[DIR_LEFT_UP]);
    ________DBG_20211201_idle_process_datas("******************************************");

    if (search_idle_3_up_swipe())
    {
        ________DBG_20211201_idle_detect("IDLE_3_UP");
        detect_idle_3_up_swipe();
    }
    else if (search_idle_3_down_swipe())
    {
        ________DBG_20211201_idle_detect("IDLE_3_DOWN");
        detect_idle_3_down_swipe();
    }
    else if (search_idle_3_x_swipe())
    {
        ________DBG_20211201_idle_detect("IDLE_3_X");
        detect_idle_3_x_swipe();
    }
    else if (search_idle_4_up_swipe())
    {
        ________DBG_20211201_idle_detect("IDLE_4_UP");
        detect_idle_4_up_swipe();
    }
    else if (search_idle_4_down_swipe())
    {
        ________DBG_20211201_idle_detect("IDLE_4_DOWN");
        detect_idle_4_down_swipe();
    }
    else if (search_idle_4_x_swipe())
    {
        ________DBG_20211201_idle_detect("IDLE_4_X");
        detect_idle_4_x_swipe();
    }
    else if (search_idle_2_pinch())
    {
        ________DBG_20211201_idle_detect("IDLE_2_PINCH");
        detect_idle_2_pinch();
    }
    else if (search_idle_2_x_swipe())
    {
        ________DBG_20211201_idle_detect("IDLE_2_X_SWIPE");
        detect_idle_2_x_swipe();
    }
    else if (search_idle_4_tap())
    {
        ________DBG_20211201_idle_detect("IDLE_4_TAP");
        detect_idle_4_tap();
    }
    else if (search_idle_3_tap())
    {
        ________DBG_20211201_idle_detect("IDLE_3_TAP");
        detect_idle_3_tap();
    }
    else if (search_idle_2_tap())
    {
        ________DBG_20211201_idle_detect("IDLE_2_TAP");
        detect_idle_2_tap();
    }
    else if (search_idle_1_tap())
    {
        ________DBG_20211201_idle_detect("IDLE_1_TAP");
        detect_idle_1_tap();
    }
    else if (search_idle_1_double_tap())
    {
        ________DBG_20211201_idle_detect("IDLE_1_DOUBLE_TAP");
        detect_idle_1_double_tap();
    }
    else
    {
    }
}

#else

static void idle_process()
{
    ________DBG_20211119_mkb_touch_detect_idle_timeout_handler("timeouthandler");

    pixart_gstr_type_t founded_gstr;

    direction_t dist = m_touch_db.queue[0].delta_dist;
    uint16_t delta_time = app_timer_cnt_diff_compute(m_touch_db.end_time, m_touch_db.start_time);
    int16_t delta_x = m_touch_db.end_x - m_touch_db.start_x;
    int16_t delta_y = m_touch_db.start_y - m_touch_db.end_y;
    uint8_t max_touch_cnt = m_touch_db.max_touch_cnt;
    int8_t zerocnt = 0;
    int16_t delta_area_sum = 0;

    for (int i = 0; i < m_touch_db.data_cnt; i++)
    {
        if (m_touch_db.queue[i].gesture.type == 0)
        {
            zerocnt++;
        }
        delta_area_sum += m_touch_db.queue[i].delta_area;
    }

    if (zerocnt > 28)
    {
        for (int i = 0; i < m_touch_db.data_cnt; i++)
        {
            if (m_touch_db.queue[i].gesture.type == PIXART_GSTR_TYPE_THREE_UP)
            {
                founded_gstr = PIXART_GSTR_TYPE_THREE_UP;
                break;
            }
            else if (m_touch_db.queue[i].gesture.type == PIXART_GSTR_TYPE_THREE_DOWN)
            {
                founded_gstr = PIXART_GSTR_TYPE_THREE_DOWN;
                break;
            }
            else if (m_touch_db.queue[i].gesture.type == PIXART_GSTR_TYPE_FOUR_UP)
            {
                founded_gstr = PIXART_GSTR_TYPE_FOUR_UP;
                break;
            }
            else if (m_touch_db.queue[i].gesture.type == PIXART_GSTR_TYPE_FOUR_DOWN)
            {
                founded_gstr = PIXART_GSTR_TYPE_FOUR_DOWN;
                break;
            }
            else if (m_touch_db.queue[i].gesture.type == PIXART_GSTR_TYPE_FOUR_LEFT)
            {
                founded_gstr = PIXART_GSTR_TYPE_FOUR_LEFT;
                break;
            }
            else if (m_touch_db.queue[i].gesture.type == PIXART_GSTR_TYPE_FOUR_RIGHT)
            {
                founded_gstr = PIXART_GSTR_TYPE_FOUR_RIGHT;
                break;
            }
            else if (m_touch_db.queue[i].gesture.type == PIXART_GSTR_TYPE_TWO_PINCH)
            {
                founded_gstr = PIXART_GSTR_TYPE_TWO_PINCH;
                break;
            }
        }
    }

    // data_cnt
    // finger_cnts[5]
    // pinch_cnts[3]
    // directions_cnts[4]
    // delta_time ms
    //
    //
    //

    ________DBG_20211201_idle_process_datas("cnts:[%d], gstr:[%02d], time:[%04d], dxy:[%03d,%03d], delta_area_sum:[%04d], keyup:[%d], gstrup:[%d], tapup:[%d]",
                                            m_touch_db.data_cnt,
                                            founded_gstr,
                                            delta_time,
                                            delta_x, delta_y,
                                            delta_area_sum,
                                            is_safe_from_key_up_ms(400),
                                            is_safe_from_gstr_up_ms(400),
                                            is_safe_from_gstr_up_ms(400));

    ________DBG_20211201_idle_process_datas("fngr[0:%d, 1:%d, 2:%d, 3:%d, 4:%d, 5:%d]",
                                            m_touch_db.fingers_cnts[0], m_touch_db.fingers_cnts[1], m_touch_db.fingers_cnts[2], m_touch_db.fingers_cnts[3], m_touch_db.fingers_cnts[4], m_touch_db.fingers_cnts[5]);
    ________DBG_20211201_idle_process_datas("pinch[X:%d, N:%d, W:%d] dir[X:%d, U:%d, D:%d, L:%d, R:%d]",
                                            m_touch_db.pinchs_cnts[PINCH_NONE], m_touch_db.pinchs_cnts[PINCH_NARR], m_touch_db.pinchs_cnts[PINCH_WIDE],
                                            m_touch_db.directions_cnts[DIR_NONE], m_touch_db.directions_cnts[DIR_UP], m_touch_db.directions_cnts[DIR_DOWN], m_touch_db.directions_cnts[DIR_LEFT], m_touch_db.directions_cnts[DIR_RIGHT]);

    //터치갯수등 오입력 걸러내기
    if (m_touch_db.data_cnt < 2)
    {
        ________DBG_20211119_mkb_touch_detect_idle("TOO LITTLE DATACNT");
    }

    if (!is_safe_from_typing_ready_end_ms(500))
    {
        ________DBG_20220314_mkb_fngr_n_typing("IDLE BLOCK ACTIVATED");
        return false;
    }

    // 3점 업
    else if (
        (m_touch_db.data_cnt >= 20 && m_touch_db.fingers_cnts[3] >= 6) &&
        (founded_gstr == PIXART_GSTR_TYPE_THREE_UP || m_touch_db.directions_cnts[DIR_UP] >= 16) &&
        (m_touch_db.fingers_cnts[4] == 0) &&
        is_safe_from_key_up_ms(800) &&
        is_safe_from_gstr_up_ms(400))
    {
        ________DBG_20211119_mkb_touch_detect_idle("3 SWIPE UP");
        if (APP_TIMER_TICKS(delta_time) < 50)
        {
            ________DBG_20211119_mkb_touch_3up_test("delta time block");
            return;
        }
        if (abs(delta_y) < 180)
        {
            ________DBG_20211119_mkb_touch_3up_test("dist y  block :%d", delta_y);
            return;
        }
        active_idle_3_up_swipe();
    }
    // 3점 다운
    else if (
        m_touch_db.data_cnt >= 15 && m_touch_db.fingers_cnts[3] >= 6 &&
        (founded_gstr == PIXART_GSTR_TYPE_THREE_DOWN || m_touch_db.directions_cnts[DIR_DOWN] >= 14) &&
        (m_touch_db.fingers_cnts[4] == 0) &&
        is_safe_from_key_up_ms(800) &&
        is_safe_from_gstr_up_ms(400))
    {
        ________DBG_20211119_mkb_touch_detect_idle("3 SWIPE DOWN");
        if (APP_TIMER_TICKS(delta_time) < 50)
        {
            ________DBG_20211119_mkb_touch_3up_test("delta time block");
            return;
        }
        if (abs(delta_y) < 180)
        {
            ________DBG_20211119_mkb_touch_3up_test("dist y  block :%d", delta_y);
            return;
        }
        active_idle_3_down_swipe();
    }
    // 3점 좌우 스와이프
    else if (
        m_touch_db.data_cnt < 42 && m_touch_db.data_cnt >= 15 && m_touch_db.fingers_cnts[3] >= 6 &&
        (m_touch_db.directions_cnts[DIR_RIGHT] >= 14 || m_touch_db.directions_cnts[DIR_LEFT] >= 14) &&
        (m_touch_db.fingers_cnts[4] == 0) &&
        is_safe_from_key_up_ms(800) &&
        is_safe_from_gstr_up_ms(400))
    {
        ________DBG_20211119_mkb_touch_detect_idle("3 SWIPE LEFT RIGHT");
        if (APP_TIMER_TICKS(delta_time) < 50)
        {
            ________DBG_20211119_mkb_touch_3up_test("delta time block");
            return;
        }
        if (abs(delta_x) > 60)
        {
            ________DBG_20211119_mkb_touch_3up_test("dist y  block :%d", delta_y);

            switch (mkb_db_curr_central_type_get())
            {
            case CENTRAL_MAC:
            {
                mkb_event_send_ms(0, EVT_KEY_DOWN, HID_KEY_L_ALT);
                mkb_event_send_ms(5, EVT_KEY_DOWN, HID_KEY_TAB);

                mkb_event_send_ms(10, EVT_KEY_UP, HID_KEY_TAB);
                mkb_event_send_ms(15, EVT_KEY_UP, HID_KEY_L_ALT);
                break;
            }
            case CENTRAL_IOS:
            {
                mkb_event_send_ms(0, EVT_KEY_DOWN, HID_KEY_L_ALT);
                mkb_event_send_ms(5, EVT_KEY_DOWN, HID_KEY_TAB);

                mkb_event_send_ms(10, EVT_KEY_UP, HID_KEY_TAB);
                mkb_event_send_ms(15, EVT_KEY_UP, HID_KEY_L_ALT);
                break;
            }
            break;
            case CENTRAL_WINDOWS:
            case CENTRAL_LINUX:
            case CENTRAL_ANDROID:
            case CENTRAL_DEX:
            case CENTRAL_OTHER:
            {
                mkb_event_send_ms(0, EVT_KEY_DOWN, HID_KEY_L_ALT);
                mkb_event_send_ms(5, EVT_KEY_DOWN, HID_KEY_TAB);

                mkb_event_send_ms(10, EVT_KEY_UP, HID_KEY_TAB);
                mkb_event_send_ms(15, EVT_KEY_UP, HID_KEY_L_ALT);
            }
            break;
            default:
                break;
            }
        }
    }
    // 4점 업
    else if (
        m_touch_db.data_cnt < 42 && m_touch_db.data_cnt >= 15 && m_touch_db.fingers_cnts[4] >= 6 &&
        (founded_gstr == PIXART_GSTR_TYPE_FOUR_UP || m_touch_db.directions_cnts[DIR_UP] >= 16) &&
        is_safe_from_key_up_ms(800) &&
        is_safe_from_gstr_up_ms(800))
    {
        ________DBG_20211119_mkb_touch_detect_idle("4 SWIPE UP");
        if (APP_TIMER_TICKS(delta_time) < 50)
        {
            ________DBG_20211119_mkb_touch_3up_test("delta time block");
            return;
        }
        if (abs(delta_y) < 180)
        {
            ________DBG_20211119_mkb_touch_3up_test("dist y  block :%d", delta_y);
            return;
        }
        active_idle_4_up_swipe();
    }
    // 4점 아래
    else if (
        m_touch_db.data_cnt < 42 && m_touch_db.data_cnt >= 15 && m_touch_db.fingers_cnts[4] >= 6 &&
        (founded_gstr == PIXART_GSTR_TYPE_FOUR_DOWN || m_touch_db.directions_cnts[DIR_DOWN] >= 16) &&
        is_safe_from_key_up_ms(800) &&
        is_safe_from_gstr_up_ms(800))
    {
        ________DBG_20211119_mkb_touch_detect_idle("4 SWIPE DOWN");
        if (APP_TIMER_TICKS(delta_time) < 50)
        {
            ________DBG_20211119_mkb_touch_3up_test("delta time block");
            return;
        }
        if (abs(delta_y) < 180)
        {
            ________DBG_20211119_mkb_touch_3up_test("dist y  block :%d", delta_y);
            return;
        }
        active_idle_4_down_swipe();
    }
    // 4점 좌
    else if (
        m_touch_db.data_cnt < 42 && m_touch_db.data_cnt >= 15 && m_touch_db.fingers_cnts[4] >= 6 &&
        (founded_gstr == PIXART_GSTR_TYPE_FOUR_LEFT || (m_touch_db.directions_cnts[DIR_LEFT_DOWN] + m_touch_db.directions_cnts[DIR_LEFT] * 4 + m_touch_db.directions_cnts[DIR_LEFT_UP] > 10 * 4)) &&
        is_safe_from_key_up_ms(800))
    {
        ________DBG_20211119_mkb_touch_detect_idle("4 SWIPE LEFT");
        if (APP_TIMER_TICKS(delta_time) < 50)
        {
            ________DBG_20211119_mkb_touch_3up_test("delta time block");
            return;
        }
        if (abs(delta_x) < 100)
        {
            ________DBG_20211119_mkb_touch_3up_test("dist y  block :%d", delta_y);
            return;
        }
        active_idle_4_x_swipe(true);
    }
    // 4점 우
    else if (
        m_touch_db.data_cnt < 42 && m_touch_db.data_cnt >= 15 && m_touch_db.fingers_cnts[4] >= 6 &&
        (founded_gstr == PIXART_GSTR_TYPE_FOUR_RIGHT || (m_touch_db.directions_cnts[DIR_RIGHT_DOWN] + m_touch_db.directions_cnts[DIR_RIGHT] * 4 + m_touch_db.directions_cnts[DIR_RIGHT_UP] > 10 * 4)) &&
        is_safe_from_key_up_ms(800))
    {
        ________DBG_20211119_mkb_touch_detect_idle("3 SWIPE RIGHT");
        if (APP_TIMER_TICKS(delta_time) < 50)
        {
            ________DBG_20211119_mkb_touch_3up_test("delta time block");
            return;
        }
        if (abs(delta_x) < 100)
        {
            ________DBG_20211119_mkb_touch_3up_test("dist y  block :%d", delta_y);
            return;
        }
        active_idle_4_x_swipe(false);
    }
    /*
    // 2점 핀치 인아웃 deprecated
    else if (
        ((m_touch_db.data_cnt >= 25 && m_touch_db.fingers_cnts[2] >= 17) || (founded_gstr == PIXART_GSTR_TYPE_TWO_PINCH)) &&
        (abs(delta_area_sum) > 200) &&
        ((m_touch_db.pinchs_cnts[PINCH_NARR] > 18 && m_touch_db.pinchs_cnts[PINCH_WIDE] <= 1) || (m_touch_db.pinchs_cnts[PINCH_WIDE] > 18 && m_touch_db.pinchs_cnts[PINCH_NARR] <= 1)) &&
        ((m_touch_db.directions_cnts[DIR_RIGHT] > 10 && m_touch_db.directions_cnts[DIR_LEFT] < 8) || (m_touch_db.directions_cnts[DIR_LEFT] > 10 && m_touch_db.directions_cnts[DIR_RIGHT] < 4)) &&
        (delta_time > 3000) &&
        (m_touch_db.fingers_cnts[4] == 0) &&
        (m_touch_db.fingers_cnts[3] == 0) &&
        (is_safe_from_key_up_ms(800)))
    {
        ________DBG_20211119_mkb_touch_detect_idle("2 PINCH NARR WIDE");
        if (m_touch_db.pinchs_cnts[PINCH_NARR] > 18)
        {
            ________DBG_20211119_mkb_touch_zoom_test("pinch  narrow");
            active_idle_2_narr_pinch();
        }
        else if (m_touch_db.pinchs_cnts[PINCH_WIDE] > 18)
        {
            ________DBG_20211119_mkb_touch_zoom_test("pinch  wider");
            active_idle_2_wide_pinch();
        }
        //________DBG_20211119_mkb_touch_zoom_test("  queue[0].delta_area: %d, sum:%d", queue[0].delta_area, delta_area_sum);
    }
    */
    // 2점 좌우 스와이프
    else if (
        m_touch_db.data_cnt < 42 && m_touch_db.data_cnt >= 15 && m_touch_db.fingers_cnts[2] >= 8 &&
        (m_touch_db.directions_cnts[DIR_RIGHT_DOWN] + m_touch_db.directions_cnts[DIR_RIGHT] * 4 + m_touch_db.directions_cnts[DIR_RIGHT_UP] > 10 * 4 ||
         m_touch_db.directions_cnts[DIR_LEFT_DOWN] + m_touch_db.directions_cnts[DIR_LEFT] * 4 + m_touch_db.directions_cnts[DIR_LEFT_UP] > 10 * 4) &&
        (m_touch_db.fingers_cnts[4] == 0) &&
        (m_touch_db.fingers_cnts[3] == 0) &&
        is_safe_from_key_up_ms(800))
    {
        ________DBG_20211119_mkb_touch_detect_idle("2 SWIPE LEFT RIGHT");
        if (APP_TIMER_TICKS(delta_time) < 50)
        {
            ________DBG_20211119_mkb_touch_3up_test("delta time block");
            return;
        }
        if (abs(delta_x) < 60)
        {
            ________DBG_20211119_mkb_touch_3up_test("dist y  block :%d", delta_y);
            return;
        }
        if (abs(delta_area_sum) > 120)
        {
            return;
        }
        if (m_touch_db.directions_cnts[DIR_RIGHT_DOWN] + m_touch_db.directions_cnts[DIR_RIGHT] * 4 + m_touch_db.directions_cnts[DIR_RIGHT_UP] > 10 * 4)
        {
            active_idle_2_x_swipe(DIR_RIGHT);
        }
        else if (m_touch_db.directions_cnts[DIR_LEFT_DOWN] + m_touch_db.directions_cnts[DIR_LEFT] * 4 + m_touch_db.directions_cnts[DIR_LEFT_UP] > 10 * 4)
        {
            active_idle_2_x_swipe(DIR_LEFT);
        }
    }

    // 4점 탭
    else if (
        m_touch_db.data_cnt >= 5 && m_touch_db.data_cnt <= 20 && m_touch_db.fingers_cnts[0] == 2 &&
        m_touch_db.fingers_cnts[4] >= 3 &&
        m_touch_db.directions_cnts[DIR_NONE] >= 10 &&
        is_safe_from_key_up_ms(800) &&
        is_safe_from_gstr_up_ms(800))
    {
        ________DBG_20211119_mkb_touch_detect_idle("4 TAP");
        // 4점 탭

        // cnt:15 0:02 1:00 2:02 3:01 4:10:  |0|0|2|4|4|4|4|4|4|4|4|4|4|3|2

        //최근 제스처 입력 시간 체크 추가
        //최근 키 입력 시간 체크 추가
        //최근 탭 입력 시간 체크 추가

        active_idle_4_tap();
    }
    // 3점 탭
    else if (
        m_touch_db.data_cnt >= 5 && m_touch_db.data_cnt <= 20 && m_touch_db.fingers_cnts[0] == 2 &&
        m_touch_db.fingers_cnts[3] >= 5 &&
        (m_touch_db.fingers_cnts[4] == 0) &&
        m_touch_db.directions_cnts[DIR_NONE] >= 10 &&
        is_safe_from_key_up_ms(800) &&
        is_safe_from_gstr_up_ms(800))
    {
        ________DBG_20211119_mkb_touch_detect_idle("3 TAP");
        // 3점 탭
        //  cnt:17 0:02 1:03 2:06 3:06 4:00:  |0|0|1|1|2|2|2|2|3|3|3|3|3|3|2|2|1

        //최근 제스처 입력 시간 체크 추가
        //최근 키 입력 시간 체크 추가
        //최근 탭 입력 시간 체크 추가
        active_idle_3_tap();
    }
    // 2점 탭
    else if (
        m_touch_db.data_cnt >= 5 &&
        m_touch_db.data_cnt <= 20 && m_touch_db.fingers_cnts[0] == 2 &&
        m_touch_db.fingers_cnts[2] >= 5 &&
        (m_touch_db.fingers_cnts[4] == 0) &&
        (m_touch_db.fingers_cnts[3] == 0) &&
        m_touch_db.directions_cnts[DIR_NONE] >= 5 &&
        is_safe_from_key_up_ms(800))
    {
        ________DBG_20211119_mkb_touch_detect_idle("2 TAP");
        // tl/ 2점탭
        //|0|0|1|1|2|2|2|2|2|2|2|2|2

        //흔들림체크 추가
        //최근 제스처 입력 시간 체크 추가
        //최근 키 입력 시간 체크 추가
        //최근 탭 입력 시간 체크 추가
        active_idle_2_tap();
    }
    // 1점 탭
    else if (m_touch_db.data_cnt == 3 && m_touch_db.fingers_cnts[0] == 3 &&
             is_safe_from_key_up_ms(1000) &&
             is_safe_from_red_key_up_ms(1000) &&
             is_safe_from_multttouch_up_ms(300))
    {
        ________DBG_20211119_mkb_touch_detect_idle("1 TAP");
        // 1점탭

        // cnt:14 0:02 1:12 2:00 3:00 4:00:  |0|0|1|1|1|1|1|1|1|1|1|1|1|1
        // cnt:01 0:01 1:00 2:00 3:00 4:00:  |0
        // cnt:02 0:02 1:00 2:00 3:00 4:00:  |0|0
        // cnt:03 0:03 1:00 2:00 3:00 4:00:  |0|0|0

        //흔들림체크 추가
        //최근 제스처 입력 시간 체크 추가
        active_idle_1_tap();
    }
    //더블탭
    else if (m_touch_db.data_cnt >= 10 && m_touch_db.data_cnt <= 20 &&
             m_touch_db.fingers_cnts[0] == 4 && m_touch_db.fingers_cnts[1] >= 5 &&
             m_touch_db.directions_cnts[DIR_NONE] >= 5 &&
             is_safe_from_key_up_ms(300))
    {
        ________DBG_20211119_mkb_touch_detect_idle("11 TAP");
        //더블탭

        // cnt:17 0:02 1:15 2:00 3:00 4:00:  |1|0|0|1|1|1|1|1|1|1|1|1|1|1|1|1|1
        // cnt:01 0:00 1:01 2:00 3:00 4:00:  |1
        // cnt:02 0:00 1:02 2:00 3:00 4:00:  |1|1
        // cnt:03 0:00 1:03 2:00 3:00 4:00:  |1|1|1
        // cnt:13 0:03 1:10 2:00 3:00 4:00:  |0|0|0|1|1|1|1|1|1|1|1|1|1
        // cnt:14 0:04 1:10 2:00 3:00 4:00:  |0|0|0|0|1|1|1|1|1|1|1|1|1|1

        //흔들림체크 추가
        //최근 제스처 입력 시간 체크 추가
        //최근 키 입력 시간 체크 추가
        //최근 탭 입력 시간 체크 추가
        active_idle_1_double_tap();
    }
    //드래그
    else if (m_touch_db.data_cnt >= 6 && m_touch_db.data_cnt <= 20 && m_touch_db.fingers_cnts[0] == 2 &&
             (m_touch_db.data_cnt - 4 <= m_touch_db.fingers_cnts[1]) &&
             m_touch_db.directions_cnts[DIR_NONE] >= 5 &&
             is_safe_from_key_up_ms(300))
    {
        ________DBG_20211119_mkb_touch_detect_idle("1 DRAG");
        //드래그 시작

        // 42ms
        // cnt:17 0:02 1:15 2:00 3:00 4:00:  |1|0|0|1|1|1|1|1|1|1|1|1|1|1|1|1|1
        // cnt:01 0:00 1:01 2:00 3:00 4:00:  |1
        // cnt:02 0:00 1:02 2:00 3:00 4:00:  |1|1
        // cnt:03 0:00 1:03 2:00 3:00 4:00:  |1|1|1
        // cnt:13 0:03 1:10 2:00 3:00 4:00:  |1|1|1|1|1|1|1|1|1|1|1|1|1
        // cnt:14 0:04 1:10 2:00 3:00 4:00:  |1|1|1|1|1|1|1|1|1|1|1|1|1|1

        drag_step_temp = 1;
        drag_time_temp = app_timer_cnt_get();
        ________DBG_20211201_idle_1_tap_flag("drag_step: ?->1: (%d)", drag_step_temp);
        ________DBG_20211119_mkb_touch_drag_test("drag triggered");

        //시작  : 0
        //탭    : 0->1
        //포인팅: 1->2 click
        //플로팅  2->0

        //시작  : 0
        //탭    : 0->1
        //타임아웃
        //포인팅 -> temp 0
    }
}
#endif

static void fngr_process(void) // finger_cnt 와 gester 갯수로 구한다.
{

    ________DBG_20220318_fngr_process_queue("[fcnt:%d] [jobs:%2d->%2d] [detect:%2d] [active:%2d]", m_touch_db.queue[0].finger_cnt, last_job.active_fngr_job[1], last_job.active_fngr_job[0], last_job.detect_cnt_volatile, last_job.active_cnt_volatile);
    // 2점 핀치
    if (search_fngr_n_typing())
    {
        detect_fngr_n_typing();
    }

    if (search_fngr_2_pinch())
    {
        detect_fngr_2_pinch();
    }
    else if (search_fngr_2_y_scroll())
    {
        detect_fngr_2_y_scroll();
    }
    else if (search_fngr_2_x_scroll())
    {
        detect_fngr_2_x_scroll();
    }
    else if (search_fngr_1_xy_pointing())
    {
        detect_fngr_1_xy_pointing();
    }
    else if (search_fngr_3_xy_scroll())
    {
        detect_fngr_3_xy_scroll();
    }
    else if (m_touch_db.data_cnt >= 30 && m_touch_db.fingers_cnts[0] >= 30) //관성스크롤
    {
        active_idle_0_y_scroll();
        active_idle_0_x_scroll();
    }
}

/*_________ ______   _        _______      _______
\__   __/(  __  \ ( \      (  ____ \    (  __   )
   ) (   | (  \  )| (      | (    \/    | (  )  |
   | |   | |   ) || |      | (__  _____ | | /   |
   | |   | |   | || |      |  __)(_____)| (/ /) |
   | |   | |   ) || |      | (          |   / | |
___) (___| (__/  )| (____/\| (____/\    |  (__) |
\_______/(______/ (_______/(_______/    (_______)*/

static bool search_idle_0_y_scroll()
{
}

static void detect_idle_0_y_scroll()
{
}

static void active_idle_0_y_scroll()
{
    ________DBG_20211119_mkb_touch_detect_fgr("fgr2 inertia v");
}

static void active_idle_0_x_scroll()
{
    ________DBG_20211119_mkb_touch_detect_fgr("fgr2 inertia h");
}

/*_________ ______   _        _______    __
\__   __/(  __  \ ( \      (  ____ \  /  \
   ) (   | (  \  )| (      | (    \/  \/) )
   | |   | |   ) || |      | (__  _____ | |
   | |   | |   | || |      |  __)(_____)| |
   | |   | |   ) || |      | (          | |
___) (___| (__/  )| (____/\| (____/\  __) (_
\_______/(______/ (_______/(_______/  \____/*/

static bool search_idle_1_tap()
{
    //예외 FALSE
    if (!(is_safe_from_key_up_ms(1000) && is_safe_from_red_key_up_ms(1000) && is_safe_from_multttouch_up_ms(400)))
    {
        ________DBG_20220314_idle_module_refactory("search step 1 ");
        return false;
    }

    //예외 TRUE
    if (m_touch_db.founded_gesture == PIXART_GSTR_TYPE_ONE_TAP_AND_DRAG_RELEASE)
    {
        return true;
    }

    // AND 조건들
    //데이터갯수
    if (!(4 < m_touch_db.data_cnt && m_touch_db.data_cnt < 26))
    {
        ________DBG_20220314_idle_module_refactory("search step 2 ");
        return false;
    }
    //손가락 갯수
    if (!(4 < m_touch_db.fingers_cnts[1] && m_touch_db.fingers_cnts[1] < 26))
    {
        return false;
    }
    if (!(m_touch_db.fingers_cnts[0] == 1 && m_touch_db.fingers_cnts[2] == 0 && m_touch_db.fingers_cnts[3] == 0 && m_touch_db.fingers_cnts[4] == 0))
    {
        ________DBG_20220314_idle_module_refactory("search step 4 ");
        return false;
    }
    //방향
    if (!(m_touch_db.directions_cnts[DIR_NONE] + 4 >
          m_touch_db.directions_cnts[DIR_RIGHT] + m_touch_db.directions_cnts[DIR_RIGHT_UP] +
              m_touch_db.directions_cnts[DIR_UP] + m_touch_db.directions_cnts[DIR_LEFT_UP] +
              m_touch_db.directions_cnts[DIR_LEFT] + m_touch_db.directions_cnts[DIR_LEFT_DOWN] +
              m_touch_db.directions_cnts[DIR_DOWN] + m_touch_db.directions_cnts[DIR_RIGHT_DOWN]))
    {
        return false;
    }
    //거리
    if (!(abs(m_touch_db.end_x - m_touch_db.start_x) + abs(m_touch_db.end_y - m_touch_db.start_y) < 80))
    {
        ________DBG_20220314_idle_module_refactory("search step 3 ");
        return false;
    }
    //시간
    if (!(APP_TIMER_TICKS(20) < abs(m_touch_db.end_time - m_touch_db.start_time) && abs(m_touch_db.end_time - m_touch_db.start_time) < APP_TIMER_TICKS(120)))
    {
        return false;
    }

    return true;
}

static void detect_idle_1_tap()
{
    last_job_detect(IDLE_1_TAP);
    // double tap도 여기서 처리
    if (last_job.active_fngr_job[0] == IDLE_1_TAP)
    {
        // detect_idle_1_double_tap();
    }
    else
    {
        //이게 아지고 지연되어야한다.
        active_idle_1_tap();
    }
}

static void active_idle_1_tap()
{
    ________DBG_20211201_idle_1_tap_flag("1tap click down");
    mkb_event_send_ms(0, EVT_BUTTON_CLICK_LEFT, true);
    mkb_event_send_ms(8, EVT_BUTTON_CLICK_LEFT, false);

    last_job_active(IDLE_1_TAP);
}

static bool search_idle_1_double_tap()
{
    return false;
}

static void detect_idle_1_double_tap()
{
    last_job_detect(IDLE_1_DOUBLE_TAP);
    active_idle_1_double_tap();
}

static void active_idle_1_double_tap()
{

    ________DBG_20211119_mkb_touch_detect_idle("1-2tap");

    mkb_event_send_ms(8, EVT_BUTTON_CLICK_LEFT, true);
    mkb_event_send_ms(16, EVT_BUTTON_CLICK_LEFT, false);
    // mkb_event_send_ms(20, EVT_BUTTON_CLICK_LEFT, true);
    // mkb_event_send_ms(30, EVT_BUTTON_CLICK_LEFT, false);
}

static bool search_idle_1_drag()
{
}

static void detect_idle_1_drag()
{
    last_job_detect(FNGR_1_XY_DRAG);
}

static void active_idle_1_drag()
{
    last_job_active(FNGR_1_XY_DRAG);
    ________DBG_20211119_mkb_touch_detect_idle("drag_state");
}

/* _______  _        _______  _______       __
(  ____ \( (    /|(  ____ \(  ____ )     /  \
| (    \/|  \  ( || (    \/| (    )|     \/) )
| (__    |   \ | || |      | (____)| _____ | |
|  __)   | (\ \) || | ____ |     __)(_____)| |
| (      | | \   || | \_  )| (\ (          | |
| )      | )  \  || (___) || ) \ \__     __) (_
|/       |/    )_)(_______)|/   \__/     \____/*/
static bool search_fngr_1_xy_pointing()
{
    if (m_touch_db.data_cnt > 40 && m_touch_db.data_cnt - m_touch_db.fingers_cnts[1] > 10)
    {
        ________DBG_20211201_fngr_1_posxy_vs_gstxy("delta == 0");
        return false;
    }
    if (is_safe_from_key_up_ms(400) == false)
    {
        return false;
    }
    if (is_safe_from_multttouch_up_ms(200) == false)
    {
        return false;
    }

    if (m_touch_db.data_cnt < 4 && m_touch_db.fingers_cnts[1] < 4)
    {
        return false;
    }

    //스크롤후 포인팅시 충분히 판단하고 움직이도록. 하지만 옆으로 빠르게움직이면 괜찮다.
    if (last_job.active_fngr_job[1] == FNGR_2_Y_SCROLL && m_touch_db.fingers_cnts[1] < 20)
    {
        if (abs(m_touch_db.queue[0].compensate_delta_x + m_touch_db.queue[1].compensate_delta_x + m_touch_db.queue[2].compensate_delta_x + m_touch_db.queue[4].compensate_delta_x + m_touch_db.queue[5].compensate_delta_x + m_touch_db.queue[6].compensate_delta_x) > 6)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    if ((gstr_step.fgr1_moving_step > 7))
    {
        return true;
    }
    if (last_job.active_fngr_job[1] == FNGR_1_XY_POINTING && m_touch_db.data_cnt >= 6 && m_touch_db.fingers_cnts[1] >= 6)
    {
        return true;
    }
    if (m_touch_db.fingers_cnts[1] >= 10 && m_touch_db.fingers_cnts[2] < 10 && m_touch_db.fingers_cnts[3] < 10)
    {
        return true;
    }
    if ((m_touch_db.queue[0].gesture.type == PIXART_GSTR_TYPE_ONE_MOVE))
    {
        return true;
    }
    return false;
}
static void detect_fngr_1_xy_pointing()
{
    last_job_detect(FNGR_1_XY_POINTING);
    active_fngr_1_xy_pointing();
}

static void active_fngr_1_xy_pointing()
{
    ________DBG_20211119_mkb_touch_detect_fgr("fgr1 move");

    int16_t delta_x = 0;
    int16_t delta_y = 0;

    /*
        if (m_touch_db.queue[0].gesture.type == PIXART_GSTR_TYPE_ONE_MOVE || gstr_step.fgr1_moving_step == 100)
        {
            ________LOG_20211205_fngr_1_jitter("GSTR ONE MOVE");
            delta_x = m_touch_db.queue[0].gesture.x;
            delta_y = m_touch_db.queue[0].gesture.y;

            if ((m_touch_db.queue[0].gesture.x == 0 && m_touch_db.queue[0].gesture.x != 0) || (m_touch_db.queue[0].gesture.y == 0 && m_touch_db.queue[0].delta_y != 0))
            {
                if (app_timer_cnt_diff_compute(app_timer_cnt_get(), mkb_db_last_passkey_updown_time_get()) < APP_TIMER_TICKS(100)) //키를 누르면 잠깐 gstr데이터가 안들어온다. 그때 raw값으로 사용
                {
                    delta_x = m_touch_db.queue[0].delta_x;
                    delta_y = m_touch_db.queue[0].delta_y;
                }
            }
        }
        else
        {
            ________LOG_20211205_fngr_1_jitter("RAW ONE MOVE");
            delta_x = m_touch_db.queue[0].delta_x;
            delta_y = m_touch_db.queue[0].delta_y;
        }
    */

    delta_x = m_touch_db.queue[0].compensate_delta_x;
    delta_y = m_touch_db.queue[0].compensate_delta_y;
    if (m_touch_db.queue[0].gesture.type == PIXART_GSTR_TYPE_ONE_MOVE && m_touch_db.queue[0].delta_dist > 4)
    {
        // delta_x = m_touch_db.queue[0].gesture.x;
        // delta_y = m_touch_db.queue[0].gesture.y;
    }

    float os_base_multiflyier = 1;
    int8_t os_accel_multiflyier = 64;

    if (drag_step_temp == 1)
    {
        ________DBG_20211119_mkb_touch_drag_test("if(timediff:%d < 220ms:%d)", app_timer_cnt_diff_compute(app_timer_cnt_get(), drag_time_temp), APP_TIMER_TICKS(220));
        if (app_timer_cnt_diff_compute(app_timer_cnt_get(), drag_time_temp) < APP_TIMER_TICKS(220))
        {
            ________DBG_20211119_mkb_touch_drag_test("drag act");

            drag_step_temp = 2;
            ________DBG_20211201_idle_1_tap_flag("drag_step: 1->2: (%d)", drag_step_temp);
            ________DBG_20211201_idle_1_tap_flag("drag click down");
            mkb_db_is_tap_click_down_set(true);
            mkb_event_send(EVT_BUTTON_CLICK_LEFT, true);
        }
        else
        {

            drag_step_temp = 0;
            ________DBG_20211201_idle_1_tap_flag("drag_step: 1->0: (%d)", drag_step_temp);
        }
    }
    if (drag_step_temp == 2)
    {
        // drag_step_temp = 0;
    }

    ________DBG_20211119_mkb_touch_1fgr_accel_test("add accel: %04d ,%04d", (abs(delta_x) * delta_x) / 64, (abs(delta_y) * delta_y) / 64);
    switch (mkb_db_curr_central_type_get())
    {
    case CENTRAL_IOS:
        if (abs(delta_x) + abs(delta_y) <= 3)
        {
            os_base_multiflyier = 2.4;
        }
        else
        {
            os_base_multiflyier = 2.4;
        }
        os_accel_multiflyier = 160; // 42;
        break;
    case CENTRAL_MAC:
    {
        if (abs(delta_x) + abs(delta_y) <= 2)
        {
            os_base_multiflyier = 2.8; // 2.0
        }
        else
        {
            os_base_multiflyier = 2.8; // 2.0
        }
        os_accel_multiflyier = 42; // 42;
        break;
    }
    case CENTRAL_WINDOWS:
    case CENTRAL_LINUX:
    case CENTRAL_ANDROID:
    case CENTRAL_DEX:
    case CENTRAL_OTHER:
    {
        os_base_multiflyier = 1.6;
        os_accel_multiflyier = 42; // 42;
        break;
    }

    default:
        break;
    }

    /*{
         int16_t speed_avg = 0;
         for (int i = 0; i < m_touch_db.data_cnt; i++)
         {
             speed_avg += abs(m_touch_db.queue[i].delta_x);
         }
         speed_avg = speed_avg / m_touch_db.data_cnt;

         if (speed_avg <= 0) // (delta_x < 3) //저속모드
         {
             delta_x = (delta_x) + ((abs(delta_x) * delta_x) / os_accel_multiflyier);
         }
         else
         {
             delta_x = (delta_x * os_base_multiflyier) + ((abs(delta_x) * delta_x) / os_accel_multiflyier);
         }
     }*/
    delta_x = (delta_x * os_base_multiflyier) + ((abs(delta_x) * delta_x) / os_accel_multiflyier);

    /*{
         int16_t speed_avg = 0;
         for (int i = 0; i < m_touch_db.data_cnt; i++)
         {
             speed_avg += abs(m_touch_db.queue[i].delta_y);
         }
         speed_avg = speed_avg / m_touch_db.data_cnt;
         if (speed_avg <= 0) // (delta_y < 3)
         {
             delta_y = (delta_y) + ((abs(delta_y) * delta_y) / os_accel_multiflyier);
         }
         {
             delta_y = (delta_y * os_base_multiflyier) + ((abs(delta_y) * delta_y) / os_accel_multiflyier);
         }
     }*/
    delta_y = (delta_y * os_base_multiflyier) + ((abs(delta_y) * delta_y) / os_accel_multiflyier);

    mkb_event_send(EVT_TOUCH_MOVE, delta_x, delta_y);
    last_job_active(FNGR_1_XY_POINTING);
}

/*_________ ______   _        _______    _______
\__   __/(  __  \ ( \      (  ____ \  / ___   )
   ) (   | (  \  )| (      | (    \/  \/   )  |
   | |   | |   ) || |      | (__  _____   /   )
   | |   | |   | || |      |  __)(_____)_/   /
   | |   | |   ) || |      | (         /   _/
___) (___| (__/  )| (____/\| (____/\  (   (__/\
\_______/(______/ (_______/(_______/  \_______/*/

static bool search_idle_2_tap()
{
    //예외 FALSE
    if (!(is_safe_from_key_up_ms(1000) && is_safe_from_red_key_up_ms(1000) && is_safe_from_multttouch_up_ms(200)))
    {
        ________DBG_20220314_idle_module_refactory("search step 1 ");
        return false;
    }

    //예외 TRUE
    if (m_touch_db.founded_gesture == PIXART_GSTR_TYPE_TWO_TAP_RELEASE)
    {
        return true;
    }

    // AND 조건들
    //데이터갯수
    if (!(4 < m_touch_db.data_cnt && m_touch_db.data_cnt < 26))
    {
        return false;
    }
    //손가락 갯수
    if (!(4 < m_touch_db.fingers_cnts[2] && m_touch_db.fingers_cnts[2] < 26))
    {
        return false;
    }
    if (!(m_touch_db.fingers_cnts[0] == 1 && m_touch_db.fingers_cnts[1] < m_touch_db.fingers_cnts[2] && m_touch_db.fingers_cnts[3] == 0 && m_touch_db.fingers_cnts[4] == 0))
    {
        return false;
    }
    //방향
    if (!(m_touch_db.directions_cnts[DIR_NONE] + 4 > m_touch_db.directions_cnts[DIR_RIGHT] + m_touch_db.directions_cnts[DIR_RIGHT_UP] + m_touch_db.directions_cnts[DIR_UP] + m_touch_db.directions_cnts[DIR_LEFT_UP] + m_touch_db.directions_cnts[DIR_LEFT] + m_touch_db.directions_cnts[DIR_LEFT_DOWN] + m_touch_db.directions_cnts[DIR_DOWN] + m_touch_db.directions_cnts[DIR_RIGHT_DOWN]))
    {
        return false;
    }
    //거리
    if (!(abs(m_touch_db.end_x - m_touch_db.start_x) + abs(m_touch_db.end_y - m_touch_db.start_y) < 80))
    {
        ________DBG_20220314_idle_module_refactory("search step 3 ");
        return false;
    }
    //시간
    if (!(APP_TIMER_TICKS(80) < abs(m_touch_db.end_time - m_touch_db.start_time) && abs(m_touch_db.end_time - m_touch_db.start_time) < APP_TIMER_TICKS(220)))
    {
        return false;
    }

    return true;

    /*
    if (is_safe_from_key_up_ms(800) == false)
    {
        return false;
    }
    if (m_touch_db.data_cnt >= 5 && m_touch_db.data_cnt <= 20)
    {
    }
    else
    {
        return false;
    }
    if ((m_touch_db.fingers_cnts[0] == 2) && (m_touch_db.fingers_cnts[2] >= 5) && (m_touch_db.fingers_cnts[4] == 0) && (m_touch_db.fingers_cnts[3] == 0))
    {
    }
    else
    {
        return false;
    }
    if (m_touch_db.directions_cnts[DIR_NONE] >= 5)
    {
    }
    else
    {
        return false;
    }
    return true;
    */
}
static void detect_idle_2_tap()
{
    last_job_detect(IDLE_2_TAP);
    active_idle_2_tap();
}
static void active_idle_2_tap()
{
    ________DBG_20211119_mkb_touch_detect_idle("2-1tap");
    switch (mkb_db_curr_central_type_get())
    {
    case CENTRAL_ANDROID:
    {
        break;
    }
    case CENTRAL_MAC:
    case CENTRAL_IOS:
    case CENTRAL_WINDOWS:
    case CENTRAL_LINUX:
    case CENTRAL_DEX:
    case CENTRAL_OTHER:
    {
        mkb_event_send_ms(0, EVT_BUTTON_CLICK_RIGHT, true);
        mkb_event_send_ms(10, EVT_BUTTON_CLICK_RIGHT, false);
        break;
    }
    }
    last_job_active(IDLE_2_TAP);
}

static bool search_idle_2_x_swipe()
{
    //예외 FALSE
    if (!(is_safe_from_key_up_ms(800) && is_safe_from_red_key_up_ms(1000) && is_safe_from_multttouch_up_ms(200)))
    {
        ________DBG_20211201_idle_2_swipe("false");
        return false;
    }

    // AND 조건들
    //데이터갯수
    if (!(10 <= m_touch_db.data_cnt && m_touch_db.data_cnt <= 42))
    {
        ________DBG_20211201_idle_2_swipe("false");
        return false;
    }
    //손가락 갯수
    if (!(4 <= m_touch_db.fingers_cnts[2] && m_touch_db.fingers_cnts[2] <= 25))
    {
        ________DBG_20211201_idle_2_swipe("false");
        return false;
    }
    if (!(m_touch_db.fingers_cnts[0] == 1 && m_touch_db.fingers_cnts[3] == 0 && m_touch_db.fingers_cnts[4] == 0))
    {
        ________DBG_20211201_idle_2_swipe("false");
        return false;
    }

    //방향
    if (!((m_touch_db.directions_cnts[DIR_RIGHT_DOWN] + m_touch_db.directions_cnts[DIR_RIGHT] * 4 + m_touch_db.directions_cnts[DIR_RIGHT_UP] > 10 * 4) || (m_touch_db.directions_cnts[DIR_LEFT_DOWN] + m_touch_db.directions_cnts[DIR_LEFT] * 4 + m_touch_db.directions_cnts[DIR_LEFT_UP] > 10 * 4)))
    {
        ________DBG_20211201_idle_2_swipe("false");
        return false;
    }

    if (!(m_touch_db.directions_cnts[DIR_NONE] / 2 <
          m_touch_db.directions_cnts[DIR_RIGHT] + m_touch_db.directions_cnts[DIR_RIGHT_UP] +
              m_touch_db.directions_cnts[DIR_LEFT_UP] +
              m_touch_db.directions_cnts[DIR_LEFT] + m_touch_db.directions_cnts[DIR_LEFT_DOWN] +
              m_touch_db.directions_cnts[DIR_RIGHT_DOWN]))
    {
        ________DBG_20211201_idle_2_swipe("false");
        return false;
    }

    //거리
    if (!(30 <= abs(m_touch_db.end_x - m_touch_db.start_x) && abs(m_touch_db.end_x - m_touch_db.start_x) <= 600))
    {
        ________DBG_20211201_idle_2_swipe("false");
        return false;
    }

    if (!(abs(m_touch_db.end_x - m_touch_db.start_x) > abs(m_touch_db.end_y - m_touch_db.start_y) * 2))
    {
        ________DBG_20211201_idle_2_swipe("false");
        return false;
    }

    //넓이변화
    if (!(abs(get_deltaarea_sum(20)) < 120))
    {
        ________DBG_20211201_idle_2_swipe("false");
        return false;
    }

    //시간
    if (!(APP_TIMER_TICKS(80) < abs(m_touch_db.end_time - m_touch_db.start_time) && abs(m_touch_db.end_time - m_touch_db.start_time) < APP_TIMER_TICKS(300)))
    {
        ________DBG_20211201_idle_2_swipe("false");
        return false;
    }

    return true;
}

static void detect_idle_2_x_swipe()
{
    last_job_detect(IDLE_2_X_SWIPE);
    if (m_touch_db.directions_cnts[DIR_RIGHT_DOWN] + m_touch_db.directions_cnts[DIR_RIGHT] * 4 + m_touch_db.directions_cnts[DIR_RIGHT_UP] > 10 * 4)
    {
        active_idle_2_x_swipe(DIR_RIGHT);
    }
    else if (m_touch_db.directions_cnts[DIR_LEFT_DOWN] + m_touch_db.directions_cnts[DIR_LEFT] * 4 + m_touch_db.directions_cnts[DIR_LEFT_UP] > 10 * 4)
    {
        active_idle_2_x_swipe(DIR_LEFT);
    }
}

static void active_idle_2_x_swipe(direction_t direction)
{
    last_job_active(IDLE_2_X_SWIPE);
    uint8_t input_delay = 0;

    switch (mkb_db_curr_central_type_get())
    {

    case CENTRAL_MAC:
    case CENTRAL_IOS:
    {
        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_L_ALT);
        if (direction == DIR_RIGHT)
        {
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_O_BRCKT);
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_O_BRCKT);
        }
        else if (direction == DIR_LEFT)
        {
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_C_BRCKT);
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_C_BRCKT);
        }
        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_L_ALT);
        break;
    }
    case CENTRAL_ANDROID:
    case CENTRAL_WINDOWS:
    case CENTRAL_LINUX:
    case CENTRAL_DEX:
    case CENTRAL_OTHER:
    {
        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_L_ALT);
        if (direction == DIR_RIGHT)
        {
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_LEFT);
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_LEFT);
        }
        else if (direction == DIR_LEFT)
        {
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_RIGHT);
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_RIGHT);
        }
        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_L_ALT);
        break;
    }
    }
    gstr_step.fgr0_is_need_mod_clear = true;
}

static bool search_idle_2_pinch()
{
    //예외 FALSE
    if (!(is_safe_from_key_up_ms(800) && is_safe_from_red_key_up_ms(1000) && is_safe_from_multttouch_up_ms(200)))
    {
        ________DBG_20211201_idle_2_pinch("false");
        return false;
    }

    // AND 조건들
    //데이터갯수
    if (!(20 <= m_touch_db.data_cnt && m_touch_db.data_cnt <= 42))
    {
        ________DBG_20211201_idle_2_pinch("false");
        return false;
    }

    //시간
    if (!(APP_TIMER_TICKS(150) <= abs(m_touch_db.end_time - m_touch_db.start_time) && abs(m_touch_db.end_time - m_touch_db.start_time) <= APP_TIMER_TICKS(330)))
    {
        ________DBG_20211201_idle_2_pinch("false");
        return false;
    }

    //거리
    if (!(-130 <= m_touch_db.end_x - m_touch_db.start_x && m_touch_db.end_x - m_touch_db.start_x <= 20)) // wide
    {
        ________DBG_20211201_idle_2_pinch("false");
        return false;
    }

    if (!(-30 <= m_touch_db.end_y - m_touch_db.start_y && m_touch_db.end_y - m_touch_db.start_y <= 60)) // wide
    {
        ________DBG_20211201_idle_2_pinch("false");
        return false;
    }

    //넓이변화
    if (100 <= get_deltaarea_sum(40)) // wide
    {
    }
    else if (get_deltaarea_sum(40) < -70) // narr
    {
    }
    else
    {
        ________DBG_20211201_idle_2_pinch("false");
        return false;
    }

    //손가락 갯수%
    if (!(get_qcnt_per(m_touch_db.fingers_cnts[1]) <= 60))
    {
        ________DBG_20211201_idle_2_pinch("false");
        return false;
    }

    if (!(40 <= get_qcnt_per(m_touch_db.fingers_cnts[2])))
    {
        ________DBG_20211201_idle_2_pinch("false");
        return false;
    }

    if (!(m_touch_db.fingers_cnts[0] == 1 && m_touch_db.fingers_cnts[3] == 0 && m_touch_db.fingers_cnts[4] == 0))
    {
        ________DBG_20211201_idle_2_pinch("false");
        return false;
    }

    //핀치 와이드가 30%이상
    if ((30 <= get_qcnt_per(m_touch_db.pinchs_cnts[PINCH_WIDE]) && m_touch_db.pinchs_cnts[PINCH_NARR] == 0))
    {
    }
    else if ((30 <= get_qcnt_per(m_touch_db.pinchs_cnts[PINCH_NARR]) && m_touch_db.pinchs_cnts[PINCH_WIDE] == 0))
    {
    }
    else
    {
        ________DBG_20211201_idle_2_pinch("false");
        return false;
    }

    //방향
    if (!(get_qcnt_per(m_touch_db.directions_cnts[DIR_NONE]) >= 10))
    {
        ________DBG_20211201_idle_2_pinch("false");
        return false;
    }

    return true;
}

static void detect_idle_2_pinch()
{
    last_job_detect(IDLE_2_PINCH);

    //넓이변화
    if (get_deltaarea_sum(30) > 0)
    {
        active_idle_2_pinch(PINCH_WIDE);
    }
    if (get_deltaarea_sum(30) < 0)
    {
        active_idle_2_pinch(PINCH_NARR);
    }
}

static void active_idle_2_pinch(pinch_t pinch)
{
    last_job_active(IDLE_2_PINCH);
    uint8_t input_delay = 0;

    switch (mkb_db_curr_central_type_get())
    {
    case CENTRAL_MAC:
    {
        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_L_ALT);
        if (pinch == PINCH_NARR)
        {
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_MINUS);
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_MINUS);
        }
        else if (pinch == PINCH_WIDE)
        {
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_EQUAL);
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_EQUAL);
        }
        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_L_ALT);
        break;
    }
    case CENTRAL_IOS:
    {
        break;
    }
    case CENTRAL_WINDOWS:
    case CENTRAL_LINUX:
    case CENTRAL_ANDROID:
    case CENTRAL_DEX:
    case CENTRAL_OTHER:
    {
        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_L_CTRL);
        if (pinch == PINCH_NARR)
        {
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_MINUS);
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_MINUS);
        }
        else if (pinch == PINCH_WIDE)
        {
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_EQUAL);
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_EQUAL);
        }
        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_L_CTRL);
        break;
    }
    default:
    {
        break;
    }
    }
    gstr_step.fgr0_is_need_mod_clear = true;
    ________DBG_20211119_mkb_touch_detect_fgr("fgr2 pinch out");
}

/* _______  _        _______  _______       _______
(  ____ \( (    /|(  ____ \(  ____ )     / ___   )
| (    \/|  \  ( || (    \/| (    )|     \/   )  |
| (__    |   \ | || |      | (____)| _____   /   )
|  __)   | (\ \) || | ____ |     __)(_____)_/   /
| (      | | \   || | \_  )| (\ (         /   _/
| )      | )  \  || (___) || ) \ \__     (   (__/\
|/       |/    )_)(_______)|/   \__/     \_______/*/
static bool search_fngr_2_y_scroll()
{
    if (is_safe_from_key_up_ms(600) == false)
    {
        return false;
    }

    if ((last_job.active_fngr_job[0] == FNGR_2_PINCH))
    {
        // return false;
    }

    if ((m_touch_db.fingers_cnts[1] > 20) || (m_touch_db.fingers_cnts[2] < 4) || (m_touch_db.fingers_cnts[3] > 3) || (m_touch_db.fingers_cnts[4] > 0))
    {
        return false;
    }

    if (gstr_step.fgr2_scroll_step > 8)
    {
        return true;
    }

    if (m_touch_db.directions_cnts[DIR_UP] > 4 || m_touch_db.directions_cnts[DIR_DOWN] > 4)
    {
        return true;
    }

    if ((last_job.active_fngr_job[1] == FNGR_2_Y_SCROLL) && (app_timer_cnt_diff_compute(app_timer_cnt_get(), last_job.active_time_stamp) < APP_TIMER_TICKS(3000)))
    {
        // return true;
    }

    if (m_touch_db.queue[0].gesture.type == PIXART_GSTR_TYPE_TWO_VSCRL)
    {

        if ((m_touch_db.data_cnt >= 8 && m_touch_db.fingers_cnts[2] >= 8)) //손가락 대고있을때
        {
            return true;
        }
    }

    return false;
}

static void detect_fngr_2_y_scroll()
{
    //연산을 한다. acive는 보내기만 할거다. 아직 작업중
    last_job_detect(FNGR_2_Y_SCROLL);
    active_fngr_2_y_scroll();
}

static void active_fngr_2_y_scroll()
{
    ________DBG_20211119_mkb_touch_detect_fgr("fgr2 v");

    uint8_t os_physicdist_threshold = 128;   //쓰레숄드 값. 트리거에 필요한 손가락dist값.
    double os_physicdist_threshold_mult = 1; //쓰레숄드 소모 증가비율. 클수록 트리거에 필요한 dist 감소.
    uint8_t os_scrollspeed_multi = 1;        // HID send 시의 distance 배율. delta 혹은 속도값을곱하여 가속한다.
    int8_t os_vector = 1;

    int16_t delta_y = m_touch_db.queue[0].compensate_delta_y;

    switch (mkb_db_curr_central_type_get())
    {
    case CENTRAL_MAC:
    {
        uint8_t is_stoped = 0;
        int32_t sum_y = 0;
        uint8_t max_y = 16;
        uint8_t cnt_y = 0;
        uint32_t avg_y = 0;
        int16_t dir_y = 0;
        uint8_t dist_y = 1;

        { //가능한 갯수만큼 평균내기 100을 곱해서 double의 연산을 쉽게한다.
            for (uint8_t i = 0; (i < m_touch_db.data_cnt) && (i < max_y); i++)
            {
                cnt_y++;
                sum_y += (m_touch_db.queue[i].delta_y) * 100; // compensate가 2점 스크롤 초반에 값이이상하게 들어오는 경향이있어서 delta_y 사용
            }
            avg_y = abs(sum_y / cnt_y);
        }

        { //초반 X개만 가지고 방향결정하기(빠른반응속도위해)
            for (uint8_t i = 0; (i < m_touch_db.data_cnt) && (i < 6); i++)
            {
                dir_y += (m_touch_db.queue[i].compensate_delta_y);
                // dir_y += (m_touch_db.queue[i].delta_y);
            }
            dir_y = dir_y / abs(dir_y);
        }

        //평균이 100 미만의 값이 나왔을때 적당한 값의 반올림 deprecated
        {
            /*
            if (30 <= abs(avg_y) && abs(avg_y) < 100) //평균 나올게 0.6이면 1로 올림
            {
                // avg_y = 100 * dir_y;
            }
            */
        }

        // 반응속도를 높이기위해 스크롤을 시작한 초반혹은 방향바꾸고나서는 값을 뻥튀기(이걸로 dist_y 가 1이상이 되지않도록 조심)
        {
            // static double old_dir_y;

            if (last_job.active_cnt_volatile < 4)
            {
                ________DBG_20220314_mkb_mac_scroll_init_accel("%d", (avg_y / 100) * (4 - last_job.active_cnt_volatile) * abs(4 - last_job.active_cnt_volatile));

                if ((avg_y / 100) * pow((4 - last_job.active_cnt_volatile), 2) > 1800)
                {
                    avg_y = 1800;
                }
                else
                {
                    int32_t init_accel = avg_y * pow((4 - last_job.active_cnt_volatile), 2);
                    avg_y = avg_y + init_accel;
                    //________DBG_20220314_mkb_mac_scroll_init_accel("step:%d avg_y:%d, init_accel:%d", last_job.active_cnt_volatile, (avg_y), (init_accel));
                }
                ________DBG_20220314_mkb_mac_scroll_init_accel("step:%d avg_y:%d, ", last_job.active_cnt_volatile, (avg_y));
            }
        }

        //초반 8개 0이 연속으로 들어왔을때 버림 deprecated
        {
            /*
               for (uint8_t i = 0; (i < m_touch_db.data_cnt) && (i < 8); i++)
               {
                   is_stoped += abs(m_touch_db.queue[i].compensate_delta_y);
               }
               if (is_stoped <= 2)
               {
                   // avg_y = 0;
               }
               */
        }

#ifdef ________DBG_20220314_mkb_mac_scroll_sprintf
        {
            ________DBG_20220314_mkb_mac_scroll_sprintf("rawY[%2d]compY[%d]avgY[%4d] :", m_touch_db.queue[0].delta_y, m_touch_db.queue[0].compensate_delta_y, (avg_y));
            for (uint16_t i = 0; i < (avg_y / 10); i++)
            {
                if ((i % 10 == 0 && i != 0) || i > 160)
                {
                    ________DBG_20220314_mkb_mac_scroll_sprintf("%d", i / 10);
                }
                else
                {
                    ________DBG_20220314_mkb_mac_scroll_sprintf("*");
                }
            }
            ________DBG_20220314_mkb_mac_scroll_sprintf("\n");
        }
#endif

        gstr_step.fgr2_y_scroll_move_threshold++;

        //________DBG_20220314_mkb_mac_scroll("%04d vs %d", gstr_step.fgr2_y_scroll_move_threshold, (int16_t)(avg_y));
        uint8_t threshold = 0; // threshold 는 틱을 기준으로 한다. 즉 값에 8ms 를 곱해야함.
        if ((avg_y) == 0)
        {
            threshold = 100;
        }
        else if ((avg_y) < 10)
        {
            threshold = 18;
        }
        else if ((avg_y) < 30)
        {
            threshold = 16;
        }
        else if ((avg_y) < 100) // 100
        {
            threshold = 16;
        }
        else if ((avg_y) < 150) //보정된 값은 여기서부터 시작
        {
            threshold = 15;
        }
        else if ((avg_y) < 200) // 200
        {
            threshold = 13;
        }
        else if ((avg_y) < 250)
        {
            threshold = 11;
        }
        else if ((avg_y) < 300) // 300 여기까지 저속
        {
            threshold = 11;
        }
        else if ((avg_y) < 400)
        {
            threshold = 11;
        }
        else if ((avg_y) < 500)
        {
            threshold = 10;
        }
        else if ((avg_y) < 600)
        {
            threshold = 10;
        }
        else if ((avg_y) < 700)
        {
            threshold = 9;
        }
        else if ((avg_y) < 800)
        {
            threshold = 8;
        }
        else if ((avg_y) < 900)
        {
            threshold = 8;
        }
        else if ((avg_y) < 1000)
        {
            threshold = 7;
        }
        else if ((avg_y) < 1100)
        {
            threshold = 5;
        }
        else if ((avg_y) < 1200)
        {
            threshold = 4;
        }
        else if ((avg_y) < 1300)
        {
            threshold = 3;
        }
        else if ((avg_y) < 1600)
        {
            threshold = 2;
        }
        else if (last_job.active_cnt_volatile < 4)
        {
            threshold = 1;
        }
        else
        {
            threshold = 0;
            dist_y = 2;
        }

        if (abs(gstr_step.fgr2_y_scroll_move_threshold) > threshold && threshold != 100)
        {
            mkb_event_send(EVT_TOUCH_SCROLL, 0, os_vector * dir_y * -1 * abs(dist_y));
            last_job_active(FNGR_2_Y_SCROLL);
            // gstr_step.fgr2_y_scroll_move_threshold = gstr_step.fgr2_y_scroll_move_threshold % threshold; //사용한 나머지값 저장
            gstr_step.fgr2_y_scroll_move_threshold = gstr_step.fgr2_y_scroll_move_threshold = 0; //사용한 나머지값 t삭제
        }
        return;
        break;
    }
    case CENTRAL_IOS:
    {
        os_physicdist_threshold = 24;     // 32;
        os_physicdist_threshold_mult = 1; // 1 + abs(delta_y) / 32.0; //트리거소모 배수
        os_vector = -1;
        os_scrollspeed_multi = 1 + (abs(delta_y) / 24);
        break;
    }
    case CENTRAL_DEX:
    case CENTRAL_ANDROID:
    {
        os_physicdist_threshold = 36;     // 36;
        os_physicdist_threshold_mult = 1; //트리거소모에 가속추가
        os_vector = 1;
        os_scrollspeed_multi = 1;
        int16_t dir_y = 0;
        //     +(abs(delta_y) / 24);

        { //초반 X개만 가지고 방향결정하기(빠른반응속도위해)
            for (uint8_t i = 0; (i < m_touch_db.data_cnt) && (i < 6); i++)
            {
                dir_y += (m_touch_db.queue[i].compensate_delta_y);
                // dir_y += (m_touch_db.queue[i].delta_y);
            }
            dir_y = dir_y / abs(dir_y);
        }
        gstr_step.fgr2_y_scroll_move_threshold += (os_physicdist_threshold_mult * delta_y); // delta 소모

        if (abs(gstr_step.fgr2_y_scroll_move_threshold) > os_physicdist_threshold) //소모한 값이 기준보다크면
        {
            int16_t move_distance = gstr_step.fgr2_y_scroll_move_threshold / os_physicdist_threshold; //움직일거리.
            // mkb_event_send(EVT_TOUCH_SCROLL, 0, move_distance * os_vector * os_scrollspeed_multi);
            mkb_event_send(EVT_TOUCH_SCROLL, 0, dir_y * os_vector);
            last_job_active(FNGR_2_Y_SCROLL);
            gstr_step.fgr2_y_scroll_move_threshold = gstr_step.fgr2_y_scroll_move_threshold % os_physicdist_threshold; //사용한 나머지값 저장

            ________DBG_20220314_mkb_and_scroll("threshold:%d, %d*%d", gstr_step.fgr2_y_scroll_move_threshold, dir_y, os_vector);
        }
        return;
        break;
    }
    case CENTRAL_WINDOWS:
    case CENTRAL_LINUX:
    case CENTRAL_OTHER:
    {
        os_physicdist_threshold = 26;     // 36;
        os_physicdist_threshold_mult = 1; // abs(delta_y); //트리거소모에 가속추가
        os_vector = 1;
        os_scrollspeed_multi = 1 + (abs(delta_y) / 4);
        break;
    }
    default:
    {
        break;
    }
    }

    gstr_step.fgr2_y_scroll_move_threshold += (os_physicdist_threshold_mult * delta_y); // delta 소모

    if (app_timer_cnt_diff_compute(app_timer_cnt_get(), last_job.active_time_stamp) < APP_TIMER_TICKS(3000))
    {
        ________DBG_20220314_mkb_fngr_2_y_scroll_combo("step 0 (lastjob:%d -> %d) cnt:%d", last_job.active_fngr_job[1], last_job.active_fngr_job[0], last_job.active_cnt_volatile);

        if (last_job.active_cnt_volatile == 0)
        {
            ________DBG_20220314_mkb_fngr_2_y_scroll_combo("step 1");
            os_physicdist_threshold = 2;
        }
    }

    if (abs(gstr_step.fgr2_y_scroll_move_threshold) > os_physicdist_threshold) //소모한 값이 기준보다크면
    {
        if (mkb_db_curr_central_type_get() == CENTRAL_IOS)
        {
            if (os_physicdist_threshold == 2)
            {
                ________DBG_20220314_mkb_fngr_2_y_scroll_combo("step 2");
                int16_t move_distance = gstr_step.fgr2_y_scroll_move_threshold / os_physicdist_threshold; //움직일거리.
                mkb_event_send(EVT_TOUCH_SCROLL, 0, delta_y / abs(delta_y) * os_vector * -1);
                mkb_event_send_ms(6, EVT_TOUCH_SCROLL, 0, move_distance * os_vector * os_scrollspeed_multi);
                last_job_active(FNGR_2_Y_SCROLL);
            }
            else
            {
                ________DBG_20220314_mkb_fngr_2_y_scroll_combo("no step");
                int16_t move_distance = gstr_step.fgr2_y_scroll_move_threshold / os_physicdist_threshold; //움직일거리.
                mkb_event_send(EVT_TOUCH_SCROLL, 0, move_distance * os_vector * os_scrollspeed_multi);
                last_job_active(FNGR_2_Y_SCROLL);
            }
            ________DBG_20220314_mkb_fngr_2_y_scroll("SCROLL ACTIVATED!! gstr_type:[%d], active_fngr_job[0]:%d,[1]:%d", m_touch_db.queue[0].gesture.type, last_job.active_fngr_job[0], last_job.active_fngr_job[1]);

            /*________DBG_20211119_mkb_touch_fgr2_detect("(move_distance:%d*os_vector:%d*os_scrollspeed_multi:%d)=[%d], gstr_step.fgr2_y_scroll_move_threshold:%d) > os_physicdist_threshold:%d",
                                                       move_distance, os_vector, os_scrollspeed_multi, move_distance * os_vector * os_scrollspeed_multi, gstr_step.fgr2_y_scroll_move_threshold, os_physicdist_threshold);

            ________LOG_20220216_fngr_2_y_readableity("(move_distance:%d*os_vector:%d*os_scrollspeed_multi:%d)=[%d], gstr_step.fgr2_y_scroll_move_threshold:%d) > os_physicdist_threshold:%d",
                                                      move_distance, os_vector, os_scrollspeed_multi, move_distance * os_vector * os_scrollspeed_multi, gstr_step.fgr2_y_scroll_move_threshold, os_physicdist_threshold);
                                                      */
            gstr_step.fgr2_y_scroll_move_threshold = gstr_step.fgr2_y_scroll_move_threshold % os_physicdist_threshold; //사용한 나머지값 저장
        }
        else
        {

            int16_t move_distance = gstr_step.fgr2_y_scroll_move_threshold / os_physicdist_threshold; //움직일거리.
            mkb_event_send(EVT_TOUCH_SCROLL, 0, move_distance * os_vector * os_scrollspeed_multi);
            last_job_active(FNGR_2_Y_SCROLL);
            ________DBG_20220314_mkb_fngr_2_y_scroll("SCROLL ACTIVATED!! gstr_type:[%d], active_fngr_job[0]:%d,[1]:%d", m_touch_db.queue[0].gesture.type, last_job.active_fngr_job[0], last_job.active_fngr_job[1]);

            /*________DBG_20211119_mkb_touch_fgr2_detect("(move_distance:%d*os_vector:%d*os_scrollspeed_multi:%d)=[%d], gstr_step.fgr2_y_scroll_move_threshold:%d) > os_physicdist_threshold:%d",
                                                       move_distance, os_vector, os_scrollspeed_multi, move_distance * os_vector * os_scrollspeed_multi, gstr_step.fgr2_y_scroll_move_threshold, os_physicdist_threshold);

            ________LOG_20220216_fngr_2_y_readableity("(move_distance:%d*os_vector:%d*os_scrollspeed_multi:%d)=[%d], gstr_step.fgr2_y_scroll_move_threshold:%d) > os_physicdist_threshold:%d",
                                                      move_distance, os_vector, os_scrollspeed_multi, move_distance * os_vector * os_scrollspeed_multi, gstr_step.fgr2_y_scroll_move_threshold, os_physicdist_threshold);
                                                      */
            gstr_step.fgr2_y_scroll_move_threshold = gstr_step.fgr2_y_scroll_move_threshold % os_physicdist_threshold; //사용한 나머지값 저장
        }
    }
}

static bool search_fngr_2_x_scroll()
{
    { //예외적 FALSE
        if (is_safe_from_key_up_ms(600) == false)
        {
            ________DBG_20220314_fgr2_x_scroll_detect("FALSE 1-1");
            return false;
        }
    }

    { //예외적 TRUE
        //연속적 스크롤은 TRUE
        if (last_job.active_cnt_volatile > 4 && last_job.active_fngr_job[0] == FNGR_2_X_SCROLL || gstr_step.fgr2_scroll_step > 8)
        {
            ________DBG_20220314_fgr2_x_scroll_detect("TRUE 2-1");
            return true;
        }
    }

    { //일반적 FALSE

        //방향성 상하일때 FALSE
        if (m_touch_db.directions_cnts[DIR_UP] > 8 || m_touch_db.directions_cnts[DIR_DOWN] > 8 ||
            m_touch_db.directions_cnts[DIR_LEFT_UP] > 8 || m_touch_db.directions_cnts[DIR_LEFT_DOWN] > 8 ||
            m_touch_db.directions_cnts[DIR_RIGHT_UP] > 8 || m_touch_db.directions_cnts[DIR_RIGHT_DOWN] > 8)
        {
            ________DBG_20220314_fgr2_x_scroll_detect("FALSE 3-1");
            return false;
        }

        //초기 손가락 최소 갯수에따라 FALSE
        if ((m_touch_db.data_cnt < 5) || (m_touch_db.fingers_cnts[1] > 20) || (m_touch_db.fingers_cnts[2] < 30) || (m_touch_db.fingers_cnts[3] > 0) || (m_touch_db.fingers_cnts[4] > 0))
        {
            ________DBG_20220314_fgr2_x_scroll_detect("FALSE 3-2");
            return false;
        }
    }

    { //일반적 TRUE

        // fgr0float을 포함한 3초이내 연속적 X scrol 일때
        if ((last_job.active_fngr_job[1] == FNGR_2_X_SCROLL) && (app_timer_cnt_diff_compute(app_timer_cnt_get(), last_job.active_time_stamp) < APP_TIMER_TICKS(3000)))
        {
            ________DBG_20220314_fgr2_x_scroll_detect("TRUE 4-1");
            // return true;
        }

        //픽스아트 제스처가 동작할때
        if (m_touch_db.queue[0].gesture.type == PIXART_GSTR_TYPE_TWO_HSCRL)
        {
            ________DBG_20220314_fgr2_x_scroll_detect("TRUE 4-2");
            return true;
        }
    }

    //해당없으면 다른동작으로 넘김
    ________DBG_20220314_fgr2_x_scroll_detect("FALSE 5-1");

    return false;
}

static void detect_fngr_2_x_scroll()
{
    last_job_detect(FNGR_2_X_SCROLL);
    active_fngr_2_x_scroll();
}

static void active_fngr_2_x_scroll()
{
    ________DBG_20211119_mkb_touch_detect_fgr("fgr2 x");

    uint8_t os_physicdist_threshold = 128;   //쓰레숄드 값. 트리거에 필요한 손가락dist값.
    double os_physicdist_threshold_mult = 1; //쓰레숄드 소모 증가비율. 클수록 트리거에 필요한 dist 감소.
    uint8_t os_scrollspeed_multi = 1;        // HID send 시의 distance 배율. delta 혹은 속도값을곱하여 가속한다.
    int8_t os_vector = 1;
    int16_t delta_x = m_touch_db.queue[0].delta_x;

    switch (mkb_db_curr_central_type_get())
    {
    case CENTRAL_MAC:
    case CENTRAL_IOS:
    {
        os_physicdist_threshold = 32;
        os_physicdist_threshold_mult = 1; // 1 + abs(delta_y) / 32.0; //트리거소모 배수
        os_vector = 1;
        os_scrollspeed_multi = 1; // 1 + (abs(delta_y) / 32);
        break;
    }
    case CENTRAL_DEX:
    {
        os_physicdist_threshold = 36;
        os_physicdist_threshold_mult = 1; // abs(delta_y); //트리거소모에 가속추가
        os_vector = 1;
        os_scrollspeed_multi = 1; // 1 + (abs(delta_y) / 16);
        break;
    }
    case CENTRAL_WINDOWS:
    case CENTRAL_LINUX:
    case CENTRAL_OTHER:
    {
        os_physicdist_threshold = 36;
        os_physicdist_threshold_mult = 1; // abs(delta_y); //트리거소모에 가속추가
        os_vector = -1;
        os_scrollspeed_multi = 1; // 1 + (abs(delta_y) / 16);
        break;
    }
    case CENTRAL_ANDROID:
    {
        os_physicdist_threshold = 36;
        os_physicdist_threshold_mult = 1; // abs(delta_y); //트리거소모에 가속추가
        os_vector = 1;
        os_scrollspeed_multi = 1; // 1 + (abs(delta_y) / 16);
        break;
    }
    default:
    {
        break;
    }
    }

    gstr_step.fgr2_x_scroll_move_threshold += (os_physicdist_threshold_mult * delta_x); // delta 소모

    ________DBG_20211119_mkb_touch_fgr2_detect("fngrr2scroll_move_threshold:%d += os_physicdist_threshold_mult:%d*delta_y:%d[%d]",
                                               gstr_step.fgr2_x_scroll_move_threshold, os_physicdist_threshold_mult, delta_x, os_physicdist_threshold_mult * delta_x);

    if (abs(gstr_step.fgr2_x_scroll_move_threshold) > os_physicdist_threshold) //소모한 값이 기준보다크면
    {
        int16_t move_distance = gstr_step.fgr2_x_scroll_move_threshold / os_physicdist_threshold;                  //움직일거리.
        gstr_step.fgr2_x_scroll_move_threshold = gstr_step.fgr2_x_scroll_move_threshold % os_physicdist_threshold; //사용한 나머지값 저장
        mkb_event_send(EVT_TOUCH_SCROLL, move_distance * os_vector * os_scrollspeed_multi, 0);
        last_job_active(FNGR_2_X_SCROLL);

        ________DBG_20211119_mkb_touch_fgr2_detect("(move_distance:%d*os_vector:%d*os_scrollspeed_multi:%d)=[%d], gstr_step.fgr2_x_scroll_move_threshold:%d) > os_physicdist_threshold:%d",
                                                   move_distance, os_vector, os_scrollspeed_multi, move_distance * os_vector * os_scrollspeed_multi, gstr_step.fgr2_x_scroll_move_threshold, os_physicdist_threshold);
    }
}

static bool search_fngr_2_pinch()
{
    if (is_safe_from_key_up_ms(600) == false)
    {
        return false;
    }

    if ((m_touch_db.fingers_cnts[1] > 8) || (m_touch_db.fingers_cnts[2] <= 45) || (m_touch_db.fingers_cnts[3] > 3) || (m_touch_db.fingers_cnts[4] > 0))
    {
        return false;
    }

    if (last_job.detect_fngr_job[0] == FNGR_2_X_SCROLL || last_job.detect_fngr_job[0] == FNGR_2_Y_SCROLL)
    {
        if (app_timer_cnt_diff_compute(app_timer_cnt_get(), last_job.detect_time_stamp) < APP_TIMER_TICKS(200))
        {
            return false;
        }
    }

    if (last_job.active_fngr_job[0] == FNGR_2_PINCH)
    {
        return true;
    }
    if (abs(get_is_pinch_sum(40)) > 20 && abs(get_deltaarea_sum(40) > 30))
    {
        return true;
    }
    if (m_touch_db.queue[0].gesture.type == PIXART_GSTR_TYPE_TWO_PINCH && m_touch_db.fingers_cnts[1] < 15)
    {
        return true;
    }

    return false;
}

static void detect_fngr_2_pinch()
{
    last_job_detect(FNGR_2_PINCH);
    uint8_t os_physicdist_threshold = 80; //쓰레숄드 값. 트리거에 필요한 손가락dist값.
    int8_t os_vector = 1;
    int16_t delta_area = m_touch_db.queue[0].delta_area; //제스처에서 나오는 dist값
    ________LOG_20220216_fngr_2_xy_pinch("active pinch (delta_area:%d)", m_touch_db.queue[0].delta_area);

    switch (mkb_db_curr_central_type_get())
    {
    case CENTRAL_MAC:
    case CENTRAL_IOS:
    {
        os_vector = 1;
        break;
    }
    case CENTRAL_WINDOWS:
    case CENTRAL_LINUX:
    case CENTRAL_DEX:
    case CENTRAL_OTHER:
    {
        os_vector = 1;
        break;
    }
    case CENTRAL_ANDROID:
    {
        os_vector = 1;
        break;
    }
    default:
    {
        break;
    }
    }

    gstr_step.fgr2_pinch_inout_step += (delta_area); // delta 소모

    if (abs(gstr_step.fgr2_pinch_inout_step) > os_physicdist_threshold) //소모한 값이 기준보다크면
    {
        ________LOG_20220216_fngr_2_xy_pinch("!!!!!!!!!!!!!!pinch active : %d", last_job.active_cnt_static);

        if (gstr_step.fgr2_pinch_inout_step < 0) //작아짐 zoomout
        {
            active_fngr_2_pinch(PINCH_NARR);
        }
        else if (gstr_step.fgr2_pinch_inout_step > 0) //커짐 zoom in
        {
            active_fngr_2_pinch(PINCH_WIDE);
        }
        gstr_step.fgr2_pinch_inout_step = gstr_step.fgr2_pinch_inout_step % os_physicdist_threshold; //사용한 나머지값 저장
        gstr_step.fgr0_is_need_mod_clear = true;
    }
}

static void active_fngr_2_pinch(pinch_t pinch)
{
    last_job_active(FNGR_2_PINCH);
    uint8_t input_delay = 0;
    if (pinch == PINCH_NARR) //작아지는동작
    {
        switch (mkb_db_curr_central_type_get())
        {
        case CENTRAL_IOS:
        case CENTRAL_MAC:
        {
            if (gstr_step.fgr0_is_need_mod_clear == false)
            {
                mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_L_ALT);
                gstr_step.fgr0_is_need_mod_clear == true;
            }
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_MINUS);
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_MINUS);
            break;
        }
        case CENTRAL_DEX:
        {

            if (gstr_step.fgr0_is_need_mod_clear == false)
            {
                mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_L_CTRL);
                gstr_step.fgr0_is_need_mod_clear == true;
            }
            mkb_event_send_ms((input_delay++) * 8, EVT_TOUCH_SCROLL, 0, -1);
            break;
        }
        case CENTRAL_ANDROID:
        {
            if (gstr_step.fgr0_is_need_mod_clear == false)
            {
                mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_L_CTRL);
                gstr_step.fgr0_is_need_mod_clear == true;
            }
            else
            {
                // mkb_event_send_ms((input_delay++) * 8, EVT_TOUCH_SCROLL, 0, -1);
                mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_MINUS);
                mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_MINUS);
            }
            break;
        }
        case CENTRAL_WINDOWS:
        case CENTRAL_LINUX:
        case CENTRAL_OTHER:
        {
            if (gstr_step.fgr0_is_need_mod_clear == false)
            {
                mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_L_CTRL);
                gstr_step.fgr0_is_need_mod_clear == true;
            }
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_MINUS);
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_MINUS);
            break;
        }
        default:
        {
            break;
        }
        }
    }
    else if (pinch == PINCH_WIDE) //커지는 동작
    {
        switch (mkb_db_curr_central_type_get())
        {
        case CENTRAL_IOS:
        case CENTRAL_MAC:
        {
            if (gstr_step.fgr0_is_need_mod_clear == false)
            {
                mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_L_ALT);
                gstr_step.fgr0_is_need_mod_clear == true;
            }
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_EQUAL);
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_EQUAL);
            break;
        }
        case CENTRAL_DEX:
        {

            if (gstr_step.fgr0_is_need_mod_clear == false)
            {
                mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_L_CTRL);
                gstr_step.fgr0_is_need_mod_clear == true;
            }
            mkb_event_send_ms((input_delay++) * 8, EVT_TOUCH_SCROLL, 0, 1);
            break;
        }
        case CENTRAL_ANDROID:
        {
            if (gstr_step.fgr0_is_need_mod_clear == false)
            {
                mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_L_CTRL);
                gstr_step.fgr0_is_need_mod_clear == true;
            }
            else
            {
                // mkb_event_send_ms((input_delay++) * 8, EVT_TOUCH_SCROLL, 0, 1);
                mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_EQUAL);
                mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_EQUAL);
            }
            break;
        }
        case CENTRAL_WINDOWS:
        case CENTRAL_LINUX:
        case CENTRAL_OTHER:
        {
            if (gstr_step.fgr0_is_need_mod_clear == false)
            {
                mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_L_CTRL);
                gstr_step.fgr0_is_need_mod_clear == true;
            }

            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_EQUAL);
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_EQUAL);
            break;
        }
        default:
        {
            break;
        }
        }
    }
}

/*_________ ______   _        _______    ______
\__   __/(  __  \ ( \      (  ____ \  / ___  \
   ) (   | (  \  )| (      | (    \/  \/   \  \
   | |   | |   ) || |      | (__  _____  ___) /
   | |   | |   | || |      |  __)(_____)(___ (
   | |   | |   ) || |      | (              ) \
___) (___| (__/  )| (____/\| (____/\  /\___/  /
\_______/(______/ (_______/(_______/  \______/ */
static bool search_idle_3_tap()
{
    //예외 FALSE
    if (!(is_safe_from_key_up_ms(1000) && is_safe_from_red_key_up_ms(1000) && is_safe_from_multttouch_up_ms(200)))
    {
        ________DBG_20220314_idle_module_refactory("search step 1 ");
        return false;
    }

    //예외 TRUE
    if (m_touch_db.founded_gesture == PIXART_GSTR_TYPE_THREE_TAP_RELEASE)
    {
        // return true;
    }

    // AND 조건들
    //데이터갯수
    if (!(5 <= m_touch_db.data_cnt && m_touch_db.data_cnt <= 25))
    {
        return false;
    }
    //손가락 갯수
    if (!(4 < m_touch_db.fingers_cnts[3] && m_touch_db.fingers_cnts[3] < 26))
    {
        return false;
    }
    if (!(m_touch_db.fingers_cnts[0] == 1 && m_touch_db.fingers_cnts[4] == 0))
    {
        return false;
    }
    //방향
    if (!(m_touch_db.directions_cnts[DIR_NONE] + 4 > m_touch_db.directions_cnts[DIR_RIGHT] + m_touch_db.directions_cnts[DIR_RIGHT_UP] + m_touch_db.directions_cnts[DIR_UP] + m_touch_db.directions_cnts[DIR_LEFT_UP] + m_touch_db.directions_cnts[DIR_LEFT] + m_touch_db.directions_cnts[DIR_LEFT_DOWN] + m_touch_db.directions_cnts[DIR_DOWN] + m_touch_db.directions_cnts[DIR_RIGHT_DOWN]))
    {
        return false;
    }
    //거리
    if (!(abs(m_touch_db.end_x - m_touch_db.start_x) + abs(m_touch_db.end_y - m_touch_db.start_y) < 80))
    {
        ________DBG_20220314_idle_module_refactory("search step 3 ");
        return false;
    }
    //시간
    if (!(APP_TIMER_TICKS(80) < abs(m_touch_db.end_time - m_touch_db.start_time) && abs(m_touch_db.end_time - m_touch_db.start_time) < APP_TIMER_TICKS(220)))
    {
        return false;
    }

    return true;
}

static void detect_idle_3_tap()
{
    last_job_detect(IDLE_3_TAP);
    active_idle_3_tap();
}

static void active_idle_3_tap()
{
    ________DBG_20211119_mkb_touch_detect_idle("3-1tap");

    uint8_t input_delay = 0;
    last_job_active(IDLE_3_TAP);

    switch (mkb_db_curr_central_type_get())
    {
    case CENTRAL_MAC:
    {
        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_L_CTRL);
        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_L_ALT);
        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_D);

        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_D);
        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_L_ALT);
        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_L_CTRL);
        break;
    }

    case CENTRAL_IOS:
    {
        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_L_ALT);
        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_L_GUI);
        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_D);

        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_D);
        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_L_GUI);
        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_L_ALT);
        break;
    }

    case CENTRAL_ANDROID:
    {
        mkb_event_send_ms((input_delay++) * 8, EVT_HID_INPUT_CONSUMER, m_android_menu, sizeof(m_android_menu));
        mkb_event_send_ms((input_delay++) * 8, EVT_HID_INPUT_CONSUMER, m_consumer_no_cmd, sizeof(m_consumer_no_cmd));
        break;
    }

    case CENTRAL_DEX:
    {
        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_L_GUI);
        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_L_GUI);
        break;
    }

    case CENTRAL_WINDOWS:
    case CENTRAL_LINUX:
    case CENTRAL_OTHER:
    {
        static bool flag = false;

        if (flag == false)
        {
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_L_GUI);
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_S);
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_S);
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_L_GUI);
            flag = true;
        }
        else
        {
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_ESC);
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_ESC);
            flag = false;
        }
        break;
    }
    }
}

static bool search_idle_3_up_swipe()
{

    //예외 FALSE
    if (!(is_safe_from_key_up_ms(800) && is_safe_from_red_key_up_ms(1000) && is_safe_from_multttouch_up_ms(200)))
    {
        ________DBG_20211201_idle_2_swipe("false");
        return false;
    }

    //예외 TRUE
    if (m_touch_db.founded_gesture == PIXART_GSTR_TYPE_THREE_UP)
    {
        return true;
    }

    // AND 조건들
    //데이터갯수
    if (!(10 <= m_touch_db.data_cnt && m_touch_db.data_cnt <= 42))
    {
        ________DBG_20211201_idle_2_swipe("false");
        return false;
    }
    //손가락 갯수
    if (!(2 <= m_touch_db.fingers_cnts[3] && m_touch_db.fingers_cnts[3] <= 42))
    {
        ________DBG_20211201_idle_2_swipe("false");
        return false;
    }
    if (!(m_touch_db.fingers_cnts[0] == 1 && m_touch_db.fingers_cnts[4] == 0))
    {
        ________DBG_20211201_idle_2_swipe("false");
        return false;
    }

    //방향
    if (!(m_touch_db.directions_cnts[DIR_LEFT_UP] + m_touch_db.directions_cnts[DIR_UP] * 2 + m_touch_db.directions_cnts[DIR_RIGHT_UP] >= 20))
    {
        ________DBG_20211201_idle_2_swipe("false");
        return false;
    }

    if (!(m_touch_db.directions_cnts[DIR_NONE] / 2 < m_touch_db.directions_cnts[DIR_LEFT_UP] + m_touch_db.directions_cnts[DIR_RIGHT_UP] + m_touch_db.directions_cnts[DIR_UP]))
    {
        ________DBG_20211201_idle_2_swipe("false");
        return false;
    }

    //거리
    if (!(30 <= abs(m_touch_db.end_y - m_touch_db.start_y) && abs(m_touch_db.end_y - m_touch_db.start_y) <= 600))
    {
        ________DBG_20211201_idle_2_swipe("false");
        return false;
    }

    if (!(abs(m_touch_db.end_y - m_touch_db.start_y) > abs(m_touch_db.end_x - m_touch_db.start_x) * 2))
    {
        ________DBG_20211201_idle_2_swipe("false");
        return false;
    }

    //넓이변화(손가락이 여러개면 넓이변화 불안정함)

    //시간
    if (!(APP_TIMER_TICKS(80) < abs(m_touch_db.end_time - m_touch_db.start_time) && abs(m_touch_db.end_time - m_touch_db.start_time) < APP_TIMER_TICKS(300)))
    {
        ________DBG_20211201_idle_2_swipe("false");
        return false;
    }

    return true;

    /*
    if (m_touch_db.data_cnt >= 20 && m_touch_db.fingers_cnts[3] >= 6)
    {
    }
    else
    {
        return false;
    }

    if (m_touch_db.founded_gesture == PIXART_GSTR_TYPE_THREE_UP || m_touch_db.directions_cnts[DIR_UP] >= 16)
    {
    }
    else
    {
        return false;
    }

    if (m_touch_db.fingers_cnts[4] == 0)
    {
    }
    else
    {
        return false;
    }

    if (is_safe_from_key_up_ms(800) && is_safe_from_gstr_up_ms(400))
    {
    }
    else
    {
        return false;
    }

    if (APP_TIMER_TICKS(m_touch_db.end_time - m_touch_db.start_time) < 50)
    {
        ________DBG_20211119_mkb_touch_3up_test("delta time block");
        return false;
    }
    if (abs(m_touch_db.end_y - m_touch_db.start_y) < 180)
    {
        ________DBG_20211119_mkb_touch_3up_test("dist y  block :%d", delta_y);
        return false;
    }

    return true;
    */
}

static void detect_idle_3_up_swipe()
{
    active_idle_3_up_swipe();
}

static bool is3uppded = false;
static void active_idle_3_up_swipe()
{
    uint8_t input_delay = 0;
    ________DBG_20211119_mkb_touch_detect_fgr("fgr3 v");
    switch (mkb_db_curr_central_type_get())
    {
    case CENTRAL_MAC:
    {
        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_L_CTRL);
        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_UP);

        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_UP);
        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_L_CTRL);
        break;
    }
    case CENTRAL_IOS:
    {
        mkb_event_send_ms((input_delay++) * 8, EVT_HID_INPUT_CONSUMER, m_iphone_home, sizeof(m_iphone_home));
        mkb_event_send_ms((input_delay++) * 8, EVT_HID_INPUT_CONSUMER, m_consumer_no_cmd, sizeof(m_consumer_no_cmd));

        mkb_event_send_ms((input_delay++) * 8, EVT_HID_INPUT_CONSUMER, m_iphone_home, sizeof(m_iphone_home));
        mkb_event_send_ms((input_delay++) * 8, EVT_HID_INPUT_CONSUMER, m_consumer_no_cmd, sizeof(m_consumer_no_cmd));
        break;
    }
    break;
    case CENTRAL_WINDOWS:
    case CENTRAL_LINUX:
    case CENTRAL_OTHER:
    case CENTRAL_DEX:
    case CENTRAL_ANDROID:
    {
        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_L_GUI);
        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_TAB);

        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_TAB);
        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_L_GUI);
        is3uppded = true;
        break;
    }
    default:
        break;
    }
}

static bool search_idle_3_down_swipe()
{
    return false;
}

static void detect_idle_3_down_swipe()
{
    active_idle_3_down_swipe();
}

static void active_idle_3_down_swipe()
{
    uint8_t input_delay = 0;
    ________DBG_20211119_mkb_touch_detect_fgr("fgr3 v");
    switch (mkb_db_curr_central_type_get())
    {
    case CENTRAL_MAC:
    {
        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_L_CTRL);
        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_DOWN);

        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_DOWN);
        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_L_CTRL);
        break;
    }
    case CENTRAL_IOS:
    {
        mkb_event_send_ms((input_delay++) * 8, EVT_HID_INPUT_CONSUMER, m_iphone_home, sizeof(m_iphone_home));
        mkb_event_send_ms((input_delay++) * 8, EVT_HID_INPUT_CONSUMER, m_consumer_no_cmd, sizeof(m_consumer_no_cmd));
        break;
    }
    case CENTRAL_ANDROID:
    case CENTRAL_DEX:
    {
        if (is3uppded == false)
        {

            mkb_event_send_ms((input_delay++) * 8, EVT_HID_INPUT_CONSUMER, m_android_home, sizeof(m_android_home));
            mkb_event_send_ms((input_delay++) * 8, EVT_HID_INPUT_CONSUMER, m_consumer_no_cmd, sizeof(m_consumer_no_cmd));
        }
        else
        {
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_ESC);
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_ESC);
            is3uppded = false;
        }
        break;
    }
    case CENTRAL_WINDOWS:
    case CENTRAL_LINUX:
    case CENTRAL_OTHER:
    {
        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_L_GUI);
        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_D);

        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_D);
        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_L_GUI);
        break;
    }
    default:
        break;
    }
}

static bool search_idle_3_x_swipe()
{
    return false;
}

static void detect_idle_3_x_swipe()
{
}
static void active_idle_3_x_swipe()
{
}
/*
 _______  _        _______  _______       ______
(  ____ \( (    /|(  ____ \(  ____ )     / ___  \
| (    \/|  \  ( || (    \/| (    )|     \/   \  \
| (__    |   \ | || |      | (____)| _____  ___) /
|  __)   | (\ \) || | ____ |     __)(_____)(___ (
| (      | | \   || | \_  )| (\ (              ) \
| )      | )  \  || (___) || ) \ \__     /\___/  /
|/       |/    )_)(_______)|/   \__/     \______/ */

static bool search_fngr_3_xy_scroll()
{
    if ((m_touch_db.fingers_cnts[1] > 8) || (m_touch_db.fingers_cnts[2] > 8) || (m_touch_db.fingers_cnts[4] > 0))
    {
        return false;
    }
    if (app_timer_cnt_diff_compute(app_timer_cnt_get(), m_touch_db.start_time) < APP_TIMER_TICKS(200)) //시작에 필요한 시간
    {
        return false;
    }

    if (last_job.active_fngr_job[0] == FNGR_3_XY_SCROLL)
    {
        return true;
    }
    if (m_touch_db.queue[0].gesture.type == PIXART_GSTR_TYPE_THREE_MOVE)
    {
        return true;
    }
    if (m_touch_db.fingers_cnts[3] >= 45)
    {
        return true;
    }

    return false;
}

static void detect_fngr_3_xy_scroll()
{
    last_job_detect(FNGR_3_XY_SCROLL);
    ________DBG_tmp_3scroll_test("neddclear:%d", gstr_step.fgr0_is_need_mod_clear);

    touch_db_t *touch_db;
    touch_db = &m_touch_db;
    int16_t os_x_distant_threshold = 128;
    int16_t os_y_distant_threshold = 100;
    int8_t central_vector = 1;

    int16_t delta_x = m_touch_db.queue[0].delta_x;
    int16_t delta_y = m_touch_db.queue[0].delta_y;

    gstr_step.fgr3_x_scroll_move_threshold += (delta_x + (abs(delta_x) * delta_x) / 24);
    gstr_step.fgr3_y_scroll_move_threshold += (delta_y + (abs(delta_y) * delta_y) / 12);

    if (abs(gstr_step.fgr3_x_scroll_move_threshold) > os_x_distant_threshold)
    {
        ________DBG_20211119_mkb_touch_fgr2_detect("ymove");

        ________DBG_20211119_mkb_touch_detect_fgr("3 finger leftriwght move ");
        if (touch_db->directions_cnts[DIR_LEFT] > touch_db->directions_cnts[DIR_RIGHT] + 10)
        {
            ________DBG_20220302_test3move("3fngr left");
            active_fngr_3_xy_scroll(DIR_LEFT);
        }
        else if (touch_db->directions_cnts[DIR_RIGHT] > touch_db->directions_cnts[DIR_LEFT] + 10)
        {
            ________DBG_20220302_test3move("3fngr right");
            active_fngr_3_xy_scroll(DIR_RIGHT);
        }
        else
        {
        }
        gstr_step.fgr3_x_scroll_move_threshold = gstr_step.fgr3_x_scroll_move_threshold % os_x_distant_threshold;
    }

    if (abs(gstr_step.fgr3_y_scroll_move_threshold) > os_y_distant_threshold)
    {
        ________DBG_20211119_mkb_touch_fgr2_detect("ymove");

        // mkb_event_send(EVT_TOUCH_SCROLL, 0, move_distance*move_distance*central_vector);
        // mkb_event_send(EVT_TOUCH_SCROLL, 0, move_distance*central_vector);
        ________DBG_20211119_mkb_touch_detect_fgr("3 finger leftriwght move ");
        if (touch_db->directions_cnts[DIR_DOWN] > touch_db->directions_cnts[DIR_UP] + 10)
        {
            ________DBG_20220302_test3move("3fngr down");
            active_fngr_3_xy_scroll(DIR_DOWN);
        }
        else if (touch_db->directions_cnts[DIR_UP] > touch_db->directions_cnts[DIR_DOWN] + 10)
        {
            ________DBG_20220302_test3move("3fngr up");
            active_fngr_3_xy_scroll(DIR_UP);
        }
        else
        {
        }
        gstr_step.fgr3_y_scroll_move_threshold = gstr_step.fgr3_y_scroll_move_threshold % os_y_distant_threshold;
    }
}

static void active_fngr_3_xy_scroll(direction_t direction)
{
    last_job_active(FNGR_3_XY_SCROLL);
    uint8_t input_delay = 0;
    switch (mkb_db_curr_central_type_get())
    {
    case CENTRAL_DEX:
    {
        if (gstr_step.fgr0_is_need_mod_clear == false)
        {
            gstr_step.fgr0_is_need_mod_clear = true;
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_L_ALT);
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_TAB);
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_TAB);
        }
        if (direction == DIR_RIGHT)
        {
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_RIGHT);
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_RIGHT);
        }
        else if (direction == DIR_LEFT)
        {
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_LEFT);
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_LEFT);
        }
        else if (direction == DIR_UP)
        {
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_UP);
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_UP);
        }
        else if (direction == DIR_DOWN)
        {
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_DOWN);
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_DOWN);
        }
        break;
    }
    case CENTRAL_WINDOWS:
    case CENTRAL_ANDROID:
    {
        static bool odd_flag = false;
        if (gstr_step.fgr0_is_need_mod_clear == false)
        {
            gstr_step.fgr0_is_need_mod_clear = true;
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_L_ALT);
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_TAB);
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_TAB);
        }
        if (direction == DIR_RIGHT)
        {
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_RIGHT);
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_RIGHT);
        }
        else if (direction == DIR_LEFT)
        {
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_LEFT);
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_LEFT);
        }
        else if (direction == DIR_UP)
        {
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_UP);
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_UP);
        }
        else if (direction == DIR_DOWN)
        {
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_DOWN);
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_DOWN);
        }
        break;
    }

    case CENTRAL_MAC:
    case CENTRAL_IOS:
    case CENTRAL_LINUX:
    case CENTRAL_OTHER:
    {
        if (gstr_step.fgr0_is_need_mod_clear == false)
        {
            gstr_step.fgr0_is_need_mod_clear = true;
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_L_ALT);
        }
        if (direction == DIR_RIGHT)
        {
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_TAB);
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_TAB);
        }
        else if (direction == DIR_LEFT)
        {
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_L_SHFT);
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_TAB);
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_TAB);
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_L_SHFT);
        }
        break;
    }

    default:
        break;
    }
}

static void active_fngr_3_x_scroll() // deprecated
{
    ________DBG_tmp_3scroll_test("neddclear:%d", gstr_step.fgr0_is_need_mod_clear);
    uint8_t input_delay = 0;
    touch_db_t *touch_db;
    touch_db = &m_touch_db;
    int16_t os_distant_threshold = 128;
    int8_t central_vector = 1;

    int16_t delta_x = m_touch_db.queue[0].delta_x;

    gstr_step.fgr3_x_scroll_move_threshold += (delta_x + (abs(delta_x) * delta_x) / 24);

    if (touch_db->directions_cnts[DIR_LEFT] > touch_db->directions_cnts[DIR_RIGHT] + 20)
    {
        central_vector *= -1;
    }
    else if (touch_db->directions_cnts[DIR_LEFT] + 20 < touch_db->directions_cnts[DIR_RIGHT])
    {
        central_vector *= 1;
    }
    else
    {
        return;
    }

    if (abs(gstr_step.fgr3_x_scroll_move_threshold) > os_distant_threshold)
    {
        ________DBG_20211119_mkb_touch_fgr2_detect("ymove");
        gstr_step.fgr3_x_scroll_move_threshold = gstr_step.fgr3_x_scroll_move_threshold % os_distant_threshold;
        // mkb_event_send(EVT_TOUCH_SCROLL, 0, move_distance*move_distance*central_vector);
        // mkb_event_send(EVT_TOUCH_SCROLL, 0, move_distance*central_vector);
        ________DBG_20211119_mkb_touch_detect_fgr("3 finger leftriwght move ");
        uint8_t input_delay = 0;

        switch (mkb_db_curr_central_type_get())
        {
        case CENTRAL_DEX:
        {
            if (gstr_step.fgr0_is_need_mod_clear == false)
            {
                gstr_step.fgr0_is_need_mod_clear = true;
                mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_L_ALT);
                mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_TAB);
            }
            if (central_vector == 1)
            {
                mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_RIGHT);
                mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_RIGHT);
            }
            else if (central_vector == -1)
            {
                mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_LEFT);
                mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_LEFT);
            }

            break;
        }
        case CENTRAL_ANDROID:
        {
            static bool odd_flag = false;
            if (gstr_step.fgr0_is_need_mod_clear == false)
            {
                gstr_step.fgr0_is_need_mod_clear = true;
                mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_L_ALT);
                mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_TAB);
                mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_TAB);
            }
            if (central_vector == 1)
            {

                {

                    mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_RIGHT);
                    mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_RIGHT);
                }
            }
            else if (central_vector == -1)
            {

                {

                    mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_LEFT);
                    mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_LEFT);
                }
            }
        }
        case CENTRAL_MAC:
        case CENTRAL_IOS:
        case CENTRAL_WINDOWS:
        case CENTRAL_LINUX:

        case CENTRAL_OTHER:
        {
            if (gstr_step.fgr0_is_need_mod_clear == false)
            {
                gstr_step.fgr0_is_need_mod_clear = true;
                mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_L_ALT);
            }

            if (central_vector == 1)
            {
                mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_L_SHFT);
            }
            else if (central_vector == -1)
            {
                mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_L_SHFT);
            }

            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_TAB);
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_TAB);
            break;
        }

        default:
            break;
        }
    }
}

/*
_________ ______   _        _______         ___
\__   __/(  __  \ ( \      (  ____ \       /   )
   ) (   | (  \  )| (      | (    \/      / /) |
   | |   | |   ) || |      | (__  _____  / (_) (_
   | |   | |   | || |      |  __)(_____)(____   _)
   | |   | |   ) || |      | (               ) (
___) (___| (__/  )| (____/\| (____/\         | |
\_______/(______/ (_______/(_______/         (_)  */

static bool search_idle_4_tap()
{
    return false;
}

static void detect_idle_4_tap()
{
}

static void active_idle_4_tap()
{
    uint8_t input_delay = 0;
    ________DBG_20211119_mkb_touch_detect_idle("4-1tap");
    switch (mkb_db_curr_central_type_get())
    {
    case CENTRAL_MAC:
    {
        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_L_CTRL);
        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_L_ALT);
        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_D);

        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_D);
        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_L_CTRL);
        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_L_ALT);
        break;
    }
    case CENTRAL_IOS:
    {
        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_L_ALT);
        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_L_GUI);
        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_D);

        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_D);
        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_L_GUI);
        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_L_ALT);
        break;
    }
    case CENTRAL_ANDROID:
    {
        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_L_GUI);
        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_L_GUI);
        break;
    }
    case CENTRAL_DEX:
    {
        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_L_GUI);
        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_L_GUI);
        break;
    }
    case CENTRAL_WINDOWS:
    case CENTRAL_LINUX:
    case CENTRAL_OTHER:
    {
        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_L_GUI);
        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_A);
        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_A);
        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_L_GUI);
        break;
    }
    }
}

static bool search_idle_4_up_swipe()
{
    return false;
}

static void detect_idle_4_up_swipe()
{
}

static void active_idle_4_up_swipe()
{
    uint8_t input_delay = 0;
    ________DBG_20211119_mkb_touch_detect_fgr("fgr4 v");
    switch (mkb_db_curr_central_type_get())
    {
    case CENTRAL_MAC:
    {
        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_L_CTRL);
        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_UP);

        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_UP);
        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_L_CTRL);
        break;
    }
    case CENTRAL_IOS:
    {
        mkb_event_send_ms((input_delay++) * 8, EVT_HID_INPUT_CONSUMER, m_iphone_home, sizeof(m_iphone_home));
        mkb_event_send_ms((input_delay++) * 8, EVT_HID_INPUT_CONSUMER, m_consumer_no_cmd, sizeof(m_consumer_no_cmd));

        mkb_event_send_ms((input_delay++) * 8, EVT_HID_INPUT_CONSUMER, m_iphone_home, sizeof(m_iphone_home));
        mkb_event_send_ms((input_delay++) * 8, EVT_HID_INPUT_CONSUMER, m_consumer_no_cmd, sizeof(m_consumer_no_cmd));
        break;
    }
    case CENTRAL_DEX:
    {
        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_L_CTRL);
        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_L_GUI);
        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_UP);

        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_UP);
        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_L_GUI);
        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_L_CTRL);
        break;
    }
    case CENTRAL_ANDROID:
    {
        break;
    }
    case CENTRAL_WINDOWS:
    case CENTRAL_LINUX:
    case CENTRAL_OTHER:
    {
        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_L_GUI);
        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_TAB);

        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_TAB);
        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_L_GUI);
        break;
    }

    default:
        break;
    }
}

static bool search_idle_4_down_swipe()
{
    return false;
}

static void detect_idle_4_down_swipe()
{
}

static void active_idle_4_down_swipe()
{
    uint8_t input_delay = 0;
    ________DBG_20211119_mkb_touch_detect_fgr("fgr4 v");
    switch (mkb_db_curr_central_type_get())
    {
    case CENTRAL_MAC:
    {
        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_L_CTRL);
        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_DOWN);

        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_DOWN);
        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_L_CTRL);
        break;
    }
    case CENTRAL_IOS:
    {
        mkb_event_send_ms((input_delay++) * 8, EVT_HID_INPUT_CONSUMER, m_iphone_home, sizeof(m_iphone_home));
        mkb_event_send_ms((input_delay++) * 8, EVT_HID_INPUT_CONSUMER, m_consumer_no_cmd, sizeof(m_consumer_no_cmd));
        break;
    }
    case CENTRAL_ANDROID:
    {
        break;
    }
    case CENTRAL_DEX:
    {
        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_L_CTRL);
        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_L_GUI);
        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_DOWN);

        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_DOWN);
        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_L_GUI);
        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_L_CTRL);
        break;
    }
    case CENTRAL_WINDOWS:
    case CENTRAL_LINUX:
    case CENTRAL_OTHER:
    {
        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_ESC);
        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_ESC);
        break;
    }
    default:
        break;
    }
}

static bool search_idle_4_x_swipe()
{
    return false;
}

static void detect_idle_4_x_swipe()
{
}

static void active_idle_4_x_swipe(bool isLeftorRight)
{
    ________DBG_20211119_mkb_touch_detect_fgr("fgr4 h");
    uint8_t input_delay = 0;
    switch (mkb_db_curr_central_type_get())
    {
    case CENTRAL_MAC:
    {
        if (isLeftorRight)
        {
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_L_CTRL);
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_RIGHT);

            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_RIGHT);
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_L_CTRL);
        }
        else
        {
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_L_CTRL);
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_LEFT);

            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_LEFT);
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_L_CTRL);
        }
        break;
    }
    case CENTRAL_IOS:
    {
        break;
    }
    break;

    case CENTRAL_DEX:
    {
        if (isLeftorRight)
        {
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_L_CTRL);
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_L_GUI);
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_LEFT);

            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_LEFT);
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_L_GUI);
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_L_CTRL);
        }
        else
        {
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_L_CTRL);
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_L_GUI);
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_RIGHT);

            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_RIGHT);
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_L_GUI);
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_L_CTRL);
        }
    }
    break;
    case CENTRAL_ANDROID:
    {
        break;
    }
    case CENTRAL_WINDOWS:
    case CENTRAL_LINUX:
    case CENTRAL_OTHER:
    {
        if (isLeftorRight)
        {
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_L_CTRL);
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_L_GUI);
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_RIGHT);

            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_RIGHT);
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_L_GUI);
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_L_CTRL);
        }
        else
        {
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_L_CTRL);
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_L_GUI);
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_LEFT);

            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_LEFT);
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_L_GUI);
            mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_L_CTRL);
        }

        break;
    }
    default:
        break;
    }
}

// search: 조건에 부합하면 true리턴.
//예외적 false (키입력후 쿨타임등)
//예외적 true (연속작업중 등)
//일반적 false로 이루어짐 (갯수 모자람 등)
// return true

// fngr-N
static bool search_fngr_n_typing()
{
    if (!(is_safe_from_key_up_ms(300)))
    {
        ________DBG_20220314_mkb_fngr_n_typing("true");
        return true;
    }

    if (m_touch_db.max_touch_cnt >= 3 && mkb_util_timer_ms(app_timer_cnt_diff_compute(m_touch_db.end_time, m_touch_db.start_time) > 500))
    {
        ________DBG_20220314_mkb_fngr_n_typing("true");
        return false;
    }

    if (!(m_touch_db.max_touch_cnt >= 2 && m_touch_db.data_cnt >= TOUCH_DB_MAX - 1))
    {
        ________DBG_20220314_mkb_fngr_n_typing("false");
        return false;
    }

    if (!(m_touch_db.fingers_cnts[1] <= 20))
    {
        ________DBG_20220314_mkb_fngr_n_typing("false");
        return false;
    }

    if (!(abs(m_touch_db.queue[0].continus_x - m_touch_db.queue[20].continus_x) + abs(m_touch_db.queue[0].continus_y - m_touch_db.queue[20].continus_y) < 400))
    {
        ________DBG_20220314_mkb_fngr_n_typing("false");
        return false;
    }

    // if (!drv_pixart_get_otherhand_flag())//문제있음.
    {
        //________DBG_20220314_mkb_fngr_n_typing("false");
        // return false;
    }

    ________DBG_20220314_mkb_fngr_n_typing("true");
    return true;
}

static void detect_fngr_n_typing()
{
    ________DBG_20220314_mkb_fngr_n_typing("typing ready updated!!");
    mkb_db_last_typing_ready_time_set(app_timer_cnt_get());
}
static void active_fngr_n_typing()
{
}

#endif // (defined(MKB_PIXART_ENABLED) && MKB_PIXART_ENABLED)