/*
 * ===============================================================================
 * Copyright (C) 2017-2019
 * INNOPRESSO INC. All Rights Reserved
 *
 * The content of this file is copyrighted by INNOPRESSO or its
 * affiliated, associated or related companies. All copyrights and other
 * associated intellectual property rights related thereto are expressly
 * reserved by INNOPRESSO INC.
 * ===============================================================================
 */

#include <stdbool.h>
#include <stdint.h>
#include <stdarg.h>

#include "sdk_common.h"

#include "nrf_assert.h"
#include "nrf_error.h"
#include "nrf_delay.h"
#include "nrf_cli.h"
#include "nrf_balloc.h"
#include "nrf_pwr_mgmt.h"

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

#include "app_error.h"
#include "app_timer.h"
#include "app_scheduler.h"

#define MKB_LOG_LEVEL MKB_DFU_LOG_LEVEL
#include "mkb_common.h"
#include "mkb_dfu.h"

ret_code_t mkb_dfu_cli(size_t size, char **params)
{
    ret_code_t err_code = NRF_SUCCESS;
    {}

    {}

    {

        NRF_POWER->GPREGRET = 0xffffffff;
        NRF_POWER->GPREGRET = 0xB1;
        NVIC_SystemReset();

        /*
        nrf_sdh_enable_request();
        sd_power_gpregret_clr(0, 0xffffffff);
        sd_power_gpregret_set(0, 0xb1);
        nrf_pwr_mgmt_shutdown(NRF_PWR_MGMT_SHUTDOWN_GOTO_DFU);
        */

        /*
        nrf_sdh_enable_request();
        NRF_POWER->GPREGRET = 1;
        nrf_pwr_mgmt_shutdown(NRF_PWR_MGMT_SHUTDOWN_GOTO_DFU);
        */
        return;
    }
    // else if (!strcmp(params[1], "state"))
    {
        /*
        uint32_t value;
        sd_power_gpregret_get(0, &value);
        ________DBG_20210831_mkb_cli_dbg("gpregret: %d\n", value);
        */
        return;
    }
    // else
    {
        err_code = NRF_ERROR_INTERNAL;
    }
    return err_code;
}

void mkb_dfu_start()
{

    if (mkb_db_battery_level_get() > 20)
    {
        for (int i = 0; i < 3; i++)
        {
            mkb_led_indication_debug(1, 0, 0, 0);
            mkb_led_indication_debug(2, 0, 0, 0);
            mkb_led_indication_debug(3, 0, 0, 0);
            mkb_led_indication_debug(0, 0, 0, 0);
            mkb_wdt_feed();
            nrf_delay_ms(100);
            mkb_led_indication_debug(0, 0, 100, 0);
            mkb_led_indication_debug(3, 0, 100, 0);
            mkb_led_indication_debug(1, 0, 100, 0);
            mkb_led_indication_debug(2, 0, 100, 0);
            mkb_wdt_feed();
            nrf_delay_ms(100);
        }
        mkb_led_indication_debug(1, 0, 0, 0);
        mkb_led_indication_debug(2, 0, 0, 0);
        mkb_led_indication_debug(3, 0, 0, 0);
        mkb_led_indication_debug(0, 0, 0, 0);

        nrf_sdh_enable_request();
        sd_power_gpregret_clr(0, 0xffffffff);
        sd_power_gpregret_set(0, 0xb1);
        nrf_pwr_mgmt_shutdown(NRF_PWR_MGMT_SHUTDOWN_GOTO_DFU);
    }
    else
    {
        for (int i = 0; i < 3; i++)
        {
            mkb_led_indication_debug(1, 0, 0, 0);
            mkb_led_indication_debug(2, 0, 0, 0);
            mkb_led_indication_debug(3, 0, 0, 0);
            mkb_led_indication_debug(0, 0, 0, 0);
            mkb_wdt_feed();
            nrf_delay_ms(100);
            mkb_led_indication_debug(3, 100, 0, 0);
            mkb_led_indication_debug(0, 100, 0, 0);
            mkb_led_indication_debug(1, 100, 0, 0);
            mkb_led_indication_debug(2, 100, 0, 0);
            mkb_wdt_feed();
            nrf_delay_ms(100);
        }
        mkb_led_indication_debug(1, 0, 0, 0);
        mkb_led_indication_debug(2, 0, 0, 0);
        mkb_led_indication_debug(3, 0, 0, 0);
        mkb_led_indication_debug(0, 0, 0, 0);
        return;
    }

    /*
        nrf_sdh_enable_request();
        NRF_POWER->GPREGRET = 1;
        nrf_pwr_mgmt_shutdown(NRF_PWR_MGMT_SHUTDOWN_GOTO_DFU);
        */
}
