/*
* ===============================================================================
* Copyright (C) 2017-2019
* INNOPRESSO INC. All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
* ===============================================================================
*/

#include <stdbool.h>
#include <stdint.h>
#include <stdarg.h>

#include "sdk_common.h"

#include "nrf_assert.h"
#include "nrf_error.h"
#include "nrf_delay.h"
#include "nrf_cli.h"
#include "nrf_balloc.h"
#include "nrf_pwr_mgmt.h"

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

#include "app_error.h"
#include "app_timer.h"
#include "app_scheduler.h"

#define MKB_LOG_LEVEL MKB_SM_LOG_LEVEL
#include "mkb_common.h"

#if (defined(MKB_SM_ENABLED) && MKB_SM_ENABLED)
#include "mkb_state_manager.h"

#if (defined(MKB_DB_ENABLED) && MKB_DB_ENABLED)
#include "mkb_db.h"
#else
#error "You have to enable db at mkb_config.h!"
#endif // (defined(MKB_DB_ENABLED) && MKB_DB_ENABLED)

#if (defined(MKB_LED_ENABLED) && MKB_LED_ENABLED)
#include "mkb_led.h"
#else
#error "You have to enable led at mkb_config.h!"
#endif // (defined(MKB_LED_ENABLED) && MKB_LED_ENABLED)

void mkb_sm_change(SYSTEM_STATE_t state)
{
    MKB_DB_CH_t *p_ch = mkb_db_ch_get();
    CHANNEL_ID_t ch_id = mkb_db_ch_id_get();
    SYSTEM_STATE_t state_prev = mkb_db_system_state_get();

    switch (state)
    {
    case SYSTEM_STATE_IDLE:
        mkb_db_system_state_set(state);
        break;
    case SYSTEM_STATE_INIT:
        mkb_db_system_state_set(state);
        break;
    case SYSTEM_STATE_KEYBOARD:
    {
        //________DBG_20210915_led_callstack("LED_STACK");
        //mkb_led_ind_set(p_ch->led[ch_id].touch);
        mkb_db_system_state_set(state);
    }
    break;
    case SYSTEM_STATE_TOUCH:
    {
        //________DBG_20210915_led_callstack("LED_STACK");
        //mkb_led_ind_set(p_ch->led[ch_id].touch);
        mkb_db_system_state_set(state);
    }
    break;
    case SYSTEM_STATE_TOUCHLOCK:
    {
        //________DBG_20210915_led_callstack("LED_STACK");
        //mkb_led_ind_set(p_ch->led[ch_id].touch);
        mkb_db_system_state_set(state);
    }
    break;
    }
}

ret_code_t mkb_state_manager_cli(size_t size, char **params)
{
    ret_code_t err_code = NRF_SUCCESS;
    if (!strcmp(params[1], "help"))
    {
    }
    else if (!strcmp(params[1], "mkb_sm_change"))
    {
        mkb_sm_change(atoi(params[2]));
    }
    else
    {
        err_code = NRF_ERROR_INTERNAL;
    }
    return err_code;
}

#endif // (defined(MKB_SM_ENABLED) && MKB_SM_ENABLED)
