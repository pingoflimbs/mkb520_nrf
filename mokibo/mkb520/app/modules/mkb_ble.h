/*
* ===============================================================================
* Copyright (C) 2017-2019
* INNOPRESSO INC. All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
* ===============================================================================
*/

#ifndef __MKB_BLE__
#define __MKB_BLE__

#include "mkb_config.h"
#include "mkb_db.h"

#define MIN_CONN_INTERVAL MSEC_TO_UNITS(7.5, UNIT_1_25_MS) /**< Minimum connection interval (7.5 ms). */
#define MAX_CONN_INTERVAL MSEC_TO_UNITS(10, UNIT_1_25_MS)  /**< Maximum connection interval (15 ms). */
#define SLAVE_LATENCY 20								   /**< Slave latency. */
#define CONN_SUP_TIMEOUT MSEC_TO_UNITS(3000, UNIT_10_MS)   /**< Connection supervisory timeout (3000 ms). */

#define PNP_ID_VENDOR_ID_SOURCE 0x02  /**< Vendor ID Source. */
#define PNP_ID_VENDOR_ID 0x1915		  /**< Vendor ID. */
#define PNP_ID_PRODUCT_ID 0xEEEE	  /**< Product ID. */
#define PNP_ID_PRODUCT_VERSION 0x0001 /**< Product Version. */

#define MANUFACTURER_NAME "NordicSemiconductor" /**< Manufacturer. Will be passed to Device Information Service. */

#define APP_BLE_OBSERVER_PRIO 3 /**< Application's BLE observer priority. You shouldn't need to modify this value. */
#define APP_BLE_CONN_CFG_TAG 1	/**< A tag identifying the SoftDevice BLE configuration. */

#if (defined(MKB_BLE_SWIFT_PAIR) && MKB_BLE_SWIFT_PAIR)
#define MICROSOFT_VENDOR_ID 0x0006		   /**< Microsoft Vendor ID.*/
#define MICROSOFT_BEACON_ID 0x03		   /**< Microsoft Beacon ID, used to indicate that Swift Pair feature is supported. */
#define MICROSOFT_BEACON_SUB_SCENARIO 0x00 /**< Microsoft Beacon Sub Scenario, used to indicate how the peripheral will pair using Swift Pair feature. */
#define RESERVED_RSSI_BYTE 0x80			   /**< Reserved RSSI byte, used to maintain forwards and backwards compatibility. */
#endif

#define BLE_TOUCH_DEVICE_PAD 0
#define BLE_TOUCH_DEVICE_SCREEN 1
#define BLE_TOUCH_DEVICE_PEN 2
#define BLE_TOUCH_DEVICE_POINTER 3

#if MKB_BLE_TOUCH_DEVICE_ENABLED
#if MKB_BLE_TOUCH_DEVICE_TYPE == BLE_TOUCH_DEVICE_PAD
#define FEATURE_REP_MAX_LEN 3				/**< Length of Mouse Input Report containing media player data. */
#define FEATURE_REPORT_COUNT 2				/**< Number of output reports in this application. */
#define FEATURE_REP_REF_DEVICE_CAPS_LEN 2	/**< Id of reference to Mouse Input Report containing media player data. */
#define FEATURE_REP_REF_CONFIG_LEN 1		/**< Id of reference to Mouse Input Report containing media player data. */
#define FEATURE_REP_REF_DEVICE_CERT_LEN 256 /**< Id of reference to Mouse Input Report containing media player data. */
#define FEATURE_REP_REF_DEVICE_CAPS_ID 6	/**< Id of reference to Mouse Input Report containing media player data. */
#define FEATURE_REP_REF_CONFIG_ID 8			/**< Id of reference to Mouse Input Report containing media player data. */
#define FEATURE_REP_REF_DEVICE_CERT_ID 7	/**< Id of reference to Mouse Input Report containing media player data. */
#define FEATURE_REP_REF_DEVICE_CAPS_INDEX 0 /**< Id of reference to Mouse Input Report containing media player data. */
#define FEATURE_REP_REF_CONFIG_INDEX 1		/**< Id of reference to Mouse Input Report containing media player data. */
#define FEATURE_REP_REF_DEVICE_CERT_INDEX 2 /**< Id of reference to Mouse Input Report containing media player data. */
#elif MKB_BLE_TOUCH_DEVICE_TYPE == BLE_TOUCH_DEVICE_SCREEN
#define FEATURE_REP_MAX_LEN 1  /**< Length of Mouse Input Report containing media player data. */
#define FEATURE_REPORT_COUNT 1 /**< Number of output reports in this application. */
#define FEATURE_REP_INDEX 0	   /**< Index of Output Report. */
#define FEATURE_REP_REF_ID 1   /**< Id of reference to Keyboard Output Report. */
#elif MKB_BLE_TOUCH_DEVICE_TYPE == BLE_TOUCH_DEVICE_PEN
#define FEATURE_REPORT_COUNT 0 /**< Number of output reports in this application. */
#elif MKB_BLE_TOUCH_DEVICE_TYPE == BLE_TOUCH_DEVICE_POINTER
#define FEATURE_REPORT_COUNT 0 /**< Number of output reports in this application. */
#else
#define FEATURE_REPORT_COUNT 0 /**< Number of output reports in this application. */
#endif
#else
#define FEATURE_REPORT_COUNT 0 /**< Number of output reports in this application. */
#endif

#define OUTPUT_REPORT_COUNT 1			   /**< Number of output reports in this application. */
#define OUTPUT_REP_MAX_LEN 1			   /**< Maximum length of Output Report. */
#define OUTPUT_REP_BIT_MASK_CAPS_LOCK 0x02 /**< CAPS LOCK bit in Output Report (based on 'LED Page (0x08)' of the Universal Serial Bus HID Usage Tables). */
#define OUTPUT_REP_INDEX 0				   /**< Index of Output Report. */
#define OUTPUT_REP_REF_ID 1				   /**< Id of reference to Keyboard Output Report. */

#if MKB_BLE_TOUCH_DEVICE_ENABLED
#define INPUT_REPORT_COUNT 5 /**< Number of input reports in this application. */
#else
#define INPUT_REPORT_COUNT 4 /**< Number of input reports in this application. */
#endif

#define INPUT_REP_KEYS_MAX_LEN 8 /**< Maximum length of the Input Report characteristic. */
#define INPUT_REP_BUTTONS_LEN 3	 /**< Length of Mouse Input Report containing button data. */
#define INPUT_REP_MOVEMENT_LEN 3 /**< Length of Mouse Input Report containing movement data. */
#define INPUT_REP_CONSUMER_LEN 3 /**< Length of Mouse Input Report containing media player data. */

#if MKB_BLE_TOUCH_DEVICE_ENABLED
#if MKB_BLE_TOUCH_DEVICE_TYPE == BLE_TOUCH_DEVICE_PAD
#define INPUT_REP_TOUCH_LEN 29 /**< Length of Mouse Input Report containing media player data. */
#elif MKB_BLE_TOUCH_DEVICE_TYPE == BLE_TOUCH_DEVICE_SCREEN
#define INPUT_REP_TOUCH_LEN 7 /**< Length of Mouse Input Report containing media player data. */
#elif MKB_BLE_TOUCH_DEVICE_TYPE == BLE_TOUCH_DEVICE_PEN
#define INPUT_REP_TOUCH_LEN 5 /**< Length of Mouse Input Report containing media player data. */
#elif MKB_BLE_TOUCH_DEVICE_TYPE == BLE_TOUCH_DEVICE_POINTER
#define INPUT_REP_TOUCH_LEN 5 /**< Length of Mouse Input Report containing media player data. */
#else
#define INPUT_REP_TOUCH_LEN 0 /**< Length of Mouse Input Report containing media player data. */
#endif
#else
#define INPUT_REP_TOUCH_LEN 0 /**< Length of Mouse Input Report containing media player data. */
#endif

#define INPUT_REP_KEYS_INDEX 0	   /**< Index of Mouse Input Report containing button data. */
#define INPUT_REP_BUTTONS_INDEX 1  /**< Index of Mouse Input Report containing button data. */
#define INPUT_REP_MOVEMENT_INDEX 2 /**< Index of Mouse Input Report containing movement data. */
#define INPUT_REP_CONSUMER_INDEX 3 /**< Index of Mouse Input Report containing media player data. */
#define INPUT_REP_TOUCH_INDEX 4	   /**< Index of Mouse Input Report containing media player data. */

#define INPUT_REP_REF_KEYS_ID 1		/**< Id of reference to Keyboard Input Report. */
#define INPUT_REP_REF_BUTTONS_ID 2	/**< Id of reference to Mouse Input Report containing button data. */
#define INPUT_REP_REF_MOVEMENT_ID 3 /**< Id of reference to Mouse Input Report containing movement data. */
#define INPUT_REP_REF_CONSUMER_ID 4 /**< Id of reference to Mouse Input Report containing media player data. */
#define INPUT_REP_REF_TOUCH_ID 5	/**< Id of reference to Mouse Input Report containing media player data. */

#define BASE_USB_HID_SPEC_VERSION 0x0101 /**< Version number of base USB HID Specification implemented by this application. */

#define MODIFIER_KEY_POS 0												/**< Position of the modifier byte in the Input Report. */
#define SCAN_CODE_POS 2													/**< The start position of the key scan code in a HID Report. */
#define SHIFT_KEY_CODE 0x02												/**< Key code indicating the press of the Shift Key. */
#define MAX_KEYS_IN_ONE_REPORT (INPUT_REP_KEYS_MAX_LEN - SCAN_CODE_POS) /**< Maximum number of key presses that can be sent in one Input Report. */

#define MAX_BUFFER_ENTRIES 8 /**< Number of elements that can be enqueued */

#define FIRST_CONN_PARAMS_UPDATE_DELAY APP_TIMER_TICKS(5000) /**< Time from initiating event (connect or start of notification) to first time sd_ble_gap_conn_param_update is called (5 seconds). */
#define NEXT_CONN_PARAMS_UPDATE_DELAY APP_TIMER_TICKS(30000) /**< Time between each call to sd_ble_gap_conn_param_update after the first call (30 seconds). */
#define MAX_CONN_PARAM_UPDATE_COUNT 3						 /**< Number of attempts before giving up the connection parameter negotiation. */

#define BLE_BUTTON_RELEASE 0x00
#define BLE_BUTTON_LEFT 0x01
#define BLE_BUTTON_RIGHT 0x02

#define BLE_ADDRESS_TYPE_STATIC 0
#define BLE_ADDRESS_TYPE_RANDOM 1

#define FINGER_MAX 5

#pragma pack(push, 1) // 1바이트 크기로 정렬
typedef struct
{
#if MKB_BLE_TOUCH_DEVICE_TYPE == BLE_TOUCH_DEVICE_PAD
	struct
	{
		uint8_t confidence : 1;
		uint8_t tip : 1; // touch is currently on the panel
		uint8_t inrange : 1;
		uint8_t res : 1;	 // *
		uint8_t touchID : 4; // An arbitrary ID tag associated with a finger ( 0 ~ 9)
		//uint16_t	pressure;			  // 16bit x-coordinates in pixels
		//uint16_t	width;			  // 16bit x-coordinates in pixels
		//uint16_t	height;			  // 16bit y-coordinates in pixels
		uint16_t x; // 16bit x-coordinates in pixels
		uint16_t y; // 16bit y-coordinates in pixels}
	} finger[FINGER_MAX];
	uint8_t touchCnt;	// total number of current touches
	uint8_t button;		/* 1byte */
	uint16_t timestamp; // unit: 0.1ms
#elif MKB_BLE_TOUCH_DEVICE_TYPE == BLE_TOUCH_DEVICE_SCREEN
	uint8_t touchCnt;	 // total number of current touches
	uint8_t touchID;	 // An arbitrary ID tag associated with a finger ( 0 ~ 9)
	uint8_t tip : 1;	 // touch is currently on the panel
	uint8_t inrange : 1; // touch is currently on the panel
	uint8_t res : 6;	 // *
	uint16_t x;			 // 16bit x-coordinates in pixels
	uint16_t y;			 // 16bit y-coordinates in pixels
#elif MKB_BLE_TOUCH_DEVICE_TYPE == BLE_TOUCH_DEVICE_PEN
	uint8_t tip : 1;	 // touch is currently on the panel
	uint8_t inrange : 1; // touch is currently on the panel
	uint8_t res : 6;	 // *
	uint16_t x;			 // 16bit x-coordinates in pixels
	uint16_t y;			 // 16bit y-coordinates in pixels
#elif MKB_BLE_TOUCH_DEVICE_TYPE == BLE_TOUCH_DEVICE_POINTER
	uint8_t button : 1; // touch is currently on the panel
	uint8_t res : 7;	// *
	uint16_t x;			// 16bit x-coordinates in pixels
	uint16_t y;			// 16bit y-coordinates in pixels
#endif
} BLE_REPORT_DATA_TOUCH_t;
#pragma pack(pop) // 정렬 설정을 이전 상태(기본값)로 되돌림
#define MKB_TOUCHPAD_LOGICAL_MAX_PRESSURE 256
#define MKB_TOUCHPAD_LOGICAL_MAX_X 4000 //20000
#define MKB_TOUCHPAD_LOGICAL_MAX_Y 4000 //12000
#define MKB_TOUCHPAD_PHYSICAL_MAX_X 2450
#define MKB_TOUCHPAD_PHYSICAL_MAX_Y 920
#if (defined(MKB_CYPRESS_ENABLED) && MKB_CYPRESS_ENABLED)
#define MKB_TOUCHPAD_PIXEL_MAX_X 2450
#define MKB_TOUCHPAD_PIXEL_MAX_Y 920
#endif
#if (defined(MKB_PIXART_ENABLED) && MKB_PIXART_ENABLED)
#define MKB_TOUCHPAD_PIXEL_MAX_X 2450
#define MKB_TOUCHPAD_PIXEL_MAX_Y 920
#endif

extern void mkb_ble_init(void);
extern void mkb_ble_start(void);
extern void mkb_ble_device_name_set(uint8_t *p_name);
extern ret_code_t mkb_ble_bas_battery_level_update(uint8_t level);
extern bool mkb_ble_hid_event_handler(mkb_event_t *p_event);

extern ret_code_t mkb_ble_keys_send(uint8_t *p_key_pattern, uint8_t key_pattern_len);
extern void mkb_ble_movement_send(int16_t x_delta, int16_t y_delta);
extern void mkb_ble_button_send(uint8_t button);
extern void mkb_ble_scroll_send(uint8_t button, uint8_t hscroll, uint8_t scroll);
extern void mkb_ble_consumer_send(uint8_t *p_data, uint8_t len);
extern void mkb_ble_touchpad_send(uint8_t *p_data, uint8_t len);
extern void mkb_ble_disconnect(CHANNEL_ID_t ch);
extern bool mkb_ble_conn_state(CHANNEL_ID_t ch);
extern void mkb_ble_auth_key_response(uint8_t *keys);
extern void mkb_ble_adv_stop_request(void);

#endif /* __MKB_BLE__ */
