/*
 * ===============================================================================
 * Copyright (C) 2017-2019
 * INNOPRESSO INC. All Rights Reserved
 *
 * The content of this file is copyrighted by INNOPRESSO or its
 * affiliated, associated or related companies. All copyrights and other
 * associated intellectual property rights related thereto are expressly
 * reserved by INNOPRESSO INC.
 * ===============================================================================
 */

#ifndef __MKB_DB__
#define __MKB_DB__

#include "peer_manager.h"

#include "mkb_common.h"

#if (defined(MKB_SYSTEM_ENABLED) && MKB_SYSTEM_ENABLED)
#include "mkb_system.h"
#endif //(defined(MKB_SYSTEM_ENABLED) && MKB_SYSTEM_ENABLED)

#if (defined(MKB_STM8_ENABLED) && MKB_STM8_ENABLED)
#include "drv_stm8.h"
#endif // (defined(MKB_STM8_ENABLED) && MKB_STM8_ENABLED)

#include "mkb_led.h"
#include "drv_pixart.h"

typedef enum
{
    SYSTEM_STATE_IDLE,
    SYSTEM_STATE_INIT,
    SYSTEM_STATE_KEYBOARD,
    SYSTEM_STATE_TOUCH,
    SYSTEM_STATE_TOUCHLOCK,
} SYSTEM_STATE_t;

typedef struct
{
    MKB_LED_IND_t disconnect;
    MKB_LED_IND_t touch;
    MKB_LED_IND_t touchlock;
    MKB_LED_IND_t button;
    MKB_LED_IND_t connect;
    MKB_LED_IND_t select;
    MKB_LED_IND_t adv_direct;
    MKB_LED_IND_t adv_whitelist;
    MKB_LED_IND_t adv_fast;
    MKB_LED_IND_t adv_slow;
    MKB_LED_IND_t adv_idle;
} CHANNEL_LED_t;

#define MKB_DEVICE_NAME_SIZE 32
#define MKB_MODEL_NAME_SIZE 32

#pragma pack(push, 4) // 4바이�기롕렬
typedef enum
{
#if (defined(MKB_BLE_BT1_ENABLED) && MKB_BLE_BT1_ENABLED)
    CHANNEL_ID_1,
#endif

#if (defined(MKB_BLE_BT2_ENABLED) && MKB_BLE_BT2_ENABLED)
    CHANNEL_ID_2,
#endif

#if (defined(MKB_BLE_BT3_ENABLED) && MKB_BLE_BT3_ENABLED)
    CHANNEL_ID_3,
#endif

#if !(defined(BOARD_MKB10040)) //�용�함
    CHANNEL_ID_4,
#endif

#if (defined(MKB_BLE_BT1_ENABLED) && MKB_BLE_BT1_ENABLED)
    CHANNEL_ID_BT1 = CHANNEL_ID_1,
#endif

#if (defined(MKB_BLE_BT2_ENABLED) && MKB_BLE_BT2_ENABLED)
    CHANNEL_ID_BT2 = CHANNEL_ID_2,
#endif

#if (defined(MKB_BLE_BT3_ENABLED) && MKB_BLE_BT3_ENABLED)
    CHANNEL_ID_BT3 = CHANNEL_ID_3,
#endif

#if !(defined(BOARD_MKB10040)) //�용�함
    CHANNEL_ID_USB = CHANNEL_ID_4,
#endif
    CHANNEL_ID_MAX
} CHANNEL_ID_t;

typedef enum
{
    CHANNEL_TYPE_BT,
    CHANNEL_TYPE_USB
} CHANNEL_TYPE_t;

typedef enum
{
    CENTRAL_IOS,
    CENTRAL_ANDROID,
    CENTRAL_DEX,
    CENTRAL_WINDOWS,
    CENTRAL_MAC,
    CENTRAL_MAX,
    CENTRAL_LINUX,
    CENTRAL_OTHER,
} CENTRAL_TYPE_t;

typedef struct
{
    CHANNEL_ID_t id;          // 4
    CHANNEL_TYPE_t type;      // 4
    CENTRAL_TYPE_t central;   // 4
    uint16_t state;           // 2
    pm_peer_id_t peer_id;     // 2
    ble_gap_addr_t peer_addr; // 7
    ble_gap_addr_t own_addr;  // 7
    uint16_t dummy;           // 2
} CHANNEL_INFO_t;

/**@brief MOKIBO Config DB
 */
typedef struct
{
    uint32_t magic_code;                       // 4
    uint8_t device_name[MKB_DEVICE_NAME_SIZE]; // 32
    uint8_t model_name[MKB_MODEL_NAME_SIZE];   // 32
    uint8_t touch_fw_major;                    // 1
    uint8_t touch_fw_minor;                    // 1
    uint8_t aamode;                            // 1 0:all, 1:right, 2:left 64
    uint8_t dummy2;                            // 1
} MKB_DB_CONFIG_t;

/**@brief MOKIBO Channel Information DB
 */
typedef struct
{
    CHANNEL_ID_t id;                     // current service channel id
    CHANNEL_INFO_t info[CHANNEL_ID_MAX]; // information of channels
} MKB_DB_CH_CONFIG_t;

typedef struct
{
    MKB_DB_CH_CONFIG_t config;
    CHANNEL_LED_t led[CHANNEL_ID_MAX]; // information of channels led
    CHANNEL_ID_t id_prev;
    uint16_t conn_handle[CHANNEL_ID_MAX];
} MKB_DB_CH_t;
#pragma pack(pop) // �렬 �정�전 �태(기본�롘돌�

/**@brief MOKIBO Information DB
 */
typedef struct
{
#if (defined(MKB_STM8_ENABLED) && MKB_STM8_ENABLED)
    STM8_VERSION_t stm8_version;
#endif // (defined(MKB_STM8_ENABLED) && MKB_STM8_ENABLED)

    bool erase_bonds;
    bool wait_for_disconnect;

    bool is_func_pressed;
    bool is_battery_charging;
    uint8_t battery_level;

    uint32_t last_key_up_time;
    uint32_t last_passkey_updown_time;
    uint32_t last_tap_time;
    uint32_t last_gstr_time;
    uint32_t last_typing_ready_time;

    bool is_red_click_down;
    bool is_tap_click_down;

    uint32_t last_red_up_time;
    uint32_t last_multitouch_time;

    uint32_t reset_reason;
    SYSTEM_STATE_t system_state;
} MKB_DB_WORK_t;

extern void mkb_db_init(void);
extern char *mkb_db_get_channel_type_str(CHANNEL_TYPE_t type);
extern char *mkb_db_get_central_type_str(CENTRAL_TYPE_t type);

extern void mkb_db_restore_config(void);
extern void mkb_db_restore_ch(void);
extern void mkb_db_restore(void);
extern void mkb_db_store_config(void);
extern void mkb_db_store_ch(void);
extern void mkb_db_store(void);
extern void mkb_db_create_config(void);
extern void mkb_db_create_ch(void);
extern void mkb_db_create(void);

#if (defined(MKB_STM8_ENABLED) && MKB_STM8_ENABLED)
extern void mkb_db_stm8_version_set(uint8_t version);
extern STM8_VERSION_t mkb_db_stm8_version_get(void);
#endif // (defined(MKB_STM8_ENABLED) && MKB_STM8_ENABLED)

extern void mkb_db_wait_for_disconnect_set(bool flag);
extern bool mkb_db_wait_for_disconnect_get(void);
extern void mkb_db_erase_bonds_set(bool erase_bonds);
extern bool mkb_db_erase_bonds_get(void);
extern uint32_t mkb_db_reset_reason_get(void);
extern SYSTEM_STATE_t mkb_db_system_state_get(void);
extern void mkb_db_system_state_set(SYSTEM_STATE_t state);
extern bool mkb_db_system_state_is_touch(void);

extern bool mkb_db_is_func_pressed_get(void);
extern void mkb_db_is_func_pressed_set(bool is_func_pressed);

extern bool mkb_db_is_bat_charging_get(void);
extern void mkb_db_is_bat_charging_set(bool is_battery_charging);

extern uint8_t mkb_db_battery_level_get(void);
extern void mkb_db_battery_level_set(uint8_t battery_level);

uint32_t mkb_db_last_key_up_time_get(void);
extern void mkb_db_last_key_up_time_set(uint32_t last_key_up_time);

uint32_t mkb_db_last_passkey_updown_time_get(void);
extern void mkb_db_last_passkey_updown_time_set(uint32_t last_passkey_updown_time);

uint32_t mkb_db_last_tap_time_get(void);
extern void mkb_db_last_tap_time_set(uint32_t last_key_up_time);

uint32_t mkb_db_last_gstr_time_get(void);
extern void mkb_db_last_gstr_time_set(uint32_t last_key_up_time);

uint32_t mkb_db_last_typing_ready_time_get(void);
void mkb_db_last_typing_ready_time_set(uint32_t last_typing_ready_time);

extern void mkb_db_is_red_click_down_set(bool is_clicked);
extern bool mkb_db_is_red_click_down_get(void);
extern uint32_t mkb_db_red_click_up_time_get(void);

extern void mkb_db_is_tap_click_down_set(bool is_clicked);
extern bool mkb_db_is_tap_click_down_get(void);

extern bool mkb_db_last_multitouch_time_set(uint32_t last_multitouch_time);
extern uint32_t mkb_db_last_multitouch_time_get();

extern const MKB_DB_WORK_t *mkb_db_work_get(void);

extern MKB_DB_CONFIG_t *mkb_db_config_get(void);
extern MKB_DB_CH_t *mkb_db_ch_get(void);
extern CHANNEL_INFO_t *mkb_db_ch_info_get(CHANNEL_ID_t ch);
extern void mkb_db_ch_id_set(CHANNEL_ID_t ch);
extern CHANNEL_ID_t mkb_db_ch_id_get(void);
extern void mkb_db_ch_address_set(CHANNEL_ID_t ch);
extern void mkb_db_ch_id_prev_set(CHANNEL_ID_t ch);
extern CHANNEL_ID_t mkb_db_ch_id_prev_get(void);
extern CHANNEL_ID_t mkb_db_ch_id_other(CHANNEL_ID_t ch1, CHANNEL_ID_t ch2);
extern void mkb_db_central_type_set(CHANNEL_ID_t ch, CENTRAL_TYPE_t central);
extern CENTRAL_TYPE_t mkb_db_central_type_get(CHANNEL_ID_t ch);
extern CENTRAL_TYPE_t mkb_db_curr_central_type_get();

extern void mkb_db_aamode_set(pixart_aamode_type_t type);
extern pixart_aamode_type_t mkb_db_aamode_get();
#endif /* __MKB_DB__ */
