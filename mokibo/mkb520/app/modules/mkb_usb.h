/*
* ===============================================================================
* Copyright (C) 2017-2019
* INNOPRESSO INC. All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
* ===============================================================================
*/

#ifndef __MKB_USB__
#define __MKB_USB__

extern void mkb_usb_init(void);
extern void mkb_usb_start(CHANNEL_ID_t ch);
extern void mkb_usb_stop(CHANNEL_ID_t ch);
extern void mkb_usb_kbd_modifier(uint8_t data, bool flag);
extern void mkb_usb_kbd_control(uint8_t data, bool flag);
extern void mkb_usb_mouse_button(uint8_t data, bool flag);
extern void mkb_usb_mouse_move(int8_t x, int8_t y);
extern void mkb_usb_mouse_scroll(int8_t offset);
extern void mkb_usb_consumer(uint8_t *p_data, uint8_t size);
extern void mkb_usb_event_process(void);
extern bool mkb_usb_state(void);
extern bool mkb_usb_conn_state(CHANNEL_ID_t ch);

#endif /* __MKB_USB__ */
