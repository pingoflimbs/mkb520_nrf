/*
* ===============================================================================
* Copyright (C) 2017-2019
* INNOPRESSO INC. All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
* ===============================================================================
*/

#include <stdbool.h>
#include <stdint.h>
#include <stdarg.h>

#include "sdk_common.h"

#include "nrf_assert.h"
#include "nrf_error.h"
#include "nrf_delay.h"
#include "nrf_cli.h"
#include "nrf_balloc.h"
#include "nrf_pwr_mgmt.h"
#include "nrf_power.h"

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

#include "app_error.h"
#include "app_timer.h"
#include "app_scheduler.h"

#include "ble_gap.h"

#define MKB_LOG_LEVEL MKB_UTIL_LOG_LEVEL
#include "mkb_common.h"
#include "mkb_util.h"

#include "mkb_keyboard.h"

/*
void mkb_util_system_time_start()
{
    ret_code_t err_code;
    err_code = app_timer_create(&m_util_timer, APP_TIMER_MODE_REPEATED, mkb_util_timer_handler);
    app_timer_start(m_util_timer, APP_TIMER_TICKS(1), NULL);
}

static void  mkb_util_system_time_handler()
{
    m_util_system_time++;
}

uint32_t  mkb_util_system_time_get()
{
    return m_util_system_time;
}
*/

void mkb_util_get_mac_address(uint32_t ch, ble_gap_addr_t *pAddr)
{
    pAddr->addr_type = BLE_GAP_ADDR_TYPE_RANDOM_STATIC;
    pAddr->addr[0] = (uint8_t)NRF_FICR->DEVICEADDR[0];
    pAddr->addr[1] = (uint8_t)(NRF_FICR->DEVICEADDR[0] >> 8);
    pAddr->addr[2] = (uint8_t)(NRF_FICR->DEVICEADDR[0] >> 16);
    pAddr->addr[3] = (uint8_t)(NRF_FICR->DEVICEADDR[0] >> 24);

#if (MKB_BLE_ADDRESS_TYPE == BLE_ADDRESS_TYPE_STATIC)
    pAddr->addr[4] = (uint8_t)NRF_FICR->DEVICEADDR[1];
    pAddr->addr[5] = (uint8_t)(NRF_FICR->DEVICEADDR[1] >> 8);
#else
    // change last 2-bytes by random
    uint16_t random = (uint16_t)(app_timer_cnt_get() % 0xFFFF);

    pAddr->addr[4] = (uint8_t)random;
    pAddr->addr[5] = (uint8_t)(random >> 8);
#endif

    pAddr->addr[5] &= 0x0F;
    pAddr->addr[5] |= (ch << 4) | 0xC0; // 2MSB must be set 11

    MKB_LOG_DEBUG("channel: %d, addr: 0x%02x%02x%02x%02x%02x%02x", ch,
                  pAddr->addr[5], pAddr->addr[4], pAddr->addr[3],
                  pAddr->addr[2], pAddr->addr[1], pAddr->addr[0]);
}

uint32_t mkb_util_reset_reason_get(void)
{
    uint32_t reason = nrf_power_resetreas_get();
    //nrf_power_resetreas_clear(reason);
    return reason;
}

char *mkb_util_reset_reason_str(uint32_t reason)
{
    if ((reason & NRF_POWER_RESETREAS_RESETPIN_MASK) != 0)
        return "resetpin";
    if ((reason & NRF_POWER_RESETREAS_DOG_MASK) != 0)
        return "watchdog";
    if ((reason & NRF_POWER_RESETREAS_SREQ_MASK) != 0)
        return "sreq";
    if ((reason & NRF_POWER_RESETREAS_LOCKUP_MASK) != 0)
        return "lockup";
    if ((reason & NRF_POWER_RESETREAS_OFF_MASK) != 0)
        return "off";
    if ((reason & NRF_POWER_RESETREAS_LPCOMP_MASK) != 0)
        return "lpcomp";
    if ((reason & NRF_POWER_RESETREAS_DIF_MASK) != 0)
        return "dif";
    if ((reason & NRF_POWER_RESETREAS_NFC_MASK) != 0)
        return "nfc";
#if defined(POWER_RESETREAS_VBUS_Msk) || defined(__NRFX_DOXYGEN__)
    if ((reason & NRF_POWER_RESETREAS_VBUS_MASK) != 0)
        return "vbus";
#endif
    if (reason == 0)
        return "none";
}

float mkb_util_temp_get(void)
{
    int32_t temp = 0;

    ret_code_t err_code;

    err_code = sd_temp_get(&temp);
    if (err_code == NRF_SUCCESS)
    {
        MKB_LOG_DEBUG("Die temperature: %d", temp);
    }
    else
        MKB_ERROR_CHECK(err_code);

    return temp * 0.25;
}

uint32_t mkb_util_get_tick_delta()
{
    static uint32_t old_tick = 0;
    uint32_t result = app_timer_cnt_get() - old_tick;
    old_tick = app_timer_cnt_get();
    return mkb_util_timer_ms(result);
}

uint32_t mkb_util_timer_ms(uint32_t ticks)
{
    // eg. (7 + 1) * 1000 / 32768
    //   = 8000 / 32768
    //   = 0.24414062
    // eg. (0 + 1) * 1000 / 32768
    //   = 1000 / 32768
    //   = 0.03051758
    float numerator = ((float)APP_TIMER_CONFIG_RTC_FREQUENCY + 1.0f) * 1000.0f;
    float denominator = (float)APP_TIMER_CLOCK_FREQ;
    float ms_per_tick = numerator / denominator;

    uint32_t ms = ms_per_tick * ticks;

    return ms;
}

uint32_t mkb_util_counter_ms()
{
    static uint32_t last_time;
}

uint8_t ascii_to_hid_key(uint8_t ascii)
{
    if ('1' <= ascii && ascii <= '9')
    { //숫자
        ascii -= '1';
        return HID_KEY_1 + ascii;
    }
    else if (ascii == '0')
    {
        return HID_KEY_0;
    }
    else if ('A' <= ascii && ascii <= 'Z')
    { //대문자
        ascii -= 'A';
        return HID_KEY_A + ascii;
    }
    else if ('a' <= ascii && ascii <= 'z')
    { //소문자
        ascii -= 'a';
        return HID_KEY_A + ascii;
    }
    else if (ascii == '\n')
    {
        return HID_KEY_ENTER;
    }
    else if (ascii == ' ')
    {
        return HID_KEY_SB;
    }
    else if (ascii == ':')
    {
        return HID_KEY_COLON;
    }
    else if (ascii == '-')
    {
        return HID_KEY_MINUS;
    }
    return HID_KEY_SB;
}