/*
* ===============================================================================
* Copyright (C) 2017-2019
* INNOPRESSO INC. All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
* ===============================================================================
*/

#ifndef __MKB_INFO__
#define __MKB_INFO__

extern void mkb_info_system(void);
extern void mkb_info_config(void);
extern void mkb_info_channel(void);

#endif /* __MKB_INFO__ */
