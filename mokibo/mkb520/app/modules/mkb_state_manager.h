/*
* ===============================================================================
* Copyright (C) 2017-2019
* INNOPRESSO INC. All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
* ===============================================================================
*/

#ifndef __MKB_STATE_MANAGER__
#define __MKB_SYSTEM__

#include "mkb_db.h"

extern void mkb_sm_change(SYSTEM_STATE_t state);
extern ret_code_t mkb_state_manager_cli(size_t size, char **params);

#endif /* __MKB_STATE_MANAGER__ */
