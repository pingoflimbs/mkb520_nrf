/*
 * ===============================================================================
 * Copyright (C) 2017-2019
 * INNOPRESSO INC. All Rights Reserved
 *
 * The content of this file is copyrighted by INNOPRESSO or its
 * affiliated, associated or related companies. All copyrights and other
 * associated intellectual property rights related thereto are expressly
 * reserved by INNOPRESSO INC.
 * ===============================================================================
 */

#ifndef __MKB_TOUCHPAD_PIXART__
#define __MKB_TOUCHPAD_PIXART__

#include "mkb_config.h"
#include "drv_pixart.h"

/*
    typedef struct
    {
        uint8_t touchCnt;	// total number of current touches
        uint32_t timestamp; // unit: 0.1ms
        uint8_t button;		// ?? click?

        uint16_t center_x;
        uint16_t center_y;
        uint16_t area_size;

        pixart_gesture_packet_t gesture;
        {
            pixart_gstr_type_t type;
            uint16_t x;
            uint16_t y;
            uint16_t z;
        }d
        mokibo_finger_data_t fingers[PIXART_INFO_MAX_COUNT]; // [10]
        {
            uint8_t touchID;	// adjusted touch id
            uint8_t tip;		// touch is currently on the panel
            uint8_t confidence; // no_event(0), touch_down(1), significant_displacement(2), lift_off(3)
            uint16_t x;			// 16bit x-coordinates in pixels
            uint16_t y;			// 16bit y-coordinates in pixels
            uint16_t z;			// 16bit z-coordinates in pixels
            uint16_t area;
        }
    } mokibo_touch_data_t;

*/

extern ret_code_t mkb_touchpad_pixart_init();
extern void mkb_touchpad_read_process(void *p_data);
extern void aamode_clear();
#endif /* __MKB_TOUCHPAD_PIXART_T_H__ */
