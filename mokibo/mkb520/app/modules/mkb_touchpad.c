/*
* ===============================================================================
* Copyright (C) 2017-2019
* INNOPRESSO INC. All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
* ===============================================================================
*/

#include <stdbool.h>
#include <stdint.h>

#include "sdk_common.h"

#include "nrf_assert.h"
#include "nrf_error.h"
#include "nrf_log.h"
#include "nrf_delay.h"
#include "nrf_cli.h"

#include "app_error.h"
#include "app_timer.h"
#include "app_scheduler.h"

#define MKB_LOG_LEVEL MKB_TOUCHPAD_LOG_LEVEL
#include "mkb_common.h"
#include "mkb_db.h"

#include "mkb_touchpad.h"

#if (defined(MKB_CYPRESS_ENABLED) && MKB_CYPRESS_ENABLED)
#if (defined(MKB_TOUCHPAD_TEST_ENABLED) && MKB_TOUCHPAD_TEST_ENABLED)
#include "mkb_touchpad_t_cy.h"
#else
#include "mkb_touchpad_t_cy.h" // have to change your header
#endif                         // (defined(MKB_TOUCHPAD_TEST_ENABLED) && MKB_TOUCHPAD_TEST_ENABLED)
#include "drv_cypress.h"
#endif // (defined(MKB_CYPRESS_ENABLED) && MKB_CYPRESS_ENABLED)

#if (defined(MKB_PIXART_ENABLED) && MKB_PIXART_ENABLED)
#if (defined(MKB_TOUCHPAD_TEST_ENABLED) && MKB_TOUCHPAD_TEST_ENABLED)
#include "mkb_touchpad_t_pixart.h"
#else
#include "mkb_touchpad_pixart.h" // have to change your header
#endif                           // (defined(MKB_TOUCHPAD_TEST_ENABLED) && MKB_TOUCHPAD_TEST_ENABLED)
#include "drv_pixart.h"
#endif // (defined(MKB_PIXART_ENABLED) && MKB_PIXART_ENABLED)

#if (defined(MKB_TOUCHPAD_ENABLED) && MKB_TOUCHPAD_ENABLED)

static bool m_touchpad_init;

void mkb_touchpad_read_handler(ret_code_t status, void *p_data)
{
    if (status == NRF_SUCCESS)
    {
        //if (mkb_db_system_state_is_touch())
        {
#if (defined(MKB_TOUCHPAD_TEST_ENABLED) && MKB_TOUCHPAD_TEST_ENABLED)
            mkb_touchpad_test_process(p_data);
#else
            mkb_touchpad_read_process(p_data);
#endif
        }
    }
}

ret_code_t mkb_touchpad_init(void)
{
    ret_code_t status;
    m_touchpad_init = false;

// Touchpad init
#if (defined(MKB_CYPRESS_ENABLED) && MKB_CYPRESS_ENABLED)
    status = drv_cypress_init(mkb_touchpad_read_handler);
    if (status != NRF_SUCCESS)
    {
        m_touchpad_init = false;
        return status;
    }
    else
        m_touchpad_init = true;
#endif // (defined(MKB_CYPRESS_ENABLED) && MKB_CYPRESS_ENABLED)

#if (defined(MKB_PIXART_ENABLED) && MKB_PIXART_ENABLED)
    status = drv_pixart_init(mkb_touchpad_read_handler);
    if (status != NRF_SUCCESS)
    {
        m_touchpad_init = false;
        return status;
    }
    else
    {
        m_touchpad_init = true;
    }
    status = mkb_touchpad_pixart_init();
    if (status != NRF_SUCCESS)
    {
        m_touchpad_init = false;
        return status;
    }
    else
    {
        m_touchpad_init = true;
    }

#endif // (defined(MKB_PIXART_ENABLED) && MKB_PIXART_ENABLED)

    if (m_touchpad_init)
        MKB_LOG_INFO("Touch Sensor Initialize OK!");
    else
        MKB_LOG_ERROR("Touch Sensor Initialize Fail!");

    return NRF_SUCCESS;
}

ret_code_t mkb_touchpad_start(void)
{
    ret_code_t status;

    if (m_touchpad_init == true)
    {
#if (defined(MKB_CYPRESS_ENABLED) && MKB_CYPRESS_ENABLED)
        status = drv_cypress_enable();
        if (status != NRF_SUCCESS)
        {
            return status;
        }
#endif // (defined(MKB_CYPRESS_ENABLED) && MKB_CYPRESS_ENABLED)

#if (defined(MKB_PIXART_ENABLED) && MKB_PIXART_ENABLED)
        status = drv_pixart_enable();
        if (status != NRF_SUCCESS)
        {
            return status;
        }
#endif // (defined(MKB_PIXART_ENABLED) && MKB_PIXART_ENABLED)
    }

    return NRF_SUCCESS;
}

ret_code_t mkb_touchpad_stop(void)
{
    ret_code_t status;

    if (m_touchpad_init == true)
    {
#if (defined(MKB_CYPRESS_ENABLED) && MKB_CYPRESS_ENABLED)
        status = drv_cypress_disable();
        if (status != NRF_SUCCESS)
        {
            return status;
        }
#endif // (defined(MKB_CYPRESS_ENABLED) && MKB_CYPRESS_ENABLED)

#if (defined(MKB_PIXART_ENABLED) && MKB_PIXART_ENABLED)
        status = drv_pixart_disable();
        if (status != NRF_SUCCESS)
        {
            return status;
        }
#endif // (defined(MKB_PIXART_ENABLED) && MKB_PIXART_ENABLED)
    }

    return NRF_SUCCESS;
}

bool mkb_touchpad_update_check(void)
{
    MKB_DB_CONFIG_t *p_cfg = mkb_db_config_get();

#if (defined(MKB_CYPRESS_ENABLED) && MKB_CYPRESS_ENABLED)
#if (defined(MKB_CYPRESS_FW_INCLUDED) && MKB_CYPRESS_FW_INCLUDED)
    if ((p_cfg->touch_fw_major == 0) && (p_cfg->touch_fw_minor == 0))
    {
        drv_cypress_fw_version_get(&p_cfg->touch_fw_major, &p_cfg->touch_fw_minor);
        mkb_db_store_config();
    }

    if (drv_cypress_version_check(p_cfg->touch_fw_major, p_cfg->touch_fw_minor))
        return true;
    else
        return false;
#endif // (defined(MKB_CYPRESS_FW_INCLUDED) && MKB_CYPRESS_FW_INCLUDED)
#endif // (defined(MKB_CYPRESS_ENABLED) && MKB_CYPRESS_ENABLED)
}

void mkb_touchpad_update(void)
{
    MKB_DB_CONFIG_t *p_cfg = mkb_db_config_get();

#if (defined(MKB_CYPRESS_ENABLED) && MKB_CYPRESS_ENABLED)
#if (defined(MKB_CYPRESS_FW_INCLUDED) && MKB_CYPRESS_FW_INCLUDED)
    if (drv_cypress_update_check(p_cfg->touch_fw_major, p_cfg->touch_fw_minor))
    {
        drv_cypress_img_version_get(&p_cfg->touch_fw_major, &p_cfg->touch_fw_minor);
        mkb_db_store_config();
    }
#endif // (defined(MKB_CYPRESS_FW_INCLUDED) && MKB_CYPRESS_FW_INCLUDED)
#endif // (defined(MKB_CYPRESS_ENABLED) && MKB_CYPRESS_ENABLED)
}

#endif // (defined(MKB_TOUCHPAD_ENABLED) && MKB_TOUCHPAD_ENABLED)
