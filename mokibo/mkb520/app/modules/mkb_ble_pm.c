/*
 * ===============================================================================
 * Copyright (C) 2017-2019
 * INNOPRESSO INC. All Rights Reserved
 *
 * The content of this file is copyrighted by INNOPRESSO or its
 * affiliated, associated or related companies. All copyrights and other
 * associated intellectual property rights related thereto are expressly
 * reserved by INNOPRESSO INC.
 * ===============================================================================
 */

#include <stdbool.h>
#include <stdint.h>
#include <stdarg.h>

#include "sdk_common.h"

#include "ble.h"
#include "ble_err.h"
#include "ble_conn_params.h"
#include "ble_conn_state.h"
#include "ble_advertising.h"
#include "ble_gap.h"
#include "ble_bas.h"

#include "nrf_assert.h"
#include "nrf_error.h"
#include "nrf_delay.h"
#include "nrf_cli.h"
#include "nrf_balloc.h"
#include "nrf_pwr_mgmt.h"
#include "nrf_sdh_soc.h"
#include "nrf_sdh_ble.h"

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

#include "peer_manager.h"
#include "peer_manager_handler.h"
#include "bsp.h"
#include "fds.h"

#include "app_error.h"
#include "app_timer.h"
#include "app_scheduler.h"

#define MKB_LOG_LEVEL MKB_BLE_ADV_LOG_LEVEL
#include "mkb_common.h"

#if (defined(MKB_BLE_ENABLED) && MKB_BLE_ENABLED)
#include "mkb_ble.h"
#include "mkb_ble_adv.h"
#include "mkb_ble_pm.h"

#if (defined(MKB_EVENT_ENABLED) && MKB_EVENT_ENABLED)
#include "mkb_event.h"
#else
#error "You have to enable event at mkb_config.h!"
#endif // (defined(MKB_EVENT_ENABLED) && MKB_EVENT_ENABLED)

/**@brief Clear bond information from persistent storage.
 */
void mkb_ble_pm_delete_bonds(void)
{
    ret_code_t err_code;

    MKB_LOG_INFO("Erase bonds!");

    err_code = pm_peers_delete();
    MKB_ERROR_CHECK(err_code);
}

void mkb_ble_pm_delete_peer(CHANNEL_ID_t ch)
{
    MKB_DB_CH_t *p_ch = mkb_db_ch_get();

    pm_peer_delete(p_ch->config.info[ch].peer_id);
}

void mkb_ble_pm_local_address_set(CHANNEL_ID_t ch)
{
    ret_code_t err_code;
    MKB_DB_CH_t *p_ch = mkb_db_ch_get();

    // Set the local address.
    err_code = pm_id_addr_set(&p_ch->config.info[ch].own_addr);
    if (err_code != NRF_SUCCESS)
    {
        MKB_ERROR_CHECK(err_code);
    }
}

ret_code_t mkb_ble_pm_local_address_get(ble_gap_addr_t *p_addr)
{
    ret_code_t err_code;

    // Set the local address.
    err_code = pm_id_addr_get(p_addr);
    if (err_code != NRF_SUCCESS)
    {
        MKB_ERROR_CHECK(err_code);
    }

    return err_code;
}

/**@brief Function for handling Peer Manager events.
 *
 * @param[in] p_evt  Peer Manager event.
 */
static void mkb_ble_pm_evt_handler(pm_evt_t const *p_evt)
{
    ret_code_t err_code;
    MKB_DB_CH_t *p_ch = mkb_db_ch_get();
    CHANNEL_ID_t ch_id = mkb_db_ch_id_get();
    CHANNEL_ID_t ch_prev = mkb_db_ch_id_prev_get();

    pm_handler_on_pm_evt(p_evt);
    pm_handler_flash_clean(p_evt);

    switch (p_evt->evt_id)
    {
    case PM_EVT_BONDED_PEER_CONNECTED:
    {
        MKB_LOG_INFO("PM_EVT_BONDED_PEER_CONNECTED:%d", p_evt->evt_id);
        MKB_LOG_INFO("CH(%d): Connected to a previously bonded device, peer_id: %d.", ch_id, p_evt->peer_id);
        ________DBG_20210930_ble_adv_event("PM_EVT_BONDED_PEER_CONNECTED");

        if (p_evt->peer_id != PM_PEER_ID_INVALID)
        {
            MKB_LOG_INFO("CH(%d): pm_peer_rank_highest(peer_id=%d)", ch_id, p_evt->peer_id);
            pm_peer_rank_highest(p_evt->peer_id);
        }
    }
    break;

    case PM_EVT_CONN_SEC_CONFIG_REQ:
    {
        MKB_LOG_INFO("PM_EVT_CONN_SEC_CONFIG_REQ");
        ________DBG_20210930_ble_adv_event("PM_EVT_CONN_SEC_CONFIG_REQ:%d", p_evt->evt_id);

        pm_conn_sec_config_t conn_sec_config = {.allow_repairing = true};
        MKB_LOG_INFO("CH(%d): pm_conn_sec_config_reply(conn_handle=%d, conn_sec_config), peer_id= %d",
                     ch_id, p_evt->conn_handle, p_evt->peer_id);
        pm_conn_sec_config_reply(p_evt->conn_handle, &conn_sec_config);
    }
    break;

    case PM_EVT_CONN_SEC_FAILED:
    {
        MKB_LOG_INFO("PM_EVT_CONN_SEC_FAILED");
        ________DBG_20210930_ble_adv_event("PM_EVT_CONN_SEC_FAILED:%d", p_evt->evt_id);

        MKB_LOG_INFO("CH(%d): fail state: peer_id= %d, procedure(0x%x), error(0x%x), error_src(%d)",
                     ch_id,
                     p_evt->peer_id,
                     p_evt->params.conn_sec_failed.procedure,
                     p_evt->params.conn_sec_failed.error,
                     p_evt->params.conn_sec_failed.error_src);

        mkb_led_ind_set(p_ch->led[ch_id].adv_fast);
    }
    break;

    case PM_EVT_CONN_SEC_SUCCEEDED:
    {
        MKB_LOG_INFO("PM_EVT_CONN_SEC_SUCCEEDED");
        ________DBG_20210930_ble_adv_event("PM_EVT_CONN_SEC_SUCCEEDED:%d", p_evt->evt_id);
        MKB_LOG_INFO("CH(%d): Connection secured: role: %d, conn_handle: 0x%x, procedure: %d, peer_id: %d.",
                     ch_id,
                     ble_conn_state_role(p_evt->conn_handle),
                     p_evt->conn_handle,
                     p_evt->params.conn_sec_succeeded.procedure,
                     p_evt->peer_id);

        switch (p_evt->params.conn_sec_succeeded.procedure)
        {
        case PM_CONN_SEC_PROCEDURE_ENCRYPTION:
        {
            ________DBG_20210930_ble_adv_event("PM_CONN_SEC_PROCEDURE_ENCRYPTION:%d", p_evt->evt_id);
            MKB_LOG_INFO("PM_CONN_SEC_PROCEDURE_ENCRYPTION succeed.");
        }
        break;

        case PM_CONN_SEC_PROCEDURE_BONDING:
        {
            ________DBG_20210930_ble_adv_event("PM_CONN_SEC_PROCEDURE_BONDING:%d", p_evt->evt_id);
            MKB_LOG_INFO("PM_CONN_SEC_PROCEDURE_BONDING succeed: Bonding has been successful.");
        }
        break;

        case PM_CONN_SEC_PROCEDURE_PAIRING:
        {
            ________DBG_20210930_ble_adv_event("PM_CONN_SEC_PROCEDURE_PAIRING:%d", p_evt->evt_id);
            MKB_LOG_INFO("PM_CONN_SEC_PROCEDURE_PAIRING succeed.");
        }
        break;
        }

        mkb_ble_adv_state_reset();
    }
    break;

    case PM_EVT_PEERS_DELETE_SUCCEEDED:
    {
        MKB_LOG_INFO("PM_EVT_PEERS_DELETE_SUCCEEDED");
        ________DBG_20210930_ble_adv_event("PM_EVT_PEERS_DELETE_SUCCEEDED:%d", p_evt->evt_id);

        for (uint8_t i = 0; i < CHANNEL_ID_MAX; i++)
        {
            p_ch->config.info[i].peer_id = PM_PEER_ID_INVALID;
            memcpy(&p_ch->config.info[i].peer_addr, 0, sizeof(ble_gap_addr_t));
            if (p_ch->conn_handle[i] != BLE_CONN_HANDLE_INVALID)
            {
                mkb_ble_disconnect(i);
            }
        }

        mkb_db_store_ch();
    }
    break;

    case PM_EVT_PEER_DELETE_SUCCEEDED:
    {
        MKB_LOG_INFO("PM_EVT_PEER_DELETE_SUCCEEDED: peer_id: %d, conn_handle: %d", p_evt->peer_id, p_evt->conn_handle);
        ________DBG_20210930_ble_adv_event("PM_EVT_PEER_DELETE_SUCCEEDED:%d", p_evt->evt_id);

        for (uint8_t i = 0; i < CHANNEL_ID_MAX; i++)
        {
            if (p_ch->config.info[i].peer_id == p_evt->peer_id)
            {
                MKB_LOG_INFO("CH(%d): update from peer_id= %d", i, p_evt->peer_id);
                p_ch->config.info[i].peer_id = PM_PEER_ID_INVALID;
                p_ch->config.info[i].state = false;

                // if(i != ch_id)
                //     memset(&p_ch->config.info[i].peer_addr, 0, sizeof(ble_gap_addr_t));

                mkb_db_store_ch();
            }
        }

        if (mkb_db_wait_for_disconnect_get())
        {
            if (p_ch->conn_handle[ch_id] != BLE_CONN_HANDLE_INVALID)
            {
                MKB_LOG_INFO("CH(%d): disconnect request", ch_id);
                mkb_ble_disconnect(ch_id);
            }
            else
            {
                if (p_ch->conn_handle[ch_prev] != BLE_CONN_HANDLE_INVALID)
                {
                    MKB_LOG_INFO("CH(%d): disconnect request", ch_prev);
                    mkb_ble_disconnect(ch_prev);
                }
                else
                {
                    mkb_db_wait_for_disconnect_set(false);

                    if (mkb_ble_adv_wait_for_advertizing_get() == ADV_MODE_DIRECTED)
                    {
                        MKB_LOG_INFO("CH(%d): start advertize with directed mode", ch_id);
                        mkb_ble_adv_start(ch_id, ADV_MODE_DIRECTED);
                    }
                    else
                    {
                        MKB_LOG_INFO("CH(%d): start advertize with undirected mode", ch_id);
                        mkb_ble_adv_start(ch_id, ADV_MODE_UNDIRECTED);
                    }
                }
            }
        }
        else
        {
            if (p_ch->conn_handle[ch_id] != BLE_CONN_HANDLE_INVALID)
            {
                MKB_LOG_INFO("CH(%d): disconnect request", ch_id);
                mkb_ble_disconnect(ch_id);
            }
        }
    }
    break;

    case PM_EVT_PEER_DATA_UPDATE_SUCCEEDED:
    {
        pm_peer_data_bonding_t peer_bonding_data;

        MKB_LOG_INFO("PM_EVT_PEER_DATA_UPDATE_SUCCEEDED");
        ________DBG_20210930_ble_adv_event("PM_EVT_PEER_DATA_UPDATE_SUCCEEDED:%d", p_evt->evt_id);

        if (p_evt->params.peer_data_update_succeeded.flash_changed && (p_evt->params.peer_data_update_succeeded.data_id == PM_PEER_DATA_ID_BONDING))
        {
            // Note: You should check on what kind of white list policy your application should use.
            MKB_LOG_INFO("CH(%d): New Bond, add the peer(%d) to the whitelist if possible", ch_id, p_evt->peer_id);

            mkb_ble_pm_whitelist_set(PM_PEER_ID_LIST_SKIP_NO_ID_ADDR);

            if (p_ch->config.info[ch_id].peer_id != p_evt->peer_id)
            {
                MKB_LOG_INFO("CH(%d): update the peer id(%d) to the db", ch_id, p_evt->peer_id);

                p_ch->config.info[ch_id].state = true;
                p_ch->config.info[ch_id].peer_id = p_evt->peer_id;
                mkb_db_store_ch();
            }

            err_code = pm_peer_data_bonding_load(p_evt->peer_id, &peer_bonding_data);
            if (err_code != NRF_ERROR_NOT_FOUND)
            {
                MKB_ERROR_CHECK(err_code);

                ble_gap_addr_t *p_peer_addr = &(peer_bonding_data.peer_ble_id.id_addr_info);
                ble_gap_addr_t local_addr;
                mkb_ble_pm_local_address_get(&local_addr);

                if (memcmp(&p_ch->config.info[ch_id].peer_addr, p_peer_addr, sizeof(ble_gap_addr_t)))
                {
                    MKB_LOG_DEBUG("CH(%d): peer id: %d, peer addr: 0x%02x%02x%02x%02x%02x%02x, local addr: 0x%02x%02x%02x%02x%02x%02x",
                                  ch_id, p_evt->peer_id,
                                  p_peer_addr->addr[5], p_peer_addr->addr[4], p_peer_addr->addr[3],
                                  p_peer_addr->addr[2], p_peer_addr->addr[1], p_peer_addr->addr[0],
                                  local_addr.addr[5], local_addr.addr[4], local_addr.addr[3],
                                  local_addr.addr[2], local_addr.addr[1], local_addr.addr[0]);

                    memcpy(&p_ch->config.info[ch_id].peer_addr, p_peer_addr, sizeof(ble_gap_addr_t));
                    mkb_db_store_ch();
                }
            }
        }
    }
    break;

    case PM_EVT_STORAGE_FULL:
    {
        MKB_LOG_INFO("PM_EVT_STORAGE_FULL, Run garbage collection on the flash.");
        ________DBG_20210930_ble_adv_event("PM_EVT_STORAGE_FULL:%d", p_evt->evt_id);

        // Run garbage collection on the flash.
        err_code = fds_gc();
        if (err_code == FDS_ERR_BUSY || err_code == FDS_ERR_NO_SPACE_IN_QUEUES)
        {
            // Retry.
            MKB_LOG_INFO("You need to run garbage collection again!");
        }
        else
        {
            MKB_ERROR_CHECK(err_code);
        }
    }
    break;

    case PM_EVT_CONN_SEC_PARAMS_REQ:
    {
        ________DBG_20210930_ble_adv_event("PM_EVT_CONN_SEC_PARAMS_REQ:%d", p_evt->evt_id);
    }
    break;

    case PM_EVT_LOCAL_DB_CACHE_APPLIED:
    {
        ________DBG_20210930_ble_adv_event("PM_EVT_LOCAL_DB_CACHE_APPLIED:%d", p_evt->evt_id);
    }
    break;

    case PM_EVT_CONN_SEC_START:
    {
        ________DBG_20210930_ble_adv_event("PM_EVT_CONN_SEC_START:%d", p_evt->evt_id);
    }
    break;

    default:
        ________DBG_20210930_ble_adv_event("NO_MATCH_PM_EVENT:%d", p_evt->evt_id);
        break;
    }
}

/**@brief Function for the Peer Manager initialization.
 */
void mkb_ble_pm_init(void)
{
    ble_gap_sec_params_t sec_param;
    ret_code_t err_code;

    err_code = pm_init();
    MKB_ERROR_CHECK(err_code);

    memset(&sec_param, 0, sizeof(ble_gap_sec_params_t));

    // Security parameters to be used for all security procedures.
    sec_param.bond = SEC_PARAM_BOND;
    sec_param.mitm = SEC_PARAM_MITM;
    sec_param.lesc = SEC_PARAM_LESC;
    sec_param.keypress = SEC_PARAM_KEYPRESS;
    sec_param.io_caps = SEC_PARAM_IO_CAPABILITIES;
    sec_param.oob = SEC_PARAM_OOB;
    sec_param.min_key_size = SEC_PARAM_MIN_KEY_SIZE;
    sec_param.max_key_size = SEC_PARAM_MAX_KEY_SIZE;
    sec_param.kdist_own.enc = 1;
    sec_param.kdist_own.id = 1;
    sec_param.kdist_peer.enc = 1;
    sec_param.kdist_peer.id = 1;

    err_code = pm_sec_params_set(&sec_param);
    MKB_ERROR_CHECK(err_code);

    err_code = pm_register(mkb_ble_pm_evt_handler);
    MKB_ERROR_CHECK(err_code);
}

/**@brief Function for setting filtered whitelist.
 *
 * @param[in] skip  Filter passed to @ref pm_peer_id_list.
 */
void mkb_ble_pm_whitelist_set(pm_peer_id_list_skip_t skip)
{
    pm_peer_id_t peer_ids[BLE_GAP_WHITELIST_ADDR_MAX_COUNT];
    uint32_t peer_id_count = BLE_GAP_WHITELIST_ADDR_MAX_COUNT;

    ret_code_t err_code = pm_peer_id_list(peer_ids, &peer_id_count, PM_PEER_ID_INVALID, skip);
    APP_ERROR_CHECK(err_code);

    MKB_LOG_INFO("\tm_whitelist_peer_cnt %d, MAX_PEERS_WLIST %d",
                 peer_id_count,
                 BLE_GAP_WHITELIST_ADDR_MAX_COUNT);

    err_code = pm_whitelist_set(peer_ids, peer_id_count);
    MKB_ERROR_CHECK(err_code);
}

void mkb_ble_pm_whitelist_clear(void)
{
    ret_code_t err_code = pm_whitelist_set(NULL, 0);
    MKB_ERROR_CHECK(err_code);
}

/**@brief Function for setting filtered device identities.
 *
 * @param[in] skip  Filter passed to @ref pm_peer_id_list.
 */
void mkb_ble_pm_identities_set(pm_peer_id_list_skip_t skip)
{
    pm_peer_id_t peer_ids[BLE_GAP_DEVICE_IDENTITIES_MAX_COUNT];
    uint32_t peer_id_count = BLE_GAP_DEVICE_IDENTITIES_MAX_COUNT;

    ret_code_t err_code = pm_peer_id_list(peer_ids, &peer_id_count, PM_PEER_ID_INVALID, skip);
    MKB_ERROR_CHECK(err_code);

    err_code = pm_device_identities_list_set(peer_ids, peer_id_count);
    if (err_code != 12804) // Unknown error code
    {
        MKB_ERROR_CHECK(err_code);
    }
}

void mkb_ble_pm_identities_clear(void)
{
    ret_code_t err_code = pm_device_identities_list_set(NULL, 0);
    MKB_ERROR_CHECK(err_code);
}

bool mkb_ble_pm_state(CHANNEL_ID_t ch)
{
    MKB_DB_CH_t *p_ch = mkb_db_ch_get();

    return ((p_ch->config.info[ch].state == true) && (p_ch->config.info[ch].peer_id != PM_PEER_ID_INVALID));
}

#endif // (defined(MKB_BLE_ENABLED) && MKB_BLE_ENABLED)