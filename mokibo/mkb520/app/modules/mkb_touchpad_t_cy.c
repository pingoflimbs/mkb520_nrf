/*
* ===============================================================================
* Copyright (C) 2017-2019
* INNOPRESSO INC. All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
* ===============================================================================
*/

#include <stdbool.h>
#include <stdint.h>

#include "sdk_common.h"

#include "nrf_assert.h"
#include "nrf_error.h"
#include "nrf_log.h"
#include "nrf_delay.h"
#include "nrf_cli.h"

#include "app_error.h"
#include "app_timer.h"
#include "app_scheduler.h"

#define MKB_LOG_LEVEL MKB_TOUCHPAD_LOG_LEVEL
#include "mkb_common.h"
#include "mkb_db.h"
#include "mkb_event.h"
#include "mkb_ble.h"

#if (defined(MKB_CYPRESS_ENABLED) && MKB_CYPRESS_ENABLED)
#include "drv_cypress.h"
#endif // (defined(MKB_CYPRESS_ENABLED) && MKB_CYPRESS_ENABLED)

#include "mkb_touchpad.h"
#include "mkb_touchpad_t_cy.h"
#include "math.h"

#if (defined(MKB_CYPRESS_ENABLED) && MKB_CYPRESS_ENABLED)

typedef void (*touchpad_state_machine)(cypress_info_t *p_data);

static touchpad_job_state_t m_touchpad_job_state = TOUCHPAD_JOB_STATE_IDLE;
static cypress_info_t m_touch_info_first;
static cypress_info_t m_touch_info_pre;
static uint16_t m_touch_time_start;

static uint16_t touch_point_delta_time(uint16_t prv, uint16_t now);
static touch_coordinate_t touch_point_delta_move(touch_coordinate_t pre, touch_coordinate_t now);
static touch_coordinate_t touch_point_delta_scroll(touch_coordinate_t pre, touch_coordinate_t now);

static void touchpad_process_one(cypress_info_t *p_data);
static void touchpad_process_two(cypress_info_t *p_data);
static void touchpad_process_three(cypress_info_t *p_data);
static void touchpad_process_four(cypress_info_t *p_data);
static void touchpad_tap_one(cypress_info_t *p_data);
static void touchpad_tap_two(cypress_info_t *p_data);

static void touchpad_sm_idle(cypress_info_t *p_data);
static void touchpad_sm_one(cypress_info_t *p_data);
static void touchpad_sm_two(cypress_info_t *p_data);
static void touchpad_sm_three(cypress_info_t *p_data);
static void touchpad_sm_four(cypress_info_t *p_data);

static touchpad_state_machine m_touchpad_handler[] =
    {
        touchpad_sm_idle,
        touchpad_sm_one,
        touchpad_sm_two,
        touchpad_sm_three,
        touchpad_sm_four,
};

void mkb_touchpad_test_process(void *p_data)
{
    m_touchpad_handler[m_touchpad_job_state]((cypress_info_t *)p_data);
}

static void touchpad_process_one(cypress_info_t *p_data)
{
    cypress_finger_info_t *p_info;
    touch_info_t touch_info;

#if 0
    memset(&touch_info, 0, sizeof(touch_info));

    p_info = (cypress_finger_info_t *)&p_data->fingers[0];
#if MKB_BLE_TOUCH_DEVICE_TYPE == BLE_TOUCH_DEVICE_PAD
    touch_info.fingers[0].touchID = p_info->touchID;
    touch_info.fingers[0].tip = p_info->tip;
    touch_info.fingers[0].pressure = p_info->pressure;
    touch_info.touchCnt = p_data->touchCnt;
    touch_info.timestamp = MKB_SCANTIME(p_data->timestamp);
#elif MKB_BLE_TOUCH_DEVICE_TYPE == BLE_TOUCH_DEVICE_SCREEN
    touch_info.fingers[0].touchID = p_info->touchID;
    //touch_info.fingers[0].tip = p_info->tip;
    touch_info.touchCnt = p_data->touchCnt;
#elif MKB_BLE_TOUCH_DEVICE_TYPE == BLE_TOUCH_DEVICE_PEN
    touch_info.fingers[0].tip = p_info->tip;
#elif MKB_BLE_TOUCH_DEVICE_TYPE == BLE_TOUCH_DEVICE_POINTER
    touch_info.fingers[0].button = p_info->tip;
#endif
    touch_info.fingers[0].x = p_info->x;
    touch_info.fingers[0].y = p_info->y;

    mkb_event_send(EVT_TRACPAD_MOVE, &touch_info, sizeof(touch_info));
#else
    touch_coordinate_t now, pre, delta;

    p_info = (cypress_finger_info_t *)&p_data->fingers[0];
    now.x = p_info->x;
    now.y = p_info->y;

    pre.x = m_touch_info_pre.fingers[0].x;
    pre.y = m_touch_info_pre.fingers[0].y;

    delta = touch_point_delta_move(pre, now);

    if ((abs(delta.x) >= TOUCHPAD_MOVE_MINIMUM) || (abs(delta.y) >= TOUCHPAD_MOVE_MINIMUM))
    {
        MKB_LOG_INFO("TOUCH MOVE pre(x:%d,y:%d), cur(x:%d,y:%d), delta(x:%d,y:%d)",
                     pre.x, pre.y, now.x, now.y, delta.x, delta.y);

        // send mouse move event to event manager
        mkb_event_send(EVT_TOUCH_MOVE, delta.x, delta.y);
    }
#endif
}

static void touchpad_tap_one(cypress_info_t *p_data)
{
    cypress_finger_info_t *p_info;
    touch_coordinate_t now, pre, delta;
    uint16_t delta_t;

    p_info = (cypress_finger_info_t *)&m_touch_info_pre.fingers[0];
    now.x = p_info->x;
    now.y = p_info->y;
    pre.x = m_touch_info_first.fingers[0].x;
    pre.y = m_touch_info_first.fingers[0].y;

    delta = touch_point_delta_move(pre, now);
    delta_t = touch_point_delta_time(m_touch_time_start, m_touch_info_pre.timestamp);

    if ((p_info->eventID == 3) && !p_info->tip &&
        //(p_info->pressure > TOUCHPAD_TAP_THRESHOLD_PRESSURE) &&
        (delta_t > TOUCHPAD_TAP_THRESHOLD_TIME_MIN && delta_t < TOUCHPAD_TAP_THRESHOLD_TIME_MAX) &&
        ((abs(delta.x) < MKB_TOUCHPAD_TAP_THRESHOLD_MOVE) && (abs(delta.y) < MKB_TOUCHPAD_TAP_THRESHOLD_MOVE)))
    {
        // send mouse click event to event manager
        mkb_event_send(EVT_TOUCH_TAB, TOUCHPAD_TAP_ONE);

        MKB_LOG_INFO("TOUCH TAP Time delta(%d), delta(x:%d,y:%d), pressure(%d)", delta_t, delta.x, delta.y, p_info->pressure);
    }
    else
    {
        MKB_LOG_INFO("TOUCH Time delta(%d), delta(x:%d,y:%d), pressure(%d)", delta_t, delta.x, delta.y, p_info->pressure);
    }
}

static void touchpad_process_two(cypress_info_t *p_data)
{
    cypress_finger_info_t *p_info;
    touch_coordinate_t now, pre, first, delta;
    touch_info_t touch_info;

    p_info = (cypress_finger_info_t *)&p_data->fingers[0];
    now.x = p_info->x;
    now.y = p_info->y;

    pre.x = m_touch_info_pre.fingers[0].x;
    pre.y = m_touch_info_pre.fingers[0].y;

    if ((p_data->fingers[0].tip == 1 && p_data->fingers[1].tip == 1) &&
        (p_data->fingers[0].eventID == 2 && p_data->fingers[1].eventID == 2))
    {
        delta = touch_point_delta_scroll(pre, now);

        if (abs(delta.x) >= TOUCHPAD_MOVE_MINIMUM || abs(delta.y) >= TOUCHPAD_MOVE_MINIMUM)
        {
            // send mouse scroll event to event managex
            if (abs(delta.x) > abs(delta.y))
            {
#if 0
                memset(&touch_info, 0, sizeof(touch_info));

#if MKB_BLE_TOUCH_DEVICE_TYPE == BLE_TOUCH_DEVICE_PAD
                for(uint8_t i = 0; i < p_data->touchCnt; i++)
                {
                    p_info = (cypress_finger_info_t *)&p_data->fingers[i];
                    touch_info.fingers[i].touchID = p_info->touchID;
                    touch_info.fingers[i].tip = p_info->tip;
                    touch_info.fingers[i].pressure = p_info->pressure;
                    touch_info.fingers[i].x = p_info->x;
                    touch_info.fingers[i].y = p_info->y;
                }
                touch_info.touchCnt = p_data->touchCnt;
                touch_info.timestamp = MKB_SCANTIME(p_data->timestamp);
#elif MKB_BLE_TOUCH_DEVICE_TYPE == BLE_TOUCH_DEVICE_SCREEN
                for(uint8_t i = 0; i < p_data->touchCnt; i++)
                {
                    p_info = (cypress_finger_info_t *)&p_data->fingers[i];
                    touch_info.fingers[i].touchID = p_info->touchID;
                    //touch_info.fingers[i].tip = p_info->tip;
                    touch_info.fingers[i].x = p_info->x;
                    touch_info.fingers[i].y = p_info->y;
                }
                touch_info.touchCnt = p_data->touchCnt;
#elif MKB_BLE_TOUCH_DEVICE_TYPE == BLE_TOUCH_DEVICE_PEN
                touch_info.fingers[0].tip = p_info->tip;
                touch_info.fingers[0].x = p_info->x;
                touch_info.fingers[0].y = p_info->y;
#elif MKB_BLE_TOUCH_DEVICE_TYPE == BLE_TOUCH_DEVICE_POINTER
                touch_info.fingers[0].button = p_info->tip;
                touch_info.fingers[0].x = p_info->x;
                touch_info.fingers[0].y = p_info->y;
#endif
    
                mkb_event_send(EVT_TRACPAD_MOVE, &touch_info, sizeof(touch_info));
#else
                mkb_event_send(EVT_TOUCH_SCROLL, delta.x, 0);
#endif
            }
            else
                mkb_event_send(EVT_TOUCH_SCROLL, 0, delta.y);

            MKB_LOG_INFO("TOUCH MOVE pre(x:%d,y:%d), cur(x:%d,y:%d), delta(x:%d,y:%d)",
                         pre.x, pre.y, now.x, now.y, delta.x, delta.y);
        }
    }
}

static void touchpad_tap_two(cypress_info_t *p_data)
{
    cypress_finger_info_t *p_info;
    touch_coordinate_t now, pre, first, delta;
    uint16_t delta_t;
    uint8_t m_eventID;
    uint8_t m_tip;
    uint8_t m_presure;

    p_info = (cypress_finger_info_t *)&m_touch_info_pre.fingers[0];
    now.x = p_info->x;
    now.y = p_info->y;

    pre.x = m_touch_info_first.fingers[0].x;
    pre.y = m_touch_info_first.fingers[0].y;

    delta = touch_point_delta_scroll(pre, now);
    delta_t = touch_point_delta_time(m_touch_time_start, m_touch_info_pre.timestamp);

    m_eventID = (p_data->fingers[0].eventID == 3 || p_data->fingers[1].eventID == 3) ? 3 : 2;
    m_tip = (p_data->fingers[0].tip == 0 || p_data->fingers[1].tip == 0) ? 0 : 1;
    m_presure = MAX(p_data->fingers[0].pressure, p_data->fingers[1].pressure);

    if ( //(p_info->eventID == 3) && !p_info->tip &&
        //(p_info->pressure > TOUCHPAD_TAP_THRESHOLD_PRESSURE) &&
        (delta_t > TOUCHPAD_TAP_THRESHOLD_TIME_MIN && delta_t < TOUCHPAD_TAP_THRESHOLD_TIME_MAX) &&
        ((abs(delta.x) < MKB_TOUCHPAD_TAP_THRESHOLD_MOVE) && (abs(delta.y) < MKB_TOUCHPAD_TAP_THRESHOLD_MOVE)))
    {
        // send mouse click event to event manager
        mkb_event_send(EVT_TOUCH_TAB, TOUCHPAD_TAP_TWO);

        MKB_LOG_INFO("TOUCH TAP Time delta(%d), delta(x:%d,y:%d), pressure(%d)", delta_t, delta.x, delta.y, p_info->pressure);
    }
    else
    {
        MKB_LOG_INFO("TOUCH Time delta(%d), delta(x:%d,y:%d), pressure(%d)", delta_t, delta.x, delta.y, p_info->pressure);
    }
}

static void touchpad_process_three(cypress_info_t *p_data)
{
    cypress_finger_info_t *p_info;
    touch_info_t touch_info;

#if 1
    bool state = false;

    for (uint8_t i = 0; i < p_data->touchCnt; i++)
    {
        if (p_data->fingers[i].tip == 0)
            state = true;
    }

    memset(&touch_info, 0, sizeof(touch_info));

    for (uint8_t i = 0; i < p_data->touchCnt; i++)
    {
        p_info = (cypress_finger_info_t *)&p_data->fingers[i];
        touch_info.fingers[i].touchID = p_info->touchID;
        if (state)
            touch_info.fingers[i].tip = 0;
        else
            touch_info.fingers[i].tip = p_info->tip;
        touch_info.fingers[i].pressure = p_info->pressure;
        touch_info.fingers[i].x = p_info->x;
        touch_info.fingers[i].y = p_info->y;
    }

    touch_info.touchCnt = p_data->touchCnt;
    touch_info.timestamp = MKB_SCANTIME(p_data->timestamp);
    mkb_event_send(EVT_TRACPAD_MOVE, &touch_info, sizeof(touch_info));
#else
    cypress_finger_info_t *p_info;
    int16_t delta_x, delta_y;
    uint16_t abs_x, abs_y, x_now, x_pre, y_now, y_pre;

    if (p_data->touchCnt == 3)
    {
        p_info = (cypress_finger_info_t *)&p_data->fingers[0];
        x_now = p_info->x;
        y_now = p_info->y;

        x_pre = m_touch_info_pre.fingers[0].x;
        y_pre = m_touch_info_pre.fingers[0].y;

        delta_x = x_now - x_pre;
        delta_y = y_now - y_pre;
        abs_x = abs(delta_x);
        abs_y = abs(delta_y);
        //delta_y = (delta_y < 0) ? abs_y : (delta_y > 0) ? -delta_y : 0;
        delta_y = -(delta_y);

        if (abs_x >= 5 || abs_y >= 5)
        {
            MKB_LOG_INFO("MOUSE MOVE pre(x:%d,y:%d), cur(x:%d,y:%d), delta(x:%d,y:%d)",
                         x_pre, y_pre, x_now, y_now, delta_x, delta_y);
            mkb_event_send(EVT_TOUCH_MOVE, delta_x, delta_y);
        }
    }
#endif
}

static void touchpad_process_four(cypress_info_t *p_data)
{
    cypress_finger_info_t *p_info;
    touch_info_t touch_info;

#if 1
    memset(&touch_info, 0, sizeof(touch_info));

    for (uint8_t i = 0; i < p_data->touchCnt; i++)
    {
        p_info = (cypress_finger_info_t *)&p_data->fingers[i];
        touch_info.fingers[i].touchID = p_info->touchID;
        touch_info.fingers[i].tip = p_info->tip;
        touch_info.fingers[i].x = p_info->x;
        touch_info.fingers[i].y = p_info->y;
    }

    touch_info.touchCnt = p_data->touchCnt;
    touch_info.timestamp = MKB_SCANTIME(p_data->timestamp);
    mkb_event_send(EVT_TRACPAD_MOVE, &touch_info, sizeof(touch_info));
#else
    int16_t delta_x, delta_y;
    uint16_t abs_x, abs_y, x_now, x_pre, y_now, y_pre;

    if (p_data->touchCnt == 4)
    {
        p_info = (cypress_finger_info_t *)&p_data->fingers[0];
        x_now = p_info->x;
        y_now = p_info->y;

        x_pre = m_touch_info_pre.fingers[0].x;
        y_pre = m_touch_info_pre.fingers[0].y;

        delta_x = x_now - x_pre;
        delta_y = y_now - y_pre;
        abs_x = abs(delta_x);
        abs_y = abs(delta_y);
        delta_y = -(delta_y);

        //if(abs_x >= 5 || abs_y >= 5)
        {
            MKB_LOG_INFO("MOUSE MOVE pre(x:%d,y:%d), cur(x:%d,y:%d), delta(x:%d,y:%d)",
                         x_pre, y_pre, x_now, y_now, delta_x, delta_y);
            //mkb_event_send(EVT_TOUCH_MOVE, delta_x, delta_y);
        }
    }
#endif
}

///////////////////////////////////////////////////////////////////////////////
static void touchpad_sm_idle(cypress_info_t *p_data)
{
    cypress_finger_info_t *p_info;

    p_info = (cypress_finger_info_t *)&p_data->fingers[0];

    if ((p_info->eventID == 1 || p_info->eventID == 2) && p_info->tip)
    {
        memcpy(&m_touch_info_first, p_data, sizeof(cypress_info_t));
        memcpy(&m_touch_info_pre, p_data, sizeof(cypress_info_t));

        m_touch_time_start = p_data->timestamp;

        switch (p_data->touchCnt)
        {
        case 1:
            MKB_LOG_INFO("Change State to One");
            m_touchpad_job_state = TOUCHPAD_JOB_STATE_ONE;
            break;
        case 2:
            MKB_LOG_INFO("Change State to Two");
            m_touchpad_job_state = TOUCHPAD_JOB_STATE_TWO;
            break;
        case 3:
            MKB_LOG_INFO("Change State to Three");
            m_touchpad_job_state = TOUCHPAD_JOB_STATE_THREE;
            break;
        case 4:
            MKB_LOG_INFO("Change State to Four");
            m_touchpad_job_state = TOUCHPAD_JOB_STATE_FOUR;
            break;
        }
    }
}

static void touchpad_sm_one(cypress_info_t *p_data)
{
    cypress_finger_info_t *p_info;
    touch_coordinate_t now, pre, delta;
    uint16_t delta_t;

    switch (p_data->touchCnt)
    {
    case 1:
        touchpad_process_one(p_data);
        memcpy(&m_touch_info_pre, p_data, sizeof(cypress_info_t));
        break;
    case 2:
        m_touchpad_job_state = TOUCHPAD_JOB_STATE_TWO;
        MKB_LOG_INFO("Change State to Two");
        memcpy(&m_touch_info_first, p_data, sizeof(cypress_info_t));
        memcpy(&m_touch_info_pre, p_data, sizeof(cypress_info_t));
        break;
    case 3:
        m_touchpad_job_state = TOUCHPAD_JOB_STATE_THREE;
        MKB_LOG_INFO("Change State to Three");
        memcpy(&m_touch_info_first, p_data, sizeof(cypress_info_t));
        memcpy(&m_touch_info_pre, p_data, sizeof(cypress_info_t));
        break;
    case 4:
        m_touchpad_job_state = TOUCHPAD_JOB_STATE_FOUR;
        MKB_LOG_INFO("Change State to Four");
        memcpy(&m_touch_info_first, p_data, sizeof(cypress_info_t));
        memcpy(&m_touch_info_pre, p_data, sizeof(cypress_info_t));
        break;
    case 0:
        touchpad_tap_one(p_data);
        m_touchpad_job_state = TOUCHPAD_JOB_STATE_IDLE;
        MKB_LOG_INFO("Change State to Idle");
        break;
    }
}

static void touchpad_sm_two(cypress_info_t *p_data)
{
    switch (p_data->touchCnt)
    {
    case 1:
        memcpy(&m_touch_info_pre, p_data, sizeof(cypress_info_t));
        break;
    case 2:
        touchpad_process_two(p_data);
        memcpy(&m_touch_info_pre, p_data, sizeof(cypress_info_t));
        break;
    case 3:
        m_touchpad_job_state = TOUCHPAD_JOB_STATE_THREE;
        MKB_LOG_INFO("Change State to Three");
        memcpy(&m_touch_info_first, p_data, sizeof(cypress_info_t));
        memcpy(&m_touch_info_pre, p_data, sizeof(cypress_info_t));
        break;
    case 4:
        m_touchpad_job_state = TOUCHPAD_JOB_STATE_FOUR;
        MKB_LOG_INFO("Change State to Three");
        memcpy(&m_touch_info_first, p_data, sizeof(cypress_info_t));
        memcpy(&m_touch_info_pre, p_data, sizeof(cypress_info_t));
        break;
    case 0:
        touchpad_tap_two(p_data);
        m_touchpad_job_state = TOUCHPAD_JOB_STATE_IDLE;
        MKB_LOG_INFO("Change State to Idle");
        break;
    }
}

static void touchpad_sm_three(cypress_info_t *p_data)
{
    switch (p_data->touchCnt)
    {
    case 1:
        memcpy(&m_touch_info_pre, p_data, sizeof(cypress_info_t));
        break;
    case 2:
        memcpy(&m_touch_info_pre, p_data, sizeof(cypress_info_t));
        break;
    case 3:
        touchpad_process_three(p_data);
        memcpy(&m_touch_info_pre, p_data, sizeof(cypress_info_t));
        break;
    case 4:
        m_touchpad_job_state = TOUCHPAD_JOB_STATE_FOUR;
        MKB_LOG_INFO("Change State to Four");
        memcpy(&m_touch_info_first, p_data, sizeof(cypress_info_t));
        memcpy(&m_touch_info_pre, p_data, sizeof(cypress_info_t));
        break;
    case 0:
        m_touchpad_job_state = TOUCHPAD_JOB_STATE_IDLE;
        MKB_LOG_INFO("Change State to Idle");
        break;
    }
}

static void touchpad_sm_four(cypress_info_t *p_data)
{
    switch (p_data->touchCnt)
    {
    case 1:
        memcpy(&m_touch_info_pre, p_data, sizeof(cypress_info_t));
        break;
    case 2:
        memcpy(&m_touch_info_pre, p_data, sizeof(cypress_info_t));
        break;
    case 3:
        memcpy(&m_touch_info_pre, p_data, sizeof(cypress_info_t));
        break;
    case 4:
        touchpad_process_four(p_data);
        memcpy(&m_touch_info_pre, p_data, sizeof(cypress_info_t));
        break;
    case 0:
        m_touchpad_job_state = TOUCHPAD_JOB_STATE_IDLE;
        MKB_LOG_INFO("Change State to Idle");
        break;
    }
}

///////////////////////////////////////////////////////////////////////////////
static uint16_t touch_point_delta_time(uint16_t prv, uint16_t now)
{
    uint16_t deltaT = 0;

    if (now != prv)
    {
        if ((now - prv) >= 0)
        {
            deltaT = (now - prv);
        }
        else
        {
            deltaT = (0xFFFF - prv); // This code valid when timestamp size is uint16_t
            deltaT += now;
        }
    }
    return deltaT;
}

static touch_coordinate_t touch_point_delta_move(touch_coordinate_t pre, touch_coordinate_t now)
{
    // Exponential increase of cursor movement speed:
    // m_x = x*sqrt(abs(x)); m_y = y*sqrt(abs(y)

    int16_t delta_x, delta_y, move_x, move_y;
    uint16_t abs_x, abs_y, x_now, x_pre, y_now, y_pre;
    touch_coordinate_t delta;

    delta_x = now.x - pre.x;
    delta_y = now.y - pre.y;
    abs_x = abs(delta_x);
    abs_y = abs(delta_y);
    delta_y = -(delta_y);

#if (defined(MKB_TOUCHPAD_MOVE_SPEED) && MKB_TOUCHPAD_MOVE_SPEED)
    if (TOUCHPAD_MOVE_SPEED > 0)
    {
        if (abs_x > MKB_TOUCHPAD_MOVE_SPEED_THRESHOLD)
            move_x = MIN(abs_x * (sqrt(abs_x) * TOUCHPAD_MOVE_SPEED / 100), 0x7FF);
        else
            move_x = MIN(abs_x, 0x7FF);

        if (abs_y > MKB_TOUCHPAD_MOVE_SPEED_THRESHOLD)
            move_y = MIN(abs_y * (sqrt(abs_y) * TOUCHPAD_MOVE_SPEED / 100), 0x7FF);
        else
            move_y = MIN(abs_y, 0x7FF);
    }
    else
    {
        move_x = MIN(abs_x, 0x7FF);
        move_y = MIN(abs_y, 0x7FF);
    }
#else
    move_x = MIN(abs_x, 0x7FF);
    move_y = MIN(abs_y, 0x7FF);
#endif

    delta.x = move_x * ((delta_x < 0) ? -1 : 1);
    delta.y = move_y * ((delta_y < 0) ? -1 : 1);

    return delta;
}

static touch_coordinate_t touch_point_delta_scroll(touch_coordinate_t pre, touch_coordinate_t now)
{
    // Exponential increase of cursor movement speed:
    // m_x = x*sqrt(abs(x)); m_y = y*sqrt(abs(y)

    int16_t delta_x, delta_y, move_x, move_y;
    uint16_t abs_x, abs_y, x_now, x_pre, y_now, y_pre;
    touch_coordinate_t delta;

    delta_x = now.x - pre.x;
    delta_y = now.y - pre.y;
    abs_x = abs(delta_x);
    abs_y = abs(delta_y);
    delta_y = -(delta_y);

#if (defined(MKB_TOUCHPAD_SCROLL_SPEED) && MKB_TOUCHPAD_SCROLL_SPEED)
    if (TOUCHPAD_MOVE_SPEED > 0)
    {
        move_x = MIN(abs_x * (sqrt(abs_x) * TOUCHPAD_SCROLL_SPEED / 100), 0x7F);
        move_y = MIN(abs_y * (sqrt(abs_y) * TOUCHPAD_SCROLL_SPEED / 100), 0x7F);
    }
    else
    {
        move_x = MIN(abs_x, 0x7F);
        move_y = MIN(abs_y, 0x7F);
    }
#else
        move_x = MIN(abs_x * (sqrt(abs_x) * 0.5)), 0x7F);
        move_y = MIN(abs_y * (sqrt(abs_y) * 0.5)), 0x7F);
#endif

    delta.x = move_x * ((delta_x < 0) ? -1 : 1);
    delta.y = move_y * ((delta_y < 0) ? -1 : 1);

    return delta;
}

#endif // (defined(MKB_CYPRESS_ENABLED) && MKB_CYPRESS_ENABLED)
