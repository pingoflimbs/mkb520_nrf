/*
* ===============================================================================
* Copyright (C) 2017-2019
* INNOPRESSO INC. All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
* ===============================================================================
*/

#include <stdbool.h>
#include <stdint.h>

#include "sdk_common.h"

#include "nrf_assert.h"
#include "nrf_error.h"
#include "nrf_log.h"
#include "nrf_delay.h"
#include "nrf_cli.h"

#include "app_error.h"
#include "app_timer.h"
#include "app_scheduler.h"

#define MKB_LOG_LEVEL MKB_TOUCHPAD_LOG_LEVEL
#include "mkb_common.h"
#include "mkb_db.h"
#include "mkb_ble.h"
#include "mkb_util.h"
#include "mkb_coms.h"

#if (defined(MKB_PIXART_ENABLED) && MKB_PIXART_ENABLED)
#include "drv_pixart.h"
#endif // (defined(MKB_PIXART_ENABLED) && MKB_PIXART_ENABLED)

#include "mkb_touchpad.h"
#include "mkb_touchpad_t_pixart.h"
#include "math.h"

#if (defined(MKB_PIXART_ENABLED) && MKB_PIXART_ENABLED)

typedef void (*touchpad_state_machine)(mokibo_touch_data_t *p_data);

static touchpad_job_state_t m_touchpad_job_state = TOUCHPAD_JOB_STATE_IDLE;
static mokibo_touch_data_t m_touch_info_first;
static mokibo_touch_data_t m_touch_info_pre;
static uint32_t m_touch_time_start;
static bool m_touch_tap;

// These values are in pixels:
static uint32_t m_touch_pix_width = 1024; // e.g. 1024 = 1024 pixels
static uint32_t m_touch_pix_height = 620; // e.g.  768 =  768 pixels
// These values are in percent multiplied with 100:
static uint16_t m_touch_pct_pos_x = 5000;   // e.g. 5000 = 50.00% of screen width
static uint16_t m_touch_pct_pos_y = 5000;   // e.g. 1550 = 15.50% of screen height
static uint16_t m_touch_pct_offset_x = 0;   // e.g.  750 =  7.50% of screen width
static uint16_t m_touch_pct_offset_y = 0;   // e.g. 1000 = 10.00% of screen height
static uint16_t m_touch_pct_width = 10000;  // e.g. 8500 = 85.00% of screen width
static uint16_t m_touch_pct_height = 10000; // e.g. 9000 = 90.00% of screen height

static uint16_t m_touch_pixelWidth = 1024;
static uint16_t m_touch_pixelHeight = 620;
static uint16_t m_touch_marginLeft = 0;
static uint16_t m_touch_marginTop = 0;
static uint16_t m_touch_marginRight = 1024;
static uint16_t m_touch_marginBottom = 620;

static uint16_t touch_point_delta_time(uint32_t prv, uint32_t now);
static touch_coordinate_t touch_point_delta_move(touch_coordinate_t pre, touch_coordinate_t now);
static touch_coordinate_t touch_point_delta_scroll(touch_coordinate_t pre, touch_coordinate_t now);

static void touchpad_process_one(mokibo_touch_data_t *p_data);
static void touchpad_process_two(mokibo_touch_data_t *p_data);
static void touchpad_process_three(mokibo_touch_data_t *p_data);
static void touchpad_process_four(mokibo_touch_data_t *p_data);
static void touchpad_process_tab_five(mokibo_touch_data_t *p_data);
static void touchpad_process_tab_one(mokibo_touch_data_t *p_data);
static void touchpad_process_tab_two(mokibo_touch_data_t *p_data);
static void touchpad_process_tab_three(mokibo_touch_data_t *p_data);
static void touchpad_process_tab_four(mokibo_touch_data_t *p_data);
static void touchpad_process_tab_five(mokibo_touch_data_t *p_data);

static void touchpad_sm_idle(mokibo_touch_data_t *p_data);
static void touchpad_sm_one(mokibo_touch_data_t *p_data);
static void touchpad_sm_two(mokibo_touch_data_t *p_data);
static void touchpad_sm_three(mokibo_touch_data_t *p_data);
static void touchpad_sm_four(mokibo_touch_data_t *p_data);
static void touchpad_sm_five(mokibo_touch_data_t *p_data);

static touchpad_state_machine m_touchpad_handler[] =
    {
        touchpad_sm_idle,
        touchpad_sm_one,
        touchpad_sm_two,
        touchpad_sm_three,
        touchpad_sm_four,
        touchpad_sm_five,
};

void mkb_touchpad_test_process(void *p_data)
{
    m_touchpad_handler[m_touchpad_job_state]((mokibo_touch_data_t *)p_data);
}

void mkb_touchpad_screen_size(uint16_t pixelWidth, uint16_t pixelHeight,
                              uint16_t marginLeft, uint16_t marginTop, uint16_t marginRight, uint16_t marginBottom)
{
    // in pixel
    m_touch_pix_width = pixelWidth;
    m_touch_pix_height = pixelHeight;
    // in percent * 100
    m_touch_pct_offset_x = marginLeft;
    m_touch_pct_offset_y = marginTop;
    m_touch_pct_width = marginRight - marginLeft;
    m_touch_pct_height = marginBottom - marginTop;
    // in pixel
    m_touch_pixelWidth = pixelWidth;
    m_touch_pixelHeight = pixelHeight;
    // in percent * 100
    m_touch_marginLeft = marginLeft;
    m_touch_marginTop = marginTop;
    m_touch_marginRight = marginRight;
    m_touch_marginBottom = marginBottom;
}

bool mkb_touchpad_screen(uint16_t x, uint16_t y)
{
    bool res = false;

    if (x > m_touch_marginLeft && x < m_touch_marginRight)
        res = true;
    if (y > m_touch_marginTop && y < m_touch_marginBottom)
        res = true;

    return res;
}

void mkb_touchpad_position(uint16_t x, uint16_t y)
{
//#if BLE_TOUCH_DEVICE == BLE_TOUCH_DEVICE_PAD
#if 0
    touch_coordinate_t now, pre, delta;

        now.x = x;
        now.y = y;

        pre.x = m_touch_info_pre.fingers[0].x;
        pre.y = m_touch_info_pre.fingers[0].y;

        delta = touch_point_delta_move(pre, now);

    if(x > 95 && x < 1183) m_touch_pct_pos_x = x;
    else
    {
        if(x <= 95) m_touch_pct_pos_x = 0;
        if(x >= 1183) m_touch_pct_pos_x = 1278;
    }
    if(y > 57 && y < 709) m_touch_pct_pos_y = y;
    else
    {
        if(y <= 57) m_touch_pct_pos_y = 57;
        if(y >= 709) m_touch_pct_pos_y = 1183;
    }
#else
    if (m_touch_pix_width > 0 && m_touch_pix_height > 0)
    {
        // Convert pixel --> percent * 100 with margin correction
        x = m_touch_pct_offset_x + (m_touch_pct_width * x) / m_touch_pix_width;
        y = m_touch_pct_offset_y + (m_touch_pct_height * y) / m_touch_pix_height;
    }

    // Send absolute screen position in percent * 100 (values from 0 to 10000)
    m_touch_pct_pos_x = MIN(x, 10000);
    m_touch_pct_pos_y = MIN(y, 10000);
#endif
}

void touchpad_touch_move(uint8_t id, uint8_t cnt, uint16_t x, uint16_t y, uint8_t tip, uint16_t time, bool button)
{
    BLE_REPORT_DATA_TOUCH_t buf = {0};

#if BLE_TOUCH_DEVICE == BLE_TOUCH_DEVICE_PAD
    //buf.confidence = 1;//(p_info->tip) ? p_info->confidence:0;
    //buf.tip = tip; // touch is currently on the panel
    //buf.touchID = id;    // An arbitrary ID tag associated with a finger ( 0 ~ 9)
    //buf.timestamp = time;		// unit: 0.1ms
    buf.touchCnt = cnt; // total number of current touches
//buf.button = button;		/* 1byte */
//mkb_touchpad_screen_size(1278, 766, 95, 57, 1183, 709);
#elif BLE_TOUCH_DEVICE == BLE_TOUCH_DEVICE_SCREEN
    buf.touchCnt = cnt; // total number of current touches
    buf.touchID = id;   // An arbitrary ID tag associated with a finger ( 0 ~ 9)
    buf.tip = button;   // touch is currently on the panel
    buf.inrange = 1;    // touch is currently on the panel
#elif BLE_TOUCH_DEVICE == BLE_TOUCH_DEVICE_PEN
    buf.tip = button; // touch is currently on the panel
    buf.inrange = 1;  // touch is currently on the panel
#elif BLE_TOUCH_DEVICE == BLE_TOUCH_DEVICE_POINTER
    buf.button = button; // touch is currently on the panel
#endif

    mkb_touchpad_position(x, y);

    //buf.x = m_touch_pct_pos_x;			  // 16bit x-coordinates in pixels
    //buf.y = m_touch_pct_pos_y;			  // 16bit y-coordinates in pixels

    //MKB_LOG_INFO("TOUCH (x:%d,y:%d)",buf.x, buf.y);

    mkb_ble_touchpad_send((uint8_t *)&buf, sizeof(buf));
}

static void touchpad_process_one(mokibo_touch_data_t *p_data)
{
    mokibo_finger_data_t *p_info;
    touch_coordinate_t now, pre, delta;
    uint16_t delta_t;

    p_info = (mokibo_finger_data_t *)&p_data->fingers[0];
    now.x = p_info->x;
    now.y = p_info->y;

    pre.x = m_touch_info_pre.fingers[0].x;
    pre.y = m_touch_info_pre.fingers[0].y;

    delta = touch_point_delta_move(pre, now);

    if (p_info->tip == 0 || (abs(delta.x) >= TOUCHPAD_MOVE_MINIMUM) || (abs(delta.y) >= TOUCHPAD_MOVE_MINIMUM))
    {
        MKB_LOG_INFO("TOUCH MOVE pre(x:%d,y:%d), cur(x:%d,y:%d), delta(x:%d,y:%d)",
                     pre.x, pre.y, now.x, now.y, delta.x, delta.y);

#ifdef BLE_TOUCH_DEVICE
//#if BLE_TOUCH_DEVICE == BLE_TOUCH_DEVICE_PAD
#if 0
        if(mkb_touchpad_screen(p_info->x, p_info->y))
        {
            mkb_event_send(EVT_TOUCH_MOVE, delta.x, delta.y);
        }
        else
#endif
        {
            delta_t = touch_point_delta_time(m_touch_info_pre.timestamp, p_data->timestamp);
            mkb_event_send(EVT_TOUCH_MOVE, delta.x, delta.y);
            //touchpad_touch_move(p_info->touchID, p_data->touchCnt, p_info->x, p_info->y, p_info->tip, MKB_SCANTIME(delta_t), mkb_coms_is_button_left());
        }
#else
        // send mouse move event to event manager
        mkb_event_send(EVT_TOUCH_MOVE, delta.x, delta.y);
#endif
    }
}

static void touchpad_process_tab_one(mokibo_touch_data_t *p_data)
{
    mokibo_finger_data_t *p_info;
    touch_coordinate_t now, pre, delta;
    uint16_t delta_t;

    now.x = m_touch_info_pre.fingers[0].x;
    now.y = m_touch_info_pre.fingers[0].y;

    pre.x = m_touch_info_first.fingers[0].x;
    pre.y = m_touch_info_first.fingers[0].y;

    delta = touch_point_delta_move(pre, now);
    delta_t = touch_point_delta_time(m_touch_time_start, m_touch_info_pre.timestamp);

    if ((delta_t > TOUCHPAD_TAP_THRESHOLD_TIME_MIN && delta_t < TOUCHPAD_TAP_THRESHOLD_TIME_MAX) &&
        ((abs(delta.x) <= TOUCHPAD_TAP_THRESHOLD_MOVE) && (abs(delta.y) <= TOUCHPAD_TAP_THRESHOLD_MOVE)))
    {
// send mouse click event to event manager
#ifdef BLE_TOUCH_DEVICE
#if BLE_TOUCH_DEVICE == BLE_TOUCH_DEVICE_PAD
        mkb_event_send(EVT_TOUCH_TAB, TOUCHPAD_TAP_ONE);
#else
        touchpad_touch_move(p_info->touchID, p_data->touchCnt, now.x, now.y, p_info->tip, MKB_SCANTIME(delta_t), true);
        touchpad_touch_move(p_info->touchID, p_data->touchCnt, now.x, now.y, p_info->tip, MKB_SCANTIME(delta_t), false);
#endif
#else
        mkb_event_send(EVT_TOUCH_TAB, TOUCHPAD_TAP_ONE);
#endif

        MKB_LOG_INFO("TOUCH TAP delta Time(%d), delta Move(x:%d,y:%d)", delta_t, delta.x, delta.y);
    }
}

static void touchpad_process_two(mokibo_touch_data_t *p_data)
{
    mokibo_finger_data_t *p_info;
    touch_coordinate_t now, pre, delta;
    uint16_t delta_t;

    delta_t = touch_point_delta_time(m_touch_info_pre.timestamp, p_data->timestamp);

    for (uint8_t i; i < p_data->touchCnt; i++)
    {
        p_info = (mokibo_finger_data_t *)&p_data->fingers[i];
        now.x = p_info->x;
        now.y = p_info->y;

        pre.x = m_touch_info_pre.fingers[i].x;
        pre.y = m_touch_info_pre.fingers[i].y;

        delta = touch_point_delta_move(pre, now);

        if (p_info->tip == 0 || (abs(delta.x) >= TOUCHPAD_MOVE_MINIMUM) || (abs(delta.y) >= TOUCHPAD_MOVE_MINIMUM))
        {
            MKB_LOG_INFO("TOUCH MOVE pre(x:%d,y:%d), cur(x:%d,y:%d), delta(x:%d,y:%d)",
                         pre.x, pre.y, now.x, now.y, delta.x, delta.y);

#ifdef BLE_TOUCH_DEVICE
//#if BLE_TOUCH_DEVICE == BLE_TOUCH_DEVICE_PAD
#if 0
            if(mkb_touchpad_screen(p_info->x, p_info->y))
            {
                mkb_event_send(EVT_TOUCH_MOVE, delta.x, delta.y);
            }
            else
#endif
            {
                mkb_event_send(EVT_TOUCH_SCROLL, delta.x, delta.y);
                //touchpad_touch_move(p_info->touchID, p_data->touchCnt, p_info->x, p_info->y, p_info->tip, MKB_SCANTIME(delta_t), mkb_coms_is_button_left());
            }
#else
            // send mouse move event to event manager
            mkb_event_send(EVT_TOUCH_MOVE, delta.x, delta.y);
#endif
        }
    }
}

static void touchpad_process_tab_two(mokibo_touch_data_t *p_data)
{
    mokibo_finger_data_t *p_info;
    touch_coordinate_t now, pre, delta;
    uint16_t delta_t;

    now.x = m_touch_info_pre.fingers[0].x;
    now.y = m_touch_info_pre.fingers[0].y;

    pre.x = m_touch_info_first.fingers[0].x;
    pre.y = m_touch_info_first.fingers[0].y;

    delta = touch_point_delta_move(pre, now);
    delta_t = touch_point_delta_time(m_touch_time_start, m_touch_info_pre.timestamp);

    if ((delta_t > TOUCHPAD_TAP_THRESHOLD_TIME_MIN && delta_t < TOUCHPAD_TAP_THRESHOLD_TIME_MAX) &&
        ((abs(delta.x) <= TOUCHPAD_TAP_THRESHOLD_MOVE) && (abs(delta.y) <= TOUCHPAD_TAP_THRESHOLD_MOVE)))
    {
        // send mouse click event to event manager
        mkb_event_send(EVT_TOUCH_TAB, TOUCHPAD_TAP_TWO);

        MKB_LOG_INFO("TOUCH TAP delta Time(%d), delta Move(x:%d,y:%d)", delta_t, delta.x, delta.y);
    }
}

static void touchpad_process_three(mokibo_touch_data_t *p_data)
{
    mokibo_finger_data_t *p_info;
    touch_coordinate_t now, pre, delta;
    uint16_t delta_t;
    touch_info_t touch_info;

    memset(&touch_info, 0, sizeof(touch_info));

    for (uint8_t i = 0; i < p_data->touchCnt; i++)
    {
        p_info = (mokibo_finger_data_t *)&p_data->fingers[i];
        touch_info.fingers[i].touchID = p_info->touchID;
        touch_info.fingers[i].tip = p_info->tip;
        touch_info.fingers[i].pressure = p_info->z;
        touch_info.fingers[i].x = p_info->x;
        touch_info.fingers[i].y = p_info->y;
    }

    touch_info.touchCnt = p_data->touchCnt;
    touch_info.timestamp = p_data->timestamp;
    mkb_event_send(EVT_TRACPAD_MOVE, &touch_info, sizeof(touch_info));
}

static void touchpad_process_tab_three(mokibo_touch_data_t *p_data)
{
    mokibo_finger_data_t *p_info;
    touch_coordinate_t now, pre, delta;
    uint16_t delta_t;

    touch_info_t touch_info;

    memset(&touch_info, 0, sizeof(touch_info));

    for (uint8_t i = 0; i < p_data->touchCnt; i++)
    {
        p_info = (mokibo_finger_data_t *)&p_data->fingers[i];
        touch_info.fingers[i].touchID = p_info->touchID;
        touch_info.fingers[i].tip = p_info->tip;
        touch_info.fingers[i].pressure = p_info->z;
        touch_info.fingers[i].x = p_info->x;
        touch_info.fingers[i].y = p_info->y;
    }

    touch_info.touchCnt = p_data->touchCnt;
    touch_info.timestamp = p_data->timestamp;
    mkb_event_send(EVT_TRACPAD_MOVE, &touch_info, sizeof(touch_info));
}

static void touchpad_process_four(mokibo_touch_data_t *p_data)
{
    mokibo_finger_data_t *p_info;
    touch_coordinate_t now, pre, delta;
    uint16_t delta_t;

    touch_info_t touch_info;

    memset(&touch_info, 0, sizeof(touch_info));

    for (uint8_t i = 0; i < p_data->touchCnt; i++)
    {
        p_info = (mokibo_finger_data_t *)&p_data->fingers[i];
        touch_info.fingers[i].touchID = p_info->touchID;
        touch_info.fingers[i].tip = p_info->tip;
        touch_info.fingers[i].pressure = p_info->z;
        touch_info.fingers[i].x = p_info->x;
        touch_info.fingers[i].y = p_info->y;
    }

    touch_info.touchCnt = p_data->touchCnt;
    touch_info.timestamp = p_data->timestamp;
    mkb_event_send(EVT_TRACPAD_MOVE, &touch_info, sizeof(touch_info));
}

static void touchpad_process_tab_four(mokibo_touch_data_t *p_data)
{
    mokibo_finger_data_t *p_info;
    touch_coordinate_t now, pre, delta;
    uint16_t delta_t;
}

static void touchpad_process_five(mokibo_touch_data_t *p_data)
{
    mokibo_finger_data_t *p_info;
    touch_coordinate_t now, pre, delta;
    uint16_t delta_t;
}

static void touchpad_process_tab_five(mokibo_touch_data_t *p_data)
{
    mokibo_finger_data_t *p_info;
    touch_coordinate_t now, pre, delta;
    uint16_t delta_t;
}

///////////////////////////////////////////////////////////////////////////////
static void touchpad_sm_idle(mokibo_touch_data_t *p_data)
{
    mokibo_finger_data_t *p_info;

    p_info = (mokibo_finger_data_t *)&p_data->fingers[0];

    memcpy(&m_touch_info_first, p_data, sizeof(mokibo_touch_data_t));
    memcpy(&m_touch_info_pre, p_data, sizeof(mokibo_touch_data_t));

    m_touch_time_start = p_data->timestamp;

#ifdef BLE_TOUCH_DEVICE
#if !((BLE_TOUCH_DEVICE == BLE_TOUCH_DEVICE_PAD) && (BLE_TOUCH_DEVICE == BLE_TOUCH_DEVICE_SCREEN))
    p_data->touchCnt = 1;
#endif
#endif

    switch (p_data->touchCnt)
    {
    case 1:
        m_touchpad_job_state = TOUCHPAD_JOB_STATE_ONE;
        break;
    case 2:
        m_touchpad_job_state = TOUCHPAD_JOB_STATE_TWO;
        break;
    case 3:
        m_touchpad_job_state = TOUCHPAD_JOB_STATE_THREE;
        break;
    case 4:
        m_touchpad_job_state = TOUCHPAD_JOB_STATE_FOUR;
        break;
    case 5:
        m_touchpad_job_state = TOUCHPAD_JOB_STATE_FIVE;
        break;
    }
}

static void touchpad_sm_one(mokibo_touch_data_t *p_data)
{
    switch (p_data->touchCnt)
    {
    case 1:
        touchpad_process_one(p_data);
        memcpy(&m_touch_info_pre, p_data, sizeof(mokibo_touch_data_t));
        break;
    case 2:
        touchpad_process_two(p_data);
        m_touchpad_job_state = TOUCHPAD_JOB_STATE_TWO;
        MKB_LOG_INFO("Change State to Two");
        memcpy(&m_touch_info_pre, p_data, sizeof(mokibo_touch_data_t));
        break;
    case 3:
        touchpad_process_three(p_data);
        m_touchpad_job_state = TOUCHPAD_JOB_STATE_THREE;
        MKB_LOG_INFO("Change State to Three");
        memcpy(&m_touch_info_pre, p_data, sizeof(mokibo_touch_data_t));
        break;
    case 4:
        touchpad_process_four(p_data);
        m_touchpad_job_state = TOUCHPAD_JOB_STATE_FOUR;
        MKB_LOG_INFO("Change State to Four");
        memcpy(&m_touch_info_pre, p_data, sizeof(mokibo_touch_data_t));
        break;
    case 5:
        touchpad_process_five(p_data);
        m_touchpad_job_state = TOUCHPAD_JOB_STATE_FIVE;
        MKB_LOG_INFO("Change State to Five");
        memcpy(&m_touch_info_pre, p_data, sizeof(mokibo_touch_data_t));
        break;
    case 0:
        touchpad_process_tab_one(p_data);
        m_touchpad_job_state = TOUCHPAD_JOB_STATE_IDLE;
        MKB_LOG_INFO("Change State to Idle");
        break;
    }
}

static void touchpad_sm_two(mokibo_touch_data_t *p_data)
{
    switch (p_data->touchCnt)
    {
    case 1:
        touchpad_process_one(p_data);
        m_touchpad_job_state = TOUCHPAD_JOB_STATE_ONE;
        MKB_LOG_INFO("Change State to One");
        memcpy(&m_touch_info_pre, p_data, sizeof(mokibo_touch_data_t));
        break;
    case 2:
        touchpad_process_two(p_data);
        memcpy(&m_touch_info_pre, p_data, sizeof(mokibo_touch_data_t));
        break;
    case 3:
        touchpad_process_three(p_data);
        m_touchpad_job_state = TOUCHPAD_JOB_STATE_THREE;
        MKB_LOG_INFO("Change State to Three");
        memcpy(&m_touch_info_pre, p_data, sizeof(mokibo_touch_data_t));
        break;
    case 4:
        touchpad_process_four(p_data);
        m_touchpad_job_state = TOUCHPAD_JOB_STATE_FOUR;
        MKB_LOG_INFO("Change State to Four");
        memcpy(&m_touch_info_pre, p_data, sizeof(mokibo_touch_data_t));
        break;
    case 5:
        touchpad_process_five(p_data);
        m_touchpad_job_state = TOUCHPAD_JOB_STATE_FIVE;
        MKB_LOG_INFO("Change State to Five");
        memcpy(&m_touch_info_pre, p_data, sizeof(mokibo_touch_data_t));
        break;
    case 0:
        touchpad_process_tab_two(p_data);
        m_touchpad_job_state = TOUCHPAD_JOB_STATE_IDLE;
        MKB_LOG_INFO("Change State to Idle");
        break;
    }
}

static void touchpad_sm_three(mokibo_touch_data_t *p_data)
{
    switch (p_data->touchCnt)
    {
    case 1:
        touchpad_process_one(p_data);
        m_touchpad_job_state = TOUCHPAD_JOB_STATE_ONE;
        MKB_LOG_INFO("Change State to One");
        memcpy(&m_touch_info_pre, p_data, sizeof(mokibo_touch_data_t));
        break;
    case 2:
        touchpad_process_two(p_data);
        memcpy(&m_touch_info_pre, p_data, sizeof(mokibo_touch_data_t));
        break;
    case 3:
        touchpad_process_three(p_data);
        m_touchpad_job_state = TOUCHPAD_JOB_STATE_THREE;
        MKB_LOG_INFO("Change State to Three");
        memcpy(&m_touch_info_pre, p_data, sizeof(mokibo_touch_data_t));
        break;
    case 4:
        touchpad_process_four(p_data);
        m_touchpad_job_state = TOUCHPAD_JOB_STATE_FOUR;
        MKB_LOG_INFO("Change State to Four");
        memcpy(&m_touch_info_pre, p_data, sizeof(mokibo_touch_data_t));
        break;
    case 5:
        touchpad_process_five(p_data);
        m_touchpad_job_state = TOUCHPAD_JOB_STATE_FIVE;
        MKB_LOG_INFO("Change State to Five");
        memcpy(&m_touch_info_pre, p_data, sizeof(mokibo_touch_data_t));
        break;
    case 0:
        touchpad_process_tab_two(p_data);
        m_touchpad_job_state = TOUCHPAD_JOB_STATE_IDLE;
        MKB_LOG_INFO("Change State to Idle");
        break;
    }
}

static void touchpad_sm_four(mokibo_touch_data_t *p_data)
{
}

static void touchpad_sm_five(mokibo_touch_data_t *p_data)
{
}
///////////////////////////////////////////////////////////////////////////////
static uint16_t touch_point_delta_time(uint32_t prv, uint32_t now)
{
    uint32_t deltaT = 0;

    deltaT = app_timer_cnt_diff_compute(now, prv);
    uint16_t ms = mkb_util_timer_ms(deltaT);
    return ms;
}

static touch_coordinate_t touch_point_delta_move(touch_coordinate_t pre, touch_coordinate_t now)
{
    // Exponential increase of cursor movement speed:
    // m_x = x*sqrt(abs(x)); m_y = y*sqrt(abs(y)

    int16_t delta_x, delta_y, move_x, move_y;
    uint16_t abs_x, abs_y, x_now, x_pre, y_now, y_pre;
    touch_coordinate_t delta;

    if (now.x >= pre.x)
        delta_x = now.x - pre.x;
    else
    {
        delta_x = pre.x - now.x;
        delta_x *= -1;
    }
    if (now.y >= pre.y)
        delta_y = now.y - pre.y;
    else
    {
        delta_y = pre.y - now.y;
        delta_y *= -1;
    }

    abs_x = abs(delta_x);
    abs_y = abs(delta_y);
    //delta_y = -(delta_y);

#if (defined(MKB_TOUCHPAD_MOVE_SPEED) && MKB_TOUCHPAD_MOVE_SPEED)
    if (TOUCHPAD_MOVE_SPEED > 0)
    {
        if (abs_x > MKB_TOUCHPAD_MOVE_SPEED_THRESHOLD)
            move_x = MIN(abs_x * (sqrt(abs_x) * TOUCHPAD_MOVE_SPEED / 100), 0x7FF);
        else
            move_x = MIN(abs_x, 0x7FF);

        if (abs_y > MKB_TOUCHPAD_MOVE_SPEED_THRESHOLD)
            move_y = MIN(abs_y * (sqrt(abs_y) * TOUCHPAD_MOVE_SPEED / 100), 0x7FF);
        else
            move_y = MIN(abs_y, 0x7FF);
    }
    else
    {
        move_x = MIN(abs_x, 0x7FF);
        move_y = MIN(abs_y, 0x7FF);
    }
#else
    move_x = MIN(abs_x, 0x7FF);
    move_y = MIN(abs_y, 0x7FF);
#endif

    delta.x = move_x * ((delta_x < 0) ? -1 : 1);
    delta.y = move_y * ((delta_y < 0) ? -1 : 1);

    return delta;
}

static touch_coordinate_t touch_point_delta_scroll(touch_coordinate_t pre, touch_coordinate_t now)
{
    // Exponential increase of cursor movement speed:
    // m_x = x*sqrt(abs(x)); m_y = y*sqrt(abs(y)

    int16_t delta_x, delta_y, move_x, move_y;
    uint16_t abs_x, abs_y, x_now, x_pre, y_now, y_pre;
    touch_coordinate_t delta;

    delta_x = now.x - pre.x;
    delta_y = now.y - pre.y;
    abs_x = abs(delta_x);
    abs_y = abs(delta_y);
    //delta_y = -(delta_y);

#if (defined(MKB_TOUCHPAD_SCROLL_SPEED) && MKB_TOUCHPAD_SCROLL_SPEED)
    if (TOUCHPAD_MOVE_SPEED > 0)
    {
        move_x = MIN(abs_x * (sqrt(abs_x) * TOUCHPAD_SCROLL_SPEED / 100), 0x7F);
        move_y = MIN(abs_y * (sqrt(abs_y) * TOUCHPAD_SCROLL_SPEED / 100), 0x7F);
    }
    else
    {
        move_x = MIN(abs_x, 0x7F);
        move_y = MIN(abs_y, 0x7F);
    }
#else
        move_x = MIN(abs_x * (sqrt(abs_x) * 0.5)), 0x7F);
        move_y = MIN(abs_y * (sqrt(abs_y) * 0.5)), 0x7F);
#endif

    delta.x = move_x * ((delta_x < 0) ? -1 : 1);
    delta.y = move_y * ((delta_y < 0) ? -1 : 1);

    return delta;
}

#endif // (defined(MKB_PIXART_ENABLED) && MKB_PIXART_ENABLED)
