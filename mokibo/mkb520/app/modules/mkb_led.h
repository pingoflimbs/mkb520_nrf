/*
 * ===============================================================================
 * Copyright (C) 2017-2019
 * INNOPRESSO INC. All Rights Reserved
 *
 * The content of this file is copyrighted by INNOPRESSO or its
 * affiliated, associated or related companies. All copyrights and other
 * associated intellectual property rights related thereto are expressly
 * reserved by INNOPRESSO INC.
 * ===============================================================================
 */

#ifndef __MKB_LED__
#define __MKB_LED__

#include "mkb_common.h"

#define LED_RGB_PIN_4_RED MKB_LED_RGB_PIN_4_RED
#define LED_RGB_PIN_4_GREEN MKB_LED_RGB_PIN_4_GREEN
#define LED_RGB_PIN_4_BLUE MKB_LED_RGB_PIN_4_BLUE

#define LED_RGB_PIN_3_RED MKB_LED_RGB_PIN_3_RED
#define LED_RGB_PIN_3_GREEN MKB_LED_RGB_PIN_3_GREEN
#define LED_RGB_PIN_3_BLUE MKB_LED_RGB_PIN_3_BLUE

#define LED_RGB_PIN_2_RED MKB_LED_RGB_PIN_2_RED
#define LED_RGB_PIN_2_GREEN MKB_LED_RGB_PIN_2_GREEN
#define LED_RGB_PIN_2_BLUE MKB_LED_RGB_PIN_2_BLUE

#define LED_RGB_PIN_1_RED MKB_LED_RGB_PIN_1_RED
#define LED_RGB_PIN_1_GREEN MKB_LED_RGB_PIN_1_GREEN
#define LED_RGB_PIN_1_BLUE MKB_LED_RGB_PIN_1_BLUE

#define LED_DIMMING_IN_INTERVAL 20
#define LED_DIMMING_IN_TIMEOUT 200
#define LED_DIMMING_OUT_INTERVAL 20

#define LED_FAST_ON_INTERVAL 200
#define LED_FAST_OFF_INTERVAL 200

#define LED_SLOW_ON_INTERVAL 400
#define LED_SLOW_OFF_INTERVAL 2000

typedef enum
{
    MKB_LED_LAYER_BASE = 0,
    MKB_LED_LAYER_SUB
} MKB_LED_LAYER_t;

typedef enum
{
    MKB_LED_CLR_RED,
    MKB_LED_CLR_GREEN,
    MKB_LED_CLR_BLUE,
    MKB_LED_CLR_YELLOW,
    MKB_LED_CLR_MAGENTA,
    MKB_LED_CLR_WHITE,
    MKB_LED_CLR_CYAN,
    MKB_LED_CLR_NONE,
    MKB_LED_CLR_IOS,
    MKB_LED_CLR_MAC,
    MKB_LED_CLR_WINDOWS,
    MKB_LED_CLR_ANDROD,
    MKB_LED_CLR_DEX,
    MKB_LED_CLR_MAX,
} MKB_LED_CLR_t;

typedef enum
{
    MKB_LED_NUM_1,
    MKB_LED_NUM_2,
    MKB_LED_NUM_3,
    MKB_LED_NUM_PWR,
    MKB_LED_NUM_NONE,
} MKB_LED_NUM_t;

typedef enum
{
    MKB_LED_ACT_OFF = 0,
    MKB_LED_ACT_ON_50S,
    MKB_LED_ACT_ON_PUSH_DOWN,
    MKB_LED_ACT_ON_PUSH_UP,
    MKB_LED_ACT_SLOW_BLINK,
    MKB_LED_ACT_FAST_BLINK,
    MKB_LED_ACT_DIMMING_OFF,
    MKB_LED_ACT_DIMMING_ON,
    MKB_LED_ACT_RESET,
} MKB_LED_ACT_t;

//배터리 꽂으면 PWR 동작함
// Fn 누르면 SUB 동작함

//부팅함 충전없음   Fn누름  OS누름  차징    디차징  10초
//배터리    ON      ON      ON      ON      OFF     OFF
//채널      ON      ON      ON      ON      ON      OFF

//부팅함 충전있음   Fn누름  OS누름  10초
//배터리    ON      ON      ON      ON
//채널      ON      ON      ON      ON

//컬러바꾸는 경우의수 : 배터리측정, OS변경

// on_batt_charge
// on_batt_discharge
// on_batt_check
// on_batt_alert
// on_batt_remain_change
// on_batt_sub_check

// on_chnl_select
// on_chnl_adv_fast
// on_chnl_adv_slow
// on_chnl_connect : batt_charge 와 관련있음
// on_chnl_disconnect
// on_chnl_os_change
// on_chnl_sub_os_check

typedef enum
{
    LED_IND_CH1_DISCONNECT = 0, // 1
    LED_IND_CH2_DISCONNECT,
    LED_IND_CH3_DISCONNECT,
    LED_IND_CH4_DISCONNECT,
    LED_IND_CH1_TOUCH, // 5
    LED_IND_CH2_TOUCH,
    LED_IND_CH3_TOUCH,
    LED_IND_CH4_TOUCH,
    LED_IND_CH1_TOUCHLOCK, // 9
    LED_IND_CH2_TOUCHLOCK,
    LED_IND_CH3_TOUCHLOCK,
    LED_IND_CH4_TOUCHLOCK,
    LED_IND_CH1_BUTTON, // 13
    LED_IND_CH2_BUTTON,
    LED_IND_CH3_BUTTON,
    LED_IND_CH4_BUTTON,
    LED_IND_CH1_CONNECT, // 17
    LED_IND_CH2_CONNECT,
    LED_IND_CH3_CONNECT,
    LED_IND_CH4_CONNECT,
    LED_IND_CH1_SELECT, // 21
    LED_IND_CH2_SELECT,
    LED_IND_CH3_SELECT,
    LED_IND_CH4_SELECT,
    LED_IND_CH1_ADV_DIRECT, // 25
    LED_IND_CH2_ADV_DIRECT,
    LED_IND_CH3_ADV_DIRECT,
    LED_IND_CH4_ADV_DIRECT,
    LED_IND_CH1_ADV_WHITELIST, // 29
    LED_IND_CH2_ADV_WHITELIST,
    LED_IND_CH3_ADV_WHITELIST,
    LED_IND_CH4_ADV_WHITELIST,
    LED_IND_CH1_ADV_FAST, // 33
    LED_IND_CH2_ADV_FAST,
    LED_IND_CH3_ADV_FAST,
    LED_IND_CH4_ADV_FAST,
    LED_IND_CH1_ADV_SLOW, // 37
    LED_IND_CH2_ADV_SLOW,
    LED_IND_CH3_ADV_SLOW,
    LED_IND_CH4_ADV_SLOW,
    LED_IND_CH1_ADV_IDLE, // 4 1ble idle
    LED_IND_CH2_ADV_IDLE,
    LED_IND_CH3_ADV_IDLE,
    LED_IND_CH4_ADV_IDLE,
    LED_IND_FUNC_DOWN,
    LED_IND_FUNC_UP,
    LED_IND_USB, // 45
    LED_IND_USB_POWER_ON,
    LED_IND_USB_POWER_OFF,
    LED_IND_ERROR,
    LED_IND_WARN,
    LED_IND_GOOD,
    LED_IND_ALERT,
    LED_IND_BTR_FULL,
    LED_IND_BTR_GOOD,
    LED_IND_BTR_LOW,
    LED_IND_IDLE,
    LED_IND_LAST
} MKB_LED_IND_t;

extern void mkb_led_init(void);
extern void mkb_led_ind_enable(void);
extern void mkb_led_ind_disable(void);
extern void mkb_led_ind_set(MKB_LED_IND_t ind);
extern void mkb_led_indication(MKB_LED_NUM_t numb, MKB_LED_CLR_t color, MKB_LED_ACT_t status);
extern void mkb_led_indication_debug(uint8_t numb, uint8_t red, uint8_t green, uint8_t blue);
extern void mkb_led_change_main_color(MKB_LED_NUM_t numb, MKB_LED_CLR_t color);
extern ret_code_t mkb_led_cli(size_t size, char **params);
extern bool mkb_led_event_handler(mkb_event_t *p_event);

#endif /* __MKB_LED__ */
