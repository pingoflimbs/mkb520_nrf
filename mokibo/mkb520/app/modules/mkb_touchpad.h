/*
* ===============================================================================
* Copyright (C) 2017-2019
* INNOPRESSO INC. All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
* ===============================================================================
*/

#ifndef __MKB_TOUCHPAD_H__
#define __MKB_TOUCHPAD_H__

#include "mkb_config.h"

typedef enum
{
	TOUCHPAD_TAP_ONE,
	TOUCHPAD_TAP_TWO,
	TOUCHPAD_TAP_THREE,
	TOUCHPAD_TAP_FOUR,
	TOUCHPAD_TAP_MAX
} touchpad_tap_t;

typedef struct
{ // 좌표
	int16_t x;
	int16_t y;
} touch_coordinate_t;

//----------------------------------------------------//cypress_info_t
typedef struct
{
	uint8_t touchID;   // adjusted touch id
	uint8_t tip;	   // touch is currently on the panel
	uint16_t pressure; // 16bit z-coordinates in pixels
	//uint16_t	width;			  // 16bit x-coordinates in pixels
	//uint16_t	height;			  // 16bit y-coordinates in pixels
	uint16_t x;		   // 16bit x-coordinates in pixels
	uint16_t y;		   // 16bit y-coordinates in pixels
} touch_finger_info_t; // total size: 8 bytes

#define TOUCH_INFO_MAX_COUNT 5

typedef struct
{
	uint8_t touchCnt;								   // total number of current touches
	uint8_t button;									   // button state
	uint16_t timestamp;								   // unit: 0.1ms
	touch_finger_info_t fingers[TOUCH_INFO_MAX_COUNT]; // [5]
} touch_info_t;										   // total size: 44 byets

extern ret_code_t mkb_touchpad_init(void);
extern ret_code_t mkb_touchpad_start(void);
extern ret_code_t mkb_touchpad_stop(void);
extern bool mkb_touchpad_update_check(void);
extern void mkb_touchpad_update(void);

#endif /* __MKB_TOUCHPAD_H__ */
