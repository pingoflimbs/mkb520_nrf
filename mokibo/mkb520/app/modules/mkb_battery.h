/*
 * ===============================================================================
 * Copyright (C) 2017-2019
 * INNOPRESSO INC. All Rights Reserved
 *
 * The content of this file is copyrighted by INNOPRESSO or its
 * affiliated, associated or related companies. All copyrights and other
 * associated intellectual property rights related thereto are expressly
 * reserved by INNOPRESSO INC.
 * ===============================================================================
 */

#ifndef __MKB_BATTERY__
#define __MKB_BATTERY__

#include "mkb_config.h"

#define BATTERY_SENSE_TYPE_SIM 0
#define BATTERY_SENSE_TYPE_ADC 1
#define BATTERY_SENSE_TYPE_STM8 2

#define BATTERY_LEVEL_MEAS_INTERVAL APP_TIMER_TICKS(MKB_BATTERY_SAMPLING_TIME) /**< Battery level measurement interval (ticks). */

#define BATTERY_LEVEL_MIN 81      /**< Minimum simulated battery level. */
#define BATTERY_LEVEL_MAX 100     /**< Maximum simulated battery level. */
#define BATTERY_LEVEL_INCREMENT 1 /**< Increment between each simulated battery level measurement. */

#define SAMPLES_IN_BUFFER 1
#define ADC_REF_VOLTAGE_IN_MILLIVOLTS 600 /**< Reference voltage (in milli volts) used by ADC while doing conversion. */
#define ADC_PRE_SCALING_COMPENSATION 6    /**< The ADC is configured to use VDD with 1/3 prescaling as input. And hence the result of conversion is to be multiplied by 3 to get the actual value of the battery voltage.*/
#define ADC_RES_10BIT 1024                /**< Maximum digital value for 10-bit ADC conversion. */

#define DIODE_FWD_VOLT_DROP_MILLIVOLTS 270 /**< Typical forward voltage drop of the diode . */

/**brief Digital output
 *
 * The digital output value from the SAADC is calculated using a formula.
 *
 * RESULT = (V(P) – V(N)) * (GAIN/REFERENCE) * 2(RESOLUTION - m
 *
 * Ref: Measuring Lithium battery voltage with nRF52
 * => https://devzone.nordicsemi.com/nordic/nordic-blog/b/blog/posts/measuring-lithium-battery-voltage-with-nrf52
 *
 * If we assume that the voltage range of the Lithium battery is 2.7 V - 4.2 V, i.e. 2.7 V when empty and
 * 4.2 V when fully charged, then the voltage range on the ADC AIN input pin is:
 * Maximum voltage: 4.2 V * (2 M/(0.8 M+2 M)) = 3 V
 * Minimum voltage: 2.7 V * (2 M/(0.8 M+2 M)) = 1.93 V
 * ADC value at 4.2 V – 10 bit setup: 3 V * (1/6) / 0.6 V * 1023 = 853
 * ADC value at 2.7 V - 10 bit setup: 1.93 V * (1/6) / 0.6 V * 1023 = 548
 * Usable ADC resolution - 10 bit setup: 853 - 548 = 305
 *
 */

/**@brief Macro to convert the result of ADC conversion in millivolts.
 *
 * @param[in]  ADC_VALUE   ADC result.
 *
 * @retval     Result converted to millivolts.
 */
#define ADC_RESULT_IN_MILLI_VOLTS(ADC_VALUE) \
        ((((ADC_VALUE)*ADC_REF_VOLTAGE_IN_MILLIVOLTS) / ADC_RES_10BIT) * ADC_PRE_SCALING_COMPENSATION)

extern void mkb_battery_init(void);
extern void mkb_battery_start(void);
extern uint8_t mkb_battery_level_get(void);

#endif /* __MKB_BATTERY__ */
