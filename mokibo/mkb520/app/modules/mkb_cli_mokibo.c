/*
* ===============================================================================
* Copyright (C) 2017-2019
* INNOPRESSO INC. All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
* ===============================================================================
*/

#include <stdbool.h>
#include <stdint.h>

#include "sdk_common.h"

#include "nrf_assert.h"
#include "nrf_error.h"
#include "nrf_log.h"
#include "nrf_delay.h"
#include "nrf_cli.h"

#include "fds.h"
#include "mkb_ble_adv.h" //테스트해보고 지울거
#include "mkb_db.h"      //테스트해보고 지울것ㅣ

#define MKB_LOG_LEVEL MKB_CLI_LOG_LEVEL
#include "mkb_common.h"

#if (defined(MKB_CLI_ENABLED) && MKB_CLI_ENABLED)
#include "mkb_cli.h"

#if (defined(MKB_SYSTEM_ENABLED) && MKB_SYSTEM_ENABLED)
#include "mkb_system.h"
#endif //(defined(MKB_SYSTEM_ENABLED) && MKB_SYSTEM_ENABLED)

#define REBOOT_HELP "Reboot system by software reset\n" \
                    "usage: reboot"

#define MOKIBO_HELP "MOKIBO CLI command lists print\n" \
                    "usage: mokibo"

#define PIXART_HELP "MOKIBO CLI command lists print\n" \
                    "usage: mokibo"

#if (defined(MKB_SYSTEM_ENABLED) && MKB_SYSTEM_ENABLED)
static void cmd_mokibo_reboot(nrf_cli_t const *p_cli, size_t argc, char **argv)
{
    UNUSED_PARAMETER(argc);
    UNUSED_PARAMETER(argv);

    if (nrf_cli_help_requested(p_cli))
    {
        nrf_cli_help_print(p_cli, NULL, 0);
        return;
    }

    mkb_system_reset();
}
NRF_CLI_CMD_REGISTER(reboot, NULL, REBOOT_HELP, cmd_mokibo_reboot);
#endif //(defined(MKB_SYSTEM_ENABLED) && MKB_SYSTEM_ENABLED)

static void cmd_mokibo(nrf_cli_t const *p_cli, size_t argc, char **argv)
{
    UNUSED_PARAMETER(argc);
    UNUSED_PARAMETER(argv);

    if (nrf_cli_help_requested(p_cli))
    {
        nrf_cli_help_print(p_cli, NULL, 0);
        return;
    }

    nrf_cli_fprintf(p_cli, NRF_CLI_DEFAULT, "MOKIBO CLI command lists\n");

#if (defined(MKB_CYPRESS_ENABLED) && MKB_CYPRESS_ENABLED)
    nrf_cli_fprintf(p_cli, NRF_CLI_DEFAULT, "- cypress\n");
#endif //(defined(MKB_CYPRESS_ENABLED) && MKB_CYPRESS_ENABLED)

#if (defined(MKB_DB_ENABLED) && MKB_DB_ENABLED)
    nrf_cli_fprintf(p_cli, NRF_CLI_DEFAULT, "- db\n");
#endif //(defined(MKB_DB_ENABLED) && MKB_DB_ENABLED)

#if (defined(MKB_FDS_ENABLED) && MKB_FDS_ENABLED)
    nrf_cli_fprintf(p_cli, NRF_CLI_DEFAULT, "- fds\n");
#endif //(defined(MKB_FDS_ENABLED) && MKB_FDS_ENABLED)

#if (defined(MKB_SYSTEM_ENABLED) && MKB_SYSTEM_ENABLED)
    nrf_cli_fprintf(p_cli, NRF_CLI_DEFAULT, "- reboot\n");
#endif //(defined(MKB_SYSTEM_ENABLED) && MKB_SYSTEM_ENABLED)
}

NRF_CLI_CMD_REGISTER(mokibo, NULL, MOKIBO_HELP, cmd_mokibo);
#endif // (defined(MKB_CLI_ENABLED) && MKB_CLI_ENABLED)
