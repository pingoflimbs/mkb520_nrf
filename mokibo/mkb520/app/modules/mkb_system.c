/*
 * ===============================================================================
 * Copyright (C) 2017-2019
 * INNOPRESSO INC. All Rights Reserved
 *
 * The content of this file is copyrighted by INNOPRESSO or its
 * affiliated, associated or related companies. All copyrights and other
 * associated intellectual property rights related thereto are expressly
 * reserved by INNOPRESSO INC.
 * ===============================================================================
 */

#include <stdbool.h>
#include <stdint.h>
#include <stdarg.h>

#include "sdk_common.h"

#include "nrf_assert.h"
#include "nrf_error.h"
#include "nrf_delay.h"
#include "nrf_cli.h"
#include "nrf_balloc.h"
#include "nrf_pwr_mgmt.h"

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

#include "app_error.h"
#include "app_timer.h"
#include "app_scheduler.h"

#define MKB_LOG_LEVEL MKB_SYSTEM_LOG_LEVEL
#include "mkb_common.h"
#include "mkb_system.h"

#if (defined(MKB_EVENT_ENABLED) && MKB_EVENT_ENABLED)
#include "mkb_event.h"
#else
#error "You have to enable event at mkb_config.h!"
#endif // (defined(MKB_EVENT_ENABLED) && MKB_EVENT_ENABLED)

#if (defined(MKB_KEYBOARD_ENABLED) && MKB_KEYBOARD_ENABLED)
#include "mkb_keyboard.h"
#endif // (defined(MKB_KEYBOARD_ENABLED) && MKB_KEYBOARD_ENABLED)

#if (defined(MKB_TOUCHPAD_ENABLED) && MKB_TOUCHPAD_ENABLED)
#include "mkb_touchpad.h"
#endif // (defined(MKB_TOUCHPAD_ENABLED) && MKB_TOUCHPAD_ENABLED)

#if (defined(MKB_COMS_ENABLED) && MKB_COMS_ENABLED)
#include "mkb_coms.h"
#endif // (defined(MKB_COMS_ENABLED) && MKB_COMS_ENABLED)

#include "mkb_usb.h"

/**@brief mokibo usb initialize */
bool mkb_system_event_handler(mkb_event_t *p_event)
{
    ASSERT(p_event != NULL);
    switch (p_event->type)
    {
#if (defined(MKB_BATTERY_ENABLED) && MKB_BATTERY_ENABLED)
    case EVT_SYSTEM_BATTERY_LEVEL:
        ________DBG_20210903_event_process_test("mkb_ble_bas_battery_level_update(p_event->system.data);",
                                                p_event->system.data);
        mkb_ble_bas_battery_level_update(p_event->system.data);
        // if connected

        break;
#endif // (defined(MKB_BATTERY_ENABLED) && MKB_BATTERY_ENABLED)
    case EVT_SYSTEM_SLEEP:
        MKB_LOG_INFO("System Shutdown Request!, but not shutdown");
        nrf_pwr_mgmt_shutdown(NRF_PWR_MGMT_SHUTDOWN_GOTO_SYSOFF);
        break;

    case EVT_FUNC_DOWN:
        ________DBG_20210930_fnbtn("func down");
        break;
    case EVT_FUNC_UP:
        ________DBG_20210930_fnbtn("func up");
        break;
    case EVT_FUNC_HOLD:
        ________DBG_20210930_fnbtn("func hold");
#if (defined(MKB_KEYBOARD_ENABLED) && MKB_KEYBOARD_ENABLED)
        if (p_event->key.id == MKB_KEYBOARD_HOTKEY_ONOFF)
        {
            MKB_LOG_INFO("Hot Key is ONOFF, System Shutdown Request!");
            nrf_pwr_mgmt_shutdown(NRF_PWR_MGMT_SHUTDOWN_GOTO_SYSOFF);
        }
#endif // (defined(MKB_KEYBOARD_ENABLED) && MKB_KEYBOARD_ENABLED)

        break;

#if (defined(MKB_TOUCHPAD_ENABLED) && MKB_TOUCHPAD_ENABLED)
#if (defined(MKB_TOUCHPAD_UPDATE_ENABLED) && MKB_TOUCHPAD_UPDATE_ENABLED)
    case EVT_SYSTEM_TOUCH_UPDATE:
        mkb_touchpad_update();

#if (defined(MKB_COMS_ENABLED) && MKB_COMS_ENABLED)
        mkb_coms_start();
#endif // (defined(MKB_COMS_ENABLED) && MKB_COMS_ENABLED)

        break;
#endif // (defined(MKB_TOUCHPAD_UPDATE_ENABLED) && MKB_TOUCHPAD_UPDATE_ENABLED)
#endif // (defined(MKB_TOUCHPAD_ENABLED) && MKB_TOUCHPAD_ENABLED)
    }

    return false;
}

void mkb_system_reset(void)
{
    MKB_LOG_INFO("System Reset Request!");
    nrf_pwr_mgmt_shutdown(NRF_PWR_MGMT_SHUTDOWN_RESET);
}