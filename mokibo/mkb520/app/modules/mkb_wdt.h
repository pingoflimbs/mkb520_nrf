/*
* ===============================================================================
* Copyright (C) 2017-2019
* INNOPRESSO INC. All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
* ===============================================================================
*/

#ifndef __MKB_WDT__
#define __MKB_WDT__

#include "mkb_common.h"

extern void mkb_wdt_init(void);
extern void mkb_wdt_enable(void);
extern void mkb_wdt_feed(void);
extern void mkb_wdt_time_set(uint32_t time);

#endif /* __MKB_WDT__ */
