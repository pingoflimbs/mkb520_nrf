/*
 * ===============================================================================
 * Copyright (C) 2017-2019
 * INNOPRESSO INC. All Rights Reserved
 *
 * The content of this file is copyrighted by INNOPRESSO or its
 * affiliated, associated or related companies. All copyrights and other
 * associated intellectual property rights related thereto are expressly
 * reserved by INNOPRESSO INC.
 * ===============================================================================
 */

#ifndef __MKB_EVENT__
#define __MKB_EVENT__

#include <stdint.h>
#include <stdbool.h>
#include "sdk_errors.h"

#ifdef __cplusplus
extern "C"
{
#endif

#pragma pack(push, 1)

/**@brief Translate event type to event group. */
#define EVENT_GROUP(_event_type) (mkb_event_group_t)((unsigned long)(_event_type) >> 8)

    /**@brief Event groups. */
    typedef enum
    {
        EVT_GROUP_SYSTEM,  /**< System state events. */
        EVT_GROUP_FUNC,    /**< Keyboard function events. */
        EVT_GROUP_KEY,     /**< Keyboard input events. */
        EVT_GROUP_BUTTON,  /**< Button input events. */
        EVT_GROUP_TOUCH,   /**< Touchpad motion events. */
        EVT_GROUP_TRACPAD, /**< Tracpad motion events. */
        EVT_GROUP_HID,     /**< HID report events. */
        EVT_GROUP_LED,     /**< LED events. */
    } mkb_event_group_t;

    /**@brief Event types. */
    typedef enum
    {
        /* System Events */
        EVT_SYSTEM_BATTERY_LEVEL = (EVT_GROUP_SYSTEM << 8), // 0
        EVT_SYSTEM_SLEEP,
        EVT_SYSTEM_TOUCH_UPDATE,

        /* Keyboard function events */
        EVT_FUNC_UP = (EVT_GROUP_FUNC << 8), // 1 00 00 00 00
        EVT_FUNC_DOWN,
        EVT_FUNC_HOLD,

        /* Keyboard input events */
        EVT_KEY_UP = (EVT_GROUP_KEY << 8), // 2 0000 0000
        EVT_KEY_DOWN,
        EVT_KEY_HOLD,
        EVT_CONSUMER_DOWN,
        EVT_CONSUMER_UP,

        /* Button input events */
        EVT_BUTTON_CLICK_LEFT = (EVT_GROUP_BUTTON << 8), // 3 0000 0000
        EVT_BUTTON_CLICK_RIGHT,
        EVT_BUTTON_TOUCH_LEFT,
        EVT_BUTTON_TOUCH_RIGHT,

        /* Touchpad motion events */
        EVT_TOUCH_MOVE = (EVT_GROUP_TOUCH << 8), // 4 0000 0000
        EVT_TOUCH_SCROLL,
        EVT_TOUCH_TAB,

        /* Tracpad motion events */
        EVT_TRACPAD_MOVE = (EVT_GROUP_TRACPAD << 8), // 5 0000 0000

        /* HID report events */
        EVT_HID_INPUT_KEYBOARD = (EVT_GROUP_HID << 8), // 6 0000 0000
        EVT_HID_INPUT_BUTTON,
        EVT_HID_INPUT_CONSUMER,

        /* LED events */
        EVT_LED_CH_IDLE = (EVT_GROUP_LED << 8), // 7 0000 0000, //
        EVT_LED_CH_NO_ADDR,
        EVT_LED_CH_ADV_FAST,
        EVT_LED_CH_ADV_SLOW,
        EVT_LED_CH_CONNECT, //충전 따라 동작 다름
        EVT_LED_CH_DISCONNECT,
        EVT_LED_PWR_IDLE,
        EVT_LED_PWR_CHARGE,
        EVT_LED_PWR_DISCHARGE,
        EVT_LED_PWR_ALERT,
        EVT_LED_SUB_SET_CH_OS_CHECK_PUSH_DOWN,
        EVT_LED_SUB_SET_CH_OS_CHECK_PUSH_UP,
        EVT_LED_SUB_BATT_CHECK,
        EVT_LED_COLOR_CH_OS_SET,
        EVT_LED_COLOR_PWR_REMAIN_SET,
        EVT_LED_CUSTOM_SET,
    } mkb_event_type_t;

#define EVENT_HID_PACKET_MAX 8
#define EVENT_TRACPAD_PACKET_MAX 40 // from mkb_touchpad.h touch_info_t

    typedef struct
    {
        mkb_event_type_t type;
        uint32_t delayed_ms;
        bool is_waiting;
        union
        {
            // Valid for EVT_GROUP_SYSTEM.
            struct
            {
                uint8_t data;
            } system;

            // Valid for EVT_GROUP_KEY. EVT_GROUP_FUNC
            struct
            {
                uint8_t id;
                uint32_t down_timestamp;
            } key;

            // Valid for EVT_GROUP_BUTTON.
            struct
            {
                bool state; // 0: down, 1: up
            } button;

            // Valid for EVT_GROUP_TOUCH
            struct
            {
                int16_t x;
                int16_t y;
                uint8_t tab;
                uint8_t finger; // 0:none, 1:one, 2:two, 3:three, 4:four
                uint8_t dir;    // 0:up, 1:down, 2:left, 3:right
            } touch;

            // Valid for EVT_GROUP_HID,
            struct
            {
                uint8_t data[EVENT_HID_PACKET_MAX];
                uint8_t len;
            } hid;

            // Valid for EVT_GROUP_TRACPAD.
            struct
            {
                uint8_t data[EVENT_TRACPAD_PACKET_MAX];
                uint8_t len;
            } tracpad;

            // Valid for EVT_GROUP_LED
            // EVT_LED_CH : event_type 만 사용 ex ) mkb_event_send(EVT_LED_CH_ADV_SLOW, 0, 0);
            // EVT_LED_PWR :
            // EVT_LED_SUB
            // EVT_LED_COLOR
            struct
            {
                uint8_t act;
                uint8_t color;
                uint8_t numb;
            } led;
        };
    } mkb_event_t;

#define MKB_EVENT_SIZE sizeof(mkb_event_t)

#define HID_USAGE(_page, _id) (((unsigned long)((_page)&0xFFFF) << 16) | ((_id)&0xFFFF))
#define HID_USAGE_ID(_usage) (((_usage) >> 0) & 0xFFFF)
#define HID_USAGE_PAGE(_usage) (((_usage) >> 16) & 0xFFFF)

    typedef bool (*mkb_event_handler_t)(mkb_event_t *p_event);

    extern void mkb_event_init(void);
    extern void mkb_event_start(void);
    extern ret_code_t mkb_event_send(mkb_event_type_t event_type, ...);
    extern ret_code_t mkb_event_send_ms(uint16_t ms, mkb_event_type_t event_type, ...);
    extern ret_code_t mkb_event_cli(size_t size, char **params);

#pragma pack(pop)

#ifdef __cplusplus
}
#endif

#endif /* __MKB_EVENT__ */
