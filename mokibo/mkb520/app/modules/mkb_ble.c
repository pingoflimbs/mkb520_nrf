/*
 * ===============================================================================
 * Copyright (C) 2017-2019
 * INNOPRESSO INC. All Rights Reserved
 *
 * The content of this file is copyrighted by INNOPRESSO or its
 * affiliated, associated or related companies. All copyrights and other
 * associated intellectual property rights related thereto are expressly
 * reserved by INNOPRESSO INC.
 * ===============================================================================
 */

#include <stdbool.h>
#include <stdint.h>
#include <stdarg.h>

#include "sdk_common.h"

#include "ble.h"
#include "ble_err.h"
#include "ble_gap.h"
#include "ble_advdata.h"
#include "ble_hids.h"
#include "ble_dis.h"
#include "ble_bas.h"
#include "ble_conn_params.h"
#include "ble_conn_state.h"
#include "ble_advertising.h"

#include "nrf_assert.h"
#include "nrf_error.h"
#include "nrf_delay.h"
#include "nrf_cli.h"
#include "nrf_balloc.h"
#include "nrf_ble_gatt.h"
#include "nrf_pwr_mgmt.h"
#include "nrf_sdh.h"
#include "nrf_sdh_soc.h"
#include "nrf_sdh_ble.h"

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

#include "peer_manager.h"
#include "peer_manager_handler.h"
#include "bsp.h"

#include "app_error.h"
#include "app_timer.h"
#include "app_scheduler.h"

#define MKB_LOG_LEVEL MKB_BLE_LOG_LEVEL
#include "mkb_common.h"

#if (defined(MKB_BLE_ENABLED) && MKB_BLE_ENABLED)
#include "mkb_ble.h"
#include "mkb_ble_adv.h"
#include "mkb_ble_pm.h"

#if (defined(MKB_DB_ENABLED) && MKB_DB_ENABLED)
#include "mkb_db.h"
#else
#error "You have to enable db at mkb_config.h!"
#endif // (defined(MKB_DB_ENABLED) && MKB_DB_ENABLED)

#if (defined(MKB_LED_ENABLED) && MKB_LED_ENABLED)
#include "mkb_led.h"
#endif // (defined(MKB_LED_ENABLED) && MKB_LED_ENABLED)

#if (defined(MKB_KEYBOARD_ENABLED) && MKB_KEYBOARD_ENABLED)
#include "mkb_keyboard.h"
#endif // (defined(MKB_KEYBOARD_ENABLED) && MKB_KEYBOARD_ENABLED)

#if (defined(MKB_COMS_ENABLED) && MKB_COMS_ENABLED)
#include "mkb_coms.h"
#endif // (defined(MKB_COMS_ENABLED) && MKB_COMS_ENABLED)

#if (defined(MKB_EVENT_ENABLED) && MKB_EVENT_ENABLED)
#include "mkb_event.h"
#else
#error "You have to enable event at mkb_config.h!"
#endif // (defined(MKB_EVENT_ENABLED) && MKB_EVENT_ENABLED)

/**Buffer queue access macros
 *
 * @{ */
/** Initialization of buffer list */
#define BUFFER_LIST_INIT()     \
    do                         \
    {                          \
        buffer_list.rp = 0;    \
        buffer_list.wp = 0;    \
        buffer_list.count = 0; \
    } while (0)

/** Provide status of data list is full or not */
#define BUFFER_LIST_FULL() \
    ((MAX_BUFFER_ENTRIES == buffer_list.count - 1) ? true : false)

/** Provides status of buffer list is empty or not */
#define BUFFER_LIST_EMPTY() \
    ((0 == buffer_list.count) ? true : false)

#define BUFFER_ELEMENT_INIT(i)                 \
    do                                         \
    {                                          \
        buffer_list.buffer[(i)].p_data = NULL; \
    } while (0)

/** @} */

/** Abstracts buffer element */
typedef struct hid_key_buffer
{
    uint8_t data_offset;    /**< Max Data that can be buffered for all entries */
    uint8_t data_len;       /**< Total length of data */
    uint8_t *p_data;        /**< Scanned key pattern */
    ble_hids_t *p_instance; /**< Identifies peer and service instance */
} buffer_entry_t;

STATIC_ASSERT(sizeof(buffer_entry_t) % 4 == 0);

/** Circular buffer list */
typedef struct
{
    buffer_entry_t buffer[MAX_BUFFER_ENTRIES]; /**< Maximum number of entries that can enqueued in the list */
    uint8_t rp;                                /**< Index to the read location */
    uint8_t wp;                                /**< Index to write location */
    uint8_t count;                             /**< Number of elements in the list */
} buffer_list_t;

STATIC_ASSERT(sizeof(buffer_list_t) % 4 == 0);

static buffer_list_t buffer_list; /**< List to enqueue not just data to be sent, but also related information like the handle, connection handle etc */

BLE_BAS_DEF(m_bas);  /**< Battery service instance. */
BLE_HIDS_DEF(m_hids, /**< HID service instance. */
             NRF_SDH_BLE_TOTAL_LINK_COUNT,
#if FEATURE_REPORT_COUNT
             FEATURE_REP_MAX_LEN,
#endif
             OUTPUT_REP_MAX_LEN,
             INPUT_REP_KEYS_MAX_LEN,
#if MKB_BLE_TOUCH_DEVICE_ENABLED
             INPUT_REP_TOUCH_LEN,
#endif
             INPUT_REP_BUTTONS_LEN,
             INPUT_REP_MOVEMENT_LEN,
             INPUT_REP_CONSUMER_LEN);
NRF_BLE_GATT_DEF(m_gatt); /**< GATT module instance. */

static const uint8_t *m_ble_device_name = {MKB_DEVICE_NAME};
static uint16_t m_conn_handle = BLE_CONN_HANDLE_INVALID; /**< Handle of the current connection. */
static bool m_in_boot_mode = false;                      /**< Current protocol mode. */
static bool m_ble_adv_stop_req = false;

ret_code_t mkb_ble_keys_send(uint8_t *p_key_pattern, uint8_t key_pattern_len);

/**@brief Function for enqueuing key scan patterns that could not be transmitted either completely
 *        or partially.
 *
 * @warning This handler is an example only. You need to analyze how you wish to send the key
 *          release.
 *
 * @param[in]  p_hids         Identifies the service for which Key Notifications are buffered.
 * @param[in]  p_key_pattern  Pointer to key pattern.
 * @param[in]  pattern_len    Length of key pattern.
 * @param[in]  offset         Offset applied to Key Pattern when requesting a transmission on
 *                            dequeue, @ref buffer_dequeue.
 * @return     NRF_SUCCESS on success, else an error code indicating reason for failure.
 */
static uint32_t buffer_enqueue(ble_hids_t *p_hids,
                               uint8_t *p_key_pattern,
                               uint16_t pattern_len,
                               uint16_t offset)
{
    buffer_entry_t *element;
    uint32_t err_code = NRF_SUCCESS;

    if (BUFFER_LIST_FULL())
    {
        // Element cannot be buffered.
        err_code = NRF_ERROR_NO_MEM;
    }
    else
    {
        // Make entry of buffer element and copy data.
        element = &buffer_list.buffer[(buffer_list.wp)];
        element->p_instance = p_hids;
        element->p_data = p_key_pattern;
        element->data_offset = offset;
        element->data_len = pattern_len;

        buffer_list.count++;
        buffer_list.wp++;

        if (buffer_list.wp == MAX_BUFFER_ENTRIES)
        {
            buffer_list.wp = 0;
        }
    }

    return err_code;
}

/**@brief   Function to dequeue key scan patterns that could not be transmitted either completely of
 *          partially.
 *
 * @warning This handler is an example only. You need to analyze how you wish to send the key
 *          release.
 *
 * @param[in]  tx_flag   Indicative of whether the dequeue should result in transmission or not.
 * @note       A typical example when all keys are dequeued with transmission is when link is
 *             disconnected.
 *
 * @return     NRF_SUCCESS on success, else an error code indicating reason for failure.
 */
static uint32_t buffer_dequeue(bool tx_flag)
{
    buffer_entry_t *p_element;
    uint32_t err_code = NRF_SUCCESS;
    uint16_t actual_len;

    if (BUFFER_LIST_EMPTY())
    {
        err_code = NRF_ERROR_NOT_FOUND;
    }
    else
    {
        bool remove_element = true;

        p_element = &buffer_list.buffer[(buffer_list.rp)];

        if (tx_flag)
        {
            err_code = mkb_ble_keys_send(p_element->p_data, p_element->data_len);

            // An additional notification is needed for release of all keys, therefore check
            // is for actual_len <= element->data_len and not actual_len < element->data_len
            if (err_code != NRF_SUCCESS)
            {
                // Transmission could not be completed, do not remove the entry, adjust next data to
                // be transmitted
                // p_element->data_offset = actual_len;
                remove_element = false;
            }
        }

        if (remove_element)
        {
            BUFFER_ELEMENT_INIT(buffer_list.rp);

            buffer_list.rp++;
            buffer_list.count--;

            if (buffer_list.rp == MAX_BUFFER_ENTRIES)
            {
                buffer_list.rp = 0;
            }
        }
    }

    return err_code;
}

/**@brief   Function for initializing the buffer queue used to key events that could not be
 *          transmitted
 *
 * @warning This handler is an example only. You need to analyze how you wish to buffer or buffer at
 *          all.
 *
 * @note    In case of HID keyboard, a temporary buffering could be employed to handle scenarios
 *          where encryption is not yet enabled or there was a momentary link loss or there were no
 *          Transmit buffers.
 */
static void buffer_init(void)
{
    uint32_t buffer_count;

    BUFFER_LIST_INIT();

    for (buffer_count = 0; buffer_count < MAX_BUFFER_ENTRIES; buffer_count++)
    {
        BUFFER_ELEMENT_INIT(buffer_count);
    }
}

/**@brief Function for handling BLE events.
 *
 * @param[in]   p_ble_evt   Bluetooth stack event.
 * @param[in]   p_context   Unused.
 */
static void ble_evt_handler(ble_evt_t const *p_ble_evt, void *p_context)
{
    ret_code_t err_code;
    pm_peer_id_t peer_id;
    MKB_DB_CH_t *p_ch = mkb_db_ch_get();
    CHANNEL_ID_t ch_id = mkb_db_ch_id_get();

    switch (p_ble_evt->header.evt_id)
    {
    case BLE_GAP_EVT_CONNECTED:
    {
        pm_peer_data_bonding_t peer_bonding_data;

        m_conn_handle = p_ble_evt->evt.gap_evt.conn_handle;

        p_ch->conn_handle[ch_id] = p_ble_evt->evt.gap_evt.conn_handle;
        MKB_LOG_INFO("BLE_GAP_EVT_CONNECTED: CH(%d), conn_handle= %d", ch_id, p_ble_evt->evt.gap_evt.conn_handle);
        ________DBG_20210930_ble_event("BLE_GAP_EVT_CONNECTED: CH(%d), conn_handle= %d", ch_id, p_ble_evt->evt.gap_evt.conn_handle);

#if !(defined(BOARD_MKB10040))
        uint8_t dev_name[MKB_DEVICE_NAME_SIZE];
        uint16_t dev_len;
        sd_ble_gap_device_name_get(&dev_name[0], &dev_len);
        MKB_LOG_INFO("DEV name=%s, len=%d", dev_name, dev_len);
#endif

#if (defined(MKB_LED_ENABLED) && MKB_LED_ENABLED)
        // LED indication will be changed.
        mkb_led_ind_set(p_ch->led[ch_id].connect);

#endif //(defined(MKB_LED_ENABLED) && MKB_LED_ENABLED)
    }
    break;

    case BLE_GAP_EVT_DISCONNECTED:
    {
        pm_peer_id_get(p_ble_evt->evt.gap_evt.conn_handle, &peer_id);

        MKB_LOG_INFO("BLE_GAP_EVT_DISCONNECTED: conn_handle= %d, reason= 0x%X, peer_id= %d",
                     p_ble_evt->evt.gap_evt.conn_handle, p_ble_evt->evt.gap_evt.params.disconnected.reason, peer_id);
        ________DBG_20210930_ble_event("BLE_GAP_EVT_DISCONNECTED: conn_handle= %d, reason= 0x%X, peer_id= %d",
                                       p_ble_evt->evt.gap_evt.conn_handle, p_ble_evt->evt.gap_evt.params.disconnected.reason, peer_id);

        ble_gap_addr_t ownaddr;
        if (mkb_ble_pm_local_address_get(&ownaddr) == NRF_SUCCESS)
        {
        }

#if (defined(MKB_LED_ENABLED) && MKB_LED_ENABLED)
        ________DBG_20210915_led_callstack("LED_STACK");
        mkb_led_ind_set(p_ch->led[ch_id].disconnect);
        mkb_event_send(EVT_LED_CH_DISCONNECT);
#endif //(defined(MKB_LED_ENABLED) && MKB_LED_ENABLED)

        // Dequeue all keys without transmission.
        (void)buffer_dequeue(false);

        // find channel to be deleted
        for (uint8_t i = 0; i < CHANNEL_ID_MAX; i++)
        {
            if (p_ch->conn_handle[i] == p_ble_evt->evt.gap_evt.conn_handle &&
                p_ch->config.info[i].peer_id == peer_id &&
                !memcmp(&p_ch->config.info[i].own_addr, &ownaddr, BLE_GAP_ADDR_LEN))
            {
                MKB_LOG_INFO("CH(%d): disconnected by conn_handle= %d", i, p_ble_evt->evt.gap_evt.conn_handle);
                // MKB_LOG_INFO("OWN ADDR: 0x%02x%02x%02x%02x%02x%02x",
                //         ownaddr.addr[5], ownaddr.addr[4], ownaddr.addr[3],
                //         ownaddr.addr[2], ownaddr.addr[1], ownaddr.addr[0]);
                p_ch->conn_handle[i] = BLE_CONN_HANDLE_INVALID;
                break;
            }
        }

#if (defined(MKB_KEYBOARD_ENABLED) && MKB_KEYBOARD_ENABLED)
        // Reset m_caps_on variable. Upon reconnect, the HID host will re-send the Output
        // report containing the Caps lock state.
        keyboard_caps_lock_set(false);
#endif // (defined(MKB_KEYBOARD_ENABLED) && MKB_KEYBOARD_ENABLED)

        if (mkb_db_wait_for_disconnect_get())
        {
            mkb_db_wait_for_disconnect_set(false);

            if (mkb_ble_adv_wait_for_advertizing_get() == ADV_MODE_DIRECTED)
            {
                MKB_LOG_INFO("CH(%d): start advertize with directed mode", ch_id);
                mkb_ble_adv_start(ch_id, ADV_MODE_DIRECTED);
            }
            else
            {
                MKB_LOG_INFO("CH(%d): start advertize with undirected mode", ch_id);
                mkb_ble_adv_start(ch_id, ADV_MODE_UNDIRECTED);
            }
        }
        else
        {
            if (m_ble_adv_stop_req)
            {
                m_ble_adv_stop_req = false;
                mkb_ble_adv_stop();
            }
        }
    }
    break;

    case BLE_GAP_EVT_PHY_UPDATE_REQUEST:
    {
        MKB_LOG_INFO("PHY update request.");
        ________DBG_20210930_ble_event("BLE_GAP_EVT_PHY_UPDATE_REQUEST");
        ble_gap_phys_t const phys =
            {
                .rx_phys = BLE_GAP_PHY_AUTO,
                .tx_phys = BLE_GAP_PHY_AUTO,
            };
        err_code = sd_ble_gap_phy_update(p_ble_evt->evt.gap_evt.conn_handle, &phys);
        MKB_ERROR_CHECK(err_code);
    }
    break;

    case BLE_GATTS_EVT_HVN_TX_COMPLETE:
        MKB_LOG_INFO("BLE_GATTS_EVT_HVN_TX_COMPLETE");
        ________DBG_20210930_ble_event("BLE_GATTS_EVT_HVN_TX_COMPLETE");
        // Send next key event
        (void)buffer_dequeue(true);
        break;

    case BLE_GATTC_EVT_TIMEOUT:
        // Disconnect on GATT Client timeout event.
        MKB_LOG_INFO("GATT Client Timeout.");
        ________DBG_20210930_ble_event("BLE_GATTC_EVT_TIMEOUT");
        err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gattc_evt.conn_handle,
                                         BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
        MKB_ERROR_CHECK(err_code);
        break;

    case BLE_GATTS_EVT_TIMEOUT:
        // Disconnect on GATT Server timeout event.
        MKB_LOG_INFO("GATT Server Timeout.");
        ________DBG_20210930_ble_event("BLE_GATTS_EVT_TIMEOUT");
        err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gatts_evt.conn_handle,
                                         BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
        MKB_ERROR_CHECK(err_code);
        break;

    case BLE_GATTS_EVT_RW_AUTHORIZE_REQUEST:
        MKB_LOG_INFO("BLE_GATTS_EVT_RW_AUTHORIZE_REQUEST");
        ________DBG_20210930_ble_event("BLE_GATTS_EVT_RW_AUTHORIZE_REQUEST");
        break;

    case BLE_GATTS_EVT_WRITE:
        MKB_LOG_INFO("BLE_GATTS_EVT_WRITE");
        ________DBG_20210930_ble_event("BLE_GATTS_EVT_WRITE");
        break;

    case BLE_GAP_EVT_AUTH_KEY_REQUEST:
        // Peripheral Legacy Pairing: Bonding: Passkey Entry, User Inputs on Peripheral
        // https://infocenter.nordicsemi.com/topic/com.nordic.infocenter.s140.api.v6.1.1/group___b_l_e___g_a_p___p_e_r_i_p_h___b_o_n_d_i_n_g___p_k___c_e_n_t_r_a_l___o_o_b___m_s_c.html
        MKB_LOG_INFO("BLE_GAP_EVT_AUTH_KEY_REQUEST: key_type(%d)", p_ble_evt->evt.gap_evt.params.auth_key_request.key_type);
        ________DBG_20210930_ble_event("BLE_GAP_EVT_AUTH_KEY_REQUEST: key_type(%d)", p_ble_evt->evt.gap_evt.params.auth_key_request.key_type);

#if (defined(MKB_PM_BOND_TYPE) && (MKB_PM_BOND_TYPE == PM_BOND_PASSKEY))
        // if(p_ble_evt->evt.gap_evt.params.auth_key_request.key_type)
        mkb_coms_passkey_enable();
#endif
        break;

    case BLE_GAP_EVT_AUTH_STATUS:
        // BLE_GAP_SEC_STATUS: ble_gap.h: 488
        // #define BLE_GAP_SEC_STATUS_SUCCESS                0x00  /**< Procedure completed with success. */
        // #define BLE_GAP_SEC_STATUS_TIMEOUT                0x01  /**< Procedure timed out. */
        // #define BLE_GAP_SEC_STATUS_PASSKEY_ENTRY_FAILED   0x81  /**< Passkey entry failed (user canceled or other). */
        //
        ________DBG_20210930_ble_event("BLE_GAP_EVT_AUTH_STATUS");

        if (p_ble_evt->evt.gap_evt.params.auth_status.auth_status == BLE_GAP_SEC_STATUS_SUCCESS)
        {
            MKB_LOG_INFO("BLE_GAP_EVT_AUTH_STATUS OK!");

#if (defined(MKB_PM_BOND_TYPE) && (MKB_PM_BOND_TYPE == PM_BOND_PASSKEY))
            mkb_coms_passkey_disable();
#endif
        }
        else
        {
            MKB_LOG_INFO("BLE_GAP_EVT_AUTH_STATUS Fail! status: 0x%02x", p_ble_evt->evt.gap_evt.params.auth_status.auth_status);

            if (p_ble_evt->evt.gap_evt.params.auth_status.auth_status != BLE_GAP_SEC_STATUS_PASSKEY_ENTRY_FAILED)
            {
#if (defined(MKB_PM_BOND_TYPE) && (MKB_PM_BOND_TYPE == PM_BOND_PASSKEY))
                mkb_coms_passkey_disable();
#endif

                if (p_ble_evt->evt.gap_evt.params.auth_status.auth_status != BLE_GAP_SEC_STATUS_TIMEOUT)
                {
                    if (p_ch->config.info[ch_id].peer_id != PM_PEER_ID_INVALID)
                    {
                        MKB_LOG_INFO("CH(%d), delete pm_peer", ch_id);
                        mkb_ble_pm_delete_peer(ch_id);
                    }
                    else
                    {
                        if (mkb_ble_conn_state(ch_id))
                        {
                            MKB_LOG_INFO("CH(%d), disconnect request", ch_id);
                            mkb_ble_disconnect(ch_id);
                        }
                    }
                }
            }
        }

        break;

    case BLE_GAP_EVT_CONN_PARAM_UPDATE:
    {

        pm_peer_data_bonding_t peer_bonding_data;

        pm_peer_id_get(p_ble_evt->evt.gap_evt.conn_handle, &peer_id);

        MKB_LOG_INFO("BLE_GAP_EVT_CONN_PARAM_UPDATE: CH(%d), conn_handle= %d, peer_id= %d", ch_id, p_ble_evt->evt.gap_evt.conn_handle, peer_id);
        ________DBG_20210930_ble_event("BLE_GAP_EVT_CONN_PARAM_UPDATE: CH(%d), conn_handle= %d, peer_id= %d", ch_id, p_ble_evt->evt.gap_evt.conn_handle, peer_id);

        if (p_ch->config.info[ch_id].peer_id != peer_id && peer_id != PM_PEER_ID_INVALID)
        {
            MKB_LOG_INFO("CH(%d): update information, conn_handle= %d, peer_id= %d", ch_id, p_ble_evt->evt.gap_evt.conn_handle, peer_id);

            p_ch->config.info[ch_id].peer_id = peer_id;

            err_code = pm_peer_data_bonding_load(peer_id, &peer_bonding_data);
            if (err_code == NRF_SUCCESS)
            {
                ble_gap_addr_t *p_peer_addr = &(peer_bonding_data.peer_ble_id.id_addr_info);
                MKB_LOG_DEBUG("peer id: %d, addr: 0x%02x%02x%02x%02x%02x%02x", peer_id,
                              p_peer_addr->addr[5], p_peer_addr->addr[4], p_peer_addr->addr[3],
                              p_peer_addr->addr[2], p_peer_addr->addr[1], p_peer_addr->addr[0]);

                memcpy(&p_ch->config.info[ch_id].peer_addr, p_peer_addr, sizeof(ble_gap_addr_t));
            }

            mkb_db_store_ch();
        }

        break;
    }
    case BLE_GAP_EVT_CONN_SEC_UPDATE:
        pm_peer_id_get(p_ble_evt->evt.gap_evt.conn_handle, &peer_id);
        mkb_event_send(EVT_LED_CH_CONNECT);

        MKB_LOG_INFO("BLE_GAP_EVT_CONN_SEC_UPDATE: CH(%d), conn_handle= %d, peer_id= %d", ch_id, p_ble_evt->evt.gap_evt.conn_handle, peer_id);
        ________DBG_20210930_ble_event("BLE_GAP_EVT_CONN_SEC_UPDATE: CH(%d), conn_handle= %d, peer_id= %d", ch_id, p_ble_evt->evt.gap_evt.conn_handle, peer_id);
        break;

    default:
        MKB_LOG_INFO("NO_MATCHING_BLE_EVENT:%d", p_ble_evt->header.evt_id);
        // No implementation needed.
        break;
    }
}

/**@brief Function for initializing the BLE stack.
 *
 * @details Initializes the SoftDevice and the BLE event interrupt.
 */
static ret_code_t ble_stack_init(void)
{
    ret_code_t err_code;

    // Enable the SoftDevice.
    err_code = nrf_sdh_enable_request();
    if (err_code != NRF_SUCCESS)
    {
        MKB_ERROR_CHECK(err_code);
        return err_code;
    }

    // Configure the BLE stack using the default settings.
    // Fetch the start address of the application RAM.
    uint32_t ram_start = 0;
    err_code = nrf_sdh_ble_default_cfg_set(APP_BLE_CONN_CFG_TAG, &ram_start);
    if (err_code != NRF_SUCCESS)
    {
        MKB_ERROR_CHECK(err_code);
        return err_code;
    }

    MKB_LOG_INFO("start address of the application RAM: 0x%08x", ram_start);

    // Enable BLE stack.
    err_code = nrf_sdh_ble_enable(&ram_start);
    if (err_code != NRF_SUCCESS)
    {
        MKB_ERROR_CHECK(err_code);
        return err_code;
    }

    // Register a handler for BLE events.
    NRF_SDH_BLE_OBSERVER(m_ble_observer, APP_BLE_OBSERVER_PRIO, ble_evt_handler, NULL);

#if defined(POWER_ENABLED) && (POWER_ENABLED)
    sd_power_dcdc_mode_set((POWER_CONFIG_DEFAULT_DCDCEN) ? NRF_POWER_DCDC_ENABLE : NRF_POWER_DCDC_DISABLE);
    MKB_LOG_INFO("DCDC Converter: %s", (POWER_CONFIG_DEFAULT_DCDCEN) ? "NRF_POWER_DCDC_ENABLE" : "NRF_POWER_DCDC_DISABLE");
#endif

    MKB_LOG_INFO("Initialization Success!");

    return NRF_SUCCESS;
}

/**@brief Disable the BLE stack.
 *
 * @details Disables the SoftDevice.
 */
static void ble_stack_disable(void)
{
    ret_code_t err_code;

    err_code = nrf_sdh_disable_request();
    MKB_ERROR_CHECK(err_code);
}

/**@brief Function for the GAP initialization.
 *
 * @details This function sets up all the necessary GAP (Generic Access Profile) parameters of the
 *          device including the device name, appearance, and the preferred connection parameters.
 */
static void gap_params_init(void)
{
    ret_code_t err_code;
    ble_gap_conn_params_t gap_conn_params;
    ble_gap_conn_sec_mode_t sec_mode;

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&sec_mode);

    err_code = sd_ble_gap_device_name_set(&sec_mode,
                                          (const uint8_t *)m_ble_device_name,
                                          strlen(m_ble_device_name));
    MKB_ERROR_CHECK(err_code);

    // err_code = sd_ble_gap_appearance_set(BLE_APPEARANCE_GENERIC_HID);
    err_code = sd_ble_gap_appearance_set(BLE_APPEARANCE_HID_KEYBOARD);
    // err_code = sd_ble_gap_appearance_set(BLE_APPEARANCE_HID_MOUSE);
    MKB_ERROR_CHECK(err_code);

    memset(&gap_conn_params, 0, sizeof(gap_conn_params));

    gap_conn_params.min_conn_interval = MIN_CONN_INTERVAL;
    gap_conn_params.max_conn_interval = MAX_CONN_INTERVAL;
    gap_conn_params.slave_latency = SLAVE_LATENCY;
    gap_conn_params.conn_sup_timeout = CONN_SUP_TIMEOUT;

    err_code = sd_ble_gap_ppcp_set(&gap_conn_params);
    MKB_ERROR_CHECK(err_code);
}

/**@brief Function for handling events from the GATT library. */
static void gatt_evt_handler(nrf_ble_gatt_t *p_gatt, nrf_ble_gatt_evt_t const *p_evt)
{
    switch (p_evt->evt_id)
    {
    case NRF_BLE_GATT_EVT_ATT_MTU_UPDATED:
    {
        // m_mtu_exchanged = true;
        MKB_LOG_INFO("ATT MTU exchange completed. MTU set to %u bytes.",
                     p_evt->params.att_mtu_effective);
    }
    break;

    case NRF_BLE_GATT_EVT_DATA_LENGTH_UPDATED:
    {
        // m_data_length_updated = true;
        MKB_LOG_INFO("Data length updated to %u bytes.", p_evt->params.data_length);
    }
    break;
    }

    // nrf_ble_amts_on_gatt_evt(&m_amts, p_evt);
}

/**@brief Function for initializing the GATT module.
 */
static void gatt_init(void)
{
    ret_code_t err_code = nrf_ble_gatt_init(&m_gatt, gatt_evt_handler);
    MKB_ERROR_CHECK(err_code);
}

/**@brief Function for initializing Device Information Service.
 */
static void dis_init(void)
{
    ret_code_t err_code;
    ble_dis_init_t dis_init_obj;
    ble_dis_pnp_id_t pnp_id;

    pnp_id.vendor_id_source = PNP_ID_VENDOR_ID_SOURCE;
    pnp_id.vendor_id = PNP_ID_VENDOR_ID;
    pnp_id.product_id = PNP_ID_PRODUCT_ID;
    pnp_id.product_version = PNP_ID_PRODUCT_VERSION;

    memset(&dis_init_obj, 0, sizeof(dis_init_obj));

    ble_srv_ascii_to_utf8(&dis_init_obj.manufact_name_str, MANUFACTURER_NAME);
    dis_init_obj.p_pnp_id = &pnp_id;

    dis_init_obj.dis_char_rd_sec = SEC_JUST_WORKS;

    err_code = ble_dis_init(&dis_init_obj);
    MKB_ERROR_CHECK(err_code);
}

ret_code_t mkb_ble_bas_battery_level_update(uint8_t level)
{
    ret_code_t err_code;

    err_code = ble_bas_battery_level_update(&m_bas, level, BLE_CONN_HANDLE_ALL);
    ________DBG_20210907_sgs_battery("errcode:%d = ble_bas_battery_level_update(%d)", err_code, level);
    if ((err_code != NRF_SUCCESS) &&
        (err_code != NRF_ERROR_BUSY) &&
        (err_code != NRF_ERROR_RESOURCES) &&
        (err_code != NRF_ERROR_FORBIDDEN) &&
        (err_code != NRF_ERROR_INVALID_STATE) &&
        (err_code != BLE_ERROR_GATTS_SYS_ATTR_MISSING))
    {
        MKB_ERROR_HANDLER(err_code);
    }

    return err_code;
}

/**@brief Function for initializing Battery Service.
 */
static void bas_init(void)
{
    ret_code_t err_code;
    ble_bas_init_t bas_init_obj;

    memset(&bas_init_obj, 0, sizeof(bas_init_obj));

    bas_init_obj.evt_handler = NULL;
    bas_init_obj.support_notification = true;
    bas_init_obj.p_report_ref = NULL;
    bas_init_obj.initial_batt_level = 100;

    bas_init_obj.bl_rd_sec = SEC_JUST_WORKS;
    bas_init_obj.bl_cccd_wr_sec = SEC_JUST_WORKS;
    bas_init_obj.bl_report_rd_sec = SEC_JUST_WORKS;

    err_code = ble_bas_init(&m_bas, &bas_init_obj);
    MKB_ERROR_CHECK(err_code);
}

/**@brief Function for handling the HID Report Characteristic Write event.
 *
 * @param[in]   p_evt   HID service event.
 */
static void on_hid_rep_char_write(ble_hids_evt_t *p_evt)
{
    ret_code_t err_code;
    uint8_t report_val;
    uint8_t report_index = p_evt->params.char_write.char_id.rep_index;

    MKB_LOG_INFO("uuid %d report type %d, index %d!",
                 p_evt->params.char_write.char_id.uuid,
                 p_evt->params.char_write.char_id.rep_type, report_index);

    if (p_evt->params.char_write.char_id.rep_type == BLE_HIDS_REP_TYPE_OUTPUT)
    {
        MKB_LOG_INFO("BLE_HIDS_REP_TYPE_OUTPUT!");

        if (report_index == OUTPUT_REP_INDEX)
        {
            // This code assumes that the output report is one byte long. Hence the following
            // static assert is made.
            STATIC_ASSERT(OUTPUT_REP_MAX_LEN == 1);

            err_code = ble_hids_outp_rep_get(&m_hids,
                                             report_index,
                                             OUTPUT_REP_MAX_LEN,
                                             0,
                                             m_conn_handle,
                                             &report_val);
            MKB_ERROR_CHECK(err_code);

            // if (!m_caps_on && ((report_val & OUTPUT_REP_BIT_MASK_CAPS_LOCK) != 0))
            if (((report_val & OUTPUT_REP_BIT_MASK_CAPS_LOCK) != 0))
            {
                // Caps Lock is turned On.
                MKB_LOG_INFO("Caps Lock is turned On!");
                // err_code = bsp_indication_set(BSP_INDICATE_ALERT_3);
                // MKB_ERROR_CHECK(err_code);

#if (defined(MKB_KEYBOARD_ENABLED) && MKB_KEYBOARD_ENABLED)
                keyboard_caps_lock_set(true);
#endif // (defined(MKB_KEYBOARD_ENABLED) && MKB_KEYBOARD_ENABLED)
            }
            // else if (m_caps_on && ((report_val & OUTPUT_REP_BIT_MASK_CAPS_LOCK) == 0))
            else if (((report_val & OUTPUT_REP_BIT_MASK_CAPS_LOCK) == 0))
            {
                // Caps Lock is turned Off .
                MKB_LOG_INFO("Caps Lock is turned Off!");
                // err_code = bsp_indication_set(BSP_INDICATE_ALERT_OFF);
                // MKB_ERROR_CHECK(err_code);

#if (defined(MKB_KEYBOARD_ENABLED) && MKB_KEYBOARD_ENABLED)
                keyboard_caps_lock_set(false);
#endif // (defined(MKB_KEYBOARD_ENABLED) && MKB_KEYBOARD_ENABLED)
            }
            else
            {
                // The report received is not supported by this application. Do nothing.
            }
        }
    }

    if (p_evt->params.char_write.char_id.rep_type == BLE_HIDS_REP_TYPE_FEATURE)
    {
        MKB_LOG_INFO("BLE_HIDS_REP_TYPE_FEATURE!");

        // if (report_index == FEATURE_REP_INDEX)
        {
        }
    }
}

/**@brief Function for handling the HID Report Characteristic Read event.
 *
 * @param[in]   p_evt   HID service event.
 */
static void on_hid_rep_char_read(ble_hids_evt_t *p_evt)
{
    ret_code_t err_code;
    uint8_t report_val;
    uint8_t report_index = p_evt->params.char_auth_read.char_id.rep_index;

    MKB_LOG_INFO("uuid %d report type %d, index %d!",
                 p_evt->params.char_auth_read.char_id.uuid,
                 p_evt->params.char_auth_read.char_id.rep_type, report_index);

    if (p_evt->params.char_auth_read.char_id.rep_type == BLE_HIDS_REP_TYPE_FEATURE)
    {
        MKB_LOG_INFO("BLE_HIDS_REP_TYPE_FEATURE!");

        // if (report_index == FEATURE_REP_INDEX)
        {
        }
    }
}

/**@brief Function for handling HID events.
 *
 * @details This function will be called for all HID events which are passed to the application.
 *
 * @param[in]   p_hids  HID service structure.
 * @param[in]   p_evt   Event received from the HID service.
 */
static void on_hids_evt(ble_hids_t *p_hids, ble_hids_evt_t *p_evt)
{
    switch (p_evt->evt_type)
    {
    case BLE_HIDS_EVT_BOOT_MODE_ENTERED:
        MKB_LOG_INFO("HIDS BOOT MODE");
        ________DBG_20210930_ble_event("BLE_HIDS_EVT_BOOT_MODE_ENTERED");
        m_in_boot_mode = true;
        break;

    case BLE_HIDS_EVT_REPORT_MODE_ENTERED:
        MKB_LOG_INFO("HIDS REPORT MODE");
        ________DBG_20210930_ble_event("BLE_HIDS_EVT_REPORT_MODE_ENTERED");
        m_in_boot_mode = false;
        break;

    case BLE_HIDS_EVT_REP_CHAR_WRITE:
        on_hid_rep_char_write(p_evt);
        ________DBG_20210930_ble_event("BLE_HIDS_EVT_REP_CHAR_WRITE");
        break;

    case BLE_HIDS_EVT_REPORT_READ:
        MKB_LOG_INFO("HIDS REPORT READ");
        ________DBG_20210930_ble_event("BLE_HIDS_EVT_REPORT_READ");
        on_hid_rep_char_read(p_evt);
        break;

    case BLE_HIDS_EVT_NOTIF_ENABLED:
        ________DBG_20210930_ble_event("BLE_HIDS_EVT_NOTIF_ENABLED");
        break;

    default:
        // No implementation needed.
        ________DBG_20210930_ble_event("NO_MATCHING_ON_HIDS_EVENT");
        break;
    }
}

/**@brief Function for handling Service errors.
 *
 * @details A pointer to this function will be passed to each service which may need to inform the
 *          application about an error.
 *
 * @param[in]   nrf_error   Error code containing information about what went wrong.
 */
static void service_error_handler(uint32_t nrf_error)
{
    MKB_ERROR_HANDLER(nrf_error);
}

/**@brief Function for initializing HID Service.
 */

#define FINGER_USAGE                                                          \
    0x05, 0x0D,                                /*   Usage Page (Digitizer) */ \
        0x09, 0x22,                            /*   Usage (Finger) */         \
        0xA1, 0x02,                            /*   Collection (Logical) */   \
        0x09, 0x47,                            /*     Usage (Confidence) */   \
        0x09, 0x42,                            /*     Usage (Tip Switch) */   \
        0x09, 0x32, /*     Usage (In Range) */ //0x15, 0x00,        /*     Logical Minimum (0) */ \
	0x25, 0x01,        /*     Logical Maximum (1) */ \
	0x75, 0x01,        /*     Report Size (1) */ \
	0x95, 0x03,        /*     Report Count (3) */ \
	0x81, 0x02,        /*     Input (Data,Var,Abs) */ \
    0x95, 0x01,        /* Report Count (1) */ \
    0x75, 0x01,        /* Report Size (1) */ \
    0x81, 0x03,        /* Input (Constant) for padding */ \
	0x09, 0x51,        /*     Usage (0x51) Contact identifier */ \
	0x75, 0x04,        /*     Report Size (4) */ \
	0x95, 0x01,        /*     Report Count (1) */ \
	0x25, 0x0A,        /*     Logical Maximum (10) */ \
	0x81, 0x02,        /*     Input (Data,Var,Abs) */ \
	0x05, 0x01,        /*     Usage Page (Generic Desktop Ctrls) */ \
	0x09, 0x30,        /*     Usage (X) */ \
	0x75, 0x10,        /*     Report Size (16) */ \
	0x55, 0x0E,        /*     Unit Exponent (-2) */ \
	0x65, 0x11,        /*     Unit (System: SI Linear, Length: cm) */ \
	0x35, 0x00,        /*     Physical Minimum (0) */ \
	0x26, (MKB_TOUCHPAD_LOGICAL_MAX_X & 0xff), \
	      (MKB_TOUCHPAD_LOGICAL_MAX_X >> 8), \
	                   /*     Logical Maximum */ \
	0x46, (MKB_TOUCHPAD_PHYSICAL_MAX_X & 0xff), \
	      (MKB_TOUCHPAD_PHYSICAL_MAX_X >> 8), \
	                   /*     Physical Maximum (tenth of mm) */ \
	0x81, 0x02,        /*     Input (Data,Var,Abs) */ \
	0x09, 0x31,        /*     Usage (Y) */ \
	0x26, (MKB_TOUCHPAD_LOGICAL_MAX_Y & 0xff), \
	      (MKB_TOUCHPAD_LOGICAL_MAX_Y >> 8), \
	                   /*     Logical Maximum */ \
	0x46, (MKB_TOUCHPAD_PHYSICAL_MAX_Y & 0xff), \
	      (MKB_TOUCHPAD_PHYSICAL_MAX_Y >> 8), \
	                   /*     Physical Maximum (tenth of mm) */ \
	0x81, 0x02,        /*     Input (Data,Var,Abs) */ \
	0xC0,               /*   End Collection */

static void hids_init(void)
{
    ret_code_t err_code;
    ble_hids_init_t hids_init_obj;
    ble_hids_inp_rep_init_t *p_input_report;
    ble_hids_outp_rep_init_t *p_output_report;
    ble_hids_feature_rep_init_t *p_feature_report;
    uint8_t hid_info_flags;

    static ble_hids_inp_rep_init_t inp_rep_array[INPUT_REPORT_COUNT];
    static ble_hids_outp_rep_init_t out_rep_array[OUTPUT_REPORT_COUNT];
    static ble_hids_feature_rep_init_t feature_rep_array[FEATURE_REPORT_COUNT];
    static uint8_t rep_map_data[] =
    {
        /////////////////////////////////////////////////////////////////////////
        // Keyboard
        /////////////////////////////////////////////////////////////////////////
        0x05,
        0x01, // Usage Page (Generic Desktop)
        0x09,
        0x06, // Usage (Keyboard)
        0xA1,
        0x01, // Collection (Application)

        // Report ID 1: Keyboard
        0x85,
        INPUT_REP_REF_KEYS_ID, // Report Id (1)
        0x05,
        0x07, // Usage Page (Key Codes)
        0x19,
        0xe0, // Usage Minimum (224)
        0x29,
        0xe7, // Usage Maximum (231)
        0x15,
        0x00, // Logical Minimum (0)
        0x25,
        0x01, // Logical Maximum (1)
        0x75,
        0x01, // Report Size (1)
        0x95,
        0x08, // Report Count (8)
        0x81,
        0x02, // Input (Data, Variable, Absolute)
        0x95,
        0x01, // Report Count (1)
        0x75,
        0x08, // Report Size (8)
        0x81,
        0x01, // Input (Constant) reserved byte(1)
        0x95,
        0x05, // Report Count (5)
        0x75,
        0x01, // Report Size (1)
        0x05,
        0x08, // Usage Page (Page# for LEDs)
        0x19,
        0x01, // Usage Minimum (1)
        0x29,
        0x05, // Usage Maximum (5)
        0x91,
        0x02, // Output (Data, Variable, Absolute), Led report
        0x95,
        0x01, // Report Count (1)
        0x75,
        0x03, // Report Size (3)
        0x91,
        0x01, // Output (Data, Variable, Absolute), Led report padding
        0x95,
        0x06, // Report Count (6)
        0x75,
        0x08, // Report Size (8)
        0x15,
        0x00, // Logical Minimum (0)
        0x25,
        0x65, // Logical Maximum (101)
        0x05,
        0x07, // Usage Page (Key codes)
        0x19,
        0x00, // Usage Minimum (0)
        0x29,
        0x65, // Usage Maximum (101)
        0x81,
        0x00, // Input (Data, Array) Key array(6 bytes)

        // 0x09, 0x05,       // Usage (Vendor Defined)
        // 0x15, 0x00,       // Logical Minimum (0)
        // 0x26, 0xFF, 0x00, // Logical Maximum (255)
        // 0x75, 0x08,       // Report Count (2)
        // 0x95, 0x02,       // Report Size (8 bit)
        // 0xB1, 0x02,       // Feature (Data, Variable, Absolute)

        0xC0, // End Collection (Application)

        /////////////////////////////////////////////////////////////////////////
        // Mouse
        /////////////////////////////////////////////////////////////////////////
        0x05,
        0x01, // Usage Page (Generic Desktop)
        0x09,
        0x02, // Usage (Mouse)

        0xA1,
        0x01, // Collection (Application)

        // Report ID 2: Mouse buttons + scroll/pan
        0x85,
        INPUT_REP_REF_BUTTONS_ID, // Report Id 2
        0x09,
        0x01, // Usage (Pointer)
        0xA1,
        0x00, // Collection (Physical)
        0x95,
        0x05, // Report Count (3)
        0x75,
        0x01, // Report Size (1)
        0x05,
        0x09, // Usage Page (Buttons)
        0x19,
        0x01, // Usage Minimum (01)
        0x29,
        0x05, // Usage Maximum (05)
        0x15,
        0x00, // Logical Minimum (0)
        0x25,
        0x01, // Logical Maximum (1)
        0x81,
        0x02, // Input (Data, Variable, Absolute)
        0x95,
        0x01, // Report Count (1)
        0x75,
        0x03, // Report Size (3)
        0x81,
        0x01, // Input (Constant) for padding
        0x75,
        0x08, // Report Size (8)
        0x95,
        0x01, // Report Count (1)
        0x05,
        0x01, // Usage Page (Generic Desktop)
        0x09,
        0x38, // Usage (V-Wheel)
        0x15,
        0x81, // Logical Minimum (-127)
        0x25,
        0x7F, // Logical Maximum (127)
        0x81,
        0x06, // Input (Data, Variable, Relative)
        0x05,
        0x0C, // Usage Page (Consumer)
        0x0A,
        0x38,
        0x02, // Usage (H-Wheel)
        0x95,
        0x01, // Report Count (1)
        0x81,
        0x06, // Input (Data,Value,Relative,Bit Field)
        0xC0, // End Collection (Physical)

        // Report ID 3: Mouse motion
        0x85,
        INPUT_REP_REF_MOVEMENT_ID, // Report Id 3
        0x09,
        0x01, // Usage (Pointer)
        0xA1,
        0x00, // Collection (Physical)
        0x75,
        0x0C, // Report Size (12)
        0x95,
        0x02, // Report Count (2)
        0x05,
        0x01, // Usage Page (Generic Desktop)
        0x09,
        0x30, // Usage (X)
        0x09,
        0x31, // Usage (Y)
        0x16,
        0x01,
        0xF8, // Logical maximum (2047)
        0x26,
        0xFF,
        0x07, // Logical minimum (-2047)
        0x81,
        0x06, // Input (Data, Variable, Relative)
        0xC0, // End Collection (Physical)
        0xC0, // End Collection (Application)

        /////////////////////////////////////////////////////////////////////////
        // Consumer
        /////////////////////////////////////////////////////////////////////////
        0x05,
        0x0C, // Usage Page (Consumer)
        0x09,
        0x01, // Usage (Consumer Control)
        0xA1,
        0x01, // Collection (Application)

        // Report ID 4: Consumer
        0x85,
        INPUT_REP_REF_CONSUMER_ID, // Report Id (4)

#if (INPUT_REP_CONSUMER_LEN == 3)
        0x75,
        12, //      Report Size (CONSUMER_CTRL_IN_REP_SIZE)
        0x95,
        2, //      Report Count (CONSUMER_CTRL_IN_REP_COUNT)
        0x15,
        0x00, //      Logical Minimum (0)
        0x26,
        0xFF,
        0x07, //      Logical Maximum (2047)
        0x19,
        0x00, //      Usage Minimum (0)
        0x2A,
        0xFF,
        0x07, //      Usage Maximum (2047)
        0x81,
        0x00, //      Input (Data, Ary, Abs)
#elif (INPUT_REP_CONSUMER_LEN == 1)
        0x15,
        0x00, // Logical minimum (0)
        0x25,
        0x01, // Logical maximum (1)
        0x75,
        0x01, // Report Size (1)
        0x95,
        0x01, // Report Count (1)

        0x09,
        0xE2, // Usage (Mute)
        0x81,
        0x06, // Input (Data,Value,Relative,Bit Field)
        0x09,
        0xEA, // Usage (Volume Down)
        0x81,
        0x06, // Input (Data,Value,Relative,Bit Field)
        0x09,
        0xE9, // Usage (Volume Up)
        0x81,
        0x06, // Input (Data,Value,Relative,Bit Field)
        0x09,
        0xCD, // Usage (Play/Pause)
        0x81,
        0x06, // Input (Data,Value,Relative,Bit Field)

        0x09,
        0xB5, // Usage (Scan Next Track)
        0x81,
        0x06, // Input (Data,Value,Relative,Bit Field)
        0x09,
        0xB6, // Usage (Scan Previous Track)
        0x81,
        0x06, // Input (Data,Value,Relative,Bit Field)
        0x0A,
        0x25,
        0x02, // Usage (AC Forward)
        0x81,
        0x06, // Input (Data,Value,Relative,Bit Field)
        0x0A,
        0x24,
        0x02, // Usage (AC Back)
        0x81,
        0x06, // Input (Data,Value,Relative,Bit Field)
#endif

        0xC0, // End Collection

#if MKB_BLE_TOUCH_DEVICE_ENABLED

// ================================================================================
#if MKB_BLE_TOUCH_DEVICE_TYPE == 0 // multi touch device (product ID: 0x048E + 0x048F) \
                                   // ================================================================================

        /////////////////////////////////////////////////////////////////////////
        // Touchpad
        // ----------------------------------------------------------------------
        // Scan Time: 100usec unit time
        // ----------------------------------------------------------------------
        // NRF_ERROR_NO_MEM: 참조,  https://devzone.nordicsemi.com/f/nordic-q-a/34533/nrf_error_no_mem
        // modifying NRF_SDH_BLE_GATTS_ATTR_TAB_SIZE in the sdk_config.h
        // 580 -> 640
        /////////////////////////////////////////////////////////////////////////
        0x05,
        0x0d, // Usage Page (Digitizers)
        0x09,
        0x05, // Usage (Touch Pad)
        0xA1,
        0x01, // Collection (Application)

        // Report ID 5: Touchpad
        0x85,
        INPUT_REP_REF_TOUCH_ID, // Report Id (5)

        /* Finger 1 */
        FINGER_USAGE
            /* Finger 2 */
            FINGER_USAGE
                /* Finger 3 */
                FINGER_USAGE
                    /* Finger 4 */
                    FINGER_USAGE
                        /* Finger 5 */
                        FINGER_USAGE

        /* Contact count */
        0x05,
        0x0D, /*   Usage Page (Digitizer) */
        0x09,
        0x54, /*   Usage (Contact count) */
        0x25,
        0x05, /*   Logical Maximum (MAX_FINGERS) */
        0x75,
        0x08, /*   Report Size (8) */
        0x95,
        0x01, /*   Report Count (1) */
        0x81,
        0x02, /*   Input (Data,Var,Abs) */
        /* Button */
        0x05,
        0x09, /*   Usage Page(Button) */
        0x09,
        0x01, /*   Usage (Button) */
        0x19,
        0x00, /*   Usage Minimum (0x00) */
        0x29,
        0x01, /*   Usage Maximum (0x01) */
        0x15,
        0x00, /*   Logical Minimum (0) */
        0x25,
        0x01, /*   Logical Maximum (1) */
        0x75,
        0x01, /*   Report Size (1) */
        0x95,
        0x01, /*   Report Count (1) */
        0x81,
        0x02, /*   Input (Data,Var,Abs) */
        0x95,
        0x01, // Report Count (1)
        0x75,
        0x07, // Report Size (7)
        0x81,
        0x01, // Input (Constant) for padding
        /* Timestamp */
        0x05,
        0x0D, /*   Usage Page (Digitizer) */
        0x09,
        0x56, /*   Usage (0x56, Relative Scan Time) */
        0x55,
        0x0C, /*   Unit Exponent (-4) */
        0x66,
        0x01,
        0x10, /*   Unit (Seconds) */
        0x09,
        0x56, // Usage (Scan Time)
        0x35,
        0x00, // Physical Minimum (0)
        0x47,
        0xFF,
        0xFF,
        0x00,
        0x00, /*   Physical Maximum (65535) */
        0x15,
        0x00, // Logical Minimum (0)
        0x27,
        0xFF,
        0xFF,
        0x00,
        0x00, /*   Logical Maximum (65535) */
        0x75,
        0x10, /*   Report Size (16) */
        0x95,
        0x01, /*   Report Count (1) */
        0x81,
        0x02, /*   Input (Data,Var,Abs) */
        /* Feature */
        0x05,
        0x0D, /*   Usage Page (Digitizer) */
        0x85,
        FEATURE_REP_REF_DEVICE_CAPS_ID, /*   Report ID (Device Capabilities) */
        0x09,
        0x55, /*   Usage (Contact Count Maximum) */
        0x09,
        0x59, /*   Usage (Pad Type) */
        0x25,
        0x05, /*   Logical Maximum (5) */
        0x75,
        0x08, /*   Report Size (8) */
        0x95,
        0x02, /*   Report Count (2) */
        0xB1,
        0x02, /*   Feature (Data,Var,Abs) */
        // CONFIG TLC
        0x05,
        0x0d, //    USAGE_PAGE (Digitizer)
        0x09,
        0x0E, //    USAGE (Configuration)
        0xa1,
        0x01, //   COLLECTION (Application)
        0x85,
        FEATURE_REP_REF_CONFIG_ID, //   REPORT_ID (Feature)
        0x09,
        0x22, //   USAGE (Finger)
        0xa1,
        0x02, //   COLLECTION (logical)
        0x09,
        0x52, //    USAGE (Input Mode)
        0x15,
        0x00, //    LOGICAL_MINIMUM (0)
        0x25,
        0x0a, //    LOGICAL_MAXIMUM (10)
        0x75,
        0x08, //    REPORT_SIZE (8)
        0x95,
        0x01, //    REPORT_COUNT (1)
        0xb1,
        0x02, //    FEATURE (Data,Var,Abs
        0xc0, //   END_COLLECTION
#if 0
        /* Page 0xFF, usage 0xC5 is device certificate. */
        0x06, 0x00, 0xFF,              /*   Usage Page (Vendor Defined) */
        0x85, FEATURE_REP_REF_DEVICE_CERT_ID,   /*   Report ID (Device Certification) */
        0x09, 0xC5,                    /*   Usage (Vendor Usage 0xC5) */
        0x15, 0x00,                    /*   Logical Minimum (0) */
        0x26, 0xFF, 0x00,              /*   Logical Maximum (255) */
        0x75, 0x08,                    /*   Report Size (8) */
        0x96, 0x00, 0x01,              /*   Report Count (256) */
        0xB1, 0x02,                    /*   Feature (Data,Var,Abs) */
#endif
        0xC0, /* End Collection */

// ================================================================================
#elif MKB_BLE_TOUCH_DEVICE_TYPE == 1 // multi touch device (product ID: 0x048E + 0x048F)
              // ================================================================================

        0x05,
        0x0D, // USAGE_PAGE(Digitizers)
        0x09,
        0x04, // USAGE     (Touch Screen)
        0xA1,
        0x01, // COLLECTION(Application)

        // Report ID 5: Touchpad
        0x85,
        INPUT_REP_REF_TOUCH_ID, // Report Id (5)

        // define the maximum amount of fingers that the device supports
        0x09,
        0x55, //   USAGE(Contact Count Maximum)
        0x25,
        0x05, //   LOGICAL_MAXIMUM (5)
        0x95,
        0x01, //   REPORT_COUNT(1)
        0x75,
        0x08, //   REPORT_SIZE (8)
        0xB1,
        0x02, //   FEATURE (Data,Var,Abs)

        // define the actual amount of fingers that are concurrently touching the screen
        0x09,
        0x54, //   USAGE (Contact count)
        0x95,
        0x01, //   REPORT_COUNT(1)
        0x75,
        0x08, //   REPORT_SIZE (8)
        0x81,
        0x02, //   INPUT (Data,Var,Abs)

        // declare a finger collection
        0x09,
        0x22, //   USAGE (Finger)
        0xA1,
        0x02, //   COLLECTION (Logical)

        // declare an identifier for the finger
        0x09,
        0x51, //     USAGE (Contact Identifier)
        0x75,
        0x08, //     REPORT_SIZE (8)
        0x95,
        0x01, //     REPORT_COUNT (1)
        0x81,
        0x02, //     INPUT (Data,Var,Abs)

        // declare Tip Switch and In Range
        0x09,
        0x42, //     USAGE (Tip Switch)
        0x09,
        0x32, //     USAGE (In Range)
        0x15,
        0x00, //     LOGICAL_MINIMUM (0)
        0x25,
        0x01, //     LOGICAL_MAXIMUM (1)
        0x75,
        0x01, //     REPORT_SIZE (1)
        0x95,
        0x02, //     REPORT_COUNT(2)
        0x81,
        0x02, //     INPUT (Data,Var,Abs)

        // declare the remaining 6 bits of the first data byte as constant -> the driver will ignore them
        0x95,
        0x06, //     REPORT_COUNT (6)
        0x81,
        0x03, //     INPUT (Cnst,Ary,Abs)

        // define absolute X and Y coordinates of 16 bit each (percent values multiplied with 100)
        0x05,
        0x01, //     USAGE_PAGE (Generic Desktop)
        0x09,
        0x30, //     Usage (X)
        0x16,
        0x00,
        0x00, //     Logical Minimum (0)
        0x26,
        (MKB_TOUCHPAD_LOGICAL_MAX_X & 0xff),
        (MKB_TOUCHPAD_LOGICAL_MAX_X >> 8), //     Logical Maximum
        0x36,
        0x00,
        0x00, //     Physical Minimum (0)
        0x46,
        (MKB_TOUCHPAD_PHYSICAL_MAX_X & 0xff),
        (MKB_TOUCHPAD_PHYSICAL_MAX_X >> 8), //     Physical Maximum
                                            // 0x46, 0x10, 0x27,               (10000)
        0x66,
        0x00,
        0x00, //     UNIT (None)
        0x75,
        0x10, //     Report Size (16),
        0x95,
        0x01, //     Report Count (1),
        0x81,
        0x62, //     Input (Data,Var,Abs)
        0x05,
        0x01, //     USAGE_PAGE (Generic Desktop)
        0x09,
        0x31, //     Usage (Y)
        0x16,
        0x00,
        0x00, //     Logical Minimum (0)
        0x26,
        (MKB_TOUCHPAD_LOGICAL_MAX_Y & 0xff),
        (MKB_TOUCHPAD_LOGICAL_MAX_Y >> 8), //     Logical Maximum
        0x36,
        0x00,
        0x00, //     Physical Minimum (0)
        0x46,
        (MKB_TOUCHPAD_PHYSICAL_MAX_Y & 0xff),
        (MKB_TOUCHPAD_PHYSICAL_MAX_Y >> 8), //     Physical Maximum
                                            // 0x46, 0x10, 0x27,               (10000)
        0x66,
        0x00,
        0x00, //     UNIT (None)
        0x75,
        0x10, //     Report Size (16),
        0x95,
        0x01, //     Report Count (1),
        0x81,
        0x62, //     Input (Data,Var,Abs)
        0xC0, //   END_COLLECTION
        0xC0  // END_COLLECTION

    // With this declaration a data packet must be sent as:
    // byte 1   -> "contact count"        (always == 1)
    // byte 2   -> "contact identifier"   (any value)
    // byte 3   -> "Tip Switch" state     (bit 0 = Tip Switch up/down, bit 1 = In Range)
    // byte 4,5 -> absolute X coordinate  (0...10000)
    // byte 6,7 -> absolute Y coordinate  (0...10000)

// ================================================================================
#elif MKB_BLE_TOUCH_DEVICE_TYPE == 2 // single touch device (product ID: 0x048C + 0x048D)
              // ================================================================================

        0x05,
        0x0d, // USAGE_PAGE (Digitizer)
        0x09,
        0x02, // USAGE (Pen)
        0xa1,
        0x01, // COLLECTION (Application)

        // Report ID 5: Touchpad
        0x85,
        INPUT_REP_REF_TOUCH_ID, // Report Id (5)

        // declare a finger collection
        0x09,
        0x20, //   Usage (Stylus)
        0xA1,
        0x00, //   Collection (Physical)

        // Declare a finger touch (finger up/down)
        0x09,
        0x42, //     Usage (Tip Switch)
        0x09,
        0x32, //     USAGE (In Range)
        0x15,
        0x00, //     LOGICAL_MINIMUM (0)
        0x25,
        0x01, //     LOGICAL_MAXIMUM (1)
        0x75,
        0x01, //     REPORT_SIZE (1)
        0x95,
        0x02, //     REPORT_COUNT (2)
        0x81,
        0x02, //     INPUT (Data,Var,Abs)

        // Declare the remaining 6 bits of the first data byte as constant -> the driver will ignore them
        0x75,
        0x01, //     REPORT_SIZE (1)
        0x95,
        0x06, //     REPORT_COUNT (6)
        0x81,
        0x01, //     INPUT (Cnst,Ary,Abs)

        // Define absolute X and Y coordinates of 16 bit each (percent values multiplied with 100)
        // http://www.usb.org/developers/hidpage/Hut1_12v2.pdf
        // Chapter 16.2 says: "In the Stylus collection a Pointer physical collection will contain the axes reported by the stylus."
        0x05,
        0x01, //     Usage Page (Generic Desktop)
        0x09,
        0x01, //     Usage (Pointer)
        0xA1,
        0x00, //     Collection (Physical)
        0x09,
        0x30, //        Usage (X)
        0x09,
        0x31, //        Usage (Y)
        0x16,
        0x00,
        0x00, //        Logical Minimum (0)
        0x26,
        0x10,
        0x27, //     Logical Maximum (10000)
        0x36,
        0x00,
        0x00, //        Physical Minimum (0)
        0x46,
        0x10,
        0x27, //     Physical Maximum (10000)
        0x66,
        0x00,
        0x00, //        UNIT (None)
        0x75,
        0x10, //        Report Size (16),
        0x95,
        0x02, //        Report Count (2),
        0x81,
        0x02, //        Input (Data,Var,Abs)
        0xc0, //     END_COLLECTION

        0xc0, //   END_COLLECTION
        0xc0  // END_COLLECTION

    // With this declaration a data packet must be sent as:
    // byte 1   -> "touch" state          (bit 0 = pen up/down, bit 1 = In Range)
    // byte 2,3 -> absolute X coordinate  (0...10000)
    // byte 4,5 -> absolute Y coordinate  (0...10000)

// ================================================================================
#elif MKB_BLE_TOUCH_DEVICE_TYPE == 3 // pointer device (product ID: 0x048A + 0x048B)
              // This descriptor is a copy of a real touchscreen: ELO TouchSystems CarrollTouch 4500U
              // ================================================================================

        0x05,
        0x01, // USAGE_PAGE (Generic Desktop)
        0x09,
        0x01, // USAGE (Pointer)
        0xa1,
        0x01, // COLLECTION (Application)

        // Report ID 5: Touchpad
        0x85,
        INPUT_REP_REF_TOUCH_ID, // Report Id (5)

        // declare a pointer collection
        0x05,
        0x01, //   USAGE_PAGE (Generic Desktop)
        0x09,
        0x01, //   USAGE (Pointer)
        0xa1,
        0x00, //   COLLECTION (Physical)

        // Declare a button
        0x05,
        0x09, //     USAGE_PAGE (Button)
        0x19,
        0x01, //     USAGE_MINIMUM (Button 1)
        0x29,
        0x01, //     USAGE_MAXIMUM (Button 1)
        0x15,
        0x00, //     LOGICAL_MINIMUM (0)
        0x25,
        0x01, //     LOGICAL_MAXIMUM (1)
        0x35,
        0x00, //     PHYSICAL_MINIMUM (0)
        0x45,
        0x01, //     PHYSICAL_MAXIMUM (1)
        0x66,
        0x00,
        0x00, //     UNIT (None)
        0x75,
        0x01, //     REPORT_SIZE (1)
        0x95,
        0x01, //     REPORT_COUNT (1)
        0x81,
        0x62, //     INPUT (Data,Var,Abs)

        // Declare the remaining 7 bits of the first data byte as constant -> the driver will ignore them
        0x75,
        0x01, //     REPORT_SIZE (1)
        0x95,
        0x07, //     REPORT_COUNT (7)
        0x81,
        0x01, //     INPUT (Cnst,Ary,Abs)

        // Define absolute X and Y coordinates of 16 bit each (percent values multiplied with 100)
        0x05,
        0x01, //     Usage Page (Generic Desktop)
        0x09,
        0x30, //     Usage (X)
        0x09,
        0x31, //     Usage (Y)
        0x16,
        0x00,
        0x00, //     Logical Minimum (0)
        0x26,
        0x10,
        0x27, //     Logical Maximum (10000)
        0x36,
        0x00,
        0x00, //     Physical Minimum (0)
        0x46,
        0x10,
        0x27, //     Physical Maximum (10000)
        0x66,
        0x00,
        0x00, //     UNIT (None)
        0x75,
        0x10, //     Report Size (16),
        0x95,
        0x02, //     Report Count (2),
        0x81,
        0x62, //     Input (Data,Var,Abs)

        0xc0, //   END_COLLECTION
        0xc0  // END_COLLECTION

    // With this declaration a data packet must be sent as:
    // byte 1   -> "button" state         (bit 0 = Button up/down)
    // byte 2,3 -> absolute X coordinate  (0...10000)
    // byte 4,5 -> absolute Y coordinate  (0...10000)

#else
#error "Invalid value for TOUCH_DEVICE"
#endif
#endif
    };

    memset(inp_rep_array, 0, sizeof(inp_rep_array));
    memset(out_rep_array, 0, sizeof(out_rep_array));
    memset(feature_rep_array, 0, sizeof(feature_rep_array));

    // Initialize HID Service.
    p_input_report = &inp_rep_array[INPUT_REP_KEYS_INDEX];
    p_input_report->max_len = INPUT_REP_KEYS_MAX_LEN;
    p_input_report->rep_ref.report_id = INPUT_REP_REF_KEYS_ID;
    p_input_report->rep_ref.report_type = BLE_HIDS_REP_TYPE_INPUT;

    p_input_report->sec.cccd_wr = SEC_JUST_WORKS;
    p_input_report->sec.wr = SEC_JUST_WORKS;
    p_input_report->sec.rd = SEC_JUST_WORKS;

    p_input_report = &inp_rep_array[INPUT_REP_BUTTONS_INDEX];
    p_input_report->max_len = INPUT_REP_BUTTONS_LEN;
    p_input_report->rep_ref.report_id = INPUT_REP_REF_BUTTONS_ID;
    p_input_report->rep_ref.report_type = BLE_HIDS_REP_TYPE_INPUT;

    p_input_report->sec.cccd_wr = SEC_JUST_WORKS;
    p_input_report->sec.wr = SEC_JUST_WORKS;
    p_input_report->sec.rd = SEC_JUST_WORKS;

    p_input_report = &inp_rep_array[INPUT_REP_MOVEMENT_INDEX];
    p_input_report->max_len = INPUT_REP_MOVEMENT_LEN;
    p_input_report->rep_ref.report_id = INPUT_REP_REF_MOVEMENT_ID;
    p_input_report->rep_ref.report_type = BLE_HIDS_REP_TYPE_INPUT;

    p_input_report->sec.cccd_wr = SEC_JUST_WORKS;
    p_input_report->sec.wr = SEC_JUST_WORKS;
    p_input_report->sec.rd = SEC_JUST_WORKS;

    p_input_report = &inp_rep_array[INPUT_REP_CONSUMER_INDEX];
    p_input_report->max_len = INPUT_REP_CONSUMER_LEN;
    p_input_report->rep_ref.report_id = INPUT_REP_REF_CONSUMER_ID;
    p_input_report->rep_ref.report_type = BLE_HIDS_REP_TYPE_INPUT;

    p_input_report->sec.cccd_wr = SEC_JUST_WORKS;
    p_input_report->sec.wr = SEC_JUST_WORKS;
    p_input_report->sec.rd = SEC_JUST_WORKS;

#if MKB_BLE_TOUCH_DEVICE_ENABLED
    p_input_report = &inp_rep_array[INPUT_REP_TOUCH_INDEX];
    p_input_report->max_len = INPUT_REP_TOUCH_LEN;
    p_input_report->rep_ref.report_id = INPUT_REP_REF_TOUCH_ID;
    p_input_report->rep_ref.report_type = BLE_HIDS_REP_TYPE_INPUT;

    p_input_report->sec.cccd_wr = SEC_JUST_WORKS;
    p_input_report->sec.wr = SEC_JUST_WORKS;
    p_input_report->sec.rd = SEC_JUST_WORKS;
#endif

#if 1
#if FEATURE_REPORT_COUNT
#if MKB_BLE_TOUCH_DEVICE_TYPE == BLE_TOUCH_DEVICE_PAD
    p_feature_report = &feature_rep_array[FEATURE_REP_REF_DEVICE_CAPS_INDEX];
    p_feature_report->max_len = FEATURE_REP_REF_DEVICE_CAPS_LEN;
    p_feature_report->rep_ref.report_id = FEATURE_REP_REF_DEVICE_CAPS_ID;
    p_feature_report->rep_ref.report_type = BLE_HIDS_REP_TYPE_FEATURE;

    p_feature_report->sec.cccd_wr = SEC_JUST_WORKS;
    p_feature_report->sec.wr = SEC_JUST_WORKS;
    p_feature_report->sec.rd = SEC_JUST_WORKS;

    p_feature_report = &feature_rep_array[FEATURE_REP_REF_CONFIG_INDEX];
    p_feature_report->max_len = FEATURE_REP_REF_CONFIG_LEN;
    p_feature_report->rep_ref.report_id = FEATURE_REP_REF_CONFIG_ID;
    p_feature_report->rep_ref.report_type = BLE_HIDS_REP_TYPE_FEATURE;

    p_feature_report->sec.cccd_wr = SEC_JUST_WORKS;
    p_feature_report->sec.wr = SEC_JUST_WORKS;
    p_feature_report->sec.rd = SEC_JUST_WORKS;
#elif MKB_BLE_TOUCH_DEVICE_TYPE == BLE_TOUCH_DEVICE_SCREEN
    p_feature_report = &feature_rep_array[FEATURE_REP_INDEX];
    p_feature_report->max_len = FEATURE_REP_MAX_LEN;
    p_feature_report->rep_ref.report_id = FEATURE_REP_REF_ID;
    p_feature_report->rep_ref.report_type = BLE_HIDS_REP_TYPE_FEATURE;

    p_feature_report->sec.cccd_wr = SEC_JUST_WORKS;
    p_feature_report->sec.wr = SEC_JUST_WORKS;
    p_feature_report->sec.rd = SEC_JUST_WORKS;
#endif
#endif
#endif

    p_output_report = &out_rep_array[OUTPUT_REP_INDEX];
    p_output_report->max_len = OUTPUT_REP_MAX_LEN;
    p_output_report->rep_ref.report_id = OUTPUT_REP_REF_ID;
    p_output_report->rep_ref.report_type = BLE_HIDS_REP_TYPE_OUTPUT;

    p_output_report->sec.wr = SEC_JUST_WORKS;
    p_output_report->sec.rd = SEC_JUST_WORKS;

    hid_info_flags = HID_INFO_FLAG_REMOTE_WAKE_MSK | HID_INFO_FLAG_NORMALLY_CONNECTABLE_MSK;

    memset(&hids_init_obj, 0, sizeof(hids_init_obj));

    hids_init_obj.evt_handler = on_hids_evt;
    hids_init_obj.error_handler = service_error_handler;
    hids_init_obj.is_kb = true;
    hids_init_obj.is_mouse = true;
    hids_init_obj.inp_rep_count = INPUT_REPORT_COUNT;
    hids_init_obj.p_inp_rep_array = inp_rep_array;
    hids_init_obj.outp_rep_count = OUTPUT_REPORT_COUNT;
    hids_init_obj.p_outp_rep_array = out_rep_array;
#if FEATURE_REPORT_COUNT
    hids_init_obj.feature_rep_count = FEATURE_REPORT_COUNT;
    hids_init_obj.p_feature_rep_array = feature_rep_array;
#else
    hids_init_obj.feature_rep_count = 0;
    hids_init_obj.p_feature_rep_array = NULL;
#endif
    hids_init_obj.rep_map.data_len = sizeof(rep_map_data);
    hids_init_obj.rep_map.p_data = rep_map_data;
    hids_init_obj.hid_information.bcd_hid = BASE_USB_HID_SPEC_VERSION;
    hids_init_obj.hid_information.b_country_code = 0;
    hids_init_obj.hid_information.flags = hid_info_flags;
    hids_init_obj.included_services_count = 0;
    hids_init_obj.p_included_services_array = NULL;

    hids_init_obj.rep_map.rd_sec = SEC_JUST_WORKS;
    hids_init_obj.hid_information.rd_sec = SEC_JUST_WORKS;

    hids_init_obj.boot_kb_inp_rep_sec.cccd_wr = SEC_JUST_WORKS;
    hids_init_obj.boot_kb_inp_rep_sec.rd = SEC_JUST_WORKS;

    hids_init_obj.boot_kb_outp_rep_sec.rd = SEC_JUST_WORKS;
    hids_init_obj.boot_kb_outp_rep_sec.wr = SEC_JUST_WORKS;

    hids_init_obj.boot_mouse_inp_rep_sec.cccd_wr = SEC_JUST_WORKS;
    hids_init_obj.boot_mouse_inp_rep_sec.wr = SEC_JUST_WORKS;
    hids_init_obj.boot_mouse_inp_rep_sec.rd = SEC_JUST_WORKS;

    hids_init_obj.protocol_mode_rd_sec = SEC_JUST_WORKS;
    hids_init_obj.protocol_mode_wr_sec = SEC_JUST_WORKS;
    hids_init_obj.ctrl_point_wr_sec = SEC_JUST_WORKS;

    err_code = ble_hids_init(&m_hids, &hids_init_obj);
    MKB_ERROR_CHECK(err_code);
}

/**@brief Function for initializing services that will be used by the application.
 */
static void services_init(void)
{
    dis_init();
    bas_init();
    hids_init();
}

/**@brief Function for handling a Connection Parameters error.
 *
 * @param[in]   nrf_error   Error code containing information about what went wrong.
 */
static void conn_params_error_handler(uint32_t nrf_error)
{
    MKB_ERROR_HANDLER(nrf_error);
}

/**@brief Function for initializing the Connection Parameters module.
 */
static void conn_params_init(void)
{
    ret_code_t err_code;
    ble_conn_params_init_t cp_init;

    memset(&cp_init, 0, sizeof(cp_init));

    cp_init.p_conn_params = NULL;
    cp_init.first_conn_params_update_delay = FIRST_CONN_PARAMS_UPDATE_DELAY;
    cp_init.next_conn_params_update_delay = NEXT_CONN_PARAMS_UPDATE_DELAY;
    cp_init.max_conn_params_update_count = MAX_CONN_PARAM_UPDATE_COUNT;
    cp_init.start_on_notify_cccd_handle = BLE_GATT_HANDLE_INVALID;
    cp_init.disconnect_on_fail = false;
    cp_init.evt_handler = NULL;
    cp_init.error_handler = conn_params_error_handler;

    err_code = ble_conn_params_init(&cp_init);
    MKB_ERROR_CHECK(err_code);
}

/**@brief Function for stop the Connection Parameters module.
 */
static void conn_params_stop(void)
{
    ret_code_t err_code;

    // Stop connection parameter update negotiation.
    // Stop app timers
    err_code = ble_conn_params_stop();
    MKB_ERROR_CHECK(err_code);
}

/**@brief Function for sending sample key presses to the peer.
 *
 * @param[in]   p_key_pattern     Pattern to be sent.
 * @param[in]   key_pattern_len   Pattern length.
 */
ret_code_t mkb_ble_keys_send(uint8_t *p_key_pattern, uint8_t key_pattern_len)
{
    ret_code_t err_code = NRF_ERROR_INTERNAL;
    uint16_t actual_len;

    // if (m_conn_handle != BLE_CONN_HANDLE_INVALID)
    {
        MKB_LOG_INFO("KEY=> [0x%02x][0x%02x][0x%02x][0x%02x][0x%02x][0x%02x][0x%02x][0x%02x]",
                     p_key_pattern[0], p_key_pattern[1], p_key_pattern[2], p_key_pattern[3],
                     p_key_pattern[4], p_key_pattern[5], p_key_pattern[6], p_key_pattern[7]);
        if (!m_in_boot_mode)
        {
            err_code = ble_hids_inp_rep_send(&m_hids,
                                             INPUT_REP_KEYS_INDEX,
                                             INPUT_REP_KEYS_MAX_LEN,
                                             p_key_pattern,
                                             m_conn_handle);
        }
        else
        {
            err_code = ble_hids_boot_kb_inp_rep_send(&m_hids,
                                                     INPUT_REP_KEYS_MAX_LEN,
                                                     p_key_pattern,
                                                     m_conn_handle);
        }

        // An additional notification is needed for release of all keys, therefore check
        // is for actual_len <= key_pattern_len and not actual_len < key_pattern_len.
        // if ((err_code == NRF_ERROR_RESOURCES) && (actual_len <= key_pattern_len))
        if ((err_code == NRF_ERROR_RESOURCES))
        {
            ________DBG_20210831_mkb_ble_send_error("Fail to send error code= 0x%04x, keyis:", err_code);
            ________DBG_20210831_mkb_ble_send_error("KEY=> [0x%02x][0x%02x][0x%02x][0x%02x][0x%02x][0x%02x][0x%02x][0x%02x]",
                                                    p_key_pattern[0], p_key_pattern[1], p_key_pattern[2], p_key_pattern[3],
                                                    p_key_pattern[4], p_key_pattern[5], p_key_pattern[6], p_key_pattern[7]);
            // Buffer enqueue routine return value is not intentionally checked.
            // Rationale: Its better to have a a few keys missing than have a system
            // reset. Recommendation is to work out most optimal value for
            // MAX_BUFFER_ENTRIES to minimize chances of buffer queue full condition
            UNUSED_VARIABLE(buffer_enqueue(&m_hids, p_key_pattern, key_pattern_len, key_pattern_len));
        }

        if ((err_code != NRF_SUCCESS) &&
            (err_code != NRF_ERROR_INVALID_STATE) &&
            (err_code != NRF_ERROR_RESOURCES) &&
            (err_code != NRF_ERROR_BUSY) &&
            (err_code != BLE_ERROR_GATTS_SYS_ATTR_MISSING) &&
            (err_code != BLE_ERROR_INVALID_CONN_HANDLE) // 0x3002
        )
        {
            ________DBG_20210831_mkb_ble_send_error("Fail to send error code= 0x%04x, keyis:", err_code);

            MKB_LOG_ERROR("Fail to send error code= 0x%04x", err_code);
            // MKB_ERROR_HANDLER(err_code);to
        }
    }

    return err_code;
}

/**@brief Function for sending a Mouse Movement.
 *
 * @param[in]   x_delta   Horizontal movement.
 * @param[in]   y_delta   Vertical movement.
 */
void mkb_ble_movement_send(int16_t x_delta, int16_t y_delta)
{
    ret_code_t err_code;

    if (m_in_boot_mode)
    {
        x_delta = MIN(x_delta, 0x00ff);
        y_delta = MIN(y_delta, 0x00ff);

        err_code = ble_hids_boot_mouse_inp_rep_send(&m_hids,
                                                    0x00,
                                                    (int8_t)x_delta,
                                                    (int8_t)y_delta,
                                                    0,
                                                    NULL,
                                                    m_conn_handle);
    }
    else
    {
        uint8_t buffer[INPUT_REP_MOVEMENT_LEN];

        APP_ERROR_CHECK_BOOL(INPUT_REP_MOVEMENT_LEN == 3);

        x_delta = MIN(x_delta, 0x0fff);
        y_delta = MIN(y_delta, 0x0fff);

        buffer[0] = x_delta & 0x00ff;
        buffer[1] = ((y_delta & 0x000f) << 4) | ((x_delta & 0x0f00) >> 8);
        buffer[2] = (y_delta & 0x0ff0) >> 4;

        err_code = ble_hids_inp_rep_send(&m_hids,
                                         INPUT_REP_MOVEMENT_INDEX,
                                         INPUT_REP_MOVEMENT_LEN,
                                         buffer,
                                         m_conn_handle);
    }

    if ((err_code != NRF_SUCCESS) &&
        (err_code != NRF_ERROR_INVALID_STATE) &&
        (err_code != NRF_ERROR_RESOURCES) &&
        (err_code != NRF_ERROR_BUSY) &&
        (err_code != BLE_ERROR_GATTS_SYS_ATTR_MISSING) &&
        (err_code != NRF_ERROR_FORBIDDEN) &&
        (err_code != BLE_ERROR_INVALID_CONN_HANDLE) // 0x3002
    )
    {
        ________DBG_20210831_mkb_ble_send_error("Fail to send error code= 0x%04x", err_code);

        MKB_LOG_ERROR("Fail to send error code= 0x%04x", err_code);
        // MKB_ERROR_HANDLER(err_code);
    }
}

void mkb_ble_button_send(uint8_t button)
{
    ret_code_t err_code;

    if (m_in_boot_mode)
    {
        err_code = ble_hids_boot_mouse_inp_rep_send(&m_hids,
                                                    button,
                                                    0,
                                                    0,
                                                    0,
                                                    NULL,
                                                    m_conn_handle);
    }
    else
    {
        uint8_t buffer[INPUT_REP_BUTTONS_LEN];

        APP_ERROR_CHECK_BOOL(INPUT_REP_BUTTONS_LEN == 3);

        buffer[0] = button;
        buffer[1] = 0;
        buffer[2] = 0;

        err_code = ble_hids_inp_rep_send(&m_hids,
                                         INPUT_REP_BUTTONS_INDEX,
                                         INPUT_REP_BUTTONS_LEN,
                                         buffer,
                                         m_conn_handle);
    }

    if ((err_code != NRF_SUCCESS) &&
        (err_code != NRF_ERROR_INVALID_STATE) &&
        (err_code != NRF_ERROR_RESOURCES) &&
        (err_code != NRF_ERROR_BUSY) &&
        (err_code != BLE_ERROR_GATTS_SYS_ATTR_MISSING) &&
        (err_code != NRF_ERROR_FORBIDDEN) &&
        (err_code != BLE_ERROR_INVALID_CONN_HANDLE) // 0x3002
    )
    {
        MKB_LOG_ERROR("Fail to send error code= 0x%04x", err_code);
        // MKB_ERROR_HANDLER(err_code);
    }
}

void mkb_ble_scroll_send(uint8_t button, uint8_t hscroll, uint8_t scroll)
{
    ret_code_t err_code;

    if (m_in_boot_mode)
    {
        err_code = ble_hids_boot_mouse_inp_rep_send(&m_hids,
                                                    button,
                                                    MIN(scroll, 0xff),
                                                    0,
                                                    0,
                                                    NULL,
                                                    m_conn_handle);
    }
    else
    {
        uint8_t buffer[INPUT_REP_BUTTONS_LEN];

        APP_ERROR_CHECK_BOOL(INPUT_REP_BUTTONS_LEN == 3);

        buffer[0] = button;
        buffer[1] = MIN(scroll, 0xff);
        buffer[2] = MIN(hscroll, 0xff);

        err_code = ble_hids_inp_rep_send(&m_hids,
                                         INPUT_REP_BUTTONS_INDEX,
                                         INPUT_REP_BUTTONS_LEN,
                                         buffer,
                                         m_conn_handle);
    }

    if ((err_code != NRF_SUCCESS) &&
        (err_code != NRF_ERROR_INVALID_STATE) &&
        (err_code != NRF_ERROR_RESOURCES) &&
        (err_code != NRF_ERROR_BUSY) &&
        (err_code != BLE_ERROR_GATTS_SYS_ATTR_MISSING) &&
        (err_code != NRF_ERROR_FORBIDDEN) &&
        (err_code != BLE_ERROR_INVALID_CONN_HANDLE) // 0x3002
    )
    {
        MKB_LOG_ERROR("Fail to send error code= 0x%04x", err_code);
        // MKB_ERROR_HANDLER(err_code);
    }
}

void mkb_ble_consumer_send(uint8_t *p_data, uint8_t len)
{
    ret_code_t err_code;

    if (!m_in_boot_mode)
    {
        if (INPUT_REP_CONSUMER_LEN == 3)
            MKB_LOG_INFO("Consumer: 0x%02x, 0x%02x, 0x%02x", p_data[0], p_data[1], p_data[2]);
        else if (INPUT_REP_CONSUMER_LEN == 1)
            MKB_LOG_INFO("Consumer: 0x%02x", p_data[0]);

        err_code = ble_hids_inp_rep_send(&m_hids,
                                         INPUT_REP_CONSUMER_INDEX,
                                         INPUT_REP_CONSUMER_LEN,
                                         p_data,
                                         m_conn_handle);
    }

    if ((err_code != NRF_SUCCESS) &&
        (err_code != NRF_ERROR_INVALID_STATE) &&
        (err_code != NRF_ERROR_RESOURCES) &&
        (err_code != NRF_ERROR_BUSY) &&
        (err_code != BLE_ERROR_GATTS_SYS_ATTR_MISSING) &&
        (err_code != NRF_ERROR_FORBIDDEN) &&
        (err_code != BLE_ERROR_INVALID_CONN_HANDLE) // 0x3002
    )
    {
        MKB_LOG_ERROR("Fail to send error code= 0x%04x", err_code);
        // MKB_ERROR_HANDLER(err_code);
    }
}

void mkb_ble_touchpad_send(uint8_t *p_data, uint8_t len)
{
#if MKB_BLE_TOUCH_DEVICE_ENABLED
    ret_code_t err_code;

    if (!m_in_boot_mode)
    {
        //        MKB_LOG_INFO("touchpad: 0x%02x, 0x%02x, 0x%02x", p_data[0], p_data[1], p_data[2]);
        err_code = ble_hids_inp_rep_send(&m_hids,
                                         INPUT_REP_TOUCH_INDEX,
                                         INPUT_REP_TOUCH_LEN,
                                         p_data,
                                         m_conn_handle);
    }

    if ((err_code != NRF_SUCCESS) &&
        (err_code != NRF_ERROR_INVALID_STATE) &&
        (err_code != NRF_ERROR_RESOURCES) &&
        (err_code != NRF_ERROR_BUSY) &&
        (err_code != BLE_ERROR_GATTS_SYS_ATTR_MISSING) &&
        (err_code != NRF_ERROR_FORBIDDEN) &&
        (err_code != BLE_ERROR_INVALID_CONN_HANDLE) // 0x3002
    )
    {
        MKB_LOG_ERROR("Fail to send error code= 0x%04x", err_code);
        // MKB_ERROR_HANDLER(err_code);
    }
#endif
}

void mkb_ble_device_name_set(uint8_t *p_name)
{
    ret_code_t err_code;
    ble_gap_conn_sec_mode_t sec_mode;

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&sec_mode);

    err_code = sd_ble_gap_device_name_set(&sec_mode,
                                          (const uint8_t *)p_name,
                                          strlen(p_name));
    MKB_ERROR_CHECK(err_code);

    m_ble_device_name = p_name;
}

void mkb_ble_disconnect(CHANNEL_ID_t ch)
{
    ret_code_t err_code;
    MKB_DB_CH_t *p_ch = mkb_db_ch_get();
    CHANNEL_ID_t ch_id = mkb_db_ch_id_get();

    if (p_ch->conn_handle[ch] != BLE_CONN_HANDLE_INVALID)
    {
        MKB_LOG_INFO("CH(%d): Disconnect the original connection handle(%d)", ch, p_ch->conn_handle[ch]);

        err_code = sd_ble_gap_disconnect(p_ch->conn_handle[ch],
                                         BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
        // MKB_ERROR_CHECK(err_code);
    }
}

bool mkb_ble_conn_state(CHANNEL_ID_t ch)
{
    MKB_DB_CH_t *p_ch = mkb_db_ch_get();

    return (p_ch->conn_handle[ch] != BLE_CONN_HANDLE_INVALID);
}

/**@brief mokibo ble initialize */
void mkb_ble_init(void)
{
    buffer_init();

    // SoftDevice and event handling initialization
    ble_stack_init();

    // Initializing GAP parameters
    gap_params_init();

    // Initializing GATT module.
    gatt_init();
    services_init();
    conn_params_init();

    mkb_ble_pm_init();
    mkb_ble_adv_init();

    MKB_LOG_INFO("Initialization Success!");
}

void mkb_ble_start(void)
{
    MKB_DB_CH_t *p_ch = mkb_db_ch_get();
    CHANNEL_ID_t ch_id = mkb_db_ch_id_get();

#if (defined(MKB_KEYBOARD_STARTUP_ENABLED) && MKB_KEYBOARD_STARTUP_ENABLED)
    if (mkb_db_erase_bonds_get() == true)
    {
        mkb_db_erase_bonds_set(false);
        mkb_ble_pm_delete_bonds();
    }
    else
#else
    {
        if ((p_ch->config.info[ch_id].peer_id != PM_PEER_ID_INVALID))
            mkb_ble_adv_start(ch_id, ADV_MODE_DIRECTED);
    }
#endif //(defined(MKB_KEYBOARD_STARTUP_ENABLED) && MKB_KEYBOARD_STARTUP_ENABLED)
}

bool mkb_ble_hid_event_handler(mkb_event_t *p_event)
{
    ASSERT(p_event != NULL);

    switch (p_event->type)
    {
#if (defined(MKB_KEYBOARD_ENABLED) && MKB_KEYBOARD_ENABLED)
    case EVT_HID_INPUT_KEYBOARD:
        ________DBG_20210903_event_process_test("mkb_ble_keys_send(p_event->hid.data:%d, p_event->hid.len:%d);",
                                                p_event->hid.data,
                                                p_event->hid.len);
        mkb_ble_keys_send(p_event->hid.data, p_event->hid.len);
        break;
    case EVT_HID_INPUT_BUTTON:
        ________DBG_20210903_event_process_test("mkb_ble_button_send(p_event->hid.data[0]:%d);;",
                                                p_event->hid.data[0]);
        mkb_ble_button_send(p_event->hid.data[0]);

        break;
    case EVT_HID_INPUT_CONSUMER:
        ________DBG_20210903_event_process_test("mkb_ble_consumer_send(p_event->hid.data:%d, p_event->hid.len:%d);",
                                                p_event->hid.data, p_event->hid.len);
        mkb_ble_consumer_send(p_event->hid.data, p_event->hid.len);
        break;
#endif // (defined(MKB_KEYBOARD_ENABLED) && MKB_KEYBOARD_ENABLED)
    }

    return false;
}

void mkb_ble_auth_key_response(uint8_t *keys)
{
    ret_code_t err_code;
    MKB_DB_CH_t *p_ch = mkb_db_ch_get();
    CHANNEL_ID_t ch_id = mkb_db_ch_id_get();

    err_code = sd_ble_gap_auth_key_reply(p_ch->conn_handle[ch_id], BLE_GAP_AUTH_KEY_TYPE_PASSKEY, keys); // 6-key authentication
    MKB_ERROR_CHECK(err_code);
}

void mkb_ble_adv_stop_request(void)
{
    m_ble_adv_stop_req = true;
}

#if (defined(NRF_PWR_MGMT_ENABLED) && NRF_PWR_MGMT_ENABLED)
static bool mkb_ble_shutdown(nrf_pwr_mgmt_evt_t event)
{
    MKB_LOG_INFO("Shutdown prepare");

    ble_stack_disable();
    conn_params_stop();
    app_timer_stop_all();

#if defined(BOARD_PCA10056) || defined(BOARD_PCA10040)
    ret_code_t err_code = bsp_indication_set(BSP_INDICATE_IDLE);
    MKB_ERROR_CHECK(err_code);
#endif // defined(BOARD_PCA10056)

    return true;
}

NRF_PWR_MGMT_HANDLER_REGISTER(mkb_ble_shutdown, SHUTDOWN_PRIORITY_DEFAULT);
#endif /* NRF_PWR_MGMT_ENABLED */

#endif // (defined(MKB_BLE_ENABLED) && MKB_BLE_ENABLED)
