/*
 * ===============================================================================
 * Copyright (C) 2017-2019
 * INNOPRESSO INC. All Rights Reserved
 *
 * The content of this file is copyrighted by INNOPRESSO or its
 * affiliated, associated or related companies. All copyrights and other
 * associated intellectual property rights related thereto are expressly
 * reserved by INNOPRESSO INC.
 * ===============================================================================
 */

#include <stdbool.h>
#include <stdint.h>
#include <stdarg.h>

#include "sdk_common.h"

#include "nrf_assert.h"
#include "nrf_error.h"
#include "nrf_delay.h"
#include "nrf_cli.h"
#include "nrf_balloc.h"
#include "nrf_pwr_mgmt.h"

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

#include "app_error.h"
#include "app_timer.h"
#include "app_scheduler.h"

#define MKB_LOG_LEVEL MKB_USB_LOG_LEVEL
#include "mkb_common.h"
#include "mkb_db.h"
#include "mkb_event.h"
#include "mkb_keyboard.h"
#include "mkb_usb.h"

#if (defined(MKB_USB_ENABLED) && MKB_USB_ENABLED)

#include "nrf_drv_usbd.h"

#include "app_usbd.h"
#include "app_usbd_core.h"
#include "app_usbd_hid_kbd.h"
#include "app_usbd_hid_mouse.h"
#include "app_usbd_hid_generic.h"

/**
 * @brief Propagate SET_PROTOCOL command to other HID instance
 */
#define PROPAGATE_PROTOCOL 0

#define USB_MOUSE_ENABLED 1
#define USB_KEYBOARD_ENABLED 1
#define USB_CONSUMER_ENABLED 1

#define INPUT_REP_CONSUMER_LEN 3

/**
 * @brief USB composite interfaces
 */
#define APP_USBD_INTERFACE_KBD 0
#define APP_USBD_INTERFACE_MOUSE 1
#define APP_USBD_INTERFACE_CONSUMER 2

/**
 * @brief Mouse button count
 */
#define CONFIG_MOUSE_BUTTON_COUNT 2

/**
 * @brief Example of USB HID consummer report descriptor.
 *
 */
#if USB_CONSUMER_ENABLED
#if (INPUT_REP_CONSUMER_LEN == 3)
#define APP_USBD_HID_CONSUMER_REPORT_DSC                                           \
    {                                                                              \
        0x05, 0x0C,           /* Usage Page (Consumer),                 */         \
            0x09, 0x01,       /* Usage (Consumer Control),              */         \
            0xA1, 0x01,       /*  Collection (Application),             */         \
            0x75, 12,         /*      Report Size (CONSUMER_CTRL_IN_REP_SIZE)   */ \
            0x95, 2,          /*      Report Count (CONSUMER_CTRL_IN_REP_COUNT) */ \
            0x15, 0x00,       /*      Logical Minimum (0)                       */ \
            0x26, 0xFF, 0x07, /*      Logical Maximum (2047)                    */ \
            0x19, 0x00,       /*      Usage Minimum (0)                         */ \
            0x2A, 0xFF, 0x07, /*      Usage Maximum (2047)                      */ \
            0x81, 0x00,       /*      Input (Data, Ary, Abs)                    */ \
            0xC0,             /* End Collection                             */     \
    }
#else
#define APP_USBD_HID_CONSUMER_REPORT_DSC                                       \
    {                                                                          \
        0x05, 0x0C,           /* Usage Page (Consumer),                 */     \
            0x09, 0x01,       /* Usage (Consumer Control),              */     \
            0xA1, 0x01,       /*  Collection (Application),             */     \
            0x15, 0x00,       /* Logical minimum (0)                    */     \
            0x25, 0x01,       /* Logical maximum (1)                    */     \
            0x75, 0x01,       /* Report Size (1)                        */     \
            0x95, 0x01,       /* Report Count (1)                       */     \
            0x09, 0xE2,       /* Usage (Mute)                           */     \
            0x81, 0x06,       /* Input (Data,Value,Relative,Bit Field)  */     \
            0x09, 0xEA,       /* Usage (Volume Down)                    */     \
            0x81, 0x06,       /* Input (Data,Value,Relative,Bit Field)  */     \
            0x09, 0xE9,       /* Usage (Volume Up)                      */     \
            0x81, 0x06,       /* Input (Data,Value,Relative,Bit Field)  */     \
            0x09, 0xCD,       /* Usage (Play/Pause)                     */     \
            0x81, 0x06,       /* Input (Data,Value,Relative,Bit Field)  */     \
            0x09, 0xB5,       /* Usage (Scan Next Track)                */     \
            0x81, 0x06,       /* Input (Data,Value,Relative,Bit Field)  */     \
            0x09, 0xB6,       /* Usage (Scan Previous Track)            */     \
            0x81, 0x06,       /* Input (Data,Value,Relative,Bit Field)  */     \
            0x0A, 0x25, 0x02, /* Usage (AC Forward)                     */     \
            0x81, 0x06,       /* Input (Data,Value,Relative,Bit Field)  */     \
            0x0A, 0x24, 0x02, /* Usage (AC Back)                        */     \
            0x81, 0x06,       /* Input (Data,Value,Relative,Bit Field)  */     \
            0xC0,             /* End Collection                             */ \
    }
#endif

/**
 * @brief Number of reports defined in report descriptor.
 */
#define REPORT_IN_QUEUE_SIZE 1

#define REPORT_OUT_MAXSIZE 0

/**
 * @brief Feature report maximum size. HID generic class will reserve
 *        this buffer size + 1 memory space.
 */
#define REPORT_FEATURE_MAXSIZE 31

/**
 * @brief HID generic class endpoint number.
 * */
#define HID_GENERIC_EPIN NRF_DRV_USBD_EPIN3

/**
 * @brief HID generic class endpoints count.
 * */
#define HID_GENERIC_EP_COUNT 1

/**
 * @brief List of HID generic class endpoints.
 * */
#define ENDPOINT_LIST() \
    (                   \
        HID_GENERIC_EPIN)

/**
 * @brief User event handler, HID consumer
 */
static void hid_consumer_user_ev_handler(app_usbd_class_inst_t const *p_inst,
                                         app_usbd_hid_user_event_t event);
/**
 * @brief Reuse HID consumer report descriptor for HID generic class
 */
APP_USBD_HID_GENERIC_SUBCLASS_REPORT_DESC(consumer_desc, APP_USBD_HID_CONSUMER_REPORT_DSC);

static const app_usbd_hid_subclass_desc_t *consumer_reps[] = {&consumer_desc};

/**
 * @brief Global HID generic instance
 */
APP_USBD_HID_GENERIC_GLOBAL_DEF(m_app_hid_generic,
                                APP_USBD_INTERFACE_CONSUMER,
                                hid_consumer_user_ev_handler,
                                ENDPOINT_LIST(),
                                consumer_reps,
                                REPORT_IN_QUEUE_SIZE,
                                REPORT_OUT_MAXSIZE,
                                REPORT_FEATURE_MAXSIZE,
                                APP_USBD_HID_SUBCLASS_BOOT,
                                APP_USBD_HID_PROTO_GENERIC);

#endif

/**
 * @brief User event handler, HID keyboard
 */
#if USB_KEYBOARD_ENABLED
static void hid_kbd_user_ev_handler(app_usbd_class_inst_t const *p_inst,
                                    app_usbd_hid_user_event_t event);

/**
 * @brief Global HID keyboard instance
 */
APP_USBD_HID_KBD_GLOBAL_DEF(m_app_hid_kbd,
                            APP_USBD_INTERFACE_KBD,
                            NRF_DRV_USBD_EPIN2,
                            hid_kbd_user_ev_handler,
                            APP_USBD_HID_SUBCLASS_BOOT);
#endif

/**
 * @brief User event handler, HID mouse
 */
#if USB_MOUSE_ENABLED
static void hid_mouse_user_ev_handler(app_usbd_class_inst_t const *p_inst,
                                      app_usbd_hid_user_event_t event);

/**
 * @brief Global HID mouse instance
 */
APP_USBD_HID_MOUSE_GLOBAL_DEF(m_app_hid_mouse,
                              APP_USBD_INTERFACE_MOUSE,
                              NRF_DRV_USBD_EPIN1,
                              CONFIG_MOUSE_BUTTON_COUNT,
                              hid_mouse_user_ev_handler,
                              APP_USBD_HID_SUBCLASS_BOOT);
#endif

static bool m_is_charing = false;
static bool m_usb_run = false;

#if USB_KEYBOARD_ENABLED
static void kbd_status(void)
{
    if (app_usbd_hid_kbd_led_state_get(&m_app_hid_kbd, APP_USBD_HID_KBD_LED_CAPS_LOCK))
    {
        // bsp_board_led_on(LED_CAPSLOCK);
        keyboard_caps_lock_set(true);
    }
    else
    {
        // bsp_board_led_off(LED_CAPSLOCK);
        keyboard_caps_lock_set(false);
    }
}
#endif

#if USB_MOUSE_ENABLED
/**
 * @brief Class specific event handler.
 *
 * @param p_inst    Class instance.
 * @param event     Class specific event.
 * */
static void hid_mouse_user_ev_handler(app_usbd_class_inst_t const *p_inst,
                                      app_usbd_hid_user_event_t event)
{
    UNUSED_PARAMETER(p_inst);
    switch (event)
    {
    case APP_USBD_HID_USER_EVT_OUT_REPORT_READY:
        // MKB_LOG_INFO("USB APP_USBD_HID_USER_EVT_OUT_REPORT_READY");
        /* No output report defined for HID mouse.*/
        ASSERT(0);
        break;
    case APP_USBD_HID_USER_EVT_IN_REPORT_DONE:
        // MKB_LOG_INFO("USB APP_USBD_HID_USER_EVT_IN_REPORT_DONE");
        // bsp_board_led_invert(LED_HID_REP);
        break;
    case APP_USBD_HID_USER_EVT_SET_BOOT_PROTO:
        MKB_LOG_INFO("USB APP_USBD_HID_USER_EVT_SET_BOOT_PROTO");
        UNUSED_RETURN_VALUE(hid_mouse_clear_buffer(p_inst));
#if PROPAGATE_PROTOCOL
        hid_kbd_on_set_protocol(&m_app_hid_kbd, APP_USBD_HID_USER_EVT_SET_BOOT_PROTO);
#endif
        break;
    case APP_USBD_HID_USER_EVT_SET_REPORT_PROTO:
        MKB_LOG_INFO("USB APP_USBD_HID_USER_EVT_SET_REPORT_PROTO");
        UNUSED_RETURN_VALUE(hid_mouse_clear_buffer(p_inst));
#if PROPAGATE_PROTOCOL
        hid_kbd_on_set_protocol(&m_app_hid_kbd, APP_USBD_HID_USER_EVT_SET_REPORT_PROTO);
#endif
        break;
    default:
        break;
    }
}
#endif

#if USB_KEYBOARD_ENABLED
/**
 * @brief Class specific event handler.
 *
 * @param p_inst    Class instance.
 * @param event     Class specific event.
 * */
static void hid_kbd_user_ev_handler(app_usbd_class_inst_t const *p_inst,
                                    app_usbd_hid_user_event_t event)
{
    UNUSED_PARAMETER(p_inst);
    switch (event)
    {
    case APP_USBD_HID_USER_EVT_OUT_REPORT_READY:
        // MKB_LOG_INFO("USB APP_USBD_HID_USER_EVT_OUT_REPORT_READY");
        /* Only one output report IS defined for HID keyboard class. Update LEDs state. */
        // bsp_board_led_invert(LED_HID_REP);
        kbd_status();
        break;
    case APP_USBD_HID_USER_EVT_IN_REPORT_DONE:
        // MKB_LOG_INFO("USB APP_USBD_HID_USER_EVT_IN_REPORT_DONE");
        // bsp_board_led_invert(LED_HID_REP);
        break;
    case APP_USBD_HID_USER_EVT_SET_BOOT_PROTO:
        MKB_LOG_INFO("USB APP_USBD_HID_USER_EVT_SET_BOOT_PROTO");
        UNUSED_RETURN_VALUE(hid_kbd_clear_buffer(p_inst));
#if PROPAGATE_PROTOCOL
        hid_mouse_on_set_protocol(&m_app_hid_mouse, APP_USBD_HID_USER_EVT_SET_BOOT_PROTO);
#endif
        break;
    case APP_USBD_HID_USER_EVT_SET_REPORT_PROTO:
        MKB_LOG_INFO("USB APP_USBD_HID_USER_EVT_SET_REPORT_PROTO");
        UNUSED_RETURN_VALUE(hid_kbd_clear_buffer(p_inst));
#if PROPAGATE_PROTOCOL
        hid_mouse_on_set_protocol(&m_app_hid_mouse, APP_USBD_HID_USER_EVT_SET_REPORT_PROTO);
#endif
        break;
    default:
        break;
    }
}
#endif

#if USB_CONSUMER_ENABLED
/**
 * @brief Class specific event handler.
 *
 * @param p_inst    Class instance.
 * @param event     Class specific event.
 * */
static void hid_consumer_user_ev_handler(app_usbd_class_inst_t const *p_inst,
                                         app_usbd_hid_user_event_t event)
{
    switch (event)
    {
    case APP_USBD_HID_USER_EVT_OUT_REPORT_READY:
    {
        /* No output report defined for this example.*/
        // ASSERT(0);
        break;
    }
    case APP_USBD_HID_USER_EVT_IN_REPORT_DONE:
    {
        // m_report_pending = false;
        // bsp_board_led_invert(LED_HID_REP_IN);
        break;
    }
    case APP_USBD_HID_USER_EVT_SET_BOOT_PROTO:
    {
        UNUSED_RETURN_VALUE(hid_generic_clear_buffer(p_inst));
        NRF_LOG_INFO("SET_BOOT_PROTO");
        break;
    }
    case APP_USBD_HID_USER_EVT_SET_REPORT_PROTO:
    {
        UNUSED_RETURN_VALUE(hid_generic_clear_buffer(p_inst));
        NRF_LOG_INFO("SET_REPORT_PROTO");
        break;
    }
    default:
        break;
    }
}
#endif

/**
 * @brief USBD library specific event handler.
 *
 * @param event     USBD library event.
 * */
static void mkb_usbd_user_ev_handler(app_usbd_event_type_t event)
{
    MKB_DB_CH_t *p_ch = mkb_db_ch_get();
    CHANNEL_ID_t ch_id = mkb_db_ch_id_get();

    //전원꽂으면 8 10 15 13 2
    //전원빼면 1 15 3 9 15 14
    switch (event)
    {
        //?
    case APP_USBD_EVT_DRV_SOF: // 0
        break;
    case APP_USBD_EVT_DRV_SUSPEND:
        MKB_LOG_INFO("USB Suspend");
        ________DBG_20210930_usb_event("APP_USBD_EVT_DRV_SUSPEND:%d", event);

        app_usbd_suspend_req(); // Allow the library to put the peripheral into sleep mode
        // bsp_board_leds_off();

        break;
    case APP_USBD_EVT_DRV_RESUME: // 3
        MKB_LOG_INFO("USB Resume");
        ________DBG_20210930_usb_event("APP_USBD_EVT_DRV_RESUME:%d", event);

        // bsp_board_led_on(LED_USB_START);
#if USB_KEYBOARD_ENABLED
        kbd_status(); /* Restore LED state - during SUSPEND all LEDS are turned off */
#endif
        break;
    case APP_USBD_EVT_STARTED: // 13
        MKB_LOG_INFO("USB Started");
        ________DBG_20210930_usb_event("APP_USBD_EVT_STARTED:%d", event);

        // bsp_board_led_on(LED_USB_START);
        m_usb_run = true;

        if (ch_id == CHANNEL_ID_USB)
            mkb_usb_start(ch_id);
        break;
    case APP_USBD_EVT_STOPPED: // 14
        MKB_LOG_INFO("USB Stoped");
        ________DBG_20211205_battery_test("USB stoped");
        ________DBG_20210930_usb_event("APP_USBD_EVT_STOPPED:%d", event);
        app_usbd_disable();
        // bsp_board_leds_off();

        m_usb_run = false;

        if (ch_id == CHANNEL_ID_USB)
            mkb_usb_stop(ch_id);
        break;
    case APP_USBD_EVT_POWER_DETECTED: // 8
        MKB_LOG_INFO("USB power detected");
        ________DBG_20210930_usb_event("APP_USBD_EVT_POWER_DETECTED:%d", event);

        if (!nrf_drv_usbd_is_enabled())
        {
            app_usbd_enable();
        }

        m_is_charing = true;
        ________DBG_20211205_battery_test("charge started");
        mkb_db_is_bat_charging_set(m_is_charing);
        mkb_event_send(EVT_LED_SUB_BATT_CHECK);
        mkb_event_send(EVT_LED_SUB_SET_CH_OS_CHECK_PUSH_DOWN);
        break;

    case APP_USBD_EVT_POWER_REMOVED: // 9
        MKB_LOG_INFO("USB power removed");
        ________DBG_20210930_usb_event("APP_USBD_EVT_POWER_REMOVED:%d", event);
        ________DBG_20210915_led_callstack("LED_STACK");
        app_usbd_stop();

        ________DBG_20211205_battery_test("charge stoped");
        m_is_charing = false;
        mkb_db_is_bat_charging_set(m_is_charing);
        break;
    case APP_USBD_EVT_POWER_READY: // 10
        ________DBG_20211205_battery_test("charge ready");
        ________DBG_20210930_usb_event("APP_USBD_EVT_POWER_READY:%d", event);
        MKB_LOG_INFO("USB ready");
        ________DBG_20210915_led_callstack("LED_STACK");
        app_usbd_start();
        break;
    default:
        break;
    }
}

void mkb_usb_kbd_modifier(uint8_t data, bool flag)
{
#if USB_KEYBOARD_ENABLED
    app_usbd_hid_kbd_ctx_t *p_kbd_ctx = &m_app_hid_kbd.specific.p_data->ctx;
    ;
    app_usbd_hid_state_flag_clr(&p_kbd_ctx->hid_ctx, APP_USBD_HID_STATE_FLAG_TRANS_IN_PROGRESS);

    if (m_is_charing && m_usb_run)
        app_usbd_hid_kbd_modifier_state_set(&m_app_hid_kbd, data, flag);
#endif
}

void mkb_usb_kbd_control(uint8_t data, bool flag)
{
#if USB_KEYBOARD_ENABLED
    app_usbd_hid_kbd_ctx_t *p_kbd_ctx = &m_app_hid_kbd.specific.p_data->ctx;
    ;
    app_usbd_hid_state_flag_clr(&p_kbd_ctx->hid_ctx, APP_USBD_HID_STATE_FLAG_TRANS_IN_PROGRESS);

    if (m_is_charing && m_usb_run)
        app_usbd_hid_kbd_key_control(&m_app_hid_kbd, data, flag);
#endif
}

void mkb_usb_mouse_button(uint8_t data, bool flag)
{
#if USB_MOUSE_ENABLED

    if (m_is_charing && m_usb_run)
        app_usbd_hid_mouse_button_state(&m_app_hid_mouse, data, flag);
#endif
}

void mkb_usb_mouse_move(int8_t x, int8_t y)
{
#if USB_MOUSE_ENABLED

    if (m_is_charing && m_usb_run)
    {
        app_usbd_hid_mouse_x_move(&m_app_hid_mouse, x);
        app_usbd_hid_mouse_y_move(&m_app_hid_mouse, y);
    }
#endif
}

void mkb_usb_mouse_scroll(int8_t offset)
{
#if USB_MOUSE_ENABLED

    if (m_is_charing && m_usb_run)
        app_usbd_hid_mouse_scroll_move(&m_app_hid_mouse, offset);
#endif
}

void mkb_usb_consumer(uint8_t *p_data, uint8_t size)
{
#if USB_CONSUMER_ENABLED

    if (m_is_charing && m_usb_run)
        app_usbd_hid_generic_in_report_set(&m_app_hid_generic, p_data, size);
#endif
}

void mkb_usb_event_process(void)
{
    app_usbd_event_queue_process();
}

/**@brief mokibo usb initialize */
void mkb_usb_init(void)
{
    ret_code_t err_code;

    static const app_usbd_config_t usbd_config = {
        .ev_state_proc = mkb_usbd_user_ev_handler,
    };

    err_code = app_usbd_init(&usbd_config);
    MKB_ERROR_CHECK(err_code);

#if USB_MOUSE_ENABLED
    app_usbd_class_inst_t const *class_inst_mouse;
    class_inst_mouse = app_usbd_hid_mouse_class_inst_get(&m_app_hid_mouse);
    err_code = app_usbd_class_append(class_inst_mouse);
    MKB_ERROR_CHECK(err_code);
#endif

#if USB_KEYBOARD_ENABLED
    app_usbd_class_inst_t const *class_inst_kbd;
    class_inst_kbd = app_usbd_hid_kbd_class_inst_get(&m_app_hid_kbd);
    err_code = app_usbd_class_append(class_inst_kbd);
    MKB_ERROR_CHECK(err_code);
#endif

#if USB_CONSUMER_ENABLED
    app_usbd_class_inst_t const *class_inst_generic;
    class_inst_generic = app_usbd_hid_generic_class_inst_get(&m_app_hid_generic);
    err_code = app_usbd_class_append(class_inst_generic);
    MKB_ERROR_CHECK(err_code);
#endif

    err_code = app_usbd_power_events_enable();
    MKB_ERROR_CHECK(err_code);

    MKB_LOG_INFO("Initialization Success!");
}

/**@brief mokibo usb start */
void mkb_usb_start(CHANNEL_ID_t ch)
{
    MKB_DB_CH_t *p_ch = mkb_db_ch_get();

    if (mkb_usb_state() && p_ch->conn_handle[ch] == BLE_CONN_HANDLE_INVALID)
    {
        MKB_LOG_INFO("Start request");

        p_ch->conn_handle[ch] = 0;
    }
}

/**@brief mokibo usb stop */
void mkb_usb_stop(CHANNEL_ID_t ch)
{
    MKB_DB_CH_t *p_ch = mkb_db_ch_get();

    if (mkb_usb_state() && p_ch->conn_handle[ch] != BLE_CONN_HANDLE_INVALID)
    {
        MKB_LOG_INFO("Stop request");

        p_ch->conn_handle[ch] = BLE_CONN_HANDLE_INVALID;
    }
}

bool mkb_usb_state(void)
{
    return (m_is_charing && m_usb_run) ? true : false;
}

bool mkb_usb_conn_state(CHANNEL_ID_t ch)
{
    MKB_DB_CH_t *p_ch = mkb_db_ch_get();

    return (p_ch->conn_handle[ch] != BLE_CONN_HANDLE_INVALID);
}

#if (defined(NRF_PWR_MGMT_ENABLED) && NRF_PWR_MGMT_ENABLED)
static bool mkb_usb_shutdown(nrf_pwr_mgmt_evt_t event)
{

    return true;
}

NRF_PWR_MGMT_HANDLER_REGISTER(mkb_usb_shutdown, SHUTDOWN_PRIORITY_DEFAULT);
#endif /* NRF_PWR_MGMT_ENABLED */

#endif // (defined(MKB_USB_ENABLED) && MKB_USB_ENABLED)