/*
 * ===============================================================================
 * Copyright (C) 2017-2019
 * INNOPRESSO INC. All Rights Reserved
 *
 * The content of this file is copyrighted by INNOPRESSO or its
 * affiliated, associated or related companies. All copyrights and other
 * associated intellectual property rights related thereto are expressly
 * reserved by INNOPRESSO INC.
 * ===============================================================================
 */

#include <stdbool.h>
#include <stdint.h>
#include <stdarg.h>

#include "sdk_common.h"

#include "nrf_assert.h"
#include "nrf_error.h"
#include "nrf_delay.h"
#include "nrf_cli.h"
#include "nrf_balloc.h"
#include "nrf_pwr_mgmt.h"

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

#include "app_error.h"
#include "app_timer.h"
#include "app_scheduler.h"

#define MKB_LOG_LEVEL MKB_DB_LOG_LEVEL
#include "mkb_common.h"

#if (defined(MKB_DB_ENABLED) && MKB_DB_ENABLED)
#include "mkb_db.h"
#include "mkb_util.h"

#if (defined(MKB_FDS_ENABLED) && MKB_FDS_ENABLED)
#include "mkb_fds.h"
#else
#error "You have to enable fds at mkb_config.h!"
#endif //(defined(MKB_FDS_ENABLED) && MKB_FDS_ENABLED)

#if (defined(MKB_LED_ENABLED) && MKB_LED_ENABLED)
#include "mkb_led.h"
#endif //(defined(MKB_LED_ENABLED) && MKB_LED_ENABLED)

#include "drv_pixart.h"

static MKB_DB_CONFIG_t mkb_db_cfg;
static MKB_DB_CH_t mkb_db_ch;
static MKB_DB_WORK_t mkb_db_work;

void mkb_bd_config_default_set(void)
{
    mkb_db_cfg.magic_code = 0xDEADBADA;
    memset(mkb_db_cfg.device_name, 0, MKB_DEVICE_NAME_SIZE);
    strcpy(mkb_db_cfg.device_name, MKB_DEVICE_NAME);
    memset(mkb_db_cfg.model_name, 0, MKB_MODEL_NAME_SIZE);
    strcpy(mkb_db_cfg.model_name, MKB_MODEL_NAME);
    mkb_db_cfg.aamode = PIXART_AAMODE_TYPE_RIGHT;
}

void mkb_bd_ch_default_set(void)
{
    CHANNEL_INFO_t *p_ch_info;

    for (CHANNEL_ID_t i = 0; i < CHANNEL_ID_MAX; i++)
    {
        p_ch_info = &mkb_db_ch.config.info[i];

        switch (i)
        {
#if (defined(MKB_BLE_BT1_ENABLED) && MKB_BLE_BT1_ENABLED)
        case CHANNEL_ID_BT1:
            p_ch_info->type = CHANNEL_TYPE_BT;
            p_ch_info->state = false;
            p_ch_info->peer_id = PM_PEER_ID_INVALID;
            break;
#endif

#if (defined(MKB_BLE_BT2_ENABLED) && MKB_BLE_BT2_ENABLED)
        case CHANNEL_ID_BT2:
            p_ch_info->type = CHANNEL_TYPE_BT;
            p_ch_info->state = false;
            p_ch_info->peer_id = PM_PEER_ID_INVALID;
            break;
#endif

#if (defined(MKB_BLE_BT3_ENABLED) && MKB_BLE_BT3_ENABLED)
        case CHANNEL_ID_BT3:
            p_ch_info->type = CHANNEL_TYPE_BT;
            p_ch_info->state = false;
            p_ch_info->peer_id = PM_PEER_ID_INVALID;
            break;
#endif

#if !(defined(BOARD_MKB10040)) //사용안함
        case CHANNEL_ID_USB:
            p_ch_info->type = CHANNEL_TYPE_USB;
            p_ch_info->state = false;
            p_ch_info->peer_id = PM_PEER_ID_INVALID;
            break;
#endif
        }

        p_ch_info->id = i;
        p_ch_info->central = CENTRAL_IOS;
        mkb_util_get_mac_address(i, &p_ch_info->own_addr);
    }
}

char *mkb_db_get_channel_type_str(CHANNEL_TYPE_t type)
{
    switch (type)
    {
    case CHANNEL_TYPE_BT:
        return "BT";
    case CHANNEL_TYPE_USB:
        return "USB";
    default:
        return "NONE";
    }
}

char *mkb_db_get_central_type_str(CENTRAL_TYPE_t type)
{
    switch (type)
    {
    case CENTRAL_WINDOWS:
        return "MICROSOFT";
    case CENTRAL_MAC:
        return "MAC";
    case CENTRAL_IOS:
        return "IOS";
    case CENTRAL_ANDROID:
        return "ANDROID";
    case CENTRAL_DEX:
        return "DEX";
    case CENTRAL_LINUX:
        return "LINUX";
    default:
        return "NONE";
    }
}

void mkb_db_default_set(void)
{
    mkb_bd_config_default_set();
    mkb_bd_ch_default_set();
}

void mkb_bd_ch_init(void)
{
    CHANNEL_LED_t *p_ch_led;

    for (CHANNEL_ID_t i = 0; i < CHANNEL_ID_MAX; i++)
    {
        mkb_db_ch.id_prev = mkb_db_ch.config.id;
        mkb_db_ch.conn_handle[i] = BLE_CONN_HANDLE_INVALID;

        p_ch_led = &mkb_db_ch.led[i];

        switch (i)
        {
#if (defined(MKB_BLE_BT1_ENABLED) && MKB_BLE_BT1_ENABLED)
        case CHANNEL_ID_BT1:
#if (defined(MKB_LED_ENABLED) && MKB_LED_ENABLED)
            p_ch_led->disconnect = LED_IND_CH1_DISCONNECT;
            p_ch_led->touch = LED_IND_CH1_TOUCH;
            p_ch_led->touchlock = LED_IND_CH1_TOUCHLOCK;
            p_ch_led->button = LED_IND_CH1_BUTTON;
            p_ch_led->connect = LED_IND_CH1_CONNECT;
            p_ch_led->select = LED_IND_CH1_SELECT;
            p_ch_led->adv_direct = LED_IND_CH1_ADV_DIRECT;
            p_ch_led->adv_whitelist = LED_IND_CH1_ADV_WHITELIST;
            p_ch_led->adv_fast = LED_IND_CH1_ADV_FAST;
            p_ch_led->adv_slow = LED_IND_CH1_ADV_SLOW;
            p_ch_led->adv_idle = LED_IND_CH1_ADV_IDLE;
#endif //(defined(MKB_LED_ENABLED) && MKB_LED_ENABLED)
            break;
#endif

#if (defined(MKB_BLE_BT2_ENABLED) && MKB_BLE_BT2_ENABLED)
        case CHANNEL_ID_BT2:
#if (defined(MKB_LED_ENABLED) && MKB_LED_ENABLED)
            p_ch_led->disconnect = LED_IND_CH2_DISCONNECT;
            p_ch_led->touch = LED_IND_CH2_TOUCH;
            p_ch_led->touchlock = LED_IND_CH2_TOUCHLOCK;
            p_ch_led->button = LED_IND_CH2_BUTTON;
            p_ch_led->connect = LED_IND_CH2_CONNECT;
            p_ch_led->select = LED_IND_CH2_SELECT;
            p_ch_led->adv_direct = LED_IND_CH2_ADV_DIRECT;
            p_ch_led->adv_whitelist = LED_IND_CH2_ADV_WHITELIST;
            p_ch_led->adv_fast = LED_IND_CH2_ADV_FAST;
            p_ch_led->adv_slow = LED_IND_CH2_ADV_SLOW;
            p_ch_led->adv_idle = LED_IND_CH2_ADV_IDLE;
#endif //(defined(MKB_LED_ENABLED) && MKB_LED_ENABLED)
            break;
#endif

#if (defined(MKB_BLE_BT3_ENABLED) && MKB_BLE_BT3_ENABLED)
        case CHANNEL_ID_BT3:
#if (defined(MKB_LED_ENABLED) && MKB_LED_ENABLED)
            p_ch_led->disconnect = LED_IND_CH3_DISCONNECT;
            p_ch_led->touch = LED_IND_CH3_TOUCH;
            p_ch_led->touchlock = LED_IND_CH3_TOUCHLOCK;
            p_ch_led->button = LED_IND_CH3_BUTTON;
            p_ch_led->connect = LED_IND_CH3_CONNECT;
            p_ch_led->select = LED_IND_CH3_SELECT;
            p_ch_led->adv_direct = LED_IND_CH3_ADV_DIRECT;
            p_ch_led->adv_whitelist = LED_IND_CH3_ADV_WHITELIST;
            p_ch_led->adv_fast = LED_IND_CH3_ADV_FAST;
            p_ch_led->adv_slow = LED_IND_CH3_ADV_SLOW;
            p_ch_led->adv_idle = LED_IND_CH3_ADV_IDLE;
#endif //(defined(MKB_LED_ENABLED) && MKB_LED_ENABLED)
            break;
#endif

#if !(defined(BOARD_MKB10040)) //사용안함
        case CHANNEL_ID_USB:
#if (defined(MKB_USB_ENABLED) && MKB_USB_ENABLED)
            p_ch_led->disconnect = LED_IND_CH4_DISCONNECT;
            p_ch_led->touch = LED_IND_CH4_TOUCH;
            p_ch_led->touchlock = LED_IND_CH4_TOUCHLOCK;
            p_ch_led->button = LED_IND_CH4_BUTTON;
            p_ch_led->connect = LED_IND_CH4_CONNECT;
            p_ch_led->select = LED_IND_CH4_SELECT;
            p_ch_led->adv_direct = LED_IND_CH4_ADV_DIRECT;
            p_ch_led->adv_whitelist = LED_IND_CH4_ADV_WHITELIST;
            p_ch_led->adv_fast = LED_IND_CH4_ADV_FAST;
            p_ch_led->adv_slow = LED_IND_CH4_ADV_SLOW;
            p_ch_led->adv_idle = LED_IND_CH4_ADV_IDLE;
#endif //(defined(MKB_USB_ENABLED) && MKB_USB_ENABLED)
            break;
#endif
        }
    }
}

void mkb_db_restore_config(void)
{
    if (mkb_fds_config_find() != NRF_SUCCESS)
    {
        MKB_LOG_ERROR("can't find config db from fds");
    }
    else
    {
        MKB_LOG_INFO("find config db from fds");
        if (mkb_fds_config_read(&mkb_db_cfg, sizeof(mkb_db_cfg)) != NRF_SUCCESS)
        {
            MKB_LOG_ERROR("can't read config db");
        }
    }
}

void mkb_db_restore_ch(void)
{
    if (mkb_fds_channel_find() != NRF_SUCCESS)
    {
        MKB_LOG_ERROR("can't find channel db from fds");
    }
    else
    {
        MKB_LOG_INFO("find channel db from fds");
        if (mkb_fds_channel_read(&mkb_db_ch.config, sizeof(MKB_DB_CH_CONFIG_t)) != NRF_SUCCESS)
        {
            MKB_LOG_ERROR("can't read channel db");
        }
    }
}

void mkb_db_restore(void)
{
    mkb_db_restore_config();
    mkb_db_restore_ch();
}

void mkb_db_create_config(void)
{
    if (mkb_fds_config_write(&mkb_db_cfg, sizeof(mkb_db_cfg)) != NRF_SUCCESS)
        MKB_LOG_ERROR("can't create config db");
}

void mkb_db_create_ch(void)
{
    if (mkb_fds_channel_write(&mkb_db_ch.config, sizeof(MKB_DB_CH_CONFIG_t)) != NRF_SUCCESS)
        MKB_LOG_ERROR("can't create channel db");
}

void mkb_db_create(void)
{
    mkb_db_create_config();
    mkb_db_create_ch();
}

void mkb_db_store_config(void)
{
    ret_code_t err_code;
    ________DBG_20211229_fds_test("store_config");
    err_code = mkb_fds_config_update(&mkb_db_cfg, sizeof(mkb_db_cfg));
    if (err_code != NRF_SUCCESS)
    {
        MKB_LOG_ERROR("can't store config db (err:%d)", err_code);
    }
    else
    {
    }
}

void mkb_db_store_ch(void)
{

    ret_code_t err_code;

    ________DBG_20211229_fds_test("mkb_db_store_ch");
    err_code = mkb_fds_channel_update(&mkb_db_ch.config, sizeof(MKB_DB_CH_CONFIG_t));
    if (err_code != NRF_SUCCESS)
    {
        MKB_LOG_ERROR("can't store channel db (err:%d)", err_code);
    }
    else
    {
    }
}

void mkb_db_store(void)
{

    mkb_db_store_config();
    mkb_db_store_ch();
    ________DBG_20211229_fds_test("1");
}

/**@brief mokibo db initialize */
void mkb_db_init(void)
{
    memset(&mkb_db_cfg, 0, sizeof(mkb_db_cfg));
    memset(&mkb_db_ch, 0, sizeof(mkb_db_ch));
    memset(&mkb_db_work, 0, sizeof(mkb_db_work));

    mkb_db_restore();

    if (mkb_db_cfg.magic_code != 0xDEADBADA)
    {
        MKB_LOG_INFO("Create default DB");

        mkb_db_default_set();
        mkb_db_create();
    }

    mkb_db_work.reset_reason = mkb_util_reset_reason_get();

    mkb_db_system_state_set(SYSTEM_STATE_INIT);

    mkb_bd_ch_init();

    MKB_LOG_DEBUG("config read OK! magic code: 0x%08x", mkb_db_cfg.magic_code);
    MKB_LOG_DEBUG("device name: %s, model name: %s", mkb_db_cfg.device_name, mkb_db_cfg.model_name);

    MKB_LOG_DEBUG("current channel: %d, state: %d", mkb_db_ch.config.id, mkb_db_ch.config.info[mkb_db_ch.config.id].state);

    MKB_LOG_INFO("Initialization Success!");
}

#if (defined(MKB_STM8_ENABLED) && MKB_STM8_ENABLED)
void mkb_db_stm8_version_set(uint8_t version)
{
    mkb_db_work.stm8_version.version = version;
}

STM8_VERSION_t mkb_db_stm8_version_get(void)
{
    return mkb_db_work.stm8_version;
}
#endif // (defined(MKB_STM8_ENABLED) && MKB_STM8_ENABLED)

void mkb_db_wait_for_disconnect_set(bool flag)
{
    mkb_db_work.wait_for_disconnect = flag;
}

bool mkb_db_wait_for_disconnect_get(void)
{
    return mkb_db_work.wait_for_disconnect;
}

void mkb_db_erase_bonds_set(bool erase_bonds)
{
    mkb_db_work.erase_bonds = erase_bonds;
}

bool mkb_db_erase_bonds_get(void)
{
    return mkb_db_work.erase_bonds;
}

SYSTEM_STATE_t mkb_db_system_state_get(void)
{
    return mkb_db_work.system_state;
}

void mkb_db_system_state_set(SYSTEM_STATE_t state)
{
    mkb_db_work.system_state = state;
}

bool mkb_db_system_state_is_touch(void)
{
    return (mkb_db_work.system_state == SYSTEM_STATE_TOUCH);
}

uint32_t mkb_db_reset_reason_get(void)
{
    return mkb_db_work.reset_reason;
}

bool mkb_db_is_func_pressed_get()
{
    return mkb_db_work.is_func_pressed;
}

void mkb_db_is_func_pressed_set(bool is_func_pressed)
{
    mkb_db_work.is_func_pressed = is_func_pressed;
}

bool mkb_db_is_bat_charging_get(void)
{
    return mkb_db_work.is_battery_charging;
}

void mkb_db_is_bat_charging_set(bool is_battery_charging)
{
    mkb_db_work.is_battery_charging = is_battery_charging;
}

uint8_t mkb_db_battery_level_get(void)
{
    return mkb_db_work.battery_level;
}

void mkb_db_battery_level_set(uint8_t battery_level)
{
    battery_level = 100 < battery_level ? 100 : battery_level;
    mkb_db_work.battery_level = battery_level;
}

uint32_t mkb_db_last_key_up_time_get(void)
{
    return mkb_db_work.last_key_up_time;
}
void mkb_db_last_key_up_time_set(uint32_t last_key_up_time)
{
    mkb_db_work.last_key_up_time = last_key_up_time;
}

uint32_t mkb_db_last_typing_ready_time_get(void)
{
    return mkb_db_work.last_typing_ready_time;
}
void mkb_db_last_typing_ready_time_set(uint32_t last_typing_ready_time)
{
    mkb_db_work.last_typing_ready_time = last_typing_ready_time;
}

uint32_t mkb_db_last_passkey_updown_time_get(void)
{
    return mkb_db_work.last_passkey_updown_time;
}
void mkb_db_last_passkey_updown_time_set(uint32_t last_passkey_updown_time)
{
    mkb_db_work.last_passkey_updown_time = last_passkey_updown_time;
}

uint32_t mkb_db_last_tap_time_get(void)
{
    return mkb_db_work.last_tap_time;
}
void mkb_db_last_tap_time_set(uint32_t last_tap_up_time)
{
    mkb_db_work.last_tap_time = last_tap_up_time;
}

uint32_t mkb_db_last_gstr_time_get(void)
{
    return mkb_db_work.last_gstr_time;
}
void mkb_db_last_gstr_time_set(uint32_t last_gstr_time)
{
    mkb_db_work.last_gstr_time = last_gstr_time;
}

bool mkb_db_last_multitouch_time_set(uint32_t last_multitouch_time)
{
    mkb_db_work.last_multitouch_time = last_multitouch_time;
}

uint32_t mkb_db_last_multitouch_time_get()
{
    return mkb_db_work.last_multitouch_time;
}

void mkb_db_is_red_click_down_set(bool is_clicked)
{
    if (is_clicked == false)
    {
        mkb_db_work.last_red_up_time = app_timer_cnt_get();
    }

    mkb_db_work.is_red_click_down = is_clicked;
}

bool mkb_db_is_red_click_down_get(void)
{
    return mkb_db_work.is_red_click_down;
}

uint32_t mkb_db_red_click_up_time_get(void)
{
    return mkb_db_work.last_red_up_time;
}

void mkb_db_is_tap_click_down_set(bool is_clicked)
{
    mkb_db_work.is_tap_click_down = is_clicked;
}
bool mkb_db_is_tap_click_down_get(void)
{
    return mkb_db_work.is_tap_click_down;
}

const MKB_DB_WORK_t *mkb_db_work_get(void)
{
    return &mkb_db_work;
}

MKB_DB_CONFIG_t *mkb_db_config_get(void)
{
    return &mkb_db_cfg;
}

MKB_DB_CH_t *mkb_db_ch_get(void)
{
    return &mkb_db_ch;
}

CHANNEL_INFO_t *mkb_db_ch_info_get(CHANNEL_ID_t ch)
{
    return &mkb_db_ch.config.info[ch];
}

void mkb_db_ch_id_set(CHANNEL_ID_t ch)
{
    if (mkb_db_ch.config.id != ch)
    {
        mkb_db_ch.config.id = ch;

        mkb_db_store_ch();
        ________DBG_20211229_fds_test("1");
    }
}

CHANNEL_ID_t mkb_db_ch_id_get(void)
{
    return mkb_db_ch.config.id;
}

void mkb_db_ch_address_set(CHANNEL_ID_t ch)
{
    mkb_util_get_mac_address(ch, &mkb_db_ch.config.info[ch].own_addr);

    mkb_db_store_ch();
    ________DBG_20211229_fds_test("1");
}

void mkb_db_ch_id_prev_set(CHANNEL_ID_t ch)
{
    mkb_db_ch.id_prev = ch;
}

CHANNEL_ID_t mkb_db_ch_id_prev_get(void)
{
    return mkb_db_ch.id_prev;
}

CHANNEL_ID_t mkb_db_ch_id_other(CHANNEL_ID_t ch1, CHANNEL_ID_t ch2)
{
    CHANNEL_ID_t ch;

    for (ch = 0; ch < CHANNEL_ID_MAX; ch++)
    {
        if (ch != ch1 && ch != ch2 && mkb_db_ch.config.info[ch].type == CHANNEL_TYPE_BT)
            return ch;
    }
    return CHANNEL_ID_MAX;
}

void mkb_db_central_type_set(CHANNEL_ID_t ch, CENTRAL_TYPE_t central)
{
    if (mkb_db_ch.config.info[ch].central != central)
    {
        mkb_db_ch.config.info[ch].central = central;

        mkb_db_store_ch();
        ________DBG_20211229_fds_test("1");
    }
}

CENTRAL_TYPE_t mkb_db_central_type_get(CHANNEL_ID_t ch)
{
    return mkb_db_ch.config.info[ch].central;
}

CENTRAL_TYPE_t mkb_db_curr_central_type_get()
{
    return mkb_db_ch.config.info[mkb_db_ch.config.id].central;
}

void mkb_db_aamode_set(pixart_aamode_type_t type)
{
    mkb_db_cfg.aamode = type;
    mkb_db_store_config();
}

pixart_aamode_type_t mkb_db_aamode_get()
{
    if (mkb_db_cfg.aamode != PIXART_AAMODE_TYPE_LEFT && mkb_db_cfg.aamode != PIXART_AAMODE_TYPE_RIGHT)
    {
        mkb_db_aamode_set(PIXART_AAMODE_TYPE_RIGHT);
    }
    return mkb_db_cfg.aamode;
}

#endif // (defined(MKB_DB_ENABLED) && MKB_DB_ENABLED)
