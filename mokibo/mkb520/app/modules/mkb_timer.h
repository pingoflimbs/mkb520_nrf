/*
* ===============================================================================
* Copyright (C) 2017-2019
* INNOPRESSO INC. All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
* ===============================================================================
*/

#include <stdint.h>

extern void mkb_timer_system_time_init(void);
extern uint32_t mkb_util_system_time_get(void);
extern uint32_t m_timer_system_time_delta_get(void);
