/*
* ===============================================================================
* Copyright (C) 2017-2019
* INNOPRESSO INC. All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
* ===============================================================================
*/

#include <stdbool.h>
#include <stdint.h>

#include "sdk_common.h"

#include "nrf_assert.h"
#include "nrf_error.h"
#include "nrf_log.h"
#include "nrf_delay.h"
#include "nrf_cli.h"
#include "nrf_log.h"

#include "fds.h"

#define MKB_LOG_LEVEL MKB_CLI_LOG_LEVEL
#include "mkb_common.h"

#if (defined(MKB_CLI_ENABLED) && MKB_CLI_ENABLED)
#include "mkb_cli.h"
#include "mkb_util.h"

#if (defined(MKB_DB_ENABLED) && MKB_DB_ENABLED)
#include "mkb_db.h"

#if (defined(MKB_BATTERY_ENABLED) && MKB_BATTERY_ENABLED)
#include "mkb_battery.h"
#endif //(defined(MKB_BATTERY_ENABLED) && MKB_BATTERY_ENABLED)

#define DB_HELP "db commands\n" \
                "usage: db"

#define SYSTEM_HELP "print system information\n" \
                    "usage: system"

#define CFG_HELP "print system configuration\n" \
                 "usage: config"

#define CHANNEL_HELP "print channel configuration\n" \
                     "usage: channel"

static void cmd_db_cfg(nrf_cli_t const *p_cli, size_t argc, char **argv)
{
    UNUSED_PARAMETER(argc);
    UNUSED_PARAMETER(argv);

    if (nrf_cli_help_requested(p_cli))
    {
        nrf_cli_help_print(p_cli, NULL, 0);
        return;
    }

    MKB_DB_CONFIG_t *p_cfg = (MKB_DB_CONFIG_t *)mkb_db_config_get();

    nrf_cli_fprintf(p_cli, NRF_CLI_DEFAULT,
                    "magic  code:\t0x%08x\n"
                    "device name:\t%s\n"
                    "model  name:\t%s\n"
                    "Touch  Version: %d.%d (Major.Minor)\n",
                    p_cfg->magic_code,
                    p_cfg->device_name,
                    p_cfg->model_name,
                    p_cfg->touch_fw_major, p_cfg->touch_fw_minor);
}

static void cmd_db_channel(nrf_cli_t const *p_cli, size_t argc, char **argv)
{
    CHANNEL_ID_t ch;
    CHANNEL_INFO_t *p_info;
    char str[256];
    CHANNEL_ID_t ch_current = mkb_db_ch_id_get();

    MKB_DB_CH_t *p_ch = (MKB_DB_CH_t *)mkb_db_ch_get();

    UNUSED_PARAMETER(argc);
    UNUSED_PARAMETER(argv);

    if (nrf_cli_help_requested(p_cli))
    {
        nrf_cli_help_print(p_cli, NULL, 0);
        return;
    }

    nrf_cli_fprintf(p_cli, NRF_CLI_DEFAULT, "**************************************************************************************\n");
    nrf_cli_fprintf(p_cli, NRF_CLI_DEFAULT, "*                                   MOKIBO CHANNEL                                   *\n");
    nrf_cli_fprintf(p_cli, NRF_CLI_DEFAULT, "**************************************************************************************\n");
    nrf_cli_fprintf(p_cli, NRF_CLI_DEFAULT, "* ID | TYPE |  CENTRAL   | STATE |  CONN | PEER ID | PEER      ADDR | OWN       ADDR *\n");
    nrf_cli_fprintf(p_cli, NRF_CLI_DEFAULT, "*----+------+------------+-------+-------+---------+----------------+----------------*\n");

    for (ch = 0; ch < CHANNEL_ID_MAX; ch++)
    {
        p_info = mkb_db_ch_info_get(ch);

        if (ch == ch_current)
            sprintf(str, "*>%2d<| %4s | %10s | %5d | %5d | %7d | 0x%02x%02x%02x%02x%02x%02x | 0x%02x%02x%02x%02x%02x%02x *\n",
                    ch, mkb_db_get_channel_type_str(p_info->type),
                    mkb_db_get_central_type_str(p_info->central),
                    p_info->state, p_ch->conn_handle[ch], p_info->peer_id,
                    p_info->peer_addr.addr[5], p_info->peer_addr.addr[4], p_info->peer_addr.addr[3],
                    p_info->peer_addr.addr[2], p_info->peer_addr.addr[1], p_info->peer_addr.addr[0],
                    p_info->own_addr.addr[5], p_info->own_addr.addr[4], p_info->own_addr.addr[3],
                    p_info->own_addr.addr[2], p_info->own_addr.addr[1], p_info->own_addr.addr[0]);
        else
            sprintf(str, "* %2d | %4s | %10s | %5d | %5d | %7d | 0x%02x%02x%02x%02x%02x%02x | 0x%02x%02x%02x%02x%02x%02x *\n",
                    ch, mkb_db_get_channel_type_str(p_info->type),
                    mkb_db_get_central_type_str(p_info->central),
                    p_info->state, p_ch->conn_handle[ch], p_info->peer_id,
                    p_info->peer_addr.addr[5], p_info->peer_addr.addr[4], p_info->peer_addr.addr[3],
                    p_info->peer_addr.addr[2], p_info->peer_addr.addr[1], p_info->peer_addr.addr[0],
                    p_info->own_addr.addr[5], p_info->own_addr.addr[4], p_info->own_addr.addr[3],
                    p_info->own_addr.addr[2], p_info->own_addr.addr[1], p_info->own_addr.addr[0]);

        nrf_cli_fprintf(p_cli, NRF_CLI_DEFAULT, str);
    }
    nrf_cli_fprintf(p_cli, NRF_CLI_DEFAULT, "**************************************************************************************\n");
}

static void cmd_db_system(nrf_cli_t const *p_cli, size_t argc, char **argv)
{
    UNUSED_PARAMETER(argc);
    UNUSED_PARAMETER(argv);

    if (nrf_cli_help_requested(p_cli))
    {
        nrf_cli_help_print(p_cli, NULL, 0);
        return;
    }

#if (defined(MKB_STM8_ENABLED) && MKB_STM8_ENABLED)
    STM8_VERSION_t stm8_version;

    stm8_version = mkb_db_stm8_version_get();
#endif

    nrf_cli_fprintf(p_cli, NRF_CLI_DEFAULT, "********************************************************************\n");
    nrf_cli_fprintf(p_cli, NRF_CLI_DEFAULT, "*                          MOKIBO                                  *\n");
    nrf_cli_fprintf(p_cli, NRF_CLI_DEFAULT, "********************************************************************\n");
    nrf_cli_fprintf(p_cli, NRF_CLI_DEFAULT, "* Model Name      : %s\n", MKB_MODEL_NAME);
    nrf_cli_fprintf(p_cli, NRF_CLI_DEFAULT, "* MainPBA Version : %s\n", MKB_HW_VERSION);
    nrf_cli_fprintf(p_cli, NRF_CLI_DEFAULT, "* FW Version      : %s\n", MKB_FW_VERSION);
    nrf_cli_fprintf(p_cli, NRF_CLI_DEFAULT, "* Build Date/Time : %s / %s\n", MKB_BUILD_DATE, MKB_BUILD_TIME);
#if (defined(MKB_STM8_ENABLED) && MKB_STM8_ENABLED)
    nrf_cli_fprintf(p_cli, NRF_CLI_DEFAULT, "* Clickbar FW Ver : 0x%02x(V%d.%d.%d)\n",
                    stm8_version.version,
                    stm8_version.maj,
                    stm8_version.min,
                    stm8_version.rev);
#endif
#if (defined(BOARD_MKB10040))
    nrf_cli_fprintf(p_cli, NRF_CLI_DEFAULT, "* Board  Type: MKB10040\n");
#endif
#if (defined(BOARD_MKB10056))
    nrf_cli_fprintf(p_cli, NRF_CLI_DEFAULT, "* Board  Type: MKB10056\n");
#endif
#if (defined(BOARD_PCA10040))
    nrf_cli_fprintf(p_cli, NRF_CLI_DEFAULT, "* Board  Type: PCA10040\n");
#endif
#if (defined(BOARD_PCA10056))
    nrf_cli_fprintf(p_cli, NRF_CLI_DEFAULT, "* Board  Type: PCA10040\n");
#endif
    nrf_cli_fprintf(p_cli, NRF_CLI_DEFAULT, "* Reset Reason    : %s\n", mkb_util_reset_reason_str(mkb_db_reset_reason_get()));
    nrf_cli_fprintf(p_cli, NRF_CLI_DEFAULT, "* Die temperature : " NRF_LOG_FLOAT_MARKER " Celsius\n", NRF_LOG_FLOAT(mkb_util_temp_get()));
#if (defined(MKB_BATTERY_ENABLED) && MKB_BATTERY_ENABLED)
    nrf_cli_fprintf(p_cli, NRF_CLI_DEFAULT, "* Battery level   : %03d%%\n", mkb_battery_level_get());
#endif //(defined(MKB_BATTERY_ENABLED) && MKB_BATTERY_ENABLED)
    nrf_cli_fprintf(p_cli, NRF_CLI_DEFAULT, "********************************************************************\n");
}

static void cmd_db(nrf_cli_t const *p_cli, size_t argc, char **argv)
{
    UNUSED_PARAMETER(argc);
    UNUSED_PARAMETER(argv);

    if ((argc == 1) || nrf_cli_help_requested(p_cli))
    {
        nrf_cli_help_print(p_cli, NULL, 0);
        return;
    }
}

NRF_CLI_CREATE_STATIC_SUBCMD_SET(m_sub_db){
    NRF_CLI_CMD(system, NULL, SYSTEM_HELP, cmd_db_system),
    NRF_CLI_CMD(config, NULL, CFG_HELP, cmd_db_cfg),
    NRF_CLI_CMD(channel, NULL, CHANNEL_HELP, cmd_db_channel),
    NRF_CLI_SUBCMD_SET_END};
NRF_CLI_CMD_REGISTER(db, &m_sub_db, DB_HELP, cmd_db);

#endif // (defined(MKB_DB_ENABLED) && MKB_DB_ENABLED)
#endif // (defined(MKB_CLI_ENABLED) && MKB_CLI_ENABLED)
