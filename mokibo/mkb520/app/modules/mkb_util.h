/*
* ===============================================================================
* Copyright (C) 2017-2019
* INNOPRESSO INC. All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
* ===============================================================================
*/

#ifndef __MKB_UTIL__
#define __MKB_UTIL__

#include "ble_gap.h"

extern void mkb_util_get_mac_address(uint32_t ch, ble_gap_addr_t *pAddr);
extern uint32_t mkb_util_reset_reason_get(void);
extern char *mkb_util_reset_reason_str(uint32_t reason);
extern float mkb_util_temp_get(void);
extern uint32_t mkb_util_timer_ms(uint32_t ticks);
extern uint32_t mkb_util_counter_ms();
extern uint32_t mkb_util_get_tick_delta();

#endif /* __MKB_UTIL__ */
