/*
* ===============================================================================
* Copyright (C) 2017-2019
* INNOPRESSO INC. All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
* ===============================================================================
*/

#ifndef __MKB_COMS__
#define __MKB_COMS__

#if (defined(MKB_EVENT_ENABLED) && MKB_EVENT_ENABLED)
#include "mkb_event.h"
#else
#error "You have to enable event at mkb_config.h!"
#endif // (defined(MKB_EVENT_ENABLED) && MKB_EVENT_ENABLED)

#if (defined(MKB_BLE_ENABLED) && MKB_BLE_ENABLED)
// ble_gap.h
#define MKB_COMS_PASSKEY_LEN BLE_GAP_PASSKEY_LEN

typedef struct
{
	bool is_ready;
	bool is_enter;
	uint8_t index;
	uint8_t keys[MKB_COMS_PASSKEY_LEN];
} passkey_t;

typedef void (*mkb_coms_ble_event_handler_t)(void *p_event_data, uint16_t event_size);
#endif //(defined(MKB_BLE_ENABLED) && MKB_BLE_ENABLED)

extern void mkb_coms_init(void);
extern void mkb_coms_start(void);
extern bool mkb_coms_event_handler(mkb_event_t *p_event);
extern void mkb_coms_passkey_init(void);
extern void mkb_coms_passkey_enable(void);
extern void mkb_coms_passkey_disable(void);
extern bool mkb_coms_is_button_left(void);
extern ret_code_t mkb_coms_cli(size_t size, char **params);

#endif /* __MKB_COMS__ */
