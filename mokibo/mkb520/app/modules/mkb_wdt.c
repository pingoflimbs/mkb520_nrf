/*
* ===============================================================================
* Copyright (C) 2017-2019
* INNOPRESSO INC. All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
* ===============================================================================
*/

#include <stdbool.h>
#include <stdint.h>
#include <stdarg.h>

#include "sdk_common.h"

#include "nrf_assert.h"
#include "nrf_error.h"
#include "nrf_delay.h"
#include "nrf_cli.h"
#include "nrf_balloc.h"
#include "nrf_pwr_mgmt.h"

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

#include "app_error.h"
#include "app_timer.h"
#include "app_scheduler.h"

#define MKB_LOG_LEVEL MKB_WDT_LOG_LEVEL
#include "mkb_common.h"
#include "mkb_wdt.h"

#include "nrf_drv_wdt.h"
#include "nrf_drv_clock.h"

#if (defined(MKB_WDT_ENABLED) && MKB_WDT_ENABLED)

nrf_drv_wdt_channel_id m_wdt_channel_id;
bool m_wdt_init = false;
bool m_wdt_enable = false;

/**
 * @brief WDT events handler.
 */
void wdt_event_handler(void)
{
    //bsp_board_leds_off();

    //NOTE: The max amount of time we can spend in WDT interrupt is two cycles of 32768[Hz] clock - after that, reset occurs
}

void mkb_wdt_init(void)
{
    uint32_t err_code = NRF_SUCCESS;

    //Configure WDT.
    nrf_drv_wdt_config_t config = NRF_DRV_WDT_DEAFULT_CONFIG;
    err_code = nrf_drv_wdt_init(&config, wdt_event_handler);
    MKB_ERROR_CHECK(err_code);
    err_code = nrf_drv_wdt_channel_alloc(&m_wdt_channel_id);
    MKB_ERROR_CHECK(err_code);

    if (err_code == NRF_SUCCESS)
    {
        m_wdt_init = true;
        MKB_LOG_INFO("Initialization Success!");
    }
}

void mkb_wdt_enable(void)
{
    if (m_wdt_init)
    {
        m_wdt_enable = true;
        nrf_drv_wdt_enable();

        MKB_LOG_INFO("Enabled!");
    }
}

void mkb_wdt_feed(void)
{
    if (m_wdt_init && m_wdt_enable)
        nrf_drv_wdt_channel_feed(m_wdt_channel_id);
}

void mkb_wdt_time_set(uint32_t time)
{
    uint64_t ticks = (time * 32768ULL) / 1000;
    NRFX_ASSERT(ticks <= UINT32_MAX);

    nrf_wdt_reload_value_set((uint32_t)ticks);
}

#endif // (defined(MKB_WDT_ENABLED) && MKB_WDT_ENABLED)
