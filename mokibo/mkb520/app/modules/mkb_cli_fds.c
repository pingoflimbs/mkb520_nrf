/*
* ===============================================================================
* Copyright (C) 2017-2019
* INNOPRESSO INC. All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
* ===============================================================================
*/

#include <stdbool.h>
#include <stdint.h>

#include "sdk_common.h"

#include "nrf_assert.h"
#include "nrf_error.h"
#include "nrf_log.h"
#include "nrf_delay.h"
#include "nrf_cli.h"

#include "fds.h"

#define MKB_LOG_LEVEL MKB_CLI_LOG_LEVEL
#include "mkb_common.h"

#if (defined(MKB_CLI_ENABLED) && MKB_CLI_ENABLED)
#include "mkb_cli.h"

#if (defined(MKB_FDS_ENABLED) && MKB_FDS_ENABLED)
#include "mkb_fds.h"

#if !(defined(MKB_DB_ENABLED) && MKB_DB_ENABLED)
#error "You have to enable db at mkb_config.h!"
#endif // (defined(MKB_DB_ENABLED) && MKB_DB_ENABLED)

#define FDS_HELP "fds commands\n" \
                 "usage: fds"

#define MOKIBO_HELP "mokibo related commands\n" \
                    "usage: mokibo config|channel|delete"

#define MOKIBO_CFG_HELP "print system configuration\n" \
                        "usage: mokibo config"

#define MOKIBO_CHANNEL_HELP "print channel configuration\n" \
                            "usage: mokibo channel"

#define MOKIBO_DELETE_HELP "delete channel and configuration fds records\n" \
                           "usage: mokibo delete"

#define PRINT_HELP "print records\n" \
                   "usage: print all"

#define PRINT_ALL_HELP "print all records\n" \
                       "usage: print all"

#define WRITE_HELP "write a record\n"                    \
                   "usage: write file_id key \"data\"\n" \
                   "- file_id:\tfile ID, in HEX\n"       \
                   "- key:\trecord key, in HEX\n"        \
                   "- data:\trecord contents"

#define DELETE_HELP "delete a record\n"             \
                    "usage: delete file_id key\n"   \
                    "- file_id:\tfile ID, in HEX\n" \
                    "- key:\trecord key, in HEX"

#define STAT_HELP "print statistics\n" \
                  "usage: fds stat"

#define GC_HELP "run garbage collection\n" \
                "usage: fds gc"

static void cmd_fds_print(nrf_cli_t const *p_cli, size_t argc, char **argv)
{
    if (nrf_cli_help_requested(p_cli))
    {
        nrf_cli_help_print(p_cli, NULL, 0);
    }
    else if (argc != 2)
    {
        mkb_cli_wrong_param_count_help(p_cli, "print");
    }
    else
    {
        mkb_cli_unknown_param_help(p_cli, argv[1], "print");
    }
}

static void cmd_fds_print_all(nrf_cli_t const *p_cli, size_t argc, char **argv)
{
    fds_find_token_t tok = {0};
    fds_record_desc_t desc = {0};

    nrf_cli_fprintf(p_cli, NRF_CLI_DEFAULT,
                    "rec. id\t"
                    "\tfile id\t"
                    "\trec. key"
                    "\tlength\n");

    while (fds_record_iterate(&desc, &tok) != FDS_ERR_NOT_FOUND)
    {
        ret_code_t rc;
        fds_flash_record_t frec = {0};

        rc = fds_record_open(&desc, &frec);
        switch (rc)
        {
        case NRF_SUCCESS:
            break;

        case FDS_ERR_CRC_CHECK_FAILED:
            nrf_cli_fprintf(p_cli, NRF_CLI_ERROR, "error: CRC check failed!\n");
            continue;

        case FDS_ERR_NOT_FOUND:
            nrf_cli_fprintf(p_cli, NRF_CLI_ERROR, "error: record not found!\n");
            continue;

        default:
        {
            nrf_cli_fprintf(p_cli, NRF_CLI_ERROR,
                            "error: unexpecte error %s.\n",
                            mkb_fds_err_str(rc));

            continue;
        }
        }

        uint32_t const len = frec.p_header->length_words * sizeof(uint32_t);

        nrf_cli_fprintf(p_cli, NRF_CLI_DEFAULT,
                        " 0x%04x\t"
                        "\t 0x%04x\t"
                        "\t 0x%04x\t"
                        "\t %4u bytes\n",
                        frec.p_header->record_id,
                        frec.p_header->file_id,
                        frec.p_header->record_key,
                        len);

        rc = fds_record_close(&desc);
        APP_ERROR_CHECK(rc);
    }
}

static void mkb_cli_fds_record_write(nrf_cli_t const *p_cli,
                                     uint32_t fid,
                                     uint32_t key,
                                     void const *p_data,
                                     uint32_t len)
{
    fds_record_t const rec =
        {
            .file_id = fid,
            .key = key,
            .data.p_data = p_data,
            .data.length_words = (len + 3) / sizeof(uint32_t)};

    nrf_cli_fprintf(p_cli, NRF_CLI_DEFAULT,
                    "writing record to flash...\n"
                    "file: 0x%x, key: 0x%x, \"%s\", len: %u bytes\n",
                    fid, key, p_data, len);

    ret_code_t rc = fds_record_write(NULL, &rec);
    if (rc != NRF_SUCCESS)
    {
        nrf_cli_fprintf(p_cli, NRF_CLI_ERROR,
                        "error: fds_record_write() returned %s.\n",
                        mkb_fds_err_str(rc));
    }
}

static void mkb_cli_fds_record_delete(nrf_cli_t const *p_cli, uint32_t fid, uint32_t key)
{
    fds_find_token_t tok = {0};
    fds_record_desc_t desc = {0};

    nrf_cli_fprintf(p_cli, NRF_CLI_DEFAULT,
                    "deleting record...\n"
                    "file: 0x%x, key: 0x%x\n",
                    fid,
                    key);

    if (fds_record_find(fid, key, &desc, &tok) == NRF_SUCCESS)
    {
        ret_code_t rc = fds_record_delete(&desc);
        if (rc != NRF_SUCCESS)
        {
            nrf_cli_fprintf(p_cli, NRF_CLI_ERROR,
                            "error: fds_record_delete() returned %s.\n", mkb_fds_err_str(rc));

            return;
        }

        nrf_cli_fprintf(p_cli, NRF_CLI_DEFAULT, "record id: 0x%x\n", desc.record_id);
    }
    else
    {
        nrf_cli_fprintf(p_cli, NRF_CLI_ERROR, "error: record not found!\n");
    }
}

static void cmd_fds_write(nrf_cli_t const *p_cli, size_t argc, char **argv)
{

    if (nrf_cli_help_requested(p_cli))
    {
        nrf_cli_help_print(p_cli, NULL, 0);
    }
    else if (argc != 4)
    {
        mkb_cli_wrong_param_count_help(p_cli, "write");
    }
    else
    {
        /* Must be statically allocated, because it is going to be written in flash. */
        static uint8_t m_data[256];

        uint16_t const fid = strtol(argv[1], NULL, 16);
        uint16_t const key = strtol(argv[2], NULL, 16);

        uint32_t const len = strlen(argv[3]) < sizeof(m_data) ? strlen(argv[3]) : sizeof(m_data);

        /* Copy data to static variable. */
        memset(m_data, 0x00, sizeof(m_data));
        memcpy(m_data, argv[3], len);

        mkb_cli_fds_record_write(p_cli, fid, key, m_data, len);
    }
}

static void cmd_fds_delete(nrf_cli_t const *p_cli, size_t argc, char **argv)
{
    if (nrf_cli_help_requested(p_cli))
    {
        nrf_cli_help_print(p_cli, NULL, 0);
    }
    else if (argc < 3)
    {
        mkb_cli_wrong_param_count_help(p_cli, "delete");
    }
    else
    {
        uint32_t const fid = strtol(argv[1], NULL, 16);
        uint32_t const key = strtol(argv[2], NULL, 16);

        mkb_cli_fds_record_delete(p_cli, fid, key);
    }
}

static void cmd_fds_stat(nrf_cli_t const *p_cli, size_t argc, char **argv)
{
    if (nrf_cli_help_requested(p_cli))
    {
        nrf_cli_help_print(p_cli, NULL, 0);
    }
    else
    {
        /* Print fds_stat(). */
        fds_stat_t stat = {0};

        ret_code_t rc = fds_stat(&stat);
        APP_ERROR_CHECK(rc);

        nrf_cli_fprintf(p_cli, NRF_CLI_DEFAULT,
                        "total pages:\t%u\n"
                        "total records:\t%u\n"
                        "valid records:\t%u\n"
                        "dirty records:\t%u\n"
                        "largest contig:\t%u\n"
                        "freeable words:\t%u (%u bytes)\n",
                        stat.pages_available,
                        stat.valid_records + stat.dirty_records,
                        stat.valid_records,
                        stat.dirty_records,
                        stat.largest_contig,
                        stat.freeable_words,
                        stat.freeable_words * sizeof(uint32_t));
    }
}

static void cmd_fds_gc(nrf_cli_t const *p_cli, size_t argc, char **argv)
{
    ret_code_t rc = fds_gc();
    switch (rc)
    {
    case NRF_SUCCESS:
        break;

    default:
        nrf_cli_fprintf(p_cli, NRF_CLI_ERROR,
                        "error: garbage collection returned %s\n", mkb_fds_err_str(rc));
        break;
    }
}

static void cmd_fds_mokibo(nrf_cli_t const *p_cli, size_t argc, char **argv)
{
    if (nrf_cli_help_requested(p_cli))
    {
        nrf_cli_help_print(p_cli, NULL, 0);
    }
    else if (argc != 2)
    {
        mkb_cli_wrong_param_count_help(p_cli, "mokibo");
    }
    else
    {
        mkb_cli_unknown_param_help(p_cli, argv[1], "mokibo");
    }
}

static void cmd_fds_mokibo_cfg(nrf_cli_t const *p_cli, size_t argc, char **argv)
{
    fds_record_desc_t desc = {0};
    fds_find_token_t tok = {0};

    while (fds_record_find(MKB_FDS_FILE_ID_MOKIBO, MKB_FDS_RECORD_ID_CONFIG, &desc, &tok) == NRF_SUCCESS)
    {
        ret_code_t rc;
        fds_flash_record_t frec = {0};

        rc = fds_record_open(&desc, &frec);
        switch (rc)
        {
        case NRF_SUCCESS:
            break;

        case FDS_ERR_CRC_CHECK_FAILED:
            nrf_cli_fprintf(p_cli, NRF_CLI_ERROR, "error: CRC check failed!\n");
            continue;

        case FDS_ERR_NOT_FOUND:
            nrf_cli_fprintf(p_cli, NRF_CLI_ERROR, "error: record not found!\n");
            continue;

        default:
        {
            nrf_cli_fprintf(p_cli, NRF_CLI_ERROR,
                            "error: unexpecte error %s.\n",
                            mkb_fds_err_str(rc));

            continue;
        }
        }

        uint32_t const len = frec.p_header->length_words * sizeof(uint32_t);

        nrf_cli_fprintf(p_cli, NRF_CLI_DEFAULT,
                        " 0x%04x\t"
                        "\t 0x%04x\t"
                        "\t 0x%04x\t"
                        "\t %4u bytes\n",
                        frec.p_header->record_id,
                        frec.p_header->file_id,
                        frec.p_header->record_key,
                        len);

        MKB_DB_CONFIG_t *p_cfg = (MKB_DB_CONFIG_t *)(frec.p_data);

        nrf_cli_fprintf(p_cli, NRF_CLI_DEFAULT,
                        "magic  code:\t0x%08x\n"
                        "device name:\t%s\n"
                        "model  name:\t%s\n",
                        p_cfg->magic_code,
                        p_cfg->device_name,
                        p_cfg->model_name);

        rc = fds_record_close(&desc);
        APP_ERROR_CHECK(rc);
    }
}

static void cmd_fds_mokibo_channel(nrf_cli_t const *p_cli, size_t argc, char **argv)
{
    fds_record_desc_t desc = {0};
    fds_find_token_t tok = {0};
    CHANNEL_ID_t ch;
    CHANNEL_INFO_t *p_info;
    char str[256];
    CHANNEL_ID_t ch_current = mkb_db_ch_id_get();

    while (fds_record_find(MKB_FDS_FILE_ID_MOKIBO, MKB_FDS_RECORD_ID_CHANNEL, &desc, &tok) == NRF_SUCCESS)
    {
        ret_code_t rc;
        fds_flash_record_t frec = {0};

        rc = fds_record_open(&desc, &frec);
        switch (rc)
        {
        case NRF_SUCCESS:
            break;

        case FDS_ERR_CRC_CHECK_FAILED:
            nrf_cli_fprintf(p_cli, NRF_CLI_ERROR, "error: CRC check failed!\n");
            continue;

        case FDS_ERR_NOT_FOUND:
            nrf_cli_fprintf(p_cli, NRF_CLI_ERROR, "error: record not found!\n");
            continue;

        default:
        {
            nrf_cli_fprintf(p_cli, NRF_CLI_ERROR,
                            "error: unexpecte error %s.\n",
                            mkb_fds_err_str(rc));

            continue;
        }
        }

        uint32_t const len = frec.p_header->length_words * sizeof(uint32_t);

        nrf_cli_fprintf(p_cli, NRF_CLI_DEFAULT,
                        " 0x%04x\t"
                        "\t 0x%04x\t"
                        "\t 0x%04x\t"
                        "\t %4u bytes\n",
                        frec.p_header->record_id,
                        frec.p_header->file_id,
                        frec.p_header->record_key,
                        len);

        MKB_DB_CH_CONFIG_t *p_ch = (MKB_DB_CH_CONFIG_t *)(frec.p_data);

        nrf_cli_fprintf(p_cli, NRF_CLI_DEFAULT, "******************************************************************************\n");
        nrf_cli_fprintf(p_cli, NRF_CLI_DEFAULT, "*                              MOKIBO CHANNEL                                *\n");
        nrf_cli_fprintf(p_cli, NRF_CLI_DEFAULT, "******************************************************************************\n");
        nrf_cli_fprintf(p_cli, NRF_CLI_DEFAULT, "* ID | TYPE |  CENTRAL   | STATE | PEER ID | PEER      ADDR | OWN       ADDR *\n");
        nrf_cli_fprintf(p_cli, NRF_CLI_DEFAULT, "*----+------+------------+-------+---------+----------------+----------------*\n");

        for (ch = CHANNEL_ID_BT1; ch < CHANNEL_ID_MAX; ch++)
        {
            p_info = mkb_db_ch_info_get(ch);

            if (ch == ch_current)
                sprintf(str, "*>%2d<| %4s | %10s | %5d | %7d | 0x%02x%02x%02x%02x%02x%02x | 0x%02x%02x%02x%02x%02x%02x *\n",
                        ch, mkb_db_get_channel_type_str(p_info->type),
                        mkb_db_get_central_type_str(p_info->central),
                        p_info->state, p_info->peer_id,
                        p_info->peer_addr.addr[5], p_info->peer_addr.addr[4], p_info->peer_addr.addr[3],
                        p_info->peer_addr.addr[2], p_info->peer_addr.addr[1], p_info->peer_addr.addr[0],
                        p_info->own_addr.addr[5], p_info->own_addr.addr[4], p_info->own_addr.addr[3],
                        p_info->own_addr.addr[2], p_info->own_addr.addr[1], p_info->own_addr.addr[0]);
            else
                sprintf(str, "* %2d | %4s | %10s | %5d | %7d | 0x%02x%02x%02x%02x%02x%02x | 0x%02x%02x%02x%02x%02x%02x *\n",
                        ch, mkb_db_get_channel_type_str(p_info->type),
                        mkb_db_get_central_type_str(p_info->central),
                        p_info->state, p_info->peer_id,
                        p_info->peer_addr.addr[5], p_info->peer_addr.addr[4], p_info->peer_addr.addr[3],
                        p_info->peer_addr.addr[2], p_info->peer_addr.addr[1], p_info->peer_addr.addr[0],
                        p_info->own_addr.addr[5], p_info->own_addr.addr[4], p_info->own_addr.addr[3],
                        p_info->own_addr.addr[2], p_info->own_addr.addr[1], p_info->own_addr.addr[0]);

            nrf_cli_fprintf(p_cli, NRF_CLI_DEFAULT, str);
        }
        nrf_cli_fprintf(p_cli, NRF_CLI_DEFAULT, "******************************************************************************\n");

        rc = fds_record_close(&desc);
        APP_ERROR_CHECK(rc);
    }
}

static void cmd_fds_mokibo_delete(nrf_cli_t const *p_cli, size_t argc, char **argv)
{
    uint32_t const fid = MKB_FDS_FILE_ID_MOKIBO;
    uint32_t key;

    key = MKB_FDS_RECORD_ID_CONFIG;
    mkb_cli_fds_record_delete(p_cli, fid, key);

    key = MKB_FDS_RECORD_ID_CHANNEL;
    mkb_cli_fds_record_delete(p_cli, fid, key);
}

static void cmd_fds(nrf_cli_t const *p_cli, size_t argc, char **argv)
{
    UNUSED_PARAMETER(argc);
    UNUSED_PARAMETER(argv);

    if ((argc == 1) || nrf_cli_help_requested(p_cli))
    {
        nrf_cli_help_print(p_cli, NULL, 0);
        return;
    }
}

NRF_CLI_CREATE_STATIC_SUBCMD_SET(m_sub_print){
    NRF_CLI_CMD(all, NULL, PRINT_ALL_HELP, cmd_fds_print_all),
    NRF_CLI_SUBCMD_SET_END};

NRF_CLI_CREATE_STATIC_SUBCMD_SET(m_sub_mokibo){
    NRF_CLI_CMD(config, NULL, MOKIBO_CFG_HELP, cmd_fds_mokibo_cfg),
    NRF_CLI_CMD(channel, NULL, MOKIBO_CHANNEL_HELP, cmd_fds_mokibo_channel),
    NRF_CLI_CMD(delete, NULL, MOKIBO_DELETE_HELP, cmd_fds_mokibo_delete),
    NRF_CLI_SUBCMD_SET_END};

NRF_CLI_CREATE_STATIC_SUBCMD_SET(m_sub_fds){
    NRF_CLI_CMD(print, &m_sub_print, PRINT_HELP, cmd_fds_print),
    NRF_CLI_CMD(mokibo, &m_sub_mokibo, MOKIBO_HELP, cmd_fds_mokibo),
    NRF_CLI_CMD(write, NULL, WRITE_HELP, cmd_fds_write),
    NRF_CLI_CMD(delete, NULL, DELETE_HELP, cmd_fds_delete),
    NRF_CLI_CMD(stat, NULL, STAT_HELP, cmd_fds_stat),
    NRF_CLI_CMD(gc, NULL, GC_HELP, cmd_fds_gc),
    NRF_CLI_SUBCMD_SET_END};
NRF_CLI_CMD_REGISTER(fds, &m_sub_fds, FDS_HELP, cmd_fds);

#else
#if (defined(MKB_DB_ENABLED) && MKB_DB_ENABLED)
#error "You have to enable fds at mkb_config.h!"
#endif // (defined(MKB_DB_ENABLED) && MKB_DB_ENABLED)
#endif //(defined(MKB_FDS_ENABLED) && MKB_FDS_ENABLED)
#endif //(defined(MKB_CLI_ENABLED) && MKB_CLI_ENABLED)
