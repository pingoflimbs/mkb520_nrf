/*
 * ===============================================================================
 * Copyright (C) 2017-2019
 * INNOPRESSO INC. All Rights Reserved
 *
 * The content of this file is copyrighted by INNOPRESSO or its
 * affiliated, associated or related companies. All copyrights and other
 * associated intellectual property rights related thereto are expressly
 * reserved by INNOPRESSO INC.
 * ===============================================================================
 */

#include <stdbool.h>
#include <stdint.h>
#include <stdarg.h>

#include "sdk_common.h"

#include "ble.h"
#include "ble_err.h"
#include "ble_conn_params.h"

#include "nrf_assert.h"
#include "nrf_error.h"
#include "nrf_delay.h"
#include "nrf_cli.h"
#include "nrf_balloc.h"
#include "nrf_pwr_mgmt.h"

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

#include "app_error.h"
#include "app_timer.h"
#include "app_scheduler.h"

#if defined(GPIOTE_ENABLED) && GPIOTE_ENABLED
#include "app_gpiote.h"
#endif

#define MKB_LOG_LEVEL MKB_BATTERY_LOG_LEVEL
#include "mkb_common.h"
#include "mkb_event.h"
#include "mkb_battery.h"

#if (defined(MKB_BATTERY_ENABLED) && MKB_BATTERY_ENABLED)

APP_TIMER_DEF(m_battery_timer_id); /**< Battery timer. */
static uint8_t m_battery_level = 100;
static uint16_t m_volt_avg[30] = {
    0,
};
static uint8_t m_volt_avg_cursor = 0;
static bool m_volt_avg_init_flag = false;

#if (defined(MKB_BATTERY_SENSE_TYPE))
#if (MKB_BATTERY_SENSE_TYPE == BATTERY_SENSE_TYPE_SIM)
#include "sensorsim.h"

static sensorsim_cfg_t m_battery_sim_cfg;     /**< Battery Level sensor simulator configuration. */
static sensorsim_state_t m_battery_sim_state; /**< Battery Level sensor simulator state. */

#elif (MKB_BATTERY_SENSE_TYPE == BATTERY_SENSE_TYPE_ADC)
#include "nrf.h"
#include "nrf_drv_saadc.h"

// 참조:  for VDD
// https://jimmywongiot.com/2019/07/29/how-to-add-the-battery-service-in-current-optimization/
// https://github.com/jimmywong2003/nrf5-ble-bidirection-throughput-test/blob/master/ble_app_its_bas/main.c

// nRF52-ADC-examples
// https://github.com/NordicPlayground/nRF52-ADC-examples

static nrf_saadc_value_t m_buffer_pool[2][SAMPLES_IN_BUFFER];
static uint32_t m_adc_evt_counter;
#endif
#endif

#if (defined(MKB_STM8_ENABLED) && MKB_STM8_ENABLED)
#include "drv_stm8.h"
#endif //(defined(MKB_STM8_ENABLED) && MKB_STM8_ENABLED)

#if (defined(MKB_BATTERY_SENSE_TYPE))
#if (MKB_BATTERY_SENSE_TYPE == BATTERY_SENSE_TYPE_SIM)

/**@brief Function for initializing the battery sensor simulator.
 */
static void mkb_battery_sensor_simulator_init(void)
{
    m_battery_sim_cfg.min = BATTERY_LEVEL_MIN;
    m_battery_sim_cfg.max = BATTERY_LEVEL_MAX;
    m_battery_sim_cfg.incr = BATTERY_LEVEL_INCREMENT;
    m_battery_sim_cfg.start_at_max = true;

    sensorsim_init(&m_battery_sim_state, &m_battery_sim_cfg);
}

#elif (MKB_BATTERY_SENSE_TYPE == BATTERY_SENSE_TYPE_ADC)
void mkb_battery_saadc_callback(nrf_drv_saadc_evt_t const *p_event)
{
    /*
    4.22 100% : 2016mv
4.0 80% : 1920mV
3.8 50% : 1840mV
3.5 15% : 1710mV
    */
    if (p_event->type == NRF_DRV_SAADC_EVT_DONE)
    {
        // 4.22 100 % : 2016mv
        // 4.0 95 % : 1920mV
        // 3.8 50 % : 1840mV
        // 3.5 25 % : 1710mV
        ret_code_t err_code;
        nrf_saadc_value_t adc_result;

        int16_t percentage_result;

        err_code = nrf_drv_saadc_buffer_convert(p_event->data.done.p_buffer, 1);
        MKB_ERROR_CHECK(err_code);

        adc_result = p_event->data.done.p_buffer[0];

        uint16_t millivolt_result_raw = ADC_RESULT_IN_MILLI_VOLTS(adc_result) + DIODE_FWD_VOLT_DROP_MILLIVOLTS;
        uint16_t millivolt_result_compensated = 0;
        uint16_t millivolt_result_avg_sum = 0;
        uint16_t millivolt_result_avg = 0;
        uint8_t sample_cnt = 0;

        ________DBG_20211205_battery_test("raw value : %d", millivolt_result_raw);

        //충전시 배터리 보정값
        if (mkb_db_is_bat_charging_get())
        {
            if (millivolt_result_raw < 1918)
            {
                millivolt_result_compensated = millivolt_result_raw + 1764 - 1908;
            }
            else if (millivolt_result_raw < 1918)
            {
                millivolt_result_compensated = millivolt_result_raw + 1776 - 1918;
            }
            else if (millivolt_result_raw < 1918)
            {
                millivolt_result_compensated = millivolt_result_raw + 1782 - 1922;
            }
            else if (millivolt_result_raw < 1918)
            {
                millivolt_result_compensated = millivolt_result_raw + 1788 - 1930;
            }
            else if (millivolt_result_raw < 1918)
            {
                millivolt_result_compensated = millivolt_result_raw + 1800 - 1948;
            }
            else if (millivolt_result_raw < 1918)
            {
                millivolt_result_compensated = millivolt_result_raw + 1808 - 1950;
            }
            else if (millivolt_result_raw < 1918)
            {
                millivolt_result_compensated = millivolt_result_raw + 1816 - 1954;
            }
            else if (millivolt_result_raw < 1918)
            {
                millivolt_result_compensated = millivolt_result_raw + 1830 - 1960;
            }
            else if (millivolt_result_raw < 1918)
            {
                millivolt_result_compensated = millivolt_result_raw + 1836 - 1970;
            }
            else if (millivolt_result_raw < 1918)
            {
                millivolt_result_compensated = millivolt_result_raw + 1850 - 1992;
            }
            else if (millivolt_result_raw < 1918)
            {
                millivolt_result_compensated = millivolt_result_raw + 1868 - 1998;
            }
            else if (millivolt_result_raw < 1918)
            {
                millivolt_result_compensated = millivolt_result_raw + 1872 - 1998;
            }
            else if (millivolt_result_raw < 1918)
            {
                millivolt_result_compensated = millivolt_result_raw + 1878 - 2004;
            }
            else if (millivolt_result_raw < 1918)
            {
                millivolt_result_compensated = millivolt_result_raw + 1890 - 2010;
            }
            else if (millivolt_result_raw < 1918)
            {
                millivolt_result_compensated = millivolt_result_raw + 1900 - 2020;
            }
            else if (millivolt_result_raw < 1918)
            {
                millivolt_result_compensated = millivolt_result_raw + 1908 - 2022;
            }
            else if (millivolt_result_raw < 1918)
            {
                millivolt_result_compensated = millivolt_result_raw + 1916 - 2022;
            }
            else
            {
                millivolt_result_compensated = millivolt_result_raw;
            }
        }
        else
        {
            millivolt_result_compensated = millivolt_result_raw;
        }

        m_volt_avg[m_volt_avg_cursor] = millivolt_result_compensated;
        if (m_volt_avg_init_flag == false)
        {
            for (uint8_t i = 0; i < 30; i++)
            {
                m_volt_avg[i] = millivolt_result_compensated;
            }
            m_volt_avg_init_flag = true;
        }

        for (sample_cnt = 0; sample_cnt < 30 && m_volt_avg[sample_cnt] != 0; sample_cnt++)
        {
            millivolt_result_avg_sum += m_volt_avg[sample_cnt];
        }
        millivolt_result_avg = millivolt_result_avg_sum / sample_cnt;

        // percentage_result = 1520541 - 2203743 * millivolt_result_avg + 1274103 * pow(millivolt_result_avg, 2) - 367282.3 * pow(millivolt_result_avg, 3) + 52783.85 * pow(millivolt_result_avg, 4) - 3025.109 * pow(millivolt_result_avg, 5);

        if (millivolt_result_avg < 1419)
        {
            percentage_result = 0;
        }
        else if (millivolt_result_avg < 1513)
        {
            percentage_result = 1;
        }
        else if (millivolt_result_avg < 1569)
        {
            percentage_result = 2;
        }
        else if (millivolt_result_avg < 1604)
        {
            percentage_result = 3;
        }
        else if (millivolt_result_avg < 1636)
        {
            percentage_result = 4;
        }
        else if (millivolt_result_avg < 1666)
        {
            percentage_result = 5;
        }
        else if (millivolt_result_avg < 1680)
        {
            percentage_result = 6;
        }
        else if (millivolt_result_avg < 1706)
        {
            percentage_result = 7;
        }
        else if (millivolt_result_avg < 1718)
        {
            percentage_result = 8;
        }
        else if (millivolt_result_avg < 1731)
        {
            percentage_result = 9;
        }
        else if (millivolt_result_avg < 1742)
        {
            percentage_result = 10;
        }
        else if (millivolt_result_avg < 1752)
        {
            percentage_result = 11;
        }
        else if (millivolt_result_avg < 1763)
        {
            percentage_result = 12;
        }
        else if (millivolt_result_avg < 1771)
        {
            percentage_result = 13;
        }
        else if (millivolt_result_avg < 1778)
        {
            percentage_result = 15;
        }
        else if (millivolt_result_avg < 1786)
        {
            percentage_result = 17;
        }
        else if (millivolt_result_avg < 1791)
        {
            percentage_result = 18;
        }
        else if (millivolt_result_avg < 1794)
        {
            percentage_result = 20;
        }
        else if (millivolt_result_avg < 1800)
        {
            percentage_result = 21;
        }
        else if (millivolt_result_avg < 1801)
        {
            percentage_result = 22;
        }
        else if (millivolt_result_avg < 1805)
        {
            percentage_result = 24;
        }
        else if (millivolt_result_avg < 1807)
        {
            percentage_result = 25;
        }
        else if (millivolt_result_avg < 1809)
        {
            percentage_result = 27;
        }
        else if (millivolt_result_avg < 1811)
        {
            percentage_result = 28;
        }
        else if (millivolt_result_avg < 1812)
        {
            percentage_result = 30;
        }
        else if (millivolt_result_avg < 1814)
        {
            percentage_result = 30;
        }
        else if (millivolt_result_avg < 1816)
        {
            percentage_result = 32;
        }
        else if (millivolt_result_avg < 1818)
        {
            percentage_result = 33;
        }
        else if (millivolt_result_avg < 1822)
        {
            percentage_result = 35;
        }
        else if (millivolt_result_avg < 1824)
        {
            percentage_result = 37;
        }
        else if (millivolt_result_avg < 1826)
        {
            percentage_result = 38;
        }
        else if (millivolt_result_avg < 1828)
        {
            percentage_result = 40;
        }
        else if (millivolt_result_avg < 1832)
        {
            percentage_result = 43;
        }
        else if (millivolt_result_avg < 1833)
        {
            percentage_result = 45;
        }
        else if (millivolt_result_avg < 1835)
        {
            percentage_result = 46;
        }
        else if (millivolt_result_avg < 1838)
        {
            percentage_result = 48;
        }
        else if (millivolt_result_avg < 1838)
        {
            percentage_result = 49;
        }
        else if (millivolt_result_avg < 1839)
        {
            percentage_result = 50;
        }
        else if (millivolt_result_avg < 1841)
        {
            percentage_result = 52;
        }
        else if (millivolt_result_avg < 1842)
        {
            percentage_result = 53;
        }
        else if (millivolt_result_avg < 1844)
        {
            percentage_result = 54;
        }
        else if (millivolt_result_avg < 1845)
        {
            percentage_result = 56;
        }
        else if (millivolt_result_avg < 1847)
        {
            percentage_result = 57;
        }
        else if (millivolt_result_avg < 1849)
        {
            percentage_result = 59;
        }
        else if (millivolt_result_avg < 1851)
        {
            percentage_result = 60;
        }
        else if (millivolt_result_avg < 1853)
        {
            percentage_result = 62;
        }
        else if (millivolt_result_avg < 1854)
        {
            percentage_result = 63;
        }
        else if (millivolt_result_avg < 1856)
        {
            percentage_result = 64;
        }
        else if (millivolt_result_avg < 1856)
        {
            percentage_result = 65;
        }
        else if (millivolt_result_avg < 1857)
        {
            percentage_result = 66;
        }
        else if (millivolt_result_avg < 1859)
        {
            percentage_result = 67;
        }
        else if (millivolt_result_avg < 1861)
        {
            percentage_result = 69;
        }
        else if (millivolt_result_avg < 1863)
        {
            percentage_result = 70;
        }
        else if (millivolt_result_avg < 1864)
        {
            percentage_result = 71;
        }
        else if (millivolt_result_avg < 1866)
        {
            percentage_result = 72;
        }
        else if (millivolt_result_avg < 1867)
        {
            percentage_result = 74;
        }
        else if (millivolt_result_avg < 1869)
        {
            percentage_result = 75;
        }
        else if (millivolt_result_avg < 1870)
        {
            percentage_result = 76;
        }
        else if (millivolt_result_avg < 1872)
        {
            percentage_result = 77;
        }
        else if (millivolt_result_avg < 1875)
        {
            percentage_result = 79;
        }
        else if (millivolt_result_avg < 1877)
        {
            percentage_result = 80;
        }
        else if (millivolt_result_avg < 1878)
        {
            percentage_result = 81;
        }
        else if (millivolt_result_avg < 1880)
        {
            percentage_result = 83;
        }
        else if (millivolt_result_avg < 1882)
        {
            percentage_result = 84;
        }
        else if (millivolt_result_avg < 1884)
        {
            percentage_result = 85;
        }
        else if (millivolt_result_avg < 1885)
        {
            percentage_result = 86;
        }
        else if (millivolt_result_avg < 1887)
        {
            percentage_result = 88;
        }
        else if (millivolt_result_avg < 1889)
        {
            percentage_result = 89;
        }
        else if (millivolt_result_avg < 1890)
        {
            percentage_result = 90;
        }
        else if (millivolt_result_avg < 1893)
        {
            percentage_result = 91;
        }
        else if (millivolt_result_avg < 1896)
        {
            percentage_result = 92;
        }
        else if (millivolt_result_avg < 1898)
        {
            percentage_result = 93;
        }
        else if (millivolt_result_avg < 1903)
        {
            percentage_result = 95;
        }
        else if (millivolt_result_avg < 1912)
        {
            percentage_result = 96;
        }
        else if (millivolt_result_avg < 1936)
        {
            percentage_result = 97;
        }
        else if (millivolt_result_avg < 1955)
        {
            percentage_result = 98;
        }
        else if (millivolt_result_avg < 1974)
        {
            percentage_result = 99;
        }
        else
        {
            percentage_result = 100;
        }
        m_battery_level = percentage_result;

        ________DBG_20211205_battery_level("iscon:%d, bat_raw:%d - %d = bat_comped:%d, bat_avg:%d, per_avg:%d, smaple_cnt:%d, m_volt_avg_cursor:%d",
                                           mkb_db_is_bat_charging_get(),
                                           millivolt_result_raw,
                                           millivolt_result_raw - millivolt_result_compensated,
                                           millivolt_result_compensated,
                                           millivolt_result_avg,
                                           m_battery_level,
                                           sample_cnt,
                                           m_volt_avg_cursor);
        ________DBG_20210907_sgs_battery("battery: adc:%04d volt: %04dmV, batt:%02d", adc_result, millivolt_result, percentage_result);
        m_volt_avg_cursor = (m_volt_avg_cursor + 1) % 30;

#if 0
        static uint8_t testvalue = 0;
        testvalue = (testvalue + 5) % 100;
        percentage_result = testvalue;
        ________DBG_20211205_battery_test("testvalue:%d", testvalue);
#endif

        if (mkb_db_battery_level_get != 0)
        {
            if (mkb_db_battery_level_get() / 5 != percentage_result / 5)
            {
                if (mkb_db_is_bat_charging_get())
                {
                    mkb_event_send(EVT_LED_SUB_BATT_CHECK);
                }
            }
            mkb_db_battery_level_set(percentage_result);
            mkb_event_send(EVT_SYSTEM_BATTERY_LEVEL, percentage_result);
        }

        /*
        static uint8_t poll_cnt = 0;
        poll_cnt++;
        if (poll_cnt >= 10)
        {
            poll_cnt = 0;

            char print_buf[13];

            ________DBG_20211205_battery_level("battery level : %d", mkb_db_battery_level_get());

            print_buf[0] = '\n';
            if (mkb_db_is_bat_charging_get())
            {
                print_buf[1] = 'o';
            }
            else
            {
                print_buf[1] = 'x';
            }
            print_buf[2] = ' ';
            print_buf[3] = millivolt_result_raw / 1000 + '0';
            print_buf[4] = millivolt_result_raw % 1000 / 100 + '0';
            print_buf[5] = millivolt_result_raw % 100 / 10 + '0';
            print_buf[6] = millivolt_result_raw % 10 / 1 + '0';
            print_buf[7] = 'v';
            print_buf[8] = ' ';
            print_buf[9] = m_battery_level / 100 + '0';
            print_buf[10] = m_battery_level % 100 / 10 + '0';
            print_buf[11] = m_battery_level % 10 / 1 + '0';
            print_buf[12] = 'p';

            ________DBG_20210831_mkb_coms_aamode("%s", print_buf);
            for (int i = 0; i < 13; i++)
            {
                ________DBG_20210831_mkb_coms_aamode("%d", print_buf[i]);
                if (print_buf[i] != NULL)
                {
                    uint8_t hid_key;
                    if (print_buf[i] == '0')
                    {
                        hid_key = 39;
                    }
                    else if ('1' <= print_buf[i] && print_buf[i] <= '9') // numbers
                    {
                        hid_key = print_buf[i] - '1' + 30;
                    }
                    else if ('a' <= print_buf[i] && print_buf[i] <= 'z') // alphabet small
                    {
                        hid_key = print_buf[i] - 'a' + 4;
                    }
                    else if ('A' <= print_buf[i] && print_buf[i] <= 'Z')
                    {
                        hid_key = print_buf[i] - 'A' + 4;
                    }
                    else if (print_buf[i] == '.') //.
                    {
                        hid_key = 55;
                    }
                    if (print_buf[i] == ' ')
                    {
                        hid_key = 44;
                    }
                    if (print_buf[i] == '\n')
                    {
                        hid_key = 40;
                    }
                    mkb_event_send_ms(i * 16, EVT_KEY_DOWN, hid_key);
                    mkb_event_send_ms(i * 16 + 8, EVT_KEY_UP, hid_key);
                }
                else
                {
                }
            }
        }
        */
    }
}

void mkb_battery_saadc_init(void)
{
    // NRF_SAADC_INPUT_VDD
    ret_code_t err_code;
    nrf_saadc_channel_config_t channel_config =
        NRF_DRV_SAADC_DEFAULT_CHANNEL_CONFIG_SE(NRF_SAADC_INPUT_AIN7);

    err_code = nrf_drv_saadc_init(NULL, mkb_battery_saadc_callback);
    MKB_ERROR_CHECK(err_code);

    err_code = nrf_drv_saadc_channel_init(1, &channel_config);
    MKB_ERROR_CHECK(err_code);

    err_code = nrf_drv_saadc_buffer_convert(m_buffer_pool[0], SAMPLES_IN_BUFFER);
    MKB_ERROR_CHECK(err_code);

    err_code = nrf_drv_saadc_buffer_convert(m_buffer_pool[1], SAMPLES_IN_BUFFER);
    MKB_ERROR_CHECK(err_code);
}

/**@brief Function for initializing the battery sensor ADC.
 */
static void mkb_battery_sensor_adc_init(void)
{
    mkb_battery_saadc_init();
}
#endif
#endif

/**@brief Function for performing a battery measurement, and update the Battery Level characteristic in the Battery Service.
 */
static void battery_level_update(void)
{
    ________DBG_20210907_sgs_battery("battery_level_update()");
#if (defined(MKB_BATTERY_SENSE_TYPE))
#if (MKB_BATTERY_SENSE_TYPE == BATTERY_SENSE_TYPE_SIM)
    m_battery_level = (uint8_t)sensorsim_measure(&m_battery_sim_state, &m_battery_sim_cfg);
#if (defined(MKB_EVENT_ENABLED) && MKB_EVENT_ENABLED)
    mkb_event_send(EVT_SYSTEM_BATTERY_LEVEL, m_battery_level);
#endif //(defined(MKB_EVENT_ENABLED) && MKB_EVENT_ENABLED)
#elif (MKB_BATTERY_SENSE_TYPE == BATTERY_SENSE_TYPE_ADC)
    nrf_saadc_enable();
    ret_code_t err_code = nrf_drv_saadc_sample();
    MKB_ERROR_CHECK(err_code);
#elif (MKB_BATTERY_SENSE_TYPE == BATTERY_SENSE_TYPE_STM8)
#if (defined(MKB_STM8_ENABLED) && MKB_STM8_ENABLED)
    drv_stm8_get_battery_level(&m_battery_level);
    mkb_event_send(EVT_SYSTEM_BATTERY_LEVEL, m_battery_level);
#endif //(defined(MKB_STM8_ENABLED) && MKB_STM8_ENABLED)
#endif
#endif
}

/**@brief Function for handling the Battery measurement timer timeout.
 *
 * @details This function will be called each time the battery level measurement timer expires.
 *
 * @param[in]   p_context   Pointer used for passing some arbitrary information (context) from the
 *                          app_start_timer() call to the timeout handler.
 */
static void mkb_battery_level_meas_timeout_handler(void *p_context)
{
    UNUSED_PARAMETER(p_context);
    battery_level_update();
}

/**@brief mokibo battery initialize */
void mkb_battery_init(void)
{
    ret_code_t err_code;

#if (defined(MKB_BATTERY_SENSE_TYPE))
#if (MKB_BATTERY_SENSE_TYPE == BATTERY_SENSE_TYPE_SIM)
    mkb_battery_sensor_simulator_init();
#elif (MKB_BATTERY_SENSE_TYPE == BATTERY_SENSE_TYPE_ADC)
    mkb_battery_sensor_adc_init();
#endif
#endif

    // Create battery timer.
    err_code = app_timer_create(&m_battery_timer_id,
                                APP_TIMER_MODE_REPEATED,
                                mkb_battery_level_meas_timeout_handler);
    MKB_ERROR_CHECK(err_code);

    battery_level_update();
}

/**@brief mokibo battery start */
void mkb_battery_start(void)
{
    ret_code_t err_code;

    // BATTERY_LEVEL_MEAS_INTERVAL
    // 1min: EA60
    // 1sec: 3E8

    err_code = app_timer_start(m_battery_timer_id, BATTERY_LEVEL_MEAS_INTERVAL, NULL);

    MKB_ERROR_CHECK(err_code);
}

/**@brief mokibo battery stop */
void mkb_battery_stop(void)
{
    ret_code_t err_code;

    err_code = app_timer_stop(m_battery_timer_id);
    MKB_ERROR_CHECK(err_code);
}

uint8_t mkb_battery_level_get(void)
{
    return m_battery_level;
}

#if (defined(NRF_PWR_MGMT_ENABLED) && NRF_PWR_MGMT_ENABLED)
static bool mkb_battery_shutdown(nrf_pwr_mgmt_evt_t event)
{
#if (defined(MKB_BATTERY_SENSE_TYPE) && (MKB_BATTERY_SENSE_TYPE == BATTERY_SENSE_TYPE_ADC))
    nrf_drv_saadc_uninit();
#endif

    mkb_battery_stop();

    return true;
}

NRF_PWR_MGMT_HANDLER_REGISTER(mkb_battery_shutdown, SHUTDOWN_PRIORITY_DEFAULT);
#endif /* NRF_PWR_MGMT_ENABLED */

#endif // (defined(MKB_BATTERY_ENABLED) && MKB_BATTERY_ENABLED)
