/*
* ===============================================================================
* Copyright (C) 2017-2019
* INNOPRESSO INC. All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
* ===============================================================================
*/

#ifndef __MKB_DFU__
#define __MKB_DFU__

#include "mkb_common.h"

extern ret_code_t mkb_dfu_cli(size_t size, char **params);
extern void mkb_dfu_start();

#endif /* __MKB_DFU__ */
