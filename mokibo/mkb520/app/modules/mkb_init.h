/*
* ===============================================================================
* Copyright (C) 2017-2019
* INNOPRESSO INC. All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
* ===============================================================================
*/

#ifndef __MKB_INIT__
#define __MKB_INIT__

#include <stdint.h>
#include <stdbool.h>
#include "mkb_common.h"

//#define SCHED_MAX_EVENT_DATA_SIZE    MAX(sizeof(ble_evt_t), MAX(APP_TIMER_SCHED_EVT_SIZE, sizeof(m_coms_evt_t)))      /**< Maximum size of scheduler events. */
#if (defined(MKB_KEYBOARD_ENABLED) && MKB_KEYBOARD_ENABLED)
#define SCHED_MAX_EVENT_DATA_SIZE MAX(MKB_EVENT_SIZE, MAX(KEYBOARD_MAX_KEYS, APP_TIMER_SCHED_EVENT_DATA_SIZE)) /**< Maximum size of scheduler events. */
#else
#define SCHED_MAX_EVENT_DATA_SIZE (APP_TIMER_SCHED_EVENT_DATA_SIZE) /**< Maximum size of scheduler events. */
#endif
#ifdef SVCALL_AS_NORMAL_FUNCTION
#define SCHED_QUEUE_SIZE 20 /**< Maximum number of events in the scheduler queue. More is needed in case of Serialization. */
#else
#define SCHED_QUEUE_SIZE 40 /**< Maximum number of events in the scheduler queue. */
#endif

extern void mkb_init(void);
extern void mkb_start(void);

#endif /* __MKB_INIT__ */
