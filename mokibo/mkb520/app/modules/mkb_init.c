/*
 * ===============================================================================
 * Copyright (C) 2017-2019
 * INNOPRESSO INC. All Rights Reserved
 *
 * The content of this file is copyrighted by INNOPRESSO or its
 * affiliated, associated or related companies. All copyrights and other
 * associated intellectual property rights related thereto are expressly
 * reserved by INNOPRESSO INC.
 * ===============================================================================
 */

#include <stdbool.h>
#include <stdint.h>
#include <stdarg.h>

#include "sdk_common.h"

#include "nrf_drv_clock.h"

#include "nrf_assert.h"
#include "nrf_error.h"
#include "nrf_delay.h"
#include "nrf_cli.h"
#include "nrf_balloc.h"
#include "nrf_pwr_mgmt.h"

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

#include "app_error.h"
#include "app_timer.h"
#include "app_scheduler.h"

#if defined(GPIOTE_ENABLED) && GPIOTE_ENABLED
#include "app_gpiote.h"
#endif

#define MKB_LOG_LEVEL MKB_INIT_LOG_LEVEL
#include "mkb_common.h"
#include "mkb_init.h"
#include "mkb_util.h"

#if (defined(MKB_BATTERY_ENABLED) && MKB_BATTERY_ENABLED)
#include "mkb_battery.h"
#endif

#if (defined(MKB_COMS_ENABLED) && MKB_COMS_ENABLED)
#include "mkb_coms.h"
#endif

#if (defined(MKB_FDS_ENABLED) && MKB_FDS_ENABLED)
#include "mkb_fds.h"
#endif

#if (defined(MKB_LED_ENABLED) && MKB_LED_ENABLED)
#include "mkb_led.h"
#endif

#if (defined(MKB_BUTTON_ENABLED) && MKB_BUTTON_ENABLED)
#include "mkb_button.h"
#endif

#if (defined(MKB_TWI_ENABLED) && MKB_TWI_ENABLED)
#include "drv_twi.h"
#endif

#if (defined(MKB_STM8_ENABLED) && MKB_STM8_ENABLED)
#include "drv_stm8.h"
#endif

#if (defined(MKB_AM2320_ENABLED) && MKB_AM2320_ENABLED)
#include "am2320.h"
#endif

#if (defined(MKB_EVENT_ENABLED) && MKB_EVENT_ENABLED)
#include "mkb_event.h"
#endif // (defined(MKB_EVENT_ENABLED) && MKB_EVENT_ENABLED)

#if (defined(MKB_TOUCHPAD_ENABLED) && MKB_TOUCHPAD_ENABLED)
#include "mkb_touchpad.h"
#endif // (defined(MKB_TOUCHPAD_ENABLED) && MKB_TOUCHPAD_ENABLED)

#if (defined(MKB_KEYBOARD_ENABLED) && MKB_KEYBOARD_ENABLED)
#include "mkb_keyboard.h"
#endif // (defined(MKB_KEYBOARD_ENABLED) && MKB_KEYBOARD_ENABLED)

#if (defined(MKB_COMS_ENABLED) && MKB_COMS_ENABLED)
#include "mkb_coms.h"
#endif // (defined(MKB_COMS_ENABLED) && MKB_COMS_ENABLED)

#if (defined(MKB_CLI_ENABLED) && MKB_CLI_ENABLED)
#include "mkb_cli.h"
#endif

#if (defined(MKB_WDT_ENABLED) && MKB_WDT_ENABLED)
#include "mkb_wdt.h"
#endif

extern void advertising_start(bool erase_bonds);

/**@brief Function for initializing the nrf log module.
 */
static void mkb_init_log(void)
{
    ret_code_t err_code = NRF_LOG_INIT(NULL);
    MKB_ERROR_CHECK(err_code);

    NRF_LOG_DEFAULT_BACKENDS_INIT();
}

/**@brief Function for the Timer initialization.
 *
 * @details Initializes the timer module.
 */
static void mkb_init_timers(void)
{
    ret_code_t err_code;

#if (defined(MKB_USB_ENABLED) && MKB_USB_ENABLED)
    err_code = nrf_drv_clock_init();
    MKB_ERROR_CHECK(err_code);

    nrf_drv_clock_lfclk_request(NULL);
    while (!nrf_drv_clock_lfclk_is_running())
    {
        /* Just waiting */
    }
#endif // (defined(MKB_USB_ENABLED) && MKB_USB_ENABLED)

    err_code = app_timer_init();
    MKB_ERROR_CHECK(err_code);
}

/**@brief Function for initializing power management.
 */
static void mkb_init_power_management(void)
{
    ret_code_t err_code;
    err_code = nrf_pwr_mgmt_init();
    MKB_ERROR_CHECK(err_code);
}

/**@brief Function for the Event Scheduler initialization.
 */
static void mkb_init_scheduler(void)
{
    APP_SCHED_INIT(SCHED_MAX_EVENT_DATA_SIZE, SCHED_QUEUE_SIZE);
}

#if (defined(MKB_AM2320_ENABLED) && MKB_AM2320_ENABLED)
void am2320_temperature_handler(ret_code_t status, void *p_samples)
{
    if (status == NRF_SUCCESS)
    {
        MKB_LOG_RAW_INFO("Temp: " NRF_LOG_FLOAT_MARKER "C\n", NRF_LOG_FLOAT(*((float *)p_samples)));
    }
}
void am2320_humidity_handler(ret_code_t status, void *p_samples)
{
    if (status == NRF_SUCCESS)
    {
        MKB_LOG_RAW_INFO("Humi: " NRF_LOG_FLOAT_MARKER "%c\n", NRF_LOG_FLOAT(*((float *)p_samples)), '%');
    }
}
#endif

/**@brief mokibo board initialize */
void mkb_init(void)
{
    ret_code_t err_code;

    mkb_init_log();
    mkb_init_timers();
    mkb_init_power_management();
    mkb_init_scheduler();

#if (defined(MKB_CLI_ENABLED) && MKB_CLI_ENABLED)
    mkb_cli_init();
    mkb_cli_start();
#endif

#if (defined(MKB_TWI_ENABLED) && MKB_TWI_ENABLED)
    err_code = twi_init();
    MKB_ERROR_CHECK(err_code);
#endif

#if (defined(MKB_COMS_ENABLED) && MKB_COMS_ENABLED)
    mkb_coms_init();
#endif // (defined(MKB_COMS_ENABLED) && MKB_COMS_ENABLED)

#if (defined(MKB_FDS_ENABLED) && MKB_FDS_ENABLED)
    mkb_fds_init();
#endif // (defined(MKB_FDS_ENABLED) && MKB_FDS_ENABLED)

#if (defined(MKB_DB_ENABLED) && MKB_DB_ENABLED)
    mkb_db_init();
#endif // (defined(MKB_DB_ENABLED) && MKB_DB_ENABLED)

// Initialize GPIOTE manager.
#if defined(GPIOTE_ENABLED) && GPIOTE_ENABLED
    APP_GPIOTE_INIT(APP_GPIOTE_MAX_USERS);
#else
#error "You have to enable GPIOTE at sdk_config.h!"
#endif

#if (defined(MKB_WDT_ENABLED) && MKB_WDT_ENABLED)
    mkb_wdt_init();
#endif

#if (defined(MKB_AM2320_ENABLED) && MKB_AM2320_ENABLED)
    am2320_init(am2320_temperature_handler, am2320_humidity_handler);
#endif

#if (defined(MKB_EVENT_ENABLED) && MKB_EVENT_ENABLED)
    mkb_event_init();
#endif // (defined(MKB_EVENT_ENABLED) && MKB_EVENT_ENABLED)

#if (defined(MKB_KEYBOARD_ENABLED) && MKB_KEYBOARD_ENABLED)
    mkb_keyboard_init();
#endif // (defined(MKB_KEYBOARD_ENABLED) && MKB_KEYBOARD_ENABLED)

#if (defined(MKB_BATTERY_ENABLED) && MKB_BATTERY_ENABLED)
    mkb_battery_init();
#endif

#if (defined(MKB_BUTTON_ENABLED) && MKB_BUTTON_ENABLED)
    mkb_button_init();
#endif

#if (defined(MKB_LED_ENABLED) && MKB_LED_ENABLED)
    mkb_led_init();
#endif

#if (defined(MKB_LED_ENABLED) && MKB_LED_ENABLED)
    mkb_led_ind_enable();
#endif

#if (defined(MKB_TOUCHPAD_ENABLED) && MKB_TOUCHPAD_ENABLED)
    mkb_touchpad_init();
#endif // (defined(MKB_TOUCHPAD_ENABLED) && MKB_TOUCHPAD_ENABLED)

    MKB_LOG_INFO("Initialization Success!");
}

/**@brief mokibo board start */
void mkb_start(void)
{
#if (defined(MKB_AM2320_ENABLED) && MKB_AM2320_ENABLED)
    am2320_enable();
#endif

#if (defined(MKB_BUTTON_ENABLED) && MKB_BUTTON_ENABLED)
    mkb_button_enable();
#endif

#if (defined(MKB_KEYBOARD_ENABLED) && MKB_KEYBOARD_ENABLED)
#if (defined(MKB_KEYBOARD_STARTUP_ENABLED) && MKB_KEYBOARD_STARTUP_ENABLED)
    bool erase_bonds;
    erase_bonds = mkb_keyboard_startup_check();
    if (!mkb_db_erase_bonds_get())
    {
        mkb_db_erase_bonds_set(erase_bonds);
    }
#endif //(defined(MKB_KEYBOARD_STARTUP_ENABLED) && MKB_KEYBOARD_STARTUP_ENABLED)

    mkb_keyboard_start();
#endif // (defined(MKB_KEYBOARD_ENABLED) && MKB_KEYBOARD_ENABLED)

#if (defined(MKB_BATTERY_ENABLED) && MKB_BATTERY_ENABLED)
    mkb_battery_start();
#endif

#if (defined(MKB_TOUCHPAD_ENABLED) && MKB_TOUCHPAD_ENABLED)
    mkb_touchpad_start();
#endif // (defined(MKB_TOUCHPAD_ENABLED) && MKB_TOUCHPAD_ENABLED)

#if (defined(MKB_WDT_ENABLED) && MKB_WDT_ENABLED)
    mkb_wdt_enable();
#endif

#if (defined(MKB_TOUCHPAD_UPDATE_ENABLED) && MKB_TOUCHPAD_UPDATE_ENABLED)
    if (mkb_touchpad_update_check())
    {
        mkb_event_send(EVT_SYSTEM_TOUCH_UPDATE, true);
    }
    else
    {
#if (defined(MKB_COMS_ENABLED) && MKB_COMS_ENABLED)
        mkb_coms_start();
#endif // (defined(MKB_COMS_ENABLED) && MKB_COMS_ENABLED)
    }
#else
#if (defined(MKB_COMS_ENABLED) && MKB_COMS_ENABLED)
    mkb_coms_start();
#endif // (defined(MKB_COMS_ENABLED) && MKB_COMS_ENABLED)
#endif // (defined(MKB_TOUCHPAD_UPDATE_ENABLED) && MKB_TOUCHPAD_UPDATE_ENABLED)
}