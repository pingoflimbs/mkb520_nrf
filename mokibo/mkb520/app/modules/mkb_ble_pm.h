/*
* ===============================================================================
* Copyright (C) 2017-2019
* INNOPRESSO INC. All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
* ===============================================================================
*/

#ifndef __MKB_BLE_PM__
#define __MKB_BLE_PM__

#include "mkb_config.h"
#include "mkb_db.h"

#define PM_BOND_JUSTWORK 0
#define PM_BOND_PASSKEY 1
#define PM_BOND_OOB 2

#if (MKB_PM_BOND_TYPE == PM_BOND_JUSTWORK)
#define SEC_PARAM_BOND 1                               /**< Perform bonding. */
#define SEC_PARAM_MITM 0                               /**< Man In The Middle protection not required. */
#define SEC_PARAM_LESC 0                               /**< LE Secure Connections not enabled. */
#define SEC_PARAM_KEYPRESS 0                           /**< Keypress notifications not enabled. */
#define SEC_PARAM_IO_CAPABILITIES BLE_GAP_IO_CAPS_NONE /**< No I/O capabilities. */
#define SEC_PARAM_OOB 0                                /**< Out Of Band data not available. */
#elif (MKB_PM_BOND_TYPE == PM_BOND_PASSKEY)
#define SEC_PARAM_BOND 1                                        /**< Perform bonding. */
#define SEC_PARAM_MITM 1                                        /**< Man In The Middle protection not required. */
#define SEC_PARAM_LESC 0                                        /**< LE Secure Connections not enabled. */
#define SEC_PARAM_KEYPRESS 0                                    /**< Keypress notifications not enabled. */
#define SEC_PARAM_IO_CAPABILITIES BLE_GAP_IO_CAPS_KEYBOARD_ONLY /**< No I/O capabilities. */
#define SEC_PARAM_OOB 0                                         /**< Out Of Band data not available. */
#elif (MKB_PM_BOND_TYPE == PM_BOND_OOB)
#define SEC_PARAM_BOND 1                               /**< Perform bonding. */
#define SEC_PARAM_MITM 1                               /**< Man In The Middle protection not required. */
#define SEC_PARAM_LESC 0                               /**< LE Secure Connections not enabled. */
#define SEC_PARAM_KEYPRESS 0                           /**< Keypress notifications not enabled. */
#define SEC_PARAM_IO_CAPABILITIES BLE_GAP_IO_CAPS_NONE /**< No I/O capabilities. */
#define SEC_PARAM_OOB 1                                /**< Out Of Band data not available. */
#endif

#define SEC_PARAM_MIN_KEY_SIZE 7  /**< Minimum encryption key size. */
#define SEC_PARAM_MAX_KEY_SIZE 16 /**< Maximum encryption key size. */

extern ret_code_t mkb_ble_pm_local_address_get(ble_gap_addr_t *p_addr);
extern void mkb_ble_pm_local_address_set(CHANNEL_ID_t ch);
extern void mkb_ble_pm_delete_peer(CHANNEL_ID_t ch);
extern void mkb_ble_pm_delete_bonds(void);
extern void mkb_ble_pm_whitelist_set(pm_peer_id_list_skip_t skip);
extern void mkb_ble_pm_whitelist_clear(void);
extern void mkb_ble_pm_identities_set(pm_peer_id_list_skip_t skip);
extern void mkb_ble_pm_identities_clear(void);
extern void mkb_ble_pm_init(void);
extern bool mkb_ble_pm_state(CHANNEL_ID_t ch);

#endif /* __MKB_BLE_PM__ */
