/*
 * ===============================================================================
 * Copyright (C) 2017-2019
 * INNOPRESSO INC. All Rights Reserved
 *
 * The content of this file is copyrighted by INNOPRESSO or its
 * affiliated, associated or related companies. All copyrights and other
 * associated intellectual property rights related thereto are expressly
 * reserved by INNOPRESSO INC.
 * ===============================================================================
 */

#include <stdbool.h>
#include <stdint.h>
#include <stdarg.h>
#include <stdlib.h>

#include "sdk_common.h"

#include "nrf_assert.h"
#include "nrf_error.h"
#include "nrf_log.h"
#include "nrf_delay.h"
#include "nrf_cli.h"
#include "nrf_balloc.h"
#include "nrf_pwr_mgmt.h"

#include "app_error.h"
#include "app_timer.h"
#include "app_scheduler.h"

#define MKB_LOG_LEVEL MKB_EVENT_LOG_LEVEL
#include "mkb_common.h"

#if (defined(MKB_EVENT_ENABLED) && MKB_EVENT_ENABLED)
#include "mkb_event.h"

#if (defined(MKB_SYSTEM_ENABLED) && MKB_SYSTEM_ENABLED)
#include "mkb_system.h"
#endif //(defined(MKB_SYSTEM_ENABLED) && MKB_SYSTEM_ENABLED)

#if (defined(MKB_COMS_ENABLED) && MKB_COMS_ENABLED)
#include "mkb_coms.h"
#endif //(defined(MKB_COMS_ENABLED) && MKB_COMS_ENABLED)

#if (defined(MKB_BLE_ENABLED) && MKB_BLE_ENABLED)
#include "mkb_ble.h"
#endif //(defined(MKB_BLE_ENABLED) && MKB_BLE_ENABLED)

// Define event allocator.
// NRF_BALLOC_DEF(s_event_pool, sizeof(mkb_event_t), MKB_EVENT_POOL_SIZE);

#define QUEUE_SIZE 64
APP_TIMER_DEF(m_event_timer);

typedef struct
{
    mkb_event_t event_datas[QUEUE_SIZE];
    uint8_t data_size;
    bool is_event_run;
} mkb_event_queue_t;

mkb_event_queue_t m_queue;

/**@brief Event Bus handlers. */
static const mkb_event_handler_t m_event_handlers[] =
    {
// m_system_state_event_handler,
#if (defined(MKB_SYSTEM_ENABLED) && MKB_SYSTEM_ENABLED)
        mkb_system_event_handler,
#endif //(defined(MKB_SYSTEM_ENABLED) && MKB_SYSTEM_ENABLED)

#if (defined(MKB_COMS_ENABLED) && MKB_COMS_ENABLED)
        mkb_coms_event_handler,
#endif //(defined(MKB_COMS_ENABLED) && MKB_COMS_ENABLED)

#if (defined(MKB_BLE_ENABLED) && MKB_BLE_ENABLED)
        mkb_ble_hid_event_handler,
#endif //(defined(MKB_BLE_ENABLED) && MKB_BLE_ENABLED)

#if (defined(MKB_LED_ENABLED) && MKB_LED_ENABLED)
        mkb_led_event_handler,
#endif
        NULL,

};

static void m_event_timer_handler();
static ret_code_t event_dequeue();
static ret_code_t event_enqueue(mkb_event_t event);

void mkb_event_init(void)
{
    ret_code_t ret_code;
    // ret_code = nrf_balloc_init(&s_event_pool);
    // APP_ERROR_CHECK(ret_code);
    app_timer_create(&m_event_timer, APP_TIMER_MODE_SINGLE_SHOT, m_event_timer_handler);

    MKB_LOG_INFO("Initialized and create %d event buffer pools", MKB_EVENT_POOL_SIZE);
}

ret_code_t mkb_event_send_ms(uint16_t ms, mkb_event_type_t event_type, ...)
{
    ________DBG_20211205_delayd_event("mkb_event_send_ms()");
    mkb_event_t event;
    { // cpy param to event

        uint8_t *p_event_data;
        va_list args;

        ________DBG_20210820_balloc_remains("mkb_event_send_ms()");

        va_start(args, event_type);
        event.type = event_type;
        event.delayed_ms = app_timer_cnt_get() + APP_TIMER_TICKS(ms);

        switch (EVENT_GROUP(event.type))
        {
            // mkb_system_event_handler,
        case EVT_GROUP_SYSTEM: // EVT_SYSTEM_BATTERY_LEVEL EVT_SYSTEM_SLEEP, EVT_SYSTEM_TOUCH_UPDATE
            event.system.data = (uint8_t)va_arg(args, uint32_t);
            ________LOG_20211205_mkb_event_qc("EVT_GROUP_SYSTEM, data:%d", event.system.data);
            break;

            // mkb_coms_event_handler,
        case EVT_GROUP_FUNC: // EVT_FUNC_UP, EVT_FUNC_DOWN, EVT_FUNC_HOLD,
            event.key.id = (uint8_t)va_arg(args, uint32_t);
            event.key.down_timestamp = (uint32_t)va_arg(args, uint32_t);
            ________LOG_20211205_mkb_event_qc("EVT_GROUP_FUNC, id:%d, timestamp:%d", event.key.id, event.key.down_timestamp);
            break;

            // mkb_coms_event_handler,
        case EVT_GROUP_KEY: // EVT_KEY_UP, EVT_KEY_DOWN, EVT_KEY_HOLD,
            event.key.id = (uint8_t)va_arg(args, uint32_t);
            event.key.down_timestamp = (uint32_t)va_arg(args, uint32_t);
            ________LOG_20211205_mkb_event_qc("EVT_GROUP_KEY, id:%d, timestamp:%d", event.key.id, event.key.down_timestamp);
            break;

            // mkb_coms_event_handler,
        case EVT_GROUP_BUTTON: // EVT_BUTTON_CLICK_LEFT, EVT_BUTTON_CLICK_RIGHT, EVT_BUTTON_TOUCH_LEFT, EVT_BUTTON_TOUCH_RIGHT,
            event.button.state = (bool)va_arg(args, uint32_t);
            ________LOG_20211205_mkb_event_qc("EVT_GROUP_BUTTON, state:%d", event.button.state);
            break;

            // mkb_coms_event_handler,
        case EVT_GROUP_TOUCH: // EVT_TOUCH_MOVE, EVT_TOUCH_SCROLL, EVT_TOUCH_TAB
            if ((event.type == EVT_TOUCH_MOVE) || (event.type == EVT_TOUCH_SCROLL))
            {
                event.touch.x = (int16_t)va_arg(args, int32_t);
                event.touch.y = (int16_t)va_arg(args, int32_t);
                ________LOG_20211205_mkb_event_qc("EVT_TOUCH_MOVE||SCROLL, x:%d, y:%d", event.touch.x, event.touch.y);
            }
            else if (event.type == EVT_TOUCH_TAB)
            {
                event.touch.tab = (uint8_t)va_arg(args, uint32_t);
                ________LOG_20211205_mkb_event_qc("EVT_TOUCH_TAB, tab:%d", event.touch.tab);
            }
            break;

            // mkb_coms_event_handler,
        case EVT_GROUP_TRACPAD: // EVT_TRACPAD_MOVE
            p_event_data = (uint8_t *)va_arg(args, uint32_t);
            event.tracpad.len = (uint8_t)va_arg(args, uint32_t);
            memcpy(event.tracpad.data, p_event_data, event.tracpad.len);
            ________LOG_20211205_mkb_event_qc("EVT_GROUP_TRACPAD, data:blabla len:%d", event.tracpad.len);
            break;

            // mkb_ble_hid_event_handler,
        case EVT_GROUP_HID: // EVT_HID_INPUT_KEYBOARD, EVT_HID_INPUT_BUTTON, EVT_HID_INPUT_CONSUMER,
            p_event_data = (uint8_t *)va_arg(args, uint32_t);
            event.hid.len = (uint8_t)va_arg(args, uint32_t);
            memcpy(event.hid.data, p_event_data, event.hid.len);
            ________LOG_20211205_mkb_event_qc("EVT_GROUP_HID, data:{%d,%d,%d}, len:%d", event.hid.data[0], event.hid.data[1], event.hid.data[2], event.hid.len);
            break;

            // mkb_led_ch_event_handler,
        case EVT_GROUP_LED: // EVT_LED_PWR_CHECK_BAT, EVT_LED_PWR_DOCK, EVT_LED_PWR_DETACH, EVT_LED_PWR_ALERT, EVT_LED_CH_SELECT, EVT_LED_CH_ADVERTISE, EVT_LED_CH_CONNECT, EVT_LED_CH_IND_OS,
            //�라미터(uint8_t act, uint8_t color, uint8_t numb) ????
            event.led.act = (uint8_t)va_arg(args, uint32_t);
            event.led.numb = (uint8_t)va_arg(args, uint32_t);
            event.led.color = (uint8_t)va_arg(args, uint32_t);
            ________LOG_20211205_mkb_event_qc("EVT_GROUP_LED, act:%d, numb:%d, color:%d", event.led.act, event.led.numb, event.led.color);
            break;

        default:
            ________DBG_20210820_balloc_remains("<<<<<<<<<<invalid event detected>>>>>>>>>>");
            ________LOG_20211205_mkb_event_qc("!!INVALID_EVT_GROUP!!");
            APP_ERROR_CHECK_BOOL(false);
            break;
        }
        va_end(args);
    }
    event_enqueue(event);
}

static ret_code_t event_enqueue(mkb_event_t new_event)
{
    ________DBG_20211205_delayd_event("event_enqueue()");
    new_event.is_waiting = true;

    { // check enqueable
        if (QUEUE_SIZE < m_queue.data_size)
        {
            // ERROR
            ________DBG_20211205_delayd_event("storage full error");
            return NRF_ERROR_STORAGE_FULL;
        }
    }

    { // ms ???????? enqueue
        for (uint8_t i = 0; i < QUEUE_SIZE; i++)
        {
            if (m_queue.event_datas[i].delayed_ms <= new_event.delayed_ms)
            {
                memmove(&(m_queue.event_datas[i + 1]), &(m_queue.event_datas[i]), sizeof(mkb_event_t) * (m_queue.data_size - i));
                memcpy(&(m_queue.event_datas[i]), &new_event, sizeof(mkb_event_t));
                m_queue.data_size++; // increase
                ________DBG_20211205_delayd_event_simple("queue++:%d/%d", i, m_queue.data_size);
                ________DBG_20211205_delayd_event("enqueue at [%d/%d] (event_delayd_ms(%d, %d))", i, m_queue.data_size, new_event.type, new_event.button.state);
                break;
            }
        }
    }

    { //???
#ifdef ________DBG_20211205_delayd_event_que_sprintf
        char logbuf[250];
        sprintf(logbuf, "+data_size:[%02d] isrun:[%d] curtime:[%04d] :", m_queue.data_size, m_queue.is_event_run, app_timer_cnt_get() % 10000);
        for (uint8_t i = 0; i < 10; i++)
        {
            if (m_queue.event_datas[i].is_waiting == true)
            {
                sprintf(logbuf, "%s|%04d", logbuf, m_queue.event_datas[i].delayed_ms % 10000);
            }
            else
            {
                sprintf(logbuf, "%s|  ", logbuf);
            }
        }
        ________DBG_20211205_delayd_event_que_sprintf("%s", logbuf);
#endif
    }

    { // run dequeue
        if (m_queue.is_event_run == false)
        {
            // event_dequeue();
            m_queue.is_event_run == true;
            ret_code_t err_code = app_timer_start(m_event_timer, APP_TIMER_TICKS(0), NULL);
            if (err_code != NRF_SUCCESS)
            {
                ________DBG_20211205_delayd_event("app_timer_fault");
            }
        }
    }
}

static void m_event_timer_handler()
{
    if (m_queue.data_size > 0)
    {
        event_dequeue();
        app_timer_start(m_event_timer, APP_TIMER_TICKS(1), NULL);
        ________DBG_20211205_delayd_event("timer_loop_run");
    }
    else
    {
        m_queue.is_event_run == false;
        ________DBG_20211205_delayd_event("timer_loop_end");
    }
}

static ret_code_t event_dequeue()
{
    ________DBG_20211205_delayd_event("event_dequeue()");

    //   0   1  2  3 queuesize=4  data_size=2
    //| 99| 49|  |  |

    for (int8_t i = QUEUE_SIZE - 1; i >= 0; i--)
    {
        // 30??? ???? ???.
        if ((m_queue.event_datas[i].delayed_ms < app_timer_cnt_get() || m_queue.event_datas[i].delayed_ms > app_timer_cnt_get() + APP_TIMER_TICKS(30000)) &&
            m_queue.event_datas[i].is_waiting == true)
        {
            { //??? ??
                const mkb_event_handler_t *p_handler = m_event_handlers;
                while (*p_handler != NULL)
                {
                    if ((*p_handler++)(&(m_queue.event_datas[i])))
                    {
                        break;
                    }
                }
            }

#ifdef ________DBG_20211205_delayd_event_que_sprintf
            char logbuf[250];
            sprintf(logbuf, "-data_size:[%02d] isrun:[%d] curtime:[%04d] :", m_queue.data_size, m_queue.is_event_run, app_timer_cnt_get() % 10000);
            for (int8_t i = 0; i < 10; i++)
            {
                if (m_queue.event_datas[i].is_waiting == true)
                {
                    sprintf(logbuf, "%s|%04d", logbuf, m_queue.event_datas[i].delayed_ms % 10000);
                }
                else
                {
                    sprintf(logbuf, "%s|  ", logbuf);
                }
            }
            ________DBG_20211205_delayd_event_que_sprintf("%s", logbuf);
#endif
            { //??? ??? ??
                memset(&(m_queue.event_datas[i]), 0, sizeof(mkb_event_t));
                if (m_queue.data_size > 0)
                {
                    m_queue.data_size--;
                }
                ________DBG_20211205_delayd_event_simple("queue--:%d/%d", i, m_queue.data_size);

                if (m_queue.data_size == 0)
                {
                    break;
                }
                ________DBG_20211205_delayd_event("dequeued success (data_size:%d)", m_queue.data_size);
            }
        }
    }
}

#if 1
ret_code_t mkb_event_send(mkb_event_type_t event_type, ...)
{
    mkb_event_t event;
    mkb_event_t *p_event = &event;
    const mkb_event_handler_t *p_handler = m_event_handlers;
    uint8_t *p_data;
    va_list args;

    ________DBG_20210820_balloc_remains(">mkb_event_send");

    va_start(args, event_type);
    p_event->type = event_type;

    switch (EVENT_GROUP(p_event->type))
    {
        // mkb_system_event_handler,
    case EVT_GROUP_SYSTEM: // EVT_SYSTEM_BATTERY_LEVEL EVT_SYSTEM_SLEEP, EVT_SYSTEM_TOUCH_UPDATE
        p_event->system.data = (uint8_t)va_arg(args, uint32_t);
        ________LOG_20211205_mkb_event_qc("EVT_GROUP_SYSTEM, data:%d", event.system.data);
        break;

        // mkb_coms_event_handler,
    case EVT_GROUP_FUNC: // EVT_FUNC_UP, EVT_FUNC_DOWN, EVT_FUNC_HOLD,
        p_event->key.id = (uint8_t)va_arg(args, uint32_t);
        p_event->key.down_timestamp = (uint32_t)va_arg(args, uint32_t);
        ________LOG_20211205_mkb_event_qc("EVT_GROUP_FUNC, id:%d, timestamp:%d", event.key.id, event.key.down_timestamp);
        break;

        // mkb_coms_event_handler,
    case EVT_GROUP_KEY: // EVT_KEY_UP, EVT_KEY_DOWN, EVT_KEY_HOLD,
        p_event->key.id = (uint8_t)va_arg(args, uint32_t);
        p_event->key.down_timestamp = (uint32_t)va_arg(args, uint32_t);
        ________LOG_20211205_mkb_event_qc("EVT_GROUP_KEY, id:%d, timestamp:%d", event.key.id, event.key.down_timestamp);
        break;

        // mkb_coms_event_handler,
    case EVT_GROUP_BUTTON: // EVT_BUTTON_CLICK_LEFT, EVT_BUTTON_CLICK_RIGHT, EVT_BUTTON_TOUCH_LEFT, EVT_BUTTON_TOUCH_RIGHT,
        p_event->button.state = (bool)va_arg(args, uint32_t);
        ________LOG_20211205_mkb_event_qc("EVT_GROUP_BUTTON, state:%d", event.button.state);
        break;

        // mkb_coms_event_handler,
    case EVT_GROUP_TOUCH: // EVT_TOUCH_MOVE, EVT_TOUCH_SCROLL, EVT_TOUCH_TAB
        if ((p_event->type == EVT_TOUCH_MOVE) || (p_event->type == EVT_TOUCH_SCROLL))
        {
            if (p_event->touch.x != 0 || p_event->touch.y != 0)
            {
            }
            p_event->touch.x = (int16_t)va_arg(args, int32_t);
            p_event->touch.y = (int16_t)va_arg(args, int32_t);
            ________LOG_20211205_mkb_event_qc("EVT_TOUCH_MOVE||SCROLL, x:%d, y:%d", event.touch.x, event.touch.y);
        }
        else if (p_event->type == EVT_TOUCH_TAB)
        {
            p_event->touch.tab = (uint8_t)va_arg(args, uint32_t);
            ________LOG_20211205_mkb_event_qc("EVT_TOUCH_TAB, tab:%d", event.touch.tab);
        }
        break;

        // mkb_coms_event_handler,
    case EVT_GROUP_TRACPAD: // EVT_TRACPAD_MOVE
        p_data = (uint8_t *)va_arg(args, uint32_t);
        p_event->tracpad.len = (uint8_t)va_arg(args, uint32_t);
        memcpy(p_event->tracpad.data, p_data, p_event->tracpad.len);
        ________LOG_20211205_mkb_event_qc("EVT_GROUP_TRACPAD, data:blabla len:%d", event.tracpad.len);
        break;

        // mkb_ble_hid_event_handler,
    case EVT_GROUP_HID: // EVT_HID_INPUT_KEYBOARD, EVT_HID_INPUT_BUTTON, EVT_HID_INPUT_CONSUMER,
        p_data = (uint8_t *)va_arg(args, uint32_t);
        p_event->hid.len = (uint8_t)va_arg(args, uint32_t);
        memcpy(p_event->hid.data, p_data, p_event->hid.len);
        ________LOG_20211205_mkb_event_qc("EVT_GROUP_HID, data:{%d,%d,%d}, len:%d", event.hid.data[0], event.hid.data[1], event.hid.data[2], event.hid.len);
        break;

        // mkb_led_ch_event_handler,
    case EVT_GROUP_LED: // EVT_LED_PWR_CHECK_BAT, EVT_LED_PWR_DOCK, EVT_LED_PWR_DETACH, EVT_LED_PWR_ALERT, EVT_LED_CH_SELECT, EVT_LED_CH_ADVERTISE, EVT_LED_CH_CONNECT, EVT_LED_CH_IND_OS,
        //�라미터(uint8_t act, uint8_t color, uint8_t numb)
        p_event->led.act = (uint8_t)va_arg(args, uint32_t);
        p_event->led.numb = (uint8_t)va_arg(args, uint32_t);
        p_event->led.color = (uint8_t)va_arg(args, uint32_t);
        ________LOG_20211205_mkb_event_qc("EVT_GROUP_LED, act:%d, numb:%d, color:%d", event.led.act, event.led.numb, event.led.color);
        break;

    default:
        ________DBG_20210820_balloc_remains("<<<<<<<<<<invalid event detected>>>>>>>>>>");
        ________LOG_20211205_mkb_event_qc("!!INVALID_EVT_GROUP!!");
        APP_ERROR_CHECK_BOOL(false);
        break;
    }

    va_end(args);

    ASSERT((p_event != NULL) && (p_handler != NULL));

    // Pass the event to handlers.
    while (*p_handler != NULL)
    {
        if ((*p_handler++)(p_event))
        {
            // Stop event processing if handler returned true.
            ________DBG_20210903_event_process_test("return true: p_handler:%d, event_type:%d, ", p_handler, (p_event->type));
            break;
        }
        else
        {
            ________DBG_20210903_event_process_test("return false: p_handler:%d, event_type:%d, ", p_handler, (p_event->type));
        }
    }
    return NRF_SUCCESS;
}

#else
ret_code_t mkb_event_send(mkb_event_type_t event_type, ...)
{
    mkb_event_t *p_event;
    uint8_t *p_data;
    va_list args;

    ________DBG_20210820_balloc_remains(">mkb_event_send");

    p_event = nrf_balloc_alloc(&s_event_pool);
    if (p_event == NULL)
    {
        MKB_LOG_WARN("Allocation error: Event lost!");
        return NRF_ERROR_NO_MEM;
    }

    va_start(args, event_type);

    p_event->type = event_type;

    switch (EVENT_GROUP(p_event->type))
    {
    case EVT_GROUP_SYSTEM: // EVT_SYSTEM_BATTERY_LEVEL EVT_SYSTEM_SLEEP, EVT_SYSTEM_TOUCH_UPDATE
        p_event->system.data = (uint8_t)va_arg(args, uint32_t);
        break;

    case EVT_GROUP_FUNC: // EVT_FUNC_UP, EVT_FUNC_DOWN, EVT_FUNC_HOLD,
        p_event->key.id = (uint8_t)va_arg(args, uint32_t);
        p_event->key.down_timestamp = (uint32_t)va_arg(args, uint32_t);
        break;

    case EVT_GROUP_KEY: // EVT_KEY_UP, EVT_KEY_DOWN, EVT_KEY_HOLD,
        p_event->key.id = (uint8_t)va_arg(args, uint32_t);
        p_event->key.down_timestamp = (uint32_t)va_arg(args, uint32_t);
        break;

    case EVT_GROUP_BUTTON: // EVT_BUTTON_CLICK_LEFT, EVT_BUTTON_CLICK_RIGHT, EVT_BUTTON_TOUCH_LEFT, EVT_BUTTON_TOUCH_RIGHT,
        p_event->button.state = (bool)va_arg(args, uint32_t);
        break;
        // mkb_system_event_handler,
        // mkb_coms_event_handler,
        // mkb_ble_hid_event_handler,
    case EVT_GROUP_TOUCH: // EVT_TOUCH_MOVE, EVT_TOUCH_SCROLL, EVT_TOUCH_TAB
        if ((p_event->type == EVT_TOUCH_MOVE) || (p_event->type == EVT_TOUCH_SCROLL))
        {
            p_event->touch.x = (int16_t)va_arg(args, int32_t);
            p_event->touch.y = (int16_t)va_arg(args, int32_t);
        }
        else if (p_event->type == EVT_TOUCH_TAB)
        {
            p_event->touch.tab = (uint8_t)va_arg(args, uint32_t);
        }
        break;

    case EVT_GROUP_TRACPAD: // EVT_TRACPAD_MOVE
        p_data = (uint8_t *)va_arg(args, uint32_t);
        p_event->tracpad.len = (uint8_t)va_arg(args, uint32_t);
        memcpy(p_event->tracpad.data, p_data, p_event->tracpad.len);
        break;

    case EVT_GROUP_HID: // EVT_HID_INPUT_KEYBOARD, EVT_HID_INPUT_BUTTON, EVT_HID_INPUT_CONSUMER,

        p_data = (uint8_t *)va_arg(args, uint32_t);
        p_event->hid.len = (uint8_t)va_arg(args, uint32_t);
        memcpy(p_event->hid.data, p_data, p_event->hid.len);
        break;
    default:
        ________DBG_20210820_balloc_remains("<<<<<<<<<<invalid event detected>>>>>>>>>>");
        APP_ERROR_CHECK_BOOL(false);
        break;
    }

    va_end(args);

    ret_code_t ret_code = app_sched_event_put(p_event, MKB_EVENT_SIZE, mkb_event_process);

    ________DBG_20210820_balloc_remains("start(state:%d)>evtgrp: %d(%d:%d), alloc:%d/%d, schd:%d", ret_code, p_event->type, p_event->type >> 8, p_event->type & 0x0F,
                                        nrf_balloc_utilization_get(&s_event_pool), nrf_balloc_max_utilization_get(&s_event_pool),
                                        app_sched_queue_space_get());

    MKB_ERROR_CHECK(ret_code);

    return ret_code;
}
#endif

ret_code_t mkb_event_cli(size_t size, char **params)
{
    ret_code_t err_code = NRF_SUCCESS;
    if (!strcmp(params[1], "help"))
    {
    }
    else if (!strcmp(params[1], "mkb_event_send"))
    {
        if (size == 3)
        {
            mkb_event_send((mkb_event_type_t)atoi(params[2]));
        }
        else if (size == 4)
        {
            mkb_event_send((mkb_event_type_t)atoi(params[2]), atoi(params[3]));
        }
        else if (size == 5)
        {
            mkb_event_send((mkb_event_type_t)atoi(params[2]), atoi(params[3]), atoi(params[4]));
        }
        else if (size == 6)
        {
            mkb_event_send((mkb_event_type_t)atoi(params[2]), atoi(params[3]), atoi(params[4]), atoi(params[5]));
        }
    }
    else
    {
        err_code = NRF_ERROR_INTERNAL;
    }
    return err_code;
}

#if (defined(NRF_PWR_MGMT_ENABLED) && NRF_PWR_MGMT_ENABLED)
static bool mkb_event_bus_log_statistics(nrf_pwr_mgmt_evt_t event)
{
    // MKB_LOG_INFO("Maximum Event Pool usage: %d entries",
    // nrf_balloc_max_utilization_get(&s_event_pool));

    return true;
}

NRF_PWR_MGMT_HANDLER_REGISTER(mkb_event_bus_log_statistics, SHUTDOWN_PRIORITY_STATISTICS);
#endif /* NRF_PWR_MGMT_ENABLED */

#endif // (defined(MKB_EVENT_ENABLED) && MKB_EVENT_ENABLED)
