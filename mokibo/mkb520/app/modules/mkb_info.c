/*
* ===============================================================================
* Copyright (C) 2017-2019
* INNOPRESSO INC. All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
* ===============================================================================
*/

#include <stdbool.h>
#include <stdint.h>
#include <stdarg.h>

#include "sdk_common.h"

#include "ble.h"
#include "ble_err.h"
#include "ble_conn_params.h"
#include "ble_bas.h"

#include "nrf_assert.h"
#include "nrf_error.h"
#include "nrf_delay.h"
#include "nrf_cli.h"
#include "nrf_balloc.h"
#include "nrf_pwr_mgmt.h"

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

#include "app_error.h"
#include "app_timer.h"
#include "app_scheduler.h"

#include "mkb_common.h"
#include "mkb_util.h"
#include "mkb_info.h"
#if (defined(MKB_BATTERY_ENABLED) && MKB_BATTERY_ENABLED)
#include "mkb_battery.h"
#endif //(defined(MKB_BATTERY_ENABLED) && MKB_BATTERY_ENABLED)

#define MKB_LOG_LEVEL MKB_BOOTING_LOG_LEVEL

/**@brief display mokibo system informations */
void mkb_info_system(void)
{
#if (defined(MKB_STM8_ENABLED) && MKB_STM8_ENABLED)
    STM8_VERSION_t stm8_version;

    stm8_version = mkb_db_stm8_version_get();
#endif

    MKB_LOG_RAW_INFO("********************************************************************\n");
    MKB_LOG_RAW_INFO("*                          MOKIBO                                  *\n");
    MKB_LOG_RAW_INFO("********************************************************************\n");
    MKB_LOG_RAW_INFO("* Model Name      : %s\n", MKB_MODEL_NAME);
    MKB_LOG_RAW_INFO("* MainPBA Version : %s\n", MKB_HW_VERSION);
    MKB_LOG_RAW_INFO("* FW Version      : %s\n", MKB_FW_VERSION);
    MKB_LOG_RAW_INFO("* Build Date/Time : %s / %s\n", MKB_BUILD_DATE, MKB_BUILD_TIME);
#if (defined(MKB_STM8_ENABLED) && MKB_STM8_ENABLED)
    MKB_LOG_RAW_INFO("* Clickbar FW Ver : 0x%02x(V%d.%d.%d)\n",
                     stm8_version.version,
                     stm8_version.maj,
                     stm8_version.min,
                     stm8_version.rev);
#endif
#if (defined(BOARD_MKB10040))
    MKB_LOG_RAW_INFO("* Board  Type: MKB10040\n");
#endif
#if (defined(BOARD_MKB10056))
    MKB_LOG_RAW_INFO("* Board  Type: MKB10056\n");
#endif
#if (defined(BOARD_PCA10040))
    MKB_LOG_RAW_INFO("* Board  Type: PCA10040\n");
#endif
#if (defined(BOARD_PCA10056))
    MKB_LOG_RAW_INFO("* Board  Type: PCA10040\n");
#endif
#if (defined(MKB_DB_ENABLED) && MKB_DB_ENABLED)
    MKB_LOG_RAW_INFO("* Reset Reason    : %s\n", mkb_util_reset_reason_str(mkb_db_reset_reason_get()));
#endif //(defined(MKB_DB_ENABLED) && MKB_DB_ENABLED)
    MKB_LOG_RAW_INFO("* Die temperature : " NRF_LOG_FLOAT_MARKER " Celsius\n", NRF_LOG_FLOAT(mkb_util_temp_get()));
#if (defined(MKB_BATTERY_ENABLED) && MKB_BATTERY_ENABLED)
    MKB_LOG_RAW_INFO("* Battery level   : %03d%%\n", mkb_battery_level_get());
#endif //(defined(MKB_BATTERY_ENABLED) && MKB_BATTERY_ENABLED)
    MKB_LOG_RAW_INFO("********************************************************************\n");
}

void mkb_info_config(void)
{
#if (defined(MKB_DB_ENABLED) && MKB_DB_ENABLED)
    MKB_DB_CONFIG_t *p_cfg = mkb_db_config_get();

    MKB_LOG_RAW_INFO("********************************************************************\n");
    MKB_LOG_RAW_INFO("*                    MOKIBO CONFIG                                 *\n");
    MKB_LOG_RAW_INFO("********************************************************************\n");
    MKB_LOG_RAW_INFO("* Magic  Code: 0x%08x\n", p_cfg->magic_code);
    MKB_LOG_RAW_INFO("* Device Name: %s\n", p_cfg->device_name);
    MKB_LOG_RAW_INFO("* Model  Name: %s\n", p_cfg->model_name);

#if (defined(MKB_PIXART_ENABLED) && MKB_PIXART_ENABLED)
    MKB_LOG_RAW_INFO("* Touch  Type: PixArt\n");
#endif
#if (defined(MKB_CYPRESS_ENABLED) && MKB_CYPRESS_ENABLED)
    MKB_LOG_RAW_INFO("* Touch  Type: Cypress\n");
#endif

    MKB_LOG_RAW_INFO("* Touch  Version: %d.%d (Major.Minor)\n", p_cfg->touch_fw_major, p_cfg->touch_fw_minor);
    MKB_LOG_RAW_INFO("********************************************************************\n");
#endif //(defined(MKB_DB_ENABLED) && MKB_DB_ENABLED)
}

void mkb_info_channel(void)
{
#if (defined(MKB_DB_ENABLED) && MKB_DB_ENABLED)
    CHANNEL_ID_t ch;
    CHANNEL_INFO_t *p_info;
    char str[256];
    CHANNEL_ID_t ch_current = mkb_db_ch_id_get();
    MKB_DB_CH_t *p_ch = (MKB_DB_CH_t *)mkb_db_ch_get();

    MKB_LOG_RAW_INFO("**************************************************************************************\n");
    MKB_LOG_RAW_INFO("*                                   MOKIBO CHANNEL                                   *\n");
    MKB_LOG_RAW_INFO("**************************************************************************************\n");
    MKB_LOG_RAW_INFO("* ID | TYPE |  CENTRAL   | STATE |  CONN | PEER ID | PEER      ADDR | OWN       ADDR *\n");
    MKB_LOG_RAW_INFO("*----+------+------------+-------+-------+---------+----------------+----------------*\n");

    for (ch = CHANNEL_ID_BT1; ch < CHANNEL_ID_MAX; ch++)
    {
        p_info = mkb_db_ch_info_get(ch);

        if (ch == ch_current)
            sprintf(str, "*>%2d<| %4s | %10s | %5d | %5d | %7d | 0x%02x%02x%02x%02x%02x%02x | 0x%02x%02x%02x%02x%02x%02x *\n",
                    ch, mkb_db_get_channel_type_str(p_info->type),
                    mkb_db_get_central_type_str(p_info->central),
                    p_info->state, p_ch->conn_handle[ch], p_info->peer_id,
                    p_info->peer_addr.addr[5], p_info->peer_addr.addr[4], p_info->peer_addr.addr[3],
                    p_info->peer_addr.addr[2], p_info->peer_addr.addr[1], p_info->peer_addr.addr[0],
                    p_info->own_addr.addr[5], p_info->own_addr.addr[4], p_info->own_addr.addr[3],
                    p_info->own_addr.addr[2], p_info->own_addr.addr[1], p_info->own_addr.addr[0]);
        else
            sprintf(str, "* %2d | %4s | %10s | %5d | %5d | %7d | 0x%02x%02x%02x%02x%02x%02x | 0x%02x%02x%02x%02x%02x%02x *\n",
                    ch, mkb_db_get_channel_type_str(p_info->type),
                    mkb_db_get_central_type_str(p_info->central),
                    p_info->state, p_ch->conn_handle[ch], p_info->peer_id,
                    p_info->peer_addr.addr[5], p_info->peer_addr.addr[4], p_info->peer_addr.addr[3],
                    p_info->peer_addr.addr[2], p_info->peer_addr.addr[1], p_info->peer_addr.addr[0],
                    p_info->own_addr.addr[5], p_info->own_addr.addr[4], p_info->own_addr.addr[3],
                    p_info->own_addr.addr[2], p_info->own_addr.addr[1], p_info->own_addr.addr[0]);

        MKB_LOG_RAW_INFO(str);
    }
    MKB_LOG_RAW_INFO("**************************************************************************************\n");
#endif //(defined(MKB_DB_ENABLED) && MKB_DB_ENABLED)
}
