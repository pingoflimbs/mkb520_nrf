/*
 * ===============================================================================
 * Copyright (C) 2017-2019
 * INNOPRESSO INC. All Rights Reserved
 *
 * The content of this file is copyrighted by INNOPRESSO or its
 * affiliated, associated or related companies. All copyrights and other
 * associated intellectual property rights related thereto are expressly
 * reserved by INNOPRESSO INC.
 * ===============================================================================
 */

#include <stdbool.h>
#include <stdint.h>
#include <stdarg.h>

#include "sdk_common.h"

#include "nrf_assert.h"
#include "nrf_error.h"
#include "nrf_delay.h"
#include "nrf_cli.h"
#include "nrf_balloc.h"
#include "nrf_pwr_mgmt.h"

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

#include "app_error.h"
#include "app_timer.h"
#include "app_scheduler.h"

#include "fds.h"

#define MKB_LOG_LEVEL MKB_FDS_LOG_LEVEL
#include "mkb_common.h"

#if (defined(MKB_FDS_ENABLED) && MKB_FDS_ENABLED)
#include "mkb_fds.h"

/* Keep track of the progress of a delete_all operation. */
static struct
{
    bool delete_next; //!< Delete next record.
    bool pending;     //!< Waiting for an fds FDS_EVT_DEL_RECORD event, to delete the next record.
} m_delete_all;

/* Flag to check fds initialization. */
static bool volatile m_fds_initialized;

/* Array to map FDS events to strings. */
static char const *fds_evt_str[] =
    {
        "FDS_EVT_INIT",
        "FDS_EVT_WRITE",
        "FDS_EVT_UPDATE",
        "FDS_EVT_DEL_RECORD",
        "FDS_EVT_DEL_FILE",
        "FDS_EVT_GC",
};

const char *mkb_fds_err_str(ret_code_t ret)
{
    /* Array to map FDS return values to strings. */
    static char const *err_str[] =
        {
            "FDS_ERR_OPERATION_TIMEOUT",
            "FDS_ERR_NOT_INITIALIZED",
            "FDS_ERR_UNALIGNED_ADDR",
            "FDS_ERR_INVALID_ARG",
            "FDS_ERR_NULL_ARG",
            "FDS_ERR_NO_OPEN_RECORDS",
            "FDS_ERR_NO_SPACE_IN_FLASH",
            "FDS_ERR_NO_SPACE_IN_QUEUES",
            "FDS_ERR_RECORD_TOO_LARGE",
            "FDS_ERR_NOT_FOUND",
            "FDS_ERR_NO_PAGES",
            "FDS_ERR_USER_LIMIT_REACHED",
            "FDS_ERR_CRC_CHECK_FAILED",
            "FDS_ERR_BUSY",
            "FDS_ERR_INTERNAL",
        };

    return err_str[ret - NRF_ERROR_FDS_ERR_BASE];
}

static fds_record_desc_t m_fds_desc_config = {0};
static fds_record_desc_t m_fds_desc_channel = {0};

// Simple event handler to handle errors during initialization.
static void fds_evt_handler(fds_evt_t const *p_fds_evt)
{
    if (p_fds_evt->result == NRF_SUCCESS)
    {
        MKB_LOG_DEBUG("Event: %s received (NRF_SUCCESS)",
                      fds_evt_str[p_fds_evt->id]);
    }
    else
    {
        MKB_LOG_ERROR("Event: %s received (%s)",
                      fds_evt_str[p_fds_evt->id],
                      mkb_fds_err_str(p_fds_evt->result));
    }

    switch (p_fds_evt->id)
    {
    case FDS_EVT_INIT:
    {
        if (p_fds_evt->result == NRF_SUCCESS)
        {
            m_fds_initialized = true;
        }
        else
        {
            // Initialization failed.
            MKB_LOG_ERROR("Initialization failed!");
        }
    }
    break;

    case FDS_EVT_WRITE:
    {
        if (p_fds_evt->result == NRF_SUCCESS)
        {
            MKB_LOG_DEBUG("Record ID:\t0x%04x", p_fds_evt->write.record_id);
            MKB_LOG_DEBUG("File ID:\t0x%04x", p_fds_evt->write.file_id);
            MKB_LOG_DEBUG("Record key:\t0x%04x", p_fds_evt->write.record_key);
        }
    }
    break;

    case FDS_EVT_UPDATE:
    {
        if (p_fds_evt->result == NRF_SUCCESS)
        {
            MKB_LOG_DEBUG("Record ID:\t0x%04x", p_fds_evt->write.record_id);
            MKB_LOG_DEBUG("File ID:\t0x%04x", p_fds_evt->write.file_id);
            MKB_LOG_DEBUG("Record key:\t0x%04x", p_fds_evt->write.record_key);
        }
    }
    break;

    case FDS_EVT_DEL_RECORD:
    {
        if (p_fds_evt->result == NRF_SUCCESS)
        {
            MKB_LOG_DEBUG("Record ID:\t0x%04x", p_fds_evt->del.record_id);
            MKB_LOG_DEBUG("File ID:\t0x%04x", p_fds_evt->del.file_id);
            MKB_LOG_DEBUG("Record key:\t0x%04x", p_fds_evt->del.record_key);
        }
        m_delete_all.pending = false;
    }
    break;

    default:
        break;
    }
}

/**@brief   Begin deleting all records, one by one. */
void fds_delete_all_begin(void)
{
    m_delete_all.delete_next = true;
}

bool fds_record_delete_next(void)
{
    fds_find_token_t tok = {0};
    fds_record_desc_t desc = {0};

    if (fds_record_iterate(&desc, &tok) == NRF_SUCCESS)
    {
        ret_code_t rc = fds_record_delete(&desc);
        if (rc != NRF_SUCCESS)
        {
            return false;
        }

        return true;
    }
    else
    {
        /* No records left to delete. */
        return false;
    }
}

/**@brief   Process a delete all command.
 *
 * Delete records, one by one, until no records are left.
 */
void fds_delete_all_process(void)
{
    if (m_delete_all.delete_next & !m_delete_all.pending)
    {
        MKB_LOG_DEBUG("Deleting next record.");

        m_delete_all.delete_next = fds_record_delete_next();
        if (!m_delete_all.delete_next)
        {
            MKB_LOG_DEBUG("No records left to delete.");
        }
    }
}

/**@brief   Wait for fds to initialize. */
extern void idle_state_handle(void);
static void fds_wait_for_ready(void)
{
    while (!m_fds_initialized)
    {
        idle_state_handle();
    }
}

ret_code_t mkb_fds_init(void)
{
    ret_code_t ret = fds_register(fds_evt_handler);
    if (ret != NRF_SUCCESS)
    {
        // Registering of the FDS event handler has failed.
        MKB_LOG_ERROR("FDS event handler registering failed!");
        MKB_ERROR_CHECK(ret);
        return ret;
    }

    m_fds_initialized = false;

    ret = fds_init();
    if (ret != NRF_SUCCESS)
    {
        MKB_LOG_ERROR("FDS init failed!");
        MKB_ERROR_CHECK(ret);
        return ret;
    }

    /* Wait for fds to initialize. */
    fds_wait_for_ready();

    MKB_LOG_INFO("Initialization Success!");

    return NRF_SUCCESS;
}

static void try_gc()
{
    static uint8_t freq_flag = 0;

    freq_flag = (freq_flag + 1) % 8;

    if (freq_flag == 1)
    {
        ret_code_t err_code = fds_gc();
        if (err_code == NRF_SUCCESS)
        {
            ________DBG_20211229_fds_test("GC SUCCESS");
        }
        else if (err_code == FDS_ERR_NO_SPACE_IN_QUEUES)
        {
            ________DBG_20211229_fds_test("GC ERROR : NO SPACE IN QUEUE");
        }
        else if (err_code == FDS_ERR_NOT_INITIALIZED)
        {
            ________DBG_20211229_fds_test("GC ERROR : NOT INITIALIZED");
        }
    }
}

ret_code_t mkb_fds_config_write(void *p_data, uint32_t length)
{
    ret_code_t rc;
    fds_record_t record;

    // Set up record.
    record.file_id = MKB_FDS_FILE_ID_MOKIBO;
    record.key = MKB_FDS_RECORD_ID_CONFIG;
    record.data.p_data = (void const *)p_data;
    record.data.length_words = length / 4; /* one word is four bytes. */

    rc = fds_record_write(&m_fds_desc_config, &record);
    if (rc == FDS_ERR_NO_SPACE_IN_FLASH)
    {
        MKB_LOG_ERROR("No space in flash, delete some records to update the config file.");
        ________DBG_20211229_fds_test("CONFIG WRITE ERROR : FDS_ERR_NO_SPACE_IN_FLASH");
        /*
                fds_record_delete(&m_fds_desc_config);
                rc = fds_record_write(&m_fds_desc_config, &record);
                if ((rc != NRF_SUCCESS) && (rc == FDS_ERR_NO_SPACE_IN_FLASH))
                {
                    MKB_LOG_ERROR("retry2 No space in flash, delete some records to update the config file.");
                    ________DBG_20211229_fds_test("FDS fail");
                }
                else
                {
                    MKB_ERROR_CHECK(rc);
                }
                */
    }
    else if (rc != NRF_SUCCESS)
    {
        ________DBG_20211229_fds_test("CHANNEL CONFIG ERROR : %d", rc);
        MKB_ERROR_CHECK(rc);
    }
    try_gc();

    return rc;
}

ret_code_t mkb_fds_config_update(void *p_data, uint32_t length)
{
    ret_code_t rc;
    fds_record_t record;

    // Set up record.
    record.file_id = MKB_FDS_FILE_ID_MOKIBO;
    record.key = MKB_FDS_RECORD_ID_CONFIG;
    record.data.p_data = (void const *)p_data;
    record.data.length_words = length / 4; /* one word is four bytes. */

    /* Write the updated record to flash. */
    rc = fds_record_update(&m_fds_desc_config, &record);
    if (rc == FDS_ERR_NO_SPACE_IN_FLASH)
    {
        MKB_LOG_ERROR("No space in flash, delete some records to update the config file.");
        ________DBG_20211229_fds_test("CONFIG UPDATE ERROR : FDS_ERR_NO_SPACE_IN_FLASH");

        /*

        fds_record_delete(&m_fds_desc_config);
        rc = fds_record_write(&m_fds_desc_config, &record);
        if ((rc != NRF_SUCCESS) && (rc == FDS_ERR_NO_SPACE_IN_FLASH))
        {
            MKB_LOG_ERROR("retry2 No space in flash, delete some records to update the config file.");
            ________DBG_20211229_fds_test("FDS fail");
        }
        else
        {
            MKB_ERROR_CHECK(rc);
        }
        */
    }
    else if (rc != NRF_SUCCESS)
    {
        ________DBG_20211229_fds_test("CHANNEL CONFIG ERROR : %d", rc);
        MKB_ERROR_CHECK(rc);
    }
    try_gc();
}

ret_code_t mkb_fds_config_read(void *p_data, uint32_t length)
{
    ret_code_t rc;

    /* A config file is in flash. Let's update it. */
    fds_flash_record_t config = {0};

    /* Open the record and read its contents. */
    rc = fds_record_open(&m_fds_desc_config, &config);
    if (rc != NRF_SUCCESS)
    {
        MKB_ERROR_CHECK(rc);
        return rc;
    }

    /* Copy the configuration from flash into p_data. */
    memcpy(p_data, config.p_data, length);

    /* Close the record when done reading. */
    rc = fds_record_close(&m_fds_desc_config);
    MKB_ERROR_CHECK(rc);
    return rc;
}

ret_code_t mkb_fds_config_find(void)
{
    fds_find_token_t tok = {0};

    ret_code_t rc;
    rc = fds_record_find(MKB_FDS_FILE_ID_MOKIBO, MKB_FDS_RECORD_ID_CONFIG, &m_fds_desc_config, &tok);

    return rc;
}

ret_code_t mkb_fds_channel_write(void *p_data, uint32_t length)
{
    ret_code_t rc;
    fds_record_t record;

    // Set up record.
    record.file_id = MKB_FDS_FILE_ID_MOKIBO;
    record.key = MKB_FDS_RECORD_ID_CHANNEL;
    record.data.p_data = (void const *)p_data;
    record.data.length_words = length / 4; /* one word is four bytes. */

    rc = fds_record_write(&m_fds_desc_channel, &record);
    if (rc == FDS_ERR_NO_SPACE_IN_FLASH)
    {
        MKB_LOG_INFO("No space in flash, delete some records to update the channel file.");
        ________DBG_20211229_fds_test("CHANNEL WRITE ERROR : FDS_ERR_NO_SPACE_IN_FLASH");
        /*
        fds_record_delete(&m_fds_desc_channel);
        rc = fds_record_write(&m_fds_desc_channel, &record);
        if ((rc != NRF_SUCCESS) && (rc == FDS_ERR_NO_SPACE_IN_FLASH))
        {
            MKB_LOG_ERROR("retry2 No space in flash, delete some records to update the config file.");
            ________DBG_20211229_fds_test("FDS fail");
        }
        else
        {
            MKB_ERROR_CHECK(rc);
        }jjjjjjftjjjjjjeftryg;tft]idatejeft
        */
    }
    else if (rc != NRF_SUCCESS)
    {
        ________DBG_20211229_fds_test("CHANNEL WRITE ERROR : %d", rc);
        MKB_ERROR_CHECK(rc);
    }
    try_gc();

    return rc;
}

ret_code_t mkb_fds_channel_update(void *p_data, uint32_t length)
{
    ret_code_t rc;
    fds_record_t record;

    // Set up record.
    record.file_id = MKB_FDS_FILE_ID_MOKIBO;
    record.key = MKB_FDS_RECORD_ID_CHANNEL;
    record.data.p_data = (void const *)p_data;
    record.data.length_words = length / 4; /* one word is four bytes. */

    /* Write the updated record to flash. */

    rc = fds_record_update(&m_fds_desc_channel, &record);
    if (rc == FDS_ERR_NO_SPACE_IN_FLASH)
    {

        MKB_LOG_ERROR("No space in flash, delete some records to update the config file.");
        ________DBG_20211229_fds_test("CHANNEL UPDATE ERROR : FDS_ERR_NO_SPACE_IN_FLASH");
        /*
                fds_record_delete(&m_fds_desc_channel);
                rc = fds_record_update(&m_fds_desc_channel, &record);
                if ((rc != NRF_SUCCESS) && (rc == FDS_ERR_NO_SPACE_IN_FLASH))
                {
                    MKB_LOG_ERROR("retry2 No space in flash, delete some records to update the config file.");
                    ________DBG_20211229_fds_test("FDS fail");
                }
                else
                {
                    MKB_ERROR_CHECK(rc);
                }
                */
    }
    else if (rc != NRF_SUCCESS)
    {
        ________DBG_20211229_fds_test("CHANNEL WRITE ERROR : %d", rc);
        MKB_ERROR_CHECK(rc);
    }
    try_gc();
}

ret_code_t mkb_fds_channel_read(void *p_data, uint32_t length)
{
    ret_code_t rc;

    /* A config file is in flash. Let's update it. */
    fds_flash_record_t channel = {0};

    /* Open the record and read its contents. */
    rc = fds_record_open(&m_fds_desc_channel, &channel);
    if (rc != NRF_SUCCESS)
    {
        MKB_ERROR_CHECK(rc);
        return rc;
    }

    /* Copy the configuration from flash into p_data. */
    memcpy(p_data, channel.p_data, length);

    /* Close the record when done reading. */
    rc = fds_record_close(&m_fds_desc_channel);
    MKB_ERROR_CHECK(rc);
    return rc;
}

ret_code_t mkb_fds_channel_find(void)
{
    fds_find_token_t tok = {0};

    ret_code_t rc;
    rc = fds_record_find(MKB_FDS_FILE_ID_MOKIBO, MKB_FDS_RECORD_ID_CHANNEL, &m_fds_desc_channel, &tok);

    return rc;
}

#endif //(defined(MKB_FDS_ENABLED) && MKB_FDS_ENABLED)
