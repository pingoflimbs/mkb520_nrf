/*
 * ===============================================================================
 * Copyright (C) 2017-2019
 * INNOPRESSO INC. All Rights Reserved
 *
 * The content of this file is copyrighted by INNOPRESSO or its
 * affiliated, associated or related companies. All copyrights and other
 * associated intellectual property rights related thereto are expressly
 * reserved by INNOPRESSO INC.
 * ===============================================================================
 */

#ifndef __MKB_COMMON__
#define __MKB_COMMON__

#include "mkb_config.h"
#include "mkb_util.h"

#define MKB_BUILD_DATE __DATE__
#define MKB_BUILD_TIME __TIME__

APP_TIMER_DEF(m_common_timer);
static uint32_t m_common_system_time;

#if (defined(MKB_ERROR_CHECK_ENABLED) && (MKB_ERROR_CHECK_ENABLED))
#define MKB_ERROR_CHECK(err_code) APP_ERROR_CHECK(err_code)
#define MKB_ERROR_HANDLER(err_code) APP_ERROR_HANDLER(err_code)
#else
#include "nrf_log_ctrl.h"
#define MKB_ERROR_CHECK(err_code)                      \
    do                                                 \
    {                                                  \
        if (err_code != NRF_SUCCESS)                   \
        {                                              \
            MKB_LOG_ERROR("Error Code: %d", err_code); \
            NRF_LOG_FLUSH();                           \
        }                                              \
    } while (0)

#define MKB_ERROR_HANDLER(err_code)                \
    do                                             \
    {                                              \
        MKB_LOG_ERROR("Error Code: %d", err_code); \
        NRF_LOG_FLUSH();                           \
    } while (0)
#endif

#define MKB_LOG_LEVEL_NONE 0    //=> None
#define MKB_LOG_LEVEL_ERROR 1   //=> Error
#define MKB_LOG_LEVEL_WARN 2    //=> Warning
#define MKB_LOG_LEVEL_INFO 3    //=> Info
#define MKB_LOG_LEVEL_DEBUG 4   //=> Debug
#define MKB_LOG_LEVEL_DEFAULT 5 //=> Default

#define __FILENAME__ (strchr(__FILE__, '\\') ? strrchr(__FILE__, '\\') + 1 : strrchr(__FILE__, '/') + 1) // string.h

#if (defined(MKB_LOG_ENABLED) && MKB_LOG_ENABLED)
#include "SEGGER_RTT.h"
#ifndef MKB_LOG_LEVEL
#define MKB_LOG_LEVEL MKB_LOG_DEFAULT_LEVEL
#endif

#define MKB_LOG(level, fmt, ...)                                                                                                                   \
    do                                                                                                                                             \
    {                                                                                                                                              \
        uint8_t max_level = MKB_LOG_LEVEL;                                                                                                         \
        if (max_level == MKB_LOG_LEVEL_DEFAULT)                                                                                                    \
        {                                                                                                                                          \
            max_level = MKB_LOG_DEFAULT_LEVEL;                                                                                                     \
        }                                                                                                                                          \
        if (level > 0 && (level <= max_level))                                                                                                     \
        {                                                                                                                                          \
            SEGGER_RTT_SetTerminal(MKB_LOG_TERMINAL_ID);                                                                                           \
            char dbg_str[256];                                                                                                                     \
            sprintf(dbg_str, "[%4d:%14.14s:%-18.18s:%04d] " fmt "\n", mkb_util_get_tick_delta(), __FILENAME__, __func__, __LINE__, ##__VA_ARGS__); \
            switch (level)                                                                                                                         \
            {                                                                                                                                      \
            case MKB_LOG_LEVEL_ERROR:                                                                                                              \
                SEGGER_RTT_printf(0, "E:%s", dbg_str);                                                                                             \
                break;                                                                                                                             \
            case MKB_LOG_LEVEL_WARN:                                                                                                               \
                SEGGER_RTT_printf(0, "W:%s", dbg_str);                                                                                             \
                break;                                                                                                                             \
            case MKB_LOG_LEVEL_INFO:                                                                                                               \
                SEGGER_RTT_printf(0, "I:%s", dbg_str);                                                                                             \
                break;                                                                                                                             \
            case MKB_LOG_LEVEL_DEBUG:                                                                                                              \
                SEGGER_RTT_printf(0, "D:%s", dbg_str);                                                                                             \
                break;                                                                                                                             \
            }                                                                                                                                      \
        }                                                                                                                                          \
    } while (0)

#define MKB_LOG_ERROR(fmt, ...) MKB_LOG(MKB_LOG_LEVEL_ERROR, fmt, ##__VA_ARGS__)
#define MKB_LOG_WARN(fmt, ...) MKB_LOG(MKB_LOG_LEVEL_WARN, fmt, ##__VA_ARGS__)
#define MKB_LOG_INFO(fmt, ...) MKB_LOG(MKB_LOG_LEVEL_INFO, fmt, ##__VA_ARGS__)
#define MKB_LOG_DEBUG(fmt, ...) MKB_LOG(MKB_LOG_LEVEL_DEBUG, fmt, ##__VA_ARGS__)

#define MKB_LOG_RAW_INFO(fmt, ...)                       \
    do                                                   \
    {                                                    \
        uint8_t level = MKB_LOG_LEVEL;                   \
        if (level == MKB_LOG_LEVEL_DEFAULT)              \
        {                                                \
            level = MKB_LOG_DEFAULT_LEVEL;               \
        }                                                \
        if (level > 0 && (level <= MKB_LOG_LEVEL_INFO))  \
        {                                                \
            SEGGER_RTT_SetTerminal(MKB_LOG_TERMINAL_ID); \
            SEGGER_RTT_printf(0, fmt, ##__VA_ARGS__);    \
        }                                                \
    } while (0)

#define MKB_LOG_RAW_DEBUG(fmt, ...)                      \
    do                                                   \
    {                                                    \
        uint8_t level = MKB_LOG_LEVEL;                   \
        if (level == MKB_LOG_LEVEL_DEFAULT)              \
        {                                                \
            level = MKB_LOG_DEFAULT_LEVEL;               \
        }                                                \
        if (level > 0 && (level <= MKB_LOG_LEVEL_DEBUG)) \
        {                                                \
            SEGGER_RTT_SetTerminal(MKB_LOG_TERMINAL_ID); \
            SEGGER_RTT_printf(0, fmt, ##__VA_ARGS__);    \
        }                                                \
    } while (0)

#define MKB_DEBUG_LOG(fmt, ...)                                                                                                                    \
    do                                                                                                                                             \
    {                                                                                                                                              \
        if (MKB_DEBUG_LOG_ENABLE > 0)                                                                                                              \
        {                                                                                                                                          \
            SEGGER_RTT_SetTerminal(0);                                                                                                             \
            char dbg_str[256];                                                                                                                     \
            sprintf(dbg_str, "[%4d:%14.14s:%-18.18s:%04d] " fmt "\n", mkb_util_get_tick_delta(), __FILENAME__, __func__, __LINE__, ##__VA_ARGS__); \
            SEGGER_RTT_printf(0, "E:%s", dbg_str);                                                                                                 \
        }                                                                                                                                          \
    } while (0)

#define MKB_RAW_DEBUG_LOG(fmt, ...)                  \
    do                                               \
    {                                                \
        SEGGER_RTT_SetTerminal(MKB_LOG_TERMINAL_ID); \
        SEGGER_RTT_printf(0, fmt, ##__VA_ARGS__);    \
    } while (0)

#else
#define MKB_LOG_ERROR(fmt, ...)
#define MKB_LOG_WARN(fmt, ...)
#define MKB_LOG_INFO(fmt, ...)
#define MKB_LOG_DEBUG(fmt, ...)
#define MKB_LOG_RAW_INFO(fmt, ...)
#define MKB_LOG_RAW_DEBUG(fmt, ...)
#endif // (defined(MKB_LOG_ENABLED) && MKB_LOG_ENABLED)

#if NRF_PWR_MGMT_ENABLED
#define SHUTDOWN_PRIORITY_EARLY 0
#define SHUTDOWN_PRIORITY_DEFAULT 1
#define SHUTDOWN_PRIORITY_LATE 2
#define SHUTDOWN_PRIORITY_STATISTICS 3
#define SHUTDOWN_PRIORITY_FINAL 4

#if (NRF_PWR_MGMT_CONFIG_HANDLER_PRIORITY_COUNT != 5)
#error "Unsupported NRF_PWR_MGMT_CONFIG_HANDLER_PRIORITY_COUNT value!"
#endif /* (NRF_PWR_MGMT_CONFIG_HANDLER_PRIORITY_COUNT != 5) */
#endif /* NRF_PWR_MGMT_ENABLED */

#if (defined(MKB_STM8_ENABLED) && MKB_STM8_ENABLED)
/**
 * drv_cypress:                     1
 * drv_keyboard:                    1
 * drv_stm8:                        1
 * ------------------------------------
 * TOTAL:                           3
 */
#define APP_GPIOTE_MAX_USERS 4
#else
/**@brief Number of modules using app_gpiote.
 *
 * drv_cypress:                     1
 * drv_keyboard:                    1
 * ------------------------------------
 * TOTAL:                           2
 */
#define APP_GPIOTE_MAX_USERS 2
#endif // (defined(MKB_STM8_ENABLED) && MKB_STM8_ENABLED)
#define MKB_SCANTIME(time) time * 10

#if (defined(MKB_DB_ENABLED) && MKB_DB_ENABLED)
#include "mkb_db.h"
#endif // (defined(MKB_DB_ENABLED) && MKB_DB_ENABLED)
#endif /* __MKB_COMMON__ */

#define ________DBG_tmp_3scroll_test(fmt, ...) // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)
/*______ _________          _______  _______ _________
(  ____ )\__   __/|\     /|(  ___  )(  ____ )\__   __/
| (    )|   ) (   ( \   / )| (   ) || (    )|   ) (
| (____)|   | |    \ (_) / | (___) || (____)|   | |
|  _____)   | |     ) _ (  |  ___  ||     __)   | |
| (         | |    / ( ) \ | (   ) || (\ (      | |
| )      ___) (___( /   \ )| )   ( || ) \ \__   | |
|/       \_______/|/     \||/     \||/   \__/   )*/
//#define ________DBG_20210801_pct_packet_raw_sprint(fmt, ...) MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)
//#define ________DBG_20210801_pct_touch_packet_raw_sprint(fmt, ...) MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)
//#define ________DBG_20210801_pct_touch_packet_value_sprint(fmt, ...) MKB_RAW_DEBUG_LOG(fmt, ##__VA_ARGS__) //패킷을 헥사말고 값으로 푼다
//#define ________DBG_20211117_check_for_i2c_stop_sprintf(fmt, ...) MKB_DEBUG_LOG(fmt, ##__VA_ARGS__) // pixart status message와 함께쓰자
//#define ________DBG_20220215_asdf_problem(fmt, ...) MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)

#define ________DBG_20210801_pct_packet_raw_gstr_packet(fmt, ...) // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)
#define ________DBG_20210801_pct_keyboard_raw(fmt, ...)           // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)

#define ________DBG_20210801_pct_gesture_raw(fmt, ...) // MKB_DEBUG_LOG(fmt,##__VA_ARGS__)

#define ________DBG_20210729_pct_gesture_detect(fmt, ...)  // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)
#define ________DBG_20210729_pct_show_delta(fmt, ...)      // MKB_DEBUG_LOG(fmt,##__VA_ARGS__)
#define ________DBG_20210726_touchdb_log(fmt, ...)         // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)
#define ________DBG_20210716_touch_hid_log(fmt, ...)       // MKB_DEBUG_LOG(fmt,##__VA_ARGS__)
#define ________DBG_20211014_pixart_scanhandler(fmt, ...)  // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)
#define ________DBG_20211014_pixart_four_gesture(fmt, ...) // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)

#define ________DBG_20210810_pixart_boot_ready_message(fmt, ...) // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)

#define ________DBG_20210809_rx_usr_test(fmt, ...) // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)

#define ________DBG_20210818_pct_debug_log(fmt, ...)             // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)
#define ________DBG_20210818_pct_debug_log_on_3release(fmt, ...) // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)
#define ________DBG_20210826_ttint(fmt, ...)                     // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)
#define ________DBG_20210811_pct_i2c_dfu_test(fmt, ...) MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)

#define ________DBG_20210810_pixart_status_message(fmt, ...) // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)

#define ________DBG_20211202_drv_pixart_sense_trace(fmt, ...) // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__) // pixart status message와 함께쓰자

#define ________DBG_20211117_check_for_i2c_stop(fmt, ...) // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__) // pixart status message와 함께쓰자
#define ________DBG_20211130_check_for_i2c_stop(fmt, ...) // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__) // pixart status message와 함께쓰자
#define ________DBG_20211130_wait_timer(fmt, ...)         // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__) // pixart status message와 함께쓰자

#define ________DBG_20211117_check_for_i2c_stop_race_check(fmt, ...) // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__) // pixart status message와 함께쓰자

#define ________DBG_20211130_check_for_i2c_app_sched(fmt, ...) // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)

#define ________DBG_20211130_check_for_i2c_int_high(fmt, ...) // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)

#define ________DBG_20211130_check_for_i2c_timer_triggerd(fmt, ...) // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)

#define ________DBG_20211130_check_for_i2c_new_scanner(fmt, ...) // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)

#define ________DBG_20211130_check_for_i2c_watchdog(fmt, ...) // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)
#define ________DBG_20211130_otherhand_flag(fmt, ...)         // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)

/*________ _______           _______
\__   __/(  ___  )|\     /|(  ____ \|\     /|
   ) (   | (   ) || )   ( || (    \/| )   ( |
   | |   | |   | || |   | || |      | (___) |
   | |   | |   | || |   | || |      |  ___  |
   | |   | |   | || |   | || |      | (   ) |
   | |   | (___) || (___) || (____/\| )   ( |
   )_(   (_______)(_______)(_______/|/     */

//#define ________DBG_20210729_pct_gesture_enqueued_sprint(fmt, ...) MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)
//#define ________DBG_20211119_finger_cnt_sprint(fmt, ...) MKB_DEBUG_LOG(fmt, ##__VA_ARGS__) // enqueue 중에 프린트
//#define ________DBG_20220303_touch_gesture_and_xy_data_sprintf(fmt, ...) MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)

#define ________DBG_20220318_fngr_process_queue(fmt, ...) // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)
#define ________DBG_20211119_mkb_touchdb_clean(fmt, ...)  // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)
//#define ________DBG_20220314_mkb_mac_scroll_sprintf(fmt, ...)     MKB_RAW_DEBUG_LOG(fmt, ##__VA_ARGS__)

#define ________DBG_20211025_touch_handlers(fmt, ...)                // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)
#define ________DBG_20210810_pixart_onejob_check(fmt, ...)           // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)
#define ________DBG_20210818_pct_debug_log_on_3release_ble(fmt, ...) // MKB_LOG_RAW_DEBUG(fmt, ##__VA_ARGS__)

#define ________DBG_20210810_nrf_driven_tt_threshold(fmt, ...) // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)
#define ________DBG_20210810_doubletap(fmt, ...)               // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)
#define ________DBG_20210810_keytouch(fmt, ...)                // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)

#define ________DBG_20210810_nrf_tt(fmt, ...) // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)

#define ________DBG_20210810_redbtn_click_down(fmt, ...) // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)

#define ________DBG_20210810_click_down_in_gstr(fmt, ...) // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)

#define ________DBG_20211028_block_time_gstr(fmt, ...) // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)

#define ________DBG_20211028_dynamic_aamode(fmt, ...) // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)

#define ________DBG_20211119_mkb_touch_detect_fgr(fmt, ...)  // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)
#define ________DBG_20211119_mkb_touch_detect_idle(fmt, ...) // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)

#define ________DBG_20211119_mkb_touch_detect_idle_timeout_handler(fmt, ...) // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)

#define ________DBG_20211119_mkb_touch_dir_test(fmt, ...) // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)

#define ________DBG_20211119_mkb_touch_drag_test(fmt, ...)        // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)
#define ________DBG_20211119_mkb_touch_3swipe_test(fmt, ...)      // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)
#define ________DBG_20211119_mkb_touch_3up_test(fmt, ...)         // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)
#define ________DBG_20211119_mkb_touch_2swip_test(fmt, ...)       // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)
#define ________DBG_20211119_mkb_touch_aamode_test(fmt, ...)      // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)
#define ________DBG_20211119_mkb_touch_zoom_test(fmt, ...)        // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)
#define ________DBG_20211119_mkb_touch_recentspeed_test(fmt, ...) // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)
#define ________DBG_20211119_mkb_touch_1fgr_accel_test(fmt, ...)  // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)

#define ________DBG_20211119_mkb_finger_continus_test(fmt, ...) // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)

#define ________DBG_20211119_mkb_finger_areasize_test(fmt, ...) // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)

#define ________DBG_20211119_mkb_finger_deltas_test(fmt, ...) // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)
#define ________DBG_20211119_mkb_finger_angle_test(fmt, ...)  // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)
#define ________DBG_20211119_directions_cnt_sprint(fmt, ...)  // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)
#define ________DBG_20211119_pinch_cnt_sprint(fmt, ...)       // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)

#define ________DBG_20211201_idle_process_datas(fmt, ...) // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)
#define ________DBG_20211201_idle_process_datas_csv(fmt, ...) MKB_RAW_DEBUG_LOG(fmt, ##__VA_ARGS__)
#define ________DBG_20211201_fngr_process_datas(fmt, ...) // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)
#define ________DBG_20211201_idle_detect(fmt, ...)        // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)

#define ________DBG_20211201_fngr_1_posxy_vs_gstxy(fmt, ...)   // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)
#define ________DBG_20211201_idle_1_tap_flag(fmt, ...)         // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)
#define ________DBG_20211201_fngr_2_y_flag(fmt, ...)           // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)
#define ________DBG_20211201_idle_2_swipe(fmt, ...)            // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)
#define ________DBG_20211201_idle_2_pinch(fmt, ...)            // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)
#define ________LOG_20211205_fngr_1_jitter(fmt, ...)           // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)
#define ________LOG_20211205_fngr_2_y_reactivity(fmt, ...)     // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)
#define ________LOG_20220216_fngr_2_y_readableity(fmt, ...)    // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)
#define ________LOG_20220216_fngr_2_xy_pinch(fmt, ...)         // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)
#define ________LOG_20220216_fngr_3_xy_scroll(fmt, ...)        // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)
#define ________LOG_20220216_touch_pos_compensate_xy(fmt, ...) // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)
#define ________LOG_20220216_pointing_sensitive_test(fmt, ...) // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)
#define ________LOG_20220216_pointing_slow(fmt, ...)           // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)
#define ________LOG_20220225_pointing_speed_xy(fmt, ...)       // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)
#define ________DBG_20211119_mkb_touch_fngr_3up_test(fmt, ...) // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)

#define ________DBG_20220302_raw_vs_gstr_vs_comp(fmt, ...) // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)
#define ________DBG_20220302_test3move(fmt, ...)           // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)

#define ________DBG_20211119_mkb_touch_fgr2_detect(fmt, ...) // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)
#define ________DBG_20220314_mkb_fngr_2_y_scroll(fmt, ...)   // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)

#define ________DBG_20220314_mkb_fngr_2_y_scroll_combo(fmt, ...) // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)

#define ________DBG_20220314_mkb_mac_scroll(fmt, ...) // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)

#define ________DBG_20220314_mkb_inertia_scroll(fmt, ...)        // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)
#define ________DBG_20220314_mkb_mac_scroll_init_accel(fmt, ...) // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)
#define ________DBG_20220314_mkb_and_scroll(fmt, ...)            // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)

#define ________DBG_20220314_fgr2_x_scroll_detect(fmt, ...)      // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)
#define ________DBG_20220314_deltaarea_finger_sumy(fmt, ...)     // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)
#define ________DBG_20220314_deltaarea_finger_summ_raw(fmt, ...) // MKB_RAW_DEBUG_LOG(fmt, ##__VA_ARGS__)

#define ________DBG_20220314_mkb_fngr_n_typing(fmt, ...) MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)
//#define                                                          ________DBG_20220314_idle_module_refactory(fmt, ...)     // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)

/*        _______           ______   _______  _______  _______  ______
| \    /\(  ____ \|\     /|(  ___ \ (  ___  )(  ___  )(  ____ )(  __  \
|  \  / /| (    \/( \   / )| (   ) )| (   ) || (   ) || (    )|| (  \  )
|  (_/ / | (__     \ (_) / | (__/ / | |   | || (___) || (____)|| |   ) |
|   _ (  |  __)     \   /  |  __ (  | |   | ||  ___  ||     __)| |   | |
|  ( \ \ | (         ) (   | (  \ \ | |   | || (   ) || (\ (   | |   ) |
|  /  \ \| (____/\   | |   | )___) )| (___) || )   ( || ) \ \__| (__/  )
|_/    \/(_______/   \_/   |/ \___/ (_______)|/     \||/   \__/(______*/
// CLI LOG
#define ________DBG_20210730_ble_key_buffer(fmt, ...)         // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)
#define ________DBG_20210802_kbd_func_check(fmt, ...)         // MKB_DEBUG_LOG(fmt,##__VA_ARGS__)
#define ________DBG_20210802_kbdprocess_space(fmt, ...)       // MKB_DEBUG_LOG(fmt,##__VA_ARGS__)
#define ________DBG_20210802_kbdprocess_time(fmt, ...)        // MKB_DEBUG_LOG(fmt,##__VA_ARGS__)
#define ________DBG_20210806_move_with_keypress(fmt, ...)     // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)
#define ________DBG_20210810_keyboard_fnctrl_change(fmt, ...) // tMKB_DEBUG_LOG(fmt, ##__VA_ARGS__)
#define ________DBG_20210810_keyboard_modkey_debug(fmt, ...)  // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)

#define ________DBG_20210819_timer_func_test(fmt, ...)                 // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)
#define ________DBG_20210830_key_event_dump_phenomenon_debug(fmt, ...) // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)
#define ________DBG_20210820_balloc_remains(fmt, ...)                  // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)

#define ________DBG_20210903_event_process_test(fmt, ...) // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)

#define ________DBG_20210907_sgs_battery(fmt, ...) // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)

#define ________DBG_20210915_led_callstack(fmt, ...)   // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)
#define ________DBG_20210915_led_callstack_2(fmt, ...) // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)
#define ________DBG_20210930_fnbtn(fmt, ...)           // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)
#define ________DBG_20210930_ledconn_test(fmt, ...)    // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)

#define ________DBG_20210930_ble_event(fmt, ...)     // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)
#define ________DBG_20210930_ble_adv_event(fmt, ...) // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)
#define ________DBG_20210930_usb_event(fmt, ...)     // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)

#define ________DBG_20211014_led_event(fmt, ...) // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)

#define ________DBG_20211014_channel_select(fmt, ...) // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)

#define ________DBG_20211014_channel_job(fmt, ...) // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)

#define ________DBG_20211014_channel_job2(fmt, ...) // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)

#define ________DBG_20211014_channel_os(fmt, ...) // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)

#define ________DBG_20211014_channel_os(fmt, ...) // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)

#define ________DBG_20211014_led_battery(fmt, ...)                 // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)
#define ________DBG_20211229_cammic_hotkey(fmt, ...)               // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)
#define ________DBG_20211229_string_hotkey(fmt, ...)               // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)
#define ________DBG_20211229_delay_time_teste(fmt, ...)            // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)
#define ________DBG_20211229_capture_hotkey_test(fmt, ...)         // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)
#define ________DBG_20220318_func_ralt(fmt, ...)                   // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)
#define ________DBG_20220318_touchlock_with_pixart_reset(fmt, ...) // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)

/*______  _______           _______  _______
(  ____ )(  ___  )|\     /|(  ____ \(  ____ )
| (    )|| (   ) || )   ( || (    \/| (    )|
| (____)|| |   | || | _ | || (__    | (____)|
|  _____)| |   | || |( )| ||  __)   |     __)
| (      | |   | || || || || (      | (\ (
| )      | (___) || () () || (____/\| ) \ \__
|/       (_______)(_______)(_______/|/   \_*/

/*______  _        _______  _______
(  ____ \( \      (  ___  )(  ____ \|\     /|
| (    \/| (      | (   ) || (    \/| )   ( |
| (__    | |      | (___) || (_____ | (___) |
|  __)   | |      |  ___  |(_____  )|  ___  |
| (      | |      | (   ) |      ) || (   ) |
| )      | (____/\| )   ( |/\____) || )   ( |
|/       (_______/|/     \|\_______)|/     */
#define ________DBG_20211229_system_state_set(fmt, ...) MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)
#define ________DBG_20211229_fds_test(fmt, ...) // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)

/*        _______  ______
( \      (  ____ \(  __  \
| (      | (    \/| (  \  )
| |      | (__    | |   ) |
| |      |  __)   | |   | |
| |      | (      | |   ) |
| (____/\| (____/\| (__/  )
(_______/(_______/(______*/
#define ________DBG_20211025_led_fastblink(fmt, ...) // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)
#define ________DBG_20211025_led_ble_event(fmt, ...) // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)

/*_____   _______ __________________ _______  _______
(  ___ \ (  ___  )\__   __/\__   __/(  ____ \(  ____ )|\     /|
| (   ) )| (   ) |   ) (      ) (   | (    \/| (    )|( \   / )
| (__/ / | (___) |   | |      | |   | (__    | (____)| \ (_) /
|  __ (  |  ___  |   | |      | |   |  __)   |     __)  \   /
| (  \ \ | (   ) |   | |      | |   | (      | (\ (      ) (
| )___) )| )   ( |   | |      | |   | (____/\| ) \ \__   | |
|/ \___/ |/     \|   )_(      )_(   (_______/|/   \__/   \*/
#define ________DBG_20211205_battery_level(fmt, ...) // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)
#define ________DBG_20211205_battery_test(fmt, ...)  // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)

/* _______           _______  _       _________
(  ____ \|\     /|(  ____ \( (    /|\__   __/
| (    \/| )   ( || (    \/|  \  ( |   ) (
| (__    | |   | || (__    |   \ | |   | |
|  __)   ( (   ) )|  __)   | (\ \) |   | |
| (       \ \_/ / | (      | | \   |   | |
| (____/\  \   /  | (____/\| )  \  |   | |
(_______/   \_/   (_______/|/    )_)   )_(
                                             */
#define ________DBG_20211205_delayd_event_simple(fmt, ...)      // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)
#define ________DBG_20211205_delayd_event(fmt, ...)             // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)
#define ________DBG_20211205_delayd_event_que_sprintf(fmt, ...) // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)

/* _______  _______  _______  _______
(  ____ \(  ___  )(       )(  ____ \
| (    \/| (   ) || () () || (    \/
| |      | |   | || || || || (_____
| |      | |   | || |(_)| |(_____  )
| |      | |   | || |   | |      ) |
| (____/\| (___) || )   ( |/\____) |
(_______/(_______)|/     \|\_______)
                                    */
#define ________DBG_20210831_mkb_coms_aamode(fmt, ...) // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)

/* _______           _______ _________ _______  _______
(  ____ \|\     /|(  ____ \\__   __/(  ____ \(       )
| (    \/( \   / )| (    \/   ) (   | (    \/| () () |
| (_____  \ (_) / | (_____    | |   | (__    | || || |
(_____  )  \   /  (_____  )   | |   |  __)   | |(_)| |
      ) |   ) (         ) |   | |   | (      | |   | |
/\____) |   | |   /\____) |   | |   | (____/\| )   ( |
\_______)   \_/   \_______)   )_(   (_______/|/     \|
                                                      */
#define ________DBG_20211025_db_system_state(fmt, ...) // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)

#define ________DBG_20210831_mkb_cli_dbg(fmt, ...) MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)
#define ________DBG_20210603_module_obj(fmt, ...)       // MKB_DEBUG_LOG(fmt,##__VA_ARGS__)
#define ________DBG_20210810_all_timers_check(fmt, ...) // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)
#define ________DBG_20210811_print_prj_ver(fmt, ...) MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)

/*release log*/
#define ________LOG_20211205_mkb_event_qc(fmt, ...) // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)

/* ______   _                 _______ _________ _______  _______ _________
(  ___ \ ( \      |\     /|(  ____ \\__   __/(  ___  )(  ___  )\__   __/|\     /|
| (   ) )| (      | )   ( || (    \/   ) (   | (   ) || (   ) |   ) (   | )   ( |
| (__/ / | |      | |   | || (__       | |   | |   | || |   | |   | |   | (___) |
|  __ (  | |      | |   | ||  __)      | |   | |   | || |   | |   | |   |  ___  |
| (  \ \ | |      | |   | || (         | |   | |   | || |   | |   | |   | (   ) |
| )___) )| (____/\| (___) || (____/\   | |   | (___) || (___) |   | |   | )   ( |
|/ \___/ (_______/(_______)(_______/   )_(   (_______)(_______)   )_(   |/     \|
                                                                                 */
#define ________LOG_20211205_mkb_ble_conn_state(fmt, ...) // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)
#define ________DBG_20210831_mkb_ble_send_error(fmt, ...) // MKB_DEBUG_LOG(fmt, ##__VA_ARGS__)