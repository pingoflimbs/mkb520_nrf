/*
* ===============================================================================
* Copyright (C) 2017-2019
* INNOPRESSO INC. All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
* ===============================================================================
*/

#include <stdbool.h>
#include <stdint.h>
#include <stdarg.h>

#include "sdk_common.h"

#include "ble.h"
#include "ble_err.h"
#include "ble_conn_params.h"
#include "ble_bas.h"

#include "nrf_assert.h"
#include "nrf_error.h"
#include "nrf_delay.h"
#include "nrf_cli.h"
#include "nrf_balloc.h"
#include "nrf_pwr_mgmt.h"

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

#include "bsp.h"

#if defined(BSP_BTN_BLE_ENABLED) && BSP_BTN_BLE_ENABLED
#include "bsp_btn_ble.h"
#endif // defined(BSP_BTN_BLE_ENABLED) && BSP_BTN_BLE_ENABLED

#include "app_error.h"
#include "app_timer.h"
#include "app_scheduler.h"

#define MKB_LOG_LEVEL MKB_BUTTON_LOG_LEVEL
#include "mkb_common.h"

#if (defined(MKB_EVENT_ENABLED) && MKB_EVENT_ENABLED)
#include "mkb_event.h"
#else
#error "You have to enable event at mkb_config.h!"
#endif // (defined(MKB_EVENT_ENABLED) && MKB_EVENT_ENABLED)

#if (defined(MKB_STM8_ENABLED) && MKB_STM8_ENABLED)
#include "drv_stm8.h"
#endif

#if (defined(MKB_BUTTON_ENABLED) && MKB_BUTTON_ENABLED)
#include "mkb_button.h"

/**@brief Function for putting the chip into sleep mode.
 *
 * @note This function will not return.
 */
static void mkb_button_sleep_mode_enter(void)
{
    ret_code_t err_code;

    MKB_LOG_INFO("Go to Sleep");

    mkb_event_send(EVT_SYSTEM_SLEEP, 0);
}

/**@brief Function for handling events from the BSP module.
 *
 * @param[in]   event   Event generated by button press.
 */
static void bsp_event_handler(bsp_event_t event)
{
    ret_code_t err_code;

    switch (event)
    {
#if !defined(BOARD_MKB10040)
#if defined(BOARD_PCA10056)
    case BSP_EVENT_SLEEP:
        MKB_LOG_INFO("BSP_EVENT_SLEEP");
        mkb_button_sleep_mode_enter();
        break;
#endif                    // !defined(BOARD_PCA10056)
    case BSP_EVENT_KEY_0: // Button Touch Left Push
        MKB_LOG_INFO("BSP_EVENT_KEY_0");
        mkb_event_send(EVT_BUTTON_TOUCH_LEFT, true);
        break;

    case BSP_EVENT_KEY_1: // Btuuon Touch Right Push
        MKB_LOG_INFO("BSP_EVENT_KEY_1");
        mkb_event_send(EVT_BUTTON_TOUCH_RIGHT, true);
        break;
    case BSP_EVENT_KEY_2: // Button Click Left Push
        MKB_LOG_INFO("BSP_EVENT_KEY_2");
        mkb_event_send(EVT_BUTTON_CLICK_LEFT, true);
        break;

    case BSP_EVENT_KEY_3: // Button Click Right Push
        MKB_LOG_INFO("BSP_EVENT_KEY_3");
        mkb_event_send(EVT_BUTTON_CLICK_RIGHT, true);
        break;

    case BSP_EVENT_KEY_4: // Button Touch Left Release
        MKB_LOG_INFO("BSP_EVENT_KEY_4");
        mkb_event_send(EVT_BUTTON_TOUCH_LEFT, false);
        break;

    case BSP_EVENT_KEY_5: // Button Touch Right Release
        MKB_LOG_INFO("BSP_EVENT_KEY_5");
        mkb_event_send(EVT_BUTTON_TOUCH_RIGHT, false);
        break;
    case BSP_EVENT_KEY_6: // Button Click Left Release
        MKB_LOG_INFO("BSP_EVENT_KEY_6");
        mkb_event_send(EVT_BUTTON_CLICK_LEFT, false);
        break;

    case BSP_EVENT_KEY_7: // Button Click Right Release
        MKB_LOG_INFO("BSP_EVENT_KEY_7");
        mkb_event_send(EVT_BUTTON_CLICK_RIGHT, false);
        break;

#endif // !defined(BOARD_MKB10040)
    default:
        break;
    }
}

#if (defined(MKB_STM8_ENABLED) && MKB_STM8_ENABLED)
STM8_STATE_t m_stm8_button;
void stm8_read_handler(ret_code_t status, void *p_samples)
{
    STM8_STATE_t *p_state;

    if (status == NRF_SUCCESS)
    {
        p_state = (STM8_STATE_t *)p_samples;

        if (m_stm8_button.bits.touch_left)
        {
            if (!p_state->bits.touch_left)
            {
                mkb_event_send(EVT_BUTTON_TOUCH_LEFT, false);

                MKB_LOG_INFO("MOKIBO ClickBar touch left:%d,right:%d", p_state->bits.touch_left, p_state->bits.touch_right);
            }
        }
        else
        {
            if (p_state->bits.touch_left)
            {
                mkb_event_send(EVT_BUTTON_TOUCH_LEFT, true);

                MKB_LOG_INFO("MOKIBO ClickBar touch left:%d,right:%d", p_state->bits.touch_left, p_state->bits.touch_right);
            }
        }

        if (m_stm8_button.bits.touch_right)
        {
            if (!p_state->bits.touch_right)
            {
                mkb_event_send(EVT_BUTTON_TOUCH_RIGHT, false);

                MKB_LOG_INFO("MOKIBO ClickBar touch left:%d,right:%d", p_state->bits.touch_left, p_state->bits.touch_right);
            }
        }
        else
        {
            if (p_state->bits.touch_right)
            {
                mkb_event_send(EVT_BUTTON_TOUCH_RIGHT, true);

                MKB_LOG_INFO("MOKIBO ClickBar touch left:%d,right:%d", p_state->bits.touch_left, p_state->bits.touch_right);
            }
        }

        if (m_stm8_button.bits.button_left)
        {
            if (!p_state->bits.button_left)
            {
                mkb_event_send(EVT_BUTTON_CLICK_LEFT, false);

                MKB_LOG_INFO("MOKIBO ClickBar button left:%d,right:%d", p_state->bits.button_left, p_state->bits.button_right);
            }
        }
        else
        {
            if (p_state->bits.button_left)
            {
                mkb_event_send(EVT_BUTTON_CLICK_LEFT, true);

                MKB_LOG_INFO("MOKIBO ClickBar button left:%d,right:%d", p_state->bits.button_left, p_state->bits.button_right);
            }
        }

        if (m_stm8_button.bits.button_right)
        {
            if (!p_state->bits.button_right)
            {
                mkb_event_send(EVT_BUTTON_CLICK_RIGHT, false);

                MKB_LOG_INFO("MOKIBO ClickBar button left:%d,right:%d", p_state->bits.button_left, p_state->bits.button_right);
            }
        }
        else
        {
            if (p_state->bits.button_right)
            {
                mkb_event_send(EVT_BUTTON_CLICK_RIGHT, true);

                MKB_LOG_INFO("MOKIBO ClickBar button left:%d,right:%d", p_state->bits.button_left, p_state->bits.button_right);
            }
        }

        m_stm8_button = *p_state;
    }
}
#endif

/**@brief mokibo button initialize */
void mkb_button_init(void)
{
    ret_code_t err_code;
    bsp_event_t startup_event;
    bool erase_bonds;

#if defined(BOARD_MKB10040) || defined(BOARD_MKB10056)
    err_code = bsp_init(BSP_INIT_BUTTONS, bsp_event_handler);
    MKB_ERROR_CHECK(err_code);
#else
    err_code = bsp_init(BSP_INIT_LEDS | BSP_INIT_BUTTONS, bsp_event_handler);
    MKB_ERROR_CHECK(err_code);
#endif

#if defined(BOARD_MKB10056)
    err_code = bsp_event_to_button_action_assign(BUTTON_ID_TOUCH_LEFT,
                                                 BSP_BUTTON_ACTION_PUSH,
                                                 BSP_EVENT_KEY_0);
    MKB_ERROR_CHECK(err_code);
    err_code = bsp_event_to_button_action_assign(BUTTON_ID_TOUCH_LEFT,
                                                 BSP_BUTTON_ACTION_RELEASE,
                                                 BSP_EVENT_KEY_4);
    MKB_ERROR_CHECK(err_code);
    err_code = bsp_event_to_button_action_assign(BUTTON_ID_CLICK_LEFT,
                                                 BSP_BUTTON_ACTION_PUSH,
                                                 BSP_EVENT_KEY_2);
    MKB_ERROR_CHECK(err_code);
    err_code = bsp_event_to_button_action_assign(BUTTON_ID_CLICK_LEFT,
                                                 BSP_BUTTON_ACTION_RELEASE,
                                                 BSP_EVENT_KEY_6);
    MKB_ERROR_CHECK(err_code);
#elif defined(BOARD_PCA10056)
    err_code = bsp_event_to_button_action_assign(BUTTON_ID_TOUCH_LEFT,
                                                 BSP_BUTTON_ACTION_PUSH,
                                                 BSP_EVENT_SLEEP);
    MKB_ERROR_CHECK(err_code);
    //err_code = bsp_event_to_button_action_assign(BUTTON_ID_TOUCH_LEFT,
    //                                             BSP_BUTTON_ACTION_LONG_PUSH,
    //                                             BSP_EVENT_SLEEP);
    //MKB_ERROR_CHECK(err_code);

    //err_code = bsp_event_to_button_action_assign(BUTTON_ID_TOUCH_LEFT,
    //                                             BSP_BUTTON_ACTION_PUSH,
    //                                             BSP_EVENT_DEFAULT);
    //MKB_ERROR_CHECK(err_code);
    //err_code = bsp_event_to_button_action_assign(BUTTON_ID_TOUCH_LEFT,
    //                                             BSP_BUTTON_ACTION_RELEASE,
    //                                             BSP_EVENT_KEY_4);
    //MKB_ERROR_CHECK(err_code);
    err_code = bsp_event_to_button_action_assign(BUTTON_ID_TOUCH_RIGHT,
                                                 BSP_BUTTON_ACTION_PUSH,
                                                 BSP_EVENT_DEFAULT);
    MKB_ERROR_CHECK(err_code);
    err_code = bsp_event_to_button_action_assign(BUTTON_ID_TOUCH_RIGHT,
                                                 BSP_BUTTON_ACTION_RELEASE,
                                                 BSP_EVENT_KEY_5);
    MKB_ERROR_CHECK(err_code);
    err_code = bsp_event_to_button_action_assign(BUTTON_ID_CLICK_LEFT,
                                                 BSP_BUTTON_ACTION_PUSH,
                                                 BSP_EVENT_DEFAULT);
    MKB_ERROR_CHECK(err_code);
    err_code = bsp_event_to_button_action_assign(BUTTON_ID_CLICK_LEFT,
                                                 BSP_BUTTON_ACTION_RELEASE,
                                                 BSP_EVENT_KEY_6);
    MKB_ERROR_CHECK(err_code);
    err_code = bsp_event_to_button_action_assign(BUTTON_ID_CLICK_RIGHT,
                                                 BSP_BUTTON_ACTION_PUSH,
                                                 BSP_EVENT_DEFAULT);
    MKB_ERROR_CHECK(err_code);
    err_code = bsp_event_to_button_action_assign(BUTTON_ID_CLICK_RIGHT,
                                                 BSP_BUTTON_ACTION_RELEASE,
                                                 BSP_EVENT_KEY_7);
    MKB_ERROR_CHECK(err_code);
#elif defined(BOARD_MKB10040)
#if (defined(MKB_STM8_ENABLED) && MKB_STM8_ENABLED)
    drv_stm8_init(stm8_read_handler);
    mkb_db_stm8_version_set(drv_stm8_get_version());
#endif
#else
#endif
}

/**@brief mokibo button initialize */
void mkb_button_enable(void)
{
#if defined(BOARD_MKB10040)
#if (defined(MKB_STM8_ENABLED) && MKB_STM8_ENABLED)
    drv_stm8_enable();
#endif
#elif defined(BOARD_PCA10056) || defined(BOARD_MKB10056)
#else
#endif // defined(BOARD_MKB10040)
}

#if (defined(NRF_PWR_MGMT_ENABLED) && NRF_PWR_MGMT_ENABLED)
static bool mkb_button_shutdown(nrf_pwr_mgmt_evt_t event)
{
    ret_code_t err_code;

    switch (event)
    {
    case NRF_PWR_MGMT_EVT_PREPARE_WAKEUP:
        MKB_LOG_INFO("NRF_PWR_MGMT_EVT_PREPARE_WAKEUP: Power management wants to prepare wakeup.\n");

        MKB_LOG_INFO("Prepare wakeup buttons");

#if defined(BOARD_MKB10056)
        bsp_wakeup_button_enable(BUTTON_ID_CLICK_LEFT);
        bsp_wakeup_button_disable(BUTTON_ID_TOUCH_LEFT);
#elif defined(BOARD_PCA10056)
        bsp_wakeup_button_enable(BUTTON_ID_CLICK_LEFT);
        bsp_wakeup_button_enable(BUTTON_ID_CLICK_RIGHT);
        bsp_wakeup_button_disable(BUTTON_ID_TOUCH_LEFT);
        //bsp_wakeup_button_enable(BUTTON_ID_TOUCH_LEFT);
        bsp_wakeup_button_enable(BUTTON_ID_TOUCH_RIGHT);
#endif // !defined(BOARD_MKB10040)

#if defined(BSP_BTN_BLE_ENABLED) && BSP_BTN_BLE_ENABLED
// Prepare wakeup buttons.
//err_code = bsp_btn_ble_sleep_mode_prepare();
//APP_ERROR_CHECK(err_code);
#endif // defined(BSP_BTN_BLE_ENABLED) && BSP_BTN_BLE_ENABLED
        break;

    case NRF_PWR_MGMT_EVT_PREPARE_SYSOFF:
        MKB_LOG_INFO("NRF_PWR_MGMT_EVT_PREPARE_SYSOFF: Power management wants to poweroff.\n");
        //SaveInfoToFlash(m_config.config.selected_ch);
        //configure the pin like this to wake up from sleep:
        //nrf_gpio_cfg_sense_input(PIN NUMBER, NRF_GPIO_PIN_PULLUP, NRF_GPIO_PIN_SENSE_LOW);
        break;

    case NRF_PWR_MGMT_EVT_PREPARE_DFU:
        MKB_LOG_INFO("NRF_PWR_MGMT_EVT_PREPARE_DFU: Power management wants to reset to DFU mode.\n");
        // YOUR_JOB: Get ready to reset into DFU mode
        //
        // If you aren't finished with any ongoing tasks, return "false" to
        // signal to the system that reset is impossible at this stage.
        //
        // Here is an example using a variable to delay resetting the device.
        //
        // if (!m_ready_for_reset)
        // {
        //      return false;
        // }
        // else
        //{
        //
        //    // Device ready to enter
        //    uint32_t err_code;
        //    err_code = sd_softdevice_disable();
        //    APP_ERROR_CHECK(err_code);
        //    err_code = app_timer_stop_all();
        //    APP_ERROR_CHECK(err_code);
        //}
        break;

    default:
        MKB_LOG_INFO("default: %d", event);
        // YOUR_JOB: Implement any of the other events available from the power management module:
        //      -NRF_PWR_MGMT_EVT_PREPARE_SYSOFF
        //      -NRF_PWR_MGMT_EVT_PREPARE_WAKEUP
        //      -NRF_PWR_MGMT_EVT_PREPARE_RESET
        return true;
    }

    return true;
}

NRF_PWR_MGMT_HANDLER_REGISTER(mkb_button_shutdown, SHUTDOWN_PRIORITY_DEFAULT);
#endif /* NRF_PWR_MGMT_ENABLED */

#endif // (defined(MKB_BUTTON_ENABLED) && MKB_BUTTON_ENABLED)
