/*
* ===============================================================================
* Copyright (C) 2017-2019
* INNOPRESSO INC. All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
* ===============================================================================
*/

#include "mkb_timer.h"
#include "app_timer.h"

APP_TIMER_DEF(m_timer_system_timer);

uint32_t m_time = 0;
uint32_t old_time = 0;

void mkb_timer_system_time_handler()
{
    m_time++;
}

void mkb_timer_system_time_init(void)
{
    ret_code_t err_code;
    //err_code = app_timer_create(&m_timer_system_timer, APP_TIMER_MODE_REPEATED, mkb_timer_system_time_handler);
    //app_timer_start(m_timer_system_timer, APP_TIMER_TICKS(10), NULL);
}

uint32_t m_timer_system_time_get()
{
    return app_timer_cnt_get();
    ;
}

uint32_t m_timer_system_time_delta_get()
{
    m_time = app_timer_cnt_get();
    uint32_t delta_time = m_time - old_time;
    old_time = m_time;
    return delta_time;
}