/*
 * ===============================================================================
 * Copyright (C) 2017-2019
 * INNOPRESSO INC. All Rights Reserved
 *
 * The content of this file is copyrighted by INNOPRESSO or its
 * affiliated, associated or related companies. All copyrights and other
 * associated intellectual property rights related thereto are expressly
 * reserved by INNOPRESSO INC.
 * ===============================================================================
 */

#include <stdbool.h>
#include <stdint.h>
#include <stdarg.h>

#include "sdk_common.h"

#include "ble.h"
#include "ble_err.h"
#include "ble_conn_params.h"
#include "ble_conn_state.h"
#include "ble_bas.h"

#include "nrf_assert.h"
#include "nrf_error.h"
#include "nrf_delay.h"
#include "nrf_cli.h"
#include "nrf_balloc.h"
#include "nrf_pwr_mgmt.h"

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

#include "bsp.h"

#include "app_error.h"
#include "app_timer.h"
#include "app_scheduler.h"

#define MKB_LOG_LEVEL MKB_COMS_LOG_LEVEL
#include "mkb_common.h"

#if (defined(MKB_COMS_ENABLED) && MKB_COMS_ENABLED)
#include "mkb_coms.h"

#if (defined(MKB_EVENT_ENABLED) && MKB_EVENT_ENABLED)
#include "mkb_event.h"
#endif // (defined(MKB_EVENT_ENABLED) && MKB_EVENT_ENABLED)

#if (defined(MKB_KEYBOARD_ENABLED) && MKB_KEYBOARD_ENABLED)
#include "mkb_keyboard.h"
#endif // (defined(MKB_KEYBOARD_ENABLED) && MKB_KEYBOARD_ENABLED)

#if (defined(MKB_TOUCHPAD_ENABLED) && MKB_TOUCHPAD_ENABLED)
#include "mkb_touchpad.h"
#endif // (defined(MKB_TOUCHPAD_ENABLED) && MKB_TOUCHPAD_ENABLED)

#if (defined(MKB_SM_ENABLED) && MKB_SM_ENABLED)
#include "mkb_state_manager.h"
#endif // (defined(MKB_SM_ENABLED) && MKB_SM_ENABLED)

#if (defined(MKB_BLE_ENABLED) && MKB_BLE_ENABLED)
#include "mkb_ble.h"
#include "mkb_ble_adv.h"
#include "mkb_ble_pm.h"
#endif // (defined(MKB_BLE_ENABLED) && MKB_BLE_ENABLED)

#if (defined(MKB_USB_ENABLED) && MKB_USB_ENABLED)
#include "mkb_usb.h"
#endif // (defined(MKB_USB_ENABLED) && MKB_USB_ENABLED)

#if (defined(MKB_WDT_ENABLED) && MKB_WDT_ENABLED)
#include "mkb_wdt.h"
#endif

#include "drv_pixart.h"

#define COMS_PACKET_MAX 8
uint8_t m_coms_ble_packet_key[COMS_PACKET_MAX];
uint8_t m_coms_ble_key_idx = 0;
uint8_t m_coms_ble_packet_button;
uint8_t m_coms_ble_packet_consumer[3];

#define M_COMM_CONSUMER_BUF_LEN 3
static const uint8_t m_vol_mute_cmd[M_COMM_CONSUMER_BUF_LEN] = {0xE2, 0x00, 0x00};
static const uint8_t m_vol_down_cmd[M_COMM_CONSUMER_BUF_LEN] = {0xEA, 0x00, 0x00};
static const uint8_t m_vol_up_cmd[M_COMM_CONSUMER_BUF_LEN] = {0xE9, 0x00, 0x00};

static const uint8_t m_android_home[M_COMM_CONSUMER_BUF_LEN] = {35, 2, 0};
static const uint8_t m_android_srch[M_COMM_CONSUMER_BUF_LEN] = {207, 0, 0};
static const uint8_t m_android_menu[M_COMM_CONSUMER_BUF_LEN] = {162, 1, 0};
static const uint8_t m_android_lock[M_COMM_CONSUMER_BUF_LEN] = {48, 0, 0};
static const uint8_t m_android_back[M_COMM_CONSUMER_BUF_LEN] = {70, 0, 0};

static const uint8_t m_iphone_home[M_COMM_CONSUMER_BUF_LEN] = {64, 0x00, 0x00};
static const uint8_t m_iphone_lock[M_COMM_CONSUMER_BUF_LEN] = {48, 0x00, 0x00};
static const uint8_t m_iphone_vkbd[M_COMM_CONSUMER_BUF_LEN] = {184, 0x00, 0x00};

static const uint8_t m_consumer_no_cmd[M_COMM_CONSUMER_BUF_LEN] = {0x00, 0x00, 0x00};
static const uint8_t m_virtual_keyboard_cmd[M_COMM_CONSUMER_BUF_LEN] = {0xAE, 0x01, 0x00};

#if (defined(MKB_PM_BOND_TYPE) && (MKB_PM_BOND_TYPE == PM_BOND_PASSKEY))
passkey_t m_coms_passkey;
static const uint8_t m_coms_passkey_table[] =
    {
        0x31, // HID_KEY_1
        0x32, // HID_KEY_2
        0x33, // HID_KEY_3
        0x34, // HID_KEY_4
        0x35, // HID_KEY_5
        0x36, // HID_KEY_6
        0x37, // HID_KEY_7
        0x38, // HID_KEY_8
        0x39, // HID_KEY_9
        0x30, // HID_KEY_0
};
#endif

/**@brief mokibo coms initialize */
void mkb_coms_init(void)
{
#if (defined(MKB_PM_BOND_TYPE) && (MKB_PM_BOND_TYPE == PM_BOND_PASSKEY))
    mkb_coms_passkey_init();
#endif

#if (defined(MKB_USB_ENABLED) && MKB_USB_ENABLED)
    mkb_usb_init();
#endif // (defined(MKB_USB_ENABLED) && MKB_USB_ENABLED)

#if (defined(MKB_BLE_ENABLED) && MKB_BLE_ENABLED)
    mkb_ble_init();
#endif // (defined(MKB_BLE_ENABLED) && MKB_BLE_ENABLED)

    MKB_LOG_INFO("Initialization Success!");
}

#if (defined(MKB_PM_BOND_TYPE) && (MKB_PM_BOND_TYPE == PM_BOND_PASSKEY))
void mkb_coms_passkey_init(void)
{
    memset(&m_coms_passkey, 0, sizeof(passkey_t));
}

void mkb_coms_passkey_enable(void)
{
    memset(&m_coms_passkey, 0, sizeof(passkey_t));

    m_coms_passkey.is_ready = true;
}

void mkb_coms_passkey_disable(void)
{
    m_coms_passkey.is_ready = false;
}

void mkb_coms_passkey_handle(uint8_t key)
{
    if (m_coms_passkey.index < MKB_COMS_PASSKEY_LEN)
    {
        if (key >= HID_KEY_1 && key <= HID_KEY_0)
        {
            m_coms_passkey.keys[m_coms_passkey.index] = m_coms_passkey_table[key - HID_KEY_1];
            m_coms_passkey.index++;
        }
    }

    if (key == HID_KEY_ENTER)
    {
        mkb_ble_auth_key_response(m_coms_passkey.keys);
        m_coms_passkey.is_ready = false;
    }
}
#endif

/**@brief mokibo coms start */
void mkb_coms_start(void)
{
    MKB_DB_CH_t *p_ch = mkb_db_ch_get();
    CHANNEL_ID_t ch_id = mkb_db_ch_id_get();

#if (defined(MKB_BLE_ENABLED) && MKB_BLE_ENABLED)
    if (p_ch->config.info[ch_id].type == CHANNEL_TYPE_BT)
    {
        mkb_ble_start();
    }
#endif // (defined(MKB_BLE_ENABLED) && MKB_BLE_ENABLED)
}

#if (defined(MKB_KEYBOARD_ENABLED) && MKB_KEYBOARD_ENABLED)
uint8_t mkb_coms_key_convert_by_central(CENTRAL_TYPE_t central, uint8_t key)
{
    if (central == CENTRAL_MAC || central == CENTRAL_IOS)
    {
        switch (key)
        {
        case S2L(L_GUI):
            return S2L(L_ALT);
        case S2L(L_ALT):
            return S2L(L_GUI);
        case S2L(R_GUI):
            return S2L(R_ALT);
        case S2L(R_ALT):
            return S2L(R_GUI);
        }
    }
    return key;
}

void mkb_coms_key_process(mkb_event_type_t event, uint8_t key)
{
    MKB_DB_CH_t *p_ch = mkb_db_ch_get();
    CHANNEL_ID_t ch_id = mkb_db_ch_id_get();
    CHANNEL_INFO_t *p_ch_info = mkb_db_ch_info_get(mkb_db_ch_id_get());

#if (defined(MKB_PM_BOND_TYPE) && (MKB_PM_BOND_TYPE == PM_BOND_PASSKEY))
    if (m_coms_passkey.is_ready && p_ch_info->type == CHANNEL_TYPE_BT)
    {
        if (event == EVT_KEY_UP)
            mkb_coms_passkey_handle(key);
        return;
    }
#endif

    ________DBG_20210915_led_callstack("LED_STACK");

    ________LOG_20211205_mkb_ble_conn_state("con_state:%d, adv_running:%d, ble_pm_state:%d ", mkb_ble_conn_state(ch_id),
                                            mkb_ble_adv_running(),
                                            mkb_ble_pm_state(ch_id));
#if 1 //키입력시
    if (!mkb_ble_conn_state(ch_id) &&
        !mkb_ble_adv_running() &&
        mkb_ble_pm_state(ch_id))
    {
        mkb_led_ind_set(p_ch->led[ch_id].adv_direct);
        mkb_event_send(EVT_LED_CH_ADV_FAST);
        MKB_LOG_INFO("CH(%d) peer_id(%d) valid, start advertize with directed mode", ch_id, p_ch->config.info[ch_id].peer_id);

        mkb_ble_adv_start(ch_id, ADV_MODE_DIRECTED);
    }

#endif
    ________DBG_20210930_fnbtn("call");

    if (key >= HID_MODIFIER_START && key <= HID_MODIFIER_END)
    {
        if (p_ch_info->central == CENTRAL_MAC || p_ch_info->central == CENTRAL_IOS)
        {
            key = mkb_coms_key_convert_by_central(CENTRAL_MAC, key);
        }

        if (p_ch_info->type == CHANNEL_TYPE_BT)
        {
            if (event == EVT_KEY_DOWN)
            {
                m_coms_ble_packet_key[0] |= 0x01 << (key - HID_MODIFIER_START);
            }
            else if (event == EVT_KEY_UP)
            {
                m_coms_ble_packet_key[0] &= ~(0x01 << (key - HID_MODIFIER_START));
            }

            // mkb_event_send(EVT_HID_INPUT_KEYBOARD, m_coms_ble_packet_key, COMS_PACKET_MAX);
            mkb_ble_keys_send(m_coms_ble_packet_key, 8);

            // driver event
            //
            // input //key, power, battery, bleinput
            // database
            // eventhandler#
            // process
            // output // led, ble,
        }
#if (defined(MKB_USB_ENABLED) && MKB_USB_ENABLED)
        else
        {
            mkb_usb_kbd_modifier(0x01 << (key - HID_MODIFIER_START), (event == EVT_KEY_DOWN) ? true : false);
        }
#endif

        MKB_LOG_INFO("MOD KEY: 0x%02x", m_coms_ble_packet_key[0]);
    }
    else
    {
        if (p_ch_info->type == CHANNEL_TYPE_BT)
        {
            if (event == EVT_KEY_DOWN)
            {
                for (uint8_t i = 2; i < COMS_PACKET_MAX; i++)
                {
                    if (m_coms_ble_packet_key[i] == key)
                    {
                        break;
                    }

                    if (m_coms_ble_packet_key[i] == HID_KEY_NONE)
                    {
                        m_coms_ble_packet_key[i] = key;
                        m_coms_ble_key_idx++;
                        break;
                    }
                }

                // mkb_event_send(EVT_HID_INPUT_KEYBOARD, m_coms_ble_packet_key, COMS_PACKET_MAX);
                mkb_ble_keys_send(m_coms_ble_packet_key, 8);

                MKB_LOG_INFO("PACKET=> MOD: 0x%02x, KEY ADD:0x%x, IDX: %d, send size: %d",
                             m_coms_ble_packet_key[0], key, m_coms_ble_key_idx, sizeof(m_coms_ble_packet_key));
            }
            else if (event == EVT_KEY_UP)
            {
                for (uint8_t i = 2; i < COMS_PACKET_MAX; i++)
                {
                    if (m_coms_ble_packet_key[i] == key)
                    {
                        m_coms_ble_packet_key[i] = HID_KEY_NONE;
                        m_coms_ble_key_idx--;

                        for (uint8_t j = i; j < (COMS_PACKET_MAX - 1); j++)
                        {
                            if (m_coms_ble_packet_key[j + 1] != HID_KEY_NONE)
                            {
                                m_coms_ble_packet_key[j] = m_coms_ble_packet_key[j + 1];
                                m_coms_ble_packet_key[j + 1] = HID_KEY_NONE;
                            }
                        }
                    }
                }

                // mkb_event_send(EVT_HID_INPUT_KEYBOARD, m_coms_ble_packet_key, COMS_PACKET_MAX);
                mkb_ble_keys_send(m_coms_ble_packet_key, 8);
                MKB_LOG_INFO("PACKET=> MOD: 0x%02x, KEY DEL:0x%x, IDX: %d, send size: %d",
                             m_coms_ble_packet_key[0], key, m_coms_ble_key_idx, sizeof(m_coms_ble_packet_key));
            }
        }
#if (defined(MKB_USB_ENABLED) && MKB_USB_ENABLED)
        else
        {
            mkb_usb_kbd_control(key, (event == EVT_KEY_DOWN) ? true : false);
            MKB_LOG_INFO("KEY: 0x%02x", key);
        }
#endif
    }
}

void mkb_coms_channel_select(CHANNEL_ID_t ch)
{
#if (defined(MKB_BLE_ENABLED) && MKB_BLE_ENABLED)
    MKB_DB_CH_t *p_ch = mkb_db_ch_get();
    CHANNEL_ID_t ch_id = mkb_db_ch_id_get();

    ________DBG_20211014_channel_select("before: curr:%d ,parm:%d ", mkb_db_ch_id_get(), ch);

    //같은채널인지 * 이전이 USB인지 * 지금이USB인지 * PEER존재하는지 * CONN상태인지

    //이전 이후 USB 삭제
    //이전PEER 존재여부 * 이전CONN 존재여부
    //이후PEER 존재여부 * 이후CONN 존재여부

    // if(is_oldch_peer==true && is_newch_peer==true)

    ________DBG_20211014_channel_job("%d->%d PEER:%d CONN:%d", ch_id, ch, p_ch->config.info[ch].peer_id != PM_PEER_ID_INVALID, mkb_ble_conn_state(ch));
    ________DBG_20211014_channel_job2("%d->%d PEER:%d CONN:%d", ch_id, ch, p_ch->config.info[ch].peer_id != PM_PEER_ID_INVALID, mkb_ble_conn_state(ch));

    //같은채널에서
    if (ch_id == ch)
    {
        //전채널 & 새채널이 BT일때
        if (p_ch->config.info[ch].type == CHANNEL_TYPE_BT)
        {
            // PEER 가 있을때
            if (p_ch->config.info[ch].peer_id != PM_PEER_ID_INVALID)
            {
                if (mkb_ble_conn_state(ch))
                {
                    ////////////////////////////////////
                    // OLD==NEW, PEER:O, CONN:O
                    ////////////////////////////////////
                    ________DBG_20211014_channel_job2("//SELECT OLD==NEW, PEER:O, CONN:O");
                    ________DBG_20210915_led_callstack("LED_STACK");
                    mkb_led_ind_set(p_ch->led[ch].select);
                    mkb_event_send(EVT_LED_CH_CONNECT);
                    ________DBG_20211025_led_ble_event("// OLD==NEW, PEER:O, CONN:O ch:%d->%d", ch_id, ch);

                    ________DBG_20211014_channel_select("%d->%d PEER:O CONN:O", ch_id, ch);
                }
                else
                {
                    ////////////////////////////////////
                    // OLD==NEW, PEER:O, CONN:X
                    ////////////////////////////////////
                    ________DBG_20210915_led_callstack("LED_STACK");
                    ________DBG_20211014_channel_job2("//SELECT OLD==NEW, PEER:O, CONN:X");
                    mkb_led_ind_set(p_ch->led[ch].adv_direct);
                    mkb_ble_adv_start(ch, ADV_MODE_DIRECTED);
                    ________DBG_20211025_led_ble_event("// OLD==NEW, PEER:O, CONN:X ch:%d->%d", ch_id, ch);

                    MKB_LOG_INFO("CH(%d) peer_id(%d) valid, start advertize with directed mode", ch, p_ch->config.info[ch].peer_id);
                }
            }
            else
            {
                ////////////////////////////////////
                // OLD==NEW, PEER:X, CONN:-
                ////////////////////////////////////
                ________DBG_20211014_channel_job2("//SELECT OLD==NEW, PEER:X, CONN:-");
                ________DBG_20210915_led_callstack("LED_STACK");
                mkb_led_ind_set(LED_IND_IDLE);
                mkb_event_send(EVT_LED_CH_NO_ADDR);
                ________DBG_20211025_led_ble_event("// OLD==NEW, PEER:O, CONN:X ch:%d->%d", ch_id, ch);
                ________DBG_20211014_channel_select("SAME BTBT PEER:X CONN:-");
            }
        }
        //전채널 & 새채널이 USB일때
        else
        {
#if (defined(MKB_USB_ENABLED) && MKB_USB_ENABLED)
            if (mkb_usb_state())
            {
                if (mkb_usb_conn_state(ch))
                {
                    ________DBG_20210915_led_callstack("LED_STACK");
                    ________DBG_20211014_channel_select("SAME BTUS PEER:O CONN:O");
                    mkb_led_ind_set(p_ch->led[ch].select);
                    mkb_event_send(EVT_LED_CH_CONNECT);
                }
                else
                {
                    MKB_LOG_INFO("CH(%d), start request", ch);
                    ________DBG_20210915_led_callstack("LED_STACK");
                    ________DBG_20211014_channel_select("SAME BTUS PEER:O CONN:X");
                    mkb_led_ind_set(p_ch->led[ch].select);
                    mkb_event_send(EVT_LED_CH_CONNECT);
                    mkb_usb_start(ch);
                }
            }
            else
#endif
            {
                ________DBG_20211014_channel_select("SAME BTUS PEER:X CONN:-");
                ________DBG_20210915_led_callstack("LED_STACK");
                mkb_led_ind_set(LED_IND_IDLE);
            }
        }
    }
    else // old != new //다른채널
    {
        ________DBG_20210915_led_callstack("LED_STACK");
        mkb_led_ind_set(p_ch->led[ch].select);
        mkb_event_send(EVT_LED_CH_CONNECT);

        if (p_ch->config.info[ch].type == CHANNEL_TYPE_BT)
        {
#if (defined(MKB_USB_ENABLED) && MKB_USB_ENABLED)
            if (p_ch->config.info[ch_id].type == CHANNEL_TYPE_USB) //이전 USB채널 끊기동작
            {
                if (mkb_usb_state() && mkb_usb_conn_state(ch_id))
                {
                    MKB_LOG_INFO("CH(%d), stop request", ch_id);
                    mkb_usb_stop(ch_id);
                }
            }
#endif

            mkb_db_ch_id_prev_set(ch_id);
            ________DBG_20211229_fds_test("2");
            mkb_db_ch_id_set(ch);

            // new 가 PEER 가있을때
            if (p_ch->config.info[ch].peer_id != PM_PEER_ID_INVALID)
            {
                if (mkb_ble_conn_state(ch)) // OLD!=NEW, PEER:O, CONN:O
                {
                    ////////////////////////////////////
                    // OLD!=NEW, PEER:O, CONN:O
                    ////////////////////////////////////
                    ________DBG_20211014_channel_job2("//SELECT OLD!=NEW, PEER:O, CONN:O");
                    ________DBG_20211025_led_ble_event("// OLD==NEW, PEER:O, CONN:O ch:%d->%d", ch_id, ch);
                    ________DBG_20210915_led_callstack("LED_STACK");
                    ________DBG_20211014_channel_select("DIFR BTBT PEER:O CONN:O");
                    mkb_led_ind_set(p_ch->led[ch].select);
                    mkb_event_send(EVT_LED_CH_CONNECT);
                }
                else
                {
                    ////////////////////////////////////
                    // OLD!=NEW, PEER:O, CONN:X
                    ////////////////////////////////////
                    ________DBG_20211014_channel_job2("//SELECT OLD!=NEW, PEER:O, CONN:X");
                    ________DBG_20210915_led_callstack("LED_STACK");
                    ________DBG_20211014_channel_select("DIFR BTBT PEER:O CONN:X");
                    mkb_led_ind_set(p_ch->led[ch].adv_direct);
                    mkb_event_send(EVT_LED_CH_ADV_FAST);

                    //이전BT 연결되어있으면 끊기동작
                    if (mkb_ble_conn_state(ch_id) && (p_ch->config.info[ch_id].type == CHANNEL_TYPE_BT))
                    {
                        MKB_LOG_INFO("CH(%d), disconnect request", ch_id);
                        mkb_db_wait_for_disconnect_set(true);
                        mkb_ble_adv_wait_for_advertizing_set(ADV_MODE_DIRECTED);
                        mkb_ble_disconnect(ch_id);
                    }
                    else
                    {
                        //? 어드버타이징
                        MKB_LOG_INFO("CH(%d) peer_id(%d) valid, start advertize with directed mode", ch, p_ch->config.info[ch].peer_id);
                        mkb_ble_adv_start(ch, ADV_MODE_DIRECTED);
                    }
                }
            }
            else // new 가 PEER가 없을때
            {
                ////////////////////////////////////
                // OLD!=NEW, PEER:X, CONN:-
                ////////////////////////////////////
                ________DBG_20211014_channel_job2("//SELECT OLD!=NEW, PEER:X, CONN:-");
                ________DBG_20211014_channel_select("DIFR BTBT PEER:X CONN:-");
                mkb_event_send(EVT_LED_CH_NO_ADDR);
                if (mkb_ble_conn_state(ch_id)) //이전 채널이 연결되어있다면 끊기.
                {
                    MKB_LOG_INFO("CH(%d), disconnect request", ch_id);
                    mkb_db_wait_for_disconnect_set(false);
                    mkb_ble_adv_stop_request();
                    mkb_ble_disconnect(ch_id);
                    mkb_led_ind_set(p_ch->led[ch].select);
                }
                else
                {
                    ________DBG_20210915_led_callstack("LED_STACK");
                    mkb_led_ind_set(p_ch->led[ch].select);
                }
            }
        }
        else //새채널이 블루투스아니면
        {
            mkb_db_ch_id_prev_set(ch_id);
            ________DBG_20211229_fds_test("1");
            mkb_db_ch_id_set(ch);

            if (mkb_ble_conn_state(ch_id))
            {
                MKB_LOG_INFO("CH(%d), disconnect request", ch_id);
                mkb_db_wait_for_disconnect_set(false);
                mkb_ble_adv_stop_request();
                mkb_ble_disconnect(ch_id);
            }

#if (defined(MKB_USB_ENABLED) && MKB_USB_ENABLED)
            if (mkb_usb_state())
            {
                if (mkb_usb_conn_state(ch))
                {
                    ________DBG_20210915_led_callstack("LED_STACK");
                    ________DBG_20211014_channel_select("DIFR BTUS PEER:O CONN:O");
                    mkb_led_ind_set(p_ch->led[ch].select);
                    mkb_event_send(EVT_LED_CH_CONNECT);
                }
                else
                {
                    MKB_LOG_INFO("CH(%d), start request", ch);
                    ________DBG_20210915_led_callstack("LED_STACK");
                    ________DBG_20211014_channel_select("DIFR BTUS PEER:O CONN:X");
                    mkb_led_ind_set(p_ch->led[ch].select);
                    mkb_event_send(EVT_LED_CH_CONNECT);
                    mkb_usb_start(ch);
                }
            }
            else
#endif
            {
                ________DBG_20211014_channel_select("DIFR BTUS PEER:X CONN:-");
                ________DBG_20210915_led_callstack("LED_STACK");
                mkb_led_ind_set(LED_IND_IDLE);
            }
        }
    }
#endif // (defined(MKB_BLE_ENABLED) && MKB_BLE_ENABLED)

    ________DBG_20211014_channel_select("after: curr:%d ,parm:%d ", mkb_db_ch_id_get(), ch);
}

void mkb_coms_channel_new(CHANNEL_ID_t ch)
{
#if (defined(MKB_BLE_ENABLED) && MKB_BLE_ENABLED)
    MKB_DB_CH_t *p_ch = mkb_db_ch_get();
    CHANNEL_ID_t ch_id = mkb_db_ch_id_get();

    // prev 가 USB? BT? PEER? CONN?
    // ch가 USB? BT? PEER? CONN?
    ________DBG_20211014_channel_job("%d->%d PEER:%d CONN:%d", ch_id, ch, p_ch->config.info[ch].peer_id != PM_PEER_ID_INVALID, mkb_ble_conn_state(ch));
    ________DBG_20211014_channel_select("before: curr:%d ,parm:%d ", mkb_db_ch_id_get(), ch);
    ________DBG_20211014_channel_job2("%d->%d PEER:%d CONN:%d", ch_id, ch, p_ch->config.info[ch].peer_id != PM_PEER_ID_INVALID, mkb_ble_conn_state(ch));

#if (MKB_BLE_ADDRESS_TYPE == BLE_ADDRESS_TYPE_RANDOM)
    if (p_ch->config.info[ch].type == CHANNEL_TYPE_BT)
    {
        mkb_db_ch_address_set(ch);
    }
#endif

    if (ch_id != ch)
    {
        ________DBG_20210915_led_callstack("LED_STACK");
        mkb_led_ind_set(LED_IND_IDLE);
        mkb_db_ch_id_prev_set(ch_id);
        ________DBG_20211229_fds_test("1");
        mkb_db_ch_id_set(ch);

#if (defined(MKB_USB_ENABLED) && MKB_USB_ENABLED)
        if ((p_ch->config.info[ch_id].type == CHANNEL_TYPE_USB) && mkb_usb_state())
        {
            if (mkb_usb_conn_state(ch_id))
            {
                mkb_usb_stop(ch_id); //이전이 USB면 USB정지
            }
        }
#endif

        if (p_ch->config.info[ch].type == CHANNEL_TYPE_BT)
        {
            ________DBG_20210915_led_callstack("LED_STACK");
            mkb_led_ind_set(p_ch->led[ch].adv_fast);
            mkb_event_send(EVT_LED_CH_ADV_FAST);

            if (p_ch->config.info[ch].peer_id != PM_PEER_ID_INVALID)
            {
                ////////////////////////////////////
                // OLD!=NEW, PEER:O
                ////////////////////////////////////
                ________DBG_20211014_channel_job2("//NEW OLD!=NEW, PEER:O");
                MKB_LOG_INFO("CH(%d): peer_id delete request", ch);
                mkb_db_wait_for_disconnect_set(true);
                mkb_ble_adv_wait_for_advertizing_set(ADV_MODE_UNDIRECTED);
                mkb_ble_pm_delete_peer(ch);
                ________DBG_20211014_channel_select("NEW BT PEER:O CONN:-");
            }
            else
            {
                ////////////////////////////////////
                // OLD!=NEW, PEER:X
                ////////////////////////////////////
                ________DBG_20211014_channel_job2("//NEW OLD!=NEW, PEER:X");

                // find same peer address
                uint8_t addr[BLE_GAP_ADDR_LEN] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
                uint8_t find_cnt = 0, pm_del_ch = 0;
                for (uint8_t ch_f = 0; ch_f < CHANNEL_ID_MAX; ch_f++)
                {
                    if (memcmp(&p_ch->config.info[ch_f].peer_addr.addr, &addr, BLE_GAP_ADDR_LEN) &&
                        !memcmp(&p_ch->config.info[ch_f].peer_addr.addr, &p_ch->config.info[ch].peer_addr.addr, BLE_GAP_ADDR_LEN))
                    {
                        find_cnt++;

                        if (ch_f != ch)
                            pm_del_ch = ch_f;
                    }
                }

                if (find_cnt == 2)
                {
                    if ((p_ch->config.info[pm_del_ch].type == CHANNEL_TYPE_BT) && (p_ch->config.info[pm_del_ch].peer_id != PM_PEER_ID_INVALID))
                    {
                        MKB_LOG_INFO("CH(%d): peer_id delete request", pm_del_ch);
                        mkb_db_wait_for_disconnect_set(true);
                        mkb_ble_adv_wait_for_advertizing_set(ADV_MODE_UNDIRECTED);
                        mkb_ble_pm_delete_peer(pm_del_ch);
                    }
                }
                else
                {
                    if ((p_ch->config.info[ch_id].type == CHANNEL_TYPE_BT) && mkb_ble_conn_state(ch_id))
                    {
                        MKB_LOG_INFO("CH(%d), disconnect request", ch_id);
                        mkb_db_wait_for_disconnect_set(true);
                        mkb_ble_adv_wait_for_advertizing_set(ADV_MODE_UNDIRECTED);
                        mkb_ble_disconnect(ch_id);
                    }
                    else
                    {
                        MKB_LOG_INFO("CH(%d): start advertize with undirected mode", ch);
                        mkb_ble_adv_start(ch, ADV_MODE_UNDIRECTED);
                    }
                }
            }
        }
        else //새채널타입 USB
        {
            if (p_ch->config.info[ch_id].peer_id != PM_PEER_ID_INVALID)
            {
                if (mkb_ble_conn_state(ch_id))
                {
                    MKB_LOG_INFO("CH(%d), disconnect request", ch_id);
                    mkb_db_wait_for_disconnect_set(false);
                    mkb_ble_adv_stop_request();
                    mkb_ble_disconnect(ch_id);
                }
            }

#if (defined(MKB_USB_ENABLED) && MKB_USB_ENABLED)
            if (mkb_usb_state())
            {
                if (mkb_usb_conn_state(ch))
                {
                    ________DBG_20210915_led_callstack("LED_STACK");
                    mkb_led_ind_set(p_ch->led[ch].select);
                    mkb_event_send(EVT_LED_CH_CONNECT);
                }
                else
                {
                    MKB_LOG_INFO("CH(%d), start request", ch);
                    ________DBG_20210915_led_callstack("LED_STACK");
                    mkb_led_ind_set(p_ch->led[ch].select);
                    mkb_event_send(EVT_LED_CH_CONNECT);
                    mkb_usb_start(ch);
                }
            }
            else
#endif
            {
                ________DBG_20210915_led_callstack("LED_STACK");
                mkb_led_ind_set(LED_IND_IDLE);
            }
        }
    }
    else //같은채널에서 NEW
    {
        ________DBG_20210915_led_callstack("LED_STACK");
        mkb_led_ind_set(LED_IND_IDLE);
        if (p_ch->config.info[ch].type == CHANNEL_TYPE_BT)
        {
            ________DBG_20210915_led_callstack("LED_STACK");
            mkb_led_ind_set(p_ch->led[ch].adv_fast);
            mkb_event_send(EVT_LED_CH_ADV_FAST);

            if (p_ch->config.info[ch].peer_id != PM_PEER_ID_INVALID)
            {
                ////////////////////////////////////
                // OLD!=NEW, PEER:O, CONN:X
                ////////////////////////////////////
                MKB_LOG_INFO("CH(%d): peer_id delete request", ch);
                ________DBG_20211014_channel_job2("//NEW OLD!=NEW, PEER:O, CONN:X");
                ________DBG_20211014_channel_select("SAME NWBT PEER:O CONN:-");

                mkb_db_wait_for_disconnect_set(true);
                mkb_ble_adv_wait_for_advertizing_set(ADV_MODE_UNDIRECTED);
                mkb_ble_pm_delete_peer(ch);
            }
            else
            {
                if (mkb_ble_conn_state(ch))
                {
                    ////////////////////////////////////
                    // OLD!=NEW, PEER:X, CONN:O
                    ////////////////////////////////////
                    ________DBG_20211014_channel_job2("//NEW OLD!=NEW, PEER:X, CONN:O");

                    ________DBG_20211014_channel_select("SAME NWBT PEER:X CONN:O");
                    MKB_LOG_INFO("CH(%d), disconnect request", ch);
                    mkb_db_wait_for_disconnect_set(true);
                    mkb_ble_adv_wait_for_advertizing_set(ADV_MODE_UNDIRECTED);
                    mkb_ble_disconnect(ch);
                }
                else
                {
                    ////////////////////////////////////
                    // OLD!=NEW, PEER:X, CONN:X
                    ////////////////////////////////////
                    ________DBG_20211014_channel_job2("//NEW OLD!=NEW, PEER:X, CONN:X");
                    ________DBG_20211014_channel_select("SAME NWBT PEER:X CONN:X");
                    MKB_LOG_INFO("CH(%d): start advertize with undirected mode", ch);
                    mkb_ble_adv_start(ch, ADV_MODE_UNDIRECTED);
                }
            }
        }
        else
        {
#if (defined(MKB_USB_ENABLED) && MKB_USB_ENABLED)
            if (mkb_usb_state())
            {
                if (mkb_usb_conn_state(ch))
                {
                    ________DBG_20210915_led_callstack("LED_STACK");
                    mkb_led_ind_set(p_ch->led[ch].select);
                    mkb_event_send(EVT_LED_CH_CONNECT);
                }
                else
                {
                    MKB_LOG_INFO("CH(%d), start request", ch);
                    ________DBG_20210915_led_callstack("LED_STACK");
                    mkb_led_ind_set(p_ch->led[ch].select);
                    mkb_event_send(EVT_LED_CH_CONNECT);
                    mkb_usb_start(ch);
                }
            }
            else
#endif
            {
                ________DBG_20210915_led_callstack("LED_STACK");
                mkb_led_ind_set(LED_IND_IDLE);
            }
        }
    }
#endif // (defined(MKB_BLE_ENABLED) && MKB_BLE_ENABLED)
}

void mkb_coms_function_process(mkb_event_type_t event, uint8_t key)
{
    CHANNEL_INFO_t *p_ch_info;
    MKB_DB_CH_t *p_ch = mkb_db_ch_get();
    CHANNEL_ID_t ch_id = mkb_db_ch_id_get();

    p_ch_info = mkb_db_ch_info_get(mkb_db_ch_id_get());

    if (event == EVT_FUNC_DOWN)
    {
        ________DBG_20210930_fnbtn("func down");

        if (key != MKB_KEYBOARD_HOTKEY_HOME && key != MKB_KEYBOARD_HOTKEY_END && key != MKB_KEYBOARD_HOTKEY_PAGEUP && key != MKB_KEYBOARD_HOTKEY_PAGEDOWN)
        { //다른 키를 누르고 있으면 Down만 들어가고 Up이 안들어가는 상황이 생기기때문에 Fn을 누르면 키를 클리어해준다.
            memset(m_coms_ble_packet_key, 0, sizeof(m_coms_ble_packet_key[0]) * 8);
            mkb_event_send(EVT_HID_INPUT_KEYBOARD, m_coms_ble_packet_key, COMS_PACKET_MAX);
        }
    }
    else if (event == EVT_FUNC_UP)
    {
        ________DBG_20210930_fnbtn("func up");
    }

    switch (key)
    {
    case MKB_KEYBOARD_HOTKEY_MOKIBO:
        MKB_LOG_INFO("MOKIBO");
        break;
    case MKB_KEYBOARD_HOTKEY_CAPTURE:
    {
        MKB_LOG_INFO("CAPTURE");
        if (event == EVT_FUNC_DOWN)
        {
            switch (mkb_db_curr_central_type_get())
            {
            case CENTRAL_MAC:
            {
                mkb_event_send_ms(0, EVT_KEY_DOWN, HID_KEY_L_SHFT);
                mkb_event_send_ms(10, EVT_KEY_DOWN, HID_KEY_L_ALT);
                mkb_event_send_ms(20, EVT_KEY_DOWN, HID_KEY_3);

                mkb_event_send_ms(25, EVT_KEY_UP, HID_KEY_3);
                mkb_event_send_ms(30, EVT_KEY_UP, HID_KEY_L_ALT);
                mkb_event_send_ms(35, EVT_KEY_UP, HID_KEY_L_SHFT);
                break;
            }
            case CENTRAL_IOS:
            {
                mkb_event_send_ms(0, EVT_KEY_DOWN, HID_KEY_L_SHFT);
                mkb_event_send_ms(10, EVT_KEY_DOWN, HID_KEY_L_ALT);
                mkb_event_send_ms(20, EVT_KEY_DOWN, HID_KEY_3);

                mkb_event_send_ms(25, EVT_KEY_UP, HID_KEY_3);
                mkb_event_send_ms(30, EVT_KEY_UP, HID_KEY_L_ALT);
                mkb_event_send_ms(35, EVT_KEY_UP, HID_KEY_L_SHFT);
                break;
            }
            case CENTRAL_ANDROID:
            case CENTRAL_DEX:
            case CENTRAL_WINDOWS:
            case CENTRAL_LINUX:
            case CENTRAL_OTHER:
            {

                mkb_event_send_ms(0, EVT_KEY_DOWN, HID_KEY_PRNTSC);
                mkb_event_send_ms(16, EVT_KEY_UP, HID_KEY_PRNTSC);
                ________DBG_20211229_capture_hotkey_test("captured");
                break;
            }
            default:
                break;
            }
        }
    }
    break;

    case MKB_KEYBOARD_HOTKEY_ERASE_BOND:
        MKB_LOG_INFO("ERASE_BOND");
        break;
    case MKB_KEYBOARD_HOTKEY_HOUSE:
    {
        MKB_LOG_INFO("HOUSE");
        if (event == EVT_FUNC_DOWN)
        {
            switch (mkb_db_curr_central_type_get())
            {
            case CENTRAL_MAC:
            {
                mkb_event_send_ms(0, EVT_KEY_DOWN, HID_KEY_F11);
                mkb_event_send_ms(10, EVT_KEY_UP, HID_KEY_F11);
                break;
            }
            case CENTRAL_IOS:
            {
                mkb_event_send_ms(0, EVT_HID_INPUT_CONSUMER, m_iphone_home, sizeof(m_iphone_home));
                mkb_event_send_ms(30, EVT_HID_INPUT_CONSUMER, m_consumer_no_cmd, sizeof(m_consumer_no_cmd));
                break;
            }
            case CENTRAL_ANDROID:
            {
                mkb_event_send_ms(0, EVT_HID_INPUT_CONSUMER, m_android_home, sizeof(m_android_home));
                mkb_event_send_ms(30, EVT_HID_INPUT_CONSUMER, m_consumer_no_cmd, sizeof(m_consumer_no_cmd));
                break;
            }
            case CENTRAL_WINDOWS:
            case CENTRAL_LINUX:
            case CENTRAL_DEX:
            case CENTRAL_OTHER:
            {
                mkb_event_send_ms(0, EVT_KEY_DOWN, HID_KEY_L_GUI);
                mkb_event_send_ms(8, EVT_KEY_DOWN, HID_KEY_D);

                mkb_event_send_ms(16, EVT_KEY_UP, HID_KEY_D);
                mkb_event_send_ms(24, EVT_KEY_UP, HID_KEY_L_GUI);
                break;
            }
            default:
                break;
            }
        }
    }
    break;

#if (defined(MKB_BLE_BT1_ENABLED) && MKB_BLE_BT1_ENABLED)
    case MKB_KEYBOARD_HOTKEY_BT1:
        MKB_LOG_INFO("BT1");
        mkb_coms_channel_select(CHANNEL_ID_BT1);
        break;
#endif

#if (defined(MKB_BLE_BT2_ENABLED) && MKB_BLE_BT2_ENABLED)
    case MKB_KEYBOARD_HOTKEY_BT2:
        MKB_LOG_INFO("BT2");
        mkb_coms_channel_select(CHANNEL_ID_BT2);
        break;
#endif

#if (defined(MKB_BLE_BT3_ENABLED) && MKB_BLE_BT3_ENABLED)
    case MKB_KEYBOARD_HOTKEY_BT3:
        MKB_LOG_INFO("BT3");
        mkb_coms_channel_select(CHANNEL_ID_BT3);
        break;
#endif

#if (defined(MKB_USB_ENABLED) && MKB_USB_ENABLED)
    case MKB_KEYBOARD_HOTKEY_USB:
        MKB_LOG_INFO("USB");
        // mkb_coms_channel_select(CHANNEL_ID_USB);
        break;
#endif

    case MKB_KEYBOARD_HOTKEY_BTRCHK:
    {
        MKB_LOG_INFO("Battery check");
        break;
    }
    case MKB_KEYBOARD_HOTKEY_SWTAPP:
    {
        MKB_LOG_INFO("Switching App");
    }
    break;

    case MKB_KEYBOARD_HOTKEY_VKBD:
    {
        MKB_LOG_INFO("Virtual keyboard");
        if (event == EVT_FUNC_DOWN)
        {
            switch (mkb_db_curr_central_type_get())
            {
            case CENTRAL_MAC:
            {
                break;
            }
            case CENTRAL_IOS:
            {
                mkb_event_send_ms(0, EVT_HID_INPUT_CONSUMER, m_iphone_vkbd, sizeof(m_iphone_vkbd));
                mkb_event_send_ms(30, EVT_HID_INPUT_CONSUMER, m_consumer_no_cmd, sizeof(m_consumer_no_cmd));
                break;
            }
            case CENTRAL_ANDROID:
            {
                break;
            }
            case CENTRAL_WINDOWS:
            case CENTRAL_LINUX:
            case CENTRAL_DEX:
            case CENTRAL_OTHER:
            {
                break;
            }
            default:
                break;
            }
        }
    }
    break;

    case MKB_KEYBOARD_HOTKEY_FIND:
    {
        MKB_LOG_INFO("Search");
    }
    break;

#if (defined(MKB_BLE_BT1_ENABLED) && MKB_BLE_BT1_ENABLED)
    case MKB_KEYBOARD_HOTKEY_BT1_ADV:
    {
        MKB_LOG_INFO("BT1_ADV");

        mkb_coms_channel_new(CHANNEL_ID_BT1);
    }
    break;
#endif

#if (defined(MKB_BLE_BT2_ENABLED) && MKB_BLE_BT2_ENABLED)
    case MKB_KEYBOARD_HOTKEY_BT2_ADV:
        MKB_LOG_INFO("BT2_ADV");

        mkb_coms_channel_new(CHANNEL_ID_BT2);
        break;
#endif

#if (defined(MKB_BLE_BT3_ENABLED) && MKB_BLE_BT3_ENABLED)
    case MKB_KEYBOARD_HOTKEY_BT3_ADV:
        MKB_LOG_INFO("BT3_ADV");

        mkb_coms_channel_new(CHANNEL_ID_BT3);
        break;
#endif

#if (defined(MKB_USB_ENABLED) && MKB_USB_ENABLED)
    case MKB_KEYBOARD_HOTKEY_USB_NEW:
        MKB_LOG_INFO("USB_NEW");

        // mkb_coms_channel_new(CHANNEL_ID_USB);
        break;
#endif

    case MKB_KEYBOARD_HOTKEY_CENTRAL_IOS:
        MKB_LOG_INFO("CENTRAL_IOS");

        // LED INDICATION set

        uint8_t cur_central = mkb_db_central_type_get(mkb_db_ch_id_get());

        cur_central++;
        if (cur_central >= CENTRAL_MAX)
        {
            cur_central = 0;
        }

        mkb_db_central_type_set(mkb_db_ch_id_get(), cur_central);
        ________DBG_20211014_channel_os("os");
        mkb_event_send(EVT_LED_SUB_SET_CH_OS_CHECK_PUSH_DOWN);

        break;

    case MKB_KEYBOARD_HOTKEY_LOCKSCREEN:
        MKB_LOG_INFO("LOCKSCREEN");
        break;

    case MKB_KEYBOARD_HOTKEY_TOUCHLOCK:
        MKB_LOG_INFO("TOUCHLOCK");
        /*
        if (mkb_db_system_state_get() == SYSTEM_STATE_TOUCHLOCK)
        {
            pixart_software_reset();
            mkb_sm_change(SYSTEM_STATE_KEYBOARD);
        }
        else
        {
            mkb_sm_change(SYSTEM_STATE_TOUCHLOCK);
        }
        */
        break;

    case MKB_KEYBOARD_HOTKEY_INSERT:
        MKB_LOG_INFO("INSERT");
        if (event == EVT_FUNC_DOWN)
        {
            mkb_event_send_ms(0, EVT_KEY_DOWN, HID_KEY_INSERT);
        }
        else if (event == EVT_FUNC_UP)
        {
            mkb_event_send_ms(0, EVT_KEY_UP, HID_KEY_INSERT);
        }
        break;
    case MKB_KEYBOARD_HOTKEY_DELETE:
        MKB_LOG_INFO("DELETE");
        if (event == EVT_FUNC_DOWN)
        {
            mkb_event_send_ms(0, EVT_KEY_DOWN, HID_KEY_DELETE);
        }
        else if (event == EVT_FUNC_UP)
        {
            mkb_event_send_ms(0, EVT_KEY_UP, HID_KEY_DELETE);
        }
        break;

    case MKB_KEYBOARD_HOTKEY_BACKWARD:
        MKB_LOG_INFO("BACKWARD");
        break;

    case MKB_KEYBOARD_HOTKEY_V_UP:
        MKB_LOG_INFO("Volume up");
        if (event == EVT_FUNC_DOWN)
        {
            if (INPUT_REP_CONSUMER_LEN == 3)
            {
                m_coms_ble_packet_consumer[0] = 0xE9;
                m_coms_ble_packet_consumer[1] = 0x00;
                m_coms_ble_packet_consumer[2] = 0x00;
            }
            else
                m_coms_ble_packet_consumer[0] = 0x04;
        }
        else
        {
            m_coms_ble_packet_consumer[0] = 0x00;
            m_coms_ble_packet_consumer[1] = 0x00;
            m_coms_ble_packet_consumer[2] = 0x00;
        }
        if (p_ch->config.info[ch_id].type == CHANNEL_TYPE_BT)
            mkb_event_send(EVT_HID_INPUT_CONSUMER, m_coms_ble_packet_consumer, sizeof(m_coms_ble_packet_consumer));
#if (defined(MKB_USB_ENABLED) && MKB_USB_ENABLED)
        else
            mkb_usb_consumer(&m_coms_ble_packet_consumer[0], INPUT_REP_CONSUMER_LEN);
#endif
        break;
    case MKB_KEYBOARD_HOTKEY_V_DOWN:
        MKB_LOG_INFO("Volume down");
        if (event == EVT_FUNC_DOWN)
        {
            if (INPUT_REP_CONSUMER_LEN == 3)
            {
                m_coms_ble_packet_consumer[0] = 0xEA;
                m_coms_ble_packet_consumer[1] = 0x00;
                m_coms_ble_packet_consumer[2] = 0x00;
            }
            else
                m_coms_ble_packet_consumer[0] = 0x02;
        }
        else
        {
            m_coms_ble_packet_consumer[0] = 0x00;
            m_coms_ble_packet_consumer[1] = 0x00;
            m_coms_ble_packet_consumer[2] = 0x00;
        }

        if (p_ch->config.info[ch_id].type == CHANNEL_TYPE_BT)
            mkb_event_send(EVT_HID_INPUT_CONSUMER, m_coms_ble_packet_consumer, sizeof(m_coms_ble_packet_consumer));
#if (defined(MKB_USB_ENABLED) && MKB_USB_ENABLED)
        else
            mkb_usb_consumer(&m_coms_ble_packet_consumer[0], INPUT_REP_CONSUMER_LEN);
#endif
        break;
    case MKB_KEYBOARD_HOTKEY_V_MUTE:
        MKB_LOG_INFO("Volume MUTE");

        if (event == EVT_FUNC_DOWN)
        {
            if (INPUT_REP_CONSUMER_LEN == 3)
            {
                m_coms_ble_packet_consumer[0] = 0xE2;
                m_coms_ble_packet_consumer[1] = 0x00;
                m_coms_ble_packet_consumer[2] = 0x00;
            }
            else
                m_coms_ble_packet_consumer[0] = 0x01;
        }
        else
        {
            m_coms_ble_packet_consumer[0] = 0x00;
            m_coms_ble_packet_consumer[1] = 0x00;
            m_coms_ble_packet_consumer[2] = 0x00;
        }

        if (p_ch->config.info[ch_id].type == CHANNEL_TYPE_BT)
            mkb_event_send(EVT_HID_INPUT_CONSUMER, m_coms_ble_packet_consumer, sizeof(m_coms_ble_packet_consumer));
#if (defined(MKB_USB_ENABLED) && MKB_USB_ENABLED)
        else
            mkb_usb_consumer(&m_coms_ble_packet_consumer[0], INPUT_REP_CONSUMER_LEN);
#endif

        break;
    case MKB_KEYBOARD_HOTKEY_HOME:

        MKB_LOG_INFO("HOME");
        if (event == EVT_FUNC_DOWN)
        {
            switch (mkb_db_curr_central_type_get())
            {
            case CENTRAL_MAC:
            {
                mkb_event_send_ms(0, EVT_KEY_DOWN, HID_KEY_L_CTRL);
                mkb_event_send_ms(8, EVT_KEY_DOWN, HID_KEY_LEFT);
                break;
            }
            case CENTRAL_IOS:
            {
                mkb_event_send_ms(0, EVT_KEY_DOWN, HID_KEY_L_CTRL);
                mkb_event_send_ms(8, EVT_KEY_DOWN, HID_KEY_LEFT);
                break;
            }
            case CENTRAL_ANDROID:
            case CENTRAL_WINDOWS:
            case CENTRAL_LINUX:
            case CENTRAL_DEX:
            case CENTRAL_OTHER:
            {
                mkb_event_send_ms(0, EVT_KEY_DOWN, HID_KEY_HOME);
                break;
            }
            default:
                break;
            }
        }
        else if (event == EVT_FUNC_UP)
        {
            switch (mkb_db_curr_central_type_get())
            {
            case CENTRAL_MAC:
            {
                mkb_event_send_ms(0, EVT_KEY_UP, HID_KEY_LEFT);
                mkb_event_send_ms(8, EVT_KEY_UP, HID_KEY_L_CTRL);
                break;
            }
            case CENTRAL_IOS:
            {
                mkb_event_send_ms(0, EVT_KEY_UP, HID_KEY_LEFT);
                mkb_event_send_ms(8, EVT_KEY_UP, HID_KEY_L_CTRL);
                break;
            }
            case CENTRAL_ANDROID:
            case CENTRAL_WINDOWS:
            case CENTRAL_LINUX:
            case CENTRAL_DEX:
            case CENTRAL_OTHER:
            {
                mkb_event_send_ms(0, EVT_KEY_UP, HID_KEY_HOME);
                break;
            }
            default:
                break;
            }
        }
        break;

    case MKB_KEYBOARD_HOTKEY_END:
        MKB_LOG_INFO("END");
        if (event == EVT_FUNC_DOWN)
        {
            switch (mkb_db_curr_central_type_get())
            {
            case CENTRAL_MAC:
            {
                mkb_event_send_ms(0, EVT_KEY_DOWN, HID_KEY_L_CTRL);
                mkb_event_send_ms(8, EVT_KEY_DOWN, HID_KEY_RIGHT);
                break;
            }
            case CENTRAL_IOS:
            {
                mkb_event_send_ms(0, EVT_KEY_DOWN, HID_KEY_L_CTRL);
                mkb_event_send_ms(8, EVT_KEY_DOWN, HID_KEY_RIGHT);
                break;
            }
            case CENTRAL_ANDROID:
            case CENTRAL_WINDOWS:
            case CENTRAL_LINUX:
            case CENTRAL_DEX:
            case CENTRAL_OTHER:
            {
                mkb_event_send_ms(0, EVT_KEY_DOWN, HID_KEY_END);
                break;
            }
            default:
                break;
            }
        }
        else if (event == EVT_FUNC_UP)
        {
            switch (mkb_db_curr_central_type_get())
            {
            case CENTRAL_MAC:
            {
                mkb_event_send_ms(0, EVT_KEY_UP, HID_KEY_RIGHT);
                mkb_event_send_ms(8, EVT_KEY_UP, HID_KEY_L_CTRL);
                break;
                break;
            }
            case CENTRAL_IOS:
            {
                mkb_event_send_ms(0, EVT_KEY_UP, HID_KEY_RIGHT);
                mkb_event_send_ms(8, EVT_KEY_UP, HID_KEY_L_CTRL);
                break;
                break;
            }
            case CENTRAL_ANDROID:
            case CENTRAL_WINDOWS:
            case CENTRAL_LINUX:
            case CENTRAL_DEX:
            case CENTRAL_OTHER:
            {
                mkb_event_send_ms(0, EVT_KEY_UP, HID_KEY_END);
                break;
            }
            default:
                break;
            }
        }
        break;

    case MKB_KEYBOARD_HOTKEY_PAGEUP:
        MKB_LOG_INFO("PAGEUP");
        if (event == EVT_FUNC_DOWN)
        {
            switch (mkb_db_curr_central_type_get())
            {
            case CENTRAL_MAC:
            {
                mkb_event_send_ms(0, EVT_KEY_DOWN, HID_KEY_L_CTRL);
                mkb_event_send_ms(8, EVT_KEY_DOWN, HID_KEY_UP);
                break;
            }
            case CENTRAL_IOS:
            {
                mkb_event_send_ms(0, EVT_KEY_DOWN, HID_KEY_L_CTRL);
                mkb_event_send_ms(8, EVT_KEY_DOWN, HID_KEY_UP);
                break;
            }
            case CENTRAL_ANDROID:
            case CENTRAL_WINDOWS:
            case CENTRAL_LINUX:
            case CENTRAL_DEX:
            case CENTRAL_OTHER:
            {
                mkb_event_send_ms(0, EVT_KEY_DOWN, HID_KEY_PAGEUP);
                break;
            }
            default:
                break;
            }
        }
        else if (event == EVT_FUNC_UP)
        {
            switch (mkb_db_curr_central_type_get())
            {
            case CENTRAL_MAC:
            {
                mkb_event_send_ms(0, EVT_KEY_UP, HID_KEY_UP);
                mkb_event_send_ms(8, EVT_KEY_UP, HID_KEY_L_CTRL);
                break;
            }
            case CENTRAL_IOS:
            {
                mkb_event_send_ms(0, EVT_KEY_UP, HID_KEY_UP);
                mkb_event_send_ms(8, EVT_KEY_UP, HID_KEY_L_CTRL);
                break;
            }
            case CENTRAL_ANDROID:
            case CENTRAL_WINDOWS:
            case CENTRAL_LINUX:
            case CENTRAL_DEX:
            case CENTRAL_OTHER:
            {
                mkb_event_send_ms(0, EVT_KEY_UP, HID_KEY_PAGEUP);
                break;
            }
            default:
                break;
            }
        }
        break;

    case MKB_KEYBOARD_HOTKEY_PAGEDOWN:
        MKB_LOG_INFO("PAGEDOWN");
        if (event == EVT_FUNC_DOWN)
        {
            switch (mkb_db_curr_central_type_get())
            {
            case CENTRAL_MAC:
            {
                mkb_event_send_ms(0, EVT_KEY_DOWN, HID_KEY_L_CTRL);
                mkb_event_send_ms(8, EVT_KEY_DOWN, HID_KEY_DOWN);
                break;
            }
            case CENTRAL_IOS:
            {
                mkb_event_send_ms(0, EVT_KEY_DOWN, HID_KEY_L_CTRL);
                mkb_event_send_ms(8, EVT_KEY_DOWN, HID_KEY_DOWN);
                break;
            }
            case CENTRAL_ANDROID:
            case CENTRAL_WINDOWS:
            case CENTRAL_LINUX:
            case CENTRAL_DEX:
            case CENTRAL_OTHER:
            {
                mkb_event_send_ms(0, EVT_KEY_DOWN, HID_KEY_PAGEDOWN);
                break;
            }
            default:
                break;
            }
        }
        else if (event == EVT_FUNC_UP)
        {
            switch (mkb_db_curr_central_type_get())
            {
            case CENTRAL_MAC:
            {
                mkb_event_send_ms(0, EVT_KEY_UP, HID_KEY_DOWN);
                mkb_event_send_ms(8, EVT_KEY_UP, HID_KEY_L_CTRL);
                break;
            }
            case CENTRAL_IOS:
            {
                mkb_event_send_ms(0, EVT_KEY_UP, HID_KEY_DOWN);
                mkb_event_send_ms(8, EVT_KEY_UP, HID_KEY_L_CTRL);
                break;
            }
            case CENTRAL_ANDROID:
            case CENTRAL_WINDOWS:
            case CENTRAL_LINUX:
            case CENTRAL_DEX:
            case CENTRAL_OTHER:
            {
                mkb_event_send_ms(0, EVT_KEY_UP, HID_KEY_PAGEDOWN);
                break;
            }
            default:
                break;
            }
        }
        break;

    case MKB_KEYBOARD_HOTKEY_LEFTED:
        if (event == EVT_FUNC_DOWN)
        {
            drv_pixart_set_aamode(PIXART_AAMODE_TYPE_LEFT);
            mkb_db_aamode_set(PIXART_AAMODE_TYPE_LEFT);
            ________DBG_20210831_mkb_coms_aamode("lefted");
        }
        break;

    case MKB_KEYBOARD_HOTKEY_RIGHTED:
        if (event == EVT_FUNC_DOWN)
        {
            drv_pixart_set_aamode(PIXART_AAMODE_TYPE_RIGHT);
            mkb_db_aamode_set(PIXART_AAMODE_TYPE_RIGHT);
            ________DBG_20210831_mkb_coms_aamode("righted");
        }
        break;

    case MKB_KEYBOARD_HOTKEY_PRINTVER:
        if (event == EVT_FUNC_DOWN)
        {
            send_version_string();
            ________DBG_20210831_mkb_coms_aamode("printver");
        }
        break;

    case MKB_KEYBOARD_HOTKEY_TOGGLE_MIC:
    {
        uint8_t delay_time = 0;
        switch (mkb_db_curr_central_type_get())
        {
        case CENTRAL_MAC:
            // F1		zoom			webx			meet
            //-mac: 	shift+cmd+a 	ctrl+alt+m	 	cmd+d

            // zoom
            mkb_event_send_ms((++delay_time) * 8, EVT_KEY_DOWN, HID_KEY_L_ALT);
            mkb_event_send_ms((++delay_time) * 8, EVT_KEY_DOWN, HID_KEY_L_SHFT);
            mkb_event_send_ms((++delay_time) * 8, EVT_KEY_DOWN, HID_KEY_A);
            mkb_event_send_ms((++delay_time) * 8, EVT_KEY_UP, HID_KEY_A);
            mkb_event_send_ms((++delay_time) * 8, EVT_KEY_UP, HID_KEY_L_SHFT);
            mkb_event_send_ms((++delay_time) * 8, EVT_KEY_UP, HID_KEY_L_ALT);

            // webx
            mkb_event_send_ms((++delay_time) * 8, EVT_KEY_DOWN, HID_KEY_L_CTRL);
            mkb_event_send_ms((++delay_time) * 8, EVT_KEY_DOWN, HID_KEY_L_GUI);
            mkb_event_send_ms((++delay_time) * 8, EVT_KEY_DOWN, HID_KEY_M);
            mkb_event_send_ms((++delay_time) * 8, EVT_KEY_UP, HID_KEY_M);
            mkb_event_send_ms((++delay_time) * 8, EVT_KEY_UP, HID_KEY_L_GUI);
            mkb_event_send_ms((++delay_time) * 8, EVT_KEY_UP, HID_KEY_L_CTRL);
            break;
        case CENTRAL_IOS:
            // F1		zoom			webx			meet
            //-ios: 	shift+cmd+a
            // zoom
            mkb_event_send_ms((++delay_time) * 8, EVT_KEY_DOWN, HID_KEY_L_ALT);
            mkb_event_send_ms((++delay_time) * 8, EVT_KEY_DOWN, HID_KEY_L_SHFT);
            mkb_event_send_ms((++delay_time) * 8, EVT_KEY_DOWN, HID_KEY_A);
            mkb_event_send_ms((++delay_time) * 8, EVT_KEY_UP, HID_KEY_A);
            mkb_event_send_ms((++delay_time) * 8, EVT_KEY_UP, HID_KEY_L_SHFT);
            mkb_event_send_ms((++delay_time) * 8, EVT_KEY_UP, HID_KEY_L_ALT);
            break;
        case CENTRAL_WINDOWS:
            // F1		zoom			webx			meet
            //-win: 	alt+a			ctrl+m			ctrl+d
            // zoom
            mkb_event_send_ms((++delay_time) * 8, EVT_KEY_DOWN, HID_KEY_L_ALT);
            mkb_event_send_ms((++delay_time) * 8, EVT_KEY_DOWN, HID_KEY_A);
            mkb_event_send_ms((++delay_time) * 8, EVT_KEY_UP, HID_KEY_A);
            mkb_event_send_ms((++delay_time) * 8, EVT_KEY_UP, HID_KEY_L_ALT);
            // webx
            mkb_event_send_ms((++delay_time) * 8, EVT_KEY_DOWN, HID_KEY_L_CTRL);
            mkb_event_send_ms((++delay_time) * 8, EVT_KEY_DOWN, HID_KEY_M);
            mkb_event_send_ms((++delay_time) * 8, EVT_KEY_UP, HID_KEY_M);
            mkb_event_send_ms((++delay_time) * 8, EVT_KEY_UP, HID_KEY_L_CTRL);
            // meet
            mkb_event_send_ms((++delay_time) * 8, EVT_KEY_DOWN, HID_KEY_L_CTRL);
            mkb_event_send_ms((++delay_time) * 8, EVT_KEY_DOWN, HID_KEY_D);
            mkb_event_send_ms((++delay_time) * 8, EVT_KEY_UP, HID_KEY_D);
            mkb_event_send_ms((++delay_time) * 8, EVT_KEY_UP, HID_KEY_L_CTRL);
            break;
        case CENTRAL_ANDROID:
        case CENTRAL_DEX:
        case CENTRAL_LINUX:
            break;
        }
        ________DBG_20211229_cammic_hotkey("togglemic");
        break;
    }
    case MKB_KEYBOARD_HOTKEY_TOGGLE_CAM:
    {
        uint8_t delay_time = 0;
        switch (mkb_db_curr_central_type_get())
        {
        case CENTRAL_MAC:
            // F2		zoom			webx			meet
            //-mac: 	shift+cmd+v 	ctrl+alt+v	 	cmd+e

            // zoom
            mkb_event_send_ms((++delay_time) * 8, EVT_KEY_DOWN, HID_KEY_L_ALT);
            mkb_event_send_ms((++delay_time) * 8, EVT_KEY_DOWN, HID_KEY_L_SHFT);
            mkb_event_send_ms((++delay_time) * 8, EVT_KEY_DOWN, HID_KEY_V);
            mkb_event_send_ms((++delay_time) * 8, EVT_KEY_UP, HID_KEY_V);
            mkb_event_send_ms((++delay_time) * 8, EVT_KEY_UP, HID_KEY_L_SHFT);
            mkb_event_send_ms((++delay_time) * 8, EVT_KEY_UP, HID_KEY_L_ALT);

            // webx
            mkb_event_send_ms((++delay_time) * 8, EVT_KEY_DOWN, HID_KEY_L_CTRL);
            mkb_event_send_ms((++delay_time) * 8, EVT_KEY_DOWN, HID_KEY_L_GUI);
            mkb_event_send_ms((++delay_time) * 8, EVT_KEY_DOWN, HID_KEY_V);
            mkb_event_send_ms((++delay_time) * 8, EVT_KEY_UP, HID_KEY_V);
            mkb_event_send_ms((++delay_time) * 8, EVT_KEY_UP, HID_KEY_L_GUI);
            mkb_event_send_ms((++delay_time) * 8, EVT_KEY_UP, HID_KEY_L_CTRL);
            break;

        case CENTRAL_IOS:
            // F2		zoom			webx			meet
            //-ios: 	shift+cmd+v
            mkb_event_send_ms((++delay_time) * 8, EVT_KEY_DOWN, HID_KEY_L_ALT);
            mkb_event_send_ms((++delay_time) * 8, EVT_KEY_DOWN, HID_KEY_L_SHFT);
            mkb_event_send_ms((++delay_time) * 8, EVT_KEY_DOWN, HID_KEY_V);
            mkb_event_send_ms((++delay_time) * 8, EVT_KEY_UP, HID_KEY_V);
            mkb_event_send_ms((++delay_time) * 8, EVT_KEY_UP, HID_KEY_L_SHFT);
            mkb_event_send_ms((++delay_time) * 8, EVT_KEY_UP, HID_KEY_L_ALT);
            break;

        case CENTRAL_WINDOWS:
            // F2		zoom			webx			meet
            //-win: 	alt+v			ctrl+shift+v	ctrl+e
            mkb_event_send_ms((++delay_time) * 8, EVT_KEY_DOWN, HID_KEY_L_ALT);
            mkb_event_send_ms((++delay_time) * 8, EVT_KEY_DOWN, HID_KEY_V);
            mkb_event_send_ms((++delay_time) * 8, EVT_KEY_UP, HID_KEY_V);
            mkb_event_send_ms((++delay_time) * 8, EVT_KEY_UP, HID_KEY_L_ALT);
            // webx
            mkb_event_send_ms((++delay_time) * 8, EVT_KEY_DOWN, HID_KEY_L_CTRL);
            mkb_event_send_ms((++delay_time) * 8, EVT_KEY_DOWN, HID_KEY_V);
            mkb_event_send_ms((++delay_time) * 8, EVT_KEY_UP, HID_KEY_V);
            mkb_event_send_ms((++delay_time) * 8, EVT_KEY_UP, HID_KEY_L_CTRL);
            // meet
            mkb_event_send_ms((++delay_time) * 8, EVT_KEY_DOWN, HID_KEY_L_CTRL);
            mkb_event_send_ms((++delay_time) * 8, EVT_KEY_DOWN, HID_KEY_E);
            mkb_event_send_ms((++delay_time) * 8, EVT_KEY_UP, HID_KEY_E);
            mkb_event_send_ms((++delay_time) * 8, EVT_KEY_UP, HID_KEY_L_CTRL);
            ________DBG_20211229_delay_time_teste("delaytime:%d", delay_time);
            break;
        case CENTRAL_ANDROID:
        case CENTRAL_DEX:
        case CENTRAL_LINUX:
            break;
        }

        ________DBG_20211229_cammic_hotkey("togglecam");
        break;
    }
    }
}
#endif // (defined(MKB_KEYBOARD_ENABLED) && MKB_KEYBOARD_ENABLED)

#if (defined(MKB_BUTTON_ENABLED) && MKB_BUTTON_ENABLED)
void mkb_coms_button_process(mkb_event_type_t event, bool state)
{
    MKB_DB_CH_t *p_ch = mkb_db_ch_get();
    CHANNEL_ID_t ch_id = mkb_db_ch_id_get();
    CHANNEL_INFO_t *p_ch_info = mkb_db_ch_info_get(ch_id);

    switch (event)
    {
    case EVT_BUTTON_CLICK_LEFT:
        if (p_ch_info->type == CHANNEL_TYPE_BT)
        {
            if (state)
            {
                if (!mkb_ble_adv_running())
                {
                    ________DBG_20210915_led_callstack("LED_STACK");
                    // mkb_led_ind_set(p_ch->led[ch_id].button);
                }

                m_coms_ble_packet_button = BLE_BUTTON_LEFT;
            }
            else
            {
                if (!mkb_ble_adv_running())
                {
                    ________DBG_20210915_led_callstack("LED_STACK");
                    // mkb_led_ind_set(p_ch->led[ch_id].disconnect);
                }
                m_coms_ble_packet_button = BLE_BUTTON_RELEASE;
            }

            mkb_event_send(EVT_HID_INPUT_BUTTON, &m_coms_ble_packet_button, 1);
        }
#if (defined(MKB_USB_ENABLED) && MKB_USB_ENABLED)
        else
        {
            if (state)
            {
                ________DBG_20210915_led_callstack("LED_STACK");
                // mkb_led_ind_set(p_ch->led[ch_id].button);
            }
            else
            {
                ________DBG_20210915_led_callstack("LED_STACK");
                // mkb_led_ind_set(p_ch->led[ch_id].disconnect);
            }

            mkb_usb_mouse_button(0, state);
        }
#endif
        break;
    case EVT_BUTTON_CLICK_RIGHT:
        if (p_ch_info->type == CHANNEL_TYPE_BT)
        {
            if (state)
            {
                if (!mkb_ble_adv_running())
                {
                    ________DBG_20210915_led_callstack("LED_STACK");
                    // mkb_led_ind_set(p_ch->led[ch_id].button);
                }

                m_coms_ble_packet_button = BLE_BUTTON_RIGHT;
            }
            else
            {
                if (!mkb_ble_adv_running())
                {
                    ________DBG_20210915_led_callstack("LED_STACK");
                    // mkb_led_ind_set(p_ch->led[ch_id].disconnect);
                }

                m_coms_ble_packet_button = BLE_BUTTON_RELEASE;
            }

            mkb_event_send(EVT_HID_INPUT_BUTTON, &m_coms_ble_packet_button, 1);
        }
#if (defined(MKB_USB_ENABLED) && MKB_USB_ENABLED)
        else
        {
            if (state)
            {
                ________DBG_20210915_led_callstack("LED_STACK");
                // mkb_led_ind_set(p_ch->led[ch_id].button);
            }
            else
            {
                ________DBG_20210915_led_callstack("LED_STACK");
                // mkb_led_ind_set(p_ch->led[ch_id].disconnect);
            }

            mkb_usb_mouse_button(1, state);
        }
#endif
        break;
    default:
    {
        break;
    }
    }
}

bool mkb_coms_is_button_left(void)
{
    return m_coms_ble_packet_button == BLE_BUTTON_LEFT;
}
#endif // (defined(MKB_BUTTON_ENABLED) && MKB_BUTTON_ENABLED)

#if (defined(MKB_TOUCHPAD_ENABLED) && MKB_TOUCHPAD_ENABLED)
void mkb_coms_touch_process(mkb_event_t *p_event)
{
    MKB_DB_CH_t *p_ch = mkb_db_ch_get();
    CHANNEL_ID_t ch_id = mkb_db_ch_id_get();
    CHANNEL_INFO_t *p_ch_info = mkb_db_ch_info_get(ch_id);

    switch (p_event->type)
    {
    case EVT_TOUCH_MOVE:
        if (p_ch_info->type == CHANNEL_TYPE_BT)
        {
            mkb_ble_movement_send(p_event->touch.x, p_event->touch.y);
        }
#if (defined(MKB_USB_ENABLED) && MKB_USB_ENABLED)
        else
        {
            mkb_usb_mouse_move((int8_t)(p_event->touch.x % 8), (int8_t)(p_event->touch.y % 8));
        }
#endif
        break;
    case EVT_TOUCH_SCROLL:
        if (p_ch_info->type == CHANNEL_TYPE_BT)
            mkb_ble_scroll_send(0, p_event->touch.x, p_event->touch.y);
#if (defined(MKB_USB_ENABLED) && MKB_USB_ENABLED)
        else
            mkb_usb_mouse_scroll(p_event->touch.y);
#endif
        break;
    case EVT_TOUCH_TAB:
        switch (p_event->touch.tab)
        {
        case TOUCHPAD_TAP_ONE:
            if (p_ch_info->type == CHANNEL_TYPE_BT)
            {
                m_coms_ble_packet_button = BLE_BUTTON_LEFT;
                mkb_event_send(EVT_HID_INPUT_BUTTON, &m_coms_ble_packet_button, 1);

                m_coms_ble_packet_button = BLE_BUTTON_RELEASE;
                mkb_event_send(EVT_HID_INPUT_BUTTON, &m_coms_ble_packet_button, 1);
            }
#if (defined(MKB_USB_ENABLED) && MKB_USB_ENABLED)
            else
            {
                mkb_usb_mouse_button(0, true);
                mkb_usb_mouse_button(0, false);
            }
#endif
            break;
        case TOUCHPAD_TAP_TWO:
            if (p_ch_info->type == CHANNEL_TYPE_BT)
            {
                m_coms_ble_packet_button = BLE_BUTTON_RIGHT;
                mkb_event_send(EVT_HID_INPUT_BUTTON, &m_coms_ble_packet_button, 1);

                m_coms_ble_packet_button = BLE_BUTTON_RELEASE;
                mkb_event_send(EVT_HID_INPUT_BUTTON, &m_coms_ble_packet_button, 1);
            }
#if (defined(MKB_USB_ENABLED) && MKB_USB_ENABLED)
            else
            {
                mkb_usb_mouse_button(1, true);
                mkb_usb_mouse_button(1, false);
            }
#endif
            break;
        }
        break;
    }
}

void mkb_coms_tracpad_process(mkb_event_t *p_event)
{
    MKB_DB_CH_t *p_ch = mkb_db_ch_get();
    CHANNEL_ID_t ch_id = mkb_db_ch_id_get();
    CHANNEL_INFO_t *p_ch_info = mkb_db_ch_info_get(ch_id);

    BLE_REPORT_DATA_TOUCH_t buf = {0};
    touch_info_t *p_touch_info;

    switch (p_event->type)
    {
    case EVT_TRACPAD_MOVE:
        p_touch_info = (touch_info_t *)p_event->tracpad.data;

#if MKB_BLE_TOUCH_DEVICE_ENABLED
#if MKB_BLE_TOUCH_DEVICE_TYPE == BLE_TOUCH_DEVICE_PAD
        for (uint8_t i = 0; i < p_touch_info->touchCnt; i++)
        {
            buf.finger[i].confidence = 1;                          //(p_info->tip) ? p_info->confidence:0;
            buf.finger[i].tip = p_touch_info->info[i].tip;         // touch is currently on the panel
            buf.finger[i].inrange = 1;                             //(p_info->tip) ? p_info->confidence:0;
            buf.finger[i].touchID = p_touch_info->info[i].touchID; // An arbitrary ID tag associated with a finger ( 0 ~ 9)
            // buf.finger[i].pressure = p_touch_info->info[i].pressure;    // An arbitrary ID tag associated with a finger ( 0 ~ 9)
            // buf.finger[i].width = p_touch_info->info[i].width;    // An arbitrary ID tag associated with a finger ( 0 ~ 9)
            // buf.finger[i].height = p_touch_info->info[i].height;    // An arbitrary ID tag associated with a finger ( 0 ~ 9)
            buf.finger[i].x = (p_touch_info->info[i].x * MKB_TOUCHPAD_LOGICAL_MAX_X) / MKB_TOUCHPAD_PIXEL_MAX_X; // 16bit x-coordinates in pixels
            buf.finger[i].y = (p_touch_info->info[i].y * MKB_TOUCHPAD_LOGICAL_MAX_Y) / MKB_TOUCHPAD_PIXEL_MAX_Y; // 16bit y-coordinates in pixels

            MKB_LOG_INFO("%d: {x:%04d,y:%04d} tip:%d\n", i,
                         buf.finger[i].x, buf.finger[i].y, buf.finger[i].tip);
        }
        buf.timestamp = p_touch_info->timestamp; // unit: 0.1ms
        buf.touchCnt = p_touch_info->touchCnt;   // total number of current touches
        buf.button = p_touch_info->button;       /* 1byte */

        if (p_ch_info->type == CHANNEL_TYPE_BT)
            mkb_ble_touchpad_send((uint8_t *)&buf, sizeof(buf));
#if (defined(MKB_USB_ENABLED) && MKB_USB_ENABLED)
// else
// mkb_usb_mouse_move(p_event->touch.x, p_event->touch.y);
#endif

#elif MKB_BLE_TOUCH_DEVICE_TYPE == BLE_TOUCH_DEVICE_SCREEN
        for (uint8_t i = 0; i < p_touch_info->touchCnt; i++)
        {
            buf.tip = p_touch_info->info[i].tip;         // touch is currently on the panel
            buf.inrange = 1;                             //(p_info->tip) ? p_info->confidence:0;
            buf.touchID = p_touch_info->info[i].touchID; // An arbitrary ID tag associated with a finger ( 0 ~ 9)
            // buf.finger[i].pressure = p_touch_info->info[i].pressure;    // An arbitrary ID tag associated with a finger ( 0 ~ 9)
            // buf.finger[i].width = p_touch_info->info[i].width;    // An arbitrary ID tag associated with a finger ( 0 ~ 9)
            // buf.finger[i].height = p_touch_info->info[i].height;    // An arbitrary ID tag associated with a finger ( 0 ~ 9)
            buf.x = (p_touch_info->info[i].x * MKB_TOUCHPAD_LOGICAL_MAX_X) / MKB_TOUCHPAD_PIXEL_MAX_X; // 16bit x-coordinates in pixels
            buf.y = (p_touch_info->info[i].y * MKB_TOUCHPAD_LOGICAL_MAX_Y) / MKB_TOUCHPAD_PIXEL_MAX_Y; // 16bit y-coordinates in pixels
            // buf.x = (p_touch_info->info[i].x);// * MKB_TOUCHPAD_LOGICAL_MAX_X) / MKB_TOUCHPAD_PHYSICAL_MAX_X ;			  // 16bit x-coordinates in pixels
            // buf.y = (p_touch_info->info[i].y);// * MKB_TOUCHPAD_LOGICAL_MAX_Y) / MKB_TOUCHPAD_PHYSICAL_MAX_Y;			  // 16bit y-coordinates in pixels
            buf.touchCnt = p_touch_info->touchCnt; // total number of current touches

            MKB_LOG_INFO("%d: {x:%04d,y:%04d} tip:%d\n", i,
                         buf.x, buf.y, buf.tip);

            if (p_ch_info->type == CHANNEL_TYPE_BT)
                mkb_ble_touchpad_send((uint8_t *)&buf, sizeof(buf));
#if (defined(MKB_USB_ENABLED) && MKB_USB_ENABLED)
// else
// mkb_usb_mouse_move(p_event->touch.x, p_event->touch.y);
#endif
        }
#elif MKB_BLE_TOUCH_DEVICE_TYPE == BLE_TOUCH_DEVICE_PEN
        buf.tip = p_touch_info->info[0].tip;                                                       // touch is currently on the panel
        buf.inrange = 1;                                                                           //(p_info->tip) ? p_info->confidence:0;
        buf.x = (p_touch_info->info[0].x * MKB_TOUCHPAD_LOGICAL_MAX_X) / MKB_TOUCHPAD_PIXEL_MAX_X; // 16bit x-coordinates in pixels
        buf.y = (p_touch_info->info[0].y * MKB_TOUCHPAD_LOGICAL_MAX_Y) / MKB_TOUCHPAD_PIXEL_MAX_Y; // 16bit y-coordinates in pixels

        if (p_ch_info->type == CHANNEL_TYPE_BT)
            mkb_ble_touchpad_send((uint8_t *)&buf, sizeof(buf));
#if (defined(MKB_USB_ENABLED) && MKB_USB_ENABLED)
// else
// mkb_usb_mouse_move(p_event->touch.x, p_event->touch.y);
#endif
#elif MKB_BLE_TOUCH_DEVICE_TYPE == BLE_TOUCH_DEVICE_POINTER
        buf.button = p_touch_info->button;                                                         /* 1byte */
        buf.x = (p_touch_info->info[0].x * MKB_TOUCHPAD_LOGICAL_MAX_X) / MKB_TOUCHPAD_PIXEL_MAX_X; // 16bit x-coordinates in pixels
        buf.y = (p_touch_info->info[0].y * MKB_TOUCHPAD_LOGICAL_MAX_Y) / MKB_TOUCHPAD_PIXEL_MAX_Y; // 16bit y-coordinates in pixels

        if (p_ch_info->type == CHANNEL_TYPE_BT)
            mkb_ble_touchpad_send((uint8_t *)&buf, sizeof(buf));
#if (defined(MKB_USB_ENABLED) && MKB_USB_ENABLED)
// else
// mkb_usb_mouse_move(p_event->touch.x, p_event->touch.y);
#endif
#endif
#endif // MKB_BLE_TOUCH_DEVICE_ENABLED
        break;
    }
}

#endif // (defined(MKB_TOUCHPAD_ENABLED) && MKB_TOUCHPAD_ENABLED)

bool mkb_coms_event_handler(mkb_event_t *p_event)
{
    MKB_DB_CH_t *p_ch = mkb_db_ch_get();
    CHANNEL_ID_t ch_id = mkb_db_ch_id_get();

    ASSERT(p_event != NULL);

#if (defined(MKB_WDT_ENABLED) && MKB_WDT_ENABLED)
    mkb_wdt_feed();
#endif

#if (defined(NRF_PWR_MGMT_CONFIG_STANDBY_TIMEOUT_ENABLED) && NRF_PWR_MGMT_CONFIG_STANDBY_TIMEOUT_ENABLED)
    switch (EVENT_GROUP(p_event->type))
    {
    case EVT_GROUP_FUNC:
    case EVT_GROUP_KEY:
    case EVT_GROUP_BUTTON:
    case EVT_GROUP_TOUCH:
    case EVT_GROUP_TRACPAD:
        nrf_pwr_mgmt_feed();
        // MKB_LOG_INFO("Power management feed");
        break;
    }
#endif

    switch (p_event->type)
    {
#if (defined(MKB_KEYBOARD_ENABLED) && MKB_KEYBOARD_ENABLED)
    case EVT_KEY_DOWN:
        ________DBG_20210903_event_process_test("mkb_coms_key_process(EVT_KEY_DOWN, p_event->key.id:%d);",
                                                p_event->key.id);
        mkb_coms_key_process(EVT_KEY_DOWN, p_event->key.id);
        break;
    case EVT_KEY_UP:
        ________DBG_20210903_event_process_test("mkb_coms_key_process(EVT_KEY_UP, p_event->key.id:%d);",
                                                p_event->key.id);
        mkb_coms_key_process(EVT_KEY_UP, p_event->key.id);
        break;
    case EVT_FUNC_UP:
        ________DBG_20210930_fnbtn("func up");
        ________DBG_20210903_event_process_test("mkb_coms_function_process(EVT_FUNC_UP, p_event->key.id:%d);",
                                                p_event->key.id);
        mkb_coms_function_process(EVT_FUNC_UP, p_event->key.id);
        break;
    case EVT_FUNC_DOWN:
        ________DBG_20210930_fnbtn("func down");
        ________DBG_20210903_event_process_test("mkb_coms_function_process(EVT_FUNC_DOWN, p_event->key.id:%d);",
                                                p_event->key.id);
        mkb_coms_function_process(EVT_FUNC_DOWN, p_event->key.id);
        break;
    case EVT_FUNC_HOLD:
        ________DBG_20210930_fnbtn("func hold");
        ________DBG_20210903_event_process_test("mkb_coms_function_process(EVT_FUNC_HOLD, p_event->key.id:%d);",
                                                p_event->key.id);
        mkb_coms_function_process(EVT_FUNC_HOLD, p_event->key.id);
        break;
#endif // (defined(MKB_KEYBOARD_ENABLED) && MKB_KEYBOARD_ENABLED)

    case EVT_CONSUMER_DOWN:
        ________DBG_20210903_event_process_test("mkb_coms_key_process(EVT_CONSUMER_DOWN, p_event->key.id:%d);",
                                                p_event->key.id);
        mkb_coms_key_process(EVT_CONSUMER_DOWN, p_event->key.id);
        break;
    case EVT_CONSUMER_UP:
        ________DBG_20210903_event_process_test("mkb_coms_key_process(EVT_CONSUMER_UP, p_event->key.id:%d);",
                                                p_event->key.id);
        mkb_coms_key_process(EVT_CONSUMER_UP, p_event->key.id);

#if (defined(MKB_BUTTON_ENABLED) && MKB_BUTTON_ENABLED)
    case EVT_BUTTON_CLICK_LEFT:
        ________DBG_20210903_event_process_test("mkb_coms_button_process(p_event->type:%d, p_event->button.state:%d);",
                                                p_event->type,
                                                p_event->button.state);
        mkb_coms_button_process(p_event->type, p_event->button.state);
        break;
    case EVT_BUTTON_CLICK_RIGHT:
        ________DBG_20210903_event_process_test("mkb_coms_button_process(p_event->type:%d, p_event->button.state:%d);",
                                                p_event->type,
                                                p_event->button.state);
        mkb_coms_button_process(p_event->type, p_event->button.state);
        break;
    case EVT_BUTTON_TOUCH_LEFT:
    case EVT_BUTTON_TOUCH_RIGHT:
        break;
#endif // (defined(MKB_BUTTON_ENABLED) && MKB_BUTTON_ENABLED)

#if (defined(MKB_TOUCHPAD_ENABLED) && MKB_TOUCHPAD_ENABLED)
    case EVT_TOUCH_MOVE:
    case EVT_TOUCH_SCROLL:
    case EVT_TOUCH_TAB:
        ________DBG_20210903_event_process_test("mkb_coms_touch_process(p_event:%d);", p_event);
        mkb_coms_touch_process(p_event);
        break;

    case EVT_TRACPAD_MOVE:
        ________DBG_20210903_event_process_test("mkb_coms_tracpad_process(p_event:%d);", p_event);
        mkb_coms_tracpad_process(p_event);
        break;
#endif // (defined(MKB_TOUCHPAD_ENABLED) && MKB_TOUCHPAD_ENABLED)
    }

    return false;
}

ret_code_t mkb_coms_cli(size_t size, char **params)
{
    ret_code_t err_code = NRF_SUCCESS;
    if (!strcmp(params[1], "help"))
    {
    }
    if (!strcmp(params[1], "start"))
    {
        mkb_coms_start();
        return;
    }
    else if (!strcmp(params[1], "mkb_coms_channel_new"))
    {
        mkb_coms_channel_new((CHANNEL_ID_t)atoi(params[2]));
        return;
    }
    else if (!strcmp(params[1], "mkb_coms_channel_select"))
    {
        // mkb_coms_start();
        mkb_coms_channel_select((CHANNEL_ID_t)atoi(params[2]));
        return;
    }
    else if (!strcmp(params[1], "mkb_db_ch_id_get"))
    {
        ________DBG_20210831_mkb_cli_dbg("%d", mkb_db_ch_id_get());
        return;
    }
    else if (!strcmp(params[1], "mkb_coms_channel_new"))
    {
        // mkb_coms_start();
        mkb_coms_channel_new((CHANNEL_ID_t)atoi(params[2]));
        return;
    }
    else
    {
        err_code = NRF_ERROR_INTERNAL;
    }

    return err_code;
}

#endif // (defined(MKB_COMS_ENABLED) && MKB_COMS_ENABLED)
