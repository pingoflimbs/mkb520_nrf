/*
* ===============================================================================
* Copyright (C) 2017-2019
* INNOPRESSO INC. All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
* ===============================================================================
*/

#ifndef __MKB_TOUCHPAD_PIXART_T_H__
#define __MKB_TOUCHPAD_PIXART_T_H__

#include "mkb_config.h"

typedef enum
{
    TOUCHPAD_JOB_STATE_IDLE,
    TOUCHPAD_JOB_STATE_ONE,
    TOUCHPAD_JOB_STATE_TWO,
    TOUCHPAD_JOB_STATE_THREE,
    TOUCHPAD_JOB_STATE_FOUR,
    TOUCHPAD_JOB_STATE_FIVE,
    TOUCHPAD_JOB_STATE_MAX
} touchpad_job_state_t;

#define TOUCHPAD_MOVE_MINIMUM MKB_TOUCHPAD_MOVE_MINIMUM
#define TOUCHPAD_TAP_THRESHOLD_TIME_MIN MKB_TOUCHPAD_TAP_THRESHOLD_TIME_MIN
#define TOUCHPAD_TAP_THRESHOLD_TIME_MAX MKB_TOUCHPAD_TAP_THRESHOLD_TIME_MAX
#define TOUCHPAD_TAP_THRESHOLD_PRESSURE MKB_TOUCHPAD_TAP_THRESHOLD_PRESSURE
#define TOUCHPAD_TAP_THRESHOLD_MOVE MKB_TOUCHPAD_TAP_THRESHOLD_MOVE

#define TOUCHPAD_MOVE_SPEED MKB_TOUCHPAD_MOVE_SPEED
#define TOUCHPAD_SCROLL_SPEED MKB_TOUCHPAD_SCROLL_SPEED

extern void mkb_touchpad_read_process(void *p_data);

#endif /* __MKB_TOUCHPAD_PIXART_T_H__ */
