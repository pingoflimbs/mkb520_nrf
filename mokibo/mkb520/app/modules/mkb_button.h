/*
* ===============================================================================
* Copyright (C) 2017-2019
* INNOPRESSO INC. All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
* ===============================================================================
*/

#ifndef __MKB_BUTTON__
#define __MKB_BUTTON__

#include "mkb_config.h"

#if defined(BOARD_MKB10056)
#define BUTTON_ID_TOUCH_LEFT 0
#define BUTTON_ID_CLICK_LEFT 1
#else
#define BUTTON_ID_TOUCH_LEFT 0
#define BUTTON_ID_TOUCH_RIGHT 1
#define BUTTON_ID_CLICK_LEFT 2
#define BUTTON_ID_CLICK_RIGHT 3
#endif

extern void mkb_button_init(void);
extern void mkb_button_enable(void);

#endif /* __MKB_BUTTON__ */
