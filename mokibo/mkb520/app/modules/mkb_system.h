/*
* ===============================================================================
* Copyright (C) 2017-2019
* INNOPRESSO INC. All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
* ===============================================================================
*/

#ifndef __MKB_SYSTEM__
#define __MKB_SYSTEM__

#include "mkb_event.h"

extern bool mkb_system_event_handler(mkb_event_t *p_event);
extern void mkb_system_reset(void);

#endif /* __MKB_SYSTEM__ */
