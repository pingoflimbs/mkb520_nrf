/*
 * ===============================================================================
 * Copyright (C) 2017-2019
 * INNOPRESSO INC. All Rights Reserved
 *
 * The content of this file is copyrighted by INNOPRESSO or its
 * affiliated, associated or related companies. All copyrights and other
 * associated intellectual property rights related thereto are expressly
 * reserved by INNOPRESSO INC.
 * ===============================================================================
 */

#include <stdbool.h>
#include <stdint.h>
#include <stdarg.h>

#include "sdk_common.h"

#include "ble.h"
#include "ble_err.h"
#include "ble_conn_params.h"
#include "ble_conn_state.h"
#include "ble_advertising.h"
#include "ble_gap.h"
#include "ble_bas.h"

#include "nrf_assert.h"
#include "nrf_error.h"
#include "nrf_delay.h"
#include "nrf_cli.h"
#include "nrf_balloc.h"
#include "nrf_pwr_mgmt.h"
#include "nrf_sdh_soc.h"
#include "nrf_sdh_ble.h"

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

#include "peer_manager.h"
#include "peer_manager_handler.h"
#include "bsp.h"
#include "fds.h"

#include "app_error.h"
#include "app_timer.h"
#include "app_scheduler.h"

#define MKB_LOG_LEVEL MKB_BLE_ADV_LOG_LEVEL
#include "mkb_common.h"

#if (defined(MKB_BLE_ENABLED) && MKB_BLE_ENABLED)
#include "mkb_ble.h"
#include "mkb_ble_adv.h"
#include "mkb_ble_pm.h"

#if (defined(MKB_EVENT_ENABLED) && MKB_EVENT_ENABLED)
#include "mkb_event.h"
#else
#error "You have to enable event at mkb_config.h!"
#endif // (defined(MKB_EVENT_ENABLED) && MKB_EVENT_ENABLED)

static MKB_BLE_ADV_STATE_t m_ble_adv_state;
static MKB_BLE_ADV_MODE_t m_ble_adv_wait_for_advertizing;

BLE_ADVERTISING_DEF(m_advertising); /**< Advertising module instance. */

static ble_uuid_t m_adv_uuids[] = /**< Universally unique service identifiers. */
    {
        {BLE_UUID_HUMAN_INTERFACE_DEVICE_SERVICE, BLE_UUID_TYPE_BLE}};

#if (defined(MKB_BLE_SWIFT_PAIR) && MKB_BLE_SWIFT_PAIR)
static uint8_t m_sp_payload[] = /**< Payload of advertising data structure for Microsoft Swift Pair feature. */
    {
        MICROSOFT_BEACON_ID,
        MICROSOFT_BEACON_SUB_SCENARIO,
        RESERVED_RSSI_BYTE};
static ble_advdata_manuf_data_t m_sp_manuf_advdata = /**< Advertising data structure for Microsoft Swift Pair feature. */
    {
        .company_identifier = MICROSOFT_VENDOR_ID,
        .data =
            {
                .size = sizeof(m_sp_payload),
                .p_data = &m_sp_payload[0]}};
static ble_advdata_t m_sp_advdata;
#endif

/**@brief Function for handling advertising events.
 *
 * @details This function will be called for advertising events which are passed to the application.
 *
 * @param[in] ble_adv_evt  Advertising event.
 */
static void mkb_ble_adv_on_adv_evt(ble_adv_evt_t ble_adv_evt)
{
    ret_code_t err_code;
    MKB_DB_CH_t *p_ch = mkb_db_ch_get();
    CHANNEL_ID_t ch_id = mkb_db_ch_id_get();

    switch (ble_adv_evt)
    {
    case BLE_ADV_EVT_DIRECTED_HIGH_DUTY:
        MKB_LOG_INFO("Directed high duty advertising.");
        ________DBG_20210930_ble_adv_event("BLE_ADV_EVT_DIRECTED_HIGH_DUTY");

        m_ble_adv_state.adv_active = true;
        mkb_led_ind_set(p_ch->led[ch_id].adv_direct);
        break;

    case BLE_ADV_EVT_DIRECTED:
        MKB_LOG_INFO("Directed advertising.");
        ________DBG_20210930_ble_adv_event("BLE_ADV_EVT_DIRECTED");
        m_ble_adv_state.adv_active = true;
        mkb_led_ind_set(p_ch->led[ch_id].adv_direct);
        break;

    case BLE_ADV_EVT_FAST:
        MKB_LOG_INFO("Fast advertising.");
        ________DBG_20210930_ble_adv_event("BLE_ADV_EVT_FAST");
        m_ble_adv_state.adv_active = true;
        mkb_led_ind_set(p_ch->led[ch_id].adv_fast);

        break;

    case BLE_ADV_EVT_SLOW:
        MKB_LOG_INFO("Slow advertising.");
        ________DBG_20210930_ble_adv_event("BLE_ADV_EVT_SLOW");
        m_ble_adv_state.adv_active = true;

#if (defined(MKB_BLE_SWIFT_PAIR) && MKB_BLE_SWIFT_PAIR)
        m_sp_advdata.p_manuf_specific_data = NULL;
        err_code = ble_advertising_advdata_update(&m_advertising, &m_sp_advdata, NULL);
        MKB_ERROR_CHECK(err_code);
#endif

        mkb_led_ind_set(p_ch->led[ch_id].adv_slow);
        break;

    case BLE_ADV_EVT_FAST_WHITELIST:
        MKB_LOG_INFO("Fast advertising with whitelist.");
        ________DBG_20210930_ble_adv_event("BLE_ADV_EVT_FAST_WHITELIST");
        m_ble_adv_state.adv_active = true;
        mkb_led_ind_set(p_ch->led[ch_id].adv_whitelist);
        break;
    case BLE_ADV_EVT_SLOW_WHITELIST:
        MKB_LOG_INFO("Slow advertising with whitelist.");
        ________DBG_20210930_ble_adv_event("BLE_ADV_EVT_SLOW_WHITELIST");
        m_ble_adv_state.adv_active = true;
        mkb_led_ind_set(p_ch->led[ch_id].adv_whitelist);
        break;

    case BLE_ADV_EVT_IDLE:
        MKB_LOG_INFO("BLE_ADV_EVT_IDLE");
        ________DBG_20210930_ble_adv_event("BLE_ADV_EVT_IDLE");
        m_ble_adv_state.adv_active = false;
        if (!mkb_db_system_state_is_touch())
        {
            mkb_led_ind_set(p_ch->led[ch_id].adv_idle);
        }
        break;

    case BLE_ADV_EVT_WHITELIST_REQUEST:
    {
        ble_gap_addr_t whitelist_addrs[BLE_GAP_WHITELIST_ADDR_MAX_COUNT];
        ble_gap_irk_t whitelist_irks[BLE_GAP_WHITELIST_ADDR_MAX_COUNT];
        uint32_t addr_cnt = BLE_GAP_WHITELIST_ADDR_MAX_COUNT;
        uint32_t irk_cnt = BLE_GAP_WHITELIST_ADDR_MAX_COUNT;

        MKB_LOG_INFO("BLE_ADV_EVT_WHITELIST_REQUEST");
        ________DBG_20210930_ble_adv_event("BLE_ADV_EVT_WHITELIST_REQUEST");

        if (m_ble_adv_state.mode == ADV_MODE_WHITELIST)
        {
            err_code = pm_whitelist_get(whitelist_addrs, &addr_cnt,
                                        whitelist_irks, &irk_cnt);
            if (err_code != NRF_ERROR_NOT_FOUND)
            {
                MKB_ERROR_CHECK(err_code);
                MKB_LOG_DEBUG("pm_whitelist_get returns %d addr in whitelist and %d irk whitelist",
                              addr_cnt,
                              irk_cnt);

                if (addr_cnt)
                {
                    // Set the correct identities list (no excluding peers with no Central Address Resolution).
                    mkb_ble_pm_identities_set(PM_PEER_ID_LIST_SKIP_NO_IRK);
                }

                // Apply the whitelist.
                err_code = ble_advertising_whitelist_reply(&m_advertising,
                                                           whitelist_addrs,
                                                           addr_cnt,
                                                           whitelist_irks,
                                                           irk_cnt);
                MKB_ERROR_CHECK(err_code);
            }
        }
    }
    break;

    case BLE_ADV_EVT_PEER_ADDR_REQUEST:
    {
        pm_peer_id_t peer_id = p_ch->config.info[ch_id].peer_id;

        MKB_LOG_INFO("BLE_ADV_EVT_PEER_ADDR_REQUEST: CH(%d), peer_id(%d)", ch_id, peer_id);
        ________DBG_20210930_ble_adv_event("BLE_ADV_EVT_PEER_ADDR_REQUEST: CH(%d), peer_id(%d)", ch_id, peer_id);

        // Only Give peer address if we have a handle to the bonded peer.
        if (peer_id != PM_PEER_ID_INVALID)
        {
            m_ble_adv_state.adv_active = true;

            pm_peer_data_bonding_t peer_bonding_data;

            err_code = pm_peer_data_bonding_load(peer_id, &peer_bonding_data);
            if (err_code != NRF_ERROR_NOT_FOUND)
            {
                MKB_ERROR_CHECK(err_code);

                if (m_ble_adv_state.mode == ADV_MODE_WHITELIST)
                {
                    // Manipulate identities to exclude peers with no Central Address Resolution.
                    mkb_ble_pm_identities_set(PM_PEER_ID_LIST_SKIP_ALL);
                }

                ble_gap_addr_t *p_peer_addr = &(peer_bonding_data.peer_ble_id.id_addr_info);
                err_code = ble_advertising_peer_addr_reply(&m_advertising, p_peer_addr);
                MKB_ERROR_CHECK(err_code);

                MKB_LOG_DEBUG("CH(%d): peer id: %d, addr: 0x%02x%02x%02x%02x%02x%02x", ch_id, peer_id,
                              p_peer_addr->addr[5], p_peer_addr->addr[4], p_peer_addr->addr[3],
                              p_peer_addr->addr[2], p_peer_addr->addr[1], p_peer_addr->addr[0]);
            }
        }
        else
        {
            err_code = ble_advertising_peer_addr_reply(&m_advertising, NULL);
            MKB_ERROR_CHECK(err_code);
        }
        break;
    }

    default:
        ________DBG_20210930_ble_adv_event("NO_MATCH_ADV_EVENT: %d", ble_adv_evt);
        break;
    }
}

/**@brief Function for handling advertising errors.
 *
 * @param[in] nrf_error  Error code containing information about what went wrong.
 */
static void ble_advertising_error_handler(uint32_t nrf_error)
{
    MKB_ERROR_HANDLER(nrf_error);
}

/**@brief Function for initializing the Advertising functionality.
 */
int8_t tx_power_level_table[] =
    {
        -40,
        -30,
        -20,
        -16,
        -12,
        -8,
        -4,
        0,
        4};

static void mkb_ble_adv_advertising_init(void)
{
    ret_code_t err_code;
    uint8_t adv_flags;
    int8_t tx_power_level = tx_power_level_table[MKB_BLE_RADIO_TX_POWER];
    ble_advertising_init_t init;

    memset(&init, 0, sizeof(init));

    adv_flags = BLE_GAP_ADV_FLAGS_LE_ONLY_LIMITED_DISC_MODE;
    init.advdata.name_type = BLE_ADVDATA_FULL_NAME;
    init.advdata.include_appearance = false;
    init.advdata.flags = adv_flags;
    init.advdata.p_tx_power_level = &tx_power_level;
    init.advdata.uuids_complete.uuid_cnt = sizeof(m_adv_uuids) / sizeof(m_adv_uuids[0]);
    init.advdata.uuids_complete.p_uuids = m_adv_uuids;

#if (defined(MKB_BLE_SWIFT_PAIR) && MKB_BLE_SWIFT_PAIR)
    init.advdata.p_manuf_specific_data = &m_sp_manuf_advdata;
    memcpy(&m_sp_advdata, &init.advdata, sizeof(m_sp_advdata));
#endif

    init.config.ble_adv_whitelist_enabled = true;
    init.config.ble_adv_directed_high_duty_enabled = true;
    init.config.ble_adv_directed_enabled = false;
    init.config.ble_adv_directed_interval = APP_ADV_DIRECT_INTERVAL;
    init.config.ble_adv_directed_timeout = APP_ADV_DIRECT_DURATION;
    init.config.ble_adv_fast_enabled = true;
    init.config.ble_adv_fast_interval = APP_ADV_FAST_INTERVAL;
    init.config.ble_adv_fast_timeout = APP_ADV_FAST_DURATION;
    init.config.ble_adv_slow_enabled = false;
    init.config.ble_adv_slow_interval = APP_ADV_SLOW_INTERVAL;
    init.config.ble_adv_slow_timeout = APP_ADV_SLOW_DURATION;

    init.evt_handler = mkb_ble_adv_on_adv_evt;
    init.error_handler = ble_advertising_error_handler;

    err_code = ble_advertising_init(&m_advertising, &init);
    MKB_ERROR_CHECK(err_code);

    err_code = sd_ble_gap_tx_power_set(BLE_GAP_TX_POWER_ROLE_ADV, m_advertising.adv_handle, tx_power_level);
    MKB_ERROR_CHECK(err_code);

    ble_advertising_conn_cfg_tag_set(&m_advertising, APP_BLE_CONN_CFG_TAG);
}

/**@brief mokibo ble initialize */
ret_code_t mkb_ble_adv_init(void)
{
    memset(&m_ble_adv_state, 0, sizeof(m_ble_adv_state));

    m_ble_adv_state.mode = ADV_MODE_WHITELIST;
    m_ble_adv_state.adv_active = false;

    m_ble_adv_wait_for_advertizing = ADV_MODE_WHITELIST;

    mkb_ble_adv_advertising_init();
}

/**@brief Function for starting advertising.
 */
ret_code_t mkb_ble_adv_start(CHANNEL_ID_t adv_ch, MKB_BLE_ADV_MODE_t adv_mode)
{
    ret_code_t err_code;
    m_ble_adv_state.adv_active = true;

    MKB_LOG_INFO("CH(%d): START Advertizing!", adv_ch);

    sd_ble_gap_adv_stop(m_advertising.adv_handle);

    if (adv_mode == ADV_MODE_WHITELIST)
    {
        mkb_ble_pm_whitelist_set(PM_PEER_ID_LIST_SKIP_NO_ID_ADDR);
    }

    switch (adv_ch)
    {
#if (defined(MKB_BLE_BT1_ENABLED) && MKB_BLE_BT1_ENABLED)
    case CHANNEL_ID_1:
        mkb_ble_device_name_set(MKB_DEVICE_NAME_BT1);
        break;
#endif
#if (defined(MKB_BLE_BT2_ENABLED) && MKB_BLE_BT2_ENABLED)
    case CHANNEL_ID_2:
        mkb_ble_device_name_set(MKB_DEVICE_NAME_BT2);
        break;
#endif
#if (defined(MKB_BLE_BT3_ENABLED) && MKB_BLE_BT3_ENABLED)
    case CHANNEL_ID_3:
        mkb_ble_device_name_set(MKB_DEVICE_NAME_BT3);
        break;
#endif
    default:
        return NRF_ERROR_INVALID_PARAM;
    }

    mkb_ble_pm_local_address_set(adv_ch);

    m_ble_adv_state.mode = adv_mode;

    if (adv_mode == ADV_MODE_WHITELIST || adv_mode == ADV_MODE_UNDIRECTED)
    {
        err_code = ble_advertising_start(&m_advertising, BLE_ADV_MODE_FAST);
    }
    else if (adv_mode == ADV_MODE_DIRECTED)
    {
        err_code = ble_advertising_start(&m_advertising, BLE_ADV_MODE_DIRECTED);
    }
    else
    {
        err_code = ble_advertising_restart_without_whitelist(&m_advertising);
    }
    mkb_event_send(EVT_LED_CH_ADV_FAST);
    MKB_ERROR_CHECK(err_code);

    return err_code;
}

ret_code_t mkb_ble_adv_stop(void)
{
    ret_code_t status = sd_ble_gap_adv_stop(m_advertising.adv_handle);
    if (status == NRF_SUCCESS)
    {
        m_ble_adv_state.adv_active = false;
    }

    return status;
}

bool mkb_ble_adv_running(void)
{
    return (m_ble_adv_state.adv_active);
}

void mkb_ble_adv_state_reset(void)
{
    m_ble_adv_state.adv_active = false;
}

void mkb_ble_adv_wait_for_advertizing_set(MKB_BLE_ADV_MODE_t adv_mode)
{
    m_ble_adv_wait_for_advertizing = adv_mode;
}

MKB_BLE_ADV_MODE_t mkb_ble_adv_wait_for_advertizing_get(void)
{
    return m_ble_adv_wait_for_advertizing;
}

ret_code_t mkb_ble_adv_cli(size_t size, char **params)
{
    ret_code_t err_code = NRF_SUCCESS;
    if (!strcmp(params[1], "help"))
    {
    }
    else
    {
        err_code = NRF_ERROR_INTERNAL;
    }
    return err_code;
}

#endif // (defined(MKB_BLE_ENABLED) && MKB_BLE_ENABLED)