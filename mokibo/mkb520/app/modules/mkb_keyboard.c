/*
 * ===============================================================================
 * Copyright (C) 2017-2019
 * INNOPRESSO INC. All Rights Reserved
 *
 * The content of this file is copyrighted by INNOPRESSO or its
 * affiliated, associated or related companies. All copyrights and other
 * associated intellectual property rights related thereto are expressly
 * reserved by INNOPRESSO INC.
 * ===============================================================================
 */

#include <stdbool.h>
#include <stdint.h>

#include "sdk_common.h"

#include "nrf_assert.h"
#include "nrf_error.h"
#include "nrf_log.h"
#include "nrf_delay.h"
#include "nrf_cli.h"
#include "nrf_pwr_mgmt.h"

#include "bsp.h"

#include "app_error.h"
#include "app_timer.h"
#include "app_scheduler.h"

#define MKB_LOG_LEVEL MKB_KEYBOARD_LOG_LEVEL
#include "mkb_common.h"

#if (defined(MKB_KEYBOARD_ENABLED) && MKB_KEYBOARD_ENABLED)
#include "mkb_event.h"

#if (defined(MKB_KEYBOARD_ENABLED) && MKB_KEYBOARD_ENABLED)
#include "mkb_keyboard.h"
#endif // (defined(MKB_KEYBOARD_ENABLED) && MKB_KEYBOARD_ENABLED)

#if (defined(MKB_KEYMATRIX_ENABLED) && MKB_KEYMATRIX_ENABLED)
#include "drv_keymatrix.h"
#endif // (defined(MKB_KEYMATRIX_ENABLED) && MKB_KEYMATRIX_ENABLED)

#if (defined(MKB_PIXART_ENABLED) && MKB_PIXART_ENABLED)
#include "drv_pixart.h"
#endif // (defined(MKB_PIXART_ENABLED) && MKB_PIXART_ENABLED)

// test
#include "mkb_ble.h"
#include "mkb_led.h"
#include "mkb_usb.h"
#include "mkb_dfu.h"
#include "mkb_wdt.h"
#include "mkb_db.h"

APP_TIMER_DEF(m_keyboard_timer);

APP_TIMER_DEF(m_keyboard_timer_id_MOKIBO);
APP_TIMER_DEF(m_keyboard_timer_id_ONOFF);
APP_TIMER_DEF(m_keyboard_timer_id_BT1);
APP_TIMER_DEF(m_keyboard_timer_id_BT2);
APP_TIMER_DEF(m_keyboard_timer_id_BT3);
APP_TIMER_DEF(m_keyboard_timer_id_USB);

APP_TIMER_DEF(m_send_help_timer);

static bool m_keyboard_caps_on = false; /**< Variable to indicate if Caps Lock is turned on. */

#if (KEYBOARD_TYPE == KEYBOARD_TYPE_MOKIBO)
static const uint8_t m_keyboard_lookup[KEYBOARD_NUM_OF_COLUMNS][KEYBOARD_NUM_OF_ROWS] =
    {
        /*         R0          R1          R2          R3          R4          R5          R6          R7 */
        /* C00 */ {S2L(NONE), S2L(NONE), S2L(UP), S2L(2), S2L(3), S2L(BS), S2L(F9), S2L(F10)},
        /* C01 */ {S2L(LEFT), S2L(DOWN), S2L(RIGHT), S2L(NONE), S2L(NONE), S2L(NONE), S2L(DELETE), S2L(NONE)},
        /* C02 */ {S2L(F7), S2L(1), S2L(F3), S2L(F4), S2L(Q), S2L(F8), S2L(F12), S2L(F11)},
        /* C03 */ {S2L(F6), S2L(CAPS), S2L(F2), S2L(F1), S2L(TAB), S2L(F5), S2L(TILDE), S2L(ESC)},
        /* C04 */ {S2L(NONE), S2L(NONE), S2L(DOT), S2L(SLASH), S2L(COLON), S2L(QUOTE), S2L(P), S2L(O_BRCKT)},
        /* C05 */ {S2L(L_CTRL), S2L(NONE), S2L(FUNC), S2L(NONE), S2L(NONE), S2L(NONE), S2L(NONE), S2L(NONE)},
        /* C06 */ {S2L(NONE), S2L(NONE), S2L(L_SHFT), S2L(R_SHFT), S2L(NONE), S2L(NONE), S2L(NONE), S2L(NONE)},
        /* C07 */ {S2L(R_ALT), S2L(NONE), S2L(NONE), S2L(NONE), S2L(ENTER), S2L(C_BRCKT), S2L(EQUAL), S2L(NONE)},
        /* C08 */ {S2L(M), S2L(COMMA), S2L(K), S2L(L), S2L(I), S2L(O), S2L(0), S2L(MINUS)},
        /* C09 */ {S2L(BSLASH), S2L(B), S2L(N), S2L(J), S2L(Y), S2L(U), S2L(8), S2L(9)},
        /* C10 */ {S2L(SB), S2L(C), S2L(V), S2L(G), S2L(H), S2L(T), S2L(6), S2L(7)},
        /* C11 */ {S2L(NONE), S2L(X), S2L(D), S2L(F), S2L(E), S2L(R), S2L(4), S2L(5)},
        /* C12 */ {S2L(L_GUI), S2L(R_CTRL), S2L(NONE), S2L(NONE), S2L(NONE), S2L(NONE), S2L(NONE), S2L(NONE)},
        /* C13 */ {S2L(L_ALT), S2L(NONE), S2L(NONE), S2L(NONE), S2L(NONE), S2L(NONE), S2L(NONE), S2L(NONE)},
        /* C14 */ {S2L(FUNC), S2L(NONE), S2L(Z), S2L(A), S2L(S), S2L(W), S2L(NONE), S2L(NONE)},
};
#elif (KEYBOARD_TYPE == KEYBOAED_TYPE_IMAGES)
static const uint8_t m_keyboard_lookup[KEYBOARD_NUM_OF_COLUMNS][KEYBOARD_NUM_OF_ROWS] =
    {
        /*         R0          R1          R2          R3          R4          R5          R6          R7 */
        /* C00 */ {S2L(NONE), S2L(NONE), S2L(L_CTRL), S2L(NONE), S2L(R_CTRL), S2L(NONE), S2L(NONE), S2L(NONE)},
        /* C01 */ {S2L(CAPS), S2L(ESC), S2L(TAB), S2L(TILDE), S2L(A), S2L(Z), S2L(1), S2L(Q)},
        /* C02 */ {S2L(F1), S2L(F4), S2L(F3), S2L(F2), S2L(D), S2L(C), S2L(3), S2L(E)},
        /* C03 */ {S2L(NONE), S2L(L_UI), S2L(NONE), S2L(NONE), S2L(NONE), S2L(NONE), S2L(NONE), S2L(NONE)},
        /* C04 */ {S2L(B), S2L(G), S2L(T), S2L(5), S2L(F), S2L(V), S2L(4), S2L(R)},
        /* C05 */ {S2L(F8), S2L(F7), S2L(F6), S2L(F5), S2L(S), S2L(X), S2L(2), S2L(W)},
        /* C06 */ {S2L(NONE), S2L(NONE), S2L(C_BRCKT), S2L(NONE), S2L(K), S2L(COMMA), S2L(8), S2L(I)},
        /* C07 */ {S2L(NONE), S2L(NONE), S2L(NONE), S2L(FUNC), S2L(NONE), S2L(NONE), S2L(NONE), S2L(NONE)},
        /* C08 */ {S2L(N), S2L(H), S2L(Y), S2L(6), S2L(J), S2L(M), S2L(7), S2L(U)},
        /* C09 */ {S2L(NONE), S2L(NONE), S2L(NONE), S2L(NONE), S2L(NONE), S2L(L_SHFT), S2L(NONE), S2L(R_SHFT)},
        /* C10 */ {S2L(EQUAL), S2L(QUOTE), S2L(O_BRCKT), S2L(MINUS), S2L(COLON), S2L(SLASH), S2L(0), S2L(P)},

        /* C11 */ {S2L(NONE), S2L(X), S2L(D), S2L(F), S2L(E), S2L(R), S2L(4), S2L(5)},
        /* C12 */ {S2L(L_GUI), S2L(R_CTRL), S2L(NONE), S2L(NONE), S2L(NONE), S2L(NONE), S2L(NONE), S2L(NONE)},
        /* C13 */ {S2L(L_ALT), S2L(NONE), S2L(NONE), S2L(NONE), S2L(NONE), S2L(NONE), S2L(NONE), S2L(NONE)},
        /* C14 */ {S2L(FUNC), S2L(NONE), S2L(Z), S2L(A), S2L(S), S2L(W), S2L(NONE), S2L(NONE)},
};
#else
#error "You have to define MKB_KEYBOARD_TYPE in mkb_config.h"
#endif

/*
핫키 추가하는법
1. mkb_keyboard.h 에 HID_KEY_CENTRAL_blabla 추가
2. mkb_keyboard.h 에 MKB_KEYBOARD_HOTKEY_blabla 추가
3. m_fn_keys_table 에 실제 키 , 1 에서 추가한 키 추가
4. m_hotkey_table 에
{S2L(BT1),
EDGE(R), HOTK(BT1),
EDGE(PL), HOTK(BT1_ADV), M_TMR(BT1), M_TIMEOUT(BT), false, false},
*/
static const fn_key_t m_fn_keys_table[] =
    {
        // 참조:
        //		S2L(KKK)는  HID_KEY_KKK 와 같다.
        //
        // function key detection table
        //		(key)					(Fn+key)
        //{	S2L(M),                     S2L(MOKIBO)},
        {S2L(ESC), S2L(HOUSE)},
        {S2L(F9), S2L(BT1)},
        {S2L(F10), S2L(BT2)},

#if defined(BOARD_PCA10056) || defined(BOARD_MKB10056)
#if (defined(MKB_USB_ENABLED) && MKB_USB_ENABLED)
        {S2L(F11), S2L(BT3)},
#else
        {S2L(0), S2L(USB)},
#endif
#else
        {S2L(F11), S2L(BT3)},
#endif

        {S2L(F1), S2L(TOGGLE_MIC)},
        {S2L(F2), S2L(TOGGLE_CAM)},
        {S2L(F3), S2L(V_MUTE)},
        {S2L(F4), S2L(V_DOWN)},
        {S2L(F5), S2L(V_UP)},

        {S2L(F6), S2L(VKBD)},
        {S2L(F7), S2L(CAPTURE)},
        {S2L(F8), S2L(BTRCHK)},

        {S2L(F12), S2L(CENTRAL_IOS)},
        {S2L(DELETE), S2L(INSERT)},

        {S2L(UP), S2L(PAGEUP)},
        {S2L(DOWN), S2L(PAGEDOWN)},
        {S2L(LEFT), S2L(HOME)},
        {S2L(RIGHT), S2L(END)},

        {S2L(DELETE), S2L(ONOFF)},

        {S2L(TILDE), S2L(FIND)},
        {S2L(TAB), S2L(SWTAPP)},
        {S2L(BS), S2L(BACKWARD)},
        {S2L(BSLASH), S2L(VKBD)},
        {S2L(DELETE), S2L(LOCKSCREEN)},
        {S2L(CAPS), S2L(TOUCHLOCK)},

        {S2L(Z), S2L(LANGUAGE_Z)},
        {S2L(X), S2L(LANGUAGE_X)},
        {S2L(C), S2L(LANGUAGE_C)},
        {S2L(INTERNATIONAL4), S2L(INTERNATIONAL5)}};

// hot key for return
static hot_key_t m_hotkey_table[] =
    {
        // 참조:
        //		S2L(KKK)는  APP_USBD_HID_KBD_KKK 와 같다.
        //		HOTK(JJJ)는 DRV_KEYBOARD_HOTKEY_JJJ 와 같다.
        //	  EDGE(TTT)는 TTT_EDGE와 같다.
        //		M_TMR(MMM)은 M_TIMER_ID_MMM과 같다.
        //			#define P_EDGE		0x01				// push edge(release --> push)
        //			#define R_EDGE		0x02				// release edge(push --> release)
        //			#define LONG_KEY	0x04				// LONG PRESS KEY
        //			#define A_EDGE		(P_EDGE|R_EDGE)		// any edge
        //			#define RL_EDGE		(R_EDGE|LONG_KEY)	// release + long
        //			#define L_EDGE		(LONG_KEY)			// long
        //
        //  key,	  				attr, 		hot_key value,			long attr,      long press hot_key,     timer id            duration
        //
        {S2L(MOKIBO), EDGE(N), HOTK(NONE), EDGE(PL), HOTK(MOKIBO), M_TMR(MOKIBO), M_TIMEOUT(MOKIBO), false, false},
        {S2L(ONOFF), EDGE(R), HOTK(DELETE), EDGE(RL), HOTK(ONOFF), M_TMR(ONOFF), M_TIMEOUT(ONOFF), false, false},
        {S2L(INSERT), EDGE(A), HOTK(INSERT), EDGE(N), HOTK(NONE), NULL, IMMEDIATELY, false, false},
        {S2L(HOUSE), EDGE(A), HOTK(HOUSE), EDGE(N), HOTK(NONE), NULL, IMMEDIATELY, false, false},
        {S2L(HOME), EDGE(A), HOTK(HOME), EDGE(N), HOTK(NONE), NULL, IMMEDIATELY, false, false},
        {S2L(END), EDGE(A), HOTK(END), EDGE(N), HOTK(NONE), NULL, IMMEDIATELY, false, false},
        {S2L(PAGEUP), EDGE(A), HOTK(PAGEUP), EDGE(N), HOTK(NONE), NULL, IMMEDIATELY, false, false},
        {S2L(PAGEDOWN), EDGE(A), HOTK(PAGEDOWN), EDGE(N), HOTK(NONE), NULL, IMMEDIATELY, false, false},
        {S2L(BT1), EDGE(R), HOTK(BT1), EDGE(PL), HOTK(BT1_ADV), M_TMR(BT1), M_TIMEOUT(BT), false, false},
        {S2L(BT2), EDGE(R), HOTK(BT2), EDGE(PL), HOTK(BT2_ADV), M_TMR(BT2), M_TIMEOUT(BT), false, false},
#if defined(BOARD_PCA10056) || defined(BOARD_MKB10056)
#if (defined(MKB_USB_ENABLED) && MKB_USB_ENABLED)
        {S2L(BT3), EDGE(R), HOTK(BT3), EDGE(PL), HOTK(BT3_ADV), M_TMR(BT3), M_TIMEOUT(BT), false, false},
        {S2L(USB), EDGE(R), HOTK(USB), EDGE(PL), HOTK(USB_NEW), M_TMR(BT3), M_TIMEOUT(BT), false, false},
#else
        {S2L(BT3), EDGE(R), HOTK(BT3), EDGE(PL), HOTK(BT3_ADV), M_TMR(BT3), M_TIMEOUT(BT), false, false},
#endif
#else
        {S2L(BT3), EDGE(R), HOTK(BT3), EDGE(PL), HOTK(BT3_ADV), M_TMR(BT3), M_TIMEOUT(BT), false, false},
#endif
        {S2L(BTRCHK), EDGE(P), HOTK(BTRCHK), EDGE(N), HOTK(NONE), NULL, IMMEDIATELY, false, false},
        {S2L(SWTAPP), EDGE(A), HOTK(SWTAPP), EDGE(N), HOTK(NONE), NULL, IMMEDIATELY, false, false},
        {S2L(VKBD), EDGE(A), HOTK(VKBD), EDGE(N), HOTK(NONE), NULL, IMMEDIATELY, false, false},
        {S2L(FIND), EDGE(A), HOTK(FIND), EDGE(N), HOTK(NONE), NULL, IMMEDIATELY, false, false},
        {S2L(V_MUTE), EDGE(A), HOTK(V_MUTE), EDGE(N), HOTK(NONE), NULL, IMMEDIATELY, false, false},
        {S2L(V_UP), EDGE(A), HOTK(V_UP), EDGE(N), HOTK(NONE), NULL, IMMEDIATELY, false, false},
        {S2L(V_DOWN), EDGE(A), HOTK(V_DOWN), EDGE(N), HOTK(NONE), NULL, IMMEDIATELY, false, false},
        {S2L(CAPTURE), EDGE(A), HOTK(CAPTURE), EDGE(N), HOTK(NONE), NULL, IMMEDIATELY, false, false},
        {S2L(CENTRAL_IOS), EDGE(P), HOTK(CENTRAL_IOS), EDGE(N), HOTK(NONE), NULL, IMMEDIATELY, false, false},
        {S2L(BACKWARD), EDGE(A), HOTK(BACKWARD), EDGE(N), HOTK(NONE), NULL, IMMEDIATELY, false, false},
        {S2L(LOCKSCREEN), EDGE(P), HOTK(LOCKSCREEN), EDGE(N), HOTK(NONE), NULL, IMMEDIATELY, false, false},
        {S2L(TOUCHLOCK), EDGE(P), HOTK(TOUCHLOCK), EDGE(N), HOTK(NONE), NULL, IMMEDIATELY, false, false},
        {S2L(LANGUAGE_Z), EDGE(P), HOTK(LANGUAGE_Z), EDGE(N), HOTK(NONE), NULL, IMMEDIATELY, false, false},
        {S2L(LANGUAGE_X), EDGE(P), HOTK(LANGUAGE_X), EDGE(N), HOTK(NONE), NULL, IMMEDIATELY, false, false},
        {S2L(LANGUAGE_C), EDGE(P), HOTK(LANGUAGE_C), EDGE(N), HOTK(NONE), NULL, IMMEDIATELY, false, false},
        {S2L(TOGGLE_MIC), EDGE(P), HOTK(TOGGLE_MIC), EDGE(N), HOTK(NONE), NULL, IMMEDIATELY, false, false},
        {S2L(TOGGLE_CAM), EDGE(P), HOTK(TOGGLE_CAM), EDGE(N), HOTK(NONE), NULL, IMMEDIATELY, false, false},
};

static char *m_hid_key_str[] =
    {
        [0] = "NONE",

        [4] = "A",
        [5] = "B",
        [6] = "C",
        [7] = "D",
        [8] = "E",
        [9] = "F",
        [10] = "G",
        [11] = "H",
        [12] = "I",
        [13] = "J",
        [14] = "K",
        [15] = "L",
        [16] = "M",
        [17] = "N",
        [18] = "O",
        [19] = "P",
        [20] = "Q",
        [21] = "R",
        [22] = "S",
        [23] = "T",
        [24] = "U",
        [25] = "V",
        [26] = "W",
        [27] = "X",
        [28] = "Y",
        [29] = "Z",
        [30] = "1",
        [31] = "2",
        [32] = "3",
        [33] = "4",
        [34] = "5",
        [35] = "6",
        [36] = "7",
        [37] = "8",
        [38] = "9",
        [39] = "0",
        [40] = "ENTER",
        [41] = "ESC",
        [42] = "BACK SPACE",
        [43] = "TAB",
        [44] = "SPACE BAR",
        [45] = "MINUS",
        [46] = "EQUAL",
        [47] = "[",
        [48] = "]",
        [49] = "BACK SLASH",
        [50] = "HASHTILDE",
        [51] = "SEMI COLON",
        [52] = "APOSTROPHE",
        [53] = "GRAVE",
        [54] = "COMMA",
        [55] = "DOT",
        [56] = "SLASH",
        [57] = "Caps Lock",
        [58] = "F1",
        [59] = "F2",
        [60] = "F3",
        [61] = "F4",
        [62] = "F5",
        [63] = "F6",
        [64] = "F7",
        [65] = "F8",
        [66] = "F9",
        [67] = "F10",
        [68] = "F11",
        [69] = "F12",
        [70] = "Print Screen",
        [71] = "Scroll Lock",
        [72] = "Pause",
        [73] = "Insert",
        [74] = "Home",
        [75] = "Page Up",
        [76] = "Delete Forward",
        [77] = "End",
        [78] = "Page Down",
        [79] = "Right Arrow",
        [80] = "Left Arrow",
        [81] = "Down Arrow",
        [82] = "Up Arrow",
        [83] = "Num Lock",
        [84] = "Keypad /",
        [85] = "Keypad *",
        [86] = "Keypad -",
        [87] = "Keypad +",
        [88] = "Keypad ENTER",
        [89] = "Keypad 1",
        [90] = "Keypad 2",
        [91] = "Keypad 3",
        [92] = "Keypad 4",
        [93] = "Keypad 5",
        [94] = "Keypad 6",
        [95] = "Keypad 7",
        [96] = "Keypad 8",
        [97] = "Keypad 9",
        [98] = "Keypad 0",
        [99] = "Keypad .",

        [160] = "MOKIBO",
        [165] = "House",
        [166] = "BT1",
        [167] = "BT2",
        [168] = "BT3",
        [169] = "DONGLE",
        [170] = "Battery Check",
        [171] = "Switching App",
        [172] = "Virtual Keyboard",
        [173] = "Find",
        [174] = "Volume Up",
        [175] = "Volume Down",
        [176] = "Mokibo On/Off",
        [177] = "RESET",
        [178] = "Erase Bond",

        [183] = "Capture",
        [185] = "Central Windows",
        [186] = "Central Android",
        [187] = "Central MAC",
        [188] = "Backward",
        [189] = "Touch Lock",
        [190] = "Lock creen",
        [191] = "Volume Mute",
        [192] = "USB",

        [224] = "Left Control",
        [225] = "Left Shift",
        [226] = "Left Alt",
        [227] = "Left GUI",
        [228] = "Right Control",
        [229] = "Right Shift",
        [230] = "Right Alt",
        [231] = "Right GUI",

        [255] = "Function Key"};

extern void mkb_keys_send(uint8_t *keys, uint8_t len);

void send_ascii_string(uint8_t delay, char *string)
{
    //+ is shift
    //^ is control
    //# is gui
    //! is alt key
}

char *hid_to_char(uint8_t *keys, uint8_t len)
{
    static char print_buf[256];
    memset(print_buf, 0, print_buf[0] * 256);
    for (int i = 0; i < len; i++)
    {
        char char_key;
        if (keys[i] == HID_KEY_0)
        {
            char_key = '0';
        }

        print_buf[i] = char_key;
    }
}

bool arr_cmp(uint8_t *arr_1, uint8_t *arr_2, uint8_t len)
{

    for (int i = 0; i < len; i++)
    {
        if (arr_1[i] != arr_2[i])
        {
            return false;
        }
    }
    return true;
}

uint8_t ascii_to_hid(char input_letter)
{
    uint8_t hid_key = 0;

    if (input_letter == '0')
    {
        hid_key = HID_KEY_0;
    }
    else if ('1' <= input_letter && input_letter <= '9') // numbers
    {
        hid_key = input_letter - '1' + HID_KEY_1;
    }
    else if ('a' <= input_letter && input_letter <= 'z') // alphabet small
    {
        hid_key = input_letter - 'a' + HID_KEY_A;
    }
    else if ('A' <= input_letter && input_letter <= 'Z')
    {
        hid_key = input_letter - 'A' + HID_KEY_A;
    }
    else if (input_letter == '.') //.
    {
        hid_key = HID_KEY_DOT;
    }
    else if (input_letter == '-') //.
    {
        hid_key = HID_KEY_MINUS;
    }
    else if (input_letter == '\n')
    {
        hid_key = HID_KEY_ENTER;
    }
    else if (input_letter == ' ')
    {
        hid_key = HID_KEY_SB;
    }
    else if (input_letter == '[')
    {
        hid_key = HID_KEY_O_BRCKT;
    }
    else if (input_letter == ']')
    {
        hid_key = HID_KEY_C_BRCKT;
    }
    else if (input_letter == '/')
    {
        hid_key = HID_KEY_SLASH;
    }
    else if (input_letter == ';' || input_letter == ':')
    {
        hid_key = HID_KEY_COLON;
    }
    else
    {
        hid_key = HID_KEY_X;
    }
    return hid_key;
}

bool auto_send_hid_keys(uint8_t *hid_keys, uint8_t length)
{
    uint8_t delay = 10;
    for (uint8_t i = 0; i < length; i++)
    {
        mkb_event_send_ms(delay * i, EVT_KEY_DOWN, ascii_to_hid(hid_keys[i]));
        mkb_event_send_ms(delay * i + 5, EVT_KEY_UP, ascii_to_hid(hid_keys[i]));
    }
    return true;
}

char *asciis_to_hids(uint8_t *keys, uint8_t len)
{
    static char print_buf[256];
    memset(print_buf, 0, print_buf[0] * 256);

    for (int i = 0; i < len; i++)
    {
        if (keys[i] != NULL)
        {
            uint8_t hid_key;
            if (keys[i] == '0')
            {
                hid_key = HID_KEY_0;
            }
            else if ('1' <= keys[i] && keys[i] <= '9') // numbers
            {
                hid_key = keys[i] - '1' + HID_KEY_1;
            }
            else if ('a' <= keys[i] && keys[i] <= 'z') // alphabet small
            {
                hid_key = keys[i] - 'a' + HID_KEY_A;
            }
            else if ('A' <= keys[i] && keys[i] <= 'Z')
            {
                hid_key = keys[i] - 'A' + HID_KEY_A;
            }
            else if (keys[i] == '.') //.
            {
                hid_key = HID_KEY_DOT;
            }
            else
            {
                hid_key = HID_KEY_X;
            }
            print_buf[i] = hid_key;
        }
    }
    ________DBG_20211229_string_hotkey("printbuf: %d,%d,%d", print_buf[0], print_buf[1], print_buf[2]);
    return print_buf;
}

void send_battery_string()
{
    char print_buf[10];

    ________DBG_20211205_battery_level("battery level : %d", mkb_db_battery_level_get());

    print_buf[0] = mkb_db_battery_level_get() / 100 + '0';
    print_buf[1] = mkb_db_battery_level_get() % 100 / 10 + '0';
    print_buf[2] = mkb_db_battery_level_get() % 10 + '0';
    print_buf[3] = 'p';
    print_buf[4] = 'e';
    print_buf[5] = 'r';
    print_buf[6] = 'c';
    print_buf[7] = 'e';
    print_buf[8] = 'n';
    print_buf[9] = 't';

    if (mkb_db_battery_level_get() / 100 == 0)
    {
        print_buf[0] = ' ';
        if (mkb_db_battery_level_get() % 100 / 10 == 0)
        {
            print_buf[1] = ' ';
        }
    }

    ________DBG_20210831_mkb_coms_aamode("%s", print_buf);
    for (int i = 0; i < 10; i++)
    {
        ________DBG_20210831_mkb_coms_aamode("%d", print_buf[i]);
        if (print_buf[i] != NULL)
        {
            uint8_t hid_key;
            if (print_buf[i] == '0')
            {
                hid_key = HID_KEY_0;
            }
            else if ('1' <= print_buf[i] && print_buf[i] <= '9') // numbers
            {
                hid_key = print_buf[i] - '1' + HID_KEY_1;
            }
            else if ('a' <= print_buf[i] && print_buf[i] <= 'z') // alphabet small
            {
                hid_key = print_buf[i] - 'a' + HID_KEY_A;
            }
            else if ('A' <= print_buf[i] && print_buf[i] <= 'Z')
            {
                hid_key = print_buf[i] - 'A' + HID_KEY_A;
            }
            else if (print_buf[i] == '.') //.
            {
                hid_key = HID_KEY_DOT;
            }
            if (print_buf[i] == ' ')
            {
                continue;
            }
            mkb_event_send_ms(i * 16, EVT_KEY_DOWN, hid_key);
            mkb_event_send_ms(i * 16 + 8, EVT_KEY_UP, hid_key);
        }
        else
        {
            return;
        }
    }
}

void send_version_string()
{
    char print_buf[20];
    sprintf(print_buf, "%s", MKB_FW_VERSION);
    ________DBG_20210831_mkb_coms_aamode("%s", print_buf);
    for (int i = 0; i < 20; i++)
    {
        ________DBG_20210831_mkb_coms_aamode("%d", print_buf[i]);
        if (print_buf[i] != NULL)
        {
            uint8_t hid_key;
            if (print_buf[i] == '0')
            {
                hid_key = HID_KEY_0;
            }
            else if ('1' <= print_buf[i] && print_buf[i] <= '9') // numbers
            {
                hid_key = print_buf[i] - '1' + HID_KEY_1;
            }
            else if ('a' <= print_buf[i] && print_buf[i] <= 'z') // alphabet small
            {
                hid_key = print_buf[i] - 'a' + HID_KEY_A;
            }
            else if ('A' <= print_buf[i] && print_buf[i] <= 'Z')
            {
                hid_key = print_buf[i] - 'A' + HID_KEY_A;
            }
            else if (print_buf[i] == '.') //.
            {
                hid_key = HID_KEY_DOT;
            }
            mkb_event_send_ms(i * 20, EVT_KEY_DOWN, hid_key);
            mkb_event_send_ms(i * 20 + 10, EVT_KEY_UP, hid_key);
        }
        else
        {
            return;
        }
    }
}

static void keyboard_keyid_decode(uint8_t keyid)
{
    uint8_t row, col, key;
    char *p_str;

    row = (keyid & 0xF0) >> 4;
    col = (keyid & 0x0F);
    key = m_keyboard_lookup[col][row];
    p_str = (m_hid_key_str[key] != NULL) ? m_hid_key_str[key] : m_hid_key_str[0];

    MKB_LOG_DEBUG("KEY: 0x%02x, row:%d, col:%d, hid key: 0x%02x(%s)", keyid, row, col, key, p_str);
}

static char *keyboard_keyid_str(uint8_t keyid)
{
    char *p_str;

#if (defined(MKB_KEYMATRIX_ENABLED) && MKB_KEYMATRIX_ENABLED)
    uint8_t row, col, key;

    row = (keyid & 0xF0) >> 4;
    col = (keyid & 0x0F);
    key = m_keyboard_lookup[col][row];
    p_str = (m_hid_key_str[key] != NULL) ? m_hid_key_str[key] : m_hid_key_str[0];
#else
#if (defined(MKB_PIXART_ENABLED) && MKB_PIXART_ENABLED)
    p_str = (m_hid_key_str[keyid] != NULL) ? m_hid_key_str[keyid] : m_hid_key_str[0];
#endif
#endif // (defined(MKB_KEYMATRIX_ENABLED) && MKB_KEYMATRIX_ENABLED)

    return p_str;
}

static char *keyboard_hidkey_str(uint8_t key)
{
    char *p_str;

    p_str = (m_hid_key_str[key] != NULL) ? m_hid_key_str[key] : m_hid_key_str[0];

    return p_str;
}

static uint8_t keyboard_keyid_to_hidkey(uint8_t keyid)
{
    uint8_t row, col, key;

    row = (keyid & 0xF0) >> 4;
    col = (keyid & 0x0F);
    key = m_keyboard_lookup[col][row];

    return key;
}

static uint8_t keyboard_fn_key_remap(uint8_t hidkey)
{
    uint8_t key;

    // Re-mapping
    uint8_t table_size = sizeof(m_fn_keys_table) / sizeof(fn_key_t);

    for (uint8_t i = 0; i < table_size; i++)
    {
        // TODO, Funtion key select setting
        if (hidkey == m_fn_keys_table[i].org)
        {
            key = m_fn_keys_table[i].new;

            return key;
        }
    }

    return 0;
}

static int8_t keyboard_hotkey_table_search(uint8_t hidkey)
{
    uint8_t limit = sizeof(m_hotkey_table) / sizeof(hot_key_t);

    for (uint8_t i = 0; i < limit; i++)
    {
        if (hidkey == m_hotkey_table[i].key)
        {
            return i;
        }
    }

    return -1;
}

static hot_key_t *keyboard_hotkey_table_get(uint8_t key)
{
    for (size_t i = 0; i < ARRAY_SIZE(m_hotkey_table); ++i)
    {
        if (m_hotkey_table[i].key == key)
        {
            return &m_hotkey_table[i];
        }
    }

    return NULL;
}

static void keyboard_hold_timer_timeout_handler(void *p_context)
{
    uint32_t key;
    uint32_t timestamp;
    hot_key_t *p_hotkey_table;

    key = (uint32_t)p_context;
    p_hotkey_table = keyboard_hotkey_table_get(key);

    // MKB_LOG_INFO("TIMEOUT: 0x%x", key);

    // Check if the key is still pressed
    if (p_hotkey_table != NULL)
    {
        p_hotkey_table->timer_act = false;
        timestamp = app_timer_cnt_get();

#if 0
        if ((p_hotkey_table->attr & L_EDGE) == L_EDGE) 
        {
            MKB_LOG_DEBUG("ATTR:0x%x, RL: 0x%x", p_hotkey_table->attr, RL_EDGE);
            p_hotkey_table->event_act = true;
            MKB_LOG_INFO("FUNC KEY EVENT Set: 0x%02x(%s)", key, keyboard_hidkey_str(key));
            mkb_event_send(EVT_FUNC_DOWN, p_hotkey_table->long_hotkey, timestamp);
        }
#else
        p_hotkey_table->event_act = true;

        if ((p_hotkey_table->long_attr & RL_EDGE) == RL_EDGE)
        {
            MKB_LOG_DEBUG("ATTR:0x%x, RL: 0x%x", p_hotkey_table->attr, RL_EDGE);
            MKB_LOG_INFO("FUNC KEY EVENT Set: 0x%02x(%s)", key, keyboard_hidkey_str(key));
        }

        if ((p_hotkey_table->long_attr & PL_EDGE) == PL_EDGE)
        {
            MKB_LOG_DEBUG("ATTR:0x%x, PL: 0x%x", p_hotkey_table->attr, PL_EDGE);
            MKB_LOG_INFO("FUNC LONG KEY EVENT: 0x%02x(%s)", key, keyboard_hidkey_str(key));
            mkb_event_send(EVT_FUNC_HOLD, p_hotkey_table->long_hotkey, timestamp);
        }
#endif
    }
}

static ret_code_t keyboard_hold_timer_init(void)
{
    ret_code_t status;

    for (size_t i = 0; i < ARRAY_SIZE(m_hotkey_table); ++i)
    {
        app_timer_id_t tid = m_hotkey_table[i].timer_id;

        status = app_timer_create(&tid,
                                  APP_TIMER_MODE_SINGLE_SHOT,
                                  keyboard_hold_timer_timeout_handler);
        if (status != NRF_SUCCESS)
        {
            return status;
        }
    }

    return NRF_SUCCESS;
}

static void keyboard_hold_timer_start(uint8_t key)
{
    ret_code_t status;
    uint32_t context;

    for (uint8_t i = 0; i < ARRAY_SIZE(m_hotkey_table); i++)
    {
        if ((m_hotkey_table[i].key == key) &&
            (m_hotkey_table[i].timer_id != NULL) &&
            (m_hotkey_table[i].timer_act == false))
        {
            context = (uint32_t)m_hotkey_table[i].key;
            status = app_timer_start(m_hotkey_table[i].timer_id,
                                     APP_TIMER_TICKS(m_hotkey_table[i].duration),
                                     (void *)context);
            if (status != NRF_SUCCESS)
            {
                MKB_ERROR_CHECK(status);
            }

            m_hotkey_table[i].timer_act = true;

            MKB_LOG_INFO("Timer Start: 0x%x", m_hotkey_table[i].key);
        }
    }
}

static void keyboard_hold_timer_stop(uint8_t key)
{
    ret_code_t status;

    for (size_t i = 0; i < ARRAY_SIZE(m_hotkey_table); ++i)
    {
        if ((m_hotkey_table[i].key == key) &&
            (m_hotkey_table[i].timer_id != NULL) &&
            (m_hotkey_table[i].timer_act == true))
        {
            m_hotkey_table[i].timer_act = false;

            status = app_timer_stop(m_hotkey_table[i].timer_id);
            if (status != NRF_SUCCESS)
            {
                MKB_ERROR_CHECK(status);
            }

            MKB_LOG_INFO("Timer Stop: 0x%x", m_hotkey_table[i].key);
        }
    }
}

static void keyboard_hold_timers_stop(void)
{
    ret_code_t status;

    for (size_t i = 0; i < ARRAY_SIZE(m_hotkey_table); ++i)
    {
        if ((m_hotkey_table[i].timer_id != NULL) && (m_hotkey_table[i].timer_act == true))
        {
            m_hotkey_table[i].timer_act = false;

            status = app_timer_stop(m_hotkey_table[i].timer_id);
            if (status != NRF_SUCCESS)
            {
                MKB_ERROR_CHECK(status);
            }

            MKB_LOG_INFO("Timer Stop: 0x%x", m_hotkey_table[i].key);
        }
    }
}

char help_strings[200] = "[OS color]\niOS - White\nAndroid - Green\nDeX - Orange\nWindows - Blue\nmac OS - Sky blue\n[User manual]\nhttps://www.mokibo.com/support";
void send_help_timer_handler(void *p_context)
{
    static uint8_t char_numb = 0;
    if (help_strings[char_numb] != NULL)
    {
        if (('A' <= help_strings[char_numb] && help_strings[char_numb] <= 'Z') || help_strings[char_numb] == ':')
        {
            mkb_event_send_ms(0, EVT_KEY_DOWN, HID_KEY_L_SHFT);
            mkb_event_send_ms(4, EVT_KEY_DOWN, ascii_to_hid(help_strings[char_numb]));
            mkb_event_send_ms(8, EVT_KEY_UP, ascii_to_hid(help_strings[char_numb]));
            mkb_event_send_ms(12, EVT_KEY_UP, HID_KEY_L_SHFT);
        }
        else
        {
            mkb_event_send_ms(0, EVT_KEY_DOWN, ascii_to_hid(help_strings[char_numb]));
            mkb_event_send_ms(12, EVT_KEY_UP, ascii_to_hid(help_strings[char_numb]));
        }
        char_numb++;
    }
    else
    {
        char_numb = 0;
        app_timer_stop(m_send_help_timer);
    }
}

static uint8_t m_remap_key = 0;
bool m_func = false;
static bool click_down = false;
#define string_hotkey_max 10

void mkb_keyboard_event_generate(mkb_event_type_t event_type, uint8_t keyid, uint32_t timestamp)
{
    uint8_t hid_key;
    static uint8_t hid_key_down_old;
    static uint32_t hid_key_down_time_old;

    static char string_hotkey[string_hotkey_max];
    static uint8_t string_hotkey_pos = 0;

    hot_key_t *p_hotkey_table;

#if (defined(MKB_KEYMATRIX_ENABLED) && MKB_KEYMATRIX_ENABLED)
    hid_key = keyboard_keyid_to_hidkey(keyid);
#else
#if (defined(MKB_PIXART_ENABLED) && MKB_PIXART_ENABLED)
    hid_key = keyid;
#endif
#endif

    if (event_type == EVT_KEY_DOWN)
    {
        if (hid_key == HID_KEY_MKB)
        {
            hid_key = HID_KEY_SB;
        }
        if (hid_key == HID_KEY_RED && click_down == false)
        {
            ________DBG_20211201_idle_1_tap_flag("red click down");
            mkb_db_is_red_click_down_set(true);

            if (mkb_db_is_func_pressed_get() == true)
            {
                mkb_event_send(EVT_BUTTON_CLICK_RIGHT, true);
            }
            else
            {
                mkb_event_send(EVT_BUTTON_CLICK_LEFT, true);
            }

            click_down = true;
        }

        { //너무 빠른 연속된 같은 DOWN 키 입력은 무시하도록함.
            if (hid_key_down_old == hid_key && app_timer_cnt_diff_compute(app_timer_cnt_get(), hid_key_down_time_old) < APP_TIMER_TICKS(90))
            {
                return;
            }
            else
            {
                hid_key_down_old = hid_key;
                hid_key_down_time_old = app_timer_cnt_get();
            }
        }
    }
    else if (event_type == EVT_KEY_UP)
    {
        if (hid_key == HID_KEY_MKB)
        {
            hid_key = HID_KEY_SB;
        }
        else if (hid_key == HID_KEY_RED)
        {
            ________DBG_20211201_idle_1_tap_flag("red click up");

            mkb_db_is_red_click_down_set(false);
            if (mkb_db_is_tap_click_down_get() == false)
            {
                mkb_event_send(EVT_BUTTON_CLICK_LEFT, false);
            }
            mkb_event_send(EVT_BUTTON_CLICK_RIGHT, false);

            click_down = false;
        }
    }

    if (hid_key == HID_KEY_UP ||
        hid_key == HID_KEY_DOWN ||
        hid_key == HID_KEY_MKB ||
        hid_key == HID_KEY_SB ||
        hid_key == HID_KEY_RED ||
        hid_key == HID_KEY_L_SHFT ||
        hid_key == HID_KEY_L_CTRL ||
        hid_key == HID_KEY_L_ALT ||
        hid_key == HID_KEY_L_GUI ||
        hid_key == HID_KEY_R_SHFT ||
        hid_key == HID_KEY_FUNC)
    {
        mkb_db_last_passkey_updown_time_set(app_timer_cnt_get());
    }
    else
    {
        mkb_db_last_key_up_time_set(app_timer_cnt_get());
        aamode_clear();
    }
    /*
    if(mkb_db_system_state_is_touch())
    {
        if(hid_key == HID_KEY_SB)
            return;
        if(hid_key >= HID_KEY_A && hid_key <= HID_KEY_0)
            return;
        if(hid_key == HID_KEY_MINUS || hid_key == HID_KEY_EQUAL)
            return;
        if(hid_key == HID_KEY_O_BRCKT || hid_key == HID_KEY_C_BRCKT)
            return;
        if(hid_key == HID_KEY_COLON || hid_key == HID_KEY_QUOTE)
            return;
        if(hid_key == HID_KEY_COMMA || hid_key == HID_KEY_DOT || hid_key == HID_KEY_SLASH)
            return;
    }
    */

    // if(hid_key == HID_KEY_FUNC)
    if (hid_key == KEYBOARD_FUNC_KEY) // fnc
    {
        if (event_type == EVT_KEY_DOWN)
        {
            MKB_LOG_INFO("FUNC DOWN");
            ________DBG_20211229_string_hotkey("funcdown");
            string_hotkey_pos = 0;
            memset(string_hotkey, 0, sizeof(string_hotkey[0]) * string_hotkey_max);
            m_func = true;
            mkb_db_is_func_pressed_set(m_func);
            mkb_event_send(EVT_FUNC_DOWN);
            mkb_event_send(EVT_LED_SUB_BATT_CHECK);
            mkb_event_send(EVT_LED_SUB_SET_CH_OS_CHECK_PUSH_DOWN);
        }

        if (event_type == EVT_KEY_UP)
        {

            MKB_LOG_INFO("FUNC UP");
            m_func = false;
            mkb_db_is_func_pressed_set(m_func);
            mkb_event_send(EVT_FUNC_UP);
            ________DBG_20211229_string_hotkey("funcup");
            mkb_event_send(EVT_LED_SUB_SET_CH_OS_CHECK_PUSH_UP);

            char string_touchlock[1] = {HID_KEY_CAPS};
            char string_version[7] = {HID_KEY_V, HID_KEY_E, HID_KEY_R, HID_KEY_S, HID_KEY_I, HID_KEY_O, HID_KEY_N};
            char string_right[5] = {HID_KEY_R, HID_KEY_I, HID_KEY_G, HID_KEY_H, HID_KEY_T};
            char string_left[4] = {HID_KEY_L, HID_KEY_E, HID_KEY_F, HID_KEY_T};
            char string_mokibo[6] = {HID_KEY_M, HID_KEY_O, HID_KEY_K, HID_KEY_I, HID_KEY_B, HID_KEY_O};
            char string_update[6] = {HID_KEY_U, HID_KEY_P, HID_KEY_D, HID_KEY_A, HID_KEY_T, HID_KEY_E};
            char string_battery[7] = {HID_KEY_B, HID_KEY_A, HID_KEY_T, HID_KEY_T, HID_KEY_E, HID_KEY_R, HID_KEY_Y};
            char string_reset[5] = {HID_KEY_R, HID_KEY_E, HID_KEY_S, HID_KEY_E, HID_KEY_T};
            char string_help[4] = {HID_KEY_H, HID_KEY_E, HID_KEY_L, HID_KEY_P};

            for (int i = 0; i < string_hotkey_pos; i++)
            {
                ________DBG_20211229_string_hotkey("hotkey:%d", string_hotkey[i]);
                ________DBG_20220318_touchlock_with_pixart_reset("hotkey:%d", string_hotkey[i]);
            }

            if (arr_cmp(string_touchlock, string_hotkey, 1) == true)
            {
                ________DBG_20220318_touchlock_with_pixart_reset("pixart reset try");
                if (mkb_db_system_state_get() == SYSTEM_STATE_TOUCHLOCK)
                {
                    ________DBG_20220318_touchlock_with_pixart_reset("pixart reset");

                    nrf_delay_ms(100);

                    mkb_led_indication_debug(1, 24, 24, 24);
                    mkb_led_indication_debug(2, 24, 24, 24);
                    mkb_led_indication_debug(3, 24, 24, 24);
                    mkb_led_indication_debug(0, 24, 24, 24);
                    mkb_wdt_feed();
                    nrf_delay_ms(100);
                    mkb_led_indication_debug(1, 0, 0, 0);
                    mkb_led_indication_debug(2, 0, 0, 0);
                    mkb_led_indication_debug(3, 0, 0, 0);
                    mkb_led_indication_debug(0, 0, 0, 0);

                    drv_pixart_reset();
                    mkb_sm_change(SYSTEM_STATE_KEYBOARD);                    

                    nrf_delay_ms(200);
                    mkb_wdt_feed();
                    mkb_led_indication_debug(1, 24, 24, 24);
                    mkb_led_indication_debug(2, 24, 24, 24);
                    mkb_led_indication_debug(3, 24, 24, 24);
                    mkb_led_indication_debug(0, 24, 24, 24);
                    nrf_delay_ms(100);

                    mkb_led_indication_debug(1, 0, 0, 0);
                    mkb_led_indication_debug(2, 0, 0, 0);
                    mkb_led_indication_debug(3, 0, 0, 0);
                    mkb_led_indication_debug(0, 0, 0, 0);
                }
                else
                {

                    nrf_delay_ms(100);

                    mkb_led_indication_debug(1, 24, 24, 24);
                    mkb_led_indication_debug(2, 24, 24, 24);
                    mkb_led_indication_debug(3, 24, 24, 24);
                    mkb_led_indication_debug(0, 24, 24, 24);
                    mkb_wdt_feed();
                    nrf_delay_ms(100);
                    mkb_led_indication_debug(1, 0, 0, 0);
                    mkb_led_indication_debug(2, 0, 0, 0);
                    mkb_led_indication_debug(3, 0, 0, 0);
                    mkb_led_indication_debug(0, 0, 0, 0);

                    mkb_sm_change(SYSTEM_STATE_TOUCHLOCK);

                    nrf_delay_ms(200);
                    mkb_wdt_feed();
                    mkb_led_indication_debug(1, 24, 24, 24);
                    mkb_led_indication_debug(2, 24, 24, 24);
                    mkb_led_indication_debug(3, 24, 24, 24);
                    mkb_led_indication_debug(0, 24, 24, 24);
                    nrf_delay_ms(100);
                    mkb_led_indication_debug(1, 0, 0, 0);
                    mkb_led_indication_debug(2, 0, 0, 0);
                    mkb_led_indication_debug(3, 0, 0, 0);
                    mkb_led_indication_debug(0, 0, 0, 0);
                }
            }
            else if (arr_cmp(string_version, string_hotkey, 7) == true)
            {
                ________DBG_20211229_string_hotkey("ver recognize");
                mkb_event_send(EVT_FUNC_DOWN, MKB_KEYBOARD_HOTKEY_PRINTVER, app_timer_cnt_get());
            }
            else if (arr_cmp(string_right, string_hotkey, 5) == true)
            {
                ________DBG_20211229_string_hotkey("right recognize");
                mkb_event_send(EVT_FUNC_DOWN, MKB_KEYBOARD_HOTKEY_RIGHTED, app_timer_cnt_get());

                for (int i = 0; i < 3; i++)
                {
                    mkb_led_indication_debug(1, 0, 0, 0);
                    mkb_led_indication_debug(2, 0, 0, 0);
                    mkb_led_indication_debug(3, 0, 0, 0);
                    mkb_led_indication_debug(0, 0, 0, 0);
                    mkb_wdt_feed();
                    nrf_delay_ms(100);
                    mkb_led_indication_debug(3, 0, 0, 0);
                    mkb_led_indication_debug(0, 0, 0, 0);
                    mkb_led_indication_debug(1, 100, 100, 100);
                    mkb_led_indication_debug(2, 100, 100, 100);
                    mkb_wdt_feed();
                    nrf_delay_ms(100);
                }
                mkb_led_indication_debug(1, 0, 0, 0);
                mkb_led_indication_debug(2, 0, 0, 0);
                mkb_led_indication_debug(3, 0, 0, 0);
                mkb_led_indication_debug(0, 0, 0, 0);
            }
            else if (arr_cmp(string_left, string_hotkey, 4) == true)
            {
                ________DBG_20211229_string_hotkey("right complete");
                mkb_event_send(EVT_FUNC_DOWN, MKB_KEYBOARD_HOTKEY_LEFTED, app_timer_cnt_get());

                for (int i = 0; i < 3; i++)
                {
                    mkb_led_indication_debug(1, 0, 0, 0);
                    mkb_led_indication_debug(2, 0, 0, 0);
                    mkb_led_indication_debug(3, 0, 0, 0);
                    mkb_led_indication_debug(0, 0, 0, 0);
                    mkb_wdt_feed();
                    nrf_delay_ms(100);
                    mkb_led_indication_debug(1, 0, 0, 0);
                    mkb_led_indication_debug(2, 0, 0, 0);
                    mkb_led_indication_debug(3, 100, 100, 100);
                    mkb_led_indication_debug(0, 100, 100, 100);
                    mkb_wdt_feed();
                    nrf_delay_ms(100);
                }
                mkb_led_indication_debug(1, 0, 0, 0);
                mkb_led_indication_debug(2, 0, 0, 0);
                mkb_led_indication_debug(3, 0, 0, 0);
                mkb_led_indication_debug(0, 0, 0, 0);
            }
            else if (arr_cmp(string_mokibo, string_hotkey, 6) == true)
            {
                ________DBG_20211229_string_hotkey("mokibo complete");
            }
            else if (arr_cmp(string_update, string_hotkey, 5) == true)
            {
                ________DBG_20211229_string_hotkey("dfu complete");
                mkb_dfu_start();
            }
            else if (arr_cmp(string_battery, string_hotkey, 6) == true)
            {
                send_battery_string();
            }
            else if (arr_cmp(string_help, string_hotkey, 4) == true)
            {
                ret_code_t err_code;

                err_code = app_timer_start(m_send_help_timer, APP_TIMER_TICKS(16), NULL);
                MKB_ERROR_CHECK(err_code);

                /*
ios-white
android-green
dex-orange
windows-blue
macos-skyblue

https://www.mokibo.com/support

                */

            } /*
             else if (arr_cmp(string_reset, string_hotkey, 6) == true)
             {
                 mkb_db_default_set();
                 for (int i = 0; i < 1; i++)
                 {
                     mkb_led_indication_debug(1, 0, 0, 0);
                     mkb_led_indication_debug(2, 0, 0, 0);
                     mkb_led_indication_debug(3, 0, 0, 0);
                     mkb_led_indication_debug(0, 0, 0, 0);

                     mkb_led_indication_debug(1, 30, 30, 30);
                     mkb_wdt_feed();
                     nrf_delay_ms(100);
                     mkb_led_indication_debug(2, 30, 30, 30);
                     mkb_wdt_feed();
                     nrf_delay_ms(100);
                     mkb_led_indication_debug(3, 30, 30, 30);
                     mkb_wdt_feed();
                     nrf_delay_ms(100);
                     mkb_led_indication_debug(0, 30, 30, 30);
                     mkb_wdt_feed();
                     nrf_delay_ms(100);
                 }
                 mkb_led_indication_debug(1, 0, 0, 0);
                 mkb_led_indication_debug(2, 0, 0, 0);
                 mkb_led_indication_debug(3, 0, 0, 0);
                 mkb_led_indication_debug(0, 0, 0, 0);
             }*/

            string_hotkey_pos = 0;
            memset(string_hotkey, 0, sizeof(string_hotkey[0]) * string_hotkey_max);

            if (m_remap_key)
            {
                p_hotkey_table = keyboard_hotkey_table_get(m_remap_key);

                if ((p_hotkey_table->long_attr & LONG_KEY) == LONG_KEY)
                {
                    keyboard_hold_timer_stop(m_remap_key);

                    if ((p_hotkey_table->event_act == true))
                    {
                        p_hotkey_table->event_act = false;

                        if ((p_hotkey_table->long_hotkey != MKB_KEYBOARD_HOTKEY_NONE) &&
                            ((p_hotkey_table->long_attr & RL_EDGE) == RL_EDGE))
                        {
                            MKB_LOG_INFO("FUNC LONG KEY EVENT: 0x%02x(%s)", m_remap_key, keyboard_hidkey_str(m_remap_key));
                            mkb_event_send(EVT_FUNC_HOLD, p_hotkey_table->long_hotkey, timestamp);
                        }
                    }
                    else
                    {
                        if ((p_hotkey_table->hotkey != MKB_KEYBOARD_HOTKEY_NONE) &&
                            (p_hotkey_table->attr & R_EDGE) == R_EDGE)
                        {
                            MKB_LOG_INFO("FUNC KEY EVENT: 0x%02x(%s)", m_remap_key, keyboard_hidkey_str(m_remap_key));
                            mkb_event_send(EVT_FUNC_UP, p_hotkey_table->hotkey, timestamp);
                        }
                    }
                }
                else
                {
                    if ((p_hotkey_table->event_act == false) &&
                        (p_hotkey_table->hotkey != MKB_KEYBOARD_HOTKEY_NONE) &&
                        (p_hotkey_table->attr & R_EDGE) == R_EDGE)
                    {
                        MKB_LOG_INFO("FUNC KEY EVENT: 0x%02x(%s)", m_remap_key, keyboard_hidkey_str(m_remap_key));
                        mkb_event_send(EVT_FUNC_UP, p_hotkey_table->hotkey, timestamp);
                    }
                }

                m_remap_key = 0;
            }
        }
    }
    else // if (hid_key != KEYBOARD_FUNC_KEY)
    {
        if (m_func) //조합키가 눌려진 상태라면
        {
            if (hid_key == HID_KEY_UP ||
                hid_key == HID_KEY_DOWN ||
                hid_key == HID_KEY_MKB ||
                hid_key == HID_KEY_SB ||
                hid_key == HID_KEY_RED ||
                hid_key == HID_KEY_L_SHFT ||
                hid_key == HID_KEY_L_CTRL ||
                hid_key == HID_KEY_L_ALT ||
                hid_key == HID_KEY_L_GUI ||
                hid_key == HID_KEY_R_GUI ||
                hid_key == HID_KEY_R_ALT ||
                hid_key == HID_KEY_R_CTRL ||
                hid_key == HID_KEY_R_SHFT)
            {
                if (event_type == EVT_KEY_DOWN)
                {
                    ________DBG_20210830_key_event_dump_phenomenon_debug("key_down");
                    mkb_event_send(EVT_KEY_DOWN, hid_key, timestamp);
                }
                else if (event_type == EVT_KEY_UP)
                {
                    ________DBG_20210830_key_event_dump_phenomenon_debug("key_up");
                    mkb_event_send(EVT_KEY_UP, hid_key, timestamp);
                    MKB_LOG_INFO("KEYINFO:%d", hid_key);
                }
            }

            MKB_LOG_INFO("fn + key");
            m_remap_key = keyboard_fn_key_remap(hid_key);

            if (event_type == EVT_KEY_DOWN)
            {
                if (hid_key != HID_KEY_UP ||
                    hid_key != HID_KEY_DOWN ||
                    hid_key != HID_KEY_MKB ||
                    hid_key != HID_KEY_SB ||
                    hid_key != HID_KEY_RED ||
                    hid_key != HID_KEY_L_SHFT ||
                    hid_key != HID_KEY_L_CTRL ||
                    hid_key != HID_KEY_L_ALT ||
                    hid_key != HID_KEY_L_GUI ||
                    hid_key != HID_KEY_R_SHFT ||
                    hid_key != HID_KEY_R_ALT ||
                    hid_key != HID_KEY_R_CTRL)
                {
                    ________DBG_20211229_string_hotkey("pos++");
                    string_hotkey[string_hotkey_pos] = hid_key;
                    if (string_hotkey_pos < string_hotkey_max - 1)
                    {
                        ________DBG_20220318_touchlock_with_pixart_reset("hotkey pos");
                        string_hotkey_pos++;
                    }
                }
            }

            if (m_remap_key)
            {
                p_hotkey_table = keyboard_hotkey_table_get(m_remap_key);

                if (event_type == EVT_KEY_DOWN)
                {
                    if ((p_hotkey_table->long_attr & LONG_KEY) == LONG_KEY)
                    {
                        keyboard_hold_timer_start(m_remap_key);
                    }
                    else
                    {
                        if ((p_hotkey_table->attr & P_EDGE) == P_EDGE)
                        {
                            MKB_LOG_INFO("FUNC KEY EVENT: 0x%02x(%s)", m_remap_key, keyboard_hidkey_str(m_remap_key));
                            mkb_event_send(EVT_FUNC_DOWN, p_hotkey_table->hotkey, timestamp);
                        }
                    }
                }
                else
                {
                    if ((p_hotkey_table->long_attr & LONG_KEY) == LONG_KEY)
                    {
                        keyboard_hold_timer_stop(m_remap_key);

                        if ((p_hotkey_table->event_act == true))
                        {
                            p_hotkey_table->event_act = false;

                            if ((p_hotkey_table->long_hotkey != MKB_KEYBOARD_HOTKEY_NONE) &&
                                ((p_hotkey_table->long_attr & RL_EDGE) == RL_EDGE))
                            {
                                MKB_LOG_INFO("FUNC LONG KEY EVENT: 0x%02x(%s)", m_remap_key, keyboard_hidkey_str(m_remap_key));
                                mkb_event_send(EVT_FUNC_HOLD, p_hotkey_table->long_hotkey, timestamp);
                            }
                        }
                        else
                        {
                            if ((p_hotkey_table->hotkey != MKB_KEYBOARD_HOTKEY_NONE) &&
                                (p_hotkey_table->attr & R_EDGE) == R_EDGE)
                            {
                                MKB_LOG_INFO("FUNC KEY EVENT: 0x%02x(%s)", m_remap_key, keyboard_hidkey_str(m_remap_key));
                                mkb_event_send(EVT_FUNC_UP, p_hotkey_table->hotkey, timestamp);
                            }
                        }
                    }
                    else
                    {
                        if ((p_hotkey_table->event_act == false) &&
                            (p_hotkey_table->hotkey != MKB_KEYBOARD_HOTKEY_NONE) &&
                            (p_hotkey_table->attr & R_EDGE) == R_EDGE)
                        {
                            MKB_LOG_INFO("FUNC KEY EVENT: 0x%02x(%s)", m_remap_key, keyboard_hidkey_str(m_remap_key));
                            mkb_event_send(EVT_FUNC_UP, p_hotkey_table->hotkey, timestamp);
                        }
                    }

                    m_remap_key = 0;
                }
            }
        }
        else //조합키가 눌려져있지 않은 상태라면
        {
            if (event_type == EVT_KEY_DOWN)
            {

                if (hid_key == HID_KEY_R_ALT)
                {
                    uint8_t input_delay = 0;
                    ________DBG_20220318_func_ralt("2");
                    switch (mkb_db_curr_central_type_get())
                    {
                    case CENTRAL_DEX:
                    case CENTRAL_ANDROID:
                        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_L_SHFT);
                        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_SB);
                        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_SB);
                        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_L_SHFT);
                        break;
                    case CENTRAL_MAC:
                    case CENTRAL_IOS:
                        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_L_CTRL);
                        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_SB);
                        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_SB);
                        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_L_CTRL);
                        break;
                    case CENTRAL_WINDOWS:
                    case CENTRAL_LINUX:
                        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_DOWN, HID_KEY_R_ALT);
                        mkb_event_send_ms((input_delay++) * 8, EVT_KEY_UP, HID_KEY_R_ALT);
                        break;
                    }
                }
                else
                {
                    mkb_event_send(EVT_KEY_DOWN, hid_key, timestamp);
                }
                ________DBG_20210830_key_event_dump_phenomenon_debug("key_down");
            }
            else if (event_type == EVT_KEY_UP)
            {
                ________DBG_20210830_key_event_dump_phenomenon_debug("key_up");
                mkb_event_send(EVT_KEY_UP, hid_key, timestamp);
                MKB_LOG_INFO("KEYINFO:%d", hid_key);
            }
        }
    }
}

static void mkb_keyboard_process(void *p_event_data, uint16_t event_size)
{
    uint8_t new_keys[6] = {
        0,
    };
    uint8_t new_mods = 0;

    static uint8_t old_keys[6] = {
        0,
    };
    static uint8_t old_mods = 0; // modifier key

    uint32_t timestamp = app_timer_cnt_get();
    bool is_key_changed = false;

    memcpy(new_keys, (uint8_t *)p_event_data, 6 * sizeof(uint8_t)); // p_event_data의 [0]~[5] 번 패킷
    new_mods = *((uint8_t *)(p_event_data + sizeof(uint8_t) * 13)); //*p_event_data의 [14] 번 패킷

    ________DBG_20210830_key_event_dump_phenomenon_debug("news> mods: 0x%02x keys: %02d %02d %02d %02d %02d %02d", new_mods, new_keys[0], new_keys[1], new_keys[2], new_keys[3], new_keys[4], new_keys[5]);

    //키값 기준으로 큰순으로 정렬한다.
    for (uint8_t i = 0; i < KEYBOARD_MAX_KEYS - 1; i++)
    {
        if (new_keys[i] == 0 && old_keys[i] == 0) //둘다 0이면 정렬 종료
        {
            break;
        }
        else
        {
            for (uint8_t k = i + 1; k < KEYBOARD_MAX_KEYS; k++)
            {
                if (new_keys[i] < new_keys[k])
                {
                    uint8_t new_keys_temp = new_keys[i];
                    new_keys[i] = new_keys[k];
                    new_keys[k] = new_keys_temp;
                }
            }
        }
    }

    ________DBG_20210830_key_event_dump_phenomenon_debug("olds> mods: 0x%02x keys: %02d %02d %02d %02d %02d %02d", old_mods, old_keys[0], old_keys[1], old_keys[2], old_keys[3], old_keys[4], old_keys[5]);
    ________DBG_20210830_key_event_dump_phenomenon_debug("sort> mods: 0x%02x keys: %02d %02d %02d %02d %02d %02d", new_mods, new_keys[0], new_keys[1], new_keys[2], new_keys[3], new_keys[4], new_keys[5]);

    for (uint8_t new_key_pos = 0, old_key_pos = 0;; (new_key_pos < KEYBOARD_MAX_KEYS || old_key_pos < KEYBOARD_MAX_KEYS))
    {
        if (new_keys[new_key_pos] == 0 && old_keys[old_key_pos] == 0)
        {
            break;
        }
        else if (new_keys[new_key_pos] > old_keys[old_key_pos]) // down
        {
            mkb_keyboard_event_generate(EVT_KEY_DOWN, new_keys[new_key_pos], timestamp);
            new_key_pos++;
        }
        else if (new_keys[new_key_pos] < old_keys[old_key_pos]) // up
        {
            mkb_keyboard_event_generate(EVT_KEY_UP, old_keys[old_key_pos], timestamp);
            old_key_pos++;
        }
        else // 키값이 같을때
        {
            new_key_pos++;
            old_key_pos++;
        }
    }
    memcpy(old_keys, new_keys, sizeof(uint8_t) * 6); //저장

    if (old_mods != new_mods)
    {
        for (uint8_t i = 0; i < 8; i++)
        {
            if ((new_mods & (1 << i)) != (old_mods & (1 << i)))
            {
                if (new_mods & (1 << i))
                {
                    MKB_LOG_INFO("EVT_KEY_DOWN");
                    mkb_keyboard_event_generate(EVT_KEY_DOWN, HID_MODIFIER_START + i, timestamp);
                }
                else
                {
                    MKB_LOG_INFO("EVT_KEY_UP");
                    mkb_keyboard_event_generate(EVT_KEY_UP, HID_MODIFIER_START + i, timestamp);
                }
            }
        }
        old_mods = new_mods;
    }
}

static void mkb_keyboard_read_handler(uint8_t *p_pressed_keys, uint8_t num_of_pressed_keys, bool keys_blocked)
{
    ________DBG_20210830_key_event_dump_phenomenon_debug("mkb_keyboard_read_handler");
    if (!keys_blocked)
    {
        mkb_keyboard_process(p_pressed_keys, num_of_pressed_keys);
    }
}

bool mkb_keyboard_startup_check(void)
{
#if (defined(MKB_KEYBOARD_STARTUP_ENABLED) && MKB_KEYBOARD_STARTUP_ENABLED)
    ret_code_t status;
    uint8_t pressed_keys[KEYBOARD_MAX_KEYS];
    uint8_t number_of_pressed_keys;
    bool keys_blocked;
    uint8_t key_hid, key_cnt = 0;

#if (defined(MKB_KEYBOARD_STARTUP_KEY_1) && MKB_KEYBOARD_STARTUP_KEY_1)
    bool key_1 = false;
#endif
#if (defined(MKB_KEYBOARD_STARTUP_KEY_2) && MKB_KEYBOARD_STARTUP_KEY_2)
    bool key_2 = false;
#endif
#if (defined(MKB_KEYBOARD_STARTUP_KEY_3) && MKB_KEYBOARD_STARTUP_KEY_3)
    bool key_3 = false;
#endif

    /* Perform initial keyboard scan. */
    status = drv_keymatrix_keys_get(pressed_keys, &number_of_pressed_keys, &keys_blocked);
    if (status != NRF_SUCCESS)
    {
        MKB_LOG_ERROR("Can't read keymatrix for checking startup keys");
        return status;
    }

    for (uint8_t i = 0; i < number_of_pressed_keys; i++)
    {
        key_hid = keyboard_keyid_to_hidkey(pressed_keys[i]);

#if (defined(MKB_KEYBOARD_STARTUP_KEY_1) && MKB_KEYBOARD_STARTUP_KEY_1)
        if (key_hid != HID_KEY_NONE && key_hid == MKB_KEYBOARD_STARTUP_KEY_1)
        {
            key_1 = true;
            key_cnt++;
            MKB_LOG_DEBUG("KEY[%d]: 0x%02x", i, pressed_keys[i]);
        }
#endif
#if (defined(MKB_KEYBOARD_STARTUP_KEY_2) && MKB_KEYBOARD_STARTUP_KEY_2)
        if (key_hid != HID_KEY_NONE && key_hid == MKB_KEYBOARD_STARTUP_KEY_2)
        {
            key_2 = true;
            key_cnt++;
            MKB_LOG_DEBUG("KEY[%d]: 0x%02x", i, pressed_keys[i]);
        }
#endif
#if (defined(MKB_KEYBOARD_STARTUP_KEY_3) && MKB_KEYBOARD_STARTUP_KEY_3)
        if (key_hid != HID_KEY_NONE && key_hid == MKB_KEYBOARD_STARTUP_KEY_3)
        {
            key_3 = true;
            key_cnt++;
            MKB_LOG_DEBUG("KEY[%d]: 0x%02x", i, pressed_keys[i]);
        }
#endif
    }

    MKB_LOG_DEBUG("Startup Keys state [%d]", (key_cnt) ? true : false);

    if (key_cnt == MKB_KEYBOARD_STARTUP_KEY_CNT)
        return true;
    else
        return false;
#else
    return false;
#endif // (defined(MKB_KEYBOARD_STARTUP_ENABLED) && MKB_KEYBOARD_STARTUP_ENABLED)
}

bool mkb_keyboard_exist_check(void)
{
#if (defined(MKB_KEYMATRIX_ENABLED) && MKB_KEYMATRIX_ENABLED)
    ret_code_t status;
    uint8_t pressed_keys[KEYBOARD_MAX_KEYS];
    uint8_t number_of_pressed_keys;
    bool keys_blocked;

    /* Perform initial keyboard scan. */
    status = drv_keymatrix_keys_get(pressed_keys, &number_of_pressed_keys, &keys_blocked);
    if (status != NRF_SUCCESS)
    {
        MKB_LOG_ERROR("Can't read keymatrix for checking startup keys");
        return false;
    }

    if (number_of_pressed_keys > 0)
        return true;
    else
        return false;
#else
    return false;
#endif // (defined(MKB_KEYMATRIX_ENABLED) && MKB_KEYMATRIX_ENABLED)
}

ret_code_t mkb_keyboard_init(void)
{
    ret_code_t status;

    status = keyboard_hold_timer_init();
    if (status != NRF_SUCCESS)
    {
        return status;
    }

// Keyboard init
#if (defined(MKB_KEYMATRIX_ENABLED) && MKB_KEYMATRIX_ENABLED)
    status = drv_keymatrix_init(mkb_keyboard_read_handler);
    if (status != NRF_SUCCESS)
    {
        return status;
    }
#endif // (defined(MKB_KEYMATRIX_ENABLED) && MKB_KEYMATRIX_ENABLED)

#if (defined(MKB_PIXART_ENABLED) && MKB_PIXART_ENABLED)
    status = drv_pixart_register_key_handler(mkb_keyboard_read_handler);
    if (status != NRF_SUCCESS)
    {
        return status;
    }
#endif // (defined(MKB_PIXART_ENABLED) && MKB_PIXART_ENABLED)

    MKB_LOG_INFO("Initialization Success!");

    status = app_timer_create(&m_send_help_timer, APP_TIMER_MODE_REPEATED, send_help_timer_handler);

    return NRF_SUCCESS;
}

ret_code_t mkb_keyboard_start(void)
{
    ret_code_t status;

#if (defined(MKB_KEYMATRIX_ENABLED) && MKB_KEYMATRIX_ENABLED)
    status = drv_keymatrix_enable();
    if (status != NRF_SUCCESS)
    {
        return status;
    }
#endif // (defined(MKB_KEYMATRIX_ENABLED) && MKB_KEYMATRIX_ENABLED)

    MKB_LOG_INFO("Enabled");

    return NRF_SUCCESS;
}

ret_code_t mkb_keyboard_stop(void)
{
    ret_code_t status;

#if (defined(MKB_KEYMATRIX_ENABLED) && MKB_KEYMATRIX_ENABLED)
    status = drv_keymatrix_disable();
    if (status != NRF_SUCCESS)
    {
        return status;
    }
#endif // (defined(MKB_KEYMATRIX_ENABLED) && MKB_KEYMATRIX_ENABLED)

    MKB_LOG_INFO("Disabled");

    return NRF_SUCCESS;
}

void keyboard_caps_lock_set(bool flag)
{
    MKB_LOG_INFO("caps lock: %s", (flag) ? "ON" : "OFF");

    m_keyboard_caps_on = flag;
}

bool keyboard_caps_lock_get(void)
{
    return m_keyboard_caps_on;
}

ret_code_t mkb_keyboard_cli(size_t size, char **params)
{
    ret_code_t err_code = NRF_SUCCESS;
    if (!strcmp(params[1], "help"))
    {
    }
    else
    {
        err_code = NRF_ERROR_INTERNAL;
    }
    return err_code;
}

#if (defined(NRF_PWR_MGMT_ENABLED) && NRF_PWR_MGMT_ENABLED)
static bool mkb_keyboard_shutdown(nrf_pwr_mgmt_evt_t event)
{
    // stop hold timer
    keyboard_hold_timers_stop();

    return true;
}

NRF_PWR_MGMT_HANDLER_REGISTER(mkb_keyboard_shutdown, SHUTDOWN_PRIORITY_DEFAULT);
#endif /* NRF_PWR_MGMT_ENABLED */

///////////////////////////////////////////////////////////////////////////////

#endif // (defined(MKB_KEYBOARD_ENABLED) && MKB_KEYBOARD_ENABLED)
