/*
* ===============================================================================
* Copyright (C) 2017-2019
* INNOPRESSO INC. All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
* ===============================================================================
*/

#ifndef __MKB_BLE_ADV__
#define __MKB_BLE_ADV__

#include "mkb_config.h"
#include "mkb_db.h"

#define APP_ADV_DIRECT_INTERVAL 0x0028 /**< Fast advertising interval (in units of 0.625 ms. This value corresponds to 25 ms.). */
#define APP_ADV_FAST_INTERVAL 0x0028   /**< Fast advertising interval (in units of 0.625 ms. This value corresponds to 25 ms.). */
#define APP_ADV_SLOW_INTERVAL 0x00A0   /**< Slow advertising interval (in units of 0.625 ms. This value corresponds to 100 ms.). */

#define APP_ADV_DIRECT_DURATION 1000 /**< The advertising duration of fast advertising in units of 10 milliseconds. */
#define APP_ADV_FAST_DURATION 3000   /**< The advertising duration of fast advertising in units of 10 milliseconds. */
#define APP_ADV_SLOW_DURATION 18000  /**< The advertising duration of slow advertising in units of 10 milliseconds. */

/**@brief Advertising mode. */
typedef enum
{
    ADV_MODE_NONE,        /**< No advertising running. */
    ADV_MODE_DIRECTED,    /**< Directed advertising. */
    ADV_MODE_UNDIRECTED,  /**< Undirected advertising. */
    ADV_MODE_WHITELIST,   /**< Undirected advertising with whitelist (when directed cannot be used, not bondable). */
    ADV_MODE_NOWHITELIST, /**< Undirected advertising without whitelist (when directed cannot be used, not bondable). */
} MKB_BLE_ADV_MODE_t;

/**@brief Struct to keep track of advertising type and dynamic parameters. */
typedef struct
{
    MKB_BLE_ADV_MODE_t mode; /**< Advertising mode */
    bool bond_initiate;      /**< Indicates whether a new bond should be made. */
    bool adv_active;         /**< Indicates whether advertising is active. */
} MKB_BLE_ADV_STATE_t;

extern ret_code_t mkb_ble_adv_init(void);
extern ret_code_t mkb_ble_adv_start(CHANNEL_ID_t adv_ch, MKB_BLE_ADV_MODE_t adv_mode);
extern ret_code_t mkb_ble_adv_stop(void);
extern bool mkb_ble_adv_running(void);
extern void mkb_ble_adv_state_reset(void);
extern void mkb_ble_adv_wait_for_advertizing_set(MKB_BLE_ADV_MODE_t adv_mode);
extern MKB_BLE_ADV_MODE_t mkb_ble_adv_wait_for_advertizing_get(void);
extern ret_code_t mkb_ble_adv_cli(size_t size, char **params);

#endif /* __MKB_BLE_ADV__ */
