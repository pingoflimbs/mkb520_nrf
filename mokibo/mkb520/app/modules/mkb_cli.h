/*
* ===============================================================================
* Copyright (C) 2017-2019
* INNOPRESSO INC. All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
* ===============================================================================
*/

#ifndef __MKB_CLI__
#define __MKB_CLI__

#include "nrf_cli.h"

extern void mkb_cli_init(void);
extern void mkb_cli_start(void);
extern void mkb_cli_process(void);
extern void mkb_cli_unknown_param_help(nrf_cli_t const *p_cli, char const *p_param, char const *p_cmd);
extern void mkb_cli_wrong_param_count_help(nrf_cli_t const *p_cli, char const *p_cmd);

#endif /* __MKB_CLI__ */
