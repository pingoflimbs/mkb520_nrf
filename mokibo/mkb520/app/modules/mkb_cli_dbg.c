/*
* ===============================================================================
* Copyright (C) 2017-2019
* INNOPRESSO INC. All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
* ===============================================================================
*/

#include <stdbool.h>
#include <stdint.h>

#include "sdk_common.h"

#include "nrf_assert.h"
#include "nrf_error.h"
#include "nrf_log.h"
#include "nrf_delay.h"
#include "nrf_cli.h"
#include "nrf_pwr_mgmt.h"

#define MKB_LOG_LEVEL MKB_CLI_LOG_LEVEL
#include "mkb_common.h"

#if (defined(MKB_CLI_ENABLED) && MKB_CLI_ENABLED)
#include "mkb_cli.h"

#if (defined(MKB_SYSTEM_ENABLED) && MKB_SYSTEM_ENABLED)
#include "mkb_system.h"
#endif //(defined(MKB_SYSTEM_ENABLED) && MKB_SYSTEM_ENABLED)

#include "mkb_led.h"

#include "drv_pixart.h"

#include "mkb_state_manager.h"

#include "mkb_ble_adv.h"
#include "mkb_keyboard.h"
#include "mkb_dfu.h"
#include "mkb_event.h"
#include "mkb_coms.h"

#define MOKIBO_HELP "MOKIBO CLI command lists print\n" \
                    "usage: mokibo"

static void cmd_dbg_kbd(nrf_cli_t const *p_cli, size_t argc, char **argv)
{
    ret_code_t err_code = NRF_ERROR_INTERNAL;

    char params_log[256];
    sprintf(params_log, "%s(", argv[1]);
    for (uint8_t i = 2; i < argc || argv[i]; i++)
    {
        if (argv[i + 1] == NULL)
        {
            sprintf(params_log, "%s%s", params_log, argv[i]);
            break;
        }
        sprintf(params_log, "%s%s,", params_log, argv[i]);
    }
    sprintf(params_log, "%s)", params_log);
    ________DBG_20210831_mkb_cli_dbg("cli_dbg_%s-> %s", argv[0], params_log);

    if (p_cli)
    {
        err_code = mkb_keyboard_cli(argc, argv);
    }

    if (err_code == NRF_SUCCESS)
    {
        ________DBG_20210831_mkb_cli_dbg("CLI_DBG:SUCCESS ");
    }
    else
    {
        ________DBG_20210831_mkb_cli_dbg("CLI_DBG:FAIL");
    }
}

static void cmd_dbg_pix(nrf_cli_t const *p_cli, size_t argc, char **argv)
{
    ret_code_t err_code = NRF_ERROR_INTERNAL;

    char params_log[256];
    sprintf(params_log, "%s(", argv[1]);
    for (uint8_t i = 2; i < argc || argv[i]; i++)
    {
        if (argv[i + 1] == NULL)
        {
            sprintf(params_log, "%s%s", params_log, argv[i]);
            break;
        }
        sprintf(params_log, "%s%s,", params_log, argv[i]);
    }
    sprintf(params_log, "%s)", params_log);
    ________DBG_20210831_mkb_cli_dbg("cli_dbg_%s-> %s", argv[0], params_log);

    if (p_cli)
    {
        err_code = mkb_pixart_cli(argc, argv);
    }

    if (err_code == NRF_SUCCESS)
    {
        ________DBG_20210831_mkb_cli_dbg("CLI_DBG:SUCCESS ");
    }
    else
    {
        ________DBG_20210831_mkb_cli_dbg("CLI_DBG:FAIL");
    }
}

static void cmd_dbg_stt(nrf_cli_t const *p_cli, size_t argc, char **argv)
{
    ret_code_t err_code = NRF_ERROR_INTERNAL;

    char params_log[256];
    sprintf(params_log, "%s(", argv[1]);
    for (uint8_t i = 2; i < argc || argv[i]; i++)
    {
        if (argv[i + 1] == NULL)
        {
            sprintf(params_log, "%s%s", params_log, argv[i]);
            break;
        }
        sprintf(params_log, "%s%s,", params_log, argv[i]);
    }
    sprintf(params_log, "%s)", params_log);
    ________DBG_20210831_mkb_cli_dbg("cli_dbg_%s-> %s", argv[0], params_log);

    if (p_cli)
    {
        err_code = mkb_state_manager_cli(argc, argv);
    }

    if (err_code == NRF_SUCCESS)
    {
        ________DBG_20210831_mkb_cli_dbg("CLI_DBG:SUCCESS ");
    }
    else
    {
        ________DBG_20210831_mkb_cli_dbg("CLI_DBG:FAIL");
    }
}

static void cmd_dbg_adv(nrf_cli_t const *p_cli, size_t argc, char **argv)
{
    ret_code_t err_code = NRF_ERROR_INTERNAL;

    char params_log[256];
    sprintf(params_log, "%s(", argv[1]);
    for (uint8_t i = 2; i < argc || argv[i]; i++)
    {
        if (argv[i + 1] == NULL)
        {
            sprintf(params_log, "%s%s", params_log, argv[i]);
            break;
        }
        sprintf(params_log, "%s%s,", params_log, argv[i]);
    }
    sprintf(params_log, "%s)", params_log);
    ________DBG_20210831_mkb_cli_dbg("cli_dbg_%s-> %s", argv[0], params_log);

    if (p_cli)
    {
        err_code = mkb_ble_adv_cli(argc, argv);
    }

    if (err_code == NRF_SUCCESS)
    {
        ________DBG_20210831_mkb_cli_dbg("CLI_DBG:SUCCESS ");
    }
    else
    {
        ________DBG_20210831_mkb_cli_dbg("CLI_DBG:FAIL");
    }
}

static void cmd_dbg_com(nrf_cli_t const *p_cli, size_t argc, char **argv)
{
    ret_code_t err_code = NRF_ERROR_INTERNAL;

    char params_log[256];
    sprintf(params_log, "%s(", argv[1]);
    for (uint8_t i = 2; i < argc || argv[i]; i++)
    {
        if (argv[i + 1] == NULL)
        {
            sprintf(params_log, "%s%s", params_log, argv[i]);
            break;
        }
        sprintf(params_log, "%s%s,", params_log, argv[i]);
    }
    sprintf(params_log, "%s)", params_log);
    ________DBG_20210831_mkb_cli_dbg("cli_dbg_%s-> %s", argv[0], params_log);

    if (p_cli)
    {
        err_code = mkb_coms_cli(argc, argv);
    }

    if (err_code == NRF_SUCCESS)
    {
        ________DBG_20210831_mkb_cli_dbg("CLI_DBG:SUCCESS ");
    }
    else
    {
        ________DBG_20210831_mkb_cli_dbg("CLI_DBG:FAIL");
    }
}

static void cmd_dbg_led(nrf_cli_t const *p_cli, size_t argc, char **argv)
{
    ASSERT(p_cli);
    ASSERT(p_cli->p_ctx && p_cli->p_iface && p_cli->p_name);
    /* Options must be handled with strcmp function */
    ret_code_t err_code = NRF_ERROR_INTERNAL;

    char params_log[256];
    sprintf(params_log, "%s(", argv[1]);
    for (uint8_t i = 2; i < argc || argv[i]; i++)
    {
        if (argv[i + 1] == NULL)
        {
            sprintf(params_log, "%s%s", params_log, argv[i]);
            break;
        }
        sprintf(params_log, "%s%s,", params_log, argv[i]);
    }
    sprintf(params_log, "%s)", params_log);
    ________DBG_20210831_mkb_cli_dbg("cli_dbg_%s-> %s", argv[0], params_log);

    if (p_cli)
    {
        err_code = mkb_led_cli(argc, argv);
    }

    if (err_code == NRF_SUCCESS)
    {
        ________DBG_20210831_mkb_cli_dbg("CLI_DBG:SUCCESS ");
    }
    else
    {
        ________DBG_20210831_mkb_cli_dbg("CLI_DBG:FAIL");
    }
}

static void cmd_dbg_dfu(nrf_cli_t const *p_cli, size_t argc, char **argv)
{
    ret_code_t err_code = NRF_ERROR_INTERNAL;

    char params_log[256];
    sprintf(params_log, "%s(", argv[1]);
    for (uint8_t i = 2; i < argc || argv[i]; i++)
    {
        if (argv[i + 1] == NULL)
        {
            sprintf(params_log, "%s%s", params_log, argv[i]);
            break;
        }
        sprintf(params_log, "%s%s,", params_log, argv[i]);
    }
    sprintf(params_log, "%s)", params_log);
    ________DBG_20210831_mkb_cli_dbg("cli_dbg_%s-> %s", argv[0], params_log);

    if (p_cli)
    {
        err_code = mkb_dfu_cli(argc, argv);
    }

    if (err_code == NRF_SUCCESS)
    {
        ________DBG_20210831_mkb_cli_dbg("CLI_DBG:SUCCESS ");
    }
    else
    {
        ________DBG_20210831_mkb_cli_dbg("CLI_DBG:FAIL");
    }
}

static void cmd_dbg_evt(nrf_cli_t const *p_cli, size_t argc, char **argv)
{
    ret_code_t err_code = NRF_ERROR_INTERNAL;

    char params_log[256];
    sprintf(params_log, "%s(", argv[1]);
    for (uint8_t i = 2; i < argc || argv[i]; i++)
    {
        if (argv[i + 1] == NULL)
        {
            sprintf(params_log, "%s%s", params_log, argv[i]);
            break;
        }
        sprintf(params_log, "%s%s,", params_log, argv[i]);
    }
    sprintf(params_log, "%s)", params_log);
    ________DBG_20210831_mkb_cli_dbg("cli_dbg_%s-> %s", argv[0], params_log);

    if (p_cli)
    {
        err_code = mkb_event_cli(argc, argv);
    }

    if (err_code == NRF_SUCCESS)
    {
        ________DBG_20210831_mkb_cli_dbg("CLI_DBG:SUCCESS ");
    }
    else
    {
        ________DBG_20210831_mkb_cli_dbg("CLI_DBG:FAIL");
    }
}

static void cmd_dbg(nrf_cli_t const *p_cli, size_t argc, char **argv)
{
    UNUSED_PARAMETER(argc);
    UNUSED_PARAMETER(argv);

    if (nrf_cli_help_requested(p_cli))
    {
        nrf_cli_help_print(p_cli, NULL, 0);
        return;
    }

    nrf_cli_fprintf(p_cli, NRF_CLI_DEFAULT, "dbg CLI command lists\n");
    nrf_cli_fprintf(p_cli, NRF_CLI_DEFAULT, "dbg pix\n");
    nrf_cli_fprintf(p_cli, NRF_CLI_DEFAULT, "dbg stt\n");
    nrf_cli_fprintf(p_cli, NRF_CLI_DEFAULT, "dbg adv\n");
    nrf_cli_fprintf(p_cli, NRF_CLI_DEFAULT, "dbg led\n");
    nrf_cli_fprintf(p_cli, NRF_CLI_DEFAULT, "dbg kbd\n");
    nrf_cli_fprintf(p_cli, NRF_CLI_DEFAULT, "dbg dfu\n");
    nrf_cli_fprintf(p_cli, NRF_CLI_DEFAULT, "dbg evt\n");
    nrf_cli_fprintf(p_cli, NRF_CLI_DEFAULT, "dbg com\n");
}

NRF_CLI_CREATE_STATIC_SUBCMD_SET(m_sub_dbg){
    NRF_CLI_CMD(pix, NULL, "", cmd_dbg_pix), //enable,disable,reset, wakeup
    NRF_CLI_CMD(stt, NULL, "", cmd_dbg_stt), //touch, touchlock, keyboard
    NRF_CLI_CMD(adv, NULL, "", cmd_dbg_adv), // start
    NRF_CLI_CMD(led, NULL, "", cmd_dbg_led), //red,green,blue,off
    NRF_CLI_CMD(kbd, NULL, "", cmd_dbg_kbd),
    NRF_CLI_CMD(dfu, NULL, "", cmd_dbg_dfu),
    NRF_CLI_CMD(pwr, NULL, "", cmd_dbg_evt),
    NRF_CLI_CMD(pwr, NULL, "", cmd_dbg_com),
    NRF_CLI_SUBCMD_SET_END};

NRF_CLI_CMD_REGISTER(dbg, &m_sub_dbg, MOKIBO_HELP, cmd_dbg);

#endif
