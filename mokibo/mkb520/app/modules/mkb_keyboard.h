/*
 * ===============================================================================
 * Copyright (C) 2017-2019
 * INNOPRESSO INC. All Rights Reserved
 *
 * The content of this file is copyrighted by INNOPRESSO or its
 * affiliated, associated or related companies. All copyrights and other
 * associated intellectual property rights related thereto are expressly
 * reserved by INNOPRESSO INC.
 * ===============================================================================
 */

#ifndef __MKB_KEYBOARD_H__
#define __MKB_KEYBOARD_H__

#include "app_timer.h"

extern bool m_func;
#define KEYBOARD_TYPE_MOKIBO 0
#define KEYBOAED_TYPE_IMAGES 1
#define KEYBOARD_TYPE MKB_KEYBOARD_TYPE

#define KEYBOARD_MAX_KEYS 6
#define KEYBOARD_NUM_OF_COLUMNS 15 //!< Number of columns in the keyboard matrix.
#define KEYBOARD_NUM_OF_ROWS 8	   //!< Number of rows in the keyboard matrix.

#define KEYBOARD_FUNC_KEY HID_KEY_FUNC

#define S2L(VAL) (HID_KEY_##VAL)

#define HID_KEY_NONE 0			   /** 0x00 */
#define HID_KEY_A 4				   /** 0x04 */
#define HID_KEY_B 5				   /** 0x05 */
#define HID_KEY_C 6				   /** 0x06 */
#define HID_KEY_D 7				   /** 0x07 */
#define HID_KEY_E 8				   /** 0x08 */
#define HID_KEY_F 9				   /** 0x09 */
#define HID_KEY_G 10			   /** 0x0A */
#define HID_KEY_H 11			   /** 0x0B */
#define HID_KEY_I 12			   /** 0x0C */
#define HID_KEY_J 13			   /** 0x0D */
#define HID_KEY_K 14			   /** 0x0E */
#define HID_KEY_L 15			   /** 0x0F */
#define HID_KEY_M 16			   /** 0x10 */
#define HID_KEY_N 17			   /** 0x11 */
#define HID_KEY_O 18			   /** 0x12 */
#define HID_KEY_P 19			   /** 0x13 */
#define HID_KEY_Q 20			   /** 0x14 */
#define HID_KEY_R 21			   /** 0x15 */
#define HID_KEY_S 22			   /** 0x16 */
#define HID_KEY_T 23			   /** 0x17 */
#define HID_KEY_U 24			   /** 0x18 */
#define HID_KEY_V 25			   /** 0x19 */
#define HID_KEY_W 26			   /** 0x1A */
#define HID_KEY_X 27			   /** 0x1B */
#define HID_KEY_Y 28			   /** 0x1C */
#define HID_KEY_Z 29			   /** 0x1D */
#define HID_KEY_1 30			   /** 0x1E, ! */
#define HID_KEY_2 31			   /** 0x1F, @ */
#define HID_KEY_3 32			   /** 0x20, # */
#define HID_KEY_4 33			   /** 0x21, $ */
#define HID_KEY_5 34			   /** 0x22, % */
#define HID_KEY_6 35			   /** 0x23, ^ */
#define HID_KEY_7 36			   /** 0x24, & */
#define HID_KEY_8 37			   /** 0x25, * */
#define HID_KEY_9 38			   /** 0x26, ( */
#define HID_KEY_0 39			   /** 0x27, ) */
#define HID_KEY_ENTER 40		   /** 0x28, Return */
#define HID_KEY_ESC 41			   /** 0x29, ESCAPE */
#define HID_KEY_BS 42			   /** 0x2A, DELETE */
#define HID_KEY_TAB 43			   /** 0x2B, TAB */
#define HID_KEY_SB 44			   /** 0x2C, SPACE BAR */
#define HID_KEY_MINUS 45		   /** 0x2D, - and _ */
#define HID_KEY_EQUAL 46		   /** 0x2E, = and + */
#define HID_KEY_O_BRCKT 47		   /** 0x2F, [ and { */
#define HID_KEY_C_BRCKT 48		   /** 0x30, ] and } */
#define HID_KEY_BSLASH 49		   /** 0x31, \ and | */
#define HID_KEY_HASHTILDE 50	   /** 0x32, Non-US # and ~ */
#define HID_KEY_COLON 51		   /** 0x33, ; and : */
#define HID_KEY_QUOTE 52		   /** 0x34, ' and " */
#define HID_KEY_TILDE 53		   /** 0x35, ` and ~ */
#define HID_KEY_COMMA 54		   /** 0x36, , and < */
#define HID_KEY_DOT 55			   /** 0x37, . and > */
#define HID_KEY_SLASH 56		   /** 0x38, / and ? */
#define HID_KEY_CAPS 57			   /** 0x39 */
#define HID_KEY_F1 58			   /** 0x3A */
#define HID_KEY_F2 59			   /** 0x3B */
#define HID_KEY_F3 60			   /** 0x3C */
#define HID_KEY_F4 61			   /** 0x3D */
#define HID_KEY_F5 62			   /** 0x3E */
#define HID_KEY_F6 63			   /** 0x3F */
#define HID_KEY_F7 64			   /** 0x40 */
#define HID_KEY_F8 65			   /** 0x41 */
#define HID_KEY_F9 66			   /** 0x42 */
#define HID_KEY_F10 67			   /** 0x43 */
#define HID_KEY_F11 68			   /** 0x44 */
#define HID_KEY_F12 69			   /** 0x45 */
#define HID_KEY_PRNTSC 70		   /** 0x46, Print Screen */
#define HID_KEY_SCROLL_LOCK 71	   /** 0x47, Scroll Lock */
#define HID_KEY_PAUSE 72		   /** 0x48, Pause */
#define HID_KEY_INSERT 73		   /** 0x49, Insert */
#define HID_KEY_HOME 74			   /** 0x4A, Home */
#define HID_KEY_PAGEUP 75		   /** 0x4B, Page Up */
#define HID_KEY_DELETE 76		   /** 0x4C, Delete Forward */
#define HID_KEY_END 77			   /** 0x4D, End */
#define HID_KEY_PAGEDOWN 78		   /** 0x4E, Page Down */
#define HID_KEY_RIGHT 79		   /** 0x4F, Right Arrow */
#define HID_KEY_LEFT 80			   /** 0x50, Left Arrow */
#define HID_KEY_DOWN 81			   /** 0x51, Down Arrow */
#define HID_KEY_UP 82			   /** 0x52, Up Arrow */
#define HID_KEY_KEYPAD_NUM_LOCK 83 /** 0x53, Num Lock */
#define HID_KEY_KEYPAD_DIVIDE 84   /** 0x54, Keypad / */
#define HID_KEY_KEYPAD_ASTER 85	   /** 0x55, Keypad * */
#define HID_KEY_KEYPAD_MINUS 86	   /** 0x56, Keypad - */
#define HID_KEY_KEYPAD_PLUS 87	   /** 0x57, Keypad + */
#define HID_KEY_KEYPAD_ENTER 88	   /** 0x58, Keypad ENTER */
#define HID_KEY_KEYPAD_1 89		   /** 0x59, Keypad 1 and End */
#define HID_KEY_KEYPAD_2 90		   /** 0x5A, Keypad 2 and Down Arrow */
#define HID_KEY_KEYPAD_3 91		   /** 0x5B, Keypad 3 and PageDn */
#define HID_KEY_KEYPAD_4 92		   /** 0x5C, Keypad 4 and Left Arrow */
#define HID_KEY_KEYPAD_5 93		   /** 0x5D, Keypad 5 */
#define HID_KEY_KEYPAD_6 94		   /** 0x5E, Keypad 6 and Right Arrow */
#define HID_KEY_KEYPAD_7 95		   /** 0x5F, Keypad 7 and Home */
#define HID_KEY_KEYPAD_8 96		   /** 0x60, Keypad 8 and Up Arrow */
#define HID_KEY_KEYPAD_9 97		   /** 0x61, Keypad 9 and Page Up */
#define HID_KEY_KEYPAD_0 98		   /** 0x62, Keypad 0 and Insert */
#define HID_KEY_KEYCODE_99 99	   /** 0x63, Keypad . and Delete */
#define HID_KEY_KEYCODE_100 100	   /** 0x64, Keyboard Non-US \ and |, UK \ GER <(다른나라도 동일)*/
#define HID_KEY_KEYCODE_101 101	   /** 0x65 */
#define HID_KEY_KEYCODE_102 102	   /** 0x66 */
#define HID_KEY_KEYCODE_103 103	   /** 0x67 */
#define HID_KEY_KEYCODE_104 104	   /** 0x68 */
#define HID_KEY_KEYCODE_105 105	   /** 0x69 */
#define HID_KEY_KEYCODE_106 106	   /** 0x6A */
#define HID_KEY_KEYCODE_107 107	   /** 0x6B */
#define HID_KEY_KEYCODE_108 108	   /** 0x6C */
#define HID_KEY_KEYCODE_109 109	   /** 0x6D */

#define HID_KEY_INTERNATIONAL1 135 /** 0x87 */ // 일본어 백슬래시 (시프트 왼쪽)
#define HID_KEY_INTERNATIONAL2 136 /** 0x88 */ // 일본어 히라가나 (스페이스 오른쪽오른쪽)
#define HID_KEY_INTERNATIONAL3 137 /** 0x89 */ // 일본어 엔화 (백스페이스 왼쪽)
#define HID_KEY_INTERNATIONAL4 138 /** 0x8A */ // 일본어 변환 (스페이스 오른쪽)
#define HID_KEY_INTERNATIONAL5 139 /** 0x8B */ // 일본어 무변환 (스페이스 왼쪽)
#define HID_KEY_INTERNATIONAL6 140			   /** 0x8C */
#define HID_KEY_INTERNATIONAL7 141			   /** 0x8D */
#define HID_KEY_INTERNATIONAL8 142			   /** 0x8E */
#define HID_KEY_INTERNATIONAL9 143			   /** 0x8F */

#define HID_KEY_MOKIBO 160	   /** 0xA0, MOKIBO */
#define HID_KEY_HOUSE 165	   /** 0xA5, HOME */
#define HID_KEY_BT1 166		   /** 0xA6, BT1 */
#define HID_KEY_BT2 167		   /** 0xA7, BT2 */
#define HID_KEY_BT3 168		   /** 0xA8, BT3 */
#define HID_KEY_DONGLE 169	   /** 0xA9, Dongle */
#define HID_KEY_BTRCHK 170	   /** 0xAA, Battery check */
#define HID_KEY_SWTAPP 171	   /** 0xAB, Switching App */
#define HID_KEY_VKBD 172	   /** 0xAC, Virtual Keyboard */
#define HID_KEY_FIND 173	   /** 0xAD, Find */
#define HID_KEY_V_UP 174	   /** 0xAE, Volume up */
#define HID_KEY_V_DOWN 175	   /** 0xAF, Volume down */
#define HID_KEY_ONOFF 176	   /** 0xB0, Mokibo On/Off */
#define HID_KEY_RESET 177	   /** 0xB1, HOTKEY */
#define HID_KEY_ERASEBOND 178  /** 0xB2, HOTKEY */
#define HID_KEY_LANGUAGE_Z 179 /** 0xB3, HOTKEY */
#define HID_KEY_LANGUAGE_X 180 /** 0xB4, HOTKEY */
#define HID_KEY_LANGUAGE_C 181 /** 0xB5, HOTKEY */
#define HID_KEY_TEST_ENTER 182 /** 0xB6, HOTKEY */
#define HID_KEY_CAPTURE 183	   /** 0xB7, HOTKEY */
#define HID_KEY_CERT_MODE 184  /** 0xB8, HOTKEY */
#define HID_KEY_CENTRAL_IOS 185
#define HID_KEY_BACKWARD 186   /** 0xBC, Backward */
#define HID_KEY_TOUCHLOCK 187  /** 0xBD */
#define HID_KEY_LOCKSCREEN 188 /** 0xBE */
#define HID_KEY_V_MUTE 189	   /** 0xBF */
#define HID_KEY_USB 190		   /** 0xC0 */
#define HID_KEY_LEFTED 191	   /** 0xC1 */
#define HID_KEY_RIGHTED 192	   /** 0xC2 */
#define HID_KEY_PRINTVER 193   /** 0xC3 */
#define HID_KEY_TOGGLE_MIC 194
#define HID_KEY_TOGGLE_CAM 195

#define HID_KEY_L_CTRL 224 /** 0xE0 */
#define HID_KEY_L_SHFT 225 /** 0xE1 */
#define HID_KEY_L_ALT 226  /** 0xE2 */
#define HID_KEY_L_GUI 227  /** 0xE3 */
#define HID_KEY_R_CTRL 228 /** 0xE4 */
#define HID_KEY_R_SHFT 229 /** 0xE5 */
#define HID_KEY_R_ALT 230  /** 0xE6 */
#define HID_KEY_R_GUI 231  /** 0xE7 */

#define HID_KEY_RED 247	 /** 0xF7 */
#define HID_KEY_MKB 248	 /** 0xF8 */
#define HID_KEY_FUNC 249 /** 0xF9 */

#define HID_MODIFIER_START HID_KEY_L_CTRL
#define HID_MODIFIER_END HID_KEY_R_GUI

#define HID_MODIFIER_L_CTRL 0x01 << (HID_KEY_L_CTRL - HID_MODIFIER_START)
#define HID_MODIFIER_L_SHFT 0x01 << (HID_KEY_L_SHFT - HID_MODIFIER_START)
#define HID_MODIFIER_L_ALT 0x01 << (HID_KEY_L_ALT - HID_MODIFIER_START)
#define HID_MODIFIER_L_GUI 0x01 << (HID_KEY_L_GUI - HID_MODIFIER_START)
#define HID_MODIFIER_R_CTRL 0x01 << (HID_KEY_R_CTRL - HID_MODIFIER_START)
#define HID_MODIFIER_R_SHFT 0x01 << (HID_KEY_R_SHFT - HID_MODIFIER_START)
#define HID_MODIFIER_R_ALT 0x01 << (HID_KEY_R_ALT - HID_MODIFIER_START)
#define HID_MODIFIER_R_GUI 0x01 << (HID_KEY_R_GUI - HID_MODIFIER_START)

// Currently maximum MKB_KEYBOARD_HOTKEY number is 32, bitfield of unsigned int
// When adding new HOT KEY
// 1. Increase DRV_KEYBOARD_HOTKEY_MAX
// 2. Add new string drv_keyboard_get_hotkey()
#define HOTK(NAME) (MKB_KEYBOARD_HOTKEY_##NAME)

#define MKB_KEYBOARD_HOTKEY_NONE 0		   //   HOTK(NONE)
#define MKB_KEYBOARD_HOTKEY_MOKIBO 1	   // 0:HOTK(MOKIBO) 			Fn + m
#define MKB_KEYBOARD_HOTKEY_CAPTURE 2	   // 1:HOTK(CAPTURE) 			Capture screen(Fn10자리)
#define MKB_KEYBOARD_HOTKEY_ERASE_BOND 3   // 2:HOTK(ERASE_BOND) 		Fn + 3
#define MKB_KEYBOARD_HOTKEY_HOUSE 4		   // 3:HOTK(HOUSE) 			Fn + Esc, HOME key와 구별하려 HOUSE 이름 사용
#define MKB_KEYBOARD_HOTKEY_BT1 5		   // 4:HOTK(BT1) 				BT1(F1자리)
#define MKB_KEYBOARD_HOTKEY_BT2 6		   // 5:HOTK(BT2) 				BT2(F2자리)
#define MKB_KEYBOARD_HOTKEY_BT3 7		   // 6:HOTK(BT3) 				BT3(F3자리)
#define MKB_KEYBOARD_HOTKEY_DONGLE 8	   // 7:HOTK(DONGLE) 			Dongle(F4자리)
#define MKB_KEYBOARD_HOTKEY_BTRCHK 9	   // 8:HOTK(BTRCHK) 			Battery check(F5자리)
#define MKB_KEYBOARD_HOTKEY_SWTAPP 10	   // 9:HOTK(SWTAPP) 			Switching App(F8자리)
#define MKB_KEYBOARD_HOTKEY_VKBD 11		   // 10:HOTK(VKBD) 			Virtual keyboard(F9자리)
#define MKB_KEYBOARD_HOTKEY_FIND 12		   // 11:HOTK(FIND) 			Search(F11자리)
#define MKB_KEYBOARD_HOTKEY_V_UP 13		   // 12:HOTK(V_UP) 			Volume up(F7자리)
#define MKB_KEYBOARD_HOTKEY_V_DOWN 14	   // 13:HOTK(V_DOWN) 			Volume down(F6자리)
#define MKB_KEYBOARD_HOTKEY_ONOFF 15	   // 14:HOTK(ONOFF) 			On/Off: Fn + Del
#define MKB_KEYBOARD_HOTKEY_LANGUAGE_Z 16  // 15
#define MKB_KEYBOARD_HOTKEY_LANGUAGE_X 17  // 16
#define MKB_KEYBOARD_HOTKEY_LANGUAGE_C 18  // 17
#define MKB_KEYBOARD_HOTKEY_TEST_ENTER 19  // 18:HOTK(TEST_ENTER) 		Fn + 7
#define MKB_KEYBOARD_HOTKEY_BT1_ADV 20	   // 19:HOTK(BT1_NEW) 			Long press BT1
#define MKB_KEYBOARD_HOTKEY_BT2_ADV 21	   // 20:HOTK(BT2_NEW) 			Long press BT2
#define MKB_KEYBOARD_HOTKEY_BT3_ADV 22	   // 21:HOTK(BT3_NEW) 			Long press BT3
#define MKB_KEYBOARD_HOTKEY_CERT_MODE 23   // 22:HOTK(CERT_MODE) 		Fn + 0  ...for certification testing ... repeat KBD+Mouse operatons
#define MKB_KEYBOARD_HOTKEY_CENTRAL_IOS 24 // 25:HOTK(CENTRAL_IOS)		Fn +
#define MKB_KEYBOARD_HOTKEY_BACKWARD 25	   // 26:HOTK(BACK_WARD)		Fn + Backspace
#define MKB_KEYBOARD_HOTKEY_LOCKSCREEN 26  // 27
#define MKB_KEYBOARD_HOTKEY_TOUCHLOCK 27   // 28
#define MKB_KEYBOARD_HOTKEY_V_MUTE 28	   // 29
#define MKB_KEYBOARD_HOTKEY_INSERT 29	   // 29
#define MKB_KEYBOARD_HOTKEY_DELETE 30	   // 29
#define MKB_KEYBOARD_HOTKEY_HOME 31		   // 29
#define MKB_KEYBOARD_HOTKEY_END 32		   // 29
#define MKB_KEYBOARD_HOTKEY_PAGEUP 33	   // 29
#define MKB_KEYBOARD_HOTKEY_PAGEDOWN 34	   // 29
#define MKB_KEYBOARD_HOTKEY_USB 35		   // 38:HOTK(USB) 			USB()
#define MKB_KEYBOARD_HOTKEY_USB_NEW 36	   // 39:HOTK(USB) 			USB()
#define MKB_KEYBOARD_HOTKEY_LEFTED 37	   // 39:HOTK(USB) 			USB()
#define MKB_KEYBOARD_HOTKEY_RIGHTED 38	   // 39:HOTK(USB) 			USB()
#define MKB_KEYBOARD_HOTKEY_PRINTVER 39	   // 39:HOTK(USB) 			USB()
#define MKB_KEYBOARD_HOTKEY_TOGGLE_MIC 40
#define MKB_KEYBOARD_HOTKEY_TOGGLE_CAM 41
#define MKB_KEYBOARD_HOTKEY_TYPE_MAX 42

#define EDGE(NAME) (NAME##_EDGE) //

#define N_EDGE 0x00					// none edge
#define P_EDGE 0x01					// push edge(release --> push)
#define R_EDGE 0x02					// release edge(push --> release)
#define LONG_KEY 0x04				// LONG PRESS KEY
#define A_EDGE (P_EDGE | R_EDGE)	//
#define RL_EDGE (R_EDGE | LONG_KEY) //
#define PL_EDGE (P_EDGE | LONG_KEY) //
#define L_EDGE (LONG_KEY)			//

#define M_TMR(NAME) (m_keyboard_timer_id_##NAME) // for short name

#define M_TIMER_TIME_MS(VAL) (VAL)			  // currently resolution is 1ms
#define M_TIMER_TIME_10MS(VAL) (VAL * 10)	  // currently resolution is 1ms
#define M_TIMER_TIME_100MS(VAL) (VAL * 100)	  // currently resolution is 1ms
#define M_TIMER_TIME_SECOND(VAL) (VAL * 1000) // currently resolution is 1ms

#define IMMEDIATELY M_TIMER_TIME_10MS(0)			 // Immediately
#define LONG_PRESS_TIME_BT M_TIMER_TIME_MS(600)		 // 3sec
#define LONG_PRESS_TIME_MOKIBO M_TIMER_TIME_MS(2000) // 5sec
#define LONG_PRESS_TIME_ONOFF M_TIMER_TIME_MS(2000)	 // 5sec

#define M_TIMEOUT(NAME) (LONG_PRESS_TIME_##NAME) // for short name

typedef struct
{
	uint8_t key_id;
	uint8_t key_hid;
	uint32_t down_timestamp;
} keyboard_state_t;

typedef struct
{
	uint8_t org; // HID KEY
	uint8_t new; // Remaped KEY: HID KEY + Function Key
} fn_key_t;

typedef struct
{
	uint8_t key; // hid key
	uint8_t attr;
	uint32_t hotkey;
	uint8_t long_attr;
	uint32_t long_hotkey;
	app_timer_id_t timer_id;
	uint32_t duration;
	bool timer_act; // timer started
	bool event_act; // event activated
} hot_key_t;

extern bool mkb_keyboard_startup_check(void);
extern ret_code_t mkb_keyboard_init(void);
extern ret_code_t mkb_keyboard_start(void);
extern ret_code_t mkb_keyboard_stop(void);
extern void keyboard_caps_lock_set(bool flag);
extern bool keyboard_caps_lock_get(void);
extern ret_code_t mkb_keyboard_cli(size_t size, char **params);
extern void send_version_string();
extern void send_battery_string();

#endif /* __MKB_KEYBOARD_H__ */
