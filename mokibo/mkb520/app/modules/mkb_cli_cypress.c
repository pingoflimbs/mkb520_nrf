/*
* ===============================================================================
* Copyright (C) 2017-2019
* INNOPRESSO INC. All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
* ===============================================================================
*/

#include <stdbool.h>
#include <stdint.h>

#include "sdk_common.h"

#include "nrf_assert.h"
#include "nrf_error.h"
#include "nrf_log.h"
#include "nrf_delay.h"
#include "nrf_cli.h"

#define MKB_LOG_LEVEL MKB_CLI_LOG_LEVEL
#include "mkb_common.h"
#include "mkb_cli.h"

#if (defined(MKB_CLI_ENABLED) && MKB_CLI_ENABLED)
#if (defined(MKB_CYPRESS_ENABLED) && MKB_CYPRESS_ENABLED)
#include "drv_cypress.h"

#define PARAGET_HELP "read a parameter\n"          \
                     "usage: cypress paraget id\n" \
                     "- id: Parameter ID, in HEX"
#define PARASET_HELP "write a parameter\n"                     \
                     "usage: cypress paraset id size value\n"  \
                     "- id   : Parameter ID, in HEX\n"         \
                     "- size : Parameter Data Size, [1|2|4]\n" \
                     "- value: Parameter Data, in HEX"

static void cmd_cypress_start(nrf_cli_t const *p_cli, size_t argc, char **argv)
{
    if (nrf_cli_help_requested(p_cli))
    {
        nrf_cli_help_print(p_cli, NULL, 0);
        return;
    }

    drv_cypress_enable();
}

static void cmd_cypress_stop(nrf_cli_t const *p_cli, size_t argc, char **argv)
{
    if (nrf_cli_help_requested(p_cli))
    {
        nrf_cli_help_print(p_cli, NULL, 0);
        return;
    }

    drv_cypress_disable();
}

static void cmd_cypress_sleep(nrf_cli_t const *p_cli, size_t argc, char **argv)
{
    if (nrf_cli_help_requested(p_cli))
    {
        nrf_cli_help_print(p_cli, NULL, 0);
        return;
    }

    drv_cypress_sleep();
}

static void cmd_cypress_wakeup(nrf_cli_t const *p_cli, size_t argc, char **argv)
{
    if (nrf_cli_help_requested(p_cli))
    {
        nrf_cli_help_print(p_cli, NULL, 0);
        return;
    }

    drv_cypress_wakeup();
}

static void cmd_cypress_reset(nrf_cli_t const *p_cli, size_t argc, char **argv)
{
    if (nrf_cli_help_requested(p_cli))
    {
        nrf_cli_help_print(p_cli, NULL, 0);
        return;
    }

    drv_cypress_reset();
}

static void cmd_cypress_system_information(nrf_cli_t const *p_cli, size_t argc, char **argv)
{
    if (nrf_cli_help_requested(p_cli))
    {
        nrf_cli_help_print(p_cli, NULL, 0);
        return;
    }

    cypress_system_information_t *p_data = drv_cypress_system_information_get();

    nrf_cli_fprintf(p_cli, NRF_CLI_DEFAULT, "PIP VER: %d.%d\n",
                    p_data->fields.pip_version_major, p_data->fields.pip_version_minor);
    nrf_cli_fprintf(p_cli, NRF_CLI_DEFAULT, "Product ID: %d(0x%04x)\n", p_data->fields.product_id, p_data->fields.product_id);
    nrf_cli_fprintf(p_cli, NRF_CLI_DEFAULT, "FW VER: %d.%d, Rev: %d\n",
                    p_data->fields.fw_version_major, p_data->fields.fw_version_minor, p_data->fields.revision);
    nrf_cli_fprintf(p_cli, NRF_CLI_DEFAULT, "Bootloader VER: %d.%d\n",
                    p_data->fields.bootloader_major, p_data->fields.bootloader_minor);
    nrf_cli_fprintf(p_cli, NRF_CLI_DEFAULT, "Family ID: %d(0x%02x)\n",
                    p_data->fields.family_id, p_data->fields.family_id);
    nrf_cli_fprintf(p_cli, NRF_CLI_DEFAULT, "Silicon ID: %d(0x%04x), Revision ID: %d(0x%02x)\n",
                    p_data->fields.silicon_id, p_data->fields.silicon_id, p_data->fields.revision_id, p_data->fields.revision_id);
    nrf_cli_fprintf(p_cli, NRF_CLI_DEFAULT, "LEN x: %d, y: %d, Res x: %d, y: %d\n",
                    p_data->fields.len_x, p_data->fields.len_y,
                    p_data->fields.res_x, p_data->fields.res_y);
}

static void cmd_cypress_parameter_get(nrf_cli_t const *p_cli, size_t argc, char **argv)
{
    if (nrf_cli_help_requested(p_cli))
    {
        nrf_cli_help_print(p_cli, NULL, 0);
        return;
    }

    uint8_t id = strtol(argv[1], NULL, 16);
    cypress_data_block_format_t data;
    uint8_t size;

    if (drv_cypress_get_parameter(id, data.data, &size) == NRF_SUCCESS)
    {
        switch (size)
        {
        case 1:
            nrf_cli_fprintf(p_cli, NRF_CLI_DEFAULT, "PARAMETER ID: 0x%02x, size: %d, Data: 0x%02x\n",
                            id, size, data.byte1);
            break;
        case 2:
            nrf_cli_fprintf(p_cli, NRF_CLI_DEFAULT, "PARAMETER ID: 0x%02x, size: %d, Data: 0x%04x\n",
                            id, size, data.byte2);
            break;
        case 4:
            nrf_cli_fprintf(p_cli, NRF_CLI_DEFAULT, "PARAMETER ID: 0x%02x, size: %d, Data: 0x%08x\n",
                            id, size, data.byte4);
            break;
        }
    }
}

static void cmd_cypress_parameter_set(nrf_cli_t const *p_cli, size_t argc, char **argv)
{
    if (nrf_cli_help_requested(p_cli))
    {
        nrf_cli_help_print(p_cli, NULL, 0);
        return;
    }

    uint8_t id = strtol(argv[1], NULL, 16);
    cypress_data_block_format_t data;
    uint8_t size = strtol(argv[2], NULL, 10);

    switch (size)
    {
    case 1:
        data.byte1 = strtol(argv[3], NULL, 16);
        break;
    case 2:
        data.byte2 = strtol(argv[3], NULL, 16);
        break;
    case 4:
        data.byte4 = strtol(argv[3], NULL, 16);
        break;
    }

    if (drv_cypress_set_parameter(id, data.data, size) == NRF_SUCCESS)
    {
        nrf_cli_fprintf(p_cli, NRF_CLI_DEFAULT, "PARAMETER Write Success!\n");
    }
}

static void cmd_cypress_config_read(nrf_cli_t const *p_cli, size_t argc, char **argv)
{
    if (nrf_cli_help_requested(p_cli))
    {
        nrf_cli_help_print(p_cli, NULL, 0);
        return;
    }

    drv_cypress_config_read();
}

#if (defined(MKB_CYPRESS_FW_INCLUDED) && MKB_CYPRESS_FW_INCLUDED)
static void cmd_cypress_firmware_update(nrf_cli_t const *p_cli, size_t argc, char **argv)
{
    if (nrf_cli_help_requested(p_cli))
    {
        nrf_cli_help_print(p_cli, NULL, 0);
        return;
    }

    drv_cypress_update();
}
#endif //(defined(MKB_CYPRESS_FW_INCLUDED) && MKB_CYPRESS_FW_INCLUDED)

static void cmd_cypress(nrf_cli_t const *p_cli, size_t argc, char **argv)
{
    UNUSED_PARAMETER(argc);
    UNUSED_PARAMETER(argv);

    if ((argc == 1) || nrf_cli_help_requested(p_cli))
    {
        nrf_cli_help_print(p_cli, NULL, 0);
        return;
    }

    nrf_cli_print(p_cli, "                Nordic Semiconductor              \n");
    nrf_cli_print(p_cli, "Nice cypress ;)");
}

NRF_CLI_CREATE_STATIC_SUBCMD_SET(m_sub_cypress)
{
    NRF_CLI_CMD(reset, NULL, "Reset cypress sensor.", cmd_cypress_reset),
        NRF_CLI_CMD(start, NULL, "Start cypress scanning.", cmd_cypress_start),
        NRF_CLI_CMD(stop, NULL, "Stop cypress scanning.", cmd_cypress_stop),
        NRF_CLI_CMD(sleep, NULL, "Sleep cypress.", cmd_cypress_sleep),
        NRF_CLI_CMD(wakeup, NULL, "Wakeup cypress.", cmd_cypress_wakeup),
        NRF_CLI_CMD(sysinfo, NULL, "Read system information.", cmd_cypress_system_information),
        NRF_CLI_CMD(paraget, NULL, PARAGET_HELP, cmd_cypress_parameter_get),
        NRF_CLI_CMD(paraset, NULL, PARASET_HELP, cmd_cypress_parameter_set),
        NRF_CLI_CMD(cfgread, NULL, "Read Config Data Block.", cmd_cypress_config_read),
#if (defined(MKB_CYPRESS_FW_INCLUDED) && MKB_CYPRESS_FW_INCLUDED)
        NRF_CLI_CMD(update, NULL, "update firmware", cmd_cypress_firmware_update),
#endif //(defined(MKB_CYPRESS_FW_INCLUDED) && MKB_CYPRESS_FW_INCLUDED)
        NRF_CLI_SUBCMD_SET_END
};
NRF_CLI_CMD_REGISTER(cypress, &m_sub_cypress, "cypress touch sensor command", cmd_cypress);

#endif // (defined(MKB_CYPRESS_ENABLED) && MKB_CYPRESS_ENABLED)
#endif // (defined(MKB_CLI_ENABLED) && MKB_CLI_ENABLED)
