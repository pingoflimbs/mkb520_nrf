/*
* ===============================================================================
* Copyright (C) 2017-2019
* INNOPRESSO INC. All Rights Reserved
*
* The content of this file is copyrighted by INNOPRESSO or its
* affiliated, associated or related companies. All copyrights and other
* associated intellectual property rights related thereto are expressly
* reserved by INNOPRESSO INC.
* ===============================================================================
*/

#ifndef __MKB_FDS__
#define __MKB_FDS__

#include "mkb_common.h"

/* File ID range 0x0000 - 0xBFFF */
#define MKB_FDS_FILE_ID_MOKIBO 0x5000

/* Record ID range 0x0001 - 0xBFFF */
#define MKB_FDS_RECORD_ID_CONFIG 0x5000
#define MKB_FDS_RECORD_ID_CHANNEL 0x5001

extern ret_code_t mkb_fds_init(void);
extern const char *mkb_fds_err_str(ret_code_t ret);
extern ret_code_t mkb_fds_config_write(void *p_data, uint32_t length);
extern ret_code_t mkb_fds_config_update(void *p_data, uint32_t length);
extern ret_code_t mkb_fds_config_read(void *p_data, uint32_t length);
extern ret_code_t mkb_fds_config_find(void);
extern ret_code_t mkb_fds_channel_write(void *p_data, uint32_t length);
extern ret_code_t mkb_fds_channel_update(void *p_data, uint32_t length);
extern ret_code_t mkb_fds_channel_read(void *p_data, uint32_t length);
extern ret_code_t mkb_fds_channel_find(void);

#endif /* __MKB_FDS__ */
