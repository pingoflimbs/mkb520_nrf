/*
 * ===============================================================================
 * Copyright (C) 2017-2019
 * INNOPRESSO INC. All Rights Reserved
 *
 * The content of this file is copyrighted by INNOPRESSO or its
 * affiliated, associated or related companies. All copyrights and other
 * associated intellectual property rights related thereto are expressly
 * reserved by INNOPRESSO INC.
 * ===============================================================================
 */

#include <stdbool.h>
#include <stdint.h>
#include <stdarg.h>

#include "sdk_common.h"

#include "nrf_assert.h"
#include "nrf_error.h"
#include "nrf_delay.h"
#include "nrf_cli.h"
#include "nrf_balloc.h"
#include "nrf_pwr_mgmt.h"
#include "nrf_drv_pwm.h"

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

#include "app_error.h"
#include "app_timer.h"
#include "app_scheduler.h"
//#include "app_pwm.h"
#include "bsp.h"
#include "boards.h"

#define MKB_LOG_LEVEL MKB_LED_LOG_LEVEL
#include "mkb_common.h"

#include "mkb_event.h"

#if (defined(MKB_LED_ENABLED) && MKB_LED_ENABLED)
#include "mkb_led.h"
#endif

APP_TIMER_DEF(m_led_ch_timer);
APP_TIMER_DEF(m_led_pwr_timer);

#define LED_CH_1 0
#define LED_CH_2 1
#define LED_CH_3 2
#define LED_CH_PWR 3
#define LED_CH_MAX 4

static void mkb_led_ind_layer_stop(MKB_LED_NUM_t numb);
static void mkb_led_ind_layer_start(MKB_LED_NUM_t numb, MKB_LED_CLR_t color);

typedef struct
{
    nrf_drv_pwm_t *p_pwm;
    nrf_pwm_sequence_t *p_seq;                 // power of led
    nrf_pwm_values_individual_t *p_seq_values; // seq values. 컬러값을 들고있는 포인터
} led_pwm_t;

typedef struct
{
    uint8_t red;
    uint8_t green;
    uint8_t blue;
} led_color_t;

typedef struct
{
    MKB_LED_ACT_t act;
    uint16_t progress;
    uint16_t nextdelay;
    led_color_t color;
    uint8_t brightness;
} led_state_t;

typedef struct
{
    led_pwm_t pwm;
    led_state_t state_main;
    led_state_t state_sub;
    MKB_LED_LAYER_t working_layer;
    app_timer_id_t *timer;
} led_t;

static led_t m_leds[LED_CH_MAX];

// CH LED SETTING
static nrf_drv_pwm_t m_pwm1 = NRF_DRV_PWM_INSTANCE(0);
static nrf_drv_pwm_t m_pwm2 = NRF_DRV_PWM_INSTANCE(1);
static nrf_drv_pwm_t m_pwm3 = NRF_DRV_PWM_INSTANCE(2);
static nrf_drv_pwm_t m_pwm4 = NRF_DRV_PWM_INSTANCE(3);

static nrf_pwm_values_individual_t m_led1_seq_values;
static nrf_pwm_values_individual_t m_led2_seq_values;
static nrf_pwm_values_individual_t m_led3_seq_values;
static nrf_pwm_values_individual_t m_led_pwr_seq_values;

static nrf_pwm_sequence_t const m_led1_seq =
    {
        .values.p_individual = &m_led1_seq_values,
        .length = NRF_PWM_VALUES_LENGTH(m_led1_seq_values),
        .repeats = 0,
        .end_delay = 0};

static nrf_pwm_sequence_t const m_led2_seq =
    {
        .values.p_individual = &m_led2_seq_values,
        .length = NRF_PWM_VALUES_LENGTH(m_led2_seq_values),
        .repeats = 0,
        .end_delay = 0};

static nrf_pwm_sequence_t const m_led3_seq =
    {
        .values.p_individual = &m_led3_seq_values,
        .length = NRF_PWM_VALUES_LENGTH(m_led3_seq_values),
        .repeats = 0,
        .end_delay = 0};

static nrf_pwm_sequence_t const m_led_pwr_seq =
    {
        .values.p_individual = &m_led_pwr_seq_values,
        .length = NRF_PWM_VALUES_LENGTH(m_led_pwr_seq_values),
        .repeats = 0,
        .end_delay = 0};

nrf_drv_pwm_config_t const led1_cfg =
    {
        .output_pins =
            {
                LED_RGB_PIN_1_RED | NRF_DRV_PWM_PIN_INVERTED,
                LED_RGB_PIN_1_GREEN | NRF_DRV_PWM_PIN_INVERTED,
                LED_RGB_PIN_1_BLUE | NRF_DRV_PWM_PIN_INVERTED,
            },
        .irq_priority = APP_IRQ_PRIORITY_LOWEST,
        .base_clock = NRF_PWM_CLK_1MHz,
        .count_mode = NRF_PWM_MODE_UP,
        .top_value = 100,
        .load_mode = NRF_PWM_LOAD_INDIVIDUAL,
        .step_mode = NRF_PWM_STEP_AUTO};

nrf_drv_pwm_config_t const led2_cfg =
    {
        .output_pins =
            {
                LED_RGB_PIN_2_RED | NRF_DRV_PWM_PIN_INVERTED,
                LED_RGB_PIN_2_GREEN | NRF_DRV_PWM_PIN_INVERTED,
                LED_RGB_PIN_2_BLUE | NRF_DRV_PWM_PIN_INVERTED,
            },
        .irq_priority = APP_IRQ_PRIORITY_LOWEST,
        .base_clock = NRF_PWM_CLK_1MHz,
        .count_mode = NRF_PWM_MODE_UP,
        .top_value = 100,
        .load_mode = NRF_PWM_LOAD_INDIVIDUAL,
        .step_mode = NRF_PWM_STEP_AUTO};

nrf_drv_pwm_config_t const led3_cfg =
    {
        .output_pins =
            {
                LED_RGB_PIN_3_RED | NRF_DRV_PWM_PIN_INVERTED,
                LED_RGB_PIN_3_GREEN | NRF_DRV_PWM_PIN_INVERTED,
                LED_RGB_PIN_3_BLUE | NRF_DRV_PWM_PIN_INVERTED,
            },
        .irq_priority = APP_IRQ_PRIORITY_LOWEST,
        .base_clock = NRF_PWM_CLK_1MHz,
        .count_mode = NRF_PWM_MODE_UP,
        .top_value = 100,
        .load_mode = NRF_PWM_LOAD_INDIVIDUAL,
        .step_mode = NRF_PWM_STEP_AUTO};

nrf_drv_pwm_config_t const led_pwr_cfg =
    {
        .output_pins =
            {
                LED_RGB_PIN_4_RED | NRF_DRV_PWM_PIN_INVERTED,
                LED_RGB_PIN_4_GREEN | NRF_DRV_PWM_PIN_INVERTED,
                LED_RGB_PIN_4_BLUE | NRF_DRV_PWM_PIN_INVERTED,
            },
        .irq_priority = APP_IRQ_PRIORITY_LOWEST,
        .base_clock = NRF_PWM_CLK_1MHz,
        .count_mode = NRF_PWM_MODE_UP,
        .top_value = 100,
        .load_mode = NRF_PWM_LOAD_INDIVIDUAL,
        .step_mode = NRF_PWM_STEP_AUTO};

static mkb_led_clear_state(led_t *p_led)
{
    app_timer_stop(*(p_led->timer));
    p_led->working_layer = MKB_LED_LAYER_BASE;

    p_led->state_main.act = MKB_LED_ACT_OFF;
    p_led->state_main.progress = 0;
    p_led->state_main.nextdelay = 10;
    p_led->state_main.color.red = 0;
    p_led->state_main.color.blue = 0;
    p_led->state_main.color.green = 0;
    p_led->state_main.brightness = 0;

    p_led->state_sub.act = MKB_LED_ACT_OFF;
    p_led->state_sub.progress = 0;
    p_led->state_sub.nextdelay = 10;
    p_led->state_sub.color.red = 0;
    p_led->state_sub.color.blue = 0;
    p_led->state_sub.color.green = 0;
    p_led->state_sub.brightness = 0;

    p_led->pwm.p_seq_values->channel_0 = 0;
    p_led->pwm.p_seq_values->channel_1 = 0;
    p_led->pwm.p_seq_values->channel_2 = 0;
}

static void mkb_led_ind_pwr_off()
{
    ret_code_t err_code;
    mkb_led_clear_state(&m_leds[LED_CH_PWR]);
}

static void mkb_led_ind_ch_off()
{
    ret_code_t err_code;
    mkb_led_clear_state(&m_leds[LED_CH_1]);
    mkb_led_clear_state(&m_leds[LED_CH_2]);
    mkb_led_clear_state(&m_leds[LED_CH_3]);
}

static void mkb_led_ind_on(uint8_t brightness)
{
}

void mkb_led_ind_enable(void)
{
    nrf_drv_pwm_simple_playback(m_leds[LED_CH_1].pwm.p_pwm, m_leds[LED_CH_1].pwm.p_seq, 1, NRF_DRV_PWM_FLAG_LOOP);
    nrf_drv_pwm_simple_playback(m_leds[LED_CH_2].pwm.p_pwm, m_leds[LED_CH_2].pwm.p_seq, 1, NRF_DRV_PWM_FLAG_LOOP);
    nrf_drv_pwm_simple_playback(m_leds[LED_CH_3].pwm.p_pwm, m_leds[LED_CH_3].pwm.p_seq, 1, NRF_DRV_PWM_FLAG_LOOP);
    nrf_drv_pwm_simple_playback(m_leds[LED_CH_PWR].pwm.p_pwm, m_leds[LED_CH_PWR].pwm.p_seq, 1, NRF_DRV_PWM_FLAG_LOOP);
}

void mkb_led_ind_disable()
{
    nrf_drv_pwm_stop(m_leds[LED_CH_1].pwm.p_pwm, true);
    nrf_drv_pwm_stop(m_leds[LED_CH_2].pwm.p_pwm, true);
    nrf_drv_pwm_stop(m_leds[LED_CH_3].pwm.p_pwm, true);
    nrf_drv_pwm_stop(m_leds[LED_CH_PWR].pwm.p_pwm, true);
}

static led_color_t mkb_led_color_get(MKB_LED_CLR_t color, led_color_t *return_color)
{ //개선필요.
    switch (color)
    {
    case MKB_LED_CLR_NONE:
        return_color->red = 0;
        return_color->green = 0;
        return_color->blue = 0;
        break;

    case MKB_LED_CLR_MAGENTA:
        return_color->red = 100;
        return_color->green = 50;
        return_color->blue = 50;
        break;
    case MKB_LED_CLR_RED:
        return_color->red = 23;
        return_color->green = 1;
        return_color->blue = 0;
        break;
    case MKB_LED_CLR_GREEN:
        return_color->red = 0;
        return_color->green = 23;
        return_color->blue = 3;
        break;
    case MKB_LED_CLR_BLUE:
        return_color->red = 0;
        return_color->green = 0;
        return_color->blue = 100;
        break;
    case MKB_LED_CLR_WHITE:
        return_color->red = 18;
        return_color->green = 18;
        return_color->blue = 24;
        break;
    case MKB_LED_CLR_CYAN:
        return_color->red = 0;
        return_color->green = 100;
        return_color->blue = 100;
        break;
    case MKB_LED_CLR_YELLOW:
        return_color->red = 40;
        return_color->green = 35;
        return_color->blue = 0;
        break;
    case MKB_LED_CLR_IOS:
        return_color->red = 25;
        return_color->green = 25;
        return_color->blue = 25;
        break;
    case MKB_LED_CLR_ANDROD:
        return_color->red = 7;
        return_color->green = 25;
        return_color->blue = 0;
        break;
    case MKB_LED_CLR_WINDOWS:
        return_color->red = 0;
        return_color->green = 3;
        return_color->blue = 65;
        break;
    case MKB_LED_CLR_DEX:
        return_color->red = 30;
        return_color->green = 8;
        return_color->blue = 0;
        break;
    case MKB_LED_CLR_MAC:
        return_color->red = 6;
        return_color->green = 20;
        return_color->blue = 40;
        break;
    }
}

static MKB_LED_CLR_t mkb_led_color_get_central(CENTRAL_TYPE_t central)
{
    switch (central)
    {
    case CENTRAL_ANDROID:
        return MKB_LED_CLR_ANDROD;
        break;
    case CENTRAL_DEX:
        return MKB_LED_CLR_DEX;
        break;
    case CENTRAL_IOS:
        return MKB_LED_CLR_IOS;
        break;
    case CENTRAL_LINUX:
        return MKB_LED_CLR_WINDOWS;
        break;
    case CENTRAL_MAC:
        return MKB_LED_CLR_MAC;
        break;
    case CENTRAL_WINDOWS:
        return MKB_LED_CLR_WINDOWS;
        break;
    case CENTRAL_OTHER:
        return MKB_LED_CLR_WINDOWS;
        break;
    default:
        return MKB_LED_CLR_YELLOW;
        break;
    }
}

void mkb_led_ind_os_test(uint8_t channel, uint8_t val)
{
    switch (val)
    {
    case 0: // mac
        m_leds[channel].pwm.p_seq_values->channel_0 = 40;
        m_leds[channel].pwm.p_seq_values->channel_1 = 00;
        m_leds[channel].pwm.p_seq_values->channel_2 = 100;
        break;
    case 1: // ios
        m_leds[channel].pwm.p_seq_values->channel_0 = 100;
        m_leds[channel].pwm.p_seq_values->channel_1 = 100;
        m_leds[channel].pwm.p_seq_values->channel_2 = 100;
        break;
    case 2: // windows
        m_leds[channel].pwm.p_seq_values->channel_0 = 0;
        m_leds[channel].pwm.p_seq_values->channel_1 = 0;
        m_leds[channel].pwm.p_seq_values->channel_2 = 100;
        break;
    case 3: // linux
        m_leds[channel].pwm.p_seq_values->channel_0 = 100;
        m_leds[channel].pwm.p_seq_values->channel_1 = 0;
        m_leds[channel].pwm.p_seq_values->channel_2 = 0;
        break;
    case 4: // android
        m_leds[channel].pwm.p_seq_values->channel_0 = 0;
        m_leds[channel].pwm.p_seq_values->channel_1 = 100;
        m_leds[channel].pwm.p_seq_values->channel_2 = 0;
        break;
    case 5: // low
        m_leds[channel].pwm.p_seq_values->channel_0 = 100;
        m_leds[channel].pwm.p_seq_values->channel_1 = 0;
        m_leds[channel].pwm.p_seq_values->channel_2 = 0;
        break;
    case 6: // mid
        m_leds[channel].pwm.p_seq_values->channel_0 = 50;
        m_leds[channel].pwm.p_seq_values->channel_1 = 50;
        m_leds[channel].pwm.p_seq_values->channel_2 = 0;
        break;
    case 7: // high
        m_leds[channel].pwm.p_seq_values->channel_0 = 20;
        m_leds[channel].pwm.p_seq_values->channel_1 = 60;
        m_leds[channel].pwm.p_seq_values->channel_2 = 0;
        break;
    case 8: // full
        m_leds[channel].pwm.p_seq_values->channel_0 = 0;
        m_leds[channel].pwm.p_seq_values->channel_1 = 100;
        m_leds[channel].pwm.p_seq_values->channel_2 = 0;
        break;
    }
}

static mkb_led_process(led_t *p_led)
{
    ret_code_t err_code;
    led_state_t *state;
    state = &(p_led->state_main);
    static bool toggle_flag;

    switch (state->act)
    {
    case MKB_LED_ACT_OFF:

        if (p_led->state_main.progress < 100) // process
        {
            p_led->state_main.nextdelay = 100;
            p_led->state_main.brightness = 0;
            state->progress = 100;
        }
        else
        {
        }
        break;
    case MKB_LED_ACT_ON_PUSH_DOWN:
        p_led->state_main.nextdelay = 100;

        //________DBG_20211014_led_event("bat:%d, fn:%d  ", mkb_db_is_bat_charging_get(), mkb_db_is_func_pressed_get());
        if (mkb_db_is_bat_charging_get() || mkb_db_is_func_pressed_get())
        {
            state->progress = 0;
            state->brightness = 100;
            ________DBG_20211205_battery_test("fn or battery detected charg:%d, fn:%d", mkb_db_is_bat_charging_get(), mkb_db_is_func_pressed_get());
            //________DBG_20211014_led_event("fn or battery detected");
        }
        else
        {
            ________DBG_20211205_battery_test("fn or battery not detcted");
            if (state->progress < 100)
            {
                state->progress += 100 / 4;
                state->brightness = (100 - state->progress);
            }
            else
            {
                state->brightness = 0;
            }
            //________DBG_20211014_led_event("fn or power detach");
        }

        err_code = app_timer_start(*(p_led->timer), APP_TIMER_TICKS(state->nextdelay), p_led);
        break;
    case MKB_LED_ACT_ON_50S:
        if (true) //(p_led->state_main.progress < 100) // process
        {
            p_led->state_main.nextdelay = 1000;
            if (mkb_db_is_bat_charging_get())
            {
                ________DBG_20211205_battery_test("battery charge get 1");

                state->progress = 0;
                state->brightness = 100;
                //________DBG_20211014_led_event("power detected");
            }
            else
            {
                if (state->progress < 100)
                {
                    state->progress += 100 / 50;
                    state->brightness = (100 - state->progress);
                }
                else
                {
                    state->brightness = 0;
                }
                //________DBG_20211014_led_event("power detach");
            }
            err_code = app_timer_start(*(p_led->timer), APP_TIMER_TICKS(state->nextdelay), p_led);
        }
        else // end progress
        {
            // mkb_led_clear_state(p_led);
        }
        break;
    case MKB_LED_ACT_SLOW_BLINK:
        if (p_led->state_main.progress < 100) // process
        {
            state->nextdelay = 1000;
            state->progress += 100 / 32;
            state->brightness = (toggle_flag)*100;

            err_code = app_timer_start(*(p_led->timer), APP_TIMER_TICKS(state->nextdelay), p_led);
        }
        else // end progress
        {
            mkb_led_clear_state(p_led);
        }
        break;
    case MKB_LED_ACT_FAST_BLINK:
        if (p_led->state_main.progress < 100) // process
        {
            state->nextdelay = 200;
            state->progress += 100 / 100;
            state->brightness = (toggle_flag)*100;

            err_code = app_timer_start(*(p_led->timer), APP_TIMER_TICKS(state->nextdelay), p_led);
        }
        else // end progress
        {
            mkb_led_clear_state(p_led);
        }
        break;
    case MKB_LED_ACT_DIMMING_ON:
        if (p_led->state_main.progress < 100) // process
        {
            state->nextdelay = 50;
            state->progress += 100 / 10;
            state->brightness = state->progress;

            err_code = app_timer_start(*(p_led->timer), APP_TIMER_TICKS(state->nextdelay), p_led);
        }
        else
        {
            mkb_led_clear_state(p_led);
        }
        break;
    case MKB_LED_ACT_DIMMING_OFF:
        if (p_led->state_main.progress < 100) // process
        {
            state->nextdelay = 50;
            state->progress += 100 / 10;
            state->brightness = (100 - state->progress);
            err_code = app_timer_start(*(p_led->timer), APP_TIMER_TICKS(state->nextdelay), p_led);
        }
        else
        {
            mkb_led_clear_state(p_led);
        }
        break;
    }

    toggle_flag = !toggle_flag;
    if (p_led->working_layer == MKB_LED_LAYER_BASE)
    {
        p_led->pwm.p_seq_values->channel_0 = state->color.red * (state->brightness / 100.0);
        p_led->pwm.p_seq_values->channel_1 = state->color.green * (state->brightness / 100.0);
        p_led->pwm.p_seq_values->channel_2 = state->color.blue * (state->brightness / 100.0);
    }
}

static mkb_led_ch_timer_handler(led_t *p_led)
{
    mkb_led_process(p_led);
}

static mkb_led_pwr_timer_handler(led_t *p_led)
{
    mkb_led_process(p_led);
}

void mkb_led_indication(MKB_LED_NUM_t numb, MKB_LED_CLR_t color, MKB_LED_ACT_t act)
{ // mkb_led_process로 옮겨야함.
    ________DBG_20210915_led_callstack_2("indication numb:%d act:%d", numb, act);

    ret_code_t err_code;
    mkb_led_clear_state(&m_leds[numb]);
    m_leds[numb].state_main.act = act;
    mkb_led_color_get(color, &(m_leds[numb].state_main.color));

    err_code = app_timer_start(*(m_leds[numb].timer), APP_TIMER_TICKS(m_leds[numb].state_main.nextdelay), &(m_leds[numb]));
}

static void mkb_led_chnl_event_handler(mkb_event_t *p_event)
{

    mkb_event_type_t event_type = p_event->type;
    MKB_LED_NUM_t num = mkb_db_ch_id_get();
    MKB_LED_CLR_t color = mkb_led_color_get_central(mkb_db_central_type_get(num));
    MKB_LED_ACT_t act = p_event->led.act;

    switch (event_type)
    {
    case EVT_LED_CH_IDLE:
    {
        ________DBG_20211014_led_event("EVT_LED_CH_IDLE(event_type:%d, num:%d, clr:%d, act:%d)", event_type, num, color, act);
        mkb_led_ind_ch_off();
    }
    break;
    case EVT_LED_CH_NO_ADDR:
    {
        ________DBG_20211014_led_event("EVT_LED_CH_NO_ADDR(event_type:%d, num:%d, clr:%d, act:%d)", event_type, num, color, act);
        mkb_led_ind_ch_off();
        mkb_led_indication(num, color, MKB_LED_ACT_DIMMING_OFF);
    }
    break;
    case EVT_LED_CH_ADV_FAST:
    {
        ________DBG_20211014_led_event("EVT_LED_CH_ADV_FAST(event_type:%d, num:%d, clr:%d, act:%d)", event_type, num, color, act);
        mkb_led_ind_ch_off();
        mkb_led_indication(num, color, MKB_LED_ACT_FAST_BLINK);
    }
    break;
    case EVT_LED_CH_ADV_SLOW:
    {
        ________DBG_20211014_led_event("EVT_LED_CH_ADV_SLOW(event_type:%d, num:%d, clr:%d, act:%d)", event_type, num, color, act);
        mkb_led_ind_ch_off();
        mkb_led_indication(num, color, MKB_LED_ACT_SLOW_BLINK);
    }
    break;
    case EVT_LED_CH_CONNECT:
    {
        ________DBG_20211014_led_event("EVT_LED_CH_CONNECT(event_type:%d, num:%d, clr:%d, act:%d)", event_type, num, color, act);
        mkb_led_ind_ch_off();
        mkb_led_indication(num, color, MKB_LED_ACT_ON_50S);
    }
    break;
    case EVT_LED_CH_DISCONNECT:
    {
        ________DBG_20211014_led_event("chnl_event_handler disconnect");
        mkb_led_ind_ch_off();
        mkb_led_indication(num, color, MKB_LED_ACT_DIMMING_OFF);
    }
    break;
    default:
        break;
    }
}

static void mkb_led_pwr_event_handler(mkb_event_t *p_event)
{

    mkb_event_type_t event_type = p_event->type;
    MKB_LED_NUM_t num = MKB_LED_NUM_PWR;
    uint8_t bat = mkb_db_battery_level_get();
    if (bat > 100)
    {
    }
    MKB_LED_ACT_t act = p_event->led.act;

    switch (event_type)
    {
    case EVT_LED_PWR_IDLE:
    {
        mkb_led_indication(num, MKB_LED_CLR_BLUE, MKB_LED_ACT_DIMMING_OFF);
    }
    break;
    case EVT_LED_PWR_CHARGE:
    {
    }
    break;
    case EVT_LED_PWR_DISCHARGE:
    {
    }
    break;
    case EVT_LED_PWR_ALERT:
    {
    }
    break;
    }
    ________DBG_20211014_led_event("chnl_event_handler on");
}

static void mkb_led_sub_event_handler(mkb_event_t *p_event)
{
    mkb_event_type_t event_type = p_event->type;

    switch (event_type)
    {
    case EVT_LED_SUB_SET_CH_OS_CHECK_PUSH_DOWN:
    {
        ________DBG_20211014_channel_os("os");
        mkb_led_ind_ch_off();
        MKB_LED_NUM_t num = mkb_db_ch_id_get();
        MKB_LED_CLR_t color = mkb_led_color_get_central(mkb_db_central_type_get(num));
        MKB_LED_ACT_t act = p_event->led.act;
        // mkb_led_ind_layer_start(num, color);
        mkb_led_indication(num, color, MKB_LED_ACT_ON_PUSH_DOWN);
    }
    break;
    case EVT_LED_SUB_SET_CH_OS_CHECK_PUSH_UP:
    {
        MKB_LED_NUM_t num = mkb_db_ch_id_get();
        // mkb_led_ind_layer_stop(num);
    }
    break;
    case EVT_LED_SUB_BATT_CHECK:
    {

        MKB_LED_ACT_t act = p_event->led.act;

        static MKB_LED_CLR_t clr = MKB_LED_CLR_YELLOW;
        MKB_LED_NUM_t num = MKB_LED_NUM_PWR;

        ________DBG_20211205_battery_test("level =%d", mkb_db_battery_level_get());

        // |-----red-----| |-----yel-----| |-----grn-----|
        if ((clr == MKB_LED_CLR_GREEN && mkb_db_battery_level_get() <= 30) || (clr == MKB_LED_CLR_YELLOW && mkb_db_battery_level_get() <= 27) || (clr == MKB_LED_CLR_RED && mkb_db_battery_level_get() <= 33))
        { //30~80
            mkb_led_ind_pwr_off();
            clr = MKB_LED_CLR_RED;
            mkb_led_indication(num, clr, MKB_LED_ACT_ON_PUSH_DOWN);
            ________DBG_20211205_battery_test("red,");
        }
        else if ((clr == MKB_LED_CLR_GREEN && mkb_db_battery_level_get() <= 77) || (clr == MKB_LED_CLR_YELLOW && mkb_db_battery_level_get() <= 80) || (clr == MKB_LED_CLR_RED && mkb_db_battery_level_get() <= 80))
        {
            mkb_led_ind_pwr_off();
            clr = MKB_LED_CLR_YELLOW;
            mkb_led_indication(num, clr, MKB_LED_ACT_ON_PUSH_DOWN);
            ________DBG_20211205_battery_test("yellow");
        }
        else if (mkb_db_battery_level_get() != 0)
        { //80~100
            mkb_led_ind_pwr_off();
            clr = MKB_LED_CLR_GREEN;
            mkb_led_indication(num, clr, MKB_LED_ACT_ON_PUSH_DOWN);
            ________DBG_20211205_battery_test("green");
        }
    }
    break;
    }
    ________DBG_20211014_led_event("chnl_event_handler on");
}

static void mkb_led_color_event_handler(mkb_event_t *p_event)
{
    mkb_event_type_t event_type = p_event->type;

    switch (event_type)
    {
    case EVT_LED_COLOR_CH_OS_SET:
    {
    }
    break;
    case EVT_LED_COLOR_PWR_REMAIN_SET:
    {
    }
    break;
    }
}

static void mkb_led_custom_event_handler(mkb_event_t *p_event)
{
    MKB_LED_ACT_t act = p_event->led.act;
    MKB_LED_CLR_t clr = p_event->led.color;
    MKB_LED_NUM_t num = p_event->led.numb;
}

bool mkb_led_event_handler(mkb_event_t *p_event)
{
    mkb_event_type_t event_type = p_event->type;

    switch (event_type)
    {
    case EVT_LED_CH_IDLE:
    {
        ________DBG_20210915_led_callstack("CH event");
        break;
    }
    case EVT_LED_CH_NO_ADDR:
    case EVT_LED_CH_ADV_FAST:
    case EVT_LED_CH_ADV_SLOW:
    case EVT_LED_CH_CONNECT:
    case EVT_LED_CH_DISCONNECT:
    {
        mkb_led_chnl_event_handler(p_event);
    }
    break;
    case EVT_LED_PWR_IDLE:
    case EVT_LED_PWR_CHARGE:
    case EVT_LED_PWR_DISCHARGE:
    case EVT_LED_PWR_ALERT:
    {
        mkb_led_pwr_event_handler(p_event);
    }
    break;
    case EVT_LED_SUB_SET_CH_OS_CHECK_PUSH_DOWN:
    case EVT_LED_SUB_SET_CH_OS_CHECK_PUSH_UP:
    case EVT_LED_SUB_BATT_CHECK:
    {
        mkb_led_sub_event_handler(p_event);
    }
    break;
    case EVT_LED_COLOR_CH_OS_SET:
    case EVT_LED_COLOR_PWR_REMAIN_SET:
    {
        mkb_led_color_event_handler(p_event);
    }
    break;
    case EVT_LED_CUSTOM_SET:
    {
        mkb_led_custom_event_handler(p_event);
    }
    break;
    default:
        break;
    }
    return false;
}

void mkb_led_ind_set(MKB_LED_IND_t ind)
{ //삭제예정..?
    //존재위치
    // mkb_ble.c -> ble_evt_handler
    // mkb_ble_adv.c -> mkb_ble_adv_on_adv_evt()
    // mkb_ble_pm.c -> mkb_ble_pm_evt_handler()
    // mkb_coms.c -> mkb_coms_key_process(), mkb_coms_channel_select(), mkb_coms_channel_new(),
    return;
    uint8_t led_num = ind % LED_CH_MAX;
    switch (ind)
    {
    case LED_IND_IDLE:
    {
        ________DBG_20210915_led_callstack("LED_IND_IDLE");
        mkb_led_ind_ch_off();
        break;
    }
    // DISCONNECT : OFF
    case LED_IND_CH1_DISCONNECT: // 1234
    case LED_IND_CH2_DISCONNECT:
    case LED_IND_CH3_DISCONNECT:
    {

        ________DBG_20210915_led_callstack("LED_IND_CH%d_DISCONNECT", led_num);
        mkb_led_ind_ch_off();
        mkb_led_indication((MKB_LED_NUM_t)led_num, mkb_led_color_get_central(mkb_db_central_type_get(led_num)), MKB_LED_ACT_DIMMING_OFF);
        break;
    }
    case LED_IND_CH4_DISCONNECT:
    {
        ________DBG_20210915_led_callstack("LED_IND_CH4_DISCONNECT");
        break;
    }
    // TOUCH : ACT : ON
    case LED_IND_CH1_TOUCH: // 5678
    case LED_IND_CH2_TOUCH:
    case LED_IND_CH3_TOUCH:
    {
        ________DBG_20210915_led_callstack("LED_IND_CH%d_TOUCH", led_num);
        mkb_led_ind_ch_off();
        mkb_led_indication((MKB_LED_NUM_t)led_num, mkb_led_color_get_central(mkb_db_central_type_get(led_num)), MKB_LED_ACT_ON_50S);
        break;
    }
    case LED_IND_CH4_TOUCH:
    {
        ________DBG_20210915_led_callstack("LED_IND_CH4_TOUCH");
        break;
    }
    // TUOCHLOCK_ACT: NONE
    case LED_IND_CH1_TOUCHLOCK:
    case LED_IND_CH2_TOUCHLOCK:
    case LED_IND_CH3_TOUCHLOCK:
    {
        ________DBG_20210915_led_callstack("LED_IND_CH%d_TOUCHLOCK", led_num);
        break;
    }
    case LED_IND_CH4_TOUCHLOCK:
    {
        ________DBG_20210915_led_callstack("LED_IND_CH4_TOUCHLOCK");
        break;
    }
    // BUTTON_ACT : NONE
    case LED_IND_CH1_BUTTON:
    case LED_IND_CH2_BUTTON:
    case LED_IND_CH3_BUTTON:
    {
        ________DBG_20210915_led_callstack("LED_IND_CH%d_BUTTON", led_num);
        break;
    }
    case LED_IND_CH4_BUTTON:
    {
        ________DBG_20210915_led_callstack("LED_IND_CH4_BUTTON");
        break;
    }
    // CONNECt_ACT : DIM ON
    case LED_IND_CH1_CONNECT:
    case LED_IND_CH2_CONNECT:
    case LED_IND_CH3_CONNECT:
    {
        uint8_t led_num = ind % LED_CH_MAX;
        ________DBG_20210915_led_callstack("LED_IND_CH%d_CONNECT", led_num);
        mkb_led_ind_ch_off();
        // mkb_led_indication((MKB_LED_NUM_t)led_num, (MKB_LED_CLR_t)(ind % LED_CH_MAX), MKB_LED_ACT_ON_50S);
        mkb_led_indication((MKB_LED_NUM_t)led_num, mkb_led_color_get_central(mkb_db_central_type_get(led_num)), MKB_LED_ACT_ON_50S);
        break;
    }
    case LED_IND_CH4_CONNECT:
    {
        ________DBG_20210915_led_callstack("LED_IND_CH4_CONNECT");
        break;
    }
    // SELECT_ACT : DIMOUT
    case LED_IND_CH1_SELECT:
    case LED_IND_CH2_SELECT:
    case LED_IND_CH3_SELECT:
    {
        uint8_t led_num = ind % LED_CH_MAX;
        ________DBG_20210915_led_callstack("LED_IND_CH%d_SELECT", led_num);
        mkb_led_ind_ch_off();
        mkb_led_indication((MKB_LED_NUM_t)led_num, mkb_led_color_get_central(mkb_db_central_type_get(led_num)), MKB_LED_ACT_DIMMING_OFF);
        break;
    }
    case LED_IND_CH4_SELECT:
    {
        ________DBG_20210915_led_callstack("LED_IND_CH4_SELECT");
        break;
    }
    // ADV_DIRECT_ACT : NONE
    case LED_IND_CH1_ADV_DIRECT:
    case LED_IND_CH2_ADV_DIRECT:
    case LED_IND_CH3_ADV_DIRECT:
    {

        ________DBG_20210915_led_callstack("LED_IND_CH%d_ADV_DIRECT", led_num);
        mkb_led_ind_ch_off();
        mkb_led_indication((MKB_LED_NUM_t)led_num, mkb_led_color_get_central(mkb_db_central_type_get(led_num)), MKB_LED_ACT_FAST_BLINK);
        break;
    }
    case LED_IND_CH4_ADV_DIRECT:
    {
        ________DBG_20210915_led_callstack("LED_IND_CH4_ADV_DIRECT");
        break;
    }
    // ADV_WHITE_LIST : NONE
    case LED_IND_CH1_ADV_WHITELIST:
    case LED_IND_CH2_ADV_WHITELIST:
    case LED_IND_CH3_ADV_WHITELIST:
    {

        ________DBG_20210915_led_callstack("LED_IND_CH%d_ADV_WHITELIST", led_num);
        mkb_led_ind_ch_off();
        mkb_led_indication((MKB_LED_NUM_t)led_num, mkb_led_color_get_central(mkb_db_central_type_get(led_num)), MKB_LED_ACT_FAST_BLINK);
    }
    case LED_IND_CH4_ADV_WHITELIST:
    {
        ________DBG_20210915_led_callstack("LED_IND_CH4_ADV_WHITELIST");
        break;
    }
    // ADV_FAST : FAST
    case LED_IND_CH1_ADV_FAST:
    case LED_IND_CH2_ADV_FAST:
    case LED_IND_CH3_ADV_FAST:
    {

        ________DBG_20210915_led_callstack("LED_IND_CH%d_ADV_FAST", led_num);
        mkb_led_ind_ch_off();
        mkb_led_indication((MKB_LED_NUM_t)led_num, mkb_led_color_get_central(mkb_db_central_type_get(led_num)), MKB_LED_ACT_FAST_BLINK);

        break;
    }
    case LED_IND_CH4_ADV_FAST:
    {
        ________DBG_20210915_led_callstack("LED_IND_CH4_ADV_FAST");
        break;
    }
    // ADV_SLOW : SLOW
    case LED_IND_CH1_ADV_SLOW:
    case LED_IND_CH2_ADV_SLOW:
    case LED_IND_CH3_ADV_SLOW:
    {

        ________DBG_20210915_led_callstack("LED_IND_CH%d_ADV_SLOW", led_num);
        mkb_led_ind_ch_off();
        mkb_led_indication((MKB_LED_NUM_t)led_num, mkb_led_color_get_central(mkb_db_central_type_get(led_num)), MKB_LED_ACT_SLOW_BLINK);
        break;
    }
    case LED_IND_CH4_ADV_SLOW:
    {
        ________DBG_20210915_led_callstack("LED_IND_CH4_ADV_SLOW");
        break;
    }
    case LED_IND_CH1_ADV_IDLE:
    case LED_IND_CH2_ADV_IDLE:
    case LED_IND_CH3_ADV_IDLE:
    {

        ________DBG_20210915_led_callstack("LED_IND_CH%d_ADV_IDLE", led_num);
        mkb_led_ind_ch_off();
        mkb_led_indication((MKB_LED_NUM_t)led_num, mkb_led_color_get_central(mkb_db_central_type_get(led_num)), MKB_LED_ACT_DIMMING_OFF);
        break;
    }
    case LED_IND_CH4_ADV_IDLE:
    {
        ________DBG_20210915_led_callstack("LED_IND_CH4_ADV_IDLE");
        break;
    }
    case LED_IND_USB:
    {
        ________DBG_20210915_led_callstack("LED_IND_USB");
        break;
    }
    case LED_IND_USB_POWER_ON:
    {
        mkb_led_indication(LED_CH_PWR, MKB_LED_CLR_WHITE, MKB_LED_ACT_ON_50S);
        ________DBG_20210915_led_callstack("LED_IND_USB_POWER_ON");
        break;
    }
    case LED_IND_USB_POWER_OFF:
    {
        mkb_led_indication(LED_CH_PWR, MKB_LED_CLR_WHITE, MKB_LED_ACT_OFF);
        ________DBG_20210915_led_callstack("LED_IND_USB_POWER_OFF");
        break;
    }
    case LED_IND_ERROR:
    {
        ________DBG_20210915_led_callstack("LED_IND_ERROR");
        break;
    }
    case LED_IND_WARN:
    {
        ________DBG_20210915_led_callstack("LED_IND_WARN");
        break;
    }
    case LED_IND_GOOD:
    {
        ________DBG_20210915_led_callstack("LED_IND_GOOD");
        break;
    }
    case LED_IND_ALERT:
    {
        ________DBG_20210915_led_callstack("LED_IND_ALERT");
        break;
    }
    case LED_IND_BTR_FULL:
    {
        ________DBG_20210915_led_callstack("LED_IND_BTR_FULL");
        mkb_led_ind_pwr_off();
        mkb_led_indication(LED_CH_PWR, MKB_LED_CLR_GREEN, MKB_LED_ACT_ON_50S);
        break;
    }
    case LED_IND_BTR_GOOD:
    {
        ________DBG_20210915_led_callstack("LED_IND_BTR_GOOD");
        mkb_led_ind_pwr_off();
        mkb_led_indication(LED_CH_PWR, MKB_LED_CLR_YELLOW, MKB_LED_ACT_ON_50S);
        break;
    }
    case LED_IND_BTR_LOW:
    {
        ________DBG_20210915_led_callstack("LED_IND_BTR_LOW");
        mkb_led_ind_pwr_off();
        mkb_led_indication(LED_CH_PWR, MKB_LED_CLR_RED, MKB_LED_ACT_ON_50S);
        break;
    }
    case LED_IND_FUNC_DOWN:
    {
        ________DBG_20210915_led_callstack("LED_IND_BTR_LOW");
        mkb_led_ind_pwr_off();
        mkb_led_indication(LED_CH_PWR, MKB_LED_CLR_RED, MKB_LED_ACT_ON_50S);
        break;
    }
    case LED_IND_FUNC_UP:
    {
        ________DBG_20210915_led_callstack("LED_IND_BTR_LOW");
        mkb_led_ind_pwr_off();
        mkb_led_indication(LED_CH_PWR, MKB_LED_CLR_RED, MKB_LED_ACT_ON_50S);
        break;
    }
    }
}

void mkb_led_change_main_color(MKB_LED_NUM_t numb, MKB_LED_CLR_t color)
{
    return;
    mkb_led_color_get(color, &(m_leds[numb].state_main.color));
    m_leds[numb].pwm.p_seq_values->channel_0 = m_leds[numb].state_main.color.red * (m_leds[numb].state_main.brightness / 100.0);
    m_leds[numb].pwm.p_seq_values->channel_1 = m_leds[numb].state_main.color.green * (m_leds[numb].state_main.brightness / 100.0);
    m_leds[numb].pwm.p_seq_values->channel_2 = m_leds[numb].state_main.color.blue * (m_leds[numb].state_main.brightness / 100.0);
}

static void mkb_led_ind_layer_start(MKB_LED_NUM_t numb, MKB_LED_CLR_t color)
{
    ________DBG_20210915_led_callstack("mkb_led_ind_layer_start");

    m_leds[numb].working_layer = MKB_LED_LAYER_SUB;
    m_leds[numb].state_sub.brightness = 100;

    mkb_led_color_get(color, &(m_leds[numb].state_sub.color));

    m_leds[numb].pwm.p_seq_values->channel_0 = m_leds[numb].state_sub.color.red * (m_leds[numb].state_sub.brightness / 100.0);
    m_leds[numb].pwm.p_seq_values->channel_1 = m_leds[numb].state_sub.color.green * (m_leds[numb].state_sub.brightness / 100.0);
    m_leds[numb].pwm.p_seq_values->channel_2 = m_leds[numb].state_sub.color.blue * (m_leds[numb].state_sub.brightness / 100.0);
}
static void mkb_led_ind_layer_stop(MKB_LED_NUM_t numb)
{
    /*
    m_leds[numb].pwm.p_seq_values->channel_0 = 0;
    m_leds[numb].pwm.p_seq_values->channel_1 = 0;
    m_leds[numb].pwm.p_seq_values->channel_2 = 0;
    */

    m_leds[numb].working_layer = MKB_LED_LAYER_BASE;

    // if (m_leds[numb].state_main.act == MKB_LED_ACT_ON_50S)
    {
        mkb_led_process(&m_leds[numb]);
    }
}

void mkb_led_indication_debug(uint8_t numb, uint8_t red, uint8_t green, uint8_t blue)
{ //채널 컬러만 강제 변경. 후에 삭제예정
    m_leds[numb].pwm.p_seq_values->channel_0 = red;
    m_leds[numb].pwm.p_seq_values->channel_1 = green;
    m_leds[numb].pwm.p_seq_values->channel_2 = blue;
}

void mkb_led_init(void)
{
    ret_code_t err_code;

    err_code = app_timer_create(&m_led_ch_timer, APP_TIMER_MODE_SINGLE_SHOT, mkb_led_ch_timer_handler);
    err_code = app_timer_create(&m_led_pwr_timer, APP_TIMER_MODE_SINGLE_SHOT, mkb_led_pwr_timer_handler);

    m_leds[LED_CH_1].pwm.p_seq = &m_led1_seq;
    m_leds[LED_CH_1].pwm.p_seq_values = &m_led1_seq_values,
    m_leds[LED_CH_1].pwm.p_pwm = &m_pwm1;
    m_leds[LED_CH_1].timer = &m_led_ch_timer;
    mkb_led_clear_state(&m_leds[LED_CH_1]);

    m_leds[LED_CH_2].pwm.p_seq = &m_led2_seq;
    m_leds[LED_CH_2].pwm.p_seq_values = &m_led2_seq_values,
    m_leds[LED_CH_2].pwm.p_pwm = &m_pwm2;
    m_leds[LED_CH_2].timer = &m_led_ch_timer;
    mkb_led_clear_state(&m_leds[LED_CH_2]);

    m_leds[LED_CH_3].pwm.p_seq = &m_led3_seq;
    m_leds[LED_CH_3].pwm.p_seq_values = &m_led3_seq_values,
    m_leds[LED_CH_3].pwm.p_pwm = &m_pwm3;
    m_leds[LED_CH_3].timer = &m_led_ch_timer;
    mkb_led_clear_state(&m_leds[LED_CH_3]);

    m_leds[LED_CH_PWR].pwm.p_seq = &m_led_pwr_seq;
    m_leds[LED_CH_PWR].pwm.p_seq_values = &m_led_pwr_seq_values,
    m_leds[LED_CH_PWR].pwm.p_pwm = &m_pwm4;
    m_leds[LED_CH_PWR].timer = &m_led_pwr_timer;
    mkb_led_clear_state(&m_leds[LED_CH_PWR]);

    nrf_drv_pwm_init(m_leds[LED_CH_1].pwm.p_pwm, &led1_cfg, NULL);
    nrf_drv_pwm_init(m_leds[LED_CH_2].pwm.p_pwm, &led2_cfg, NULL);
    nrf_drv_pwm_init(m_leds[LED_CH_3].pwm.p_pwm, &led3_cfg, NULL);
    nrf_drv_pwm_init(m_leds[LED_CH_PWR].pwm.p_pwm, &led_pwr_cfg, NULL);

    MKB_LOG_INFO("Initialization Success!");
}

ret_code_t mkb_led_cli(size_t size, char **params)
{
    ret_code_t err_code = NRF_SUCCESS;
    if (!strcmp(params[1], "help"))
    {
    }
    else if (!strcmp(params[1], "mkb_led_ind_set"))
    {
        mkb_led_ind_set((MKB_LED_IND_t)atoi(params[2]));
    }
    else if (!strcmp(params[1], "custom"))
    {
        mkb_led_indication_debug(atoi(params[2]), atoi(params[3]), atoi(params[4]), atoi(params[5]));
    }
    else
    {
        err_code = NRF_ERROR_INTERNAL;
    }
    return err_code;
}

#if (defined(NRF_PWR_MGMT_ENABLED) && NRF_PWR_MGMT_ENABLED)
static bool mkb_led_shutdown(nrf_pwr_mgmt_evt_t event)
{
    MKB_LOG_INFO("Shutdown prepare");

    // mkb_led_indication(LED_IND_IDLE);

    // mkb_led_ind_disable();

    mkb_led_ind_ch_off();
    mkb_led_ind_pwr_off();
    mkb_led_ind_disable();

    return true;
}

NRF_PWR_MGMT_HANDLER_REGISTER(mkb_led_shutdown, SHUTDOWN_PRIORITY_DEFAULT);
#endif /* NRF_PWR_MGMT_ENABLED */
