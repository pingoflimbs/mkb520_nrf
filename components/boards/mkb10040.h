/**
 * Copyright (c) 2014 - 2019, Nordic Semiconductor ASA
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form, except as embedded into a Nordic
 *    Semiconductor ASA integrated circuit in a product or a software update for
 *    such product, must reproduce the above copyright notice, this list of
 *    conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 *
 * 3. Neither the name of Nordic Semiconductor ASA nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * 4. This software, with or without modification, must only be used with a
 *    Nordic Semiconductor ASA integrated circuit.
 *
 * 5. Any software provided in binary form under this license must not be reverse
 *    engineered, decompiled, modified and/or disassembled.
 *
 * THIS SOFTWARE IS PROVIDED BY NORDIC SEMICONDUCTOR ASA "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NORDIC SEMICONDUCTOR ASA OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#ifndef MKB10040_H
#define MKB10040_H

#ifdef __cplusplus
extern "C" {
#endif

#include "nrf_gpio.h"

// LEDs definitions for MKB10040
#define LEDS_NUMBER    0

#define LED_START      17
#define LED_1          17
#define LED_2          18
#define LED_3          19
#define LED_4          20
#define LED_STOP       20

#define LEDS_ACTIVE_STATE 0

#define LEDS_INV_MASK  LEDS_MASK

#define LEDS_LIST { LED_1, LED_2, LED_3, LED_4 }

#define BSP_LED_0      LED_1
#define BSP_LED_1      LED_2
#define BSP_LED_2      LED_3
#define BSP_LED_3      LED_4

#define BUTTONS_NUMBER 2

#define BUTTON_START   25
#define BUTTON_1       25   // Left Click
#define BUTTON_2       31   // Right Click
#define BUTTON_STOP    31
#define BUTTON_PULL    NRF_GPIO_PIN_PULLDOWN

#define BUTTONS_ACTIVE_STATE 0

#define BUTTONS_LIST { BUTTON_1, BUTTON_2 }

#define BSP_BUTTON_0   BUTTON_1
#define BSP_BUTTON_1   BUTTON_2

/* MOKIBO MKB316 Keyboard Input pins */
#define KEY_ROW_01_PIN	0
#define KEY_ROW_02_PIN	1
#define KEY_ROW_03_PIN	2
#define KEY_ROW_04_PIN	3
#define KEY_ROW_05_PIN	4
#define KEY_ROW_06_PIN	5
#define KEY_ROW_07_PIN	6
#define KEY_ROW_08_PIN	7

/* MOKIBO MKB316 Keyboard Output pins */
#define KEY_COL_01_PIN	8
#define KEY_COL_02_PIN	11
#define KEY_COL_03_PIN	12
#define KEY_COL_04_PIN	13
#define KEY_COL_05_PIN	14
#define KEY_COL_06_PIN	15
#define KEY_COL_07_PIN	16
#define KEY_COL_08_PIN	17
#define KEY_COL_09_PIN	18
#define KEY_COL_10_PIN	19
#define KEY_COL_11_PIN	20
#define KEY_COL_12_PIN	21
#define KEY_COL_13_PIN	22
#define KEY_COL_14_PIN	23
#define KEY_COL_15_PIN	24

#define I2C_SCL_PIN     28    // SCL signal pin
#define I2C_SDA_PIN     27    // SDA signal pin
#define ARDUINO_SCL_PIN	I2C_SCL_PIN    // SCL signal pin
#define ARDUINO_SDA_PIN I2C_SDA_PIN    // SDA signal pin

#define CY_RESET_PIN	29
#define	CY_INT_PIN		30

#define CLICK_LEFT_PIN	25
#define CLICK_RIGHT_PIN	31

#ifdef __cplusplus
}
#endif

#endif // MKB10040_H
